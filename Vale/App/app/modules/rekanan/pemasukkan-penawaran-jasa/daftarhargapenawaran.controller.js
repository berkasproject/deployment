﻿(function () {
	'use strict';

	angular.module("app").controller("priceListOfferCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', 'OfferEntryService', '$state', 'UIControlService', 'UploaderService', '$uibModal', 'GlobalConstantService', '$stateParams'];

	function ctrl($http, $translate, $translatePartialLoader, OEService, $state, UIControlService, UploaderService, $uibModal, GlobalConstantService, $stateParams) {
		var vm = this;

		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.IDDoc = Number($stateParams.DocID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.habisTanggal = false;
		vm.showterendah = true;
		vm.subPekerjaan = [];
		vm.init = init;

		function init() {
		    $translatePartialLoader.addPart('pemasukkan-penawaran-jasa');
			UIControlService.loadLoading("MESSAGE.LOADING");
			OEService.getPricelist({
				TenderStepID: vm.IDStepTender
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					vm.subPekerjaan = reply.data;
					vm.currency = vm.subPekerjaan.Curr;
					vm.projectTitle = vm.subPekerjaan.ProjectTitle;
					vm.subPekerjaan.OfferTotalCostStr = reply.data.OfferTotalCost.toFixed(2);
					for (var i = 0; i < reply.data.ServiceOfferEntryPricelists.length; i++) {
					    vm.subPekerjaan.ServiceOfferEntryPricelists[i].TotalPriceStr = reply.data.ServiceOfferEntryPricelists[i].TotalPrice.toFixed(2);
					}

					//{ nama: "SUB.A", total_line: 10900, 
					//	anak: [{
					//		nama: "Sub A1", cucu: [
					//			{ nama: "Administrasi dan Dokumentasi", qty: "1LS", unit: 100000, linecost: 100000 },
					//			{ nama: "Kabel", qty: "5 Meter", unit: 150000, linecost:150000 }]
					//	},
					//		{ nama: "Sub A2" }]
					//},
					//{ nama: "SUB.B", total_line:25000 }

					//console.info("tender::" + JSON.stringify(vm.dataTenderReal));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});

			//UIControlService.loadLoading("Silahkan Tunggu");
			//OEService.getPricelist(vm.listQuest, function (reply) {
			//    UIControlService.unloadLoading();
			//    if (reply.status === 200) {
			//        var data = reply.data;
			//        UIControlService.msg_growl("error", "Berhasil Simpan Data Kuisioner");
			//        vm.init();
			//    }
			//}, function (err) {
			//    UIControlService.msg_growl("error", "MESSAGE.API");
			//    UIControlService.unloadLoading();
		    //});
			loadDataTender();
		}

		function loadDataTender() {
		    OEService.GetSteps({
		        ID: vm.IDStepTender
		    }, function (reply) {
		        vm.stepOvertime = reply.data;
		        for (var i = 0; i < vm.stepOvertime.length; i++) {
		            if (vm.stepOvertime[i].step.FormTypeURL == "pemasukkan-penawaran-jasa") {
		                vm.accessPermission = vm.stepOvertime[i].IsOvertime;

		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		}

		vm.priceChange = priceChange;
		function priceChange(parent, index) {
			var unitPrice = vm.subPekerjaan.ServiceOfferEntryPricelists[parent].ServiceOfferEntryPricelistDetails[index].UnitPrice;
			var qty = vm.subPekerjaan.ServiceOfferEntryPricelists[parent].ServiceOfferEntryPricelistDetails[index].Quantity;
			var count = vm.subPekerjaan.ServiceOfferEntryPricelists[parent].ServiceOfferEntryPricelistDetails;
			var countTotal = vm.subPekerjaan.ServiceOfferEntryPricelists
			var initVal = 0;
			var initTotal = 0;

			vm.subPekerjaan.ServiceOfferEntryPricelists[parent].ServiceOfferEntryPricelistDetails[index].TotalPrice = unitPrice * qty;

			for (var i = 0; i < count.length; i++) {
				initVal = initVal + count[i].TotalPrice;
			}

			vm.subPekerjaan.ServiceOfferEntryPricelists[parent].TotalPrice = initVal;
			vm.subPekerjaan.ServiceOfferEntryPricelists[parent].TotalPriceStr = initVal.toFixed(2);

			for (var i = 0; i < countTotal.length; i++) {
				initTotal = initTotal + countTotal[i].TotalPrice;
			}

			vm.subPekerjaan.OfferTotalCost = initTotal;
			vm.subPekerjaan.OfferTotalCostStr = initTotal.toFixed(2);
		}

		vm.priceChangeChild = priceChangeChild;
		function priceChangeChild(parent, index) {
			var unitPrice = vm.subPekerjaan.ServiceOfferEntryPricelists[parent.$parent.$index].Childs[parent.$index].ServiceOfferEntryPricelistDetails[index].UnitPrice;
			var qty = vm.subPekerjaan.ServiceOfferEntryPricelists[parent.$parent.$index].Childs[parent.$index].ServiceOfferEntryPricelistDetails[index].Quantity;
			var count = vm.subPekerjaan.ServiceOfferEntryPricelists[parent.$parent.$index].Childs;
			var countTotal = vm.subPekerjaan.ServiceOfferEntryPricelists
			var initVal = 0;
			var initTotal = 0;

			vm.subPekerjaan.ServiceOfferEntryPricelists[parent.$parent.$index].Childs[parent.$index].ServiceOfferEntryPricelistDetails[index].TotalPrice = unitPrice * qty;

			for (var i = 0; i < count.length; i++) {
				for (var j = 0; j < count[i].ServiceOfferEntryPricelistDetails.length; j++) {
					initVal = initVal + count[i].ServiceOfferEntryPricelistDetails[j].TotalPrice;
				}
			}
			vm.subPekerjaan.ServiceOfferEntryPricelists[parent.$parent.$index].TotalPrice = initVal;
			vm.subPekerjaan.ServiceOfferEntryPricelists[parent.$parent.$index].TotalPriceStr = initVal.toFixed(2);

			for (var i = 0; i < countTotal.length; i++) {
				initTotal = initTotal + countTotal[i].TotalPrice;
			}
			vm.subPekerjaan.OfferTotalCost = initTotal;
			vm.subPekerjaan.OfferTotalCostStr = initTotal.toFixed(2);
		}

		vm.savePricelist = savePricelist;
		function savePricelist() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			OEService.savePricelist({
				ServiceOfferEntryPricelists: vm.subPekerjaan.ServiceOfferEntryPricelists,
				OfferTotalCost: 0,
				OfferEntryVendorID: vm.subPekerjaan.OfferEntryVendorID,
				ID: vm.subPekerjaan.ID
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
					init();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});

			//vm.subPekerjaan.OfferTotalCost = initTotal;
		}

		vm.back = back;
		function back() {
		    $state.transitionTo('kelengkapan-datakomersial-jasa-vendor', {
		        TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: vm.IDDoc
		    });
		}
	}
})();
