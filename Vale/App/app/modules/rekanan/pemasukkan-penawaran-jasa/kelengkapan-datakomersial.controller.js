﻿(function () {
	'use strict';

	angular.module("app").controller("KelengkapanDataKomersialVendorCtrl", ctrl);

	ctrl.$inject = ['GlobalConstantService','OfferEntryService', '$state', 'UIControlService', '$uibModal', '$stateParams', '$translatePartialLoader'];
	function ctrl(GlobalConstantService, OEService, $state, UIControlService, $uibModal, $stateParams, $translatePartialLoader) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.IDDoc = Number($stateParams.DocID);
		vm.listDocChecks = [];
		vm.TenderName = '';
		vm.TenderCode = '';
		vm.StartDate = null;
		vm.EndDate = null;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('pemasukkan-penawaran-jasa');
			loadDocChecklist();
			loadDataTender();
		}
		function loadDataTender() {
		    OEService.GetSteps({
		        ID: vm.IDStepTender
		    }, function (reply) {
		        vm.stepOvertime = reply.data;
		        for (var i = 0; i < vm.stepOvertime.length; i++) {
		            if (vm.stepOvertime[i].step.FormTypeURL == "pemasukkan-penawaran-jasa") {
		                vm.accessPermission = vm.stepOvertime[i].IsOvertime;
		                vm.TenderName = vm.stepOvertime[i].tender.TenderName;
		                vm.StartDate = UIControlService.convertDateTime(vm.stepOvertime[i].StartDate);
		                vm.EndDate = UIControlService.convertDateTime(vm.stepOvertime[i].EndDate);

		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		}
		function loadDocChecklist() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			OEService.getDataChecklistVendor({
				TenderStepID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listDocChecks = data.VendorChecklists;
					for (var i = 0; i < vm.listDocChecks.length; i++) {
						if (!(vm.listDocChecks[i].ApproveDate === null)) {
							vm.listDocChecks[i].ApproveDate = UIControlService.getStrDate(vm.listDocChecks[i].ApproveDate);
						}
					}
					//console.info(JSON.stringify(vm.listDocChecks));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.detailDokumen = detailDokumen;
		function detailDokumen(data) {
			//console.info("det: " + JSON.stringify(data));
			if (!(data.DocumentType === 'FORM_DOCUMENT')) {
				var datax = {
					DocumentName: data.DocumentName,
					FileType: data.FileType,
					ApproveDate: data.ApproveDate,
					DocumentURL: data.DocumentURL,
					OfferEntryDocumentID: data.OfferEntryChecklistID,
					OfferEntryVendorID: data.OfferEntryVendorID,
					IsCheck: true,
					accessPermission: vm.accessPermission,
                    FlagUpload: true
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-jasa/detailDokumen.html',
					controller: 'detailDokumenJasaCtrl',
					controllerAs: 'detDokCtrl',
					resolve: {
						item: function () {
							return datax;
						}
					}
				});
				modalInstance.result.then(function () {
					vm.init();
				});
			} else if (data.DocumentType == 'FORM_DOCUMENT' && data.DocumentName == 'Tender Questionaire') {
				$state.transitionTo('questionnaire-tenderjasa-vendor', {
					TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: data.OfferEntryChecklistID
				});
			} else if (data.DocumentType == 'FORM_DOCUMENT' && data.DocumentName == 'Pricing') {
				$state.transitionTo('pricelist-serviceoffer-vendor', {
					TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: data.OfferEntryChecklistID
				});
			}
		}

		vm.updateChecklist = updateChecklist;
		function updateChecklist(data) {
			//console.info("data:" + JSON.stringify(data));
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			OEService.approveChecklistByVendor({
				OfferEntryChecklistID: data.OfferEntryChecklistID, OfferEntryVendorID: data.OfferEntryVendorID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					var data = reply.data;
					UIControlService.msg_growl("succcess", "MESSAGE.SUC_SAVE");
					vm.init();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.back = back;
		function back() {
			$state.transitionTo('pemasukkan-penawaran-jasa-vendor', {
				TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType
			});
		}

	}
})();