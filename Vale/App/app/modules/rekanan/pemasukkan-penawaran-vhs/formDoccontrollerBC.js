﻿(function () {
	'use strict';

	angular.module("app").controller("frmBCCtrl", ctrl);

	ctrl.$inject = ['$uibModal', '$translatePartialLoader', 'PPVHSService', 'UploadFileConfigService', 'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService'];
	function ctrl($uibModal, $translatePartialLoader, PPVHSService, UploadFileConfigService, UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {
		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.detail = item.item;
		vm.TenderDocType = item.act;
		vm.Limit = item.Limit;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.maxSizeVhc = 5;
		vm.keyword = '';
		vm.action = "";
		vm.pathFile;
		vm.TenderStepID = item.TenderStepID;
		vm.Description;
		vm.fileUpload;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;
		vm.tglSekarang = UIControlService.getDateNow("");
		vm.flag = false;
		vm.init = init;
		vm.inProcess = item.inProcess;
		vm.DateNow = new Date();
		vm.currentPageVhc = 1;
		vm.currentPageEqp = 1;
		vm.currentPageBld = 1;

		function init() {
			console.info(item);
			if (vm.TenderDocType == 18) {
				vm.flag = false;
				loadExperience();
			} else if (vm.TenderDocType == 19 || vm.TenderDocType == 17) {
				vm.flag = false;
				loadOffice();
			} else if (vm.TenderDocType == 20) {
				vm.flag = false;
				loadEquipmentVehicle();
				loadEquipmentTools();
			} else {
				loadVendor();
				loadCompanyPerson();
				$translatePartialLoader.addPart('surat-pernyataan');
				$translatePartialLoader.addPart('pemasukkan-penawaran-vhs');
				UIControlService.loadLoading("MESSAGE.LOADING");
				//get tipe dan max.size file - 1
				UploadFileConfigService.getByPageName("PAGE.VENDOR.VHS.OFFERENTRY", function (response) {
					UIControlService.unloadLoading();
					if (response.status == 200) {
						vm.name = response.data.name;
						vm.idUploadConfigs = response.data;
						console.info(JSON.stringify(vm.idUploadConfigs));
						vm.idFileTypes = generateFilterStrings(response.data);
						vm.idFileSize = vm.idUploadConfigs[0];

					} else {
						UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
					return;
				});
				if (vm.isAdd === true) {
					vm.action = "Tambah";

				} else {
					vm.action = "Ubah ";
				}
			}
		}

		vm.selected = selected;
		function selected() {
			console.info("respon1:" + JSON.stringify(vm.selectedDocumentType));
		}

		vm.loadVendor = loadVendor;
		function loadVendor() {
			PPVHSService.selectVendor(function (reply) {
				if (reply.status === 200) {
					vm.vendor = reply.data;
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE"); //Gagal menyimpan data
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.loadCompanyPerson = loadCompanyPerson;
		function loadCompanyPerson() {
			PPVHSService.selectCompanyPerson(function (reply) {
				if (reply.status === 200) {
					vm.CompanyPerson = reply.data;
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
					return;
				}
			},
              function (err) {
              	UIControlService.msg_growl("error", "MESSAGE.API");
              	UIControlService.unloadLoadingModal();
              }
           );
		}

		vm.loadVendorCommodity = loadVendorCommodity;
		function loadVendorCommodity() {
			PPVHSService.SelectVendorCommodity({
				VendorID: data
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.comodity = reply.data;
					loadExperience(vm.comodity.BusinessFieldID);
				} else {
					$.growl.error({ message: "ERR.LOAD_EXP" }); //Gagal mendapatkan data pengalaman perusahaan
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadExperience = loadExperience;
		function loadExperience(data) {
			PPVHSService.selectVendorExperience({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword,
				column: 1,
				FilterType: vm.TenderStepID
			}, function (reply) {
				if (reply.status === 200) {
					vm.listFinishExp = reply.data.List;
					for (var i = 0; i < vm.listFinishExp.length; i++) {
						vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);
						if (vm.listFinishExp[i].CityLocation == null) {
						    vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address;
						    if (vm.listFinishExp[i].StateLocationRef) {
						        vm.listFinishExp[i].AddressInfo += ", " + vm.listFinishExp[i].StateLocationRef.Name + ", " + vm.listFinishExp[i].StateLocationRef.Country.Name;
						    }
                        }
						else {
						    vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address + ", " + vm.listFinishExp[i].CityLocation.Name + ", " + vm.listFinishExp[i].CityLocation.State.Country.Name;
						}
                    }
					vm.totalItems = reply.data.Count;
					UIControlService.loadLoading('LOADERS.VENDOREXPERIENCE');
					PPVHSService.selectVendorExperience({
						Offset: (vm.currentPage - 1) * vm.maxSize,
						Limit: vm.maxSize,
						Keyword: vm.keyword,
						column: 2,
						FilterType: vm.TenderStepID
					}, function (reply) {
						//console.info("current?:"+JSON.stringify(reply));
						if (reply.status === 200) {
							vm.listCurrentExp = reply.data.List;
							for (var i = 0; i < vm.listCurrentExp.length; i++) {
								vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
								if (vm.listCurrentExp[i].CityLocation == null) {
								    vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address;
								    if (vm.listCurrentExp[i].StateLocationRef) {
								        vm.listCurrentExp[i].AddressInfo += ", " + vm.listCurrentExp[i].StateLocationRef.Name + ", " + vm.listCurrentExp[i].StateLocationRef.Country.Name;
								    }
								}
								else vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].CityLocation.Name + ", " + vm.listCurrentExp[i].CityLocation.State.Country.Name;
							}
							vm.totalItems = reply.data.Count;
							UIControlService.unloadLoading();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'ERR.LOAD_EXP', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'ERR.LOAD_EXP', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
					});
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'ERR.LOAD_EXP', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'ERR.LOAD_EXP', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
			});


		}

		vm.listOffice = [];
		function loadOffice() {
			var offset = (vm.currentPageBld * vm.maxSize) - vm.maxSize;
			if (vm.TenderDocType == 19)
				vm.category = "BUILDING_CATEGORY_WORKSHOP";
			else if (vm.TenderDocType == 17)
				vm.category = "BUILDING_CATEGORY_WAREHOUSE";

			PPVHSService.SelectBuildingTender({
				Keyword: vm.category,
				Offset: offset,
				Limit: vm.maxSize,
				FilterType: vm.TenderStepID
			}, function (reply) {
				vm.listOffice = reply.data.List;
				vm.totalItems = reply.data.Count;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.editBuilding = editBuilding;
		function editBuilding() {
			var data = {
				TenderStepID: vm.TenderStepID,
				isForm: true,
				type: vm.TenderDocType
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formBuilding.html',
				controller: "FormBuildingCtrl",
				controllerAs: "FormBuildingCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.editActiveBuilding = editActiveBuilding;
		function editActiveBuilding(data) {
			UIControlService.loadLoading("MESSAGE.LOADING"); //Silahkan Tunggu
			PPVHSService.editActiveBulding({
				ID: data.ID,
				IsActive: false,
				//Action: action
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_BUILDING");
					init();
				} else {
					UIControlService.msg_growl("error", "ERR.NONACTIVE");//Gagal menonaktifkan data 
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");//Gagal Akses API
				UIControlService.unloadLoading();
			});
		}

		vm.openform = openform;
		function openform(datatender, cat) {
			console.info(vm.TenderStepID);
			var data = {
				TenderStepID: vm.TenderStepID,
				isForm: true,
				type: vm.TenderDocType,
				item: datatender,
				category: cat
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formBuilding.html',
				controller: "FormBuildingCtrl",
				controllerAs: "FormBuildingCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.editKendaraan = editKendaraan;
		function editKendaraan(dt) {
			var data = {
				TenderStepID: vm.TenderStepID,
				type: vm.TenderDocType,
				category: dt

			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formBuilding.html',
				controller: "FormBuildingCtrl",
				controllerAs: "FormBuildingCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.addExperience = addExperience;
		function addExperience() {
			var data = {
				type: vm.TenderDocType,
				TenderStepID: vm.TenderStepID

			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formBuilding.html',
				controller: "FormBuildingCtrl",
				controllerAs: "FormBuildingCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.editExperience = editExperience;
		function editExperience(obj) {
			var data = {
				item: obj,
				type: vm.TenderDocType,
				isAdd: false

			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formBuilding.html',
				controller: "FormBuildingCtrl",
				controllerAs: "FormBuildingCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.listVehicle = [];
		function loadEquipmentVehicle() {
			var offset = (vm.currentPageVhc * vm.maxSizeVhc) - vm.maxSizeVhc;
			PPVHSService.selectVehicle({ Offset: offset, Limit: vm.maxSizeVhc, FilterType: vm.TenderStepID }, function (reply) {
				vm.listVehicle = reply.data.List;
				vm.totalItemsVhc = reply.data.Count;
				for (var i = 0; i < vm.listVehicle.length; i++) {
					vm.listVehicle[i].MfgDate = UIControlService.getStrDate(vm.listVehicle[i].MfgDate);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.pageChangedVhc = pageChangedVhc;
		function pageChangedVhc() {
			loadEquipmentVehicle();
		}

		vm.pageChangedBld = pageChangedBld;
		function pageChangedBld() {
			loadOffice();
		}

		vm.listEquipmentTools = [];
		function loadEquipmentTools() {
			var offset = (vm.currentPageEqp * vm.maxSizeVhc) - vm.maxSizeVhc;
			PPVHSService.selectEquipment({ Offset: offset, Limit: vm.maxSizeVhc, FilterType: vm.TenderStepID }, function (reply) {
				UIControlService.unloadLoading();
				vm.listEquipmentTools = reply.data.List;
				vm.totalItemsEqp = reply.data.Count;
				for (var i = 0; i < vm.listEquipmentTools.length; i++) {
					vm.listEquipmentTools[i].MfgDate = UIControlService.getStrDate(vm.listEquipmentTools[i].MfgDate);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.pageChangedEqp = pageChangedEqp;
		function pageChangedEqp() {
			loadEquipmentTools();
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			console.info(allowedTypes);
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.selectUpload = selectUpload;
		//vm.fileUpload;
		function selectUpload() {
			console.info((vm.fileUpload));
		}

		/*start upload */
		vm.uploadFile = uploadFile;
		function uploadFile() {

			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			}
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {

			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}


			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileVHSOfferEntry(vm.tglSekarang, file, size, filters, function (response) {
				UIControlService.unloadLoading();
				console.info("response:" + JSON.stringify(response));
				if (response.status == 200) {
					var url = response.data.Url;
					vm.pathFile = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;
					vm.size = s;
					vm.list = [];
					var data = {
						ID: vm.detail.ID,
						IsPublish: vm.detail.IsPublish,
						TenderDocTypeID: vm.detail.TenderDocTypeID,
						vhs: {
							TenderStepID: vm.detail.vhs.TenderStepID
						},
						DocumentUrl: vm.pathFile,
						Filename: vm.name,
						FileSize: vm.size
					};
					vm.list.push(data);
					PPVHSService.InsertOpen(vm.list, function (reply) {
						console.info("reply" + JSON.stringify(reply))
						UIControlService.unloadLoadingModal();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", " FORM.MSG_SUC_UPLOAD");//Berhasil Upload File
							$uibModalInstance.close();
						} else {
							UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE"); //Berhasil Menyimpan Data
							return;
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.API"); //Gagal Akses API
						UIControlService.unloadLoadingModal();
					});
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});
		}

		vm.batal = batal;
		function batal() {
			vm.currentPageBld = 1;
			$uibModalInstance.dismiss('cancel');
		};

		vm.agree = agree;
		function agree(flag) {
			var tender = {
				IsAgree: flag,
				TenderDocTypeID: vm.TenderDocType,
				TenderStepID: vm.TenderStepID
			};
			PPVHSService.InsertDetail(tender, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'MESSAGE.AGREE');
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.editActiveNonBuilding = editActiveNonBuilding;
		function editActiveNonBuilding(data) {
			var act = null; var isTemp = null;
			UIControlService.loadLoading("MESSAGE.LOADING"); //Silahkan Tunggu
			PPVHSService.editActiveNonBulding({
				ID: data.ID,
				IsActive: false
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var msg = "";
					UIControlService.msg_growl("success", "MESSAGE.SUC_NONACTIVE");//Data Berhasil dinonaktifkan
					init();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_NONACTIVE"); //Gagal menonaktifkan data 
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");//Gagal Akses API 
				UIControlService.unloadLoading();
			});
		}

		vm.editActiveExp = editActiveExp;
		function editActiveExp(data) {
			var act = null; var isTemp = null;
			UIControlService.loadLoading("MESSAGE.LOADING");//Silahkan Tunggu
			PPVHSService.editActiveExp({
				ID: data.ID,
				IsActive: false
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var msg = "";
					UIControlService.msg_growl("success", "MESSAGE.SUC_NONACTIVE");//Data Berhasil dinonaktifkan
					init();
				} else {
					UIControlService.msg_growl("error", "ERR.NONACTIVE");//Gagal menonaktifkan data 
					return;
				}
			}, function (err) {

				UIControlService.msg_growl("error", "MESSAGE.API");//Gagal Akses API 
				UIControlService.unloadLoading();
			});
		}

	}
})();