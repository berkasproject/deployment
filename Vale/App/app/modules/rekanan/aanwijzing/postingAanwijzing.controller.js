﻿(function () {
	'use strict';

	angular.module("app").controller("PostingAanwijzingCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location',
        'AanwijzingService', '$state', 'UIControlService', 'UploadFileConfigService',
        'UploaderService', 'GlobalConstantService', '$uibModal', '$stateParams', 'item', '$uibModalInstance', 'AuthService'];
	function ctrl($http, $translate, $translatePartialLoader, $location,
        AanwijzingService, $state, UIControlService, UploadFileConfigService,
        UploaderService, GlobalConstantService, $uibModal, $stateParams, item, $uibModalInstance, AuthService) {
		var vm = this;
		vm.IDAwj = item.IDAwj;
		vm.QuestionTitle = '';
		vm.Question = '';
		vm.fileUpload;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("aanwijzing");
			getTypeSizeFile();
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.VENDOR.TENDER.AANWIJZING", function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				return;
			});
		}

		vm.save = save;
		function save() {
			if (vm.fileUpload === undefined) {
				processsave('');
			} else {
				uploadFile();
			}
		}

		/*proses upload file*/
		function uploadFile() {
			AuthService.getUserLogin(function (reply) {
				vm.VendorLogin = reply.data.CurrentUsername;
				if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.VendorLogin);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});
		}

		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoadingModal("MESSAGE.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, dates, function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					var url = response.data.Url;
					UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
					processsave(url);

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoadingModal();
			});

		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		/* end proses upload*/

		function processsave(url) {
			if (url === null) {
				url = " ";
			}
			var datasimpan = {
				AanwijzingStepID: vm.IDAwj,
				QuestionTitle: vm.QuestionTitle,
				Question: vm.Question,
				UploadURL: url
			}
			AanwijzingService.postingQuestionByVendor(datasimpan, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
					$uibModalInstance.close();
				}
				else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();