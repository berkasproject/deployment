(function () {
    'use strict';

    angular.module("app").controller("IzinUsahaController", ctrl);

    ctrl.$inject = ['$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'IzinUsahaService', 'AuthService', 'UIControlService', '$filter'];
    /* @ngInject */
    function ctrl($http, $uibModal, $translate, $translatePartialLoader, $location, IzinUsahaService, AuthService, UIControlService, $filter) {
        var vm = this;

        vm.listLicensi = [];
        vm.isChangeData = false;
        vm.IsApprovedCR = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-izinusaha');
            jLoad();
            chekcIsVerified();
        }

        function chekcIsVerified() {
            IzinUsahaService.getCRbyVendor({ CRName: 'OC_VENDORLICENSI' }, function (reply) {
                if (reply.status === 200) {
                    vm.CR = reply.data;
                    if (reply.data) {
                        vm.IsApprovedCR = true;
                    } else {
                        vm.IsApprovedCR = false;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function checkCR() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            IzinUsahaService.getCRbyVendor({ CRName: 'OC_VENDORLICENSI' }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    console.info("CR:" + JSON.stringify(reply.data));
                    //vm.CR = reply.data.length;
                    if (reply.data.length > 0) {
                        //if (reply.data === true) {
                        vm.IsApprovedCR = true;
                        // }
                        /*
                    else {
                        vm.isSentCR = false;
                    }*/
                    }
                    console.info(JSON.stringify(vm.IsApprovedCR));
                }

            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            IzinUsahaService.selectLicensi(function (response) {
                if (response.status == 200) {
                    vm.VendorID = response.data[0] ? response.data[0].VendorID : null;
                    var list = response.data;
                    for (var i = 0; i < list.length; i++) {
                        if (!(list[i].IssuedDate === null)) {
                            list[i].IssuedDate = UIControlService.getStrDate(list[i].IssuedDate);
                        }
                        if (!(list[i].ExpiredDate === null)) {
                            list[i].ExpiredDate = UIControlService.getStrDate(list[i].ExpiredDate);
                        }
                    }
                    vm.listLicensi = list;
                    checkExpiredDate(vm.listLicensi);
                    loadCityCompany();
                    UIControlService.unloadLoading();
                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    UIControlService.unloadLoading();
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }


        function loadCityCompany() {
            IzinUsahaService.selectcontact({ VendorID: vm.VendorID }, function (reply) {
                if (reply.status == 200) {
                    vm.contactCompany = reply.data;
                    for (var i = 0; i < vm.contactCompany.length; i++) {
                        if (vm.contactCompany[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            console.info("kontak" + JSON.stringify(vm.contactCompany[i].Contact.Address.State.Country.CountryID));
                            vm.cityID = vm.contactCompany[i].Contact.Address.State.Country.CountryID;
                            break
                        }
                    }

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }


        function checkExpiredDate(listLicensi) {
            console.info("masuk");
            var today = moment().format("YYYY-MM-DD");
            console.info(JSON.stringify(today));
            console.info(today);
            vm.dateA = []; vm.dateB = []; vm.dateC = [];
            for (var i = 0; i < listLicensi.length; i++) {
                vm.dateA[i] = moment(listLicensi[i].ExpiredDate).subtract(90, 'days').format("YYYY-MM-DD");
                vm.dateB[i] = moment(listLicensi[i].ExpiredDate).subtract(60, 'days').format("YYYY-MM-DD");
                vm.dateC[i] = moment(listLicensi[i].ExpiredDate).subtract(30, 'days').format("YYYY-MM-DD");
                //vm.coba = moment(listLicensi[0].ExpiredDate).subtract(30, 'days').format("YYYY-MM-DD");
                //console.info(JSON.stringify(vm.coba));
                if (vm.dateA[i] === today) {
                    console.info("hariini");
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 90;
                } else if (vm.dateB[i] === today) {
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 60;
                } else if (vm.dateC[i] === today) {
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 30;
                } else if (listLicensi[i].ExpiredDate === today) {
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 0;
                }
            }
        }

        //load email vendor
        vm.loadEmailCompany = loadEmailCompany;
        function loadEmailCompany() {
            //console.info("kirimemail");
            IzinUsahaService.selectcontact({ VendorID: vm.VendorID }, function (reply) {
                if (reply.status == 200) {
                    vm.contact = reply.data;
                    vm.listEmail = [];
                    for (var i = 0; i < vm.contact.length; i++) {
                        if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            vm.listEmail.push(vm.contact[i].Contact.Email);
                        }
                    }
                    //console.info("list email" + JSON.stringify(vm.listEmail));
                    sendMail(vm.listEmail);
                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }


        vm.sendMail = sendMail;
        function sendMail(listEmail) {
            //console.info("kirimemail");
            var email = {
                subject: 'Notifikasi Ijin Usaha',
                mailContent: 'Kurang ' + vm.days + ' hari lagi ijin usaha ' + vm.LicenseName + ' akan kadaluarsa. Terima kasih.',
                isHtml: false,
                addresses: listEmail
            };
            //console.info("kirimemail");
            // UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            IzinUsahaService.sendMail(email, function (response) {
                // UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.days = ""; vm.LicenseName = "";
                    UIControlService.msg_growl("notice", "MESSAGE.SENT_EMAIL")
                } else {
                    UIControlService.handleRequestError(response.data);
                }
            }, function (response) {
                UIControlService.handleRequestError(response.data);
                UIControlService.unloadLoading();
            });
        }

        vm.deleteLic = deleteLic;
        function deleteLic(lic) {
            vm.lic = lic;
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
                if (reply) {
                    UIControlService.loadLoading("DELETING");
                    IzinUsahaService.deleteLic({ LicenseID: lic.LicenseID, VendorID: lic.VendorID }, function (reply) {
                        if (reply.status == 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("success", "MESSAGE.DELETE_SUCCESS");
                            window.location.reload();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", "MESSAGE.DELETE_FAILED");
                            return;
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", "MESSAGE.DELETE_FAILED");
                        return;
                    });
                }
            });
        }

        //open form
        vm.openForm = openForm;
        function openForm(data, isForm) {
            var data = {
                item: data,
                isForm: isForm,
                cityID: vm.cityID
            }
            var temp;
            if (isForm === true) {
                temp = "app/modules/rekanan/data-perusahaan/izin-usaha/form-izin-usaha.html";
            } else {
                temp = "app/modules/rekanan/data-perusahaan/izin-usaha/detail-izin-usaha.html";
            }
            var modalInstance = $uibModal.open({
                templateUrl: temp,
                controller: 'FormIzinCtrl',
                controllerAs: 'FormIzinCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        }
    }
})();