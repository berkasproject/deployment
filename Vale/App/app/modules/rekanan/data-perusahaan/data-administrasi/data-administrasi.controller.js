(function () {
	'use strict';

	angular.module("app").controller("DataAdministrasiCtrl", ctrl);

	ctrl.$inject = ['$uibModal', '$translatePartialLoader', 'VerifiedSendService', 'DataAdministrasiService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'VendorRegistrationService'];
	/* @ngInject */
	function ctrl($uibModal, $translatePartialLoader, VerifiedSendService, DataAdministrasiService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService) {
		var vm = this;
		var flag;

		vm.vendorContactForm;
		vm.address1;
		vm.pageSize = 10;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.bisaMengubahData;
		vm.dataAdmin = {};
		vm.arrProvince = [];
		vm.arrTown = [];
		vm.administrasi = [];
		vm.contact = [];
		vm.id_page_config = 1;
		vm.businessID;
		vm.administrasiDate = {};
		vm.isCalendarOpened = [false, false, false, false];
		vm.pathFile;
		vm.fileUpload;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;
		vm.selectedTypeVendor = {};
		vm.listCurrFalse = [];
		vm.listPersFalse = [];
		vm.address = {
			AddressID: 0,
			AddressInfo: "",
			PostalCode: "",
			StateID: 0,
			CityID: 0,
			DistrictID: 0
		};
		vm.flag = false;
		vm.addressFlag = 0;
		vm.addressAlterFlag = 0;
		vm.AddressAlterId = 0;
		vm.address2 = {
			AddressID: 0,
			AddressInfo: "",
			PostalCode: "",
			StateID: 0,
			CityID: 0,
			DistrictID: 0
		};
		vm.addresses = [];
		vm.contact1 = {
			ContactID: 0,
			Name: "",
			Website: "",
			Phone: "",
			MobilePhone: "",
			Email: "",
			email3: "",
			Address: {}
		};
		vm.selectedBE = null;
		vm.Contact = [];
		vm.contactpersonal = {};
		vm.isApprovedCRAdm = false;
		vm.isApprovedCRBF = false;
		vm.isCalendarOpened = [false, false, false, false];
		vm.IsEdit = false;
		vm.IsEditAlter = false;
		vm.initialize = initialize;

		vm.isVerified = null;
		function initialize() {
			$translatePartialLoader.addPart('data-administrasi');
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.ADMINISTRATION.PKP", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.NPWP", function (response) {
			    UIControlService.unloadLoading();
			    if (response.status == 200) {
			        vm.name1 = response.data.name;
			        vm.idUploadConfigs1 = response.data;
			        vm.idFileTypes1 = generateFilterStrings(response.data);
			        vm.idFileSize1 = vm.idUploadConfigs1[0];

			    } else {
			        UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
			        return;
			    }
			}, function (err) {
			    UIControlService.msg_growl("error", "MESSAGE.API");
			    UIControlService.unloadLoading();
			    return;
			});
			loadCurrency();
			loadVerifiedVendor();
			jLoad(1);
			loadCountryAlternatif();
			loadCheckCR();
			loadFilePrefix();
			cekPrakualifikasiVendor();
		}

		vm.loadFilePrefix = loadFilePrefix;
		function loadFilePrefix() {
		    //UIControlService.loadLoading("LOADERS.LOADING_PREFIX");
		    VendorRegistrationService.getUploadPrefix(
                function (response) {
                    var prefixes = response.data;
                    vm.prefixes = {};
                    for (var i = 0; i < prefixes.length; i++) {
                        vm.prefixes[prefixes[i].Name] = prefixes[i];
                    }
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.loadPhoneCodes = loadPhoneCodes;
		function loadPhoneCodes(data) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			VendorRegistrationService.getCountries(
              function (response) {
              	vm.phoneCodeList = response.data;
              	for (var i = 0; i < vm.phoneCodeList.length; i++) {
              		if (vm.phoneCodeList[i].PhonePrefix === data) {
              			vm.phoneCode = vm.phoneCodeList[i];
              		}
              	}
              	UIControlService.unloadLoading();
              }, function (err) {
              	//$.growl.error({ message: "Gagal Akses API >" + err });
              	UIControlService.unloadLoading();
              });
		}

		vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
		function cekPrakualifikasiVendor() {
		    DataAdministrasiService.cekPrakualifikasiVendor(function (reply) {
		        UIControlService.loadLoadingModal();
		        if (reply.status === 200) {
		            vm.pqWarning = reply.data;
		            //console.info("isregistered" + vm.pqWarning);
		            UIControlService.unloadLoadingModal();
		        } else {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		    });
		}

		function loadCheckCR() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			DataAdministrasiService.getCRbyVendor({ CRName: 'OC_ADM_LEGAL' }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.CR = reply.data;
				    //console.info("cr" + vm.CR);
					if (reply.data === true) {
						vm.isApprovedCRAdm = true;
					}
					else {
						if (reply.data === false) {
							vm.isApprovedCRAdm = false;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
			DataAdministrasiService.getCRbyVendor({ CRName: 'OC_VENDORBUSINESSFIELD' }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.CRBF = reply.data;
					if (reply.data === true) {
						vm.isApprovedCRBF = true;
					}
					else {
						if (reply.data === false) {
							vm.isApprovedCRBF = false;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadVerifiedVendor = loadVerifiedVendor;
		function loadVerifiedVendor() {
			VerifiedSendService.selectVerifikasi(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data;
					//vm.isVerified = vm.verified.Isverified;
					//loadCheckCR();
					vm.cekTemporary = vm.verified.IsTemporary;
					//console.info("verified" + JSON.stringify(vm.verified));
					if (vm.verified.Isverified == null) {
					    if (vm.verified.VerifiedSendDate != null) {
					        vm.isVerified = false;
					    }
					}
					else {
					    if (vm.verified.Isverified == true) {
					        vm.isVerified = true;
					    }
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			DataAdministrasiService.select({
				Offset: offset,
				Limit: vm.pageSize,
				Keyword: vm.Username
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = {};
					vm.administrasi = reply.data[0];
					vm.administrasiDate.StartDate = vm.administrasi.FoundedDate;
					vm.Username = vm.administrasi.user.Username;
					vm.PKPNumber = vm.administrasi.PKPNumber;
					convertToDate();
					loadTypeVendor(vm.administrasi);
					loadAssociation(vm.administrasi);
					loadVendorCommodity(vm.administrasi.VendorID);
					DataAdministrasiService.selectcontact({
						VendorID: vm.administrasi.VendorID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							vm.listPersonal = [];
							vm.contact = reply.data;
							vm.flagPrimary = 0;
							for (var i = 0; i < vm.contact.length; i++) {
								if (vm.contact[i].IsPrimary === 2) { vm.flagPrimary = 1; }
								if (i == (vm.contact.length - 1)) {
									if (vm.flagPrimary == 0) {
										loadCountryAlternatif();
									}
								}
							}
							for (var i = 0; i < vm.contact.length; i++) {
								if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
									vm.VendorContactTypeCompany = vm.contact[i].VendorContactType;
									if (vm.contact[i].Contact.Fax !== null) {
										vm.fax = vm.contact[i].Contact.Fax;
									}
									//console.info(vm.contact[i].Contact);
									vm.ContactID = vm.contact[i].Contact.ContactID;
									if (vm.contact[i].Contact.Phone != null) {
									    //console.info("phone" + vm.contact[i].Contact.Phone);
									    vm.phone = vm.contact[i].Contact.Phone.split(' ');
									    vm.Phone = (vm.phone[1]);
									    if (vm.Phone == undefined) {
									        vm.Phone = vm.contact[i].Contact.Phone;
									    }
									    vm.phone = vm.phone[0].split(')');
									    vm.phone = vm.phone[0].split('(');
									    loadPhoneCodes(vm.phone[1]);
									}
									else loadPhoneCodes(undefined);
									vm.EmailOri = vm.contact[i].Contact.Email;
									vm.Email = vm.contact[i].Contact.Email;
									vm.Website = vm.contact[i].Contact.Website;
									vm.addressIdComp = vm.contact[i].Contact.AddressID;
									loadCountryAdmin(vm.contact[i].Contact.Address.State);
									vm.CityCompany = vm.contact[i].Contact.Address.City;
									vm.DistrictCompany = vm.contact[i].Contact.Address.Distric;
									vm.Region = vm.contact[i].Contact.Address.State.Country.Continent.Name;
									if (vm.contact[i].IsPrimary === 1 || vm.contact[i].IsPrimary === null) {
										vm.CountryCode = vm.contact[i].Contact.Address.State.Country.Code;
										loadCurrencies(vm.administrasi);
										if (vm.CountryCode == 'IDN') {
											loadBusinessEntity(vm.administrasi);
										}
									}
									//console.info("isprim" + JSON.stringify(vm.contact[i].IsPrimary));
									vm.contactpersonal = vm.contact[i];
								} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {
									vm.listPersonal.push(vm.contact[i]);
									vm.VendorContactTypePers = vm.contact[i].VendorContactType;
								} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === null) {
									vm.addressFlag = 1;
									vm.ContactOfficeId = vm.contact[i].Contact.ContactID;
									vm.ContactName = "Kantor Pusat";
									vm.Name = vm.contact[i].Contact.Name;
									vm.AddressId = vm.contact[i].Contact.AddressID;
									vm.VendorContactType = vm.contact[i].VendorContactType;

									vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
									vm.cekAddress = vm.contact[i].Contact.Address.AddressInfo;
									vm.postalcode = vm.contact[i].Contact.Address.PostalCode;
									vm.cekPostCode = vm.postalcode;
									loadCountry(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
									vm.selectedState1 = vm.contact[i].Contact.Address.State;
									if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
										vm.selectedCity1 = vm.contact[i].Contact.Address.City;
										vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

									}
								} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === null) {
									if (vm.addressFlag == 0) {
										vm.AddressId = vm.contact[i].Contact.AddressID;
										vm.ContactOfficeId = vm.contact[i].Contact.ContactID;
										vm.VendorContactType = vm.contact[i].VendorContactType;
										vm.ContactName = "Kantor Cabang";
										vm.Name = vm.contact[i].Contact.Name;
										vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
										vm.cekAddress = vm.contact[i].Contact.Address.AddressInfo;
										vm.postalcode = vm.contact[i].Contact.Address.PostalCode;
										vm.cekPostCode = vm.postalcode;
										loadCountry(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
										vm.selectedState1 = vm.contact[i].Contact.Address.State;
										if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
											vm.selectedCity1 = vm.contact[i].Contact.Address.City;
											vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

										}
									}


								} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === 2) {
									vm.addressAlterFlag = 1;
									vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
									vm.AddressAlterId = vm.contact[i].Contact.AddressID;
									vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
									vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo;
									vm.PostalCodeAlternatif = vm.contact[i].Contact.Address.PostalCode;
									vm.cekPostCode1 = vm.PostalCodeAlternatif;
									//console.info("addresss" + JSON.stringify(vm.contact[i].Contact.Address));
									if (vm.contact[i].Contact.Address.State != null) {
									    loadCountryAlternatif(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
									    vm.selectedStateAlternatif1 = vm.contact[i].Contact.Address.State;
									    if (vm.contact[i].Contact.Address.State.Country.Code == "IDN") {
									        vm.selectedCityAlternatif1 = vm.contact[i].Contact.Address.City;
									        vm.selectedDistrictAlternatif1 = vm.contact[i].Contact.Address.Distric;

									    }
									}

								} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === 2) {
									if (vm.addressAlterFlag == 0) {
										vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
										vm.AddressAlterId = vm.contact[i].Contact.AddressID;
										vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
										vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo
										vm.PostalCodeAlternatif = vm.contact[i].Contact.Address.PostalCode;
										vm.cekPostCode1 = vm.PostalCodeAlternatif;
										loadCountryAlternatif(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
										vm.selectedStateAlternatif1 = vm.contact[i].Contact.Address.State;
										if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
											vm.selectedCityAlternatif1 = vm.contact[i].Contact.Address.City;
											vm.selectedDistrictAlternatif1 = vm.contact[i].Contact.Address.Distric;

										}
									}
								}

							}
						} else {
							$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						//console.info("error:" + JSON.stringify(err));
						//$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.CheckAddress = CheckAddress;
		function CheckAddress(flag) {
			if (flag == true) {
				if (vm.address1 !== vm.cekAddress) vm.IsEdit = true;
			}
			else {
				if (vm.addressInfo !== vm.cekAddress1) vm.IsEditAlter = true;
			}
		}
		vm.CheckPostcode = CheckPostcode;
		function CheckPostcode(flag) {
			if (flag == true) {
				if (vm.postalcode !== vm.cekPostCode) vm.IsEdit = true;
			}
			else {
				if (vm.PostalCodeAlternatif !== vm.cekPostCode1) vm.IsEditAlter = true;
			}
		}

		/*isi combo jenis pemasok*/
		vm.selectedSupplier;
		vm.listSupplier;
		function loadSupplier(data) {
			DataAdministrasiService.getSupplier(function (reply) {
				UIControlService.unloadLoading();
				//console.info("PMS:"+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listSupplier = reply.data.List;
					if (data) {
						for (var i = 0; i < vm.listSupplier.length; i++) {
							if (data.SupplierID === vm.listSupplier[i].RefID) {
								vm.selectedSupplier = vm.listSupplier[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		/*isi combo jenis Vendor*/
		vm.selectedTypeVendor;
		vm.listTypeVendor;
		function loadTypeVendor(data) {
			DataAdministrasiService.getTypeVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTypeVendor = reply.data.List;
					if (data !== undefined) {
						for (var i = 0; i < vm.listTypeVendor.length; i++) {
							if (data.VendorTypeID === vm.listTypeVendor[i].RefID) {
								vm.selectedTypeVendor = vm.listTypeVendor[i];
								changeTypeVendor(vm.administrasi);
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.listBussinesDetailField = []
		vm.changeTypeVendor = changeTypeVendor;
		function changeTypeVendor(data) {
			if (vm.selectedTypeVendor !== undefined) {
				if (vm.selectedTypeVendor.Value === "VENDOR_TYPE_SERVICE") {
					vm.disablePemasok = true;
					vm.listSupplier = {};
					UIControlService.msg_growl("warning", "MESSAGE.SUPPLIER");
					//console.info("dpm:" + JSON.stringify(vm.disablePemasok));
				}
				if (vm.selectedTypeVendor.Value !== "VENDOR_TYPE_SERVICE") {
					vm.disablePemasok = false;
					if (data) {

						loadSupplier(data);
					}
					else {

						loadSupplier();
					}
					//console.info("dpm:" + JSON.stringify(vm.disablePemasok));
				}

				vm.GoodsOrService = vm.selectedTypeVendor.RefID;
				loadBusinessField();
				vm.listBussinesDetailField = [];
				vm.listComodity = [];
			}

		}

		vm.loadVendorCommodity = loadVendorCommodity;
		function loadVendorCommodity(data) {
			DataAdministrasiService.SelectVendorCommodity({ VendorID: data }, function (reply) {
				UIControlService.unloadLoading();
				//console.info("PMS:"+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listBussinesDetailField = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadBusinessField = loadBusinessField;
		vm.selectedBusinessField;
		vm.listBusinessField = [];
		function loadBusinessField() {
			DataAdministrasiService.SelectBusinessField({
				GoodsOrService: vm.GoodsOrService
			},
			   function (response) {
			   	if (response.status === 200) {
			   		vm.listBusinessField = response.data;
			   		//console.info("bfield" + JSON.stringify(vm.listBusinessField));
			   	}
			   	else {
			   		UIControlService.msg_growl("error", "MESSAGE.ERR_GET_BUSSFIELD");
			   		return;
			   	}
			   }, function (err) {
			   	UIControlService.msg_growl("error", "MESSAGE.API");
			   	return;
			   });
		}

		vm.changeBussinesField = changeBussinesField;
		function changeBussinesField() {
			//console.info("bfield" + JSON.stringify(vm.listBusinessField[0].GoodsOrService));
			if (vm.selectedBusinessField === undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD_TYPE");
				return;
			} else {
			    if (vm.selectedBusinessField.Name != "Lain - lain")
			        vm.loadComodity();
			}
		}

		vm.editcontact = editcontact;
		function editcontact(data) {
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/data-administrasi/DetailContact.html',
				controller: 'DetailContactAdministrasiCtrl',
				controllerAs: 'DetailContactAdministrasiCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (data) {
				//console.info(data);
				vm.listPers = [];
				vm.listPers = vm.listPersonal;
				vm.listPersonal = [];
				for (var i = 0; i < vm.listPers.length; i++) {
					if (vm.listPers[i].ContactID === data.ContactID) {
						var aish = {
							VendorContactType: vm.listPers[i].VendorContactType,
							ContactID: vm.listPers[i].ContactID,
							VendorID: vm.listPers[i].VendorID,
							Contact: {
								ContactID: data.ContactID,
								Name: data.Name,
								Email: data.Email,
								Phone: data.Phone
							},
							IsActive: true,
							IsEdit: true
						}
						vm.listPersonal.push(aish);
					}
					else {
						vm.listPersonal.push(vm.listPers[i]);
					}
				}
			});
		}

		function handleRequestError(response) {
			UIControlService.log(response);
			//UIControlService.handleRequestError(response.data, response.status);
			UIControlService.unloadLoading();
		}

		vm.CheckEmail = CheckEmail;
		function CheckEmail() {
		    UIControlService.loadLoading("Check Email . . .");
			if (vm.EmailPers !== '') {
				var data = {
					Keyword: vm.EmailPers
				};
				DataAdministrasiService.checkEmail(data,
                        function (response) {
                        	vm.EmailAvailable = response.data;
                        	if (vm.EmailAvailable) {
                        	    UIControlService.unloadLoading();
                        	    UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');

                        	    vm.NamePers = undefined;
                        	    vm.PhonePers = undefined;
                        	    vm.EmailPers = undefined;
                        	}
                        	else {
                        	    vm.addContactPers();
                        	}
                        }, handleRequestError);
			}
		}


		vm.CheckUsername = CheckUsername;
		function CheckUsername() {
			if (vm.Username !== vm.administrasi.user.Username) {
				DataAdministrasiService.checkUsername({ Keyword: vm.Username },
                        function (response) {
                        	vm.UsernameAvailable = (response.data == false || response.data == 'false');
                        	if (!vm.UsernameAvailable) {
                        		UIControlService.msg_growl('error', 'Username not valid');
                        		vm.Username = vm.administrasi.user.Username;
                        	}
                        }, handleRequestError);
			}
			if (vm.Username === '') {
				vm.Username = vm.administrasi.user.Username;
			}
		}
		vm.addCurrency = addCurrency;
		function addCurrency() {
			vm.flagCurr = false;
			//console.info(vm.Currency);
			for (var i = 0; i < vm.listCurrencies.length; i++) {
				if (vm.Currency.CurrencyID == vm.listCurrencies[i].MstCurrency.CurrencyID && vm.listCurrencies[i].IsActive == true) { vm.flagCurr = true; }
			}
			if (vm.flagCurr == false) {
				vm.listCurrencies.push({
					ID: 0,
					CurrencyID: vm.Currency.CurrencyID,
					VendorID: vm.administrasi.VendorID,
					MstCurrency: vm.Currency,
					IsActive: true
				});
			}
			else {
			    UIControlService.msg_growl("warning", "Mata Uang yang ditambahkan sudah ada");
			    return;

			}
		}


		vm.addContactPers = addContactPers;
		function addContactPers() {
			vm.listPersonal.push({

				Contact: {
					ContactID: 0,
					Name: vm.NamePers,
					Phone: vm.PhonePers,
					Email: vm.EmailPers
				},
				IsActive: true
			});
			vm.NamePers = undefined;
			vm.PhonePers = undefined;
			vm.EmailPers = undefined;

			UIControlService.unloadLoading();
		}

		vm.loadCurrency = loadCurrency;
		function loadCurrency() {
			//console.info("ss");
			UIControlService.loadLoading("LOADERS.LOADING_CURRENCY");
			VendorRegistrationService.getCurrencies(
                function (response) {
                	vm.currencyList = response.data;
                	UIControlService.unloadLoading();
                },
                handleRequestError);
		}

		vm.loadComodity = loadComodity;
		vm.selectedComodity;
		vm.listComodity = [];
		function loadComodity() {
			//console.info("bidang usaha goodsorservice" + JSON.stringify(vm.selectedBusinessField.GoodsOrService));
			if (vm.selectedBusinessField.GoodsOrService === 3091) {
				UIControlService.msg_growl("success", "MESSAGE.COMMODITY");
				vm.listComodity = [];
			} else {
				DataAdministrasiService.SelectComodity({ ID: vm.selectedBusinessField.ID },
				   function (response) {
				   	//console.info("xx"+JSON.stringify(response));
				   	if (response.status === 200 && response.data.length > 0) {
				   		vm.listComodity = response.data;
				   	} else if (response.status === 200 && response.data.length < 1) {
				   		UIControlService.msg_growl("success", "MESSAGE.COMMODITY");
				   		vm.listComodity = [];
				   	} else {
				   		UIControlService.msg_growl("error", "MESSAGE.COMMODITY_LIST");
				   		return;
				   	}
				   }, function (err) {
				   	UIControlService.msg_growl("error", "MESSAGE.API");
				   	return;
				   });
			}
		}


		vm.addDetailBussinesField = addDetailBussinesField;
		function addDetailBussinesField() {
			if (vm.selectedBusinessField === undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD");
				return;
			}

			var comID;
			if (vm.selectedComodity === undefined) {
				//UIControlService.msg_growl("warning", "Komoditas Belum di Pilih");
				//return;
				comID = null;
			} else if (!(vm.selectedComodity === undefined)) {
				comID = vm.selectedComodity.ID;
			}
			countDetailBusinessField = [];
			for (var i = 0; i < vm.listBusinessField.length; i++) {
			    if (vm.listBusinessField[i].Name != "Lain - lain")
			        countDetailBusinessField.push(vm.listBusinessField[i]);
			}
			var countDetailBusinessField = vm.listBussinesDetailField.length;
			var addPermission = false; var sameItem = true;
			//console.info("vendortype:" + JSON.stringify(vm.selectedTypeVendor))
			//console.info("countDetail" + JSON.stringify(countDetailBusinessField));
			var dataDetail = {
				VendorID: vm.administrasi.VendorID,
				CommodityID: comID,
				BusinessFieldID: vm.selectedBusinessField.ID,
				Commodity: vm.selectedComodity,
				BusinessField: vm.selectedBusinessField,
				Remark: vm.RemarkBusinessField
			}

			DataAdministrasiService.businessfieldValidation({
			    BusinessFields: vm.listBussinesDetailField,
			    VendorID: vm.administrasi.VendorID,
			    GoodsOrService: vm.selectedTypeVendor.RefID,
                CommodityParameter: dataDetail
			}, function (reply) {
			    UIControlService.unloadLoading();
			    //console.info("PMS:"+JSON.stringify(reply));
			    if (reply.status === 200) {
			        vm.validasi = reply.data;
			        if (vm.validasi == true) {
			            vm.listBussinesDetailField.push(dataDetail);
			        }
			        else {
			            UIControlService.msg_growl("warning", "Gagal menambahkan bidang usaha atau komoditas.");
			        }
			        //console.info("validasi" + vm.validasi);
			    }
			}, function (err) {
			    UIControlService.msg_growl("error", "MESSAGE.API");
			    UIControlService.unloadLoading();
			});
            
			//if (addPermission === true && sameItem === true) {
			    //console.info(dataDetail);
			    //vm.listBussinesDetailField.push(dataDetail);
			//}
		    //}
			vm.selectedComodity = undefined;

			vm.selectedComodity = undefined;
			//console.info("listbusinessfield:" + JSON.stringify(vm.listBussinesDetailField));

		}

		vm.deleteRow = deleteRow;
		function deleteRow(index) {
			var idx = index - 1;
			var _length = vm.listBussinesDetailField.length; // panjangSemula
			vm.listBussinesDetailField.splice(idx, 1);
		};
		vm.deleteRowCurr = deleteRowCurr;
		function deleteRowCurr(index, data) {
			if (data.ID != undefined) {
				data.IsActive = false;
				vm.listCurrFalse.push(data);
			}
			var idx = index;
			var _length = vm.listCurrencies.length; // panjangSemula
			vm.listCurrencies.splice(idx, 1);
		};
		vm.deleteRowPers = deleteRowPers;
		function deleteRowPers(index, data) {
			//console.info(data);
			if (data.ContactID != 0) {
				data.IsActive = false;
				vm.listPersFalse.push(data);
			}
			var idx = index;
			var _length = vm.listPersonal.length; // panjangSemula
			vm.listPersonal.splice(idx, 1);
		};

		vm.loadSelectedBusinessEntity = loadSelectedBusinessEntity;
		function loadSelectedBusinessEntity(selectedBE) {
			vm.selectedBE = vm.selectedBusinessEntity.Description;
			if (vm.selectedBE === "CV") {
				for (var i = 0; i < vm.listTypeVendor.length; i++) {
					if (vm.listTypeVendor[i].Value === "VENDOR_TYPE_GOODS") {
						vm.listTV = vm.listTypeVendor[i];
						i = vm.listTypeVendor.length;
					}
				}
				vm.listTypeVendor = {};
				vm.listTypeVendor[0] = vm.listTV;
				//loadTypeVendor();
				changeTypeVendor();
				//console.info("lteeee" + JSON.stringify(vm.listTypeVendor));
			}
			else if (vm.selectedBE !== "CV") {
				loadTypeVendor();
			}
		}

		function showSelectedTypeVendor(selectedBE) {
			DataAdministrasiService.getTypeVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTypeVendor = reply.data.List;
					for (var i = 0; i < vm.listTypeVendor.length; i++) {
						if (vm.administrasi.VendorTypeID === vm.listTypeVendor[i].VendorTypeID) {
							vm.selectedTypeVendor = vm.listTypeVendor[i];
							changeTypeVendor();
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadBusinessEntity = loadBusinessEntity;
		vm.selectedBusinessEntity;
		vm.listBusinessEntity = [];
		function loadBusinessEntity(data1) {
			DataAdministrasiService.SelectBusinessEntity(function (response) {
				if (response.status === 200) {
					vm.listBusinessEntity = response.data;
					for (var i = 0; i < vm.listBusinessEntity.length; i++) {
						if (data1.business.BusinessID === vm.listBusinessEntity[i].BusinessID) {
							vm.selectedBusinessEntity = vm.listBusinessEntity[i];
							break;
						}
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCurrencies = loadCurrencies;
		vm.selectedCurrencies = [];
		vm.listCurrencies = [];
		var CurrencyID;
		function loadCurrencies(data) {
			DataAdministrasiService.getCurrencies(function (response) {
				if (response.status === 200) {
					vm.listCurrencies = response.data;

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadAssociation = loadAssociation;
		vm.selectedAssociation;
		vm.listAssociation = [];
		function loadAssociation(data) {
			DataAdministrasiService.getAssociation({
				Offset: 0,
				Limit: 0,
				Keyword: ""
			},
			function (response) {
				if (response.status === 200) {
					vm.listAssociation = response.data.List;
					for (var i = 0; i < vm.listAssociation.length; i++) {
						if (data.AssociationID === vm.listAssociation[i].AssosiationID) {
							vm.selectedAssociation = vm.listAssociation[i];
							break;
						}
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.administrasiDate.StartDate) {
				vm.administrasiDate.StartDate = UIControlService.getStrDate(vm.administrasiDate.StartDate);
			}
		};

		//Supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.administrasiDate.StartDate) {
				vm.administrasiDate.StartDate = new Date(Date.parse(vm.administrasiDate.StartDate));
			}
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.loadRegion = loadRegion;
		vm.selectedRegion;
		vm.listRegion = [];
		function loadRegion(countryID) {
			DataAdministrasiService.SelectRegion({
				CountryID: countryID
			}, function (response) {
				vm.listRegion = response.data;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCountry = loadCountry;
		vm.selectedCountry;
		vm.listCountry = [];
		function loadCountry(data) {
			DataAdministrasiService.SelectCountry(function (response) {
				vm.listCountry = response.data;
				for (var i = 0; i < vm.listCountry.length; i++) {
					if (data.CountryID === vm.listCountry[i].CountryID) {
						vm.selectedCountry = vm.listCountry[i];
						loadState(data);
						break;
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadState = loadState;
		vm.selectedState;
		vm.listState = [];
		function loadState(data) {
			if (!data) {
				data = vm.selectedCountry;
				vm.selectedState = "";
				vm.selectedCity = "";
				vm.selectedDistrict = "";
				vm.selectedState1 = "";
			}
			loadRegion(data.CountryID);

			DataAdministrasiService.SelectState(data.CountryID, function (response) {
				vm.listState = response.data;
				for (var i = 0; i < vm.listState.length; i++) {
					if (vm.selectedState1 !== "" && vm.selectedState1.StateID === vm.listState[i].StateID) {
						vm.selectedState = vm.listState[i];
						if (vm.selectedState.Country.Code === 'IDN') {
							loadCity(vm.selectedState);
							break;
						}
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCity = loadCity;
		vm.selectedCity;
		vm.listCity = [];
		function loadCity(data) {
			if (!data) {

				data = vm.selectedState;
				vm.selectedCity = "";
				vm.selectedCity1 = "";
				vm.selectedDistrict = "";
			}
			DataAdministrasiService.SelectCity(data.StateID, function (response) {
				vm.listCity = response.data;
				for (var i = 0; i < vm.listCity.length; i++) {
					if (vm.selectedCity1 !== "" && vm.selectedCity1.CityID === vm.listCity[i].CityID) {
						vm.selectedCity = vm.listCity[i];
						if (vm.selectedState.Country.Code === 'IDN') {
							loadDistrict(vm.selectedCity);
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadDistrict = loadDistrict;
		vm.selectedDistrict;
		vm.listDistrict = [];
		function loadDistrict(city) {
			if (!city) {
				city = vm.selectedCity;
				vm.selectedDistrict = "";
				vm.selectedDistrict1 = "";

			}
			DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
				vm.listDistrict = response.data;
				for (var i = 0; i < vm.listDistrict.length; i++) {
					if (vm.selectedDistrict1 !== "" && vm.selectedDistrict1.DistrictID === vm.listDistrict[i].DistrictID) {
						vm.selectedDistrict = vm.listDistrict[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadRegionAlternatif = loadRegionAlternatif;
		vm.selectedRegionAlternatif;
		vm.listRegionAlternatif = [];
		function loadRegionAlternatif(countryID) {
			DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
				vm.listRegionAlternatif = response.data;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadRegionAdmin = loadRegionAdmin;
		vm.selectedRegionAdmin;
		vm.listRegionAdmin = [];
		function loadRegionAdmin(countryID) {
			//console.info(countryID);
			DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
				vm.listRegionAdmin = response.data;
				//console.info(vm.listRegionAdmin);
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCountryAlternatif = loadCountryAlternatif;
		vm.selectedCountryAlternatif;
		vm.listCountryAlternatif = [];
		function loadCountryAlternatif(data) {
			DataAdministrasiService.SelectCountry(function (response) {
				vm.listCountryAlternatif = response.data;
				for (var i = 0; i < vm.listCountryAlternatif.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.listCountryAlternatif[i].CountryID) {
							vm.selectedCountryAlternatif = vm.listCountryAlternatif[i];
							loadStateAlternatif(data);
							break;
						}

					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCountryAdmin = loadCountryAdmin;
		vm.selectedCountryAdmin;
		vm.listCountry = [];
		function loadCountryAdmin(data) {
			DataAdministrasiService.SelectCountry(function (response) {
				vm.listCountryAdmin = response.data;
				for (var i = 0; i < vm.listCountryAdmin.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.listCountryAdmin[i].CountryID) {
							vm.selectedCountryAdmin = vm.listCountryAdmin[i];
							loadStateAdmin(data);
							break;
						}

					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}


		vm.loadStateAlternatif = loadStateAlternatif;
		vm.selectedStateAlternatif;
		vm.listStateAlternatif = [];
		function loadStateAlternatif(data) {
			if (!data) {
				data = vm.selectedCountryAlternatif;
				vm.selectedStateAlternatif = "";
				vm.selectedCityAlternatif = "";
				vm.selectedDistrictAlternatif = "";
				vm.selectedStateAlternatif1 = "";
			}
			loadRegionAlternatif(data.CountryID);

			DataAdministrasiService.SelectState(data.CountryID, function (response) {
				vm.listStateAlternatif = response.data;
				for (var i = 0; i < vm.listStateAlternatif.length; i++) {
					if (vm.selectedStateAlternatif1 !== "" && vm.selectedStateAlternatif1.StateID === vm.listStateAlternatif[i].StateID) {
						vm.selectedStateAlternatif = vm.listStateAlternatif[i];
						if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
							loadCityAlternatif(vm.selectedStateAlternatif);
							break;
						}
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadStateAdmin = loadStateAdmin;
		vm.selectedStateAdmin;
		vm.listStateAdmin = [];
		function loadStateAdmin(data) {
			if (!data) {
				data = vm.selectedCountryAdmin;
				vm.selectedStateAdmin = "";
				vm.selectedCityAdmin = "";
				vm.selectedDistrictAdmin = "";
				vm.selectedStateAdmin1 = "";
			}
			loadRegionAdmin(data.CountryID);

			DataAdministrasiService.SelectState(data.CountryID, function (response) {
				vm.listStateAdmin = response.data;
				for (var i = 0; i < vm.listStateAdmin.length; i++) {
					if (data !== undefined) {
						if (data.StateID === vm.listStateAdmin[i].StateID) {
							vm.selectedStateAdmin = vm.listStateAdmin[i];
							if (vm.selectedStateAdmin.Country.Code === 'IDN') {
								loadCityAdmin(vm.selectedStateAdmin);
								break;
							}
						}
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}
		vm.loadCityAdmin = loadCityAdmin;
		vm.selectedCityAdmin;
		vm.listCityAdmin = [];
		function loadCityAdmin(data) {
			if (!data) {

				data = vm.selectedStateAdmin;
				vm.selectedCityAdmin = "";
				vm.selectedCityAdmin1 = "";
				vm.selectedDistrictAdmin = "";
			}
			DataAdministrasiService.SelectCity(data.StateID, function (response) {
				vm.listCityAdmin = response.data;
				for (var i = 0; i < vm.listCityAdmin.length; i++) {
					if (data !== undefined) {
						if (vm.CityCompany.CityID === vm.listCityAdmin[i].CityID) {
							vm.selectedCityAdmin = vm.listCityAdmin[i];
							if (vm.selectedStateAdmin.Country.Code === 'IDN') {
								loadDistrictAdmin(vm.selectedCityAdmin);
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}



		vm.loadCityAlternatif = loadCityAlternatif;
		vm.selectedCityAlternatif;
		vm.listCityAlternatif = [];
		function loadCityAlternatif(data) {
			if (!data) {

				data = vm.selectedStateAlternatif;
				vm.selectedCityAlternatif = "";
				vm.selectedCityAlternatif1 = "";
				vm.selectedDistrictAlternatif = "";
			}
			DataAdministrasiService.SelectCity(data.StateID, function (response) {
				vm.listCityAlternatif = response.data;
				for (var i = 0; i < vm.listCityAlternatif.length; i++) {
					if (vm.selectedCityAlternatif1 !== "" && vm.selectedCityAlternatif1.CityID === vm.listCityAlternatif[i].CityID) {
						vm.selectedCityAlternatif = vm.listCityAlternatif[i];
						if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
							loadDistrictAlternatif(vm.selectedCityAlternatif);
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadDistrictAdmin = loadDistrictAdmin;
		vm.selectedDistrictAdmin;
		vm.listDistrictAdmin = [];
		function loadDistrictAdmin(city) {
			if (!city) {
				city = vm.selectedCityAdmin;
				vm.selectedDistrictAdmin = "";
				vm.selectedDistrictAdmin1 = "";

			}
			DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
				vm.listDistrictAdmin = response.data;
				for (var i = 0; i < vm.listDistrictAdmin.length; i++) {
					if (city !== undefined) {
						if (vm.DistrictCompany.DistrictID === vm.listDistrictAdmin[i].DistrictID) {
							vm.selectedDistrictAdmin = vm.listDistrictAdmin[i];
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}




		vm.loadDistrictAlternatif = loadDistrictAlternatif;
		vm.selectedDistrictAlternatif;
		vm.listDistrictAlternatif = [];
		function loadDistrictAlternatif(city) {
			if (!city) {
				city = vm.selectedCityAlternatif;
				vm.selectedDistrictAlternatif = "";
				vm.selectedDistrictAlternatif1 = "";

			}
			DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
				vm.listDistrictAlternatif = response.data;
				for (var i = 0; i < vm.listDistrictAlternatif.length; i++) {
					if (vm.selectedDistrictAlternatif1 !== "" && vm.selectedDistrictAlternatif1.DistrictID === vm.listDistrictAlternatif[i].DistrictID) {
						vm.selectedDistrictAlternatif = vm.listDistrictAlternatif[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}


		vm.uploadFile = uploadFile;
		function uploadFile() {
		    if (vm.fileUpload === undefined) {
		        vm.PKPUrl = vm.administrasi.PKPUrl;
		        if (vm.fileUploadNPWP === undefined) {
		            vm.NpwpUrl = vm.administrasi.NpwpUrl;
		            savedata();
		        }
		        else {
		            if (vm.fileUploadNPWP !== null) {
		                upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		            }
		            else {
		                vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                savedata();
		            }
		        }
		    }
		    else {
		        if (vm.fileUpload !== null) {
		            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
		        }
		        else {
		            vm.PKPUrl = vm.administrasi.PKPUrl;
		            if (vm.fileUploadNPWP === undefined) {
		                vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                savedata();
		            }
		            else {
		                if (vm.fileUploadNPWP !== null) {
		                    upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		                }
		                else {
		                    vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                    savedata();
		                }
		            }
		        }
		    }



			//if (vm.fileUpload === undefined) {
			//    vm.PKPUrl = vm.administrasi.PKPUrl;
			//    if (vm.fileUploadNPWP === undefined) {
			//        vm.NpwpUrl = vm.administrasi.NpwpUrl;
			//        savedata();
			//    }
			//    else {
			//        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
			//    }
			//} else {
			//	//console.info("masuk");
			//    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			//}
		}

		vm.validateFileType = validateFileType;
		function validateFileType(administrasi, flag, file, allowedFileTypes) {
		    if (flag == true) {
		        if (!file && administrasi.PKPUrl === "") {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		            return false;
		        }
		    }
		    else {
		        if (!file && administrasi.NpwpUrl === "") {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		            return false;
		        }
		    }
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
			//console.info("masuk"+JSON.stringify(vm.administrasi.PKPUrl));
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			//if (vm.administrasi.PKPUrl === null) {
			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileSPPKP(vm.administrasi.VendorID, file, size, filters, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.pathFile = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;
					if (vm.flag == 0) {
						vm.size = Math.floor(s)
					}
					if (vm.flag == 1) {
						vm.size = Math.floor(s / (1024));
					}
					vm.PKPUrl = vm.pathFile;
					if (vm.fileUploadNPWP === undefined) {
					    vm.NpwpUrl = vm.administrasi.NpwpUrl;
					    savedata();
					}
					else {
					    if (vm.fileUploadNPWP !== null) {
					        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
					    }
					    else {
					        vm.NpwpUrl = vm.administrasi.NpwpUrl;
					        savedata();
					    }
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
			    if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
                    UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
			        UIControlService.unloadLoading();
			    }
			});
			//} end if
		}

		vm.upload1 = upload1;
		function upload1(file, config, filters, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    //console.info(config);
		    UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
		    UploaderService.uploadRegistration(file, vm.administrasi.Npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            var url = response.data.Url;
		            vm.pathFile1 = url;
		            vm.name1 = response.data.FileName;
		            var s = response.data.FileLength;
		            if (vm.flag == 0) {
		                vm.size = Math.floor(s)
		            }
		            if (vm.flag == 1) {
		                vm.size = Math.floor(s / (1024));
		            }
		            vm.NpwpUrl = vm.pathFile1;
		            savedata();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (response) {
		        if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
		            UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
		            UIControlService.unloadLoading();
		        }
		    });
		}

		function savedata() {
		    vm.cek = 0;
		    if (vm.selectedCountryAdmin == undefined) {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY");
		        return;
		    }
		    else if (vm.selectedStateAdmin == undefined) {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_STATE");
		        return;
		    }
		    else if (vm.selectedStateAdmin.Country.Code === 'IDN') {
		        if (vm.selectedCityAdmin == undefined) {
		            vm.cek = 1;
		            UIControlService.msg_growl("error", "MESSAGE.ERR_CITY");
		            return;
		        }
		        else if (vm.selectedDistrictAdmin == undefined) {
		            vm.cek = 1;
		            UIControlService.msg_growl("error", "MESSAGE.ERR_DISTRICT");
		            return;
		        }
		    }
		    if (vm.administrasi.VendorName == undefined || vm.administrasi.VendorName == "") {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_VENDORNAME");
		        return;
		    }
		    else if (vm.Username == undefined || vm.Username == "") {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_USERNAME");
		        return;
		    } 
		    else if (vm.administrasiDate.StartDate == undefined) {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_STARTDATE");
		        return;
		    }
		    else if (vm.phoneCode == undefined) {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_PHONECODE");
		        return;
		    }
		    else if (vm.phoneCode !== undefined) {
		        if (vm.Phone == undefined || vm.Phone == "") {
		            vm.cek = 1;
		            UIControlService.msg_growl("error", "MESSAGE.ERR_PHONE");
		            return;
		        }
		        else if (vm.Email == undefined || vm.Email == "") {
		            vm.cek = 1;
		            UIControlService.msg_growl("error", "MESSAGE.ERR_EMAIL");
		            return;
		        }
		    }
		    if (vm.selectedStateAdmin.Country.Code === 'IDN') {
		        if (vm.PKPNumber == undefined) {
		            vm.cek = 1;
		            UIControlService.msg_growl("error", "MESSAGE.ERR_PKPNUMBER");
		            return;
		        }
		        else if (vm.administrasi.PKPUrl == undefined && vm.fileUpload == undefined) {
		            vm.cek = 1;
		            UIControlService.msg_growl("error", "MESSAGE.ERR_PKPUPLOAD");
		            return;
		        }
		    }
		    if (vm.selectedTypeVendor === undefined) {
		        vm.cek = 1;
			    UIControlService.msg_growl("error", "MESSAGE.ERR_VENDORTYPE");
			    return;
			}
			else if (vm.selectedTypeVendor !== undefined) {
				vm.VendorTypeID = vm.selectedTypeVendor.RefID;
				if (vm.selectedTypeVendor.Name === "VENDOR_TYPE_SERVICE") {
					vm.SupplierID = null;
				}
				else {
				    if (vm.selectedSupplier === undefined) {
				        vm.cek = 1;
				        UIControlService.msg_growl("error", "MESSAGE.ERR_SUPPLIER"); 
				        return;
					}
					else vm.SupplierID = vm.selectedSupplier.RefID;
				}
			}
		    if (vm.listCurrencies.length === 0) {
		        vm.cek = 1;
			    UIControlService.msg_growl("error", "MESSAGE.ERR_CURR");
			    return;
			}
		    else if (vm.listBussinesDetailField.length == 0) {
		        vm.cek = 1;
				UIControlService.msg_growl("error", "MESSAGE.ERR_BUSINESSFIELD");
				return;
			}
		    else if (vm.listPersonal.length === 0) {
		        vm.cek = 1;
			    UIControlService.msg_growl("error", "MESSAGE.ERR_CP");
				return;
		    }
		    if (vm.listBussinesDetailField.length > 6) {
		        vm.cek = 1;
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LIMITBUSINESSFIELD");
		        return;
		    }
		    if (vm.selectedCountry == undefined) {
		        vm.cek = 1;
			    UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY_ADDR");
			    return;
			}
		    else if (vm.selectedState == undefined) {
		        vm.cek = 1;
			    UIControlService.msg_growl("error", "MESSAGE.ERR_STATE_ADDR");
			    return;
			}
			else if (vm.selectedState.Country.Code === 'IDN') {
			    if (vm.selectedCity == undefined) {
			        vm.cek = 1;
			        UIControlService.msg_growl("error", "MESSAGE.ERR_CITY_ADDR");
			        return;
			    }
			    else if (vm.selectedDistrict == undefined) {
			        vm.cek = 1;
			        UIControlService.msg_growl("error", "MESSAGE.ERR_DISTRICT_ADDR");
			        return;
			    }
			}
			if(vm.cek === 0) {
			    addtolist(vm.VendorTypeID, vm.SupplierID);
			}
		}

		vm.addtolist = addtolist;
		function addtolist(data1, data2) {
		    vm.vendor = {};
		    vm.listcontact = [];
			//console.info(vm.Phone);
			if (!vm.selectedCityAdmin && !vm.selectedDistrictAdmin) {
				var addressComp = {
					AddressID: vm.addressIdComp,
					StateID: vm.selectedStateAdmin.StateID
				}
			}
			else {
				var addressComp = {
					AddressID: vm.addressIdComp,
					StateID: vm.selectedStateAdmin.StateID,
					CityID: vm.selectedCityAdmin.CityID,
					DistrictID: vm.selectedDistrictAdmin.DistrictID
				}
			}
			var contactdt = {
				VendorContactType: vm.VendorContactTypeCompany,
				Contact: {
					ContactID: vm.ContactID,
					Email: vm.Email,
					Phone: '(' + vm.phoneCode.PhonePrefix + ') ' + vm.Phone,
					Website: vm.Website,
					Fax: vm.fax,
					Address: addressComp
				}
			}
			vm.listcontact.push(contactdt);
			//console.info(vm.listcontact);

			if (vm.selectedCity == undefined && vm.selectedDistrict == undefined) {
				
				vm.address = {
					AddressID: vm.AddressId,
					AddressInfo: vm.address1,
					PostalCode: vm.postalcode,
					StateID: vm.selectedState.StateID
				}
			} else {
				
				vm.address = {
					AddressID: vm.AddressId,
					AddressInfo: vm.address1,
					PostalCode: vm.postalcode,
					StateID: vm.selectedState.StateID,
					CityID: vm.selectedCity.CityID,
					DistrictID: vm.selectedDistrict.DistrictID
				}
			}
			var contact = {
				ContactID: vm.ContactOfficeId,
				Address: vm.address
			}
			var contactdt = {
				VendorContactType: vm.VendorContactType,
				Contact: contact,
				IsEdit: vm.IsEdit
			}
			vm.listcontact.push(contactdt);
            /*
			if (vm.addressInfo != '' && vm.selectedCountryAlternatif == undefined) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_LIMITBUSINESSFIELD");
			    return;
			}*/
			if (vm.selectedCountryAlternatif != undefined && vm.addressInfo != ' ') {
				if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {

					vm.address2 = {
						AddressID: vm.AddressAlterId,
						AddressInfo: vm.addressInfo,
						PostalCode: vm.PostalCodeAlternatif,
						StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null

					}
				} else {
					
					vm.address2 = {
						AddressID: vm.AddressAlterId,
						AddressInfo: vm.addressInfo,
						PostalCode: vm.PostalCodeAlternatif,
						StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null,
						CityID: vm.selectedCityAlternatif.CityID,
						DistrictID: vm.selectedDistrictAlternatif.DistrictID
					}
				}
				if (vm.AddressAlterId == 0) {
					var contact = {
						Name: vm.Name,
						ModifiedBy: vm.administrasi.user.Username,
						Address: vm.address2
					}
				} else {
					var contact = {
						ContactID: vm.ContactOfficeAlterId,
						ModifiedBy: vm.administrasi.user.Username,
						Address: vm.address2
					}
				}
				if (vm.VendorContactTypeAlter == undefined) {
					var contactdt = {
						VendorContactType: vm.VendorContactType,
						Contact: contact,
						IsPrimary: 2,
						IsEdit: vm.IsEditAlter
					}
				} else {
					var contactdt = {
						VendorContactType: vm.VendorContactTypeAlter,
						Contact: contact,
						IsPrimary: 2,
						IsEdit: vm.IsEditAlter
					}
				}

				vm.listcontact.push(contactdt);
				vm.listcontact.push(vm.contactpersonal);
			}
			if (!vm.VendorContactTypePers) {
			    vm.VendorContactTypePers = {
			        Name: 'VENDOR_CONTACT_TYPE_PERSONAL'
			    };
			}
			for (var i = 0; i < vm.listPersonal.length; i++) {
				var contactdt = {
					VendorContactType: vm.VendorContactTypePers,
					Contact: {
						ContactID: vm.listPersonal[i].Contact.ContactID,
						Email: vm.listPersonal[i].Contact.Email,
						Phone: vm.listPersonal[i].Contact.Phone,
						Name: vm.listPersonal[i].Contact.Name
					},
					IsActive: true,
					IsEdit: vm.listPersonal[i].IsEdit
				}
				vm.listcontact.push(contactdt);
			}
			for (var i = 0; i < vm.listPersFalse.length; i++) {
				var contactdt = {
					VendorContactType: vm.VendorContactTypePers,
					Contact: {
						ContactID: vm.listPersFalse[i].Contact.ContactID,
						Email: vm.listPersFalse[i].Contact.Email,
						Phone: vm.listPersFalse[i].Contact.Phone,
						Name: vm.listPersFalse[i].Contact.Name
					},
					IsActive: false
				}
				vm.listcontact.push(contactdt);
			}
			for (var i = 0; i < vm.listCurrFalse.length; i++) {
				vm.listCurrencies.push(vm.listCurrFalse[i]);
			}

			var asoc;
			if (vm.selectedAssociation === undefined) {
				asoc = null;
			} else {
				asoc = vm.selectedAssociation.AssosiationID
			}

			//console.info("coba" + JSON.stringify(vm.selectedSupplier));
			if (vm.selectedSupplier === null) {
				vm.selectedSupplier = {
					RefID: null
				};
			}
			if (vm.CountryCode !== 'IDN') {
				vm.selectedBusinessEntity = {
					BusinessID: null
				};
			}
			vm.vendor = {
			    SupplierID: data2,
                NpwpUrl: vm.NpwpUrl,
				VendorName: vm.administrasi.VendorName,
				VendorID: vm.administrasi.VendorID,
				user: {
					Username: vm.Username
				},
				FoundedDate: UIControlService.getStrDate(vm.administrasiDate.StartDate),
				BusinessID: vm.selectedBusinessEntity.BusinessID,
				PKPNumber: vm.PKPNumber,
				PKPUrl: vm.PKPUrl,
				ModifiedBy: vm.administrasi.user.Username,
				AssociationID: asoc,
				Contacts: vm.listcontact,
				commodity: vm.listBussinesDetailField,
				VendorTypeID: data1,
				Currency: vm.listCurrencies

			}
            
			//console.info(JSON.stringify(vm.vendor));
			DataAdministrasiService.insert(vm.vendor, function (reply) {
				//console.info("reply" + JSON.stringify(reply))
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
					window.location.reload();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
            


		}


	}
})();

