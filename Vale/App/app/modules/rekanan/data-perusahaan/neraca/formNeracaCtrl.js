﻿(function () {
	'use strict';

	angular.module("app")
            .controller("formNeracaCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'BalanceVendorService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, BalanceVendorService, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isAdd = item.act;
		vm.item = item.item;
		vm.VendorID = item.VendorID;
		vm.action = "";
		vm.fileUpload;
		vm.Nominal;
		vm.Amount;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.DocUrl;
		vm.isApprovedCR = false;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('vendor-balance');
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.BALANCE", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			if (vm.isAdd === true) {
				vm.action = "Tambah";

			} else {
				vm.action = "Ubah ";
				vm.Amount = vm.item.Amount;
				vm.Nominal = vm.item.Nominal;
				vm.fileUpload = vm.item.DocUrl;
			}
			loadAsset();
			loadUnit();
			loadCheckCR();

		}
		vm.changeSubCOA = changeSubCOA;
		function changeSubCOA(param) {
			BalanceVendorService.getUnit(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listUnit = reply.data.List;
				    vm.selectedUnit = undefined;
					if (param.Name === 'SUB_CASH_TYPE2') {
						vm.selectedUnit = vm.listUnit[0];
					} else if (param.Name === 'SUB_CASH_TYPE3') {
						vm.selectedUnit = vm.listUnit[2];
					} else if (param === 0) {
						vm.selectedUnit = vm.listUnit[1];
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function loadCheckCR() {
			UIControlService.loadLoading("Silahkan Tunggu");
			BalanceVendorService.getCRbyVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					if (!(reply.data === null) && reply.data.ApproveBy === 1) {
						vm.isApprovedCR = true;
					} else {
						vm.isSentCR = false;
					}
				}

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
		vm.selectedAsset;
		vm.listAsset;
		function loadAsset() {
			BalanceVendorService.getAsset(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listAsset = reply.data.List;
				    vm.selectedAsset = undefined;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listAsset.length; i++) {
							if (item.item.Wealth.RefID === vm.listAsset[i].RefID) {
								vm.selectedAsset = vm.listAsset[i];
								loadCOA(vm.selectedAsset);
								break;
							}
						}
					}

				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadCOA = loadCOA;
		vm.selectedCOA;
		vm.disableLU;
		vm.listCOA;
		function loadCOA(data) {
			vm.param = "";
			if (data.RefID === 3097) {
				vm.param = "COA_TYPE_ASSET"
			} else if (data.RefID === 3099) {
				vm.param = "COA_TYPE_DEBTH"
			}

			BalanceVendorService.getCOA({
				Keyword: vm.param
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listCOA = reply.data.List;
				    vm.selectedCOA = undefined;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listCOA.length; i++) {
							if (item.item.COA.RefID === vm.listCOA[i].RefID) {
								vm.selectedCOA = vm.listCOA[i];
								loadSubCOA(vm.selectedCOA);
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		vm.loadSubCOA = loadSubCOA;
		vm.selectedSubCOA;
		vm.listSubCOA;
		function loadSubCOA(data) {
			vm.param = "";
			if (data.RefID === 3100) {
				vm.param = "SUB_COA_CASH"
			} else if (data.RefID === 3101) {
				vm.param = "SUB_COA_DEBTHSTOCK"
			}
			BalanceVendorService.getSubCOA({
				Keyword: vm.param
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listSubCOA = reply.data.List;
				    vm.selectedSubCOA = undefined;
					var param = vm.listSubCOA.length;
					if (param === 0) {
						changeSubCOA(param);
					}
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listSubCOA.length; i++) {
							if (item.item.SubCOA.RefID === vm.listSubCOA[i].RefID) {
								vm.selectedSubCOA = vm.listSubCOA[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedUnit;
		vm.listUnit;
		function loadUnit() {
			BalanceVendorService.getUnit(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listUnit = reply.data.List;
				    vm.selectedUnit = undefined;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listUnit.length; i++) {
							if (item.item.Unit.RefID === vm.listUnit[i].RefID) {
								vm.selectedUnit = vm.listUnit[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selected = selected;
		function selected() {
			console.info("respon1:" + JSON.stringify(vm.selectedDocumentType));
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.uploadFile = uploadFile;
		function uploadFile() {
		    if (vm.selectedAsset === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_WEALTH_TYPE"); return;
		    }
		    else if (vm.selectedCOA === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_ACC_TYPE"); return;
		    }
		    else if (vm.selectedSubCOA === undefined && vm.listSubCOA.length !== 0) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_SUBACC_TYPE"); return;
		    }
		    else if (vm.selectedUnit === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_UNIT"); return;
		    }
		    else if (vm.Amount === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_AMOUNT"); return;
		    }
		    else if (vm.Nominal === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_NOMINAL"); return;
		    }
		    else if ((vm.fileUpload == undefined && vm.isAdd == true) || (vm.fileUpload == undefined && vm.isAdd == false && item.item.DocUrl == null)) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_DOC"); return;
		    }
		    else {
		        if (vm.fileUpload === undefined && vm.isAdd === false) {
		            vm.DocUrl = item.item.DocUrl;
		            addToSave();
		        }
		        else if (vm.fileUpload !== undefined) {
		            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
		            }
		        }
		    }




			
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
		    if (vm.listSubCOA.length !== 0) vm.prefix = vm.selectedSubCOA.RefID + '_' + vm.selectedUnit.RefID;
		    else vm.prefix = vm.selectedCOA.RefID + '_' + vm.selectedUnit.RefID;
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}


			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileBalance(vm.VendorID,vm.prefix,file, size, filters, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.DocUrl = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;
					if (vm.flag == 0) {

						vm.size = Math.floor(s)
					}

					if (vm.flag == 1) {
						vm.size = Math.floor(s / (1024));
					}
					addToSave();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.addToSave = addToSave;
		vm.vendor = {};
		function addToSave() {
			if (vm.isAdd === true) {
				if (vm.listSubCOA.length === 0) {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						Amount: vm.Amount,
						UnitType: vm.selectedUnit.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID
					};
				} else {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						Amount: vm.Amount,
						UnitType: vm.selectedUnit.RefID,
						SubCOAType: vm.selectedSubCOA.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID
					};
				}

			} else if (vm.isAdd === false) {
				if (vm.listSubCOA.length === 0) {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						BalanceID: item.item.BalanceID,
						DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						Amount: vm.Amount,
						UnitType: vm.selectedUnit.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID
					};
				} else {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						BalanceID: item.item.BalanceID,
						DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						Amount: vm.Amount,
						UnitType: vm.selectedUnit.RefID,
						SubCOAType: vm.selectedSubCOA.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID
					};
				}
			}
			if (vm.isAdd === true) {
				BalanceVendorService.insert(vm.vendor,
                    function (reply) {
                    	UIControlService.unloadLoadingModal();
                    	if (reply.status === 200) {
                    		UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
                    		$uibModalInstance.close();

                    	} else {
                    		UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
                    		return;
                    	}
                    },
                    function (err) {
                    	UIControlService.msg_growl("error", "MESSAGE.API");
                    	UIControlService.unloadLoadingModal();
                    }
                );
			} else if (vm.isAdd === false) {
				BalanceVendorService.update(vm.vendor,
                    function (reply) {
                    	UIControlService.unloadLoadingModal();
                    	if (reply.status === 200) {
                    		UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
                    		$uibModalInstance.close();

                    	}
                    	else {
                    		UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
                    		return;
                    	}
                    }, function (err) {
                    	UIControlService.msg_growl("error", "MESSAGE.API");
                    	UIControlService.unloadLoadingModal();
                    }
                );
			}
		}

	}
})();