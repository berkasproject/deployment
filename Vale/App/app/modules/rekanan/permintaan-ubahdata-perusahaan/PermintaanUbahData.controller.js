﻿(function () {
	'use strict';

	angular.module("app").controller("PermintaanPerubahanDataCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'PermintaanUbahDataService', 'DataAdministrasiService', 'UIControlService', 'SocketService', '$filter'];
	function ctrl($translatePartialLoader, PUbahDataService, DataAdministrasiService, UIControlService, SocketService, $filter) {
		var vm = this;
		vm.init = init;
		vm.listCR = [];
		vm.Remark = '';
		vm.isVerified;
		vm.IsApprovedBy;
		vm.IsLocal = false;
		vm.IsOverseas = false; // 外国会社

		function init() {
		    $translatePartialLoader.addPart('permintaan-ubah-data');
		    cekPrakualifikasiVendor();
			loadDataVendor(1);
			//loadListOpsiChangeRequest();
			//loadCheckCR();
			chekcIsVerified();
		}

		vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
		function cekPrakualifikasiVendor() {
		   PUbahDataService.cekPrakualifikasiVendor(function (reply) {
		        UIControlService.loadLoadingModal();
		        if (reply.status === 200) {
		            vm.pqWarning = reply.data;
		            console.info("isregistered" + vm.pqWarning);
		            UIControlService.unloadLoadingModal();
		        } else {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		    });
		}

		function chekcIsVerified() {
			PUbahDataService.isVerified(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					console.info("ver>>" + JSON.stringify(reply));
					var data = reply.data;
					if (!(data.Isverified === null)) {
						vm.isVerified = true;
					} else {
						vm.isVerified = false;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		vm.loadDataVendor = loadDataVendor;
		function loadDataVendor(current) {
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			DataAdministrasiService.select({
				Offset: offset,
				Limit: 10,
				Keyword: vm.Username
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = {};
					vm.administrasi = reply.data[0];
					DataAdministrasiService.selectcontact({
						VendorID: vm.administrasi.VendorID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							vm.contact = reply.data;
							for (var i = 0; i < vm.contact.length; i++) {
								if (vm.contact[i].VendorContactType.Value === 'VENDOR_CONTACT_TYPE_COMPANY') {
									if (vm.contact[i].Contact.Address.DistrictID === 5101 || vm.contact[i].Contact.Address.DistrictID === 5104 || vm.contact[i].Contact.Address.DistrictID === 5104 || vm.contact[i].Contact.Address.DistrictID === 5108) {
										vm.IsLocal = true;
										vm.IsOverseas = false;
									} else if (vm.contact[0].Contact.Address.State.Country.CountryID != 360) {
										vm.IsLocal = false;
										vm.IsOverseas = true;
									}
								}
							}
							console.info("isLocal>" + vm.IsLocal);
							loadListOpsiChangeRequest();
							//loadCheckCR()
						} else {
							$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						console.info("error:" + JSON.stringify(err));
						//$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}


		//function loadCheckCR() {
		//	UIControlService.loadLoading("Silahkan Tunggu");

		//}

		function loadListOpsiChangeRequest() {
			//alert("load");
			vm.listCRAll = [];
			PUbahDataService.getOpsiChangeReq(function (reply) {
				UIControlService.unloadLoading();
				var data = reply.data.List;
				for (var i = 0; i < data.length; i++) {
					var newmap = {
						ChangeRequestRefID: data[i].RefID,
						CRName: data[i].Value,
						Description: "",
						IsApproved: false,
						IsRequested: false,
						IsTouched: false
					}
					vm.listCRAll.push(newmap);
					//console.info("listCRall" + JSON.stringify(vm.listCRAll));
					vm.listCR = [];
					vm.listCR[0] = [];
					for (var j = 0; j < vm.listCRAll.length; j++) {
						if (vm.IsLocal === true && vm.IsOverseas === false) {
							if (vm.listCRAll[j].CRName !== "OC_VENDORBUSINESSFIELD") {
								vm.listCR[0].push(vm.listCRAll[j]);
							}
						}
						else if (vm.IsLocal === false && vm.IsOverseas === true) {
							if (vm.listCRAll[j].CRName !== "OC_VENDOREQUIPMENT" && vm.listCRAll[j].CRName !== "OC_VENDORLICENSI" && vm.listCRAll[j].CRName !== "OC_VENDORSTOCK") {
								vm.listCR[0].push(vm.listCRAll[j]);
							}
						}
						else if (vm.IsLocal === false && vm.IsOverseas == false) {
							vm.listCR[0].push(vm.listCRAll[j]);
						}
					}
					/*
					else if (vm.IsLocal === false) {
						vm.listCR.push(vm.listCRAll);
					}*/
					console.info("isLoc" + vm.IsLocal);
					console.info("listCRfinal" + JSON.stringify(vm.listCR));
				}
				PUbahDataService.getCRbyVendor(vm.listCR[0], function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.listCR[0] = reply.data;
						//for (var i = 0; i < reply.data.length; i++) {
						//	for (var j = 0; j < vm.listCR[0].length; j++) {
						//		if (reply.data[i].ChangeRequestDataDetails[0].ChangeRequestRefID === vm.listCR[0][j].ChangeRequestRefID) {
						//			//vm.listCR.splice(j, 1);
						//			vm.listCR[0][j].IsRequested = true;
						//			vm.listCR[0][j].Description = reply.data[i].ChangeRequestDataDetails[0].Description;
						//			vm.listCR[0][j].IsTouched = reply.data[i].IsTouched;
						//			break;
						//		}
						//	}
						//}

						vm.isSentCR = false;
						console.info("listCR:" + JSON.stringify(vm.listCR[0]));
						//console.info("isSentCR:" + JSON.stringify(vm.isSentCR));
						//console.info("CR:" + JSON.stringify(reply));
						//if (!(reply.data === null) && reply.data.ApproveBy === null) {
						//	vm.isSentCR = true;
						//	vm.IsApprovedBy = reply.data.ApproveBy;
						//}
						//if (!(reply.data === null) && reply.data.ApproveBy === 1) {
						//	vm.isSentCR = true;
						//	vm.IsApprovedBy = reply.data.ApproveBy;
						//} else {
						//	vm.isSentCR = false;
					    //}
						if (vm.pqWarning != '') {
						    for (var i = 0; i <= vm.listCR[0].length-1; i++) {
						        if (vm.listCR[0][i].CRName == 'OC_VENDORBUSINESSFIELD' ||
                                    vm.listCR[0][i].CRName == 'OC_VENDORSTOCK' ||
                                    vm.listCR[0][i].CRName == 'OC_VENDORBALANCE' ||
                                    vm.listCR[0][i].CRName == 'OC_VENDOREQUIPMENT' ||
                                    vm.listCR[0][i].CRName == 'OC_VENDOREXPERIENCE') {
						            vm.listCR[0][i].IsRequested = true;
						        }
						    }
						}
					}

				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.commit = commit;
		function commit(refId) {
			bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('CONFIRM.CHANGES') + '<h3>', function (reply) {
				if (reply) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					PUbahDataService.commit({ OpsiCode: refId }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUC_SEND");
							SocketService.emit("SubmitUbahData");
							init();
						} else {
							UIControlService.msg_growl("error", "MESSAGE.ERR_SEND");
							return;
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.sendData = sendData;
		function sendData() {
			//mapping data
			UIControlService.loadLoading("MESSAGE.LOADING");
			var newlist = [];
			for (var i = 0; i < vm.listCR[0].length; i++) {
				if (vm.listCR[0][i].IsApproved === true) {
					var newmap = {
						ChangeRequestRefID: vm.listCR[0][i].ChangeRequestRefID,
						Description: vm.listCR[0][i].Description,
						IsApproved: 0
					}
					newlist.push(newmap);
				}
			}
			//console.info(JSON.stringify(newlist));
			var datasimpan = {
				Remark: vm.Remark,
				ChangeRequestDataDetails: newlist
			}
			PUbahDataService.insertChangeReq(datasimpan, function (reply) {
				if (reply.status === 200) {
					init();
					SocketService.emit("PermintaanUbahData");
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", "MESSAGE.SUC_SEND");
					window.location.reload();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SEND");
					UIControlService.unloadLoading();
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
	}
})();