﻿(function () {
    'use strict';

    angular.module("app")
    .controller("DetailSubLineCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams, $uibModalInstance) {
        var vm = this;
        vm.detail = item.item;
        vm.overtime = item.overtime;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.textSearch = '';
        vm.listPengumuman = [];
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.TenderRefID = item.TenderRefID;
        vm.ProcPackType = item.ProcPackType;
        vm.EmployeeID = vm.detail.EmployeeID;
        vm.init = init;
        function init() {
            console.info(vm.detail);
            $translatePartialLoader.addPart("negosiasi");
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            jLoad(1);
        }
        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.list = [];
            NegosiasiService.selectByVendor({
                Keyword: vm.EmployeeID,
                Status:item.TenderRefID,
                FilterType: item.ProcPackType
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data;
                    console.info("data subline:" + JSON.stringify(vm.list));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Penilai" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.batal = batal;
        function batal() {
            //$uibModalInstance.dismiss('cancel');
            $uibModalInstance.close();
        };

        vm.per_item = per_item;
        function per_item(i) {
            vm.list = [];
            vm.data = {
                ID: vm.detail.childern[i].ID,
                IsOpen: vm.detail.childern[i].IsOpen,
                UnitVendorPrice: vm.detail.childern[i].UnitVendorPrice,
                NegoId: vm.detail.childern[i].NegoId,
                Procentage: vm.detail.childern[i].Procentage,
                quantity: vm.detail.childern[i].quantity
            }
            NegosiasiService.updateitem(vm.data,
                                       function (reply) {
                                           UIControlService.loadLoading("MESSAGE.LOADING");
                                           if (reply.status === 200) {
                                               UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                                               NegosiasiService.CekDetail({
                                                   ID: vm.detail.childern[i].ID
                                               }, function (reply) {
                                                   UIControlService.unloadLoading();
                                                   if (reply.status === 200) {
                                                       var data = reply.data;
                                                       vm.detail.childern[i].UnitNegotiationPrice = data.UnitNegotiationPrice;
                                                       vm.detail.childern[i].TotalPriceNego = data.TotalPriceNego;
                                                   } else {
                                                       $.growl.error({ message: "Gagal mendapatkan data Penilai" });
                                                       UIControlService.unloadLoading();
                                                   }
                                               }, function (err) {
                                                   console.info("error:" + JSON.stringify(err));
                                                   //$.growl.error({ message: "Gagal Akses API >" + err });
                                                   UIControlService.unloadLoading();
                                               });
                                           }
                                           else {
                                               UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                                               return;
                                           }
                                       },
                                       function (err) {
                                           UIControlService.msg_growl("error", "MESSAGE.API");
                                       }
                                       );

        }

        vm.save = save;
        function save() {
            NegosiasiService.InsertDetailVendor(vm.detail,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                       $uibModalInstance.close();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                   UIControlService.unloadLoadingModal();
               }
          );
        }

    }
}
)();