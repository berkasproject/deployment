﻿(function () {
	'use strict';

	angular.module("app").controller("LogoutController", ctrl);

	ctrl.$inject = ['Idle', '$state'];

	function ctrl(Idle, $state) {
		var vm = this;

		vm.logout = logout;
		function logout() {
			setTimeout(function () {
				localStorage.removeItem('eProcValeToken');
				localStorage.removeItem('eProcValeRefreshToken');
				localStorage.removeItem('roles');
				localStorage.removeItem('sessEnd');
				localStorage.removeItem('username');
				localStorage.removeItem('login');
				localStorage.removeItem('moduleLayer');
				redirect();
			}, 2500);
		}

		function redirect() {
			Idle.unwatch(); // ng-idle run
			$state.transitionTo('home');
		}
	}
})();