(function () {
	'use strict';

	angular.module("app").controller("AanwijzingCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'AanwijzingService', '$state', 'UIControlService', 'UploadFileConfigService', 'UploaderService', 'GlobalConstantService', '$uibModal', '$stateParams', '$filter'];
	function ctrl($translatePartialLoader, AanwijzingService, $state, UIControlService, UploadFileConfigService, UploaderService, GlobalConstantService, $uibModal, $stateParams, $filter) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.aanwijzingStepId = 0;
		//console.info("ten:" + vm.IDTender + ":" + vm.IDStepTender);
		vm.online = true;
		vm.TenderID;
		vm.TenderName = '';
		vm.TenderCode = '';
		vm.StartDate = null;
		vm.EndDate = null;
		vm.nama_tahapan = '';
		vm.is_created = false;
		vm.totalItems = 0;
		vm.maxSize = 10;
		vm.pertanyaan = [];
		vm.IsAtur = false;
		//vm.AnswerStartDate = null;
		//vm.AnswerEndDate = null;
		vm.TypeAanwijzing = '';
		vm.TimeToAnswer = false;
		vm.dataAturAanwijzing = null;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.adminPost = null;
		vm.isCancelled;
		vm.hideDetailVendor = false;
		vm.isAllowEdit = false;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("aanwijzing");

			AanwijzingService.isAllowedEdit({ ID: vm.IDStepTender }, function (reply) {
				vm.isAllowEdit = reply.data
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_ACCESS");
			});
			isAanzjCommittee();
			loadAdminPost();
			loadDataTender();
			loadReviewerpostbystep();
		}

		function isAanzjCommittee() {
			AanwijzingService.isAanwjzCommittee({ ID: vm.IDStepTender }, function (reply) {
				vm.isAanwjzCommittee = reply.data
				console.info("is anw committee:" + vm.isAanwjzCommittee);
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_ACCESS");
			});

		}

		function loadAdminPost() {
			AanwijzingService.getAdminPostByStep({ ID: vm.IDStepTender }, function (reply) {
				if (reply.status === 200) {
					console.info("adminPost:" + JSON.stringify(reply.data));
					vm.adminPost = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_DATA");
			});
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('aanwijzing-print', {
				TenderRefID: vm.IDTender,
				StepID: vm.IDStepTender,
				ProcPackType: vm.ProcPackType
			});
		}

		function loadReviewerpostbystep() {
			AanwijzingService.getReviewerpostbystep({ ID: vm.IDStepTender }, function (reply) {
				if (reply.status === 200) {
					console.info("reviewerPost:" + JSON.stringify(reply.data));
					vm.reviewerPost = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_DATA");
			});
		}

		function loadDataTender() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			AanwijzingService.getDataStepTender({ ID: vm.IDStepTender }, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.TenderName = data.tender.TenderName;
					vm.isCancelled = data.tender.IsCancelled;
					vm.StartDate = UIControlService.getStrDate(data.StartDate);
					vm.EndDate = UIControlService.getStrDate(data.EndDate);
					vm.nama_tahapan = data.step.TenderStepName;
					vm.TenderID = data.TenderID;
					//console.info("tenderID:" + vm.TenderID);
					loadDataAanwijzing();

					if (!vm.isCancelled) {
						UIControlService.loadLoading("MESSAGE.LOADING");
						AanwijzingService.isNeedTenderStepApproval({
							TenderRefID: vm.IDTender,
							ID: vm.IDStepTender,
							ProcPackageType: vm.ProcPackType,
							TenderStepID: vm.IDStepTender
						}, function (result) {
							vm.isNeedApproval = result.data;

							AanwijzingService.isApprovalSent({ TenderStepID: vm.IDStepTender }, function (result) {
								vm.isApprovalSent = result.data;

								if (!vm.isNeedApproval && vm.isApprovalSent) {
									vm.hideDetailVendor = true;
								}
								UIControlService.unloadLoading();
							}, function (err) {
								UIControlService.msg_growl('error', "MESSAGE.ERR_GETAPPROVAL");
								UIControlService.unloadLoading();
							});

						}, function (err) {
							UIControlService.msg_growl('error', "MESSAGE.ERR_GETAPPROVAL");
							UIControlService.unloadLoading();
						});
					}

					//console.info("tender::" + JSON.stringify(data));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_TENDER");
				UIControlService.unloadLoading();
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPROVAL_CONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.SENDING');
					AanwijzingService.sendToApproval({ ID: vm.IDStepTender }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND');
							AanwijzingService.isNeedTenderStepApproval({
								TenderRefID: vm.IDTender, ID: vm.IDStepTender, ProcPackageType: vm.ProcPackType, TenderStepID: vm.IDStepTender
							}, function (result) {
								vm.isNeedApproval = result.data;
								AanwijzingService.isApprovalSent({ TenderStepID: vm.IDStepTender }, function (result) {
									vm.isApprovalSent = result.data;
									if (!vm.isNeedApproval && vm.isApprovalSent) {
										vm.hideDetailVendor = true;
									}
								}, function (err) {
									UIControlService.msg_growl('error', "MESSAGE.ERR_GETAPPROVAL");
									UIControlService.unloadLoading();
								});
							}, function (err) {
								UIControlService.msg_growl('error', "MESSAGE.ERR_GETAPPROVAL");
								UIControlService.unloadLoading();
							});
						} else {
							$.growl.error({ message: "Failed to Send Approval" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			$translatePartialLoader.addPart('data-contract-requisition');
			var item = vm.IDStepTender;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/aanwijzing/detailApproval.html',
				controller: "AanwijzingApprvCtrl",
				controllerAs: "AanwijzingApprvCtrl",
				resolve: { item: function () { vm.IDStepTender = item; return item; } }
			});

			modalInstance.result.then(function () { });
		}

		function loadDataAanwijzing() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			AanwijzingService.getDataAanwijzingByTender({
				TenderID: vm.TenderID,
				TenderStepID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.dataAturAanwijzing = data;

					//console.info("aan: " + JSON.stringify(data));
					if (!(data === null)) {
						vm.IsAtur = true;
						vm.StartDateVendorEntry = data.StartDateVendorEntry;
						vm.EndDateVendorEntry = data.EndDateVendorEntry;
						vm.AnswerStartDate = data.AnswerStartDate;
						vm.AnswerEndDate = data.AnswerEndDate;
						vm.TypeAanwijzing = data.TypeAaanwijzing.Value;
						//console.info(new Date(Date.parse(vm.AnswerStartDate)) + ">>" + "==" + UIControlService.getStrDate(new Date()));
						if (new Date(Date.parse(vm.AnswerStartDate)) == new Date()) {
							vm.TimeToAnswer === true;
						}
						if (!(vm.dataAturAanwijzing.PostedDate === null)) {
							vm.dataAturAanwijzing.PostedDate = UIControlService.getStrDate(vm.dataAturAanwijzing.PostedDate);
						}
						vm.jLoad(1);
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_AANWIJZING");
				UIControlService.unloadLoading();
			});
		}


		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, TenderID: vm.TenderID });
		}

		vm.listPertanyaan = [];
		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = current;
			var offset = (current * vm.maxSize) - vm.maxSize;
			//console.info("idnee:" + vm.dataAturAanwijzing.ID);
			AanwijzingService.getDataQuestions({
				FilterType: vm.dataAturAanwijzing.ID,
				Offset: offset,
				Limit: vm.maxSize
			}, function (reply) {
				//console.info("dataQue:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					//console.info("data" + JSON.stringify(data));
					if (data.Count > 0) {
						vm.aanwijzingStepId = data.List[0].AanwijzingStepID;
						vm.listPertanyaan = data.List;
						for (var i = 0; i < vm.listPertanyaan.length; i++) {
							vm.listPertanyaan[i].QuestionDate = UIControlService.getStrDate(vm.listPertanyaan[i].QuestionDate);
						}
						vm.totalItems = data.Count;
						vm.loadAllQuestion();
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
				UIControlService.unloadLoading();
			});
		}

		vm.daftarPertanyaan = [];
		vm.loadAllQuestion = loadAllQuestion;
		function loadAllQuestion() {
			AanwijzingService.getDataQuestions({
				FilterType: vm.dataAturAanwijzing.ID, Offset: 0, Limit: vm.totalItems
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.daftarPertanyaan = data.List;
					for (var i = 0; i < vm.daftarPertanyaan.length; i++) {
						vm.daftarPertanyaan[i].QuestionDate = UIControlService.getStrDate(vm.daftarPertanyaan[i].QuestionDate);
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));		
				UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
				UIControlService.unloadLoading();
			});
		}

		vm.postingInformasi = postingInformasi;
		function postingInformasi() {
			var item = null;
			if (vm.adminPost) {
				item = vm.adminPost;
			} else {
				item = {
					Title: "",
					Post: "",
					UploadURL: "",
					AanwijzingStepID: vm.dataAturAanwijzing.ID,
				};
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/aanwijzing/postingInfo.html',
				controller: 'PostingInfoCtrl',
				controllerAs: 'PostInfoCtrl',
				resolve: {
					item: function () {
						return item;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.postingPertanyaan = postingPertanyaan;
		function postingPertanyaan(data) {
			if (vm.isAanwjzCommittee == false) {
				var data = {
					TitleSummary: vm.dataAturAanwijzing.TitleSummary,
					Remark: vm.dataAturAanwijzing.Remark,
					daftarPertanyaan: vm.daftarPertanyaan,
					ID: vm.dataAturAanwijzing.ID,
					dataAanwijzing: vm.dataAturAanwijzing,
					IsAanwjzCommittee: vm.isAanwjzCommittee,
					tanggapanReviewer: vm.reviewerPost
				}
			} else if (vm.isAanwjzCommittee == true) {
				if (vm.reviewerPost != null) {
					var data = {
						TitleSummary: vm.reviewerPost.Title,
						Remark: vm.reviewerPost.Post,
						daftarPertanyaan: vm.daftarPertanyaan,
						ID: vm.dataAturAanwijzing.ID,
						dataAanwijzing: vm.reviewerPost,
						IsAanwjzCommittee: vm.isAanwjzCommittee
					}
				} else {
					var data = {
						TitleSummary: '',
						Remark: '',
						daftarPertanyaan: vm.daftarPertanyaan,
						ID: vm.dataAturAanwijzing.ID,
						dataAanwijzing: vm.reviewerPost,
						IsAanwjzCommittee: vm.isAanwjzCommittee
					}
				}
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/aanwijzing/postingAanwijzing.html',
				controller: 'PostingSummaryAanwijzingCtrl',
				controllerAs: 'PostSummaryAwjCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.aturAanwijzing = aturAanwijzing;
		function aturAanwijzing(data, isAtur) {
			//console.info(vm.TenderName + vm.TenderID + vm.IDStepTender);
			var senddata = {
				TenderName: vm.TenderName,
				AanwijzingStepID: vm.aanwijzingStepId,
				TenderID: vm.TenderID,
				TenderStepID: vm.IDStepTender,
				StartDateStep: vm.StartDate,
				EndDateStep: vm.EndDate,
				IsAtur: isAtur,
				data: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/aanwijzing/aturAanwijzing.html',
				controller: 'AturAanwijzingCtrl',
				controllerAs: 'AturAwjCtrl',
				resolve: { item: function () { return senddata; } }
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.aturVendorAanwijzing = aturVendorAanwijzing;
		function aturVendorAanwijzing() {
			var senddata = {
				TenderRefID: vm.IDTender,
				ProcPackType: vm.ProcPackType,
				TenderID: vm.TenderID,
				TenderStepID: vm.IDStepTender,
				isAllowEdit: vm.isAllowEdit,
				isCancelled: vm.isCancelled
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/aanwijzing/aturVendorAanwijzing.html?v=1.000003',
				controller: 'AturVendorAanwijzingCtrl',
				controllerAs: 'AturVendorAanwijzingCtrl',
				resolve: { item: function () { return senddata; } }
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}
	}
})();