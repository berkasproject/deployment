﻿(function () {
	'use strict';

	angular.module("app").controller("PostingSummaryAanwijzingCtrl", ctrl);

	ctrl.$inject = ['$sce', '$http', '$translate', '$translatePartialLoader', '$location',
        'AanwijzingService', '$state', 'UIControlService', 'UploadFileConfigService',
        'UploaderService', 'GlobalConstantService', '$uibModal', '$stateParams', 'item', '$uibModalInstance', ];
	function ctrl($sce, $http, $translate, $translatePartialLoader, $location,
        AanwijzingService, $state, UIControlService, UploadFileConfigService,
        UploaderService, GlobalConstantService, $uibModal, $stateParams, item, $uibModalInstance) {
		var vm = this;
		vm.TitleSummary = item.TitleSummary;
		vm.Remark = item.Remark;
		vm.pathfile = item.FileDocument;
		vm.IDAwj = item.ID;
		vm.daftarPertanyaan = item.daftarPertanyaan;
		vm.tanggapanReviewer = item.tanggapanReviewer;

		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.TenderID;
		vm.TenderName = '';
		vm.TenderCode = '';
		vm.StartDate = null;
		vm.EndDate = null;
		vm.nama_tahapan = '';
		vm.url = '';
		vm.IsAanwijzingCommittee = item.IsAanwjzCommittee;
		//console.info("isanw" + vm.IsAanwijzingCommittee);
		vm.fileUpload;
		if (vm.IsAanwijzingCommittee == true) {
			if (item.dataAanwijzing != null) {
				vm.url = item.dataAanwijzing.UploadURL;
			}
		}
		else {
			if (item.dataAanwijzing != null) {
				vm.url = item.dataAanwijzing.FileDocument;
			}
		}
		//console.info(JSON.stringify(item));
		//console.info(JSON.stringify(item));
		console.info(JSON.stringify(item));

		vm.getHtmlRemark = function () {
			return $sce.trustAsHtml(vm.Remark);
		};

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("aanwijzing");
			getTypeSizeFile();
			if (vm.Remark != null) {
				for (var i = 0; i < vm.Remark.length; i++)
					vm.Remark = vm.Remark.replace('<br/>', '\n');
			}
		}

		function loadReviewerpostbystep() {
			AanwijzingService.getReviewerpostbystep({ ID: vm.IDStepTender }, function (reply) {
				if (reply.status === 200) {
					//console.info("reviewerPost:" + JSON.stringify(reply.data));
					vm.reviewerPost = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_DATA");
			});
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.ADMIN.TENDER.AANWIJZING", function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
				UIControlService.unloadLoadingModal();
				return;
			});
		}
		vm.selectUpload = selectUpload;
		function selectUpload() {
			console.info(vm.fileUpload);
			//	//var test = vm.pathUpload;
		}

		/*proses upload file*/
		function uploadFile() {
			var folder = 'TENDER_';
			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder);
			}
		}

		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoadingModal("MESSAGE.UPLOADING_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                	console.info()
                	UIControlService.unloadLoadingModal();
                	if (response.status == 200) {
                		var url = response.data.Url;
                		UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                		processsave(url);

                	} else {
                		UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD")
                	UIControlService.unloadLoadingModal();
                });
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}

			/*
            var selectedFileType = file[0].type;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
            //console.info("tipefile: " + selectedFileType);
            if (selectedFileType === "vnd.ms-excel") {
                selectedFileType = "xls";
            }
            else if (selectedFileType === "vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                selectedFileType = "xlsx";
            }
            else {
                selectedFileType = selectedFileType;
            }
            //console.info("filenew:" + selectedFileType);
            //jika excel
            var allowed = false;
            for (var i = 0; i < allowedFileTypes.length; i++) {
                if (allowedFileTypes[i].Name == selectedFileType) {
                    allowed = true;
                    return allowed;
                }
            }

            if (!allowed) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_INVALID_FILETYPE");
                return false;
            }
            */
			return true;
		}

		/* end proses upload*/

		vm.save = save;
		function save() {
			//console.info("masuuuk");

			if (!vm.fileUpload && !vm.pathfile) {
				processsave('');
			}
			else if (!vm.fileUpload && vm.pathfile) {
				processsave(vm.pathfile);
			}
			else {
				uploadFile();
			}
		}

		function processsave(url) {
			if (url === null) {
				url = " ";
			}
			var keterangan = vm.Remark.replace(/\n/g, '<br/>');
			keterangan = keterangan.replace(/<[^<br/>]/g, '&lt;');
			if (vm.IsAanwijzingCommittee == false) {
				var datasimpan = {
					TitleSummary: vm.TitleSummary,
					Remark: keterangan,
					FileDocument: url,
					ID: vm.IDAwj
				}
				AanwijzingService.updateSummaryAanwijzing(datasimpan, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCC_POST");
						$uibModalInstance.close();
					}
					else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_POST");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.ERR_POST");
					UIControlService.unloadLoadingModal();
				});
			}
			else if (vm.IsAanwijzingCommittee == true) {
				var keterangan = vm.Remark.replace(/\n/g, '<br/>');
				//reviewer
				var datasimpan = {
					Title: vm.TitleSummary,
					Post: keterangan,
					UploadURL: url,
					AanwijzingStepID: vm.IDAwj,
					RoleID: 1054
				}
				console.info("datasimpan" + JSON.stringify(datasimpan));

				AanwijzingService.saveAdminPost(datasimpan, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCC_POST");
						$uibModalInstance.close();
					}
					else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_POST");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.ERR_POST");
					UIControlService.unloadLoadingModal();
				});
			}
		}

		vm.getListPertanyaan = getListPertanyaan;
		function getListPertanyaan() {

			vm.daftarQuest = vm.daftarPertanyaan.map(function (daftarPertanyaan) {
				return {
					Judul: daftarPertanyaan.QuestionTitle,
					Pertanyaan: daftarPertanyaan.Question,
					NamaVendor: daftarPertanyaan.vendorPosted.VendorName
				}
			});
			vm.Remark = '';
			//console.info("currLang"+localStorage.getItem("currLang"));
			vm.daftarQuest.forEach(function (sub, key) {
				var a = key + 1;
				if (localStorage.getItem("currLang") == 'id' || localStorage.getItem("currLang") == 'ID') {
					vm.Remark += a + '. Judul : ' + sub.Judul + 'Oleh :' + sub.NamaVendor + '\n';
					vm.Remark += 'Pertanyaan :' + sub.Pertanyaan;
					vm.Remark += '\n';
					vm.Remark += 'Jawaban :';
					vm.Remark += '\n';
					vm.Remark += '\n';
				}
				else if (localStorage.getItem("currLang") == 'en' || localStorage.getItem("currLang") == 'EN') {
					vm.Remark += a + '. Title : ' + sub.Judul + 'By :' + sub.NamaVendor + '\n';
					vm.Remark += 'Question :' + sub.Pertanyaan;
					vm.Remark += '\n';
					vm.Remark += 'Answer :';
					vm.Remark += '\n';
					vm.Remark += '\n';
				}
			});
			console.info(vm.Remark);
		}


		vm.getTanggapanReviewer = getTanggapanReviewer;
		function getTanggapanReviewer() {
			vm.Remark = '';
			//console.info("currLang"+localStorage.getItem("currLang"));
			if (localStorage.getItem("currLang") == 'id' || localStorage.getItem("currLang") == 'ID') {
				vm.Remark += 'Judul : ' + vm.tanggapanReviewer.Title;
				vm.Remark += '\n';
				vm.Remark += 'Dibuat oleh : ' + vm.tanggapanReviewer.CreatedBy + '. Dimodifikasi oleh : ' + vm.tanggapanReviewer.ModifiedBy + '\n';
				vm.Remark += 'Tanggapan :\n' + vm.tanggapanReviewer.Post;
				vm.Remark += '\n';
				vm.Remark += '\n';
			}
			else if (localStorage.getItem("currLang") == 'en' || localStorage.getItem("currLang") == 'EN') {
				vm.Remark += 'Title : ' + vm.tanggapanReviewer.Title;
				vm.Remark += '\n';
				vm.Remark += 'Created by : ' + vm.tanggapanReviewer.CreatedBy + '. Modified by : ' + vm.tanggapanReviewer.ModifiedBy + '\n';
				vm.Remark += 'Response :\n' + vm.tanggapanReviewer.Post;
				vm.Remark += '\n';
				vm.Remark += '\n';
			}
			if (vm.Remark != null) {
				for (var i = 0; i < vm.Remark.length; i++)
					vm.Remark = vm.Remark.replace('<br/>', '\n');
			}
			console.info(vm.Remark);
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();