﻿(function () {
	'use strict';

	angular.module("app").controller("ServiceOfferEntryCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'OfferEntryService', '$state', 'UIControlService', '$stateParams', '$uibModal','$filter'];
	function ctrl($translatePartialLoader, OEService, $state, UIControlService, $stateParams, $uibModal, $filter) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.TenderName = '';
		vm.TenderCode = '';
		vm.SOEVendor = null;
		vm.StartDate = null;
		vm.EndDate = null;
		vm.listVendor = [];
		vm.TotalItems = 0;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("pemasukkan-penawaran-jasa");
			if (vm.ProcPackType === 4189) {
				loadDataPenawaran();
			} else {
				UIControlService.msg_growl("warning", "Halaman Khusus Pemasukkan Penawaran Tender Jasa");
				return;
			}
		}

		vm.print = print;
		function print() {
		    $state.transitionTo('pemasukkan-penawaran-jasa-print', { TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType });
		}

		function loadDataPenawaran() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			OEService.getAllDataOffer({ TenderStepID: vm.IDStepTender }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    var jumlah = reply.data;
				    vm.listVendor = reply.data;
				    vm.TenderName = vm.listVendor.TenderName;
				    vm.StartDate = vm.listVendor.StartDate;
				    vm.EndDate = vm.listVendor.EndDate;
				    vm.TenderID = vm.listVendor.TenderID;
				    vm.TotalItems = vm.listVendor.ServiceOfferEntryVendors.length;
				    vm.SOEVendor = vm.listVendor.ServiceOfferEntryVendors;

				    //vm.TotalItems = Number(jumlah.Count);
				    //if (vm.TotalItems == 0) {
				    //    console.info("Masuk if");
				    //    OEService.GetStep({
				    //        Status: vm.IDTender,
				    //        FilterType: vm.ProcPackType
				    //    }, function (reply) {
				    //        UIControlService.unloadLoading();
				    //        if (reply.status === 200) {
				    //            console.info("re:" + JSON.stringify(reply));
				    //            var data = reply.data;
				    //            vm.StartDate = data.StartDate;
				    //            vm.EndDate = data.EndDate;
				    //            vm.TenderName = data.tender.TenderName;
				    //        }
				    //    }, function (err) {
				    //        UIControlService.msg_growl("error", "MESSAGE.API");
				    //        UIControlService.unloadLoading();
				    //    });
				    //} else {
				    //    console.info("Masuk else");
				    //    vm.StartDate = vm.detail[0].StartDateTen;
				    //    vm.EndDate = vm.detail[0].EndDateTen;
				    //    vm.TenderName = vm.detail[0].TenderName;
				    //}

					//vm.listVendor = data.ServiceOfferEntryVendors;
					//for (var i = 0; i < vm.listVendor.length; i++) {
					//	if (vm.listVendor[i].EntryDate != null) {
					//		vm.listVendor[i].EntryDate = UIControlService.getStrDate(vm.listVendor[i].EntryDate);
					//	}
					//}
					//vm.TenderName = data.TenderName;
					//vm.StartDate = UIControlService.getStrDate(data.StartDate);
					//vm.EndDate = UIControlService.getStrDate(data.EndDate);

					OEService.isNeedTenderStepApproval({ TenderStepID: vm.IDStepTender }, function (result) {
						vm.isNeedApproval = result.data;
						if (!vm.isNeedApproval) {
							OEService.isApprovalSent({ TenderStepID: vm.IDStepTender }, function (result2) {
								vm.isApprovalSent = result2.data;
							});
						}
						//console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});

					//console.info("ve" + JSON.stringify(vm.listVendor));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.SENDING');
					OEService.sendToApproval({ ID: vm.IDStepTender }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
							init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.IDStepTender;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pemasukkan-penawaran-jasa/detailApproval.html',
				controller: "SvcOfferEntryApprvCtrl",
				controllerAs: "SvcOfferEntryApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.backDetailTahapan = backDetailTahapan;
		function backDetailTahapan() {
		    $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, TenderID: vm.TenderID});
		}

		vm.backTahapan = backTahapan;
		function backTahapan() {
			$state.transitionTo('pemasukkan-penawaran-jasa', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, StepID: vm.IDStepTender });
		}

		vm.kelengkapanTender = kelengkapanTender;
		function kelengkapanTender() {
			$state.transitionTo('kelengkapan-tender-jasa', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, StepID: vm.IDStepTender });
		}
	}
})();