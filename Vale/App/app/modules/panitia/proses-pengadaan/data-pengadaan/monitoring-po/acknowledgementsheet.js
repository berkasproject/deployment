﻿(function () {
    'use strict';

    angular.module("app").controller("AknowSheetCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.paket = [];
        vm.dataPOId = String($stateParams.id);
        vm.init = init;
        vm.contactCP = [];
        vm.contactAddress = [];
        function init() {
            $translatePartialLoader.addPart('acknowledgementsheet');
            loadPaket();

        };

        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataMonitoringService.ExportPurchase({
                Status: vm.dataPOId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.export = reply.data;
                    for (var i = 0; i < vm.export.vendorcontact.length; i++) {
                        if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.contactCompany = vm.export.vendorcontact[i];
                        } else if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
                            vm.contactCP.push(vm.export.vendorcontact[i]);
                        } else if (vm.export.vendorcontact[i].VendorContactType.Type == "VENDOR_OFFICE_TYPE") {
                            vm.contactAddress.push(vm.export.vendorcontact[i]);
                            if (vm.contactAddress.length == 1) {
                                if (vm.export.vendorcontact[i].Contact.Address.State != null) {
                                    if (vm.export.vendorcontact[i].Contact.Address.State.Country.Name === "Indonesia") {
                                        if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
                                            if (vm.export.vendorcontact[i].Contact.Address.DistrictID != null) {
                                                vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.Distric.Name + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                            } else
                                                vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        } else {
                                            vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        }
                                    } else {
                                        if (vm.export.vendorcontact[i].Contact.Address.City == null) {
                                            vm.export.vendorcontact[i].Contact.Address.City = {};
                                            vm.export.vendorcontact[i].Contact.Address.City.Name = "";
                                        }

                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name
                                    }
                                }
                            }
                        }
                    }
                    showAck();
                } else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.showAck = showAck;
        function showAck() {
            UIControlService.loadLoading(loadmsg);
            DataMonitoringService.Acknowledge({
                Status: vm.dataPOId,
                FilterType: vm.export.vendorcontact[0].VendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.ack = reply.data;
                } else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.printAck = printAck;
        function printAck(acknow) {

            var innerContents = document.getElementById(acknow).innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Acknowledgement Sheet-' + vm.export.vendorcontact[0].Vendor.business.Name + ' ' + vm.export.vendorcontact[0].Vendor.VendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();

            //$uibModalInstance.close();
        }


        vm.cariPaket = cariPaket;
        function cariPaket(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            loadPaket();
        };


        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
        vm.back = back;
        function back() {
            $state.transitionTo('detail-po', { id: vm.dataPOId });
        }
    }
})();
