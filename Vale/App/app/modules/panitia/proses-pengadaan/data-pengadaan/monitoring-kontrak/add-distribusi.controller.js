﻿(function () {
    'use strict';

    angular.module("app").controller("AddDistribusiCtrl", ctrl);

    ctrl.$inject = ['$uibModalInstance', '$state', '$stateParams', 'item', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataKontrakService', 'UIControlService'];
    /* @ngInject */
    function ctrl($uibModalInstance, $state, $stateParams, item, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataKontrakService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.list = [];
        vm.init = init;
        vm.dataConSignOff;
        vm.isCalendarOpened = [false, false, false];
        vm.listItemPO = [];


        vm.years;
        vm.keterangan = '';
        vm.totalprice = 0;
        vm.buyer_remask = '';
        vm.TahunDistribusi = '';
        vm.TanggalPO = '';
        vm.NoPO = '';
        vm.budget = 0;
        vm.freight_method = '';
        vm.delivery_point = '';
        vm.SumGenerate = 0;

        function init() {
            console.info(item);
            $translatePartialLoader.addPart('add-distribusi');
            loadPaket(1);
        };

        vm.loadPaket = loadPaket;
        function loadPaket(current) {
            vm.currentPage
            UIControlService.loadLoading(loadmsg);
            DataKontrakService.GenerateDetailPO({
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Agreement: item.item.ContractNo,
                SAPCode: item.item.Vendor.SAPCode
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listItemPO = reply.data.List;
                    vm.totalItems = Number(reply.data.Count);
                    for (var i = 0; i < vm.listItemPO.length; i++) {
                        if (vm.listItemPO[i].DocDate !== null) { vm.listItemPO[i].DocDate = UIControlService.getStrDate(vm.listItemPO[i].DocDate); }
                        vm.SumGenerate = +vm.SumGenerate + +vm.listItemPO[i].NetPrice;
                    }
                }
                else {
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.Generate = Generate;
        function Generate(current) {
            vm.currentPage = current;
            UIControlService.loadLoading(loadmsg);
            DataKontrakService.GenerateDetailPO({
                ID: vm.datalempar.ItemDistribusi == undefined ? 0 : vm.datalempar.ItemDistribusi.DisBudgetId,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                DocDate: UIControlService.getStrDate(vm.StartDate),
                Agreement: vm.dataConSignOff.ContractNo,
                SAPCode: vm.dataConSignOff.Vendor.SAPCode
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listItemPO = reply.data.List;
                    vm.totalItems = Number(reply.data.Count);
                    for (var i = 0; i < vm.listItemPO.length; i++) {
                        if (vm.listItemPO[i].DocDate !== null) { vm.listItemPO[i].DocDate = UIControlService.getStrDate(vm.listItemPO[i].DocDate); }
                        vm.SumGenerate = +vm.SumGenerate + +vm.listItemPO[i].NetPrice;
                    }
                }
                else {
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }

        vm.getStrDate = getStrDate;
        function getStrDate(date) {
            return UIControlService.getStrDate(date);
        }

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.CreateDistribusi = CreateDistribusi;
        function CreateDistribusi() {
            UIControlService.loadLoading(loadmsg);
            DataKontrakService.Create({
                DistributionBudgetId: vm.datalempar.ItemDistribusi == undefined ? 0 : vm.datalempar.ItemDistribusi.DisBudgetId,
                NegoId: vm.negoId,
                buyer_remark: vm.buyer_remark,
                keterangan: vm.keterangan,
                TahunDistribusi: UIControlService.getStrDate(vm.StartDate),
                TenderStepId: vm.TenderStepId,
                totalprice: vm.SumGenerate,
                VendorId: vm.vendorId,
                ContractRequisitionId: vm.ContractRequisitionId,
                DatePurchase: UIControlService.getStrDate(vm.StartDate),
                NoPurchase: vm.dataConSignOff.ContractNo,
                SAPCode: vm.dataConSignOff.Vendor.SAPCode

            },
        function (reply) {
            if (reply.status === 200) {
                UIControlService.msg_growl("success", 'MESSAGE.MESSAGE_SUCCESS', "MESSAGE.TITLE_SUCCESS");
                UIControlService.unloadLoading();
                $uibModalInstance.close();
            }
            else {
                UIControlService.msg_growl("error", 'MESSAGE.MESSAGE_FAILED', "MESSAGE.TITLE_FAILED");
                UIControlService.unloadLoading();
            }
        },
        function (error) {
            UIControlService.unloadLoading();
            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
        });

        }

        
    }
})();
