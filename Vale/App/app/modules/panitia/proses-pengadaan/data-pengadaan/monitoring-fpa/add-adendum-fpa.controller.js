﻿(function () {
    'use strict';

    angular.module("app").controller("AddFPACtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.fileUpload;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.VHSdata;
        vm.listVHS = [];
        vm.listNewItem = [];
        vm.currencyList = [];
        vm.listUploadItem = [];
        vm.init = init;
        vm.id = Number($stateParams.id);
        vm.negoid = Number($stateParams.negoid);
        vm.flag = Number($stateParams.flag);
        vm.isCalendarOpened = [false, false, false];
        vm.datetoStart;
        vm.datetoEnd;
        vm.VA;
        vm.Remask = '';
        vm.AdditionalValue = 0;
        vm.TypeAddendumMonth = 0;
        vm.Budget_Val = 0;
        vm.TypeAddendum = 1;
        vm.StartDate;
        vm.EndDate;
        vm.Duration = 0;
        vm.RequestDate = '';
        vm.DocUrl = "";
        vm.DocName = "";
        vm.div = 0;
        vm.DivCount = 0;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadTypeSizeFile();
            loadData();
            getAddendum();
            loadDetailPO(1);
            loadDetailPOAll();

        };

        vm.loadDetailPOAll = loadDetailPOAll;
        function loadDetailPOAll() {
            vm.DataListAll = [];
            UIControlService.loadLoading(loadmsg);
            DataFPAService.DetailItemAdendum({
                Status: vm.id,
                Offset: 0,
                Limit: 0
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data.List;
                    vm.DataListAll = reply.data.List;
                }
                else {
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.getAddendum = getAddendum;
        function getAddendum() {
            DataFPAService.getCodeAddendumFPA({
                Status: vm.id
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.AddendumCode = reply.data;
                }
                else {
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadDetailPO = loadDetailPO;
        function loadDetailPO(current) {
            vm.DataDocList = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            UIControlService.loadLoading(loadmsg);
            DataFPAService.DetailItemAdendum({
                Status: vm.id,
                Offset: offset,
                Limit: vm.pageSize
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    
                    var data = reply.data.List;
                    vm.data = reply.data;
                    vm.DataDocList = reply.data.List;
                    if(vm.currentPage == 1) vm.DataDocListOriPaging = reply.data.List;
                    vm.totalOri = Number(reply.data.Count);
                    if (vm.TypeAddendum == '3') {
                        if (vm.listNewItem.length != 0) {
                            vm.totalItems = +(reply.data.Count) + +vm.listNewItem.length;
                        }
                        else vm.totalItems = Number(reply.data.Count);
                    }
                    else if (vm.TypeAddendum == '4') {
                        if (vm.listUploadItem.length != 0) {
                            vm.totalItems = vm.listUploadItem.length;
                        }
                        else vm.totalItems = Number(reply.data.Count);

                    }
                    else vm.totalItems = Number(reply.data.Count);
                    if (vm.TypeAddendum == '4') {
                        vm.DataDocList = [];
                        console.info(offset);
                        for (var i = offset; i < (offset+10); i++) {
                            if (vm.listUploadItem[i] != undefined) {
                                vm.DataDocList.push(vm.listUploadItem[i]);
                            }
                        }
                    }
                    else if (vm.TypeAddendum == '3') {
                        vm.div = ((vm.totalItems - 10) % 10);
                        vm.DivCount = ((vm.totalItems - 10) / 10).toFixed();
                        if (vm.div !== 0) vm.DivCount = +vm.DivCount + 1;
                        if ((+vm.currentPage - 1) == vm.DivCount) {
                            if (vm.listNewItem.length !== 0) {
                                for (var i = 0; i < vm.listNewItem.length; i++) {
                                    vm.DataDocList.push(vm.listNewItem[i]);
                                }
                            }
                        }
                    }
                }
                else {
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoading(loadmsg);
            DataFPAService.AddAddendum({
                Offset: vm.pageSize * (vm.currentPage - 1),
                Status: vm.id,
                Limit: vm.pageSize,
                Column: vm.column
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listVHS = reply.data.List;
                    vm.VHSdata = data;
                    vm.VendorID = vm.VHSdata.VendorID;
                    vm.TenderStepID = vm.VHSdata.TenderStepID;
                    vm.datetoStart = new Date(Date.parse(vm.VHSdata.EndDate));
                    vm.StartDate = new Date(Date.parse(vm.VHSdata.EndDate));
                    vm.datetoEnd = "";
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        function loadTypeSizeFile() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.CONTRACTREQUISITION.DOCS", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoading();
                return;
            });
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        vm.modalUploadItem = modalUploadItem;
        function modalUploadItem() {
            vm.VHSdata.listItem = vm.DataListAll;
            var item = {
                Item: vm.VHSdata
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/add-uploaditem-adendum.html',
                controller: 'AddFPAUploadItemCtrl',
                controllerAs: 'AddFPAUploadItemCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                vm.DataDocList = [];
                vm.listNewItem = [];
                vm.listUploadItem = data.listItem;
                vm.totalItems = data.listItem.length;
                for (var i = 0; i < 10; i++) {
                    vm.DataDocList.push(data.listItem[i]);
                }

            });
        }

        vm.save = save;
        function save() {
            vm.RequestDate = UIControlService.getStrDate(vm.RequestDate);
            if (!vm.RequestDate || !vm.AddendumCode) {
                UIControlService.msg_growl("error", "MESSAGE.INCOMPLETE");
                UIControlService.unloadLoading();
                return;
            }
            if (vm.TypeAddendum == '1') {
                if (vm.AdditionalValue == 0) {
                    UIControlService.msg_growl("error", "MESSAGE.NO_ADD");
                    return;
                }
            }
            if (vm.TypeAddendum == 2) {
                if (vm.TypeAddendumMonth == 0) {
                    UIControlService.msg_growl("error", "MESSAGE.NO_MONTH");
                    return;
                }
            }
            if (vm.TypeAddendum == 3 && vm.listNewItem.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_ITEM");
                UIControlService.unloadLoading();
                return;
            }

            if (vm.fileUpload) {
                uploadFile();
            }
            else {
                saveProcess();
            }
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }

        /*proses upload file*/
        vm.uploadFile = uploadFile;
        function uploadFile() {
            var folder = "Addendum_" + vm.id + vm.DocName;
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return;
            }

            if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder);
            }
        }

        function upload(file, config, filters, folder, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, folder,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var nameDoc = "Addendum_" + vm.id;

                        vm.DocUrl = url;
                        vm.DocName = nameDoc;
                        vm.pathFile = vm.folderFile + url;

                        UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPLOAD");
                        saveProcess(url, nameDoc, size);

                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                    UIControlService.unloadLoading();
                });

        }
        /* end proses upload*/

        function saveProcess(docurl, docname, docsize) {
            if (vm.TypeAddendum == '1' || vm.TypeAddendum == '3' || vm.TypeAddendum == '4') {
                vm.datetoStart = vm.VHSdata.StartDate;
                vm.datetoEnd = vm.VHSdata.EndDate;
                vm.Duration = 0;

            }
            else if (vm.TypeAddendum == '2') {
                vm.Duration = vm.TypeAddendumMonth;
                vm.datetoStart = UIControlService.getStrDate(vm.StartDate);
                vm.datetoEnd = UIControlService.getStrDate(vm.EndDate);
            }
            if (vm.TypeAddendum !== '1') {
                vm.AdditionalValue = 0;
            }
            if (vm.TypeAddendum == '3') {
                if (vm.listNewItem.length != 0) {
                    for (var i = 0; i < vm.listNewItem.length; i++) {
                        vm.DataListAll.push(vm.listNewItem[i]);
                    }
                }
            }
            else if (vm.TypeAddendum == '4') {
                vm.DataListAll = vm.listUploadItem;
            }
            var senddata = {
                AddendumCode: vm.AddendumCode,
                TypeAddendum: vm.TypeAddendum,
                BudgetContract: vm.Budget_Val,
                AdditionalValue: vm.AdditionalValue,
                RequestDate: vm.RequestDate,
                DocUrl: docurl,
                DocName: docname,
                StartDate: vm.datetoStart,
                EndDate: vm.datetoEnd,
                Requestor: vm.Requestor,
                VendorID: vm.VendorID,
                TenderStepID: vm.TenderStepID,
                VHSAwardId: vm.VHSdata.VHSAwardId,
                Remask: vm.Remask,
                Duration: vm.Duration,

                MaterialCode: vm.material_code,
                ItemDescrip: vm.item_des,
                Manufacture: vm.manufacture,
                PartNo: vm.part_no,
                Estimate: vm.estimation,
                Unit: vm.uop,
                Currency: vm.currency,
                PriceIDR: vm.PriceIDR,
                LeadTime: vm.eadtim,
                Remark: vm.Remask,
                CountryOfOrigin: vm.country,
                NegoId: vm.negoid,
                MonitoringId: vm.id,
                listItem: vm.DataListAll

            }
            DataFPAService.CreateAddendum(senddata, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.MESSAGE_SUCCESS");
                    $state.go('detail-data-adendum-fpa', { id: vm.id, flag: vm.flag });

                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.MESSAGE_FAILED");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.modalPrice = modalPrice;
        function modalPrice() {
            var lempar = {
                datalempar: {
                    negoid: negoid,
                    id: id,
                    AddendumCode: vm.AddendumCode,
                    TypeAddendum: vm.TypeAddendum,
                    BudgetContract: vm.Budget_Val,
                    AdditionalValue: vm.AdditionalValue,
                    RequestDate: vm.RequestDate,
                    StartDate: vm.datetoStart,
                    EndDate: vm.datetoEnd,
                    Requestor: vm.Requestor,
                    VendorID: vm.VendorID,
                    TenderStepID: vm.TenderStepID,
                    VHSAwardId: vm.id,
                    Remask: vm.Remask,
                    Duration: vm.Duration,

                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/price-fpa.modal.html',
                controller: 'PriceCtrl',
                controllerAs: 'PriceCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();

            });

        };

        vm.getMon = getMon;
        function getMon(duration) {
            var date = new Date(vm.datetoStart);
            var oldDate = date.getDate();
            var oldMonth = date.getMonth();
            var oldYear = date.getFullYear();

            var newMonth = oldMonth + parseInt(duration);
            var datetoEnd = new Date(oldYear, newMonth, oldDate);

            vm.datetoEnd = new Date(oldYear, newMonth, oldDate);
            vm.EndDate = vm.datetoEnd;
            return datetoEnd;
        };

        vm.modalNewItem = modalNewItem;
        function modalNewItem() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/add-newitem-adendum.html',
                controller: 'AddFPANewItemCtrl',
                controllerAs: 'AddFPANewItemCtrl'
            });
            modalInstance.result.then(function (data) {
                vm.listNewItem.push({
                    MaterialCode: data.material_code,
                    ItemDescrip: data.item_des,
                    Manufacture: data.manufacture,
                    PartNo: data.part_no,
                    Estimate: data.estimation,
                    Unit: data.uop,
                    Currency: data.currency,
                    UnitPrice: data.PriceIDR,
                    LeadTime: data.eadtim,
                    CountryOfOrigin: data.CountryOfOrigin,
                    Remark: data.Remark
                });
                vm.totalItems = +vm.totalItems + 1;
                loadDetailPO(1);
            });
        }

        vm.getAddendumListItem = getAddendumListItem;
        function getAddendumListItem(type) {
            vm.listNewItem = [];
            vm.DataDocList = vm.DataDocListOriPaging;
            vm.totalItems = vm.totalOri;
        }
    }
})();
