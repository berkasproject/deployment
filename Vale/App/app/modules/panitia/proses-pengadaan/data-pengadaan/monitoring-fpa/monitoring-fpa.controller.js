﻿(function () {
	'use strict';

	angular.module("app").controller("MonFPACtrl", ctrl);

	ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";

		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.id = 0;
		vm.maxSize = 10;
		vm.listFPA = [];
		vm.searchBy = 0;
		vm.init = init;
		vm.id = Number($stateParams.id);

		vm.searchBy = 0;
		vm.keyword = "";
		vm.pageSize = 10;
		vm.currentPage = 1;

		function init() {
			$translatePartialLoader.addPart('monitoring-fpa');
			vm.id = 1;
			loadPaket();
		};

		vm.initVHS = initVHS;
		function initVHS() {
			$translatePartialLoader.addPart('monitoring-fpa');
			vm.id = 0;
			loadPaket();
		};


		vm.loadPaket = loadPaket;
		function loadPaket() {
			UIControlService.loadLoading(loadmsg);
			DataFPAService.Select({
				FilterType: vm.searchBy,
				Keyword: vm.keyword,
				Status: vm.id
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listFPA = reply.data.List;
					vm.FPAdata = data;
					vm.totalItems = Number(data.Count);
					vm.listFPA.forEach(function (data) {
						if (data.VHSAward.SAPContractNo == null) {
							data.SpendingContract = 0;
							data.RemainingBudget = data.BudgetContract;
						}
					});
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.cariPaket = cariPaket;
		function cariPaket(keyword) {
			vm.keyword = keyword;
			vm.currentPage = 1;
			loadPaket();
		};

		vm.insertBudget = insertBudget;
		function insertBudget(id) {
			UIControlService.loadLoading(loadmsg);
			DataFPAService.UpdateBudget({
				BudgetContract: vm.Budget_Val,
				ID: id,
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'Sukses', "MESSAGE.TITLE_SUCCESS");
					UIControlService.unloadLoading();
					loadPaket();
				} else {
					UIControlService.msg_growl("error", 'Gagal', "MESSAGE.TITLE_FAILED");
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		}

		vm.viewAddendum = viewAddendum;
		function viewAddendum(id) {
			$state.transitionTo('detail-data-adendum-fpa', {id, flag: vm.id });

		};

		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.toDetail = toDetail;
		function toDetail(flag, databyclick) {
			vm.flag = flag;
			if (databyclick.RFQType == 1) vm.id = 0;
			else vm.id = 1;
			var item = {
				ID: databyclick.VHSAwardID,
				flag: vm.id
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detail-fpa.modal.html',
				controller: 'DetailItemCtrl',
				controllerAs: 'DetailItemCtrl',
				resolve: {
					item: function () {
						return item;
					}
				}
			});
			modalInstance.result.then(function () {
				if (vm.flag == 0) $state.go('monitoring-fpa');
				else $state.go('monitoring-vhs');

			});

		};

		vm.sentMail = sentMail;
		function sentMail(data) {
			vm.idEmail = data.ID;
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detail-email-monitoring.html',
				controller: 'DetEmailMonCtrl',
				controllerAs: 'DetEmailMonCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				sentMailVendor();
			});
		}

		vm.sendMail = sendMail;
		function sendMail(data) {
			vm.idEmail = data.ID;
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detail-email-monitoring-fpa.html',
				controller: 'DetEmailMonFpaCtrl',
				controllerAs: 'DetEmailMonFpaCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				sentMailVendorFPA();
			});
		}

		vm.sentMailVendor = sentMailVendor;
		function sentMailVendor() {
			DataFPAService.InsertEmail({
				ID: vm.idEmail
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl('success', "Email Sent");
					//$state.go('monitoring-vhs');
					loadPaket()
				} else {
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		}

		vm.setAck = setAck
		function setAck(data) {
			//bootbox.confirm($filter('translate')('CONFIRM_ACKNOWLEDGE'), function (res) {
			//	if (res) {
			//		DataFPAService.setAck({
			//			ID: data.ID
			//		}, function (reply) {
			//			if (reply.status === 200) {
			//				UIControlService.msg_growl('success', "ACK_SUCCESS");
			//				loadPaket()
			//			} else {
			//				UIControlService.unloadLoading();
			//			}
			//		}, function (error) {
			//			UIControlService.unloadLoading();
			//			UIControlService.msg_growl("error", 'MESSAGE.ERR_ACK');
			//		});
			//	}
			//});

			bootbox.prompt({
				title: $filter('translate')('CONFIRM_ACKNOWLEDGE'),
				buttons: {
					confirm: { label: "OK" },
					cancel: { label: "Cancel" }
				},
				callback: function (res) {
					if (res != null) {
						DataFPAService.setAck({
							ID: data.ID,
							Remark:res
						}, function (reply) {
							if (reply.status === 200) {
								UIControlService.msg_growl('success', "ACK_SUCCESS");
								loadPaket()
							} else {
								UIControlService.unloadLoading();
							}
						}, function (error) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'MESSAGE.ERR_ACK');
						});
					}
				}
			});

		}

		vm.sentMailVendorFPA = sentMailVendorFPA;
		function sentMailVendorFPA() {
			DataFPAService.InsertEmail({
				ID: vm.idEmail
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl('success', "Email Sent");
					loadPaket()
				} else {
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		}
	}
})();
