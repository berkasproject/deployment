﻿(function () {
    'use strict';

    angular.module("app").controller("DetFPACtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService', '$window'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService, $window) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.VHSdata;
        vm.listFPA = [];
        vm.init = init;
        var id = Number($stateParams.id);
        vm.id = Number($stateParams.id);
        vm.flag = Number($stateParams.flag);
        function init() {
            $translatePartialLoader.addPart('detail-data-adendum');
            loadPaket(1);
        };


        vm.bback = bback;
        function bback() {
            $window.history.back();
        }

        vm.sendToApprove = sendToApprove;
        function sendToApprove(data) {
            vm.AddendumId = data;
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_ADD'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    DataFPAService.InsertAddendumApproval({ AddendumId: data },
                              function (reply) {
                                  if (reply.status === 200) {
                                      UIControlService.unloadLoading();
                                      UIControlService.msg_growl("success", "MESSAGE.SUCC_SEND");
                                      init(); sendMailToApproval();
                                  }
                              },
                              function (err) {
                                  UIControlService.unloadLoading();
                              }
                          );
                }
            });
        }

        vm.sendMailToApproval = sendMailToApproval;
        function sendMailToApproval() {
            DataFPAService.sendMailToApproval({
                AddendumId: vm.AddendumId
            },
            function (reply) {
                UIControlService.msg_growl("success", 'MESSAGE.EMAIL_SENT');
                UIControlService.unloadLoading();
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.detailApproval = detailApproval;
        function detailApproval(dt) {
            var data = {
                data: dt
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detailApproval.modal.html',
                controller: 'detailApprovalCtrl',
                controllerAs: 'detailApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.loadPaket = loadPaket;
        function loadPaket(current) {
            vm.currentPage = current;
            UIControlService.loadLoading(loadmsg);
            DataFPAService.Addendum({
                Offset: vm.pageSize * (vm.currentPage - 1),
                Status: vm.id,
                Limit: vm.pageSize,
                Column: vm.column
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.flagButton = true;
                    vm.listFPA = reply.data.List;
                    vm.listFPAAddendum = reply.data.List;
                    vm.listFPA.forEach(function (data) {
                        if (data.SAPContractNo == null) {
                            data.SpendingVal = 0;
                            data.RemainingVal = data.Budget_Val;
                        }

                        if (data.Response !== "Approve") vm.flagButton = false;
                    });
                    vm.totalItems = Number(vm.listFPA.length);
                    if (vm.listFPA.length == 1 && vm.listFPA[0].AddendumId == 0) {
                        vm.listFPA = [];
                        vm.totalItems = 0;
                        vm.flagButton = true;
                    }

                   
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.cariPaket = cariPaket;
        function cariPaket(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            loadPaket();
        };

        vm.addAdendum = addAdendum;
        function addAdendum(id, negoid) {
            $state.transitionTo('add-adendum-fpa', { id: id, negoid: negoid, flag: vm.flag });
        };

        vm.editAddendum = editAddendum;
        function editAddendum(id, negoid) {
            $state.transitionTo('edit-adendum-fpa', { id: id, negoid: negoid, flag: 1, flagstep: vm.flag });
        };
        vm.viewEditAddendum = viewEditAddendum;
        function viewEditAddendum(id, negoid) {
            $state.transitionTo('edit-adendum-fpa', { id: id, negoid: negoid, flag: 0, flagstep: vm.flag });
        };
        vm.Approve = Approve;
        function Approve(id) {
            UIControlService.loadLoading(loadmsg);
            DataFPAService.Approval({
                AddendumId: id,
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    loadPaket();
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };
        vm.Reject = Reject;
        function Reject(id) {
            UIControlService.loadLoading(loadmsg);
            DataFPAService.Reject({
                AddendumId: id,
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    loadPaket();
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };
        vm.viewAddendum = viewAddendum;
        function viewAddendum(AddendumId) {
            AddendumId: AddendumId
            var lempar = {
                datalempar: {
                    AddendumId: AddendumId,
                    AddendumCode: vm.listFPA[0].AddendumCode,
                    VendorName: vm.listFPA[0].VendorName,
                    TitleDoc: vm.listFPA[0].TitleDoc,
                    TypeAddendum: vm.listFPA[0].TypeAddendum,
                    BudgetContract: vm.listFPA[0].Budget_Val,
                    SpendingVal: vm.listFPA[0].SpendingVal,

                    AdditionalValue: vm.listFPA[0].AdditionalValue,
                    RequestDate: vm.listFPA[0].RequestDate,
                    RFQCode: vm.listFPA[0].RFQCode,
                    StartDate: vm.listFPA[0].StartDate,
                    EndDate: vm.listFPA[0].EndDate,
                    StartContractDate: vm.listFPA[0].StartContractDate,
                    FinishContractDate: vm.listFPA[0].FinishContractDate,
                    Requestor: vm.listFPA[0].Requestor,
                    VendorID: vm.listFPA[0].VendorID,
                    TenderStepID: vm.listFPA[0].TenderStepID,
                    VHSAwardId: vm.id,
                    Remask: vm.listFPA[0].Remask,
                    Duration: vm.listFPA[0].Duration,

                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detail-fpa.modal.html',
                controller: 'DetailAddendumFPACtrl',
                controllerAs: 'DetailAddendumFPACtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();

            });

        };
        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.back = back;
        function back(){
            if (vm.flag == 0) $state.go('monitoring-vhs');
            else $state.go('monitoring-fpa');
        }
    }
})();
