﻿(function () {
	'use strict';

	angular.module("app").controller("dataUpload", ctrl);

	ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'dataUploadService', 'UIControlService', 'UploadFileConfigService', 'ExcelReaderService', 'GlobalConstantService', 'UploaderService'];
	/* @ngInject */
	function ctrl($state, $stateParams, $http, $filter, $translate, $translatePartialLoader, $location, SocketService, dataUploadService, UIControlService, UploadFileConfigService, ExcelReaderService, GlobalConstantService, UploaderService) {
		var vm = this;		//
		var loadmsg = "LOADING";

		vm.currentPage = 1;
		vm.fileUpload;
		vm.maxSize = 10;
		vm.DocName = "";
		vm.currentPage = 1;
		vm.listItemPO = [];
		vm.totalItems = 0;
		vm.keyword = "";
		vm.ID = Number($stateParams.id);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.dateNow = $filter('date')(1, 'dd/MM/yyyy');
		vm.init = init;

		function init() {
			$translatePartialLoader.addPart('detail-dataupload-po');
			$translatePartialLoader.addPart('dataupload-po');
			UIControlService.loadLoading("MESSAGE.LOADING");
			loadTypeSizeFile();
			loadData();
		}

		vm.loadData = loadData;
		function loadData(current) {
			//console.info("curr "+current)
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			dataUploadService.Select({
				Offset: offset,
				Limit: vm.maxSize
			}, function (reply) {
				//console.info("data:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listItemPO = data.List;

					for (var i = 0; i < vm.listItemPO.length; i++) {
						if (vm.listItemPO[i].PODate === null) { vm.listItemPO[i].PODate = "-"; }
						else { vm.listItemPO[i].PODate = UIControlService.getStrDate(vm.listItemPO[i].PODate); }
						if (vm.listItemPO[i].PODueDate === null) { vm.listItemPO[i].PODueDate = "-"; }
						else { vm.listItemPO[i].PODueDate = UIControlService.getStrDate(vm.listItemPO[i].PODueDate); }
					}
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		function loadTypeSizeFile() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.ADMIN.MASTER.ITEMPR", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoading();
				return;
			});
		}

		/*count of property object*/
		function numAttrs(obj) {
			var count = 0;
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					++count;
				}
			}
			return count;
		}

		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.toDetail = toDetail;
		function toDetail(id) {
			$state.transitionTo('detail-dataupload-po', { id: id });
		}

		vm.toCompare = toCompare;
		function toCompare(id) {
			$state.transitionTo('compare-dataupload-po', { id: id });
		}

		//start upload
		vm.uploadFile = uploadFile;
		function uploadFile() {
			//UIControlService.msg_growl('error', "MESSAGE.MAINTENANCE");
			//return;

			if (vm.fileUpload === undefined) {
				UIControlService.msg_growl("error", "MSG_NOFILE");
				return;
			}

			if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {

				ExcelReaderService.readExcel(vm.fileUpload, function (reply) {
					//UIControlService.unloadLoading();
					if (reply.status === 200) {
						var excelContents = reply.data;
						var Sheet1 = excelContents[Object.keys(excelContents)[0]]; /*untuk baca nama sheet*/

						if (Sheet1[1].Column1 !== null && (Sheet1[1].Column1.toLowerCase().includes('rfqm') || Sheet1[1].Column1.toLowerCase().includes('fpam'))) {
							var propCount = Object.keys(Sheet1[0]).length
							if (propCount !== 34) {
								UIControlService.msg_growl('error', "MESSAGE.INVALID_EXCEL");
								return;
							}

							upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
						} else {
							upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
						}
					}
				});
			}
		}

		function upload(file, config, filters, dates, callback) {
			//console.info(file);
			var size = config.Size;

			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoading("MESSAGE.LOADING");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates, function (response) {
				//console.info("upload:" + JSON.stringify(response.data));
				//UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					var fileName = response.data.FileName;
					vm.pathFile = url;
					uploadSave(fileName, vm.pathFile);
					//saveExcelContent(fileName, url);
				} else {
					UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
					return;
				}
			}, function (response) {
			    UIControlService.msg_growl("error", "MSG_ERR_UPLOAD")
				UIControlService.unloadLoading();
			});
		}

		function saveExcelContent(IdSAP) {
			vm.newExcel = [];

			dataUploadService.readExcelAPI({ Data1: vm.fileUpload, Data2: IdSAP }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MSG_SUC_SAVE");
					vm.init();
				} else {
					UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
			    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
			    if (err.data.Message === "EMPTY_OUR_REF") {
			        UIControlService.msg_growl("error", "MESSAGE.EMPTY_OUR_REF");
			    }
				UIControlService.unloadLoading();
			});

			//ExcelReaderService.readExcel(vm.fileUpload, function (reply) {
			//	//UIControlService.unloadLoading();
			//	if (reply.status === 200) {
			//		var excelContents = reply.data;
			//		var Sheet1 = excelContents[Object.keys(excelContents)[0]]; /*untuk baca nama sheet*/
			//		//console.info(Sheet1);
			//		var countproperty = numAttrs(Sheet1[0]);
			//		for (var a = 1; a < Sheet1.length; a++) {

			//			//if (Sheet1[a].Column1 !== null && (Sheet1[a].Column1.toLowerCase().includes('rfqm') || Sheet1[a].Column1.toLowerCase().includes('fpam'))) {
			//			//	var propCount = Object.keys(Sheet1[0]).length
			//			//	if (propCount !== 34) {
			//			//		UIControlService.msg_growl('error', "MESSAGE.INVALID_EXCEL");
			//			//		return;
			//			//	}
			//			//}

			//			if (Sheet1[a].Column2 !== null) {
			//				var objExcel = {
			//					RFQCode: Sheet1[a].Column1,
			//					VendorCodeSAP: Sheet1[a].Column2,
			//					CreatedDate: Sheet1[a].Column3,
			//					PONumber: Sheet1[a].Column4,
			//					POItem: Sheet1[a].Column5,
			//					MaterialNumber: Sheet1[a].Column6,
			//					Preq_No: Sheet1[a].Column7,
			//					Preq_Item: Sheet1[a].Column8,
			//					Agreement: Sheet1[a].Column9,
			//					Agmt_Item: Sheet1[a].Column10,
			//					Qty: Number(Sheet1[a].Column11),
			//					UnitMeasure: Sheet1[a].Column12,
			//					ShortText: Sheet1[a].Column13,
			//					Mfr_No: Sheet1[a].Column14,
			//					Mfr_Name: Sheet1[a].Column15,
			//					Manu_Mat: Sheet1[a].Column16,
			//					Purch_OrderText: Sheet1[a].Column17,
			//					PO_ItemText: Sheet1[a].Column18,
			//					TariffCode: Sheet1[a].Column19,
			//					ConcessCat: Sheet1[a].Column20,
			//					DelivDate: Sheet1[a].Column21,
			//					Currency: Sheet1[a].Column22,
			//					NettPricePO: Sheet1[a].Column23,
			//					TotalPricePO: Sheet1[a].Column24,
			//					BuyerRemark: Sheet1[a].Column25,
			//					IdSAP: IdSAP
			//				};

			//				if (Sheet1[a].Column26 != null) objExcel.Purch_Group = Sheet1[a].Column26; // PUR_GROUP

			//				if (Sheet1[a].Column27 != null) objExcel.IncoTerms1 = Sheet1[a].Column27; // INCOTERMS1

			//				if (Sheet1[a].Column28 != null) objExcel.IncoTerms2 = Sheet1[a].Column28; // INCOTERMS2

			//				if (Sheet1[a].Column29 != null) objExcel.PaymentTerms = Sheet1[a].Column29; // PMNTTRMS

			//				if (Sheet1[a].Column30 != null) objExcel.StorageLocationCode = Sheet1[a].Column30; // STORE_LOC

			//				if (Sheet1[a].Column31 != null) objExcel.ApprovalDatePR = Sheet1[a].Column31; // PR-DATE

			//				if (Sheet1[a].Column34 != null) objExcel.CurrencyPR = Sheet1[a].Column34; // PR-CURR

			//				if (Sheet1[a].Column33 != null) objExcel.TotalvaluePR = Sheet1[a].Column33; // PR-PRICE

			//				//if (Sheet1[a].Column32 != null) objExcel.TotalvaluePR = Sheet1[a].Column32;

			//				if (objExcel.RFQCode != null) {
			//					console.info(objExcel.RFQCode);
			//					objExcel.RFQCode = String(objExcel.RFQCode).trim();
			//				}
			//				if (!(objExcel.CreatedDate === null) && !(objExcel.CreatedDate === undefined)) {
			//					if (Number.isInteger(objExcel.CreatedDate)) {
			//						var date = new Date(1900, 0, 1);
			//						date.setDate(date.getDate() + objExcel.CreatedDate - 2);
			//						objExcel.CreatedDate = UIControlService.getStrDate(date);
			//					} else {
			//						if (objExcel.CreatedDate.substring(1, 2) === '/' || objExcel.CreatedDate.substring(2, 3) === '/') {
			//							var dateTemp = objExcel.CreatedDate.split('/')
			//							dateTemp[0] = Number(dateTemp[0]) - 1

			//							//objExcel.DelivDate = new Date(dateTemp[2], dateTemp[1], dateTemp[0], 0, 0)
			//							objExcel.CreatedDate = new Date(Date.UTC(dateTemp[2], dateTemp[0], dateTemp[1], 0, 0))
			//						}
			//						else
			//							objExcel.CreatedDate = UIControlService.convertDateFromExcel(objExcel.CreatedDate);
			//					}
			//				} else {
			//					objExcel.CreatedDate = null;
			//				}

			//				if (!(objExcel.DelivDate === null) && !(objExcel.DelivDate === undefined)) {
			//					if (Number.isInteger(objExcel.DelivDate)) {
			//						var date = new Date(1900, 0, 1);
			//						date.setDate(date.getDate() + objExcel.DelivDate - 2);
			//						objExcel.DelivDate = UIControlService.getStrDate(date);
			//					} else {
			//						if (objExcel.DelivDate.substring(1, 2) === '/' || objExcel.DelivDate.substring(2, 3) === '/') {
			//							var dateTemp = objExcel.DelivDate.split('/')
			//							dateTemp[0] = Number(dateTemp[0]) - 1

			//							//objExcel.DelivDate = new Date(dateTemp[2], dateTemp[1], dateTemp[0], 0, 0)
			//							objExcel.DelivDate = new Date(Date.UTC(dateTemp[2], dateTemp[0], dateTemp[1], 0, 0))
			//						}
			//						else
			//							objExcel.DelivDate = UIControlService.convertDateFromExcel(objExcel.DelivDate);
			//					}
			//				} else {
			//					objExcel.DelivDate = null;
			//				}

			//				if (!(objExcel.ApprovalDatePR === null) && !(objExcel.ApprovalDatePR === undefined)) {
			//					if (Number.isInteger(objExcel.ApprovalDatePR)) {
			//						var date = new Date(1900, 0, 1);
			//						date.setDate(date.getDate() + objExcel.ApprovalDatePR - 2);
			//						objExcel.ApprovalDatePR = UIControlService.getStrDate(date);
			//					} else {
			//						if (objExcel.ApprovalDatePR.substring(1, 2) === '/' || objExcel.ApprovalDatePR.substring(2, 3) === '/') {
			//							var dateTemp = objExcel.ApprovalDatePR.split('/')
			//							dateTemp[0] = Number(dateTemp[0]) - 1

			//							//objExcel.DelivDate = new Date(dateTemp[2], dateTemp[1], dateTemp[0], 0, 0)
			//							objExcel.ApprovalDatePR = new Date(Date.UTC(dateTemp[2], dateTemp[0], dateTemp[1], 0, 0))
			//						} else
			//							objExcel.ApprovalDatePR = UIControlService.convertDateFromExcel(objExcel.ApprovalDatePR);
			//					}
			//				} else {
			//					objExcel.ApprovalDatePR = null;
			//				}

			//				vm.newExcel.push(objExcel);
			//			}
			//		}
			//		uploadFileExcel(vm.newExcel);
			//	}
			//});
		}

		vm.tglSekarang = UIControlService.getDateNow("");
		function uploadSave(filename, url) {
			dataUploadService.InsertFile({
				FileName: filename,
				DocUrl: url
			}, function (reply) {
				//UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					//UIControlService.msg_growl("success", "MSG_SUC_SAVE");
					saveExcelContent(reply.data.ID);
					//$state.transitionTo('master-rate');
				} else {
					UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
			    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
				UIControlService.unloadLoading();
			});
		}

		vm.toDetail1 = toDetail1;
		function toDetail1(id) {
			UIControlService.loadLoading("");
			dataUploadService.Compare({ ID: id }, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					if (reply.data != "") {
						UIControlService.msg_growl("error", reply.data);
						//return;
					} else
						UIControlService.msg_growl("success", "MSG_SUC_COMPARE");
					vm.init();
				} else {
					UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				if (err == 'Upload Data SAP not found. Please retry compare.') {
					UIControlService.msg_growl("error", err);
					vm.init();
				} else {
				    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					UIControlService.unloadLoading();
				}
			});
		}

		vm.uploadFileExcel = uploadFileExcel;
		function uploadFileExcel(data) {
			dataUploadService.Insert(data, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MSG_SUC_SAVE");
					vm.init();
				} else {
					UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
			    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
				UIControlService.unloadLoading();
			});
		}

		vm.deleteFile = deleteFile;
		function deleteFile(id) {
			dataUploadService.DeleteFile({ ID: id }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MSG_SUCCESS_DELETE");
					vm.init();
				} else {
					UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoading();
			});
		}
	}
})();
