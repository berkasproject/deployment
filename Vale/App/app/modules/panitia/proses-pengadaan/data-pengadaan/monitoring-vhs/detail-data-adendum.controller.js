﻿(function () {
    'use strict';

    angular.module("app").controller("DetVHSCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.VHSdata;
        vm.listVHS = [];
        vm.init = init;
        var id = Number($stateParams.id);
        vm.id = Number($stateParams.id);
        function init() {
            $translatePartialLoader.addPart('detail-data-adendum');
            loadPaket();
        };
        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.Addendum({
                Offset: vm.pageSize * (vm.currentPage - 1),
                Status: vm.id,
                Limit: vm.pageSize,
                Column: vm.column
            },
            function (reply) {
                console.info("data:" + JSON.stringify(reply.data));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    console.info("LIST:" + JSON.stringify(data));
                    vm.listVHS = reply.data.List;
                    var lisDoc = reply.data.List[0];

                    vm.VHSdata = data;
                    vm.totalItems = Number(data.Count);
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.cariPaket = cariPaket;
        function cariPaket(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            loadPaket();
        };

        vm.addAdendum = addAdendum;
        function addAdendum(id, negoid) {
            $state.transitionTo('add-adendum', { id: id, negoid: negoid });
        };

        vm.editAddendum = editAddendum;
        function editAddendum(id, negoid) {
            $state.transitionTo('edit-adendum', { id: id, negoid: negoid });
        };
        vm.Approve = Approve;
        function Approve(id) {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.Approval({
                AddendumId: id,
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    loadPaket();
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };
        vm.Reject = Reject;
        function Reject(id) {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.Reject({
                AddendumId: id,
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    loadPaket();
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };
        vm.viewAddendum = viewAddendum;
        function viewAddendum(AddendumId) {
            AddendumId: AddendumId
            var lempar = {
                datalempar: {
                    AddendumId: AddendumId,
                    AddendumCode: vm.listVHS[0].AddendumCode,
                    VendorName: vm.listVHS[0].VendorName,
                    TitleDoc: vm.listVHS[0].TitleDoc,
                    TypeAddendum: vm.listVHS[0].TypeAddendum,
                    BudgetContract: vm.listVHS[0].Budget_Val,
                    SpendingVal: vm.listVHS[0].SpendingVal,

                    AdditionalValue: vm.listVHS[0].AdditionalValue,
                    RequestDate: vm.listVHS[0].RequestDate,
                    RFQCode: vm.listVHS[0].RFQCode,
                    StartDate: vm.listVHS[0].StartDate,
                    EndDate: vm.listVHS[0].EndDate,
                    StartContractDate: vm.listVHS[0].StartContractDate,
                    FinishContractDate: vm.listVHS[0].FinishContractDate,
                    Requestor: vm.listVHS[0].Requestor,
                    VendorID: vm.listVHS[0].VendorID,
                    TenderStepID: vm.listVHS[0].TenderStepID,
                    VHSAwardId: vm.id,
                    Remask: vm.listVHS[0].Remask,
                    Duration: vm.listVHS[0].Duration,

                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/detail.modal.html',
                controller: 'DetailAddendumCtrl',
                controllerAs: 'DetailAddendumCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();

            });

        };
        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
    }
})();
