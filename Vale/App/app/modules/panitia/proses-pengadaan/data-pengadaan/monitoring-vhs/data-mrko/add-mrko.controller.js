﻿(function () {
    'use strict';

    angular.module("app").controller("AddMRKOCtrl", ctrl);

    ctrl.$inject = ['$filter', '$state', '$stateParams', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($filter, $state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.fileUpload;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.Keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.VHSdata;
        vm.listVHS = [];
        vm.currencyList = [];
        vm.init = init;
        vm.flag = Number($stateParams.flag);
        var id = Number($stateParams.id);
        vm.id = Number($stateParams.id);
        var negoid = Number($stateParams.negoid);
        vm.negoid = Number($stateParams.negoid);
        vm.isCalendarOpened = [false, false, false];
        vm.datetoStart;
        vm.datetoEnd;
        vm.VA;
        vm.Remask = '';
        vm.AdditionalValue = 0;
        vm.Budget_Val = 0;
        vm.TypeAddendum = 1;
        vm.StartDate;
        vm.EndDate;
        vm.Duration = 0;
        vm.Requestor = 1;
        vm.RequestDate = '';
        vm.DocUrl = "";
        vm.DocName = "";
        vm.VendorName = "";
        vm.jumlah = 0;
        vm.jumlahUSD = 0;
        vm.listStorage = [];
        vm.isCalendarOpened = [false, false, false, false];
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.Status = 0;
        vm.flagChange = false;
        vm.timezone = new Date().getTimezoneOffset();
        vm.timezoneClient = vm.timezone / 60;
        function init() {
            vm.flagChange = vm.flag;
            $translatePartialLoader.addPart('data-mrko');
            if (vm.flag != 0) {
                selectById();
                selectStorageLoc();
            }
            else {
                selectStorageLoc();
            };

        };

        vm.monitoring = [];
        vm.selectById = selectById;
        function selectById() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.selectById({ Status: vm.flag }, function (response) {
                if (response.status == 200) {
                    UIControlService.unloadLoading();
                    vm.monitoring = response.data;
                    vm.StartDate = new Date(vm.monitoring.Periode1);
                    vm.EndDate = new Date(vm.monitoring.Periode2);
                    vm.selectStorage = vm.monitoring.StorageLocation;
                    vm.Vendor = vm.monitoring.Vendor;
                    vm.VendorName = vm.Vendor.businessName + '. ' + vm.Vendor.VendorName;
                    GenerateMRKO();
                }
            }, function (response) {
                UIControlService.unloadLoading();
            });
        }


        vm.selectStorageLoc = selectStorageLoc;
        function selectStorageLoc() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.selectStorageLoc({column:vm.flag}, function (response) {
                if (response.status == 200) {
                    UIControlService.unloadLoading();
                    vm.listStorage = response.data;
                }
            }, function (response) {
                UIControlService.unloadLoading();
            });
        }

        vm.listVendor = [];
        vm.selectVendorByStorageLoc = selectVendorByStorageLoc;
        function selectVendorByStorageLoc() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.selectVendorByStorageLoc({Keyword:vm.selectStorage},function (response) {
                if (response.status == 200) {
                    UIControlService.unloadLoading();
                    vm.listVendor = response.data;
                    //console.info("list vendor:" + JSON.stringify(vm.listVendor));
                    vm.Vendor = vm.listVendor[0];
                    vm.VendorName = vm.Vendor.businessName + '. ' + vm.Vendor.VendorName;
                }
            }, function (response) {
                UIControlService.unloadLoading();
            });
        }

        vm.changeStorage = changeStorage;
        function changeStorage() {
            //console.info("selected storage:" + JSON.stringify(vm.selectStorage));
            if (vm.selectStorage != undefined) {
                selectVendorByStorageLoc();
            }
            else {
                vm.listVendor = [];
            }
        }

        vm.GenerateMRKO = GenerateMRKO;
        function GenerateMRKO(){
            if (vm.selectStorage!=undefined&&vm.StartDate!=undefined
                && vm.EndDate != undefined && vm.Vendor != undefined) {
                generateItemMRKO();
            }
        }

        vm.itemMRKO = [];
        vm.generateItemMRKO = generateItemMRKO;
        function generateItemMRKO() {
            var startDate = new Date(vm.StartDate.getTime());
            var endDate = new Date(vm.EndDate.getTime());
            startDate.setHours(startDate.getHours() - vm.timezoneClient);
            endDate.setHours((endDate.getHours() - vm.timezoneClient)+23);
            UIControlService.loadLoading(loadmsg);
            DataVHSService.generateItemMRKO({
                Keyword: vm.selectStorage,
                Keyword2: vm.Vendor.SAPCode,
                Date1: startDate,
                Date2: endDate,
                Limit: vm.pageSize,
                Offset: (vm.currentPage * vm.pageSize) - vm.pageSize,
                column:vm.flag
            }, function (response) {
                if (response.status == 200) {
                    UIControlService.unloadLoading();
                    vm.dataGenerate = response.data;
                    vm.totalItems = vm.dataGenerate.Count;
                    //console.info("list item:" + JSON.stringify(vm.itemMRKO));
                }
            }, function (response) {
                UIControlService.unloadLoading();
            });
        }

/*
        vm.loadUploadDataMRKO = loadUploadDataMRKO;
        function loadUploadDataMRKO(generate) {
            //vm.Status = status;
            //vm.Keyword = keyword;
            vm.Generate = generate;
            vm.dataStorageSelect = vm.selectStorage;
            //var offset = (current * 10) - 10;
            UIControlService.loadLoading(loadmsg);
            DataVHSService.ViewToCreate({
                Status: vm.Status,
                Offset: 20,
                Limit: vm.pageSize,
                Date1: UIControlService.getStrDate(vm.StartDate),
                Date2: UIControlService.getStrDate(vm.EndDate),
                FilterType: vm.flag,
                Keyword: vm.Keyword
            },
                   function (response) {
                       if (response.status == 200) {
                           UIControlService.unloadLoading();
                           vm.data = response.data;
                           if (vm.flag == 0) {
                               if (vm.Status == 0) {
                                   vm.listStorage = [];
                                   for (var i = 0; i < vm.data.length; i++) {
                                       vm.listStorage.push({
                                           ID: vm.data[i].ID,
                                           StorageLocation: vm.data[i].UploadDataDetailMRKO[0]
                                       });
                                   }
                               }
                               else {
                                   if (vm.data.length == 0) {
                                       vm.dataGenerate = {
                                           UploadDataDetailMRKO: []
                                       }
                                       vm.totalItems = 0;
                                       vm.jumlah = 0;
                                       vm.jumlahUSD = 0;
                                   }
                                   else {
                                       for (var i = 0; i < vm.data.length; i++) {
                                           if (vm.data[i].ID == vm.selectStorage.ID) {
                                               vm.dataGenerate = vm.data[i];
                                               vm.totalItems = vm.data[i].Count;
                                               vm.jumlah = vm.data[i].Total;
                                               vm.jumlahUSD = vm.data[i].TotalInUSD;
                                           }

                                       }
                                   }
                               }
                           }
                           else {
                               vm.listStorage.forEach(function (listStorage) {
                                   vm.data.forEach(function (data) {
                                       if (listStorage.ID = data.ID) {
                                           vm.dataGenerate = data;
                                           vm.totalItems = data.Count;
                                           vm.jumlah = data.Total;
                                           vm.jumlahUSD = data.TotalInUSD;
                                           vm.selectStorage = listStorage;
                                           vm.StartDate = UIControlService.convertDate(data.Periode1);
                                           vm.EndDate = UIControlService.convertDate(data.Periode2);
                                           vm.VendorName = data.UploadDataDetailMRKO[0].VendorName;
                                       }

                                   });
                               });
                           }
                       }
                   },
                   function (response) {
                       UIControlService.unloadLoading();
                   });
        }

        vm.GenerateMRKO = GenerateMRKO;
        function GenerateMRKO(status, current, generate, keyword) {
            if (vm.StartDate == undefined) {
                UIControlService.msg_growl('error', 'Start Date Belum diisi');
                return;
            }
            if (vm.EndDate == undefined) {
                UIControlService.msg_growl('error', 'End Date Belum diisi');
                return;
            }
            else {
                vm.Status = status;
                vm.Keyword = keyword;
                vm.flag = 0;
                loadUploadDataMRKO(generate);
            }
        }

        vm.loadAllStorage = loadAllStorage;
        function loadAllStorage() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.ViewToCreate({
                Status: 0,
                Offset: 0,
                Limit: 10,
                Date1: UIControlService.getStrDate(vm.StartDate),
                Date2: UIControlService.getStrDate(vm.EndDate),
                FilterType: 0
            }, function (response) {
                if (response.status == 200) {
                    UIControlService.unloadLoading();
                    vm.data = response.data;
                    vm.listStorage = [];
                    for (var i = 0; i < vm.data.length; i++) {
                        vm.listStorage.push({
                            ID: vm.data[i].ID,
                            StorageLocation: vm.data[i].UploadDataDetailMRKO[0]
                        });
                        if (i == (vm.data.length - 1)) {
                            vm.Status = 0;
                            loadUploadDataMRKO(0);
                        }
                    }
                }
            }, function (response) {
                UIControlService.unloadLoading();
            });
        }
*/

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.save = save;
        function save() {
            var startDate = new Date(vm.StartDate.getTime());
            var endDate = new Date(vm.EndDate.getTime());
            startDate.setHours(startDate.getHours() - vm.timezoneClient);
            endDate.setHours((endDate.getHours() - vm.timezoneClient) + 23);
            bootbox.confirm($filter('translate')('Apakah anda yakin ingin mengirim data approval untuk pembuatan MRKO ?'), function (yes) {
                if (yes) {
                    DataVHSService.InsertApproval({
                        ID: vm.flagChange,
                        DataUploadMRKOId: vm.dataGenerate.ID,
                        Periode1: UIControlService.getStrDate(startDate),
                        Periode2: UIControlService.getStrDate(endDate)
                    },
                  function (response) {
                      if (response.status == 200) {
                          UIControlService.msg_growl("success", "Berhasil kirim Approval");
                          sendMailToApprover();
                          $state.go('monitoring-mrko');
                          UIControlService.unloadLoading();
                      }
                  },
                  function (response) {
                      UIControlService.unloadLoading();
                  });
                }
            });
        }

        vm.sendMailToApprover = sendMailToApprover;
        function sendMailToApprover() {
            DataVHSService.sendMailToApprover({
                DataUploadMRKOId: vm.dataGenerate.ID,
                Periode1: UIControlService.getStrDate(vm.StartDate),
                Periode2: UIControlService.getStrDate(vm.EndDate)
            },
                  function (response) {
                      if (response.status == 200) {
                          UIControlService.msg_growl("success", "Email Sent !!!");
                          UIControlService.unloadLoading();
                      }
                  },
                  function (response) {
                      UIControlService.unloadLoading();
                  });
        }

        /*
        vm.toDetail = toDetail;
        function toDetail() {
            var item = {
                item: vm.dataGenerate,
                ID: vm.dataGenerate.ID,
                Periode1: UIControlService.getStrDate(vm.StartDate),
                Periode2: UIControlService.getStrDate(vm.EndDate)

            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko/detail-itemmonitoring.html',
                controller: 'DetailItemMRKOCtrl',
                controllerAs: 'DetailItemMRKOCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();

            });
        }
        */
    }
})();
