﻿(function () {
    'use strict';

    angular.module("app")
    .controller("evaluasiHargaJasaModalController", ctrl);

    ctrl.$inject = ['$state', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiHargaJasaService', 'DataPengadaanService', 'UIControlService', 'UploaderService', 'UploadFileConfigService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, EvaluasiHargaJasaService, DataPengadaanService, UIControlService, UploaderService, UploadFileConfigService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var tenderRefID = item.tenderRefID;
        var procPackageType = item.procPackageType;
        var tenderID = item.tenderID;
        var evaluasiID = item.evaluasiID;
        var stepID = item.stepID;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.evaluationVendor = item.evaluationVendor;
        vm.tenderStepData = item.tenderStepData;
        vm.currency = item.currency;
        vm.isAllowedEdit = item.isAllowedEdit;
        vm.noPriceLimit = item.noPriceLimit;
        vm.totalCostEstimate = 0;
        vm.offerLimit = 0;
        vm.fileUpload;
        //vm.isVariation = false;
        //vm.isNational = false;
        vm.noLimitByDefault = false;

        vm.documentDate = item.tenderStepData.DocumentDate ? new Date(item.tenderStepData.DocumentDate) : new Date();
        vm.isCalendarOpen = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-harga-jasa');

            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiHargaJasaService.checkTime({
                ID: stepID
            }, function (reply) {
                vm.isTime = reply.data;
                EvaluasiHargaJasaService.checkTechnicalEvaluation({
                    ID: stepID
                }, function (reply) {
                    if (reply.data === true) {
                        UIControlService.unloadLoadingModal();
                    } else {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl('error', "MESSAGE.ERR_CHK_TE");
                        $uibModalInstance.dismiss('cancel');
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl('error', "MESSAGE.ERR_CHK_TE");
                    $uibModalInstance.dismiss('cancel');
                });
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD_TIME");
                $uibModalInstance.dismiss('cancel');
            });

            loadData();
            loadUploadFileConfig();
        };

        function loadData() {

            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiHargaJasaService.getTotalCostEstimate({
                TenderRefID: tenderRefID,
            }, function (reply) {
                vm.totalCostEstimate = reply.data;
                EvaluasiHargaJasaService.getOfferPriceLimit({
                    TenderRefID: tenderRefID,
                    ProcPackageType: procPackageType
                }, function (reply) {
                    vm.offerLimit = reply.data;
                    var limit = vm.totalCostEstimate * vm.offerLimit / 100;
                    vm.upperlimit = vm.totalCostEstimate + limit;
                    vm.lowerlimit = vm.totalCostEstimate - limit;
                    EvaluasiHargaJasaService.isNoLimit({
                        ID: tenderID
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        vm.noLimitByDefault = reply.data;
                        if (vm.evaluationVendor.length === 0) {
                            EvaluasiHargaJasaService.getOfferEntryVendor({
                                ID: tenderID
                            }, function (reply) {
                                vm.evaluationVendor = reply.data;
                            }, function (error) {
                                UIControlService.unloadLoadingModal();
                                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                            });
                        } else {
                            vm.showPriceAndScore = true;
                            UIControlService.unloadLoadingModal();
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                    });
                }, function (error) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_LIMIT');
                });
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TOTAL_CE');
            });
        };

        vm.generateSkor = generateSkor;
        function generateSkor() {
            
            var noLimit = vm.noLimitByDefault || vm.noPriceLimit;

            var minCost = null;
            vm.evaluationVendor.forEach(function (ev) {
                if (ev.IsPassed) {
                    if (noLimit || (ev.OfferTotalCost >= vm.lowerlimit && ev.OfferTotalCost <= vm.upperlimit)) {
                        if (minCost === null || minCost > ev.OfferTotalCost) {
                            minCost = ev.OfferTotalCost;
                        }
                    }
                }
            });
            vm.evaluationVendor.forEach(function (ev) {
                ev.Score = 0;
                if (ev.IsPassed) {
                    if (noLimit || (ev.OfferTotalCost >= vm.lowerlimit && ev.OfferTotalCost <= vm.upperlimit)) {
                        if (ev.OfferTotalCost) {
                            ev.Score = minCost * 5 / ev.OfferTotalCost;
                        }
                    }
                }
            });
            vm.showPriceAndScore = true;
        }

        vm.lihatKuisioner = lihatKuisioner;
        function lihatKuisioner(ev) {
            var item = {
                SOEVID: ev.OfferEntryVendorID,
                vendorName: ev.VendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-harga-jasa/detailKuisioner.modal.html',
                controller: 'detailKuisionerPenawaranController',
                controllerAs: 'dkpCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () { });
        };

        vm.openCalendar = openCalendar;
        function openCalendar() {
            if (vm.isTime && vm.isAllowedEdit) {
                vm.isCalendarOpen = true;
            }
        }

        function loadUploadFileConfig() {
            UploadFileConfigService.getByPageName("PAGE.ADMIN.EVALUATION.SOPRICE", function (response) {
                if (response.status == 200) {
                    vm.uploadConfigs = response.data;
                    vm.fileTypes = generateFilterStrings(response.data);
                    vm.fileSize = vm.uploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
            });
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        function selectUpload(file) {
            vm.fileUpload = file;
        }

        vm.simpan = simpan;
        function simpan() {
            if (vm.fileUpload){
                uploadFile();
            } else {
                saveEvaluation();
            }
        }

        function uploadFile() {
            if (validateFileType()) {
                upload();
            }
        }

        function validateFileType() {
            if (!vm.fileUpload || vm.fileUpload == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload() {
            var size = vm.fileSize.Size;
            var unit = vm.fileSize.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }
            UIControlService.loadLoadingModal(loadmsg);
            UploaderService.uploadSingleFileSOEvaluation(tenderID, vm.fileUpload, size, vm.fileTypes,
            function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    var url = reply.data.Url;
                    vm.tenderStepData.DocumentUrl = url;
                    saveEvaluation();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
            });
        };

        function saveEvaluation() {
            vm.tenderStepData.DocumentDate = UIControlService.getStrDate(vm.documentDate);
            var evaluasi = {
                ID: evaluasiID,
                ServiceOfferEntryEvaluationVendors: vm.evaluationVendor,
                TenderStepData: vm.tenderStepData,
                IsNoPriceLimit: vm.noPriceLimit
            }

            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiHargaJasaService.saveEvaluation(evaluasi, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    UIControlService.msg_growl("notice", 'MESSAGE.SUCC_SAVE');
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE');
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE');
            });
        };

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();