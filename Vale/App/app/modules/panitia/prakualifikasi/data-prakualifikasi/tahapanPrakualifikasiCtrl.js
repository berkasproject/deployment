(function () {
    'use strict';

    angular.module("app")
    .controller("prakualTahapanCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'DataPraqualService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, DataPraqualService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.ID = Number($stateParams.id);

        vm.step = [];
        vm.paket;
        vm.namaLelang;
        vm.userBisaMengatur = false;
        //vm.page_id = 37;
        //vm.selectedOption;
        //vm.menuhome = 0;
        //vm.ditolak = false;
        //vm.tahapanDitolak = {};
        vm.isCancelled = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-prakualifikasi');
            loadSteps();
        };

        function loadSteps() {
            DataPraqualService.selectbyid({
                ID: vm.ID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.data = reply.data;
                    DataPraqualService.getstepbyid({
                        PrequalSetupId: vm.ID
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            vm.step = reply.data;
                            vm.totalItems = vm.step.length;
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                        }
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        };

        vm.aturTahapan = aturTahapan;
        function aturTahapan(step) {
            var stepItem = {
                prakualID: vm.ID,
                stepID: step.ID,
                StartDate: new Date(Date.parse(step.StartDate)),
                EndDate: new Date(Date.parse(step.EndDate)),
                Status: step.Status,
                PrequalStepName: step.PrequalStepName
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/data-prakualifikasi/tahapanPrakualifikasi.modal.html',
                controller: 'prakualStepConfig',
                controllerAs: 'prakualStepConfig',
                resolve: {
                    item: function () {
                        return stepItem;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadSteps();
            });
        };

        function convertTanggal(input) {
            return UIControlService.convertDateTime(input);
        };

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('data-prekualifikasi');
        };

        vm.menujuTahapan = menujuTahapan;
        function menujuTahapan(data) {
            $state.transitionTo(data.PrequalStepURL, {
                PrequalSetupID: data.PrequalSetupID,
                SetupStepID: data.ID,
                PrequalStepID: data.ID /* alternative for SetupStepID */
            });
        };
    }
})();


