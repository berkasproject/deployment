(function () {
    'use strict';

    angular.module("app")
    .controller("dataPrequalCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'DataPraqualService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, DataPraqualService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;

        vm.paket = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-prakualifikasi');
            loadPaket();
        };

        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataPraqualService.select({
                Keyword: vm.keyword,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.data = reply.data.List;
                    vm.totalItems = reply.data.Count;
                    vm.data.forEach(function (p) {
                        //cr.PublishedDateConverted = convertDate(cr.PublishedDate);
                        p.tahapanSekarang = [];
                        p.tahapanLama = [];
                        p.tahapanNext = [];
                        p.isKetua = false;

                        p.PrequalSetupSteps.forEach(function (ts) {
                            if (ts.StateNext === 'Curr') {
                                p.tahapanSekarang.push(ts);
                            } else if (ts.StateNext === 'Next') {
                                p.tahapanNext.push(ts);
                            } else if (ts.StateNext === 'Old') {
                                p.tahapanLama.push(ts);
                            }
                        });
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoading();
            });
        };

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            loadPaket();
        };

        vm.viewTahapan = viewTahapan;
        function viewTahapan(data) {
            $state.transitionTo('data-prekualifikasi-tahapan', { id: data.ID });
        };

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
    }
})();
