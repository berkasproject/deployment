﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailEvaluasiModal", ctrl);
    
    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MetodeEvaluasiService', 'KriteriaEvaluasiService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, metodeEvaluasiService, KriteriaEvaluasiService, UIControlService, item) {

        var vm = this;
        var data = item.data;
        var med_id = item.med_id;
        var page_id = item.page_id;
        var detailLama = [];
        var detailBaru = [];
        var loadingMessage = "";
        vm.sudahDipakai = item.sudahDipakai;
        vm.hasChild = true;
        vm.evaluationMethodTypeName = item.evaluationMethodTypeName;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('metode-evaluasi');
            $translate.refresh().then(function () {
                loadingMessage = $filter('translate')('MESSAGE.LOADING');
            });
            //$rootScope.getSession().then(function (result) {
            //    $rootScope.userSession = result.data.data;
            //    $rootScope.userLogin = $rootScope.userSession.session_data.username;
            //    $rootScope.authorize(vm.initialize());
            //});
            vm.initialize()
        };

        vm.initialize = initialize;
        function initialize() {

            var selectedCriteriaId = [];
            data.forEach(function (d) {
                selectedCriteriaId.push(d.kriteria_id);
            });

            KriteriaEvaluasiService.selectCriteria({
                keyword: '',
                level: item.level,
                parentId: item.parent,
                offset: 0,
                limit: 0,
                selectedCriteriaId: selectedCriteriaId
            }, function (reply2) {
                if (reply2.status === 200) {
                    if (reply2.data.length > 0) {
                        var allKriteria = reply2.data;
                        vm.kriteria = [];
                        if (item.level === 1) { //filter kriteria barang/jasa
                            var isGoodsOrService;
                            if (vm.evaluationMethodTypeName === 'TYPE_SERVICE') {
                                isGoodsOrService = true;
                            } else if (vm.evaluationMethodTypeName === 'TYPE_GOODS' || vm.evaluationMethodTypeName === 'TYPE_VHS' || vm.evaluationMethodTypeName === 'TYPE_FPA') {
                                isGoodsOrService = false;
                            }
                            if (isGoodsOrService !== undefined) {
                                allKriteria.forEach(function (krit) {
                                    if (krit.IsGoodsOrService === isGoodsOrService) {
                                        vm.kriteria.push(krit);
                                    }
                                });
                            }
                        } else { //Tidak perlu filter untuk lv2+
                            vm.kriteria = allKriteria;
                        }
                        for (var i = 0; i < vm.kriteria.length; i++) {
                            vm.kriteria[i].EMDId = med_id
                            vm.kriteria[i].checked = false;
                            vm.kriteria[i].Weight = 0;
                            vm.kriteria[i].MustHave = 0;
                            vm.kriteria[i].ShowMustHaveOpt = true;
                            for (var j = 0; j < data.length; j++) {
                                if (vm.kriteria[i].CriteriaId === data[j].kriteria_id) {
                                    vm.kriteria[i].Id = data[j].med_kriteria_id;
                                    vm.kriteria[i].checked = true;
                                    vm.kriteria[i].Weight = data[j].bobot;
                                    vm.kriteria[i].MustHave = data[j].must_have ? data[j].must_have : 0;
                                    vm.kriteria[i].ShowMustHaveOpt = !data[j].children || data[j].children.length === 0;
                                    break;
                                }
                            }
                            if (vm.evaluationMethodTypeName === 'TYPE_GOODS' && vm.kriteria[i].IsGOEvaluationMandatory === true) {
                                vm.kriteria[i].checked = true;
                                vm.kriteria[i].IsReadOnly = true;
                            }
                            if ((vm.evaluationMethodTypeName === 'TYPE_VHS' || vm.evaluationMethodTypeName === 'TYPE_FPA') && vm.kriteria[i].IsGOEvaluationMandatory === true) {
                                vm.kriteria[i].checked = true;
                                vm.kriteria[i].IsReadOnly = true;
                            }
                        }
                    } else {
                        vm.hasChild = false;
                    }
                }
            });
        };

        vm.simpan = simpan;
        function simpan() {
            for (var i = 0; i < vm.kriteria.length; i++) {
                if (vm.kriteria[i].checked && !(vm.kriteria[i].Weight || vm.kriteria[i].Weight === 0)) {
                    UIControlService.msg_growl('error', "MESSAGE.WEIGHT");
                    return;
                }
            }
            var totalPersentase = 0;
            for (var i = 0; i < vm.kriteria.length; i++) {
                if (vm.kriteria[i].checked) {
                    totalPersentase = totalPersentase + Number(vm.kriteria[i].Weight);
                }
            }
            if (totalPersentase !== 100) {
                UIControlService.msg_growl('error', "MESSAGE.PERSENTAGE");
                return;
            }
            var detail = [];
            for (var i = 0; i < vm.kriteria.length; i++) {
                vm.kriteria[i].Parent = vm.kriteria[i].ParentId; //Beda Nama Kolom di DB
                vm.kriteria[i].IsActive = vm.kriteria[i].checked;
                vm.kriteria[i].Weight = vm.kriteria[i].checked ? vm.kriteria[i].Weight : 0;
                detail.push(vm.kriteria[i]);
            }
            UIControlService.loadLoadingModal(loadingMessage);
            metodeEvaluasiService.saveDetailCriteria(detail,
                function (reply) {
                    if (reply.status === 200) {
                        vm.kriteria = [];
                        UIControlService.msg_growl('notice', "MESSAGE.SUCC_EDIT_CRITERIA");
                        UIControlService.unloadLoadingModal();
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl('error', "MESSAGE.FAIL_EDIT_CRITERIA");
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.msg_growl('error', "MESSAGE.FAIL_EDIT_CRITERIA");
                }
            );
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();