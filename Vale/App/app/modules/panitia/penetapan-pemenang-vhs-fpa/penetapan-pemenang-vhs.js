(function () {
	'use strict';

	angular.module("app").controller("PPGVHSCtrl", ctrl);

	ctrl.$inject = ['ApprovalNoSAPService', '$filter', 'Excel', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService',
        'PenetapanPemenangVHSservice', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl(ApprovalNoSAPService, $filter, Excel, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService,
        PenetapanPemenangVHSservice, RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		var page_id = 141;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.evalsafety = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.userBisaMengatur = false;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.allowAdd = true;
		vm.allowEdit = true;
		vm.allowDelete = true;
		vm.kata = new Kata("");
		vm.init = init;
		vm.exportHref;
		vm.detail = [];
		vm.jLoad = jLoad;
		vm.isCalendarOpened = [false, false, false, false];
		vm.isCalendarOpened1 = [false, false, false, false];
		vm.step;

		function init() {
			$translatePartialLoader.addPart('negosiasi');
			$translatePartialLoader.addPart('vhs-award');
			UIControlService.loadLoading("MESSAGE.LOADING");

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			vm.flagExcel = false;
			getUserLogin();
			jLoad(1);
			//convertToDate();
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('penetapan-pemenang-vhs-fpa-print', {
				TenderRefID: vm.TenderRefID,
				StepID: vm.StepID,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.awardReport = awardReport;
		function awardReport(vendorId) {
			$state.transitionTo('award-report', { TenderStepDataID: vm.StepID, ProcPackType: vm.ProcPackType, VendorID: vendorId });
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			var tender = {
				column: vm.StepID,
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}

			PenetapanPemenangVHSservice.select(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.detail = reply.data.List;

					for (var i = 0; i < vm.detail.length; i++) {
						loadStoreLoc(vm.detail[i])
						loadTaxCode(vm.detail[i])
						if (vm.detail[i].ApprovalStatusReff == null) {
							vm.detail[i].Status = 'Draft'
							vm.detail[i].flagTemp = 0
						} else if (vm.detail[i].ApprovalStatusReff != null) {
							//cekEmployee(vm.detail[i].ID, vm.detail[i]);
							vm.detail[i].Status = vm.detail[i].ApprovalStatusReff.Value;
							if (vm.detail[i].ApprovalStatusReff.Name == "CR_APPROVED") vm.flagExcel = true;

							vm.detail[i].flagTemp = 1;
						}
						vm.detail[i].StartContractDate = new Date(Date.parse(vm.detail[i].StartContractDate));
						vm.detail[i].ExpDate = new Date(Date.parse(vm.detail[i].ExpDate));
					}
					vm.count = reply.data.Count;
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});

			PenetapanPemenangVHSservice.SelectStepAdmin(tender, function (reply) {
				vm.step = reply.data;
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
			});
		}

		vm.sendToApproveL1L2 = sendToApproveL1L2
		function sendToApproveL1L2(data) {
			vm.ID = data.ID;
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV_L1_L2'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					var dt = {
						TenderStepDataId: vm.StepID,
						VendorID: data.VendorID
					};
					ApprovalNoSAPService.sendToApproveL1L2(dt, function (reply) {
						if (reply.status === 200) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
					});
				}
			});
		}

		vm.detailApprovalL1L2 = detailApprovalL1L2;
		function detailApprovalL1L2(dt) {
			var item = {
				TenderStepDataId: vm.StepID,
				VendorID: dt.VendorID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-no-SAPcode/approval-no-SAPcode.modal.html',
				controller: 'detailApprovalNoSAPCtrl',
				controllerAs: 'detailApprovalNoSAPCtrl',
				resolve: { item: function () { return item } }
			})
			modalInstance.result.then(function (ID, filter, flag) {
				init()
			})
		}

		vm.openSummary = openSummary
		function openSummary(data) {
			var item = {
				Summary: data.Summary,
				VendorName: data.VendorName,
				isReadOnly: !vm.isAllowedEdit || !(data.Status === 'Draft' || data.Status === 'REJECT'),
				ID: data.ID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-vhs-fpa/vhsfpaSummary.modal.html',
				controller: 'vhsfpaSummaryController',
				controllerAs: 'vhsfpaSummaryController',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function (summary) {
				data.Summary = summary;
			});
		}

		vm.loadStoreLoc = loadStoreLoc;
		function loadStoreLoc(data) {
			PenetapanPemenangVHSservice.loadStoreLoc(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listStoreLoc = reply.data;
					for (var x = 0; x < reply.data.length; x++) {
						if (data.StorageLocation == reply.data[x].ID) {
							data.selectStoreLoc = reply.data[x];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Penetapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadTaxCode = loadTaxCode;
		function loadTaxCode(data) {
			PenetapanPemenangVHSservice.selectTaxCode(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listTaxCode = reply.data;
					for (var x = 0; x < reply.data.length; x++) {
						if (data.TaxCode == reply.data[x].ID) {
							data.selectTaxCode = reply.data[x];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.getUserLogin = getUserLogin;
		function getUserLogin() {
			PenetapanPemenangVHSservice.CekRequestor({
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.flagSave = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.createExcel = createExcel;
		function createExcel() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			if (vm.detail[0].RFQTypeName == "FPA") {
				var tender = {
					column: vm.StepID,
					Status: vm.TenderRefID,
					FilterType: vm.ProcPackType
				}
				PenetapanPemenangVHSservice.excelvendorFPA(tender, function (reply) {
					if (reply.status === 200) {
						vm.vendor = reply.data;
						UIControlService.unloadLoading();
						var data = {
							vendor: vm.vendor
						}
						var modalInstance = $uibModal.open({
							templateUrl: 'app/modules/panitia/penetapan-pemenang-vhs-fpa/SaveExcelNotif.html?v=1.00003',
							controller: "SaveNotifExcel",
							controllerAs: "SaveNotifExcel",
							resolve: {
								item: function () {
									return data;
								}
							}
						});
						modalInstance.result.then(function () {
							init();
						});
					} else {
						$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
						UIControlService.unloadLoading();
					}
				}, function (err) {
					console.info("error:" + JSON.stringify(err));
					//$.growl.error({ message: "Gagal Akses API >" + err });
					UIControlService.unloadLoading();
				});
			} else {
				var tender = {
					column: vm.StepID,
					Status: vm.TenderRefID,
					FilterType: vm.ProcPackType
				}
				PenetapanPemenangVHSservice.excelvendorVHS(tender, function (reply) {
					if (reply.status === 200) {
						vm.vendor = reply.data;
						UIControlService.unloadLoading();
						var data = {
							vendor: vm.vendor
						}
						var modalInstance = $uibModal.open({
							templateUrl: 'app/modules/panitia/penetapan-pemenang-vhs-fpa/SaveExcelNotif.html?v=1.00003',
							controller: "SaveNotifExcel",
							controllerAs: "SaveNotifExcel",
							resolve: {
								item: function () {
									return data;
								}
							}
						});
						modalInstance.result.then(function () {
							init();
						});
					} else {
						$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
						UIControlService.unloadLoading();
					}
				}, function (err) {
					console.info("error:" + JSON.stringify(err));
					//$.growl.error({ message: "Gagal Akses API >" + err });
					UIControlService.unloadLoading();
				});
			}

		}

		vm.cekEmployee = cekEmployee;
		function cekEmployee(Id, reff) {
			PenetapanPemenangVHSservice.CekEmployee({
				ID: Id
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					reff.flagEmp = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};
		vm.openCalendar1 = openCalendar1;
		function openCalendar1(index) {
			vm.isCalendarOpened1[index] = true;
		};

		function convertAllDateToString(data) { // TIMEZONE (-)
			if (data) {
				data = UIControlService.getStrDate(data);
			}
		};


		//supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.detail[0].StartContractDate) {
				vm.detail[0].StartContractDate = new Date(Date.parse(vm.detail[0].StartContractDate));
			}
		}




		vm.simpan = simpan;
		vm.List = [];
		function simpan() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			for (var i = 0; i < vm.detail.length; i++) {
				convertAllDateToString(vm.detail[i].StartContractDate);

				var dataVHS = {
					ID: vm.detail[i].ID,
					durrA: vm.detail[i].durrA,
					durrB: vm.detail[i].durrB,
					VendorID: vm.detail[i].VendorID,
					TaxCode: vm.detail[i].selectTaxCode == undefined ? null : vm.detail[i].selectTaxCode.ID,
					Duration: vm.detail[i].Duration,
					Summary: vm.detail[i].Summary,
					TenderStepID: vm.detail[i].TenderStepID,
					StartContractDate: vm.detail[i].StartContractDate,
					SAPContractNo: vm.detail[i].SAPContractNo,
					RFQVHSId: vm.TenderRefID,
					RFQType: vm.detail[i].RFQType,
					StorageLocationID: vm.detail[i].selectStoreLoc == undefined ? null : vm.detail[i].selectStoreLoc.ID
				}
				vm.List.push(dataVHS);
				if (i == vm.detail.length - 1 && vm.List.length === vm.detail.length) {
					PenetapanPemenangVHSservice.update(vm.List, function (reply) {
						UIControlService.unloadLoadingModal();
						if (reply.status === 200) {
							vm.ListDeal = [];
							vm.flag = false;
							UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
							//init();
							window.location.reload();
						} else {
							UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
							return;
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoadingModal();
					});
				}
			}

		}

		function sendMail(data) {
			PenetapanPemenangVHSservice.sendMail({ Status: vm.ID, FilterType: 0 },
                function (response) {
                	UIControlService.unloadLoading();
                	if (response.status == 200) {
                		UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT");
                		return;
                	}
                }, function (response) {
                	UIControlService.unloadLoading();
                });
		}


		vm.sendToApprove = sendToApprove;
		function sendToApprove(data) {
			bootbox.confirm($filter('translate')('MESSAGE.WANT_SENT'), function (yes) {
				if (yes) {
					vm.ID = data.ID;
					UIControlService.loadLoading("MESSAGE.LOADING");
					var dt = {
						ID: data.ID,
						TenderStepID: vm.StepID,
						flagEmp: 1
					};
					PenetapanPemenangVHSservice.SendApproval(dt, function (reply) {
						if (reply.status === 200) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							sendMail();
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
					});
				}
			});
		}
		//vm.simpan = simpan;
		//vm.List = [];
		//function simpan() {
		//    UIControlService.loadLoadingModal("Silahkan Tunggu...");

		//    for (var i = 0; i < vm.detail.length; i++) {
		//        //if (vm.detail[i].SAPContractNo === null) {
		//        //    UIControlService.msg_growl("warning", "No Kontrak SAP belum diisi !!");
		//        //    return;
		//        //}
		//        vm.detail[i].StartContractDate = UIControlService.getStrDate(vm.detail[i].StartContractDate);

		//        //if (vm.detail[i].StartContractDate === null) {

		//        //    UIControlService.msg_growl("warning", "Tanggal Mulai Kontrak belum diisi !!");
		//        //    return;
		//        //}
		//        //if (vm.detail[i].Duration === null) {

		//        //    UIControlService.msg_growl("warning", "Durasi Kontrak belum diisi !!");
		//        //    return;
		//        //}
		//        //if (vm.detail[i].durrA === null || vm.detail[i].durrB === null) {

		//        //    UIControlService.msg_growl("warning", "Durasi Kontrak PTVI belum diisi !!");
		//        //    return;
		//        //}
		//        var dataGoods = {
		//            ID: vm.detail[0].ID,
		//            durrA: vm.detail[0].durrA,
		//            durrB: vm.detail[0].durrB,
		//            VendorID: vm.detail[0].VendorID,
		//            Duration: vm.detail[0].Duration,
		//            TenderStepID: vm.detail[0].TenderStepID,
		//            StartContractDate: vm.detail[0].StartContractDate,
		//            SAPContractNo: vm.detail[0].SAPContractNo,
		//            RFQVHSId: vm.TenderRefID,
		//            RFQType: vm.detail[0].RFQType

		//        }
		//        vm.List.push(dataGoods);
		//        if (i == vm.detail.length - 1 && vm.List.length === vm.detail.length) {

		//            PenetapanPemenangVHSservice.update(vm.List, function (reply) {
		//                // UIControlService.unloadLoadingModal();
		//                if (reply.status === 200) {
		//                    UIControlService.msg_growl("success", "Berhasil Simpan Data Pemenang VHS/FPA!!");
		//                    init();
		//                }
		//                else {
		//                    UIControlService.msg_growl("error", "Gagal menyimpan data!!");
		//                    return;
		//                }
		//            }, function (err) {
		//                UIControlService.msg_growl("error", "Gagal Akses Api!!");
		//                //UIControlService.unloadLoadingModal();
		//            });
		//        }
		//    }
		//}


		vm.edit = edit;
		function edit(dataTabel) {
			console.info(dataTabel);
			if (dataTabel.DocumentUrl !== null) {
				var data = {
					act: true,
					item: dataTabel,
					reff: vm.TenderRefID,
					StepID: vm.StepID

				}
			} else {

				var data = {
					act: false,
					item: dataTabel,
					reff: vm.TenderRefID,
					StepID: vm.StepID
				}
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-vhs-fpa/ubah-data.html',
				controller: 'UbahPPGVHSCtrl',
				controllerAs: 'UbahPPGVHSCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.Export = Export;
		function Export(tableId) {
			vm.exportHref = Excel.tableToExcel(tableId, 'sheet name');
			$timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
		}

		vm.detailApproval = detailApproval;
		function detailApproval(dt, data) {
			var item = {
				ID: data.ID,
				flag: data.flagEmp,
				Status: dt
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-vhs-fpa/detailApproval.modal.html',
				controller: 'detailApprovalVHSCtrl',
				controllerAs: 'detailApprovalVHSCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function () {
				init();
			});
		};

		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
			//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
			var arrData = JSONData;
			console.info(arrData[0]);
			var CSV = '';
			//Set Report title in first row or line

			CSV += ReportTitle + '\r\n\n';

			//This condition will generate the Label/Header
			if (ShowLabel) {
				var row = "";

				//This loop will extract the label from 1st index of on array
				for (var index in arrData[0]) {

					//Now convert each value to string and comma-seprated
					row += index + ',';
				}

				//row = row.slice(0, -1);
				//console.info(row);
				////append Label row with line break
				//CSV += row + '\r\n';
				//console.info(CSV);
			}

			////1st loop is to extract each row
			//for (var i = 0; i < arrData.length; i++) {
			//    var row = "";

			//    //2nd loop will extract each column and convert it in string comma-seprated
			//    for (var index in arrData[i]) {
			//        row += '"' + arrData[i][index] + '",';
			//    }

			//    row.slice(0, row.length - 1);

			//    //add a line break after each row
			//    CSV += row + '\r\n';
			//}

			//if (CSV == '') {        
			//    alert("Invalid data");
			//    return;
			//}   

			////Generate a file name
			//var fileName = "MyReport_";
			////this will remove the blank-spaces from the title and replace it with an underscore
			//fileName += ReportTitle.replace(/ /g,"_");   

			////Initialize file format you want csv or xls
			//var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

			//// Now the little tricky part.
			//// you can use either>> window.open(uri);
			//// but this will not work in some browsers
			//// or you will not get the correct file extension    

			////this trick will generate a temp <a /> tag
			//var link = document.createElement("a");    
			//link.href = uri;

			////set the visibility hidden so it will not effect on your web-layout
			//link.style = "visibility:hidden";
			//link.download = fileName + ".csv";

			////this part will append the anchor tag and remove it after automatic click
			//document.body.appendChild(link);
			//link.click();
			//document.body.removeChild(link);
		}


	}
})();
//TODO

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}

