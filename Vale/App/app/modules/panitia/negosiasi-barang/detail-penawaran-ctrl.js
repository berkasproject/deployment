﻿(function () {
    'use strict';

    angular.module("app").controller("DPCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiBarangService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.VendorID = Number($stateParams.VendorID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.StepID = Number($stateParams.StepID);
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.maxSize = 10;
        vm.init = init;
        vm.flagIsOpen = true;
        vm.jLoad = jLoad;
        vm.nego = [];

        function init() {
            $translatePartialLoader.addPart('negosiasi-barang');
            loadOvertime();
            jLoad(1);
        }

        vm.loadOvertime = loadOvertime;
        function loadOvertime() {
            NegosiasiBarangService.isOvertime({ ID: vm.StepID }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.overtime = reply.data;
                    if (vm.overtime == false) vm.flagIsOpen = false;
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    //UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                //UIControlService.unloadLoading();
            });
        }

        //tampil detail penawaran
        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
            vm.nego = [];
            vm.currentPage = current;
           var offset = (current * 10) - 10;
           var tender = {
              VendorID: vm.VendorID,
              TenderStepDataID: vm.StepID,
              Offset: offset,
              Limit: vm.pageSize
           }
           UIControlService.loadLoading("");
            NegosiasiBarangService.itemall(tender, function (reply) {
              //console.info("data:" + JSON.stringify(reply));
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.nego = reply.data;
                    vm.totalItems = vm.nego.length;
                    vm.Id = vm.nego[0].GoodsNegoId;
                    vm.TenderID = vm.nego[0].tender.TenderID;
                    vm.TenderRefID = vm.nego[0].tender.tender.TenderRefID;
                    vm.ProcPackType = vm.nego[0].tender.tender.ProcPackageType;
                    vm.generet = vm.nego[0].IsGenerate;
                    vm.totalItems = vm.nego.length;
                    vm.IsOpenAll = false;
                    vm.totalHistorical = 0;
                    vm.totalEPV = 0;
                    var sameCurrenciesHistorical = true;
                    var sameCurrenciesEPV = true;
                    for (var i = 0; i < vm.nego.length; i++) {
                        vm.nego[i].TotalPriceGOE = vm.nego[i].UnitPriceGOE * vm.nego[i].Quantity;
                        if (i === 0) {
                            vm.cost = vm.nego[i].UnitPriceGOE * vm.nego[i].Quantity;
                            vm.totalNego = vm.nego[i].TotalPrice;
                            if (vm.nego[i].IsNego == true || vm.nego[i].IsActive === false) vm.flagIsOpen = false;
                        }
                        else {
                            vm.cost = +vm.cost + +(vm.nego[i].UnitPriceGOE * vm.nego[i].Quantity);
                            vm.totalNego = +vm.totalNego + +vm.nego[i].TotalPrice;
                        }

                        if (i !== 0) {
                            if (vm.nego[i].HistoricalCurrency !== vm.nego[i - 1].HistoricalCurrency) {
                                sameCurrenciesHistorical = false;
                            }
                            if (vm.nego[i].EPVCurrency !== vm.nego[i - 1].EPVCurrency) {
                                sameCurrenciesEPV = false;
                            }
                        }
                        if (sameCurrenciesHistorical) {
                            vm.totalHistorical += vm.nego[i].HistoricalTotalPrice;
                        } else {
                            vm.totalHistorical = 0;
                        }
                        if (sameCurrenciesEPV) {
                            vm.totalEPV += vm.nego[i].EPVTotalPrice;
                        } else {
                            vm.totalEPV = 0;
                        }
                    }
                    vm.selisih = vm.cost - vm.totalNego;
                    vm.presentase = ((vm.selisih / vm.cost) * 100).toFixed(2);
                    checkIsOpen();
                    UIControlService.unloadLoading();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data detail penawaran" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.checkIsOpen = checkIsOpen;
        function checkIsOpen() {
            vm.IsOpenAll = false;
            //console.info("jml:" + JSON.stringify(vm.totalItems));
            var isOpenTrue = 0;
            for (var i = 0; i < vm.nego.length; i++) {
                if (vm.nego[i].IsOpen === true || !vm.nego[i].GOEId) {
                    isOpenTrue = isOpenTrue + 1;
                }
            }
            if (isOpenTrue === vm.totalItems) {
                vm.IsOpenAll = true;
            }
        }
        
        vm.loadv = loadv;
        function loadv() {
            //console.info("curr "+current)
            vm.lv = [];
            //vm.currentPage = current;
           // var offset = (current * 10) - 10;
            NegosiasiBarangService.select({
                column: vm.StepID,
                status: vm.TenderRefID,
                FilterType: vm.ProcPackType
            }, function (reply) {
                //console.info("data:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.lv = reply.data;
                    //console.info("data:" + JSON.stringify(vm.nb));
                    //ambil data isgenerate
                    for (var i = 0; i < vm.lv.length; i++) {
                        if (vm.lv[i].ID === vm.Id && vm.lv[i].VendorID === vm.VendorID) {
                            vm.generet = vm.lv[i].IsGenerate;
                            vm.harga = vm.lv[i].NegotiationPrice;
                        }
                    }
                    //console.info("ini:" + JSON.stringify(vm.generet));
                    //console.info("ini:" + JSON.stringify(vm.harga));
                    //console.info("ini:" + JSON.stringify(vm.vendor));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
               // //console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        
        vm.save = save;
        function save() {
            vm.list = [];
            for (var i = 0; i < vm.nego.length; i++) {
                if (vm.nego[i].GOEId) {
                    if (vm.nego[i].ID !== 0) {
                        var dta = {
                            IsOpen: vm.nego[i].IsOpen,
                            ID: vm.nego[i].ID,
                            ItemPRId: vm.nego[i].ItemPRId,
                            GOEId: vm.nego[i].GOEId,
                            UnitPrice: vm.nego[i].UnitPrice,
                            GoodsNegoId: vm.nego[i].GoodsNegoId,
                            Quantity: vm.nego[i].Quantity
                        };
                    }
                    else {
                        var dta = {
                            IsOpen: vm.nego[i].IsOpen,
                            ID: vm.nego[i].ID,
                            ItemPRId: vm.nego[i].ItemPRId,
                            GOEId: vm.nego[i].GOEId,
                            UnitPrice: vm.nego[i].UnitPrice,
                            GoodsNegoId: vm.nego[i].GoodsNegoId,
                            Quantity: vm.nego[i].Quantity
                        };
                    }
                    vm.list.push(dta);
                }
            }
            NegosiasiBarangService.updatedetail(vm.list,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
                       //   $uibModalInstance.close();
                       init();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                  // UIControlService.unloadLoadingModal();
               }
          );
        }
        vm.back = back;
        function back() {
            if (vm.nego[0].IsNego == true)
                $state.transitionTo('negosiasi-barang', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, StepID: vm.StepID });
            else {
                $state.transitionTo('negosiasi-barang-chat', { StepID: vm.StepID, VendorID: vm.VendorID });
            }
        }

        vm.saveIsOpen = saveIsOpen;
        function saveIsOpen(data) {
            //console.info("data:" + JSON.stringify(data));
            var param = {
                IsOpen: data.IsOpen,
                ID: data.ID,
                ItemPRId: data.ItemPRId,
                GOEId: data.GOEId,
                UnitPrice: data.UnitPrice,
                GoodsNegoId: data.GoodsNegoId,
                Quantity: data.Quantity
            };
            vm.savedata = [];
            vm.savedata.push(param);

            NegosiasiBarangService.updatedetail(vm.savedata,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
                       init();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
               }
          );


        }

        vm.loadOpenAll = loadOpenAll;
        function loadOpenAll() {
            //tanpa paging
            //console.info("openall?" + JSON.stringify(vm.IsOpenAll));
            var countPaging = vm.nego.length;
            var countItemInPage = vm.totalItems - countPaging;
            var startIndex = (vm.currentPage - 1) * 10;
            if (vm.IsOpenAll === true) {
                //console.info("bnr");
                for (var i = 0; i < vm.totalItems; i++) {
                     vm.nego[i].IsOpen = true;
                }
            }
            else if (vm.IsOpenAll === false) {
                //console.info("salah");
                for (var i = 0; i < vm.totalItems; i++) {
                    vm.nego[i].IsOpen = false;
                }
            }
        }


        vm.generate = generate;
        function generate() {
            NegosiasiBarangService.update({
                ID: vm.Id,
                IsGenerate: vm.generet
            },
            function (reply) {
                //UIControlService.loadLoading("Silahkan Tunggu...");
                //console.info("gnrt:" + JSON.stringify(reply.status));
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
                    init();
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                    return;
                }
            },
            function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            }
            );
            
            }
        
    }
})();
//TODO


