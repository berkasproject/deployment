﻿(function() {
    'use strict';

    angular.module("app")
            .controller("KonfigEmailCtrl", ctrl);

    ctrl.$inject = ['$http', '$state', '$translate', '$translatePartialLoader', '$uibModal', 'SocketService', 'KontenEmailService', 'UIControlService'];
    function ctrl($http, $state, $translate, $translatePartialLoader, $uibModal, SocketService, KontenEmailService, UIControlService) {

        var vm = this;
        
        vm.konten_emails = [];
        vm.filter = '';
        vm.count = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('konfig-email');
            loadKontenEmails();
        };

        vm.onPageChanged = onPageChanged;
        function onPageChanged(currentPage){
            vm.currentPage = currentPage;
            loadKontenEmails();
        };

        vm.onFilterChanged = onFilterChanged;
        function onFilterChanged() {
            vm.currentPage = 1;
            loadKontenEmails();
        };

        vm.onEditClick = onEditClick;
        function onEditClick(konten_email){
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/konfig-email/editEmail.modal.html',
                controller: 'EditEmailCtrl',
                controllerAs: 'editEmailCtrl',
                resolve: {
                    item: function() {
                        return {
                            ID: konten_email.ID,
                            Name: konten_email.Name,
                            EmailContent1: konten_email.EmailContent1,
                            EmailSubject: konten_email.EmailSubject
                        };
                    }
                }
            });
            modalInstance.result.then(function() {
                loadKontenEmails();
            });  
        };

        vm.onConfigVariablesClick = onConfigVariablesClick;
        function onConfigVariablesClick(){
            $state.transitionTo("email-configuration-variables");
        };
        
        /*
        vm.onEditSignatureClick = onEditSignatureClick;
        function onEditSignatureClick(){
            var modalInstance = $modal.open({
                templateUrl: 'editSignature.html',
                controller: editSignatureCtrl
            });
            modalInstance.result.then(function() {
                loadKontenEmails();
            });  
        };
        */
        
        function loadKontenEmails() {
            UIControlService.loadLoading("LOADING");
            KontenEmailService.select({
                Keyword: vm.filter,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.konten_emails = reply.data.List;
                    vm.count = reply.data.Count;
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "ERR_LOAD_EMAILS");
                return;
            });
        }
    }
})();

//TODO


/*
var editSignatureCtrl = function($scope, $modalInstance, $http, $cookieStore, $rootScope) {
    
    $scope.variabel_email_global = {};
    $scope.signature;
    
    $scope.init = function() {
        $rootScope.authorize(
            //itp.mailconfig.getVariableById
            $http.post($rootScope.url_api + 'mailconfig/var/selectbyid',
            {
                id_variabel_global: 1
            }).success(function(reply){
                if (reply.status === 200){
                    $scope.variabel_email_global = reply.result.data.variabel_email_global;
                    $scope.variabel_email_global.namaAwal = reply.result.data.variabel_email_global.nama;
                    tinymce.get('signature').setContent(reply.result.data.variabel_email_global.nilai);
                } else {
                    $.growl.error({title: "[WARNING]", message: "Data signature gagal diload"});
                }
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            })
        );
    };
    
    $scope.onCancelClick = function() {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.onSimpanClick = function() {
        $rootScope.authorize(
            //itp.mailconfig.updateVariable
            $http.post($rootScope.url_api + 'mailconfig/varlok/update',
            {
                variabel_email_global:$scope.variabel_email_global
            }).success(function(reply){
                if (reply.status === 200){
                    $.growl.notice({title: "[INFO]", message: "Signature email berhasil diubah"});
                    $modalInstance.close();
                } else {
                    $.growl.error({title: "[WARNING]", message: "Signature email gagal diubah"});
                }
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            })
        );
    };
    
    $scope.customTinymce = {
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
        toolbar2: "forecolor backcolor",
        image_advtab: true,
        height: "200px",
        width: "auto"
    };    
};*/