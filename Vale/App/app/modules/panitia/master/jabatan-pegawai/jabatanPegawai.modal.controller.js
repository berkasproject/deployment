﻿(function () {
	'use strict';

	angular.module("app").controller("JabatanPegawaiModalCtrl", ctrl);

	ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'JabatanPegawaiService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance'];

	function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, JabatanPegawaiService,
		RoleService, UIControlService, item, $uibModalInstance) {
		var vm = this;
		//console.info("masuuk modal : " + JSON.stringify(item));
		vm.isAdd = item.act;
		vm.action = "";
		vm.IDJabatan= 0;
		vm.NamaJabatan = "";
		vm.KodeJabatan = "";
		
		vm.init = init;
		function init() {
		    if (vm.isAdd === 1) {
		        vm.action = "Tambah";
		    }
		    else {
		        vm.action = "Ubah";
		        vm.KodeJabatan = item.item.PositionCode;
		        vm.NamaJabatan = item.item.PositionName;
		        vm.IDJabatan = item.item.PositionID;
		    }
		}

		vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
		};

		vm.simpan = simpan;
		function simpan() {
		    //console.info(vm.KodeJabatan + "-" + vm.NamaJabatan + "-" );
		    if (vm.KodeJabatan === "" || vm.KodeJabatan === null) {
		        UIControlService.msg_growl("warning", "MESSAGE.NO_CODE");
		        return;
		    }

		    if (vm.NamaJabatan === "" || vm.NamaJabatan === null) {
		        UIControlService.msg_growl("warning", "MESSAGE.NO_NAME");
		        return;
		    }
		    cekKodeNama();
		}

	    //proses simpan
		vm.prosesSimpan = prosesSimpan;
        function prosesSimpan(){
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
		    if (vm.isAdd === 1) {
		        JabatanPegawaiService.insert({
		            PositionCode: vm.KodeJabatan, PositionName: vm.NamaJabatan
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
		                $uibModalInstance.close();
		            }
		            else {
		                alert($filter('translate')('MESSAGE.ERR_SAVE'));
		                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
		                return;
		            }
		        }, function (err) {
		            console.info("error:" + JSON.stringify(err));
		            alert($filter('translate')('MESSAGE.API'));
		            UIControlService.unloadLoading();
		        });
		    }
		    else {
		        JabatanPegawaiService.update({
		            PositionCode: vm.KodeJabatan, PositionName: vm.NamaJabatan, PositionID: vm.IDJabatan
		        }, function (reply) {
		            UIControlService.unloadLoading();
		            if (reply.status === 200) {
		                alert($filter('translate')('MESSAGE.SUCC_UPDATE'));
		                $uibModalInstance.close();
		            }
		            else {
		                alert($filter('translate')('MESSAGE.ERR_UPDATE'));
		                return;
		            }
		        }, function (err) {
		            console.info("error:" + JSON.stringify(err));
		            alert($filter('translate')('MESSAGE.API'));
		            UIControlService.unloadLoading();
		        });
		    }
		}

		vm.cekKodeNama = cekKodeNama;
		function cekKodeNama() {
		    //pengecekkan kode atau nama sudah ada belum?
		    JabatanPegawaiService.cekData({
		        column: 1, Keyword: vm.KodeJabatan
		    }, function (reply) {
		        console.info("cek1:" + JSON.stringify(reply));
		        if (reply.status === 200 && reply.data.length > 0) {
		            alert($filter('translate')('MESSAGE.ERR_CODE'));
		            return;
		        }
		        else if (reply.status === 200 && reply.data.length <= 0) {
		            JabatanPegawaiService.cekData({
		                column: 2, Keyword: vm.NamaJabatan
		            }, function (reply2) {
		                //console.info("cek2:" + JSON.stringify(reply2));
		                if (reply2.status === 200 && reply2.data.length > 0) {
		                    alert($filter('translate')('MESSAGE.ERR_NAME'));
		                    return;
		                }
		                else if (reply2.status === 200 && reply2.data.length <= 0) {
		                    prosesSimpan();
		                }
		                else {
		                    alert($filter('translate')('MESSAGE.ERR_CHECK'));
		                    return;
		                }
		            }, function (err) {
		                console.info("error:" + JSON.stringify(err));
		                alert($filter('translate')('MESSAGE.API'));
		                UIControlService.unloadLoading();
		            });
		        }
		        else {
		            alert($filter('translate')('MESSAGE.ERR_CHECK'));
		            return;
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        alert($filter('translate')('MESSAGE.API'));
		        UIControlService.unloadLoading();
		    });
		}

	}
})();