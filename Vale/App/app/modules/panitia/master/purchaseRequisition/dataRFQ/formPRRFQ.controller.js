﻿(function () {
	'use strict';

	angular.module("app").controller("formPRCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PurchaseRequisitionService', '$state', 'UIControlService', '$uibModal', '$stateParams', '$filter'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PurchReqService, $state, UIControlService, $uibModal, $stateParams, $filter) {
		var vm = this;
		vm.RFQID = Number($stateParams.RFQID);
		vm.flag = Number($stateParams.flag);
		vm.maxSize = 10;
		vm.currentPage = 0;
		vm.listStepTender = [];
		vm.listComodity = [];
		vm.listClasification = [];
		vm.listTypeTender = [];
		vm.listOptionsTender = [];
		vm.viewVendorChoose = [];
		vm.checkAreaKomoditi = false;
		vm.checkDaftarVendor = false;
		vm.isAdd;
		vm.dataForm = {
			IsByArea: false,
			IsByVendor: false,
			IsLocal: false,
			IsNational: false,
			IsInternational: false
		};
		vm.detailCE = [];
		vm.init = init();
		vm.itemlistPR = [];
		vm.cekViewVendor = false;
		vm.selectedState;
		vm.listState = [];
		function init() {
			vm.dataForm.Emails = "";
			$translatePartialLoader.addPart("purchase-requisition");
			getEmailTemplate();
			if (vm.RFQID > 0) {
				PurchReqService.selectPRRFQByID({ ID: vm.RFQID }, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						var data = reply.data;
						vm.dataForm = data;
						if (vm.dataForm.IsByArea === true) {
							vm.viewVendorChoose = vm.dataForm.Vendors;
							vm.checkAreaKomoditi = true;
						}
						if (vm.dataForm.IsByVendor === true) {
							vm.listVendortoView = vm.dataForm.Vendors;
							vm.checkDaftarVendor = true;
							vm.listVendor = vm.dataForm.Vendors;

						}
						vm.listStepTender = vm.dataForm.RFQGoodsSteps;
						for (var a = 0; a < vm.listStepTender.length; a++) {
							vm.listStepTender[a]['TenderStepName'] = vm.listStepTender[a].MstTenderStep.FormTypeName;
							vm.listStepTender[a].StartDate = new Date(Date.parse(vm.listStepTender[a].StartDate));
							vm.listStepTender[a].EndDate = new Date(Date.parse(vm.listStepTender[a].EndDate));
						}
						for (var b = 0; b < vm.dataForm.RFQGoodsItemPRs.length; b++) {
							vm.itemlistPR.push(vm.dataForm.RFQGoodsItemPRs[b].ItemPR);
						}
						//console.info("itemPR:: " + JSON.stringify(vm.itemPRChecked));
						itemcombo();
					}
				}, function (err) {
					//UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
				loadCommitte(1);
			}
			else {
				//console.info(vm.dataForm);
				vm.disableEmail = true;
				itemcombo();
				generatekode();
			}

		}

		vm.loadVendor = loadVendor;
		function loadVendor() {
			PurchReqService.viewVendor({
				CommodityID: vm.dataForm.CommodityID,
				IsLocal: vm.dataForm.IsLocal,
				IsNational: vm.dataForm.IsNational,
				IsInternational: vm.dataForm.IsInternational,
				CompScale: vm.dataForm.CompScale,
				contactVendor: [],
				Keyword: ""
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listVendorView = reply.data.List;
					//console.info(vm.dataForm.Vendors);
					for (var i = 0; i < vm.dataForm.Vendors.length; i++) {
						for (var x = 0; x < vm.listVendorView.length; x++) {
							if (vm.dataForm.Vendors[i].VendorID == vm.listVendorView[x].VendorID) {
								vm.listVendorView[x].IsCheck = true;
								if (vm.dataForm.IsByArea == true) vm.viewVendorChoose.push(vm.listVendorView[x]);
								else vm.listVendor.push(vm.listVendorView[x]);
							}

						}
					}
				}
			}, function (err) {
			});

		}

		vm.loadCommitte = loadCommitte;
		function loadCommitte(current) {
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			PurchReqService.selectcommite({
				Offset: offset,
				Limit: 10,
				Status: vm.RFQID,
				FilterType: vm.flagEmp
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.detailCE = reply.data.List;
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.getEndDate = getEndDate;
		function getEndDate(elementAt) {
			/*
			for (var i = elementAt; i < vm.listStepTender.length; i++) {
				vm.listStepTender[i].EndDate = UIControlService.getStrDate(UIControlService.getEndDateByWeekday(vm.listStepTender[i].StartDate, parseInt(vm.listStepTender[i].Duration)));
				if (vm.listStepTender[1 + i]) {
					vm.listStepTender[1 + i].StartDate = UIControlService.getStrDate(UIControlService.getEndDateByWeekday(vm.listStepTender[i].EndDate, 1));
				}
			}*/
			for (var i = elementAt; i < vm.listStepTender.length; i++) {
				var dat = new Date(vm.listStepTender[i].StartDate);
				if (vm.listStepTender[i].Duration === null || vm.listStepTender[i].Duration == 0) {
					dat.setDate(dat.getDate() + 0);
				}
				else {
					for (var j = 1; j <= vm.listStepTender[i].Duration; j++) {
						//console.info(j);
						dat.setDate(dat.getDate() + parseInt(1));
						var dateTemp = dat;
						var day_dateTemp = dateTemp.getDay();
						if (day_dateTemp === 6) {
							dat.setDate(dat.getDate() + 2);
						}
						else if (day_dateTemp === 0) {
							dat.setDate(dat.getDate() + 1);
						}
						else {
							dat.setDate(dat.getDate() + 0);
						}
						//console.info(j + "" + dat);
					}
				}
				vm.listStepTender[i].EndDate = UIControlService.getStrDate(dat);
				if (vm.listStepTender[1 + i]) {
					//atur startdate
					dat.setDate(dat.getDate() + 0); //jika startdate tahapan selanjutnya +1hari setelah endate tahapan sebelumnya

					//set startdate, pengecekan hari kerja
					var dateTemp = dat;
					var day_dateTemp = dateTemp.getDay();
					if (day_dateTemp === 6) {
						dat.setDate(dat.getDate() + 2);
					}
					else if (day_dateTemp === 0) {
						dat.setDate(dat.getDate() + 1);
					}

					vm.listStepTender[1 + i].StartDate = UIControlService.getStrDate(dat);
				}
			}
		}

		function generatekode() {
			PurchReqService.generateCode(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var code = reply.data;
					vm.dataForm = new RFQField(0, code, '', '', 0, 0, null, null, null, null, '', 0, 0, 0, 0, 0, null, null, null,
                    null, null, 0, null, 0, 0, 0, 0);
					vm.dataForm.RFQCode = code;
					vm.dataForm.IsByArea = false;
					vm.dataForm.IsByVendor = false;

				}
				else {
					//UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoadingModal();
			});
		}

		function itemcombo() {
			loadKomoditi();
			loadKlasifikasi();
			loadBidderMethod();
			loadDeliveryTerms();
			loadIncoTerms();
			loadSchemaTender();
			loadTypeTender();
			loadOptionsTender();
			loadEvalMethod();
			loadPaymentTerm();
		}

		function selectByID(ID) {
			PurchReqService.selectPRRFQByID({ ID: ID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.dataForm = data;
					//console.info(vm.dataForm);
					if (vm.dataForm.IsByArea === true) { vm.checkAreaKomoditi = true; }
					if (vm.dataForm.IsByVendor === true) { vm.checkDaftarVendor = true; }
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function getEmailTemplate() {
			PurchReqService.getEmail({ Keyword: "Pengumuman Tender Barang", Offset: 0, Limit: 10 }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data.List;
					vm.ListEmailContent = data;
					for (var i = 0; i < vm.ListEmailContent.length; i++) {
						if (vm.ListEmailContent[i].Name == "Pengumuman Tender Barang") vm.EmailContent = vm.ListEmailContent[i].EmailContent1;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.openCalendar = openCalendar;
		vm.isCalendarOpened = [false, false, false, false];
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		vm.openCalendar2 = openCalendar2;
		vm.isCalendarOpened2 = [false, false, false, false];
		function openCalendar2(index) {
			vm.isCalendarOpened2[index] = true;
		};

		//list combo komoditas
		vm.selectedComodity;
		function loadKomoditi() {
			PurchReqService.getCommodity(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listComodity = reply.data;
					if (vm.RFQID > 0) {
						//console.info(vm.dataForm.CommodityID + "..."+JSON.stringify(vm.listComodity));
						for (var i = 0; i < vm.listComodity.length; i++) {
							if (vm.dataForm.CommodityID === vm.listComodity[i].ID) {
								vm.selectedComodity = vm.listComodity[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo klasifikasi
		vm.selectedClasification;
		function loadKlasifikasi() {
			PurchReqService.getClasification(function (reply) {
				UIControlService.unloadLoading();
				vm.listClasification = reply.data.List;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listClasification.length; i++) {
						if (vm.dataForm.CompScale === vm.listClasification[i].RefID) {
							vm.selectedClasification = vm.listClasification[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo jenis penawaran (type tender)
		vm.selectedTypeTender;
		function loadTypeTender() {
			PurchReqService.getTypeTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listTypeTender = reply.data.List;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listTypeTender.length; i++) {
						if (vm.dataForm.TenderType === vm.listTypeTender[i].RefID) {
							vm.selectedTypeTender = vm.listTypeTender[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo tipe penawaran (options tender)
		vm.selectedOptionsTender;
		function loadOptionsTender() {
			PurchReqService.getOptionsTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listOptionsTender = reply.data.List;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listOptionsTender.length; i++) {
						if (vm.dataForm.TenderOption === vm.listOptionsTender[i].RefID) {
							vm.selectedOptionsTender = vm.listOptionsTender[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list options delivery terms
		vm.selectedDeliveryTerms;
		vm.listDeliveryTerms = [];
		function loadDeliveryTerms() {
			PurchReqService.getDeliveryTerms(function (reply) {
				UIControlService.unloadLoading();
				vm.listDeliveryTerms = reply.data.List;

			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo Inco terms
		vm.selectedIncoTerms;
		vm.listIncoTerms = [];
		function loadIncoTerms() {
			PurchReqService.getIncoTerms(function (reply) {
				UIControlService.unloadLoading();
				vm.listIncoTerms = reply.data;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listIncoTerms.length; i++) {
						if (vm.dataForm.IncoTerm === vm.listIncoTerms[i].ID) {
							vm.selectedIncoTerms = vm.listIncoTerms[i];
							changeIncoTerm(vm.dataForm.IncoTerm, 1);
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo metode bidder
		vm.selectedBidderMethod;
		vm.listBidderMethod = [];
		function loadBidderMethod() {
			PurchReqService.getBidderMethod(function (reply) {
				UIControlService.unloadLoading();
				vm.listBidderMethod = reply.data.List;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listBidderMethod.length; i++) {
						if (vm.dataForm.BidderSelMethod === vm.listBidderMethod[i].RefID) {
							vm.selectedBidderMethod = vm.listBidderMethod[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo skema Tender
		vm.selectedSchemaTender;
		vm.listSchemaTender = [];
		function loadSchemaTender() {
			PurchReqService.getProcMethods(function (reply) {
				UIControlService.unloadLoading();
				vm.listSchemaTender = reply.data;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listSchemaTender.length; i++) {
						if (vm.dataForm.ProcMethod === vm.listSchemaTender[i].MethodID) {
							vm.selectedSchemaTender = vm.listSchemaTender[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo evaluasi method
		vm.selectedEvalMethod;
		vm.listEvalMethod = [];
		function loadEvalMethod() {
			PurchReqService.getEvalMethod(function (reply) {
				UIControlService.unloadLoading();
				vm.listEvalMethod = reply.data;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listEvalMethod.length; i++) {
						if (vm.dataForm.EvalMethod === vm.listEvalMethod[i].EvaluationMethodId) {
							vm.selectedEvalMethod = vm.listEvalMethod[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list payment term
		vm.selectedPaymentTerm;
		vm.listPaymentTerm = [];
		function loadPaymentTerm() {
			PurchReqService.getPaymentTerm(function (reply) {
				UIControlService.unloadLoading();
				vm.listPaymentTerm = reply.data;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listPaymentTerm.length; i++) {
						if (vm.dataForm.PaymentTerm === vm.listPaymentTerm[i].Id) {
							vm.selectedPaymentTerm = vm.listPaymentTerm[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list provinsi
		vm.selectedState;
		vm.listState = [];
		function loadStateDelivery() {
			PurchReqService.getStateDelivery(function (reply) {
				UIControlService.unloadLoading();
				vm.listState = reply.data;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listState.length; i++) {
						if (vm.dataForm.DeliveryLocationState === vm.listState[i].FreightCostID) {
							vm.selectedState = vm.listState[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeState = changeState;
		function changeState() {
			loadCityDelivery(vm.selectedState.StateID);
		}

		//list City
		vm.selectedCity;
		vm.listCity = [];
		function loadCityDelivery(IDState) {
			//console.info("state:" + IDState);
			PurchReqService.getCityDelivery({ StateID: IDState }, function (reply) {
				UIControlService.unloadLoading();
				//console.info("city:" + JSON.stringify(reply));
				vm.listCity = reply.data;
				if (vm.RFQID > 0) {
					for (var i = 0; i < vm.listCity.length; i++) {
						if (vm.dataForm.DeliveryLocationCity === vm.listCity[i].CityID) {
							vm.selectedCity = vm.listCity[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//tampilkan tahapan
		vm.getSteps = getSteps;
		function getSteps() {
			if (vm.selectedSchemaTender === undefined) {
				UIControlService.msg_growl("error", 'Skema Tender Harus Dipilih Terlebih Dahulu');
				return;
			} else {
				vm.listStepTender = [];
				//console.info("eval:" + JSON.stringify(vm.selectedSchemaTender));
				UIControlService.loadLoadingModal('Loading Tampilkan Tahapan');
				PurchReqService.getSteps({ ProcMethod: vm.selectedSchemaTender.MethodID }, function (reply) {
					if (reply.status === 200) {
						vm.tenderSteps = reply.data;
						//console.info(vm.tenderSteps);
						var startdate2 = new Date();
						var arrsd = [];
						var arred = [];
						for (var i = 0; i < vm.tenderSteps.length; i++) {

							arrsd[i] = new Date(startdate2.setDate(startdate2.getDate() + 0));
							arred[i] = arrsd[i];

							var step = {
								RFQGoodsID: 0,
								TenderStepID: vm.tenderSteps[i].TenderStepID,
								TenderStepName: vm.tenderSteps[i].TenderStepName,
								StartDate: arrsd[i],
								EndDate: arred[i],
								Duration: vm.tenderSteps[i].Duration
							}
							vm.listStepTender.push(step);
						}
						UIControlService.unloadLoadingModal();
					} else {
						UIControlService.unloadLoadingModal();
						UIControlService.msg_growl("error", 'NOTIFICATION.GET.STEPS.ERROR', "NOTIFICATION.GET.STEPS.TITLE");
					}
				}, function (err) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.STEPS.ERROR', "NOTIFICATION.GET.STEPS.TITLE");
				})
			}
		}

		/*open form pilih item PR*/
		vm.openItemPR = openItemPR;
		vm.itemPRChecked = [];
		function openItemPR(flag) {
			if (!vm.selectedComodity) {
				UIControlService.msg_growl("error", 'Komoditi harus dipilih terlebih dahulu');
				return;
			}
			var data = {
				item: vm.itemlistPR,
				CommodityID: vm.selectedComodity ? vm.selectedComodity.ID : 0,
				CommodityName: vm.selectedComodity ? vm.selectedComodity.Name : null,
				flag: flag,
				RFQId: vm.RFQID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataRFQ/itemPR.html?v=1.000002',
				controller: 'ItemPRCtrl',
				controllerAs: 'ItemPRCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (dataitem) {
				vm.itemlistPR = dataitem;
				//console.info(vm.itemlistPR);
			});
		}

		/*tampilkan vendor*/
		vm.viewVendor = viewVendor;
		function viewVendor(data) {
			if (!vm.dataForm.IsLocal) {
				for (var i = 0; i < vm.viewVendorChoose.length; i++) {
					if (vm.viewVendorChoose[i].IsLocal === 1) {
						vm.viewVendorChoose.splice(i, 1);
						i--;
					}
				}
			}
			if (!vm.dataForm.IsNational) {
				for (var i = 0; i < vm.viewVendorChoose.length; i++) {
					if (vm.viewVendorChoose[i].IsNational === 1) {
						vm.viewVendorChoose.splice(i, 1);
						i--;
					}
				}
			}
			if (!vm.dataForm.IsInternational) {
				for (var i = 0; i < vm.viewVendorChoose.length; i++) {
					if (vm.viewVendorChoose[i].IsInternational === 1) {
						vm.viewVendorChoose.splice(i, 1);
						i--;
					}
				}
			}

			if (vm.selectedComodity === undefined) {
				UIControlService.msg_growl("warning", "FORMINPUTRFQ.MSG.ERR_CMB_COMODITY");
				return;
			}
			if (vm.selectedClasification === undefined) {
				UIControlService.msg_growl("warning", "FORMINPUTRFQ.MSG.ERR_CLASIFICATION");
				return;
			}
			if ((vm.dataForm.IsLocal === undefined || vm.dataForm.IsLocal === false) &&
                (vm.dataForm.IsNational === undefined || vm.dataForm.IsNational === false) &&
                (vm.dataForm.IsInternational === undefined || vm.dataForm.IsInternational === false)) {
				UIControlService.msg_growl("warning", "FORMINPUTRFQ.MSG.ERR_AREA");
				return;
			}
			if (vm.viewVendorModel == undefined) {
				vm.viewVendorModel = {
					CommodityID: vm.selectedComodity.ID,
					IsLocal: vm.dataForm.IsLocal,
					IsNational: vm.dataForm.IsNational,
					IsInternational: vm.dataForm.IsInternational,
					CompScale: vm.selectedClasification.RefID
				};
			}
			else {
				if ((vm.viewVendorModel.IsLocal == vm.dataForm.IsLocal &&
                    vm.viewVendorModel.CommodityID == vm.selectedComodity.ID) && vm.viewVendorModel.IsNational == vm.dataForm.IsNational
                    && vm.viewVendorModel.IsInternational == vm.dataForm.IsInternational &&
                    vm.viewVendorModel.CompScale == vm.selectedClasification.RefID) {
					vm.viewVendorModel = {
						CommodityID: vm.selectedComodity.ID,
						IsLocal: vm.dataForm.IsLocal,
						IsNational: vm.dataForm.IsNational,
						IsInternational: vm.dataForm.IsInternational,
						CompScale: vm.selectedClasification.RefID,
						getData: vm.viewVendorChoose
					};
				}
				else {
					vm.viewVendorModel = {
						CommodityID: vm.selectedComodity.ID,
						IsLocal: vm.dataForm.IsLocal,
						IsNational: vm.dataForm.IsNational,
						IsInternational: vm.dataForm.IsInternational,
						CompScale: vm.selectedClasification.RefID,
						getData: vm.viewVendorChoose
					};
				}
			}
			if (vm.flag == 1 && vm.RFQID == 0) {
				vm.viewVendorModel.flag = 1;
			}
			else if (vm.flag == 1 && vm.RFQID > 0) {
				vm.viewVendorModel.getData = vm.viewVendorChoose;
				vm.viewVendorModel.flag = 1;
			}
			else if (vm.flag == 0) {
				vm.viewVendorModel.getData = vm.viewVendorChoose;
				vm.viewVendorModel.flag = 2;
			}
			//console.info(vm.viewVendorChoose);
			vm.viewVendorModel.flagData = data;
			vm.viewVendorModel.ID = vm.RFQID;
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataRFQ/viewVendorRFQ.html',
				controller: 'viewVendorRFQCtrl',
				controllerAs: 'viewVendorRFQCtrl',
				resolve: {
					model: function () {
						return vm.viewVendorModel;
					},
				}
			});
			modalInstance.result.then(function (dataitem) {
				vm.viewVendorChoose = dataitem;
				//console.info(vm.viewVendorChoose);
			});
		}

		/*get vendor  by area ,komoditi, klasifikasi*/
		vm.getVendorByArea = getVendorByArea;
		function getVendorByArea() {
			UIControlService.loadLoading('Loading Data Vendor');
			PurchReqService.viewVendor({
				CommodityID: vm.selectedComodity.ID,
				IsLocalVendor: vm.dataForm.IsLocal,
				IsNational: vm.dataForm.IsNational,
				IsInternational: vm.dataForm.IsInternational,
				CompScale: vm.selectedClasification.RefID,
				contactVendor: [],
				Keyword: ""
			}, function (reply) {
				UIControlService.unloadLoading();
				//console.info("ven:" + JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listVendor = reply.data.List;
					var emailers = '';
					for (var i = 0; i < vm.listVendor.length; i++) {
						emailers = emailers + ", " + vm.listVendor[i].Email;
					}
					vm.dataForm.Emails = emailers;
					//console.info("em:" + emailers);
				} else {
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.LOCATION.ERROR', "NOTIFICATION.GET.LOCATION.TITLE");
					return;
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.LOCATION.ERROR', "NOTIFICATION.GET.LOCATION.TITLE");
			});
		}

		/* tambah vendor */
		vm.openDataVendor = openDataVendor;
		vm.listVendor = [];
		function openDataVendor() {
			var data = {};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataRFQ/DataVendor.html',
				controller: 'DataVendorCtrl',
				controllerAs: 'DataVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (dataVendor) {
				dataVendor.VendorName = dataVendor.Name;
				//cek vendor sudah ada belum
				var foundby = $.map(vm.listVendor, function (val) {
					return val.VendorID === dataVendor.VendorID ? val : null;
				});
				if (foundby.length > 0) {
					UIControlService.msg_growl("error", "Vendor " + dataVendor.Name + " sudah ditambahkan!!");
					return;
				}
				else {
					vm.listVendor.push(dataVendor);
				}

			});
		}

		vm.hapusVendor = hapusVendor;
		function hapusVendor(index) {
			vm.listVendor.splice(index, 1);
		}
		/* end tambah vendor */

		/* jika ubah commodity*/
		vm.commodityChange = commodityChange;
		function commodityChange() {
			vm.itemPRChecked = [];
			vm.viewVendorChoose = [];
			vm.itemlistPR = [];
            /*
			if (vm.RFQID > 0) {
				if (vm.selectedComodity.ID == vm.dataForm.CommodityID) vm.itemlistPR = vm.dataForm.RFQGoodsItemPRs;
				else vm.itemlistPR = [];
			}
			else {
				vm.itemlistPR = [];
			}
            */
		}

		//area
		vm.checkArea = checkArea;
		function checkArea(flagChange) {
			vm.disableEmail = false;
			if (vm.dataForm.IsLocal == undefined) vm.dataForm.IsLocal = false;
			if (vm.dataForm.IsNational == undefined) vm.dataForm.IsNational = false;
			if (vm.dataForm.IsInternational == undefined) vm.dataForm.IsInternational = false;
			if ((vm.dataForm.IsLocal == undefined && vm.dataForm.IsNational === undefined && vm.dataForm.IsInternational == undefined)
                || (vm.dataForm.IsLocal == false && vm.dataForm.IsNational === false && vm.dataForm.IsInternational == false)) vm.disableEmail = true;

			else if (vm.dataForm.IsNational === true) {
				vm.disableEmail = false;
			}
			else if (vm.dataForm.IsInternational === true) {
				vm.disableEmail = false;
			}
			else vm.disableEmail = true;
			if (flagChange == 1) vm.flagIsNational = true;
			else if (flagChange == 2) vm.flagIsInternational = true;
		}

		/* untuk cek form disabled enabled */
		vm.cekForm = cekForm;
		function cekForm(action) {
			if (action === 'area') {
				vm.dataForm.IsByVendor = false;
				vm.checkAreaKomoditi = true;
				vm.checkDaftarVendor = false;
				vm.listVendor = [];
				//vm.itemPRChecked = [];
				vm.disableEmail = true;
			}
			else if (action === 'vendor') {
				vm.checkAreaKomoditi = false;
				vm.checkDaftarVendor = true;
				vm.dataForm.IsByArea = false;

				vm.selectedClasification = null;
				vm.dataForm.IsLocal = false;
				vm.dataForm.IsNational = false;
				vm.dataForm.IsInternational = false;
				//vm.selectedComodity = null;
				//vm.itemPRChecked = [];
				vm.disableEmail = true;
			}
			else {
				vm.checkAreaKomoditi = false;
				vm.checkDaftarVendor = false;
				vm.disableEmail = true;
			}
		}

		/*proses simpan*/
		vm.prosesSimpan = prosesSimpan;
		function prosesSimpan() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.vendorsList = [];
			if (vm.dataForm.Emails === undefined) vm.dataForm.Emails = "";

			if (vm.dataForm.Emails === null) vm.dataForm.Emails = "";

			if (vm.dataForm.RFQName == undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_NAME_RFQ");
				return;
			} else if (vm.selectedComodity == undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_COMMODITY");
				return;
			} else if (!vm.itemlistPR || vm.itemlistPR.length === 0) {
				//bootbox.confirm($filter('translate')('CONFIRM_NO_ITEMPR'), function (res) {
				//	if (!res) {
				//		return;
				//	}
				//});

				//UIControlService.unloadLoading();
				//UIControlService.msg_growl("error", "MESSAGE.NO_ITEMPR");
				//return;
			}

			if (vm.dataForm.IsByArea == undefined && vm.dataForm.IsByVendor === undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_VENDOR");
				return;
			} else if (vm.dataForm.IsByArea == true) {
				if ((vm.dataForm.IsLocal == undefined && vm.dataForm.IsNational == undefined &&
                    vm.dataForm.IsInternational == undefined) ||
                    (vm.dataForm.IsLocal == false && vm.dataForm.IsNational == false &&
                    vm.dataForm.IsInternational == false)) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.NO_AREA");
					return;
				} else if (vm.selectedClasification == undefined) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.NO_QUALIFICATION");
					return;
				} else if (vm.dataForm.IsLocal == undefined && vm.viewVendorChoose.length === 0 || vm.dataForm.IsLocal == false && vm.viewVendorChoose.length === 0) {

					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.NOVENDOR_SELECTED");
					return;
				}
			}

			if (vm.dataForm.IsByArea === false && vm.dataForm.IsByVendor === false &&
                vm.dataForm.Emails === "") {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NOVENDOR_SELECTED");
				return;
			}

			if (vm.selectedTypeTender == null) {
				//console.info(vm.vendorsList);
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_TENDER_TYPE");
				return;
			} else if (vm.dataForm.DeliveryTerms === undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_DEL_TERM");
				return;
			} else if (vm.selectedOptionsTender === undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_TENDEROPT");
				return;
			} else if (vm.selectedPaymentTerm === undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_PAYTERM");
				return;
			} else if (vm.dataForm.DeliveryTerms == 3087) {
				if (vm.selectedIncoTerms == undefined) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.NO_INCOTERM");
					return;
				} else if (vm.selectedState == undefined) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.NO_GOAL");
					return;
				} else {
					vm.dataForm.IncoTerm = vm.selectedIncoTerms.ID;
					vm.dataForm.DeliveryLocationState = vm.selectedState.FreightCostId;
				}
			} else if (vm.dataForm.DeliveryTerms == 3088) {
				vm.dataForm.IncoTerm = null;
				vm.dataForm.DeliveryLocationState = null;
			}

			if ((vm.dataForm.Limit == undefined || vm.dataForm.Limit == "") && vm.selectedOptionsTender.RefID != 4173) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_LIMIT_OFFER");
				return;
			} else if (vm.dataForm.ExpiredDay == undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_PERIOD");
				return;
			} else {
				for (var i = 0; i < vm.listStepTender.length; i++) {
					if (vm.listStepTender[i].TenderStepName == "Evaluasi Teknis Barang" && vm.detailCE.length == 0) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.NO_REVIEWER");
						return;
					}
				}
			}
			if (vm.selectedBidderMethod == undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_BIDDER_METHOD");
				return;
			} else if (vm.selectedSchemaTender == undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_SCHEMA");
				return;
			} else if (vm.selectedEvalMethod == undefined) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.NO_EVALMETHOD");
				return;
			}

			if (vm.dataForm.IsByArea == true) {

				if (!vm.dataForm.IsLocal) {
					for (var i = 0; i < vm.viewVendorChoose.length; i++) {
						if (vm.viewVendorChoose[i].IsLocal === 1) {
							vm.viewVendorChoose.splice(i, 1);
							i--;
						}
					}
				}
				if (!vm.dataForm.IsNational) {
					for (var i = 0; i < vm.viewVendorChoose.length; i++) {
						if (vm.viewVendorChoose[i].IsNational === 1) {
							vm.viewVendorChoose.splice(i, 1);
							i--;
						}
					}
				}
				if (!vm.dataForm.IsInternational) {
					for (var i = 0; i < vm.viewVendorChoose.length; i++) {
						if (vm.viewVendorChoose[i].IsInternational === 1) {
							vm.viewVendorChoose.splice(i, 1);
							i--;
						}
					}
				}

				if (vm.viewVendorChoose.length == 0) {
					PurchReqService.viewVendor({
						CommodityID: vm.selectedComodity.ID,
						IsLocal: vm.dataForm.IsLocal,
						IsNational: false,
						IsInternational: false,
						CompScale: vm.selectedClasification.RefID,
						Keyword: "",
						contactVendor: []
					}, function (reply) {
						if (reply.status === 200) {
							vm.vendorsList = reply.data.List;
							if (vm.vendorsList.length == 0) {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", "MESSAGE.NOVENDOR_SELECTED");
								return;
							} else {
								if (vm.dataForm.Emails == "") save();
								else cekEmail();
							}
						}
					}, function (err) {
						UIControlService.unloadLoadingModal();
					});
				} else if (vm.viewVendorChoose.length !== 0 && vm.dataForm.IsLocal) {
					for (var i = 0; i < vm.viewVendorChoose.length; i++) {
						if (vm.viewVendorChoose[i].IsLocal === 1) {
							vm.viewVendorChoose.splice(i, 1);
							i--;
						}
					}

					PurchReqService.viewVendor({
						CommodityID: vm.selectedComodity.ID,
						IsLocal: vm.dataForm.IsLocal,
						IsNational: false,
						IsInternational: false,
						CompScale: vm.selectedClasification.RefID,
						Keyword: "",
						contactVendor: []
					}, function (reply) {
						if (reply.status === 200) {
							var vendorList = reply.data.List;
							for (var i = 0; i < vendorList.length; i++) {
								if (vendorList[i].IsLocal === 1)
									vm.viewVendorChoose.push(vendorList[i])
							}

							vm.vendorsList = vm.viewVendorChoose;
							if (vm.dataForm.Emails == "") save();
							else cekEmail();
						}
					}, function (err) {
						UIControlService.unloadLoadingModal();
					});

				} else if (vm.viewVendorChoose.length !== 0) {
					vm.vendorsList = vm.viewVendorChoose;
					if (vm.dataForm.Emails == "") save();
					else cekEmail();
				}
			} else if (vm.dataForm.IsByVendor == true) {
				if (vm.listVendor.length == 0) {
					UIControlService.msg_growl("error", "MESSAGE.NOVENDOR_SELECTED");
					return;
				} else {
					for (var b = 0; b < vm.listVendor.length; b++) {
						var dataVen = { VendorID: vm.listVendor[b].VendorID };
						vm.vendorsList.push(dataVen);
						if (b == vm.listVendor.length - 1) {
							if (vm.dataForm.Emails == "") save();
							else cekEmail();
						}
					}
				}
			} else {
				if (vm.dataForm.Emails == "") save();
				else cekEmail();
			}
		}

		vm.cekEmail = cekEmail;
		function cekEmail() {
			if (vm.dataForm.Emails !== "") {
				PurchReqService.cekEmails({ Keyword: vm.dataForm.Emails }, function (reply) {
					UIControlService.unloadLoading();
					//console.info("prq:: " + JSON.stringify(reply));
					if (reply.status === 200) {
						if (reply.data.Email == null) save();
						else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", "Sorry Email " + reply.data.Email + " is already in Eprocrument by vendor " + reply.data.Name);
						}
					} else {
						//UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
						return;
					}
				}, function (err) {
					//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
					UIControlService.unloadLoadingModal();
				});
			}

		}

		vm.save = save;
		function save() {
			if (!vm.itemlistPR || vm.itemlistPR.length === 0) {
				bootbox.confirm($filter('translate')('CONFIRM_NO_ITEMPR'), function (res) {
					if (res) {
						if (vm.dataForm.IsByArea == true) vm.dataForm.CompScale = vm.selectedClasification.RefID;
						else vm.dataForm.CompScale = null;

						vm.dataForm.PaymentTerm = vm.selectedPaymentTerm.Id;
						vm.dataForm.CommodityID = vm.selectedComodity.ID;
						vm.dataForm.TenderType = vm.selectedTypeTender.RefID;
						vm.dataForm.TenderOption = vm.selectedOptionsTender.RefID;
						vm.dataForm.DeliveryTerms = Number(vm.dataForm.DeliveryTerms);
						vm.dataForm.BidderSelMethod = vm.selectedBidderMethod.RefID;
						vm.dataForm.ProcMethod = vm.selectedSchemaTender.MethodID;
						vm.dataForm.EvalMethod = vm.selectedEvalMethod.EvaluationMethodId;

						var stepTenders = [];
						//console.info("dinda" + JSON.stringify(vm.listStepTender));
						for (var a = 0; a < vm.listStepTender.length; a++) {
							var step = {
								TenderStepID: vm.listStepTender[a].TenderStepID,
								StartDate: UIControlService.getStrDate(vm.listStepTender[a].StartDate),
								EndDate: UIControlService.getStrDate(vm.listStepTender[a].EndDate),
								Duration: Number(vm.listStepTender[a].Duration)
							}
							stepTenders.push(step);
						}
						//console.info("tenderstep")

						var itemPRs = [];
						for (var c = 0; c < vm.itemlistPR.length; c++) {
							var item = { ItemPRID: vm.itemlistPR[c].ID }
							itemPRs.push(item);
						}

						if (vm.dataForm.Emails.length == "") {
							vm.dataForm.IsVendorEmails = false;
							vm.dataForm.Emails = null;
						} else {
							vm.dataForm.IsVendorEmails = true;
						}

						vm.dataForm['Vendors'] = vm.vendorsList;
						vm.dataForm['RFQGoodsSteps'] = stepTenders;
						vm.dataForm['RFQGoodsItemPRs'] = itemPRs;
						//console.info(vm.dataForm);
						if (vm.RFQID > 0) {
							PurchReqService.updatePRRFQ(vm.dataForm, function (reply) {
								UIControlService.unloadLoading();
								if (reply.status === 200) {
									for (var i = 0; i < vm.detailCE.length; i++) {
										vm.detailCE[i].RFQGoodID = vm.RFQID;
										if (i == vm.detailCE.length - 1) {
											//console.info(vm.detailCE);
											PurchReqService.insertCE(vm.detailCE, function (reply) {
												if (reply.status === 200) {
													UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_RFQ");
													for (var i = 0; i < vm.detailCE.length; i++) {
														if (vm.detailCE[i].ID == 0) {
															//sendEmail(vm.detailCE[i], i);
															//if (vm.EmpEmail === "") vm.EmpEmail = vm.detailCE[i].employee.FullName;
															//else vm.EmpEmail += ", " + vm.detailCE[i].employee.FullName;
														}
													}
												} else {
													//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
													return;
												}
											}, function (err) {
												// UIControlService.msg_growl("error", "Gagal Akses Api!!");
												UIControlService.unloadLoadingModal();
											});
										}
									}
									//UIControlService.msg_growl("success", "Berhasil Simpan Data");
									localStorage.removeItem('checked-itempr');
									localStorage.removeItem('checked-all-itempr');
									$state.transitionTo('purchase-requisition');
								} else {
									///UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
									return;
								}
							}, function (err) {
								// UIControlService.msg_growl("error", "MESSAGE.ERR_API");
								UIControlService.unloadLoadingModal();
							});

						} else {
							PurchReqService.insertPRRFQ(vm.dataForm, function (reply) {
								UIControlService.unloadLoading();
								//console.info("prq:: " + JSON.stringify(reply));
								if (reply.status === 200) {
									for (var i = 0; i < vm.detailCE.length; i++) {
										vm.detailCE[i].RFQGoodID = reply.data.ID;
										if (i == vm.detailCE.length - 1) {
											//console.info(vm.detailCE);
											PurchReqService.insertCE(vm.detailCE, function (reply) {
												if (reply.status === 200) {
													UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_REV");
													for (var i = 0; i < vm.detailCE.length; i++) {
														if (vm.detailCE[i].ID == 0) {
															//sendEmail(vm.detailCE[i], i);
															//if (vm.EmpEmail === "") vm.EmpEmail = vm.detailCE[i].employee.FullName;
															//else vm.EmpEmail += ", " + vm.detailCE[i].employee.FullName;
														}
													}
												} else {
													// UIControlService.msg_growl("error", "Gagal menyimpan data!!");
													return;
												}
											}, function (err) {
												// UIControlService.msg_growl("error", "Gagal Akses Api!!");
												UIControlService.unloadLoadingModal();
											});
										}
									}
									localStorage.removeItem('checked-itempr');
									localStorage.removeItem('checked-all-itempr');
									$state.transitionTo('purchase-requisition');
								} else {
									// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
									return;
								}
							}, function (err) {
								//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", err.join('\n'));
							});
						}
					} else {
						UIControlService.unloadLoadingModal();
						UIControlService.unloadLoading();
						return;
					}
				});
			} else {
				if (vm.dataForm.IsByArea == true) vm.dataForm.CompScale = vm.selectedClasification.RefID;
				else vm.dataForm.CompScale = null;

				vm.dataForm.PaymentTerm = vm.selectedPaymentTerm.Id;
				vm.dataForm.CommodityID = vm.selectedComodity.ID;
				vm.dataForm.TenderType = vm.selectedTypeTender.RefID;
				vm.dataForm.TenderOption = vm.selectedOptionsTender.RefID;
				vm.dataForm.DeliveryTerms = Number(vm.dataForm.DeliveryTerms);
				vm.dataForm.BidderSelMethod = vm.selectedBidderMethod.RefID;
				vm.dataForm.ProcMethod = vm.selectedSchemaTender.MethodID;
				vm.dataForm.EvalMethod = vm.selectedEvalMethod.EvaluationMethodId;

				var stepTenders = [];
				//console.info("dinda" + JSON.stringify(vm.listStepTender));
				for (var a = 0; a < vm.listStepTender.length; a++) {
					var step = {
						TenderStepID: vm.listStepTender[a].TenderStepID,
						StartDate: UIControlService.getStrDate(vm.listStepTender[a].StartDate),
						EndDate: UIControlService.getStrDate(vm.listStepTender[a].EndDate),
						Duration: Number(vm.listStepTender[a].Duration)
					}
					stepTenders.push(step);
				}
				//console.info("tenderstep")

				var itemPRs = [];
				for (var c = 0; c < vm.itemlistPR.length; c++) {
					var item = { ItemPRID: vm.itemlistPR[c].ID }
					itemPRs.push(item);
				}

				if (vm.dataForm.Emails.length == "") {
					vm.dataForm.IsVendorEmails = false;
					vm.dataForm.Emails = null;
				} else {
					vm.dataForm.IsVendorEmails = true;
				}

				vm.dataForm['Vendors'] = vm.vendorsList;
				vm.dataForm['RFQGoodsSteps'] = stepTenders;
				vm.dataForm['RFQGoodsItemPRs'] = itemPRs;
				//console.info(vm.dataForm);
				if (vm.RFQID > 0) {
					PurchReqService.updatePRRFQ(vm.dataForm, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							for (var i = 0; i < vm.detailCE.length; i++) {
								vm.detailCE[i].RFQGoodID = vm.RFQID;
								if (i == vm.detailCE.length - 1) {
									//console.info(vm.detailCE);
									PurchReqService.insertCE(vm.detailCE, function (reply) {
										if (reply.status === 200) {
											UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_RFQ");
											for (var i = 0; i < vm.detailCE.length; i++) {
												if (vm.detailCE[i].ID == 0) {
													//sendEmail(vm.detailCE[i], i);
													//if (vm.EmpEmail === "") vm.EmpEmail = vm.detailCE[i].employee.FullName;
													//else vm.EmpEmail += ", " + vm.detailCE[i].employee.FullName;
												}
											}
										} else {
											//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
											return;
										}
									}, function (err) {
										// UIControlService.msg_growl("error", "Gagal Akses Api!!");
										UIControlService.unloadLoadingModal();
									});
								}
							}
							//UIControlService.msg_growl("success", "Berhasil Simpan Data");
							localStorage.removeItem('checked-itempr');
							localStorage.removeItem('checked-all-itempr');
							$state.transitionTo('purchase-requisition');
						} else {
							///UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
							return;
						}
					}, function (err) {
						// UIControlService.msg_growl("error", "MESSAGE.ERR_API");
						UIControlService.unloadLoadingModal();
					});

				} else {
					PurchReqService.insertPRRFQ(vm.dataForm, function (reply) {
						UIControlService.unloadLoading();
						//console.info("prq:: " + JSON.stringify(reply));
						if (reply.status === 200) {
							for (var i = 0; i < vm.detailCE.length; i++) {
								vm.detailCE[i].RFQGoodID = reply.data.ID;
								if (i == vm.detailCE.length - 1) {
									//console.info(vm.detailCE);
									PurchReqService.insertCE(vm.detailCE, function (reply) {
										if (reply.status === 200) {
											UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_REV");
											for (var i = 0; i < vm.detailCE.length; i++) {
												if (vm.detailCE[i].ID == 0) {
													//sendEmail(vm.detailCE[i], i);
													//if (vm.EmpEmail === "") vm.EmpEmail = vm.detailCE[i].employee.FullName;
													//else vm.EmpEmail += ", " + vm.detailCE[i].employee.FullName;
												}
											}
										} else {
											// UIControlService.msg_growl("error", "Gagal menyimpan data!!");
											return;
										}
									}, function (err) {
										// UIControlService.msg_growl("error", "Gagal Akses Api!!");
										UIControlService.unloadLoadingModal();
									});
								}
							}
							localStorage.removeItem('checked-itempr');
							localStorage.removeItem('checked-all-itempr');
							$state.transitionTo('purchase-requisition');
						} else {
							// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
							return;
						}
					}, function (err) {
						//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", err.join('\n'));
					});
				}
			}
		}

		vm.back = back;
		function back() {
			localStorage.removeItem('checked-itempr');
			localStorage.removeItem('checked-all-itempr');
			$state.transitionTo('purchase-requisition');
		}

		vm.aturCE = aturCE;
		function aturCE(flag) {
			var data = {
				act: flag,
				RFQGoodID: vm.RFQID,
				ProjectTitle: vm.dataForm.RFQName,
				DetailCE: vm.detailCE
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataRFQ/commite-modal.html',
				controller: 'CommitteeModalCtrl',
				controllerAs: 'CommitteeModalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (selected) {
				vm.detailCE = selected;
			});
		}

		vm.sendEmail = sendEmail;
		function sendEmail(data, i) {
			//console.info(data);
			ContractEngineerService.getMailContent({
				EmailContent: 'Notifikasi Contract Engineer',
				TenderName: vm.dataForm.RFQName,
				VendorName: data.employee.FullName
			}, function (response) {
				if (response.status == 200) {
					var email = {
						subject: response.data.Subject,
						mailContent: response.data.MailContent,
						isHtml: true,
						addresses: [data.email]
					};

					UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
					ContractEngineerService.sendMail(email, function (response) {
						UIControlService.unloadLoading();
						if (response.status == 200) {
							UIControlService.msg_growl("notice", "EMAIL_SENT");
							//if (vm.EmpEmail !== "" && i == vm.detail.length - 1) {
							//    sendEmail1(vm.EmpEmail);
							//}
						} else {
							UIControlService.handleRequestError(response.data);
						}
					}, function (response) {
						UIControlService.handleRequestError(response.data);
						UIControlService.unloadLoading();
					});
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
			});
		}

		vm.sendEmail1 = sendEmail1;
		function sendEmail1(dt) {
			ContractEngineerService.getMailContent1({
				EmailContent: 'Notifikasi Requestor pengadaan',
				TenderName: vm.project,
				VendorName: dt
			}, function (response) {
				if (response.status == 200) {
					var email = {
						subject: response.data.Subject,
						mailContent: response.data.MailContent,
						isHtml: true,
						addresses: [vm.EmailRequestor]
					};

					UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
					ContractEngineerService.sendMail(email, function (response) {
						UIControlService.unloadLoading();
						if (response.status == 200) {
							UIControlService.msg_growl("notice", "EMAIL_SENT");
						} else {
							UIControlService.handleRequestError(response.data);
						}
					}, function (response) {
						UIControlService.handleRequestError(response.data);
						UIControlService.unloadLoading();
					});
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
			});
		}

		vm.changeTerm = changeTerm;
		function changeTerm(data) {
			if (data == 3088) {
				vm.selectedState = {};
				vm.selectedIncoTerms = {};
			}
		}

		vm.changeIncoTerm = changeIncoTerm;
		function changeIncoTerm(data, flag) {
			PurchReqService.selectFreight({
				Status: data
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listState = reply.data;
				if (flag == 1) {
					for (var i = 0; i < vm.listState.length; i++) {
						if (vm.dataForm.DeliveryLocationState === vm.listState[i].FreightCostId) {
							vm.selectedState = vm.listState[i];
							break;
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.cekOptionsTender = cekOptionsTender;
		function cekOptionsTender() {
			if (vm.selectedOptionsTender.RefID == 4173) {
				vm.dataForm.Limit = 0;
			}
		}

		/*model form*/
		function RFQField(ID, RFQCode, RFQName, NoticeText, VendorSelectBy, CommodityID, IsLocal, IsNational, IsInternational, IsVendorEmails
            , Emails, CompScale, DeliveryTerms, IncoTerm, DeliveryLocationState, DeliveryLocationCity, BidderSelMethod, ProcMethod, EvalMethod, Status, IsByArea,
            Limit, ExpiredDay, IsOpen, IsByVendor, TenderType, TenderOption) {
			vm.self = this;
			self.RFQCode = RFQCode;
			self.RFQName = RFQName;
			self.NoticeText = NoticeText;
			self.VendorSelectBy = VendorSelectBy;
			self.CommodityID = CommodityID;
			self.IsLocal = IsLocal;
			self.IsNational = IsNational;
			self.IsInternational = IsInternational;
			self.IsVendorEmails = IsVendorEmails;
			self.Emails = Emails;
			self.CompScale = CompScale;
			self.DeliveryTerms = DeliveryTerms;
			self.IncoTerm = IncoTerm;
			self.DeliveryLocationState = DeliveryLocationState;
			self.DeliveryLocationCity = DeliveryLocationCity;
			self.BidderSelMethod = BidderSelMethod;
			self.ProcMethod = ProcMethod;
			self.EvalMethod = EvalMethod;
			self.IsByArea = IsByArea;
			self.Limit = Limit;
			self.ExpiredDay = ExpiredDay;
			self.IsOpen = IsOpen;
			self.IsByVendor = IsByVendor;
			self.TenderType = TenderType;
			self.TenderOption = TenderOption;
		}

	}
})();