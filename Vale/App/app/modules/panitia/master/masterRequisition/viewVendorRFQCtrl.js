﻿(function () {
	'use strict';

	angular.module('app').controller('viewVendorRFQCtrl', ctrl);

	ctrl.$inject = ['model', 'RFQVHSService', 'UIControlService', '$translatePartialLoader', '$uibModalInstance'];

	function ctrl(model, RFQVHSService, UIControlService, $translatePartialLoader, $uibModalInstance) {
		var vm = this;

		vm.vendors = null;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.SelectedVendors = model.SelectedVendors
		vm.totalItems = 0;
		vm.isReadOnly = model.IsReadOnly;

		vm.init = init;
		function init() {
			UIControlService.loadLoadingModal('LOADING.VIEW.VENDOR');
			$translatePartialLoader.addPart('vhs-requisition');
			RFQVHSService.viewVendor({
				CommodityID: model.CommodityID,
				IsLocal: model.IsLocal,
				IsNational: model.IsNational,
				IsInternational: model.IsInternational,
				CompScale: model.CompScale,
				Offset: (vm.currentPage - 1) * 10,
				Keyword: vm.keyword,
				RFQType: model.RFQType,
				FilteredEmails: model.SelectedVendors.map(function (a) { return a.VendorID })
			}, function (reply) {
				if (reply.status === 200) {
					vm.vendors = reply.data.List;
					if (!vm.isReadOnly && model.IsLocal) {
						for (var i = 0; i < vm.vendors.length; i++) {
							if (vm.vendors[i].IsLocal) {
								vm.vendors[i].flagButton = true;
							}
						}
					}
					vm.totalItems = reply.data.Count;
					for (var i = 0; i < vm.SelectedVendors.length; i++) {
						for (var j = 0; j < vm.vendors.length; j++) {
							if (vm.SelectedVendors[i].VendorID === vm.vendors[j].VendorID) {
								vm.vendors[j].flagButton = true;
								break;
							}
						}
					}

					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.VENDORS.ERROR', "NOTIFICATION.GET.VENDORS.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.VENDORS.ERROR', "NOTIFICATION.GET.VENDORS.TITLE");
			});
		}

		vm.addRemoveVendor = addRemoveVendor;
		function addRemoveVendor(data) {
			if (data.flagButton) {
				vm.SelectedVendors.push(data);
			} else {
				var i = 0;

				while (i < (vm.SelectedVendors.length)) {
					if (vm.SelectedVendors[i].VendorID === data.VendorID) {
						vm.SelectedVendors.splice(i, 1);
						break;
					}
					i++;
				}
			}
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			init();
		}

		vm.addSelectedVendor = addSelectedVendor;
		function addSelectedVendor() {
			$uibModalInstance.close(vm.SelectedVendors);
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();