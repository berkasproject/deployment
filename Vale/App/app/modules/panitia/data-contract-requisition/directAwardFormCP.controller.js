(function () {
    'use strict';

    angular.module("app")
    .controller("directAwardFormCPCtrl", ctrl);
    
    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataContractRequisitionService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataContractRequisitionService, UIControlService, item) {

        var vm = this;
        var contractRequisitionId = item.contractRequisitionId;
        var loadmsg = "";

        vm.directAward = {};
        vm.isTenderVerification = true;
        vm.estimatedCostLabel = ['US$', 'IDR', 'OTHERS'];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            $translate.refresh().then(function () {
                loadmsg = $filter('translate')('MESSAGE.LOADING');
            });
            vm.loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal(loadmsg);
            DataContractRequisitionService.SelectDirectAward({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.directAward = reply.data;
                    if (!vm.directAward.ContractRequisitonDACheckLists) {
                        vm.directAward.ContractRequisitonDACheckLists = [];
                        for (var i = 0; i < 13; i++){
                            vm.directAward.ContractRequisitonDACheckLists.push({
                                Index : i,
                                IsChecked : false
                            });
                        }
                    }

                    vm.directAward.ContractRequisition = {};
                    vm.directAward.ContractRequisition.ProjectManager = item.ProjectManager;
                    vm.directAward.ProjectManagerFullName = item.ProjectManagerFullName;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        };

        vm.print = print;
        function print(toPrint) {
            var innerContents = document.getElementById(toPrint).innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Direct Award Request Form</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();
        }

        vm.onBatalClick = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();