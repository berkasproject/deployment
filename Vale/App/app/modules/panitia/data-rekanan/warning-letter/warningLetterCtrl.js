(function () {
    'use strict';

    angular.module("app").controller("WarningLetterCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$state', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'RoleService', 'UIControlService', '$uibModal'];
    function ctrl($http, $filter, $translate, $state, $translatePartialLoader, $location, SocketService, WarningLetterService,
        RoleService, UIControlService, $uibModal ) {
        var vm = this;
        var page_id = 141;
        vm.search = "";
        vm.warningletter = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;

        vm.kata = new Kata("");
        vm.init = init;
        vm.isCalendarOpened = [false, false];

        vm.cariwarningLetter = cariwarningLetter;
        vm.jLoad = jLoad;
        //vm.loadAll = loadAll;
        vm.ubah_aktif = ubah_aktif;
        vm.tambah = tambah;
        vm.edit = edit;
        vm.onlyL1Support = true;
        vm.searchBy = "1";

        function init() {
            $translatePartialLoader.addPart('master-warningLetter');
            UIControlService.loadLoading($filter('translate')('MESSAGE.LOADING'));
            jLoad(1);

        }

        vm.cariwarningLetter = cariwarningLetter;
        function cariwarningLetter() {
            init();
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            cekRole();            
            vm.warningLetter = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            UIControlService.loadLoading("");
            WarningLetterService.select({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.search,
                column: vm.searchBy,
                Date1: UIControlService.getStrDate(vm.dateFrom),
                Date2: UIControlService.getStrDate(vm.dateTo)
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;                    
                    vm.warningletter = data.List;
                    vm.totalItems = Number(data.Count);
                } else {                    
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEPARTMENT'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        }

        vm.cekRole = cekRole;
        function cekRole() {
             WarningLetterService.cekRole(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.role = reply.data;
                    console.info("role :" + vm.role);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_ROLE'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        };           
       
        // Change data status to Incomplete
        vm.stateIncomplete = stateIncomplete;
        function stateIncomplete(data) {
            var flagStat = 1;
            var item = {
                LetterID: data.LetterID,
                flag: flagStat,
                IsFromComplaint: data.Type === 1
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/detailApp.modal.html?v=1.000002',
                controller: 'detailAppWLCtrl',
                controllerAs: 'detailAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {             
                vm.jLoad(1);               
            });               
        }

        // Change data status to Review
        vm.stateReview = stateReview;
        function stateReview(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REVIEW'), function (yes) {
                if (yes) {
                    WarningLetterService.stateReview({
                        LetterID: data.LetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_REVIEW'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_REVIEW'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });          
        }

        // Change data status to Canceled - pre cancel - cancel complaint
        vm.stateCanceled = stateCanceled;
        function stateCanceled(data) {

            var flagStat = 2;
            var item = {
                LetterID: data.LetterID,
                flag: flagStat,
                IsFromComplaint: data.Type === 1
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/detailApp.modal.html?v=1.000002',
                controller: 'detailAppWLCtrl',
                controllerAs: 'detailAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);              
            });                     
        }

        vm.stateCanceledPost = stateCanceledPost;
        function stateCanceledPost(data) {

            var flagStat = 4;
            var item = {
                LetterID: data.LetterID,
                flag: flagStat,
                IsFromComplaint: data.Type === 1
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/detailApp.modal.html?v=1.000002',
                controller: 'detailAppWLCtrl',
                controllerAs: 'detailAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        // Send data to Approval
        vm.stateOnProcess = stateOnProcess;
        function stateOnProcess(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_TO_APPROVAL'), function (yes) {
                if (yes) {
                    WarningLetterService.stateOnProcess({
                        LetterID: data.LetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_SEND_TO_APPROVAL'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_SEND_TO_APPROVAL'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });           
        }                   

        // Send data to Vendor
        vm.stateSendVendor = stateSendVendor;
        function stateSendVendor(data) {         
            var flagStat = 3;
            var item = {
                LetterID: data.LetterID,
                flag: flagStat,
                IsFromComplaint: data.Type === 1
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/detailApp.modal.html?v=1.000002',
                controller: 'detailAppWLCtrl',
                controllerAs: 'detailAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }
        // Change data status to Closeed WL after Revise from Vendor
        vm.stateClosed = stateClosed;
        function stateClosed(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CLOSED'), function (yes) {
                if (yes) {
                    WarningLetterService.stateClosed({
                        LetterID: data.LetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_CLOSED'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_CLOSED'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        
        // Approve by L1 Procurement Support
        vm.stateApproveByL1Support = stateApproveByL1Support;
        function stateApproveByL1Support(data){
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L1_SUPP'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL1Support({
                        LetterID: data.LetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        // Approve by L1 Procurement 
        vm.stateApproveByL1 = stateApproveByL1;
        function stateApproveByL1(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L1'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL1({
                        LetterID: data.LetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        // Approve by L3 Dir. Of SCM
        vm.stateApproveByL3 = stateApproveByL3;
        function stateApproveByL3(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L3'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL3({
                        LetterID: data.LetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        // skip approval from vendor
        vm.stateCompleteSkipVendor = stateCompleteSkipVendor;
        function stateCompleteSkipVendor(data) {
            var flagStat = 5;
            var item = {
                LetterID: data.LetterID,
                flag: flagStat,
                IsFromComplaint: data.Type === 1
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/detailApp.modal.html?v=1.000002',
                controller: 'detailAppWLCtrl',
                controllerAs: 'detailAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });s
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            UIControlService.loadLoading($filter('translate')('MESSAGE.LOADING'));
            
            DepartemenService.editActive({
                DepartmentID: data.DepartmentID, DepartmentCode: data.DepartmentCode, DepartmentName: data.DepartmentName, IsActive: active
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var msg = "";
                    if (active === false) msg = " NonAktifkan ";
                    if (active === true) msg = "Aktifkan ";
                    UIControlService.msg_growl("success", "Data Berhasil di " + msg);
                    jLoad(1);
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INACTIVATE_DATA'));
                    return;
                }
            }, function (err) {

                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });

        }

        vm.tambah = tambah;
        function tambah(data, act) {            
            var data = {
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/FormWarningLetter.html',
                controller: 'FormWarningLetterCtrl',
                controllerAs: 'frmWarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.edit = edit;
        function edit(data,act) {
            
            var data = {
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/FormWarningLetter.html',
                controller: 'FormWarningLetterCtrl',
                controllerAs: 'frmWarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                jLoad(1);
            });
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            UIControlService.loadLoading($filter('translate')('MESSAGE.LOADING'));
            WarningLetterService.editActive({
                LetterID: data.LetterID, 
                IsActive: active
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var msg = "";
                    if (active === false) msg = " NonAktifkan ";
                    if (active === true) msg = "Aktifkan ";
                    UIControlService.msg_growl("success", "Data Berhasil di " + msg);
                    jLoad(1);
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INACTIVATE_DATA')); 
                    return;
                }
            }, function (err) {

                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        }

        vm.lihatDetail = lihatDetail;
        function lihatDetail(data) {            
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/DetailWarningLetter.html',
                controller: 'DetailWarningLetterCtrl',
                controllerAs: 'DetailwarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.detailTemplate = detailTemplate;
        function detailTemplate(data) {            
            var data = {
                item:data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/DetailTemplateWL.html',
                controller: 'DetailWarningLetterCtrl',
                controllerAs: 'DetailwarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            })
        }

        vm.lihatDetailCPR = lihatDetailCPR;
        function lihatDetailCPR(data) {
            if (data.Type == 2) {
                var lempar = {
                    flagRole: 0,
                    VPCPRDataId: data.VPCPRDataID,
                    IsVhsCpr: 1
                };
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-list.modal.html?v=1.000002',
                    controller: 'cprListModal',
                    controllerAs: 'cprListModal',
                    resolve: {
                        item: function () {
                            return lempar;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    //loadAwal();
                });
            } else if (data.Type == 3) {
                var lempar = {
                    flagRole: 0,
                    VPVHSDataId: data.VPVHSDataID,
                    IsVhsCpr: 2
                };
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-list.modal.html?v=1.000002',
                    controller: 'vhsListModal',
                    controllerAs: 'vhsListModal',
                    resolve: {
                        item: function () {
                            return lempar;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    //loadAwal();
                });
            }
        }

        vm.tambahTemp = tambahTemp;
        function tambahTemp(datawl) {
            var data = {                               
                data1: datawl
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/UbahTemplate.html',
                controller: 'UbahWarningLetterCtrl',
                controllerAs: 'UbahwarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {                
                vm.jLoad(1);
            });
        }
        
        vm.listApproval = listApproval;
        function listApproval(data) {
            var item = {
                LetterID: data.LetterID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/listApp.modal.html',
                controller: 'listAppCtrl',
                controllerAs: 'listAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.listRemark = listRemark;
        function listRemark(data) {
            var item = {
                LetterID: data.LetterID,
                DetailKontrak: data.DetailKontrak
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/listRemark.modal.html',
                controller: 'listRemarkController',
                controllerAs: 'listRemarkCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                
            });
        }

        vm.kePODelay = kePODelay;
        function kePODelay() {
            $state.transitionTo("delayed-po");
        }
    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

