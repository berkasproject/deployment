﻿(function () {
    'use strict';

    angular.module("app").controller("UbahWarningLetterCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter','$translate', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance', '$sce'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, WarningLetterService,
        RoleService, UIControlService, item, $uibModalInstance, $sce) {
        var vm = this;

        vm.departemen = [];
        var page_id = 141;
        vm.one;
        vm.letterNumber = "";
        vm.isAdd = item.act;
        vm.Area;
        vm.isActive = false;        
        vm.LetterID;
        vm.Description = "";
        vm.action = "";
        vm.init = init;
        vm.warningLetter = {};
        vm.isCalendarOpened = [false, false, false, false];
        vm.contact = [];
        vm.address = [];
        vm.companyPerson = [];

        vm.letterType = "";
        vm.sequenceNum;

        vm.approverName = "";
        vm.approverPosition = "";

        vm.LastStartDate;
        vm.LastEndDate;

        vm.pmName="";
        vm.pmPosition = "";
        vm.codeDiff = "";
        vm.ProjectTitle = "";
        vm.tenderCode = "";
        vm.numbContract = "";
        vm.showDate = true;

        function init() {
          
            if (item.data1.Vendor.businessName == null) {
                vm.businessName = " ";
            }
            else {
                vm.businessName = item.data1.Vendor.businessName + ". ";
            }

          
            $translatePartialLoader.addPart('master-warningLetter');
            vm.warningLetter.CreatedDate = item.data1.CreatedDate;
         
            vm.deptID = item.data1.ReporterID;
            getDepartmentDetail(vm.deptID);

            if (item.data1.department== null) {
                vm.warningLetter.ComplaintDate = item.data1.CreatedDate;
            }
            else {
                vm.warningLetter.ComplaintDate = item.data1.department.Date;
            }
           
            vm.warningLetter.StartDate = item.data1.StartDate;
            vm.warningLetter.EndDate = item.data1.EndDate;
            vm.location = "";// item.data1.Area;

            vm.langSelected = 'IN';
            if (item.data1.LetterType) {
                vm.originalTypeLetter = getTypeLetter(item.data1.LetterType);
                if (item.data1.LetterType.indexOf('_EN_') < 0) {
                    vm.langSelected = 'IN';
                    vm.selectedWarningType = item.data1.LetterType;
                } else {
                    vm.langSelected = 'EN';
                    vm.selectedWarningType_EN = item.data1.LetterType;
                }
            }

            vm.selectedLang = vm.langSelected;

            vm.originalLetterNumber = item.data1.LetterNumber;
            vm.letterNumber = item.data1.LetterNumber;
            vm.isSuspendByField = item.data1.IsSuspendByField;
            vm.isSuspendContract = item.data1.IsSuspendContract;
            vm.isByCommodity = item.data1.IsByCommodity;
            vm.isFromDelayedPO = item.data1.Type === 4;

            WarningLetterService.checkContract({
                IDContract: item.data1.IDContract,
                ContractOrPONumber: item.data1.ContractOrPONumber
            }, function (reply) {
                if (reply.status === 200) {
                    vm.rest = reply.data;
                    if (vm.rest == "Jasa") {
                        vm.codeDiff = "CAS";
                    }
                    if (vm.rest == "Barang") {
                        vm.codeDiff = "PS"
                    }
                    if (vm.rest == "VHS") {
                        vm.codeDiff = "PS";
                    }

                    if (vm.langSelected === 'IN') {
                        getWarningType(true);
                    } else {
                        getWarningType_EN(true);
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_CONTRACT'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_CONTRACT'));
            });

            if (item.data1.TemplateDoc == "") {
                vm.trusteddepartemen = "";
            }
            else {
                vm.trusteddepartemen = item.data1.TemplateDoc;
            }

			convertToDate();
			getContact();
			getAddress();
			getCompanyPerson();
			getEmployeeDetail("8606");
			getEmployeeDetail("6187");
			getPositionDetail("ESCA11");// L1 Proc Supp
			getPositionDetail("CCSSGM");// L3
			vm.LastStartDate =item.data1.StartDate;
			vm.LastEndDate = item.data1.EndDate;
        }

        vm.chooseLanguage = chooseLanguage;
        function chooseLanguage(value) {            
            vm.selectedLang = value;
            vm.letterNumber = "";
            vm.showDate = false;
            vm.showSuspendOps = false;
            if (vm.selectedLang == 'IN') {
                getWarningType();
            }
            if (vm.selectedLang == 'EN') {
                getWarningType_EN();
            }
        }

        vm.getDepartmentDetail = getDepartmentDetail;
        function getDepartmentDetail(id) {
            WarningLetterService.getDepartmentDetail({
                DepartmentID: id
            }, function (reply) {
                if (reply.status === 200) {
                    var detail = reply.data;                 
                    vm.departName = detail.DepartmentName;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EMP'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EMP'));
            });
        }

        function getPositionDetail(name) {
            WarningLetterService.getPositionDetail({
                PositionCode: name
            }, function (reply) {
                if (reply.status === 200) {                  
                    var detail = reply.data;
                    if (name == "ESCA11") {
                        vm.approverPosition = detail.ExtPosTitle != null ? detail.ExtPosTitle : detail.PositionName;
                    }
                    if (name == "CCSSGM") {
                        vm.approverPositionL3 = detail.ExtPosTitle != null ? detail.ExtPosTitle : detail.PositionName;
                    }
                    if (name != "ESCA11" && name != "CCSSGM") {
                        vm.pmPosition = detail.ExtPosTitle != null ? detail.ExtPosTitle : detail.PositionName;
                    }

                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EMP'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EMP'));
            });
        }
          
        function getEmployeeDetail(id) {
            WarningLetterService.getEmployeeDetail({
                EmployeeID: id
            }, function (reply) {
                if (reply.status === 200) {                    
                    var info = reply.data;
                    if (id == "8606") { // approver L1 Proc Supp
                        vm.approverName = info.FullName + " " + info.SurName;                        
                    }
                    if (id == "6187") { // approver L3
                        vm.approverNameL3 = info.FullName + " " + info.SurName;
                    }
                    if (id != "8606" && id != "6187") { // pm
                        vm.pmName = info.FullName + " " + info.SurName;                        
                    }
                    
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EMP'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        }
        /// reference for romanize month--- >>> ---https://stackoverflow.com/questions/19342077/display-date-in-roman-numerals
        function romanize(num) {
            if (!+num)
                return false;
            var digits = String(+num).split(""),
                key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
                          "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
                          "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
                roman = "",
                i = 3;
            while (i--)
                roman = (key[+digits.pop() + (i * 10)] || "") + roman;
            return Array(+digits.join("") + 1).join("M") + roman;
        }

        function getTypeLetter(typeLetter) {
            var resTemp = "";
            if (typeLetter == "WARNING_TYPE_NOTIFICATION_GENERAL" || typeLetter == "TYPE_EN_NOTIFICATION") {
                resTemp = "NL";
            }
            else if (typeLetter == "WARNING_TYPE_REMINDER-1" || typeLetter == "WARNING_TYPE_REMINDER-2" || typeLetter == "WARNING_TYPE_REMINDER-3"
                  || typeLetter == "WARNING_TYPE_REMINDER-1_GOODS" || typeLetter == "WARNING_TYPE_REMINDER-2_GOODS" || typeLetter == "WARNING_TYPE_REMINDER-3_GOODS"
                  || typeLetter == "TYPE_EN_REMINDER_1" || typeLetter == "TYPE_EN_REMINDER_2" || typeLetter == "TYPE_EN_REMINDER_3"
                  || typeLetter == "TYPE_EN_REMINDER_1_GOODS" || typeLetter == "TYPE_EN_REMINDER_2_GOODS" || typeLetter == "TYPE_EN_REMINDER_3_GOODS") {
                resTemp = "RL";
            }
            else if (typeLetter == "WARNING_TYPE_ADMONITION-1" || typeLetter == "WARNING_TYPE_ADMONITION-2" || typeLetter == "WARNING_TYPE_ADMONITION-3" 
                  || typeLetter == "WARNING_TYPE_ADMONITION-1_GOODS" || typeLetter == "WARNING_TYPE_ADMONITION-2_GOODS" || typeLetter == "WARNING_TYPE_ADMONITION-3_GOODS"
                  || typeLetter == "TYPE_EN_ADMONITION_1" || typeLetter == "TYPE_EN_ADMONITION_2" || typeLetter == "TYPE_EN_ADMONITION_3"
                  || typeLetter == "TYPE_EN_ADMONITION_1_GOODS" || typeLetter == "TYPE_EN_ADMONITION_2_GOODS" || typeLetter == "TYPE_EN_ADMONITION_3_GOODS") {
                resTemp = "AL";
            }
            else if (typeLetter == "WARNING_TYPE_TERMINATION" || typeLetter == "WARNING_TYPE_TERMINATION_GOODS"
                  || typeLetter == "TYPE_EN_TERMINATION" || typeLetter == "TYPE_EN_TERMINATION_GOODS") {
                resTemp = "TL";
            }
            else if (typeLetter == "WARNING_TYPE_SUSPENSION" || typeLetter == "WARNING_TYPE_SUSPENSION_GOODS"
                  || typeLetter == "TYPE_EN_SUSPENSION" || typeLetter == "TYPE_EN_SUSPENSION_GOODS") {
                resTemp = "SL";
            }
            else if (typeLetter == "WARNING_TYPE_BLACKLIST" || typeLetter == "WARNING_TYPE_BLACKLIST_GOODS") {
                resTemp = "BLL";
            }
            else if (typeLetter == "WARNING_TYPE_PEMBATALAN" || typeLetter == "WARNING_TYPE_PEMBATALAN_GOODS") {
                resTemp = "CL";
            }
                                    
            return resTemp;
        }

        function getAdditionalInfo(restType) {           
        
            var idCtrc = item.data1.IDContract;// item.data1.department.IDContract;
            if (restType == "Jasa") {
                WarningLetterService.getAdditionalInfoJasa({
                    IDContract: idCtrc
                }, function (reply) {
                    if (reply.status === 200) {
                        var rest = reply.data;

                        vm.ProjectTitle = rest.ProjectTitle;
                        vm.tenderCode = rest.TenderCode;
                        vm.numbContract = rest.ContractNo;
                        vm.klasifikasi = rest.Klasifikasi;

                        var tempPM = rest.PMName;
                        getEmployeeDetail(tempPM);

                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_CONTRACT'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                });
            }

            if (restType == "Barang") {
            
                WarningLetterService.getAdditionalInfoBarang({
                    IDContract: idCtrc // cek parameter nya
                }, function (reply) {
                    if (reply.status === 200) {
                        var rest = reply.data;

                        vm.ProjectTitle = rest.ProjectTitle;
                        vm.tenderCode = rest.TenderCode;
                        vm.numbContract = rest.ContractNo;
                        vm.pmName = rest.PMName;                    
                        vm.klasifikasi = rest.Klasifikasi;

                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_CONTRACT'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                });
            }
            

            if (restType == "VHS") {
                WarningLetterService.getAdditionalInfoVHS({
                    IDContract: idCtrc
                }, function (reply) {
                    if (reply.status === 200) {
                        var rest = reply.data;

                        vm.ProjectTitle = rest.ProjectTitle;
                        vm.tenderCode = rest.TenderCode;
                        vm.numbContract = rest.ContractNo;
                        vm.klasifikasi = rest.Klasifikasi;

                        var tempPM = rest.PMName;
                        getEmployeeDetail(tempPM);

                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_CONTRACT'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                });
            }

          
        }

        function getCodeDiff(strSequence, typeInitial) {
            generateLetterNumber(strSequence, typeInitial, vm.codeDiff);
            getAdditionalInfo(vm.rest);         
        }
      
        function getNumberSequence(typeInitial) {
            WarningLetterService.getNumberSequence({
                NumberSeqType: typeInitial
            }, function (reply) {
                if (reply.status === 200) {                    
                    vm.sequenceNum = reply.data;
                    var upSequence = vm.sequenceNum.NumberSeqLast + 1;
                    var seq = upSequence.toString();
                    var strSequence = seq.padStart(6, "0");

                    getCodeDiff(strSequence, typeInitial);

                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_NUMBER'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });            
        }
        
        function generateLetterNumber(strSequenceNum, strTypeInitial, codeDiff) {

            if (vm.letterNumber && vm.originalLetterNumber && strTypeInitial === vm.originalTypeLetter) { //Jika jenis letter tidak berubah, tidak perlu generate ulang no surat.
                vm.letterNumber = vm.originalLetterNumber;
            }
            else {
                var currentTime = new Date();
                var strRomanMonth = romanize(currentTime.getMonth() + 1);
                var strYear = String(currentTime.getFullYear());
                var strResult = strSequenceNum + "/" + codeDiff + "-" + strTypeInitial + "/" + strRomanMonth + "/" + strYear;
                vm.letterNumber = strResult;
            }
        }

        vm.getWarningType = getWarningType;
        vm.selectedWarningType;
        vm.listWarningType = [];
        function getWarningType(setSelected) {
            WarningLetterService.SelectWarningType(
               function (response) {
                   if (response.status === 200) {
                       vm.listWarningType = filterWarningTypeByContractType(response.data);
                       if (setSelected) {
                           for (var i = 0; i < vm.listWarningType.length; i++) {
                               if (vm.selectedWarningType === vm.listWarningType[i].Name) {
                                   vm.selectedWarningType = vm.listWarningType[i];
                                   onChange();
                                   //setform(vm.selectedWarningType.Value);
                                   break;
                               }
                           }
                       }
                   }
                   else {
                       UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_WARNING_TYPE'));                       
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_WARNING_TYPE'));
                   return;
               });
        }
              
        vm.getWarningType_EN = getWarningType_EN;  //option for english version 
        vm.selectedWarningType_EN;
        vm.listWarningType_EN = []; 
        function getWarningType_EN(setSelected) {
            WarningLetterService.SelectWarningTypeEn(
              function (response) {
                  if (response.status === 200) {
                      vm.listWarningType_EN = filterWarningTypeByContractType(response.data);
                      if (setSelected) {
                          for (var i = 0; i < vm.listWarningType_EN.length; i++) {
                              if (vm.selectedWarningType_EN === vm.listWarningType_EN[i].Name) {
                                  vm.selectedWarningType_EN = vm.listWarningType_EN[i];
                                  onChange();
                                  //setformEN(vm.selectedWarningType_EN.Value);
                                  break;
                              }
                          }
                      }
                  }
                  else {
                      UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_WARNING_TYPE_EN'));
                      return;
                  }
              }, function (err) {
                  UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_WARNING_TYPE_EN'));
                  return;
              });
        }

        function filterWarningTypeByContractType(warningTypes){

            if (vm.rest !== "Jasa" && vm.rest !== "Barang" && vm.rest !== "VHS") {
                return warningTypes;
            }

            var filteredTypes = [];
            warningTypes.forEach(function (type) {
                if (type.Value.indexOf('NOTIFICATION') >= 0) {
                    filteredTypes.push(type);
                }
                else if (type.Value.indexOf('GOODS') < 0 && vm.rest === "Jasa") {
                    filteredTypes.push(type);
                }
                else if (type.Value.indexOf('GOODS') >= 0 && (vm.rest === "Barang" || vm.rest === "VHS")) {
                    filteredTypes.push(type);
                }
            });
            return filteredTypes;
        }

        vm.onChange = onChange;
        function onChange() {
            vm.isActive = false;
            setform(vm.selectedWarningType.Value);
          
            vm.typeLetterInitial = getTypeLetter(vm.selectedWarningType.Value)
            getNumberSequence(vm.typeLetterInitial);
        };
      
        vm.onChangeEn = onChangeEn;  //english version 
        function onChangeEn() {
            vm.isActive = false;
            setformEN(vm.selectedWarningType_EN.Value);
            
            vm.typeLetterInitial = getTypeLetter(vm.selectedWarningType_EN.Value)
            getNumberSequence(vm.typeLetterInitial);
        };

        function setform(letterType) {
            if (letterType == "WARNING_TYPE_NOTIFICATION_GENERAL") {
                vm.showDate = false;
                vm.showSuspendOps = false;
            }
            else if (letterType == "WARNING_TYPE_REMINDER-1" || letterType == "WARNING_TYPE_REMINDER-1_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "WARNING_TYPE_REMINDER-2" || letterType == "WARNING_TYPE_REMINDER-3"
                  || letterType == "WARNING_TYPE_REMINDER-2_GOODS" || letterType == "WARNING_TYPE_REMINDER-3_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "WARNING_TYPE_ADMONITION-1" || letterType == "WARNING_TYPE_ADMONITION-2" || letterType == "WARNING_TYPE_ADMONITION-3"
                  || letterType == "WARNING_TYPE_ADMONITION-1_GOODS" || letterType == "WARNING_TYPE_ADMONITION-2_GOODS" || letterType == "WARNING_TYPE_ADMONITION-3_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "WARNING_TYPE_TERMINATION" || letterType == "WARNING_TYPE_TERMINATION_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "WARNING_TYPE_SUSPENSION" || letterType == "WARNING_TYPE_SUSPENSION_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = true;
            }
        }

        function setformEN(letterType) {
            if (letterType == "TYPE_EN_NOTIFICATION") {
                vm.showDate = false;
                vm.showSuspendOps = false;
            }
            else if (letterType == "TYPE_EN_REMINDER_1" || letterType == "TYPE_EN_REMINDER_1_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "TYPE_EN_REMINDER_2" || letterType == "TYPE_EN_REMINDER_3"
                        || letterType == "TYPE_EN_REMINDER_2_GOODS" || letterType == "TYPE_EN_REMINDER_3_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "TYPE_EN_ADMONITION_1" || letterType == "TYPE_EN_ADMONITION_2" || letterType == "TYPE_EN_ADMONITION_3"
                        || letterType == "TYPE_EN_ADMONITION_1_GOODS" || letterType == "TYPE_EN_ADMONITION_2_GOODS" || letterType == "TYPE_EN_ADMONITION_3_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "TYPE_EN_TERMINATION" || letterType == "TYPE_EN_TERMINATION_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = false;
            }
            else if (letterType == "TYPE_EN_SUSPENSION" || letterType == "TYPE_EN_SUSPENSION_GOODS") {
                vm.showDate = true;
                vm.showSuspendOps = true;
            }
        }

        function getContact() {
            WarningLetterService.getContact({
                VendorID: item.data1.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    var vendorContact = reply.data;
                    vendorContact.forEach(function (con) {
                        vm.contact.push(con);
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };
        
        function getContractByVendor() {
            vm.idVendor = item.data1.VendorID;
            //get contract
        }

        function getAddress() {
            WarningLetterService.getAddress({
                VendorID: item.data1.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    var addressVendor = reply.data;
                    addressVendor.forEach(function (add) {
                        vm.address.push(add);
                    });
                
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        function getCompanyPerson() {
          
            WarningLetterService.getCompanyPerson({
                VendorID: item.data1.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    var companyPerson = reply.data;
                    
                    companyPerson.forEach(function (comper) {
                        vm.companyPerson.push(comper);
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
          
            var statusTemp;
            if(vm.selectedLang == 'IN'){
                statusTemp = vm.selectedWarningType.RefID;
            }
            if (vm.selectedLang == 'EN') {
                statusTemp = vm.selectedWarningType_EN.RefID;
            }
            if (!statusTemp) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_SELECTED_TEMPLATE'));
                return;
            }

            vm.isActive = true;

            WarningLetterService.SelectTemplate({
                Status: statusTemp //vm.selectedWarningType.RefID
            }, function (reply) {
               
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.departemen = data.List[0].TemplateDescription;
                    convertAllDateToString();
                    
                    replace();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_WARNING_TEMPLATE'));                    
                    UIControlService.unloadLoading();
                }
            }, function (err) {                
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        function convertAllDateToString() { // TIMEZONE (-)
            if (vm.warningLetter.CreatedDate) {
                vm.warningLetter.CreatedDate = UIControlService.getStrDate(vm.warningLetter.CreatedDate);
            }
            if (vm.warningLetter.StartDate) {
                vm.warningLetter.StartDate = UIControlService.getStrDate(vm.warningLetter.StartDate);
            }
            if (vm.warningLetter.EndDate) {
                vm.warningLetter.EndDate = UIControlService.getStrDate(vm.warningLetter.EndDate);
            }
        };

        function replace() {
            
            var compPers;
            var compPost;
            
            if (vm.companyPerson.length == 0 || vm.companyPerson[0] == null) {                
                compPers = " ";
                compPost = " ";
            }
            else {                
                compPers = vm.companyPerson[0].PersonName;
                compPost = vm.companyPerson[0].CompanyPosition;
            }
            
            var districtName;
            if (vm.address[0].Distric == null ) {
                districtName = " ";
            }
            else {
                districtName = vm.address[0].Distric.Name;
            }
            
            var cityName;
            if (vm.address[0].City == null) {
                if(vm.address[0].AddressDetail == null){
                    cityName= " ";
                }
                cityName = vm.address[0].AddressDetail;
            }
            else {
                cityName = vm.address[0].City.Name;
            }

            var no_contract;
            if (vm.numbContract == null || vm.numbContract == 'undefined') {
                no_contract = " ";
            }
            else {
                no_contract = vm.numbContract;
            }

          
            var phoneVendor;
            if ( vm.contact[0].Phone == null ) {
                phoneVendor = " ";
            }
            else {
                phoneVendor =  vm.contact[0].Phone;
            }

            var emailVendor;
            if ( vm.contact[0].Email == null ) {
                emailVendor = " ";
            }
            else {
                emailVendor =  vm.contact[0].Email;
            }

            var deptName;
            if (item.data1.department == null) {
                deptName = vm.departName;
            }
            else {
                deptName = item.data1.department.department.DepartmentName;
            }


            var letterDate = new Date();
            var date = String(letterDate.getDate());
            var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][letterDate.getMonth()];
            var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][letterDate.getMonth()];
            var year = String(letterDate.getFullYear());

            var strLetterDate = date + ' ' + month + ' ' + year; // indo vers
            var strLetterDate_EN = date + ' ' + month_EN + ' ' + year; // english vers

            var mydate = new Date(vm.warningLetter.CreatedDate);
            var date = String(mydate.getDate());
            var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][mydate.getMonth()];            
            var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][mydate.getMonth()];
            var year = String(mydate.getFullYear());

            var strCreatedDate = date + ' ' + month + ' ' + year; // indo vers
            var strCreatedDate_EN = date + ' ' + month_EN + ' ' + year; // english vers
                       
            //  vm.warningLetter.ComplaintDate

            var mycomplaintdate = new Date(vm.warningLetter.ComplaintDate);
            var date = String(mycomplaintdate.getDate());
            var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][mycomplaintdate.getMonth()];
            var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][mycomplaintdate.getMonth()];
            var year = String(mycomplaintdate.getFullYear());

            var strComplaintDate = date + ' ' + month + ' ' + year; // indo vers
            var strComplaintDate_EN = date + ' ' + month_EN + ' ' + year; // english vers

            var strFullDate_Start;
            var strFullDate_Start_EN;
            var strFullDate_End;
            var strFullDate_End_EN;

            if (vm.warningLetter.StartDate) {
          
                var mydate_Start = new Date(vm.warningLetter.StartDate);
                var date = String(mydate_Start.getDate());
                var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][mydate_Start.getMonth()];
                var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][mydate_Start.getMonth()];
                var year = String(mydate_Start.getFullYear());

                strFullDate_Start = date + ' ' + month + ' ' + year; // indo vers
                strFullDate_Start_EN = date + ' ' + month_EN + ' ' + year; // english vers
            } else {
                strFullDate_Start = strFullDate_Start_EN = '#tgl_berlaku_peringatan'; //agar bisa direplace saat approval L1 procsupp
            }

            if (vm.warningLetter.EndDate) {
            
                var mydate_End = new Date(vm.warningLetter.EndDate);
                var date = String(mydate_End.getDate());
                var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][mydate_End.getMonth()];
                var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][mydate_End.getMonth()];
                var year = String(mydate_End.getFullYear());

                strFullDate_End = date + ' ' + month + ' ' + year; // indo vers
                strFullDate_End_EN = date + ' ' + month_EN + ' ' + year; // english vers
            } else {
                strFullDate_End = strFullDate_End_EN = '#tgl_akhir_peringatan'; //agar bisa direplace saat approval L1 procsupp
            }

            ////-----------------------------------

            var lastStartDate;
            var lastStartDate_EN;
            var lastEndDate;
            var lastEndDate_EN;

            if (vm.LastStartDate != null) {

                var lastStart = new Date(vm.LastStartDate);
                var date = String(lastStart.getDate());
                var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][lastStart.getMonth()];
                var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][lastStart.getMonth()];
                var year = String(lastStart.getFullYear());

                lastStartDate = date + ' ' + month + ' ' + year; // indo vers
                lastStartDate_EN = date + ' ' + month_EN + ' ' + year; // english vers
            }
            else {
                lastStartDate = "-";
                lastStartDate_EN = "-";
            }

            if (vm.LastEndDate != null) {

                var lastEnd = new Date(vm.LastEndDate);
                var date = String(lastEnd.getDate());
                var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"][lastEnd.getMonth()];
                var month_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][lastEnd.getMonth()];
                var year = String(lastEnd.getFullYear());

                lastEndDate = date + ' ' + month + ' ' + year; // indo vers
                lastEndDate_EN = date + ' ' + month_EN + ' ' + year; // english vers
            }
            else {
                lastEndDate = "-";
                lastEndDate_EN = "-";
            }
            // replace parameter untuk type goods masih belum .. reminder & suspension // prepared value for forthcoming template

            if (vm.selectedLang == 'IN') {
             
                if (vm.selectedWarningType.Name === "WARNING_TYPE_NOTIFICATION_GENERAL" ) { // Notification Jasa dan Barang - IN & EN
                    vm.departemen = vm.departemen
                        .replace(/#nomor_surat/g, vm.letterNumber)
                        .replace(/#lokasi/g, vm.location)
                        .replace(/#tgl_surat/g, strCreatedDate)
                        .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                        .replace(/#alamat_vendor/g, vm.address[0].AddressInfo) 
                        .replace(/#alamat_kec_vendor/g, districtName)
                        .replace(/#alamat_kab_vendor/g, cityName)
                        //.replace(/#ket_pelanggaran/g, vm.reason1)
                        .replace(/#notification_contract/g, no_contract + " - " + vm.ProjectTitle)
                        .replace(/#jabatan_approver/g, vm.approverPosition)
                        .replace(/#nama_approver/g, vm.approverName)
                        .replace(/#nama_direktur_vendor/g, compPers)
                        .replace(/#nama_department/g, deptName)
                        .replace(/#nama_xc/g, " ")         
                    //.replace(/#email_vendor/g, toWords(rekanan.length))
                    //.replace(/#phone_vendor/g, rekanan.length)   
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_REMINDER-1" ) { // Reminder 1 Jasa - IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g, cityName)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#reminder_ke/g, "Reminder-01")
                    //.replace(/#ket_pelanggaran/g, vm.reason1)
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_xc/g, "-")
                    //.replace(/#email_vendor/g, toWords(rekanan.length))
                    //.replace(/#phone_vendor/g, rekanan.length)                                                            
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_REMINDER-2" ) { // Reminder 2 Jasa - IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#reminder_ke/g, "Reminder-02")
                    //.replace(/#ket_pelanggaran/g, vm.reason1)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_xc/g, "-")
                    //.replace(/#ket_pelanggaran/g, vm.reason1)                       
                    //.replace(/#email_vendor/g, toWords(rekanan.length))
                    //.replace(/#phone_vendor/g, rekanan.length)     
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_REMINDER-3" ) { // Reminder 3 Jasa - IN & EN
                    vm.departemen = vm.departemen
                        .replace(/#nomor_surat/g, vm.letterNumber)
                        .replace(/#lokasi/g, vm.location)
                        .replace(/#tgl_surat/g, strCreatedDate)
                        .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                        .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                        .replace(/#alamat_kec_vendor/g, districtName)
                        .replace(/#alamat_kab_vendor/g,cityName)
                        .replace(/#nama_department/g, deptName)
                        .replace(/#reminder_ke/g, "Reminder-03")
                        //.replace(/#ket_pelanggaran/g, vm.reason1)
                        //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                        .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                        .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                        .replace(/#jabatan_approver/g, vm.approverPosition)
                        .replace(/#nama_approver/g, vm.approverName)
                        .replace(/#nama_direktur_vendor/g, compPers)
                        .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                        .replace(/#nama_xc/g, "-")
                    //.replace(/#email_vendor/g, vm.contact[0].Email)
                    //.replace(/#phone_vendor/g, rekanan.length)     
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_ADMONITION-1" ) { // Admonition 1 - Jasa IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#phone_vendor/g, vm.contact[0].Phone)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#alasan_admonition1/g, vm.reason1)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#admonition_ke/g, "Admonition-01")
                    .replace(/#admonition_angka/g, "1")
                    .replace(/#admonition_huruf/g, "satu")
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#tgl_admonition/g, strComplaintDate)
                    .replace(/#nama_xc/g, "-")
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_ADMONITION-2" ) { // Admonition 2 - Jasa IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#no_telp_hp/g, phoneVendor)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#nama_direktur/g, compPers)
                    .replace(/#jabatan/g, compPost)
                    //.replace(/#alasan_admonition2/g, vm.reason1)
                    .replace(/#admonition_ke/g, "Admonition-02")
                    .replace(/#tgl_admonition/g, strComplaintDate)
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#pst_approver2/g, vm.approverPosition)
                    .replace(/#nama_approver2/g, vm.approverName)
                    .replace(/#pos_approver1/g, "Project Manager")
                    .replace(/#nama_approver1/g, vm.pmName)
                    .replace(/#nama_xc/g, "-")
                    .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#jabatan_approver1/g, vm.pmPosition) // belum dapat value nya
                    //.replace(/#tgl_admonition/g, strCreatedDate)   
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                         
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_ADMONITION-3" ) { // Admonition 3 - Jasa IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#no_telp_hp/g, phoneVendor)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#alasan_admonition1/g, vm.reason1)
                    .replace(/#admonition_ke/g, "Admonition-03")
                    .replace(/#admonition_ke_angka/g, "3")
                    .replace(/#admonition_ke_huruf/g, "tiga")
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#tgl_admonition/g, strComplaintDate)
                    .replace(/#pst_approver2/g, vm.approverPosition)
                    .replace(/#nama_approver2/g, vm.approverName)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#jabatan/g, compPost)
                    .replace(/#pos_approver1/g, "Project Manager")
                    .replace(/#nama_approver1/g, vm.pmName)
                    .replace(/#nama_xc/g, "-")
                    .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#nama_xc/g, rekanan_string);//inputan
                    //.replace(/#jabatan/g, vm.companyPerson[0].PositionRef)      
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_TERMINATION" ) { // Termination Jasa - IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#termination_contract/g, no_contract + " - " + vm.ProjectTitle)
                    //.replace(/#ket_pelanggaran/g, vm.reason1)
                    //.replace(/#panduan/g, vm.panduan)
                    .replace(/#jabatan_approver/g, vm.approverPositionL3)
                    .replace(/#nama_approver/g, vm.approverNameL3)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#tgl_admonition3_awal /g, lastStartDate)
                    .replace(/#tgl_admonition3_akhir /g, lastEndDate)
                    .replace(/#nama_xc/g, "-")
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                           
                    //.replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    //.replace(/#klasifikasi_vendor/g, )
                    //.replace(/#jabatan/g, compPost)          
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_SUSPENSION" ) { // Suspension jasa - IN & EN
                    vm.departemen = vm.departemen
                       .replace(/#nomor_surat/g, vm.letterNumber)
                       .replace(/#lokasi/g, vm.location)
                       .replace(/#tgl_surat/g, strCreatedDate)
                       .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                       .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                       .replace(/#nama_department/g, deptName)
                       //.replace(/#ket_pelanggaran/g, vm.reason1)
                       .replace(/#tgl_mulai/g, strFullDate_Start)
                       .replace(/#jabatan_approver/g, vm.approverPositionL3)
                       .replace(/#nama_approver/g, vm.approverNameL3)
                       .replace(/#nama_direktur_vendor/g, compPers)
                       .replace(/#nama_kontrak/g, no_contract + " - " + vm.ProjectTitle)
                       .replace(/#tgl_pengaduan/g, strComplaintDate)
                       .replace(/#nama_xc/g, "-")
                    //.replace(/#reminder_ke/g, "Reminder-03")
                    //.replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                    //.replace(/#alamat_kec_vendor/g, districtName)
                    //.replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                
                    //.replace(/#email_vendor/g, vm.contact[0].Email)
                    //.replace(/#phone_vendor/g, rekanan.length)    
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_REMINDER-1_GOODS" ) { // Reminder-1 Barang IN & EN // wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                          .replace(/#nomor_surat/g, vm.letterNumber)
                          .replace(/#lokasi/g, vm.location)
                          .replace(/#tgl_surat/g, strCreatedDate)
                          .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                          //.replace(/#ket_pelanggaran/g, vm.reason1)
                          .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                          .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                          .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                          .replace(/#alamat_kec_vendor/g, districtName)
                          .replace(/#alamat_kab_vendor/g,cityName)
                          .replace(/#jabatan_approver/g, vm.approverPosition)
                          .replace(/#nama_approver/g, vm.approverName)
                          .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                          .replace(/#nama_direktur_vendor/g, compPers)
                          .replace(/#reminder_ke/g, "Reminder-01")
                          .replace(/#nama_department/g, deptName)
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_REMINDER-2_GOODS" ) { // Reminder-2 Barang IN & EN// wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                          .replace(/#nomor_surat/g, vm.letterNumber)
                          .replace(/#lokasi/g, vm.location)
                          .replace(/#tgl_surat/g, strCreatedDate)
                          .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                          //.replace(/#ket_pelanggaran/g, vm.reason1)
                          .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                          .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                          .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                          .replace(/#alamat_kec_vendor/g, districtName)
                          .replace(/#alamat_kab_vendor/g,cityName)
                          .replace(/#jabatan_approver/g, vm.approverPosition)
                          .replace(/#nama_approver/g, vm.approverName)
                          .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                          .replace(/#nama_direktur_vendor/g, compPers)
                          .replace(/#reminder_ke/g, "Reminder-02")
                          .replace(/#nama_department/g, deptName)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_REMINDER-3_GOODS" ) { // Reminder-3 Barang IN & EN // wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                         .replace(/#nomor_surat/g, vm.letterNumber)
                         .replace(/#lokasi/g, vm.location)
                         .replace(/#tgl_surat/g, strCreatedDate)
                         .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                         //.replace(/#ket_pelanggaran/g, vm.reason1)
                         .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                         .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                         .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                         .replace(/#alamat_kec_vendor/g, districtName)
                         .replace(/#alamat_kab_vendor/g,cityName)
                         .replace(/#jabatan_approver/g, vm.approverPosition)
                         .replace(/#nama_approver/g, vm.approverName)
                         .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                         .replace(/#nama_direktur_vendor/g, compPers)
                         .replace(/#reminder_ke/g, "Reminder-03")
                         .replace(/#nama_department/g, deptName)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_ADMONITION-1_GOODS" ) { // Admonition-1 Barang IN & EN
                    vm.departemen = vm.departemen
                     .replace(/#nomor_surat/g, vm.letterNumber)
                     .replace(/#lokasi/g, vm.location)
                     .replace(/#tgl_surat/g, strCreatedDate)
                     .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                     .replace(/#email_vendor/g, emailVendor)
                     .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                     .replace(/#alamat_kec_vendor/g, districtName)
                     .replace(/#alamat_kab_vendor/g,cityName)
                     //.replace(/#alasan_admonition1/g, vm.reason1)
                     .replace(/#nama_direktur_vendor/g, compPers)
                     .replace(/#admonition_ke/g, "Admonition-01")
                     .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                     .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                     .replace(/#jabatan_approver/g, vm.approverPosition)
                     .replace(/#nama_approver/g, vm.approverName)
                     .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                     .replace(/#nama_department/g, deptName)
                    //.replace(/#tgl_admonition/g, strCreatedDate)
                    //.replace(/#admonition_angka/g, "1")
                    //.replace(/#admonition_huruf/g, "satu")
                    //.replace(/#phone_vendor/g, vm.contact[0].Phone)
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_ADMONITION-2_GOODS" ) { // Admonition-2 Barang IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#alasan_admonition1/g, vm.reason1)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#admonition_ke/g, "Admonition-02")
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#phone_vendor/g, vm.contact[0].Phone)
                    //.replace(/#admonition_angka/g, "1")
                    //.replace(/#admonition_huruf/g, "satu")
                    //.replace(/#tgl_admonition/g, strCreatedDate)
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_ADMONITION-3_GOODS" ) { // Admonition 3 Barang IN & EN
                    vm.departemen = vm.departemen
                      .replace(/#nomor_surat/g, vm.letterNumber)
                      .replace(/#lokasi/g, vm.location)
                      .replace(/#tgl_surat/g, strCreatedDate)
                      .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                      .replace(/#email_vendor/g, emailVendor)
                      .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                      .replace(/#alamat_kec_vendor/g, districtName)
                      .replace(/#alamat_kab_vendor/g,cityName)
                      //.replace(/#alasan_admonition1/g, vm.reason1)
                      .replace(/#nama_direktur_vendor/g, compPers)
                      .replace(/#admonition_ke/g, "Admonition-03")
                      .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                      .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                      .replace(/#jabatan_approver/g, vm.approverPosition)
                      .replace(/#nama_approver/g, vm.approverName)
                      .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                      .replace(/#nama_department/g, deptName)
                      .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#phone_vendor/g, vm.contact[0].Phone)
                    //.replace(/#tgl_admonition/g, strCreatedDate)
                    //.replace(/#admonition_angka/g, "1")
                    //.replace(/#admonition_huruf/g, "satu")
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_TERMINATION_GOODS" ) { // Termination Barang IN & EN
                    vm.departemen = vm.departemen
                   .replace(/#nomor_surat/g, vm.letterNumber)
                   .replace(/#lokasi/g, vm.location)
                   .replace(/#tgl_surat/g, strCreatedDate)
                   .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                   .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                   .replace(/#termination_contract/g, no_contract + " - " + vm.ProjectTitle)
                   //.replace(/#ket_pelanggaran/g, vm.reason1)
                   //.replace(/#panduan/g, vm.panduan)
                   .replace(/#jabatan_approver/g, vm.approverPositionL3)
                   .replace(/#nama_approver/g, vm.approverNameL3)
                   .replace(/#nama_direktur_vendor/g, compPers)
                   .replace(/#tgl_awal_admo /g, lastStartDate)
                   .replace(/#tgl_akhir_admo /g, lastEndDate)
                   .replace(/#tgl_mulai/g, strFullDate_Start)
                   .replace(/#tgl_akhir/g, strFullDate_End)
                   //.replace(/#panduan/g, vm.panduan)
                   .replace(/#jabatan_approver/g, vm.approverPosition)
                   .replace(/#nama_approver/g, vm.approverName)
                    //.replace(/#alamat_kec_vendor/g, districtName)
                    //.replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#nama_department/g, item.data1.department.department.DepartmentName)
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                           
                    //.replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    //.replace(/#klasifikasi_vendor/g, )
                    //.replace(/#jabatan/g, compPost)     
                }
                else if (vm.selectedWarningType.Name === "WARNING_TYPE_SUSPENSION_GOODS" ) { // Suspension barang IN & EN // wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                        .replace(/#nomor_surat/g, vm.letterNumber)
                        .replace(/#lokasi/g, vm.location)
                        .replace(/#tgl_surat/g, strCreatedDate)
                        .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                        .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                        .replace(/#nama_direktur_vendor/g, compPers)
                        //.replace(/#ket_pelanggaran/g, vm.reason1)
                        .replace(/#nama_kontrak/g, no_contract + " - " + vm.ProjectTitle)
                        .replace(/#jabatan_approver/g, vm.approverPositionL3)
                        .replace(/#nama_approver/g, vm.approverNameL3)
                }

            }

            if (vm.selectedLang == 'EN') {
              
                if ( vm.selectedWarningType_EN.Name === "TYPE_EN_NOTIFICATION") { // Notification Jasa dan Barang - IN & EN
                    vm.departemen = vm.departemen
                        .replace(/#nomor_surat/g, vm.letterNumber)
                        .replace(/#lokasi/g, vm.location)
                        .replace(/#tgl_surat/g, strCreatedDate)
                        .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                        .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                        .replace(/#alamat_kec_vendor/g, districtName)
                        .replace(/#alamat_kab_vendor/g,cityName)
                        //.replace(/#ket_pelanggaran/g, vm.reason1)
                        .replace(/#notification_contract/g, no_contract + " - " + vm.ProjectTitle)
                        .replace(/#jabatan_approver/g, vm.approverPosition)
                        .replace(/#nama_approver/g, vm.approverName)
                        .replace(/#nama_direktur_vendor/g, compPers)
                        .replace(/#nama_department/g, deptName)
                        .replace(/#nama_xc/g, "-")
                    //.replace(/#email_vendor/g, toWords(rekanan.length))
                    //.replace(/#phone_vendor/g, rekanan.length)   
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_REMINDER_1") { // Reminder 1 Jasa - IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#reminder_ke/g, "Reminder-01")
                    //.replace(/#ket_pelanggaran/g, vm.reason1)
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_xc/g, "-")
                    //.replace(/#email_vendor/g, toWords(rekanan.length))
                    //.replace(/#phone_vendor/g, rekanan.length)                                                            
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_REMINDER_2") { // Reminder 2 Jasa - IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#reminder_ke/g, "Reminder-02")
                    //.replace(/#ket_pelanggaran/g, vm.reason1)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_xc/g, "-")
                    .replace(/#email_vendor/g, emailVendor)
                    //.replace(/#ket_pelanggaran/g, vm.reason1)   
                    //.replace(/#nama_xc/g, rekanan_string);//inputan           
                    //.replace(/#email_vendor/g, toWords(rekanan.length))
                    //.replace(/#phone_vendor/g, rekanan.length)     
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_REMINDER_3") { // Reminder 3 Jasa - IN & EN
                    vm.departemen = vm.departemen
                        .replace(/#nomor_surat/g, vm.letterNumber)
                        .replace(/#lokasi/g, vm.location)
                        .replace(/#tgl_surat/g, strCreatedDate)
                        .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                        .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                        .replace(/#alamat_kec_vendor/g, districtName)
                        .replace(/#alamat_kab_vendor/g,cityName)
                        .replace(/#nama_department/g, deptName)
                        .replace(/#reminder_ke/g, "Reminder-03")
                        //.replace(/#ket_pelanggaran/g, vm.reason1)
                        //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                        .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                        .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                        .replace(/#jabatan_approver/g, vm.approverPosition)
                        .replace(/#nama_approver/g, vm.approverName)
                        .replace(/#nama_direktur_vendor/g, compPers)
                        .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                        .replace(/#nama_xc/g, "-")
                        .replace(/#email_vendor/g, emailVendor)
                    //.replace(/#email_vendor/g, vm.contact[0].Email)
                    //.replace(/#phone_vendor/g, rekanan.length)     
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_ADMONITION_1") { // Admonition 1 - Jasa IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#no_telp_hp/g, phoneVendor)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#alasan_admonition1/g, vm.reason1)
                    .replace(/#nama_direktur/g, compPers)
                    .replace(/#jabatan/g, compPost)
                    .replace(/#admonition_ke/g, "Admonition-01")
                    .replace(/#admonition_angka/g, "1")
                    .replace(/#admonition_huruf/g, "satu")
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#pst_approver2/g, vm.approverPosition)
                    .replace(/#nama_approver2/g, vm.approverName)
                    .replace(/#pos_approver1/g, "Project Manager")
                    .replace(/#nama_approver1/g, vm.pmName)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#tgl_admonition/g, strComplaintDate)
                    .replace(/#nama_xc/g, " ")
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_ADMONITION_2") { // Admonition 2 - Jasa IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#no_telp_hp/g, phoneVendor)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#nama_direktur/g, compPers)
                    .replace(/#jabatan/g, compPost)
                    //.replace(/#alasan_admonition2/g, vm.reason1)
                    .replace(/#admonition_ke/g, "Admonition-02")
                    .replace(/#tgl_admonition/g, strComplaintDate)
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#pst_approver2/g, vm.approverPosition)
                    .replace(/#nama_approver2/g, vm.approverName)
                    .replace(/#pos_approver1/g, "Project Manager")
                    .replace(/#nama_approver1/g, vm.pmName)
                    .replace(/#nama_xc/g, " ")
                    .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#jabatan_approver1/g, vm.pmPosition) // belum dapat value nya
                    //.replace(/#tgl_admonition/g, strCreatedDate)   
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                         
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_ADMONITION_3") { // Admonition 3 - Jasa IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#no_telp_hp/g, phoneVendor)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#alasan_admonition1/g, vm.reason1)
                    .replace(/#admonition_ke/g, "Admonition-03")
                    .replace(/#admonition_ke_angka/g, "3")
                    .replace(/#admonition_ke_huruf/g, "tiga")
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#tgl_admonition/g, strComplaintDate)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_direktur/g, compPers)
                    .replace(/#jabatan/g, compPost)
                    .replace(/#pst_approver2/g, vm.approverPosition)
                    .replace(/#nama_approver2/g, vm.approverName)
                    .replace(/#pos_approver1/g, "Project Manager")
                    .replace(/#nama_approver1/g, vm.pmName)
                    .replace(/#nama_xc/g, "-")
                    .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#nama_xc/g, rekanan_string);//inputan
                    //.replace(/#jabatan/g, vm.companyPerson[0].PositionRef)      
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_TERMINATION") { // Termination Jasa - IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g, cityName)
                    .replace(/#no_telp_hp/g, phoneVendor)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#termination_contract/g, no_contract + " - " + vm.ProjectTitle)
                    //.replace(/#ket_pelanggaran/g, vm.reason1)                    
                    .replace(/#jabatan_approver/g, vm.approverPositionL3)
                    .replace(/#nama_approver/g, vm.approverNameL3)
                    .replace(/#nama_direktur/g, compPers)
                    .replace(/#nama_xc/g, " ")
                    //.replace(/#panduan/g, vm.panduan)
                    //.replace(/#tgl_admonition3_awal /g, lastStartDate)
                    //.replace(/#tgl_admonition3_akhir /g, lastEndDate)
                  
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                           
                    //.replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    //.replace(/#klasifikasi_vendor/g, )
                    //.replace(/#jabatan/g, compPost)          
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_SUSPENSION") { // Suspension jasa - IN & EN
                    vm.departemen = vm.departemen
                       .replace(/#nomor_surat/g, vm.letterNumber)
                       .replace(/#lokasi/g, vm.location)
                       .replace(/#tgl_surat/g, strCreatedDate)
                       .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                       .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                       .replace(/#nama_department/g, deptName)
                       //.replace(/#ket_pelanggaran/g, vm.reason1)
                       .replace(/#tgl_mulai/g, strFullDate_Start)
                       .replace(/#jabatan_approver/g, vm.approverPositionL3)
                       .replace(/#nama_approver/g, vm.approverNameL3)
                       .replace(/#nama_direktur_vendor/g, compPers)
                       .replace(/#nama_kontrak/g, no_contract + " - " + vm.ProjectTitle)
                       .replace(/#tgl_pengaduan/g, strComplaintDate)
                    //.replace(/#reminder_ke/g, "Reminder-03")
                    //.replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                    //.replace(/#alamat_kec_vendor/g, districtName)
                    //.replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                
                    //.replace(/#email_vendor/g, vm.contact[0].Email)
                    //.replace(/#phone_vendor/g, rekanan.length)    
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_REMINDER_1_GOODS") { // Reminder-1 Barang IN & EN // wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                          .replace(/#nomor_surat/g, vm.letterNumber)
                          .replace(/#lokasi/g, vm.location)
                          .replace(/#tgl_surat/g, strCreatedDate)
                          .replace(/#nama_department/g, deptName)
                          //.replace(/#ket_pelanggaran/g, vm.reason1)
                          .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                          .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                          .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                          .replace(/#alamat_kec_vendor/g, districtName)
                          .replace(/#alamat_kab_vendor/g,cityName)
                          .replace(/#jabatan_approver/g, vm.approverPosition)
                          .replace(/#nama_approver/g, vm.approverName)
                          .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                          .replace(/#nama_direktur_vendor/g, compPers)
                          .replace(/#reminder_ke/g, "Reminder-01")
                          .replace(/#nama_department/g, deptName)
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_REMINDER_2_GOODS") { // Reminder-2 Barang IN & EN// wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                          .replace(/#nomor_surat/g, vm.letterNumber)
                          .replace(/#lokasi/g, vm.location)
                          .replace(/#tgl_surat/g, strCreatedDate)
                          .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                          //.replace(/#ket_pelanggaran/g, vm.reason1)
                          .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                          .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                          .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                          .replace(/#alamat_kec_vendor/g, districtName)
                          .replace(/#alamat_kab_vendor/g,cityName)
                          .replace(/#jabatan_approver/g, vm.approverPosition)
                          .replace(/#nama_approver/g, vm.approverName)
                          .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                          .replace(/#nama_direktur_vendor/g, compPers)
                          .replace(/#reminder_ke/g, "Reminder-02")
                          .replace(/#nama_department/g, deptName)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_REMINDER_3_GOODS") { // Reminder-3 Barang IN & EN // wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                         .replace(/#nomor_surat/g, vm.letterNumber)
                         .replace(/#lokasi/g, vm.location)
                         .replace(/#tgl_surat/g, strCreatedDate)
                         .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                         //.replace(/#ket_pelanggaran/g, vm.reason1)
                         .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                         .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                         .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                         .replace(/#alamat_kec_vendor/g, districtName)
                         .replace(/#alamat_kab_vendor/g,cityName)
                         .replace(/#jabatan_approver/g, vm.approverPosition)
                         .replace(/#nama_approver/g, vm.approverName)
                         .replace(/#reminder_contract/g, no_contract + " - " + vm.ProjectTitle)
                         .replace(/#nama_direktur_vendor/g, compPers)
                         .replace(/#reminder_ke/g, "Reminder-03")
                         .replace(/#nama_department/g, deptName)
                    //.replace(/#sanksi_pelanggaran/g, vm.konsekuensi)
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_ADMONITION_1_GOODS") { // Admonition-1 Barang IN & EN
                    vm.departemen = vm.departemen
                     .replace(/#nomor_surat/g, vm.letterNumber)
                     .replace(/#lokasi/g, vm.location)
                     .replace(/#tgl_surat/g, strCreatedDate)
                     .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                     .replace(/#email_vendor/g, emailVendor)
                     .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                     .replace(/#alamat_kec_vendor/g, districtName)
                     .replace(/#alamat_kab_vendor/g,cityName)
                     //.replace(/#alasan_admonition1/g, vm.reason1)
                     .replace(/#nama_direktur_vendor/g, compPers)
                     .replace(/#admonition_ke/g, "Admonition-01")
                     .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                     .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                     .replace(/#jabatan_approver/g, vm.approverPosition)
                     .replace(/#nama_approver/g, vm.approverName)
                     .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                     .replace(/#nama_department/g, deptName)
                    //.replace(/#tgl_admonition/g, strCreatedDate)
                    //.replace(/#admonition_angka/g, "1")
                    //.replace(/#admonition_huruf/g, "satu")
                    //.replace(/#phone_vendor/g, vm.contact[0].Phone)
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_ADMONITION_2_GOODS") { // Admonition-2 Barang IN & EN
                    vm.departemen = vm.departemen
                    .replace(/#nomor_surat/g, vm.letterNumber)
                    .replace(/#lokasi/g, vm.location)
                    .replace(/#tgl_surat/g, strCreatedDate)
                    .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                    .replace(/#email_vendor/g, emailVendor)
                    .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                    .replace(/#alamat_kec_vendor/g, districtName)
                    .replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#alasan_admonition1/g, vm.reason1)
                    .replace(/#nama_direktur_vendor/g, compPers)
                    .replace(/#admonition_ke/g, "Admonition-02")
                    .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                    .replace(/#jabatan_approver/g, vm.approverPosition)
                    .replace(/#nama_approver/g, vm.approverName)
                    .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                    .replace(/#nama_department/g, deptName)
                    .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#phone_vendor/g, vm.contact[0].Phone)
                    //.replace(/#admonition_angka/g, "1")
                    //.replace(/#admonition_huruf/g, "satu")
                    //.replace(/#tgl_admonition/g, strCreatedDate)
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_ADMONITION_3_GOODS") { // Admonition 3 Barang IN & EN
                    vm.departemen = vm.departemen
                      .replace(/#nomor_surat/g, vm.letterNumber)
                      .replace(/#lokasi/g, vm.location)
                      .replace(/#tgl_surat/g, strCreatedDate)
                      .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                      .replace(/#email_vendor/g, emailVendor)
                      .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                      .replace(/#alamat_kec_vendor/g, districtName)
                      .replace(/#alamat_kab_vendor/g,cityName)
                      //.replace(/#alasan_admonition1/g, vm.reason1)
                      .replace(/#nama_direktur_vendor/g, compPers)
                      .replace(/#admonition_ke/g, "Admonition-03")
                      .replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                      .replace(/#tgl_akhir_peringatan/g, strFullDate_End)
                      .replace(/#jabatan_approver/g, vm.approverPosition)
                      .replace(/#nama_approver/g, vm.approverName)
                      .replace(/#admonition_contract/g, no_contract + " - " + vm.ProjectTitle)
                      .replace(/#nama_department/g, deptName)
                      .replace(/#klasifikasi/g, vm.klasifikasi)
                    //.replace(/#phone_vendor/g, vm.contact[0].Phone)
                    //.replace(/#tgl_admonition/g, strCreatedDate)
                    //.replace(/#admonition_angka/g, "1")
                    //.replace(/#admonition_huruf/g, "satu")
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_TERMINATION_GOODS") { // Termination Barang IN & EN
                    vm.departemen = vm.departemen
                   .replace(/#nomor_surat/g, vm.letterNumber)
                   .replace(/#lokasi/g, vm.location)
                   .replace(/#tgl_surat/g, strCreatedDate)
                   .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)
                   .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)
                   .replace(/#termination_contract/g, no_contract + " - " + vm.ProjectTitle)
                   //.replace(/#ket_pelanggaran/g, vm.reason1)
                   //.replace(/#panduan/g, vm.panduan)
                   .replace(/#jabatan_approver/g, vm.approverPosition)
                   .replace(/#nama_approver/g, vm.approverName)
                   .replace(/#nama_direktur_vendor/g, compPers)
                   .replace(/#tgl_admonition3_awal /g, lastStartDate)
                   .replace(/#tgl_admonition3_akhir /g, lastEndDate)
                   .replace(/#tgl_mulai/g, strFullDate_Start)
                   .replace(/#tgl_akhir/g, strFullDate_End)
                   //.replace(/#panduan/g, vm.panduan)
                   .replace(/#jabatan_approver/g, vm.approverPositionL3)
                   .replace(/#nama_approver/g, vm.approverNameL3)
                    //.replace(/#alamat_kec_vendor/g, districtName)
                    //.replace(/#alamat_kab_vendor/g,cityName)
                    //.replace(/#nama_department/g, item.data1.department.department.DepartmentName)
                    //.replace(/#nama_xc/g, rekanan_string);//inputan                           
                    //.replace(/#tgl_berlaku_peringatan/g, strFullDate_Start)
                    //.replace(/#klasifikasi_vendor/g, )
                    //.replace(/#jabatan/g, compPost)     
                }
                else if (vm.selectedWarningType_EN.Name === "TYPE_EN_SUSPENSION_GOODS") { // Suspension barang IN & EN // wait for template applied in database, open comment when ready to apply
                    vm.departemen = vm.departemen
                        .replace(/#nomor_surat/g, vm.letterNumber)
                        .replace(/#lokasi/g, vm.location)
                        .replace(/#tgl_surat/g, strCreatedDate)
                        .replace(/#nama_vendor/g, vm.businessName + item.data1.Vendor.VendorName)                        
                        .replace(/#alamat_vendor/g, vm.address[0].AddressInfo)                        
                        .replace(/#nama_direktur_vendor/g, compPers)
                        //.replace(/#ket_pelanggaran/g, vm.reason1)
                        .replace(/#nama_kontrak/g, no_contract + " - " + vm.ProjectTitle)                       
                        .replace(/#jabatan_approver/g, vm.approverPositionL3)
                        .replace(/#nama_approver/g, vm.approverNameL3)
                    //.replace(/#tgl_pengaduan/g, strComplaintDate)                        
                    //.replace(/#nama_department/g, deptName)
                }
            }
            
            //Tambah Paragraf Akumulasi
            var letterType;
            var language;
            if (vm.selectedLang == 'IN') {
                language = 'id';
                letterType = vm.selectedWarningType.Name;
            }
            if (vm.selectedLang == 'EN') {
                language = 'en';
                letterType = vm.selectedWarningType_EN.Name;
            }

            WarningLetterService.getAccumulationPar({
                LetterID: item.data1.LetterID,
                LetterType: letterType,
                Remark: language, //lang
            }, function (reply) {
                if (reply.data) {
                    vm.departemen = vm.departemen.replace(/#ket_akumulasi/g, reply.data);
                }
                else {
                    vm.departemen = vm.departemen.replace(/#ket_akumulasi/g, '');
                }

                //vm.trusteddepartemen = $sce.trustAsHtml(vm.departemen);
                vm.trusteddepartemen = vm.departemen;
            }, function (err) {
                vm.departemen = vm.departemen.replace(/#ket_akumulasi/g, '');

                //vm.trusteddepartemen = $sce.trustAsHtml(vm.departemen);
                vm.trusteddepartemen = vm.departemen;
            });
        };

        //Supaya muncul di date picker saat awal load
        function convertToDate() {
            if (vm.warningLetter.CreatedDate) {
                vm.warningLetter.CreatedDate = new Date(Date.parse(vm.warningLetter.CreatedDate));
            }
            if (item.data1.CreatedDateDate) {
                item.data1.CreatedDate = new Date(Date.parse(item.data1.CreatedDate));
            }
            if (vm.warningLetter.StartDate) {
                vm.warningLetter.StartDate = new Date(Date.parse(vm.warningLetter.StartDate));
            }
            if (vm.warningLetter.EndDate) {
                vm.warningLetter.EndDate = new Date(Date.parse(vm.warningLetter.EndDate));
            }
        }

        vm.SimpanClick = SimpanClick;
        function SimpanClick() {
            var letterType;

            if(vm.selectedLang =='IN'){
                letterType = vm.selectedWarningType.Name;
            }
            if(vm.selectedLang == 'EN'){
                letterType = vm.selectedWarningType_EN.Name;
            }
            
            if (vm.typeLetterInitial === 'SL' && !vm.isSuspendByField && !vm.isSuspendContract) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_SUSPEND');
                return;
            }
            
            if (vm.typeLetterInitial !== 'SL'){
                vm.isSuspendByField = null;
                vm.isSuspendContract = null;
            }

            UIControlService.loadLoadingModal($filter('translate')('MESSAGE.LOADING'));
                WarningLetterService.UpdateTemplate({
                    LetterID: item.data1.LetterID,
                    TemplateDoc:vm.trusteddepartemen,// vm.departemen,
                    LetterType: letterType,
                    StartDate: vm.warningLetter.StartDate,
                    EndDate: vm.warningLetter.EndDate,
                    LetterNumber: vm.letterNumber,
                    IsSuspendByField: vm.isSuspendByField,
                    IsSuspendContract: vm.isSuspendContract,
                    IsByCommodity: vm.isByCommodity
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {                       
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SAVE_WARNING_LETTER'));                        
                        $uibModalInstance.close();
                        if (vm.letterNumber !== vm.originalLetterNumber) { //Tidak perlu update sequence jika nomor surat tetap
                            updateNumberSequence(); // open if sure ^^
                        }
                    }
                    else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_WARNING_LETTER'));                        
                        return;  
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_WARNING_LETTER'));
                    if (err.substr(0, 4) === 'ERR_') {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.' + err));
                    }
                    UIControlService.unloadLoadingModal();
                });
            }

        function updateNumberSequence() {
            var type = vm.sequenceNum.NumberSeqType;

            WarningLetterService.updateNumberSequence({
                NumberSeqType: type
            }, function (reply) {
                if (reply.status === 200) {                   
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_UPDATE_SEQUENCE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        }
     
    }
})();