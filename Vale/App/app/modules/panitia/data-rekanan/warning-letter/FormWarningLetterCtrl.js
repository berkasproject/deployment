﻿(function () {
    'use strict';

    angular.module("app").controller("FormWarningLetterCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'RoleService', 'UIControlService', 'item', '$uibModal', '$uibModalInstance', '$state', 'UploaderService', 'UploadFileConfigService', 'GlobalConstantService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, WarningLetterService,
        RoleService, UIControlService, item, $uibModal, $uibModalInstance, $state, UploaderService, UploadFileConfigService, GlobalConstantService) {
        var vm = this;
        var page_id = 141;

        vm.isAdd = item.act;
        vm.Area;
        vm.LetterID;
        vm.Description="";
        vm.action = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.init = init;
        vm.warningLetter = {};
        vm.isCalendarOpened = [false, false, false, false];
    
        //vm.listVendorName = [];
        vm.listDepartment = [];
        vm.listWarning = [];
        //vm.listContractJasa = [];
        //vm.listContractBarang = [];
        //vm.listContractVHS = [];
        vm.listContract = [];
        vm.listComplain = [];

        vm.selectedVendorName;
        vm.selectedDepartment;
        vm.selectedWarning;
        //vm.selectedContJasa;
        //vm.selectedContBarang;
        //vm.selectedContVHS;
        vm.selectedCont;
        vm.selectedComplain;

        vm.vendorID;
        vm.idContract;
        vm.noContract;
        //vm.isContractAvail = true;
        vm.fileUpload;
        vm.DocUrl;
        vm.endpointAttachment = GlobalConstantService.getConstant('api') + "/";
     
        function init() {
            if (vm.isAdd === true) {
                vm.action = "Tambah";
                vm.warningLetter.RequestedDate = new Date();
                getCurrentUser();
                getCurrentUserDept();                
            }
            else {
                vm.action = "Ubah";
                vm.warningLetter.RequestedDate = item.item.Date;
                vm.ReporterName = item.item.ReporterName;
                vm.Description = item.item.Description;
                vm.idContract = item.item.IDContract;
                vm.noContract = item.item.ContractOrPONumber
                vm.vendorID = item.item.vendor.VendorID;
                vm.DocUrl = item.item.DocUrl;
                vm.selectedVendorName = item.item.vendor;
                getContractByVendor_Jasa();
                getWarning();
            }

            $translatePartialLoader.addPart('master-complaint');

            getDepartment();
            //getVendorName();
            getWarning();
          
            convertToDate();

            UploadFileConfigService.getByPageName("PAGE.ADMIN.COMPLAIN", function (response) {
                UIControlService.unloadLoadingModal();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                    
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                UIControlService.unloadLoadingModal(); 
                return;
            });
        }

        // ----- > upload part

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function validateForm() {
            if (vm.ReporterName == "" || vm.ReporterName == null) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_EMPTY_REPORTER'));
                return false;
            }
            if (vm.selectedDepartment == null) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_EMPTY_DEPARTMENT'));
                return false;
            }
            if (vm.selectedVendorName == null) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_EMPTY_VENDOR'));
                return false;
            }
            if (vm.selectedComplain == null) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_EMPTY_COMPLAINT'));
                return false;
            }
            if (vm.idContract == null) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_EMPTY_CONTRACT'));
                return false;
            }
            if (vm.Description == "" || vm.Description == null) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_EMPTY_DESC'));
                return false;
            }
            return true;
        }

        function upload(file, config, types) {

            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            //   vm.Id = vm.CPRDataId;
            if (!validateForm()) {
                return;
            }

            if (vm.idContract != null && vm.selectedDepartment != null && vm.selectedComplain != null && vm.selectedVendorName != null && vm.ReporterName != null && vm.Description != null) {
                UIControlService.loadLoadingModal("");
                UploaderService.uploadSingleFileComplaint(0, "", file, size, types,
                  function (reply) {
                      if (reply.status == 200) {
                          UIControlService.unloadLoadingModal();
                          var url = reply.data.Url;
                          var size = reply.data.FileLength;

                          vm.DocUrl = url;                        

                          //UIControlService.loadLoadingModal("");
                          //WarningLetterService.checkExistingRecord({
                          //    Date: vm.warningLetter.RequestedDate,
                          //    ReporterName: vm.ReporterName,
                          //    ReporterID: vm.selectedDepartment.DepartmentID,
                          //    VendorID: vm.selectedVendorName.VendorID,
                          //    ComplainTypeID: vm.selectedComplain.TypeID,
                          //    Description: vm.Description,
                          //    IDContract: vm.idContract,
                          //    ContractOrPONumber: vm.noContract,
                          //    DocUrl: vm.DocUrl
                          //    //WarningID: vm.selectedComplain.RefID,
                          //}, function (reply) {
                          //    UIControlService.unloadLoadingModal();
                          //    if (reply.status === 200) {
                                
                          //        var isExist = reply.data;

                          //        if (isExist == true) {
                          //            UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.WARNING_ISEXIST'));
                          //        }
                          //        else {
                            UIControlService.loadLoadingModal("");
                            convertAllDateToString();
                            WarningLetterService.insert({
                                Date: vm.warningLetter.RequestedDate,
                                ReporterName: vm.ReporterName,
                                ReporterID: vm.selectedDepartment.DepartmentID,
                                VendorID: vm.selectedVendorName.VendorID,
                                ComplainTypeID: vm.selectedComplain.TypeID,
                                Description: vm.Description,
                                IDContract: vm.idContract,
                                ContractOrPONumber: vm.noContract,
                                DocUrl: vm.DocUrl
                                //WarningID: vm.selectedComplain.RefID,
                            }, function (reply) {
                                UIControlService.unloadLoadingModal();
                                if (reply.status === 200) {

                                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SAVE_DATA'));
                                    $uibModalInstance.close();
                                }
                                else {
                                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                                    return;
                                }
                            }, function (err) {
                                convertToDate();
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                                UIControlService.unloadLoadingModal();
                            });
                          //        }

                          //    }
                          //    else {
                          //        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_ISEXIST'));
                          //        return;
                          //    }
                          //}, function (err) {
                          //    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_ISEXIST'));
                          //    UIControlService.unloadLoadingModal();
                          //});
                        
                      } else {
                          UIControlService.unloadLoadingModal();
                          UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                      }
                  }, function (err) {
                      UIControlService.unloadLoadingModal();
                      UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                  });
            }          
        }

        // ----- > end of upload part


        vm.getCurrentUser = getCurrentUser;
        function getCurrentUser() {
            WarningLetterService.getCurrentEmpID(
              function (response) {
                  if (response.status === 200) {                    
                    
                      getCurrentUserDetail(response.data);
                  }
                  else {
                      UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CURR_USER'));
                      return;
                  }
              }, function (err) {
                  UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                  return;
              });
        }

        vm.getCurrentUserDetail = getCurrentUserDetail;
        function getCurrentUserDetail(id) {
            WarningLetterService.getEmployeeDetail({
                EmployeeID: id
            }, function (reply) {
                if (reply.status === 200) {
                    var info = reply.data;
                   
                    vm.ReporterName = info.FullName + " " + info.SurName;;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EMP'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        }

        vm.getCurrentUserDept = getCurrentUserDept;
        function getCurrentUserDept() {
            WarningLetterService.getCurrentUserDepartment(
              function (response) {
                  if (response.status === 200) {                   
                   
                      vm.currentDept = response.data;
                      getDepartment();
                  }
                  else {
                      UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CURR_USER'));
                      return;
                  }
              }, function (err) {
                  UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                  return;
              });
        }


        vm.getDepartment = getDepartment;          
        function getDepartment() {
            WarningLetterService.SelectDepartment(
               function (response) {
                   if (response.status === 200) {
                       vm.listDepartment = response.data;
                       if (vm.isAdd === true) {
                           for (var i = 0; i < vm.listDepartment.length; i++) {
                               if (vm.currentDept === vm.listDepartment[i].DepartmentName) {
                                   vm.selectedDepartment = vm.listDepartment[i];
                                   break;
                               }
                           }
                       }
                     
                       
                       if (vm.isAdd === false) {
                           for (var i = 0; i < vm.listDepartment.length; i++) {
                               if (item.item.department.DepartmentID === vm.listDepartment[i].DepartmentID) {
                                   vm.selectedDepartment = vm.listDepartment[i];
                                   break;
                               }
                           }
                       }
                   }
                   else {
                       UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_EMPLOYEE')); 
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                   return;
               });
        }
        
        /*
        vm.getVendorName = getVendorName;
        function getVendorName() {
            WarningLetterService.SelectVendorName(
               function (response) {
                   if (response.status === 200) {
                       vm.listVendorName = response.data;
                       if (vm.isAdd === false) {
                           for (var i = 0; i < vm.listVendorName.length; i++) {
                               if (item.item.vendor.VendorID === vm.listVendorName[i].VendorID) {
                                   vm.selectedVendorName = vm.listVendorName[i];
                                   //vm.ngModelOptionsSelected = vm.listVendorName[i];
                                   break;
                               }
                           }
                       }
                   }
                   else {
                       UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VENDOR'));
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                   return;
               });
        }
        */

        vm.selectedDepartmentOpt = selectedDepartmentOpt;
        function selectedDepartmentOpt() {
          
        }

        /*
        vm.selectedVendorOpt = selectedVendorOpt;
        function selectedVendorOpt() {                   
            getContractByVendor_Jasa();
        }         
        */

        vm.selectVendor = selectVendor;
        function selectVendor() {

            var item = {};

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/pengaduan/pilihVendor.modal.html',
                controller: 'PengaduanPilihVendorController',
                controllerAs: 'pilVendorCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function (selectedVendor) {
                vm.selectedVendorName = selectedVendor;
                getContractByVendor_Jasa();
            });
        }

        vm.selectedContractOpt = selectedContractOpt;
        function selectedContractOpt() {
            setIdAndNoContract();
        }

        function setIdAndNoContract() {

            /*
            if (vm.selectedContJasa != null || vm.selectedContJasa != undefined) {
                vm.idContract = vm.selectedContJasa.ID;
                vm.noContract = vm.selectedContJasa.ContractNo;
            }
            if (vm.selectedContBarang != null || vm.selectedContBarang != undefined) {
                vm.idContract = vm.selectedContBarang.ID;
                vm.noContract = vm.selectedContBarang.PONumber;
            }
            if (vm.selectedContVHS != null || vm.selectedContVHS != undefined) {
                vm.idContract = vm.selectedContVHS.ID;
                vm.noContract = vm.selectedContVHS.SAPContractNo;
            }
            */
            vm.idContract = vm.selectedCont.ID;
            vm.noContract = vm.selectedCont.ContractNo;
            //console.log("id contract :" + vm.idContract);
        }

        vm.getContractByVendor_Jasa = getContractByVendor_Jasa;
        function getContractByVendor_Jasa() {
            //vm.listContractJasa = [];
            //vm.listContractBarang = [];
            //vm.listContractVHS = [];
            vm.listContract - [];

            vm.idContract = null;
            vm.noContract = null;
            //if (vm.isAdd === true) {
            vm.vendorID = vm.selectedVendorName.VendorID;               
            //}
            //else {
            //    if (vm.selectedVendorName.VendorID == item.item.vendor.VendorID) {
            //        vm.vendorID = item.item.vendor.VendorID;
            //    }
            //    else {
            //        vm.vendorID = vm.selectedVendorName.VendorID;
            //    }                            
            //}

            WarningLetterService.SelectContractByVendor_Jasa({
                VendorID: vm.vendorID //vm.selectedVendorName.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    //vm.isContractAvail = true;    
                    var listContractJasa = reply.data;
                    listContractJasa.forEach(function (con) {
                        vm.listContract.push({
                            ID : con.ID,
                            ContractNo: con.ContractNo
                        });
                    });
                    getContractByVendor_Barang();
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CONTRACT_JASA'));
                    return;
                }
            }, function (err) {
                // UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                return;
            });
        }
      
        vm.getContractByVendor_Barang = getContractByVendor_Barang;
        function getContractByVendor_Barang() {
            //vm.listContractJasa = [];
            //vm.listContractBarang = [];
            //vm.listContractVHS = [];
            
            //vm.idContract = null;
            //vm.noContract = null;
            //if (vm.isAdd === true) {
            //vm.vendorID = vm.selectedVendorName.VendorID;
            //}
            //else {
            //    vm.vendorID = item.item.vendor.VendorID;
            //}
            
            WarningLetterService.SelectContractByVendor_BarangPO({ //SelectContractByVendor_Barang
                VendorID: vm.vendorID //vm.selectedVendorName.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    //vm.isContractAvail = true;
                    var listContractBarang = reply.data;
                    listContractBarang.forEach(function (con) {
                        vm.listContract.push({
                            ID: con.ID,
                            ContractNo: con.PONumber
                        });
                    });
                    getContractByVendor_VHS();
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CONTRACT_BARANG'));
                    return;
                }
            }, function (err) {
                // UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                return;
            });
        }

        vm.getContractByVendor_VHS = getContractByVendor_VHS;
        function getContractByVendor_VHS() {
            //vm.listContractJasa = [];
            //vm.listContractBarang = [];
            //vm.listContractVHS = [];
      
            //vm.idContract = null;
            //vm.noContract = null;
            //if (vm.isAdd === true) {
            //vm.vendorID = vm.selectedVendorName.VendorID;
            //}
            //else {
            //    vm.vendorID = item.item.vendor.VendorID;
            //}

            WarningLetterService.SelectContractByVendor_VHS({
                VendorID: vm.vendorID //vm.selectedVendorName.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    //vm.isContractAvail = true;
                    var listContractVHS = reply.data;
                    listContractVHS.forEach(function (con) {
                        vm.listContract.push({
                            ID: con.ID,
                            ContractNo: con.SAPContractNo
                        });
                    });
                    /*
                    if (vm.listContractVHS.length <= 0) {                      
                        vm.isContractAvail = false;                       
                    }
                    */
                    if (vm.isAdd === false) {
                        for (var i = 0; i < vm.listContract.length; i++) {
                            if (item.item.IDContract === vm.listContract[i].ID && item.item.ContractOrPONumber === vm.listContract[i].ContractNo) { //id contract
                                vm.selectedCont = vm.listContract[i];
                                setIdAndNoContract();
                                break;
                            }
                        }
                    }
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CONTRACT_VHS'));
                    return;
                }
            }, function (err) {
            //  UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                return;
            });
        }

        vm.getWarning = getWarning;        
        function getWarning() {
                    
            WarningLetterService.getComplainType(
               function (response) {
                   if (response.status === 200) {
                       vm.listComplain = response.data;                       
                       if (vm.isAdd === false) {
                           for (var i = 0; i < vm.listComplain.length; i++) {
                               if (item.item.ComplainTypeID === vm.listComplain[i].TypeID) {
                                   vm.selectedComplain = vm.listComplain[i];
                                   break;
                               }
                           }
                       }
                   }
                   else {
                       UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_TYPE'));
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                   return;
               });
        }
        
        //reference: --https://stackoverflow.com/questions/22837789/disable-specific-dates-in-angularjs-date-picker/39458715
        vm.dateOptions = {
            maxDate: new Date() // disabled future date from current date - added 
        };

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;     
        };
             
        function convertAllDateToString() { // TIMEZONE (-)
            if (vm.warningLetter.RequestedDate) {
                vm.warningLetter.RequestedDate = UIControlService.getStrDate(vm.warningLetter.RequestedDate);
            }
            if (vm.warningLetter.StartDate) {
                vm.warningLetter.StartDate = UIControlService.getStrDate(vm.warningLetter.StartDate);
            }
            if (vm.warningLetter.EndDate) {
                vm.warningLetter.EndDate = UIControlService.getStrDate(vm.warningLetter.EndDate);
            }
        };

        //Supaya muncul di date picker saat awal load
        function convertToDate() {
            if (vm.warningLetter.RequestedDate) {
                vm.warningLetter.RequestedDate = new Date(Date.parse(vm.warningLetter.RequestedDate));
            }
            if (vm.warningLetter.StartDate) {
                vm.warningLetter.StartDate = new Date(Date.parse(vm.warningLetter.StartDate));
            }
            if (vm.warningLetter.EndDate) {
                vm.warningLetter.EndDate = new Date(Date.parse(vm.warningLetter.EndDate));
            }
        }

        vm.simpan = simpan;
        function simpan() {
         //   UIControlService.loadLoadingModal($filter('translate')('MESSAGE.LOADING'));          

            if (vm.action === "Tambah") {
            
                if (!vm.DocUrl && !vm.fileUpload) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                    return;
                }

                if (vm.fileUpload) {
                    uploadFile();
                }
               
            }
            else if (vm.action === "Ubah") {

                if (vm.fileUpload) {

                    var size = vm.idFileSize.Size;
                    var unit = vm.idFileSize.SizeUnitName;
                    if (unit == 'SIZE_UNIT_KB') {
                        size *= 1024;
                        vm.flag = 0;
                    }
                    if (unit == 'SIZE_UNIT_MB') {
                        size *= (1024 * 1024);
                        vm.flag = 1;
                    }

                    UIControlService.loadLoadingModal("");
                    if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                        UIControlService.loadLoadingModal("");
                        UploaderService.uploadSingleFileComplaint(0, "", vm.fileUpload, size, vm.idFileTypes,
                          function (reply) {
                              if (reply.status == 200) {
                                  UIControlService.unloadLoadingModal();
                                  var url = reply.data.Url;
                                  var size = reply.data.FileLength;
                                  update(url);
                              } else {
                                  UIControlService.unloadLoadingModal();
                                  UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                              }
                          }, function (err) {
                              UIControlService.unloadLoadingModal();
                              UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                          });
                    }
                } else {
                    update(vm.DocUrl);
                }
            }
        }

        function update(urlDoc) {

            if (!validateForm()) {
                return;
            }

            UIControlService.loadLoadingModal("");
            convertAllDateToString();
            WarningLetterService.update({
                //LetterID: vm.LetterID,
                //CreatedDate: vm.warningLetter.RequestedDate,
                //VendorID: vm.selectedVendorName.VendorID,
                //WarningTypeID: vm.selectedWarningType.RefID,
                //StartDate: vm.warningLetter.StartDate,
                //EndDate: vm.warningLetter.EndDate,
                //Area: vm.Area,
                //Description: vm.Description,
                //IsActive: item.item.IsActive,
                //ReporterID: vm.selectedReporter.ID,
                //WarningID: vm.selectedComplain.RefID,
                IDContract: vm.idContract,
                ContractOrPONumber: vm.noContract,
                ComplaintID: item.item.ComplaintID,
                IsActive: true,
                Date: vm.warningLetter.RequestedDate,
                ReporterName: vm.ReporterName,
                ReporterID: vm.selectedDepartment.DepartmentID,
                VendorID: vm.vendorID,//vm.selectedVendorName.VendorID,              
                ComplainTypeID: vm.selectedComplain.TypeID,
                Description: vm.Description,
                DocUrl: urlDoc
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_UPDATE_DATA'));
                    $uibModalInstance.close();
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                    return;
                }
            }, function (err) {
                convertToDate();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.tambah = tambah;
        function tambah(data, act) {
           
            var data = {
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/FormWarningLetter.html',
                controller: 'FormWarningLetterCtrl',
                controllerAs: 'frmWarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                $state.transitionTo('master-warningLetter');
            });
        }

        }
})();