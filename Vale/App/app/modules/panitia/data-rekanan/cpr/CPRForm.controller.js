﻿(function () {
    'use strict';

    angular.module("app").controller("CPRFormCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$window', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'CPRService', 'VPCPRDataService', 'VPEvaluationMthodService',
        '$state', 'UIControlService', 'UploadFileConfigService', 'UploaderService', 'GlobalConstantService',
        '$uibModal', '$stateParams', 'VPCriteriaSettings','WarningLetterService'];
    function ctrl($http, $filter, $window, $translate, $translatePartialLoader, $location, SocketService, CPRService, VPCPRDataService, VPEvaluationMthodService,
        $state, UIControlService, UploadFileConfigService, UploaderService, GlobalConstantService,
        $uibModal, $stateParams, VPCriteriaSettings, WarningLetterService) {
        var vm = this;

        vm.act = Number($stateParams.act);
        vm.VPCPRDataId = Number($stateParams.id);
        vm.tipe = Number($stateParams.type);

        vm.dateNow = new Date();
        vm.timezone = vm.dateNow.getTimezoneOffset();
        vm.timezoneClient = vm.timezone / 60;

        vm.isCalendarOpened = [false, false, false, false];
        vm.contract = [];
        vm.metode = [];
        vm.SCORE = 0;
        vm.PMId = 0;
        vm.CEId = 0;
        vm.DeptId = 0;
        vm.kriteria = [];
        vm.IsVhsOrCpr = 1;

        vm.isNoCPR = false;

        var kriteria = [];
        var kriteriaLv1 = [];
        var kriteriaLv2 = [];
        var kriteriaLv3 = [];
        
        /*
        var kr = [];
        var krLv1 = [];
        var krLv2 = [];
        var krLv3 = [];
        */

        /*
        vm.w1 = 1;
        vm.w2 = 2;
        vm.w3 = 3;
        vm.weight1 = [];
        vm.weight2 = [];
        vm.weight3 = [];
        */

        var metode_evaluasi_id;
        vm.detail = [];
        vm.btnScoreFlag = false;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.docs = [];
        vm.noCtrc = null;

        //Added : autocomplete input.
        var _selected;
        vm.ngModelOptionsSelected = ngModelOptionsSelected;
        function ngModelOptionsSelected(value) {
            if (arguments.length) {
                _selected = value;
                //vm.noCtrc = _selected.ContractNo;
                vm.contractSOId = _selected.ID;
                NoContractChange();
            } else {
                return _selected;
            }
        };

        vm.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        //Added : upload dokumen/lampiran.
        vm.add = add;
        function add() {
            var lempar = {
                doc: {                  
                    DocName: "",
                    DocUrl: "",
                    Size: 0,
                    VPCPRDataId: vm.VPCPRDataId
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-doc.modal.html',
                controller: 'cprDocsModalCtrl',
                controllerAs: 'cprDocsModalCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function (saveResult) {             
                if (vm.VPCPRDataId > 0) {
                    loadDataDocPostCPR();
                }
                else {
                    //loadDataDocPreCPR();
                    vm.docs.push(saveResult);
                }
            });
        };

        // load dokumen sebelum/saat cpr dibuat.
        /*
        vm.loadDataDocPreCPR = loadDataDocPreCPR;
        function loadDataDocPreCPR() {
            VPCPRDataService.selectDocsNonCPRId(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoading();
            });
        }
        */

        // load dokumen setelah cpr dibuat by vpCPRId, cprDataID is exist.
        vm.loadDataDocPostCPR = loadDataDocPostCPR;
        function loadDataDocPostCPR() {
            VPCPRDataService.selectDocsCPRId({
                VPCPRDataId: vm.VPCPRDataId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoading();
            });            
        }

        vm.edit = edit;
        function edit(doc, index) {
            
            var lempar = {
                doc: {
                    VPCPRDataId: vm.VPCPRDataId,
                    DocName: doc.DocName,
                    DocUrl: doc.DocUrl,
                    VPCPRDocId: doc.VPCPRDocId
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-doc.modal.html',
                controller: 'cprDocsModalCtrl',
                controllerAs: 'cprDocsModalCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function (saveResult) {
                if (vm.VPCPRDataId > 0) {
                    loadDataDocPostCPR();
                }
                else {
                    //loadDataDocPreCPR();
                    vm.docs[index] = saveResult;
                }
            });
        };

        vm.del = del;
        function del(doc,index) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_DEL_DOC'), function (yes) {
                if (yes) {                    
                    VPCPRDataService.deleteDoc(doc, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_DEL_DOC'));
                            if (vm.VPCPRDataId > 0) {
                                loadDataDocPostCPR();
                            }
                            else {
                                //loadDataDocPreCPR();
                                vm.docs.splice(index, 1);
                            }
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL_DOC'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL_DOC'));
                    });
                }
            });
        }

        vm.getStandardDesc = getStandardDesc;
        function getStandardDesc(id) {
            console.info("id" + id);
            VPCriteriaSettings.getstandards({
                criteriaId: id
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.descCriteria = reply.data;
                    return vm.descCriteria;
                    //console.log("desc: " + JSON.stringify(vm.descCriteria));
                    
                    kriteriaLv2.push(vm.descCriteria);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        }

        vm.init = init;
        function init() {
            
            $translatePartialLoader.addPart("cpr");
            getKontrakCPR();
            if (vm.act == 0) {
                //insert cpr data
                loadAwal();
            } else if (vm.act == 1) {
                //update cpr data
                loadDataDocPostCPR(); // load supporting document

                VPCPRDataService.selectbyid({
                    VPCPRDataId: vm.VPCPRDataId
                }, function (reply) {
                    if (reply.status === 200) {
                        vm.vpeval = reply.data[0];
                        vm.noCtrc = vm.vpeval.ContractNo;
                        vm.ngModelOptionsSelected = vm.vpeval.ContractNo;
                        vm.contractSOId = vm.vpeval.ContractSignOffId;
                        vm.deptOwner = vm.vpeval.Department;
                        vm.sponsor = vm.vpeval.Sponsor;
                        vm.conEngineer = vm.vpeval.ContractEngineer;
                        vm.CPRDate = new Date(vm.vpeval.CPRDate);
                        vm.proManager = vm.vpeval.ProjManager;
                        vm.vendor = vm.vpeval.VendorName;
                        vm.SCORE = vm.vpeval.Score;
                        vm.catatan = vm.vpeval.Note;
                        //console.info("editeval" + JSON.stringify(vm.vpeval));
                        if (vm.vpeval.IsVhsCpr == 1) {
                            vm.tipe = 1;
                        } else if (vm.vpeval.IsVhsCpr == 2) {
                            vm.tipe = 2;
                        }

                        getEvalMethodByType();
                        getKontrakCPR();                                              

                        /*
                        // added: weight in select options
                        if (vm.w1 == 1) { // weight : 1 option
                            for (var i = 1; i <= vm.w1 ; i++) {
                                vm.weight1.push(i);
                            }
                        }
                        if (vm.w2 == 2) { // weight : 2 option
                            for (var j = 1; j <= vm.w2 ; j++) {
                                vm.weight2.push(j);
                            }
                        }
                        if (vm.w3 == 3) { // weight : 3 option
                            for (var k = 1; k <= vm.w3; k++) {
                                vm.weight3.push(k);
                            }
                        }
                        */
                        metode_evaluasi_id = vm.vpeval.VPEvaluationMethodId;
                        SetKriteria(vm.vpeval.VPCPRDataDetails);
                        
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
            }
        };

        function SetKriteria(cprDataDetail) {

            kriteria = [];
            kriteriaLv1 = [];
            kriteriaLv2 = [];
            kriteriaLv3 = [];
            vm.kriteria = [];

            for (var i = 0; i < cprDataDetail.length; i++) {
                kriteria.push(cprDataDetail[i]);
            }

            for (var i = 0; i < kriteria.length; i++) {
                if (kriteria[i].Level === 1) {
                    kriteriaLv1.push(kriteria[i]);
                }
                else if (kriteria[i].Level === 2) {
                    //var kriteriaId = kriteria[i].CriteriaId;
                    kriteriaLv2.push(kriteria[i]);

                }
                else if (kriteria[i].Level === 3) {
                    kriteriaLv3.push(kriteria[i]);
                }
            }

            for (var i = 0; i < kriteriaLv2.length; i++) {
                kriteriaLv2[i].sub = [];
                for (var j = 0; j < kriteriaLv3.length; j++) {
                    if (kriteriaLv3[j].Parent === kriteriaLv2[i].CriteriaId) {
                        kriteriaLv2[i].sub.push(kriteriaLv3[j]);
                    }
                    kriteria[i].Standard = 2;
                }
            }

            for (var i = 0; i < kriteriaLv1.length; i++) {
                kriteriaLv1[i].sub = [];
                for (var j = 0; j < kriteriaLv2.length; j++) {
                    if (kriteriaLv2[j].Parent === kriteriaLv1[i].CriteriaId) {
                        kriteriaLv1[i].sub.push(kriteriaLv2[j]);
                    }
                }
            }
            vm.viewCPR = true;
            vm.kriteria = kriteriaLv1;
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            vm.SCORE = 0;
            vm.vendor = "";
            vm.sponsor = "";
            vm.conEngineer = "";
            vm.proManager = "";
            vm.deptOwner = "";
            vm.noKon = "";
            vm.contract = [];
            getEvalMethodByType();
        };

        function getEvalMethodByType() {
            VPCPRDataService.getByType({
                IsVhsOrCpr: vm.tipe
            }, function (reply) {
                if (reply.status === 200) {
                    vm.metode = reply.data;
                    //console.info("metode:" + JSON.stringify(vm.metode));
                    if (vm.act == 1) {
                        for (var i = 0; i < vm.metode.length; i++) {
                            if (vm.metode[i].VPEvaluationMethodId == vm.vpeval.VPEvaluationMethodId) {
                                vm.evalMethod = vm.metode[i];
                                break;
                            }
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        function getKontrakCPR() {
            if (!vm.act) {
                vm.SCORE = 0;
                vm.vendor = "";
                vm.sponsor = "";
                vm.conEngineer = "";
                vm.proManager = "";
                vm.deptOwner = "";
                vm.noKon = "";
                vm.contract = [];
            }
            VPCPRDataService.getContract(function (reply) {
                if (reply.status === 200) {
                    vm.contract = reply.data;
                    vm.totalItem = vm.contract.length;
                    if (vm.act == 1) {
                        for (var i = 0; i < vm.contract.length; i++) {
                            if (vm.contract[i].ContractNo == vm.vpeval.ContractNo) {
                                vm.noKontrak = vm.contract[i];
                                break;
                            }
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };
        
        vm.NoContractChange = NoContractChange;
        function NoContractChange() {

            vm.noCtrc = null;
            vm.vendor = null;
            vm.sponsor = null;
            vm.conEngineer = null;
            vm.proManager = null;
            vm.deptOwner = null;
            vm.noKon = null;
            vm.vendorId = null;
            vm.PMId = null;
            vm.CEId = null;
            vm.DeptId = null;
            vm.TenderName = null;

            UIControlService.loadLoading("");
            VPCPRDataService.noDraft({
                ID: vm.contractSOId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.data === true) {
                    for (var i = 0; i < vm.totalItem; i++) {
                        //if (vm.noKontrak.ContractNo == vm.contract[i].ContractNo) {
                        if (vm.contractSOId == vm.contract[i].ID) {
                            vm.noCtrc = vm.contract[i].ContractNo
                            vm.vendor = vm.contract[i].VendorName;
                            vm.sponsor = vm.contract[i].ContractSponsor;
                            vm.conEngineer = vm.contract[i].ContractEngineer;
                            vm.proManager = vm.contract[i].ProjectManager;
                            vm.deptOwner = vm.contract[i].Department;
                            vm.noKon = vm.contract[i].ContractNo;
                            vm.vendorId = vm.contract[i].VendorID;
                            vm.PMId = vm.contract[i].ProjectManagerId;
                            vm.CEId = vm.contract[i].ContractEngineerId;
                            vm.DeptId = vm.contract[i].DepartmentID;
                            vm.TenderName = vm.contract[i].TenderName;
                            break;
                        }
                    }
                } else {
                    UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_THERE_IS_DRAFT'));
                }
            }, function (error) {
                UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_CHECK_CONTRACTS'));
                UIControlService.unloadLoading();
            });
        };

        vm.onMetodeEvalChange = onMetodeEvalChange;
        function onMetodeEvalChange() {
            if (vm.evalMethod !== null) {
                if (vm.evalMethod.IsVhsOrCpr == 1) {
                    vm.viewCPR = true;
                    vm.viewVHS = false;
                    metode_evaluasi_id = vm.evalMethod.VPEvaluationMethodId;
                    getEvalMetode();
                }
            } else if (vm.evalMethod === null) {
                vm.viewCPR = false;
            }
        };

        function getEvalMetode() {
            kriteria = [];
            kriteriaLv1 = [];
            kriteriaLv2 = [];
            kriteriaLv3 = [];
            vm.kriteria = [];
       
            VPEvaluationMthodService.selectDCbyID({
                VPEvaluationMethodId: metode_evaluasi_id
            }, function (reply) {
                if (reply.status === 200) {
                    var hasil = reply.data;
                    //console.info("kriteria:" + JSON.stringify(hasil));
                    for (var i = 0; i < hasil.length; i++) {
                        kriteria.push(hasil[i]);
                    }

                    for (var i = 0; i < kriteria.length; i++) {
                        if (kriteria[i].Level === 1) {
                            kriteriaLv1.push(kriteria[i]);                          
                        }
                        else if (kriteria[i].Level === 2) {
                            kriteriaLv2.push(kriteria[i]);                            
                        }
                        else if (kriteria[i].Level === 3) {
                            kriteriaLv3.push(kriteria[i]);                       
                        }                                                                     
                    }
                    
                    /*
                    // added: weight in select options
                    if (vm.w1 == 1) { // weight : 1 option
                        for (var i = 1; i <= vm.w1 ; i++) {
                            vm.weight1.push(i);                          
                        }                        
                    }
                    if (vm.w2 == 2) { // weight : 2 option
                        for (var j = 1; j <= vm.w2 ; j++) {
                            vm.weight2.push(j);                            
                        }                        
                    }
                    if (vm.w3 == 3) { // weight : 3 option
                        for (var k = 1; k <= vm.w3; k++) {
                            vm.weight3.push(k);                            
                        }
                    }
                    */

                    for (var i = 0; i < kriteriaLv2.length; i++) {
                        kriteriaLv2[i].sub = [];
                        for (var j = 0; j < kriteriaLv3.length; j++) {
                            if (kriteriaLv3[j].Parent === kriteriaLv2[i].CriteriaId) {
                                kriteriaLv2[i].sub.push(kriteriaLv3[j]);
                            }
                        }
                    }

                    for (var i = 0; i < kriteriaLv1.length; i++) {
                        kriteriaLv1[i].sub = [];
                        for (var j = 0; j < kriteriaLv2.length; j++) {
                            if (kriteriaLv2[j].Parent === kriteriaLv1[i].CriteriaId) {
                                kriteriaLv1[i].sub.push(kriteriaLv2[j]);
                            }
                        }
                    }

                    vm.kriteria = kriteriaLv1;
                    for (var i = 0; i < vm.kriteria.length; i++) {
                        vm.kriteria[i].Score = 0;
                        if (vm.kriteria[i].sub.length !== 0) {
                            for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                                vm.kriteria[i].sub[j].Score = 0;
                                if (vm.kriteria[i].sub[j].sub !== 0) {
                                    for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                                        vm.kriteria[i].sub[j].sub[k].Score = 0;
                                    }
                                }
                            }
                        }
                    }
                    //console.info("kriteria final:" + JSON.stringify(vm.kriteria));
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
                }
            }, function (err) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.copyPrevCPRDet = copyPrevCPRDet;
        function copyPrevCPRDet() {
            UIControlService.loadLoading("");
            VPCPRDataService.getPreviousCPR({
                VPCPRDataId: vm.VPCPRDataId,
                ContractSignOffId: vm.contractSOId
            }, function (reply) {
                var prevCPR = reply.data;
                metode_evaluasi_id = prevCPR.VPEvaluationMethodId;
                for (var i = 0; i < vm.metode.length; i++) {
                    if (vm.metode[i].VPEvaluationMethodId == metode_evaluasi_id) {
                        vm.evalMethod = vm.metode[i];
                        break;
                    }
                }
                if (prevCPR.VPCPRDataDetails) {
                    SetKriteria(prevCPR.VPCPRDataDetails);
                    scoring();
                } else {
                    UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.NO_PREV_CPR'));
                }
                UIControlService.unloadLoading();
            }, function (err) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                UIControlService.unloadLoading();
            });
        }

        vm.scoring = scoring;
        function scoring() {
            vm.SCORE = 0;
            var SLv1 = 0;
            var SLv2 = 0;
            var SLv3 = 0;

            var slv1 = 0;
            var slv2 = 0;
            var slv3 = 0;

            for (var i = 0; i < vm.kriteria.length; i++) {
                //SLv1 += vm.kriteria[i].Weight;
                //slv1 += vm.kriteria[i].Score;
                for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                    //SLv2 += vm.kriteria[i].sub[j].Weight;
                    //slv2 += vm.kriteria[i].sub[j].Score;
                    for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                        //SLv3 += vm.kriteria[i].sub[j].sub[k].Weight;
                        SLv3 += vm.kriteria[i].sub[j].Standards.length;
                        slv3 += vm.kriteria[i].sub[j].sub[k].Score;
                    }
                }
            }
            
            vm.SCORE = parseFloat(((slv1 + slv2 + slv3) / (SLv1 + SLv2 + SLv3)) * 100).toFixed(2);
            
            vm.btnScoreFlag = true;
        };

        vm.checkRemarkStatus = checkRemarkStatus;
        function checkRemarkStatus() {
            //false: ga ada remark di semua level. - continue for validation warning.
            //true: ada remark di salah satu level kriteria.

            if (vm.isNoCPR) {
                return true;
            }

            var status = true; 
            for (var i = 0 ; i < vm.kriteria.length; i++) {
                if (vm.kriteria[i].Remark == "" && vm.kriteria[i].Remark == null) {
                    //console.log("belum terisi di lv1.");
                    status = false;
                    break;
                    if (vm.kriteria[i].Remark == null) {
                        //console.log("belum terisi di lv1.");
                        status = false;
                        break;
                    }
                }
                else {
                    for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                        if (vm.kriteria[i].sub[j].Remark == "" && vm.kriteria[i].sub[j].Remark == null) {
                            //console.log("belum terisi di lv2.");
                            status = false;
                            break;
                            if (vm.kriteria[i].sub[j].Remark == null) {
                                //console.log("belum terisi di lv2.");
                                status = false;
                                break;
                            }
                        }
                        else {
                            for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                                if (vm.kriteria[i].sub[j].sub[k].Remark == "" && vm.kriteria[i].sub[j].Remark == null ) {
                                    //console.log("belum terisi di lv3.");
                                    status = false;
                                    break;
                                    if (vm.kriteria[i].sub[j].sub[k].Remark == null) {
                                        //console.log("belum terisi di lv3.");
                                        status = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }           
            }            
            return status;            
        }

        vm.checkValidity = checkValidity;
        function checkValidity() {
            // validation part: validation for datepicker, validation for no. contract field, & validation for criteria remark.

            var statusRemark = checkRemarkStatus();
            //console.log("status remark " + statusRemark);
            if (!vm.CPRDate || !vm.noCtrc || !statusRemark) {
                if (!vm.CPRDate) { 
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DATE_CPR'));
                }
                if (!vm.noCtrc) { 
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_CONTRACT'));
                }
                if (!statusRemark) {  
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_REMARK'));
                }
            }            
            else {
                //console.log("status remark criteria: " + statusRemark + " && lanjut proses save->insert/update.");                
                save();
            }
                                                              
        }

        vm.save = save;
        function save() {
           
            kriteriaLv1 = [];
            for (var i = 0; i < vm.kriteria.length; i++) {
                kriteriaLv1.push(vm.kriteria[i]);
                for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                    kriteriaLv1.push(vm.kriteria[i].sub[j]);
                    for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                        kriteriaLv1.push(vm.kriteria[i].sub[j].sub[k]);
                    }
                }
            }        
         
            if (vm.btnScoreFlag == false) {
                /*
                vm.SCORE = 0;
                var SLv1 = 0;
                var SLv2 = 0;
                var SLv3 = 0;

                var slv1 = 0;
                var slv2 = 0;
                var slv3 = 0;

                for (var i = 0; i < vm.kriteria.length; i++) {
                    SLv1 += vm.kriteria[i].Weight;
                    slv1 += vm.kriteria[i].Score;
                    for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                        SLv2 += vm.kriteria[i].sub[j].Weight;
                        slv2 += vm.kriteria[i].sub[j].Score;
                        for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                            SLv3 += vm.kriteria[i].sub[j].sub[k].Weight;
                            slv3 += vm.kriteria[i].sub[j].sub[k].Score;
                        }
                    }
                }

                vm.SCORE = parseFloat(((slv1 + slv2 + slv3) / (SLv1 + SLv2 + SLv3)) * 100).toFixed(2);
                */
                scoring();
            }

            vm.CPRDate = new Date(vm.CPRDate.setHours(vm.CPRDate.getHours() - vm.timezoneClient));

            //Added: score checking for directly send to warning letter.
            UIControlService.loadLoading("");
            if (vm.act == 0) {
                //if (vm.SCORE < 66) {    
                //    saveToWL();
                //}

                //console.log("no contract: " + vm.noCtrc);
                if (!vm.isNoCPR) {
                    VPCPRDataService.insert({
                        //ContractNo: vm.noKontrak.ContractNo,
                        ContractNo: vm.noCtrc,
                        VendorId: vm.vendorId,
                        Sponsor: vm.sponsor,
                        ContractEngineerId: vm.CEId,
                        ProjectManagerId: vm.PMId,
                        DepartmentHolder: vm.DeptId,
                        CPRDate: vm.CPRDate,
                        VPEvaluationMethodId: metode_evaluasi_id,
                        Score: vm.SCORE,
                        VPCPRDataDetails: kriteriaLv1,
                        TanderName: vm.TenderName,
                        IsVhsCpr: vm.tipe,
                        Note: vm.catatan,
                        ContractSignOffId: vm.contractSOId,
                        VPCPRDocuments: vm.docs
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            $state.transitionTo('cpr-vendor');
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SAVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                    });
                } else {
                    VPCPRDataService.insertDefault({
                        //ContractNo: vm.noKontrak.ContractNo,
                        ContractNo: vm.noCtrc,
                        VendorId: vm.vendorId,
                        Sponsor: vm.sponsor,
                        ContractEngineerId: vm.CEId,
                        ProjectManagerId: vm.PMId,
                        DepartmentHolder: vm.DeptId,
                        CPRDate: vm.CPRDate,
                        //VPEvaluationMethodId: metode_evaluasi_id,
                        //Score: vm.SCORE,
                        //VPCPRDataDetails: kriteriaLv1,
                        TanderName: vm.TenderName,
                        IsVhsCpr: vm.tipe,
                        Note: vm.catatan,
                        ContractSignOffId: vm.contractSOId,
                        //VPCPRDocuments: vm.docs
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            $state.transitionTo('cpr-vendor');
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SAVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                    });
                }
            } else if (vm.act == 1) {
                VPCPRDataService.update({
                    CPRDate: vm.CPRDate,
                    VPCPRDataId: vm.VPCPRDataId,
                    VPEvaluationMethodId: metode_evaluasi_id,
                    Score: vm.SCORE,
                    VPCPRDataDetails: kriteriaLv1,
                    Note: vm.catatan
                }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        $state.transitionTo('cpr-vendor');
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.UPDATE_CPR_SUCCES'));
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_UPDATE_CPR'));
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_UPDATE_CPR'));
                });
            }
        };

        /*
        vm.saveToWL = saveToWL;
        function saveToWL() {
            
            WarningLetterService.insertWarningLetterByScoreCPR({                                                
                VendorID:  vm.vendorId,   
                ReporterID: vm.DeptId,
                IDContract: vm.contractSOId
                
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    console.log("succ. cpr data saved to WL");                    
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoadingModal();
            });
        } 
        */

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        vm.back = back
        function back() {
            $state.transitionTo('cpr-vendor');
        }
    }
})();