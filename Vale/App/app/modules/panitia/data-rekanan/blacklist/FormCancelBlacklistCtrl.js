﻿(function () {
    'use strict';

    angular.module("app").controller("FormCancelBlacklistCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BlacklistService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance', '$state', '$uibModal'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, BlacklistService,
        RoleService, UIControlService, item, $uibModalInstance, $state, $uibModal) {
        var vm = this;
        var page_id = 141;

        vm.isAdd = item.act;
        vm.Area;
        vm.LetterID;
        vm.Description = "";
        vm.action = "";
        vm.actionblacklst = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.init = init;
        vm.warningLetter = {};
        vm.isCalendarOpened = [false, false, false, false];

        function init() {
            console.info(item);
            if (item.act === true) {
                vm.action = "Tambah";
                vm.actionblacklist = "BLACKLIST";
            }
            else if (item.act === false){
            vm.action = "Pembatalan";
            vm.actionblacklist = "WHITELIST";
        }

        }

        vm.blacklist = blacklist;
        function blacklist() {
            BlacklistService.InsertBlacklist({
                VendorID: item.VendorID,
                ReferenceNo: item.ReferenceNo,
                BlacklistDescription: item.BlacklistDescription,
                DocUrl: item.DocUrl,
                VendorStock: item.vendorstock,
                CompPers: item.employee,
                DetailPerson: item.DetailPerson,
                MasaBlacklistID: item.MasaBlacklistID,
                StartDate: item.StartDate,
                EndDate: item.EndDate
            },
                             function (reply) {
                                 UIControlService.unloadLoadingModal();
                                 if (reply.status === 200) {
                                     UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_BL");
                                     $uibModalInstance.close(true);
                                 }
                                 else {
                                     UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
                                     return;
                                 }
                             },
                             function (err) {
                                 UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                                 UIControlService.unloadLoadingModal();
                             }
                         );

        }

        vm.batalkan = batalkan;
        function batalkan() {
            BlacklistService.editBlacklist({
                VendorID: item.data.VendorID,
                BlacklistID: item.data.BlacklistID,
                BlacklistTypeID: item.data.BlacklistTypeID,
                BlacklistType: {
                    Name: "BLACKLIST_TYPE_NO"
                },
                ReferenceNo: item.ReferenceNo,
                DorUrlWhiteList: item.DocUrl

            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_CANCEL_BL");
                    $uibModalInstance.close(true);

                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_INACTIVATE");
                    return;
                }
            }, function (err) {

                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoading();
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();