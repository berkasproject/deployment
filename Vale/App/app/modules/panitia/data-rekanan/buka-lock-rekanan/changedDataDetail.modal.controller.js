﻿(function () {
	'use strict';

	angular.module("app").controller("ChangedDataDetailCtrl", ctrl);

	ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PermintaanUbahDataService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
	function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, PUbahDataService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

		var vm = this;
		vm.init = init;
		vm.item = item;
		vm.param = item.param;
		vm.obj = item.item;
		vm.VendorID = item.vendorID;
		vm.action = vm.item.item.Action;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.totalData = item.totalData;

		function init() {
			$translatePartialLoader.addPart('permintaan-ubah-data');
			console.info("item:" + JSON.stringify(vm.item));
			//console.info("vendorID:" + JSON.stringify(vm.VendorID));
			//UIControlService.loadLoading("Silahkan Tunggu...");
			if (vm.param === 6) {
				$translatePartialLoader.addPart('pengurus-perusahaan');
				loadCompanyPerson(1);
			} else if (vm.param === 2) {
				loadBusinessField(1);
			} else if (vm.param === 9) {
				vm.countryID = vm.item.item.StateLocationRef.CountryID;
				vm.expType = vm.item.item.ExperienceType;
				vm.datepelaksanaan = UIControlService.getStrDate(vm.item.item.StartDate);
				vm.dateselesai = UIControlService.getStrDate(vm.item.item.EndDate);
				if (vm.countryID === 360) {
					vm.lokasidetail = vm.item.item.CityLocation.Name + "," + vm.item.item.StateLocationRef.Name + "," + vm.item.item.StateLocationRef.Country.Name + "," + vm.item.item.StateLocationRef.Country.Continent.Name;
				} else {
					vm.lokasidetail = vm.item.item.StateLocationRef.Name + "," + vm.item.item.StateLocationRef.Country.Name + "," + vm.item.item.StateLocationRef.Country.Continent.Name;
				}
				loadVendorExperience(1);
			} else if (vm.param === '8a') {
				loadDataBuilding(1);
			} else if (vm.param === '8b') {
				loadDataVehicle(1);
			} else if (vm.param === '8c') {
				loadEquipmentTools(1);
			} else if (vm.param === '1067') {
				loadDataAgreement(vm.obj);
			} else if (vm.param === '1435') {
				loadBalance(1);
			} else if (vm.param === '12a') {
				loadKuesioner(vm.obj);
			} else if (vm.param === '12b') {
				loadKuesionerUrl(vm.obj);
			} else if (vm.param === '1000') {
				loadDataLegalDoc(vm.obj);
			}


		}

		//CompanyPerson
		vm.loadCompanyPerson = loadCompanyPerson;
		function loadCompanyPerson(current) {
			vm.currentPage = current;
			var offset = 0;
			PUbahDataService.selectCompanyPerson({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data.List;
					vm.totalData = reply.data.Count;
					vm.changedData = [];
					for (var i = 0; i < vm.totalData; i++) {
						if (vm.verified[i].ID === vm.item.item.RefVendorId) {
							vm.changedData.push(vm.verified[i]);
						}
					}
					console.info("changedata compers:" + JSON.stringify(vm.changedData));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoading();
			});
		}


		//BusinessField
		vm.loadBusinessField = loadBusinessField;
		function loadBusinessField(current) {
			vm.currentPage = current;
			var offset = 0;
			PUbahDataService.selectBusinessField({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data.List;
					vm.changedData = [];
					for (var i = 0; i < vm.verified.length; i++) {
						if (vm.verified[i].ID === vm.item.item.RefVendorId) {
							vm.changedData.push(vm.verified[i]);
						}
					}
					console.info("changedata bField:" + JSON.stringify(vm.changedData));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoading();
			});
		}


		//Experience
		vm.loadVendorExperience = loadVendorExperience;
		function loadVendorExperience(current) {
			console.info("expType:" + vm.expType);
			var offset = 0;
			if (vm.expType == 3154) {
				UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
				PUbahDataService.selectVendorExperience({
					Parameter: vm.VendorID,
					Offset: offset,
					Limit: vm.totalData,
					column: 1
				}, function (reply) {
					console.info("expe: " + JSON.stringify(reply));
					if (reply.status === 200) {
						vm.listCurrExp = reply.data.List;
						vm.changedData = [];
						for (var i = 0; i < vm.listCurrExp.length; i++) {
							if (vm.listCurrExp[i].ID === vm.item.item.RefVendorId) {
								vm.changedData.push(vm.listCurrExp[i]);
							}
						}
						console.info("changedata currExp:" + JSON.stringify(vm.changedData));
						if (vm.changedData[0].StateLocationRef.CountryID === 360) {
							vm.lokasidetail = vm.changedData[0].CityLocation.Name + "," + vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						} else {
							vm.lokasidetail = vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						}
						vm.changedData[0].StartDate = UIControlService.getStrDate(vm.changedData[0].StartDate);
						UIControlService.unloadLoading();
					} else {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
					}
				}, function (err) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				});
			} else if (vm.expType == 3153) {
				UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
				PUbahDataService.selectVendorExperience({
					Parameter: vm.VendorID,
					Offset: offset,
					Limit: 10,
					column: 2
				}, function (reply) {
					//console.info("current?:"+JSON.stringify(reply));
					if (reply.status === 200) {
						UIControlService.unloadLoading();
						vm.listFinExp = reply.data.List;
						vm.changedData = [];
						for (var i = 0; i < vm.listFinExp.length; i++) {
							if (vm.listFinExp[i].ID === vm.item.item.RefVendorId) {
								vm.changedData.push(vm.listFinExp[i]);
							}
						}
						console.info("changedata finExp:" + JSON.stringify(vm.changedData));
						if (vm.changedData[0].StateLocationRef.CountryID === 360) {
							vm.lokasidetail = vm.changedData[0].CityLocation.Name + "," + vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						} else {
							vm.lokasidetail = vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						}
						vm.changedData[0].StartDate = UIControlService.getStrDate(vm.changedData[0].StartDate);
					} else {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
					}
				}, function (err) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				});
			}
		}

		//Equipment
		//building
		vm.loadDataBuilding = loadDataBuilding;
		vm.listBuilding = [];
		vm.maxSizeBuild = vm.totalData;
		function loadDataBuilding(current) {
			var offset = (current * vm.maxSizeBuild) - vm.maxSizeBuild;
			UIControlService.loadLoading(vm.msgLoading);
			PUbahDataService.selectBuilding({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				console.info("bangunanCR:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				vm.listBuilding = reply.data.List;
				vm.totalData = reply.data.Count;
				vm.changedData = [];
				for (var i = 0; i < vm.totalData; i++) {
					if (vm.listBuilding[i].ID === vm.item.item.RefVendorId) {
						vm.changedData.push(vm.listBuilding[i]);
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadDataLegalDoc = loadDataLegalDoc;
		vm.listLegalDoc = [];
		vm.maxSizeLegalDoc = vm.totalData;
		function loadDataLegalDoc(current) {
			var offset = (current * vm.maxSizeLegalDoc) - vm.maxSizeLegalDoc;
			UIControlService.loadLoading(vm.msgLoading);
			PUbahDataService.loadVendorLegalDoc({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listLegalDoc = reply.data.List;
				vm.totalData = reply.data.Count;
				vm.changedData = [];
				for (var i = 0; i < vm.totalData; i++) {
					if (vm.listLegalDoc[i].ID === vm.item.item.RefVendorId) {
						vm.changedData.push(vm.listLegalDoc[i]);
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//vehicle
		vm.listVehicle = [];
		vm.loadDataVehicle = loadDataVehicle;
		function loadDataVehicle(current) {
			var maxsize = 10;
			var offset = 0;
			UIControlService.loadLoading(vm.msgLoading);
			PUbahDataService.selectVehicle({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				console.info("kendaraan:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				vm.listVehicle = reply.data.List;
				for (var i = 0; i < vm.listVehicle.length; i++) {
					vm.listVehicle[i].MfgDate = UIControlService.getStrDate(vm.listVehicle[i].MfgDate);
				}
				vm.totalData = reply.data.Count;
				vm.changedData = [];
				for (var i = 0; i < vm.totalData; i++) {
					if (vm.listVehicle[i].ID === vm.item.item.RefVendorId) {
						vm.changedData.push(vm.listVehicle[i]);
					}
				}
				console.info("changedata vehicle:" + JSON.stringify(vm.changedData));
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadBalance = loadBalance;
		function loadBalance(current) {
			var maxsize = 10;
			var offset = 0;
			UIControlService.loadLoading(vm.msgLoading);
			PUbahDataService.selectBalance({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listBalance = reply.data.List;
				vm.totalData = reply.data.Count;
				vm.changedData = [];
				for (var i = 0; i < vm.totalData; i++) {
					if (vm.listBalance[i].BalanceID === vm.item.item.RefVendorId) {
						vm.changedData.push(vm.listBalance[i]);
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadDataAgreement = loadDataAgreement;
		function loadDataAgreement(obj) {
			PUbahDataService.GetStatementDetail({
				ID: obj.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listAgreement = reply.data;
				//for (var i = 0; i < vm.listEquipmentTools.length; i++) {
				//	vm.listEquipmentTools[i].UploadDate = UIControlService.getStrDate(vm.listEquipmentTools[i].UploadDate);
				//}
				//vm.totalData = reply.data.Count;
				//vm.changedData = [];
				//for (var i = 0; i < vm.totalData; i++) {
				//	if (vm.listEquipmentTools[i].ID === vm.item.item.RefVendorId) {
				//		vm.changedData.push(vm.listEquipmentTools[i]);
				//	}
				//}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//tools
		vm.listEquipmentTools = [];
		vm.loadEquipmentTools = loadEquipmentTools;
		function loadEquipmentTools(current) {
			//console.info("mlebu");
			var maxSize = 10;
			var offset = (current * maxSize) - maxSize;
			UIControlService.loadLoading(vm.msgLoading);
			PUbahDataService.selectEquipment({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				console.info("peralatan:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				vm.listEquipmentTools = reply.data.List;
				for (var i = 0; i < vm.listEquipmentTools.length; i++) {
					vm.listEquipmentTools[i].MfgDate = UIControlService.getStrDate(vm.listEquipmentTools[i].MfgDate);
				}
				vm.totalData = reply.data.Count;
				vm.changedData = [];
				for (var i = 0; i < vm.totalData; i++) {
					if (vm.listEquipmentTools[i].ID === vm.item.item.RefVendorId) {
						vm.changedData.push(vm.listEquipmentTools[i]);
					}
				}
				console.info("changedata tools:" + JSON.stringify(vm.changedData));
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//VendorBalance
		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}

		vm.loadKuesioner = loadKuesioner;
		function loadKuesioner() {
			PUbahDataService.loadQuestionnaire({
				Parameter: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.vendorQuestionnaireTemp = [];
				vm.vendorQuestionnaire = [];
				for (var i = 0; i < reply.data.List.length; i++) {
					if (reply.data.List[i].IsActive == true) {
						vm.vendorQuestionnaireTemp.push(reply.data.List[i]);
					}
					else vm.vendorQuestionnaire.push(reply.data.List[i]);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadKuesionerUrl = loadKuesionerUrl;
		function loadKuesionerUrl() {
			PUbahDataService.loadQuestionnaireUrl({
				Parameter: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.QuestionnaireUrl = reply.data;

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
	}
})();
//TODO


