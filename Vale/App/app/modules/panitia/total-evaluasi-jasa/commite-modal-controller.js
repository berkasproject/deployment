﻿(function () {
    'use strict';

    angular.module("app").controller("CommitteeModalCtrl", ctrl);
    ctrl.$inject = ['$state','$uibModal','$stateParams','$filter','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'UIControlService', 'GlobalConstantService', 'TotalEvaluasiJasaService'];
    /* @ngInject */
    function ctrl($state, $uibModal, $stateParams, $filter, $http, $translate, $translatePartialLoader, $location, SocketService,
        UIControlService, GlobalConstantService, TotalEvaluasiJasaService) {
        var vm = this;
        vm.contractRequisitionId = Number($stateParams.contractRequisitionId);
        vm.TenderStepID = Number($stateParams.TenderStepID);
        vm.totalEval = Number($stateParams.totalEval);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.datarekanan = [];
        vm.currentPage = 1;
        vm.fullSize = 10;
        vm.offset = (vm.currentPage * 10) - 10;
        vm.totalRecords = 0;
        vm.totalItems = 0;
        vm.user = '';
        vm.activator;
        vm.verificator;
        vm.menuhome = 0;
        vm.cmbStatus = 0;
        vm.rekanan_id = '';
        vm.flag = false;
        vm.pageSize = 10;
        vm.date = "";
        vm.datapegawai;
        vm.flagTemplate = false;
        vm.year = "";
        vm.datemonth = "";
        vm.project = "";
        vm.waktuMulai1 = (vm.year - 1) + '-' + vm.datemonth;
        vm.waktuMulai2 = vm.date;

        vm.sStatus = -1;
        vm.thisPage = 12;
        vm.verificationPage = 130;
        vm.verifikasi = {};
        vm.isCalendarOpened = [false, false, false, false];
        //functions
        vm.init = init;
        vm.selectedPosition1 = {};
        vm.addPegawai = {
            ContractRequisitionID: 0,
            position: {},
            employee: {},
            IsActive: ""
        };
        vm.empNonAct = [];
        vm.L1 = false;
        vm.L2 = false;
        vm.CountCommittee = 0;

        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');
            $translatePartialLoader.addPart('total-evaluasi-jasa');
            loadData();
            loadReferenceType();
            loadDataTotalEval();


        };

        function loadDataTotalEval() {
            vm.listVendor = [];
            vm.rank1 = true;
            TotalEvaluasiJasaService.getFinalTotalEval({
                TenderStepID: vm.TenderStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].IsCheck == true) {
                            if (data[i].rank == 1) vm.rank1 = false;
                        }
                    }

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.loadReferenceType = loadReferenceType;
        function loadReferenceType() {
            vm.tipeApp = [];
            UIControlService.loadLoading("Loading");
            TotalEvaluasiJasaService.getReferenceType(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.list = reply.data;
                    vm.tipeApp = vm.list.List;
                } else {
                    //UIControlService.msg_growl("error", $filter('translate') ('MESSAGE.ERR_LOAD_APPROVERS'));
            }
            }, function (error) {
                UIControlService.unloadLoading();
                //UIControlService.msg_growl("error", $filter('translate') ('MESSAGE.ERR_LOAD_APPROVERS'));
            });
        }

        vm.loadData = loadData;
        function loadData() {
            vm.crApps = [];
            vm.list = [];
            UIControlService.loadLoading("Loading");
            TotalEvaluasiJasaService.GetCRApprovals({
                Status: vm.contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.list = reply.data;
                    for (var i = 0; i < vm.list.length; i++) {
                        vm.crApps.push({
                            IsActive: vm.list[i].IsActive,
                            ID: vm.list[i].ID,
                            EmployeeID: vm.list[i].EmployeeID,
                            EmployeeFullName: vm.list[i].MstEmployee.FullName + ' ' + vm.list[i].MstEmployee.SurName,
                            EmployeePositionName: vm.list[i].MstEmployee.PositionName,
                            EmployeeDepartmentName: vm.list[i].MstEmployee.DepartmentName,
                            IsHighPriority: vm.list[i].IsHighPriority,
                            SysReference1: vm.list[i].SysReference1
                        });
                    }
                } else {
                    //UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                //UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $state.transitionTo('verifikasi-totalEval');
        }

        vm.tambah = tambah;
        function tambah() {
            var data = {
                currentData: vm.crApps,
                selectedPosition: vm.selectedPosition.RefID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/total-evaluasi-jasa/aturApproval.modal.html',
                controller: 'selectApproverModalTotalEvalCtrl',
                controllerAs: 'selAppModalTotalEvalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (selected) {
                vm.datapegawai = selected;
                vm.Name = selected.FullName + ' ' + selected.SurName;
            });
        }

        vm.load = load;
        function load(data) {
            console.info(JSON.stringify(data));
        }

        vm.addCommiteEmployee = addCommiteEmployee;
        function addCommiteEmployee() {
            if (vm.empNonAct.length !== 0) {
                for (var i = 0; i < vm.empNonAct.length; i++) {
                    vm.crApps.push(vm.empNonAct[i]);
                }
            }
            loadInsert();
        }

        vm.addEmployee = addEmployee;
        function addEmployee() {
            vm.addEmp = 0;
            vm.ListEmp = [];
            vm.act = true;
            vm.flagExp = false;
            if (vm.selectedPosition === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_APP_TYPE"); return;
            }
            else if (vm.Name === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_EMP"); return;
            }
            else if (vm.selectedPosition.Name == "TC_EXCEPTION") {
                if (vm.datapegawai.LevelInfo == "L1") {
                    if (vm.L1 == true) {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_EXC"); return;
                    }
                    else {
                        vm.flagExp = true;
                        vm.crApps.push({
                            ID: 0,
                            level_info: vm.datapegawai.LevelInfo,
                            EmployeeID: vm.datapegawai.EmployeeID,
                            EmployeeFullName: vm.Name,
                            EmployeePositionName: vm.datapegawai.PositionName,
                            EmployeeDepartmentName: vm.datapegawai.DepartmentName,
                            IsHighPriority: false,
                            IsActive: true,
                            SysReference1: vm.selectedPosition
                        });
                        vm.Name = undefined;
                        vm.L1 = true;
                    }
                }
                else if (vm.datapegawai.LevelInfo == "L2") {
                    if (vm.L2 == true) {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_EXC_L2"); return;
                    }
                    else {
                        vm.flagExp = true;
                        vm.crApps.push({
                            ID: 0,
                            level_info: vm.datapegawai.LevelInfo,
                            EmployeeID: vm.datapegawai.EmployeeID,
                            EmployeeFullName: vm.Name,
                            EmployeePositionName: vm.datapegawai.PositionName,
                            EmployeeDepartmentName: vm.datapegawai.DepartmentName,
                            IsHighPriority: false,
                            IsActive: true,
                            SysReference1: vm.selectedPosition
                        });
                        vm.Name = undefined;
                        vm.L2 = true;
                    }
                }
                else if (vm.datapegawai.doaModel.length != 0) {
                    vm.datapegawai.doaModel.forEach(function (data) {
                        if (data.LevelInfo == "L1" && vm.L1 == false) {
                            vm.flagExp = true;
                            vm.crApps.push({
                                ID: 0,
                                level_info: data.LevelInfo,
                                EmployeeID: vm.datapegawai.EmployeeID,
                                EmployeeFullName: vm.Name,
                                EmployeePositionName: vm.datapegawai.PositionName,
                                EmployeeDepartmentName: vm.datapegawai.DepartmentName,
                                IsHighPriority: false,
                                IsActive: true,
                                SysReference1: vm.selectedPosition
                            });
                            vm.Name = undefined;
                            vm.L1 = true;
                        }
                        else if (data.LevelInfo == "L2" && vm.L2 == false) {
                            vm.flagExp = true;
                            vm.crApps.push({
                                ID: 0,
                                level_info: data.LevelInfo,
                                EmployeeID: vm.datapegawai.EmployeeID,
                                EmployeeFullName: vm.Name,
                                EmployeePositionName: vm.datapegawai.PositionName,
                                EmployeeDepartmentName: vm.datapegawai.DepartmentName,
                                IsHighPriority: false,
                                IsActive: true,
                                SysReference1: vm.selectedPosition
                            });
                            vm.Name = undefined;
                            vm.L2 = true;
                        }

                    });
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_EXC_EMP"); return;
                }
                if(vm.flagExp == false) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_EXC_EMP"); return;
                }

            }
            else {
                vm.CountCommittee = +vm.CountCommittee + 1;
                vm.crApps.push({
                    ID: 0,
                    level_info: vm.datapegawai.LevelInfo,
                    EmployeeID: vm.datapegawai.EmployeeID,
                    EmployeeFullName: vm.Name,
                    EmployeePositionName: vm.datapegawai.PositionName,
                    EmployeeDepartmentName: vm.datapegawai.DepartmentName,
                    IsHighPriority: false,
                    IsActive: true,
                    SysReference1: vm.selectedPosition,
                    ListDOA: vm.datapegawai.doaModel
                });
                vm.Name = undefined;
}
        }

        vm.deleteRow = deleteRow;
        function deleteRow(data, index) {
            if (data.ID != 0) {
                data.IsActive = false;
                vm.empNonAct.push(data);
            }
            if (data.SysReference1.Name == 'TC_EXCEPTION') {
                if (data.LevelInfo == "L1") vm.L1 = false;
                else vm.L2 = false;
            }
            var idx = index;
            var _length = vm.crApps.length; // panjangSemula
            vm.crApps.splice(idx, 1);
        }

        vm.loadInsert = loadInsert;
        function loadInsert() {
            vm.IsHighPriority = false;
            for (var i = 0; i < vm.crApps.length; i++) {
                if (vm.crApps[i].IsHighPriority == true) {
                    if (vm.IsHighPriority == false) vm.IsHighPriority = true;
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_CHAIRMAN");
                        return;
                    }
                }
                if (i == (vm.crApps.length - 1) && vm.IsHighPriority == false) {
                    UIControlService.msg_growl("error", "MESSAGE.NO_CHAIRMAN");
                    return;
                }
            }
            if (vm.CountCommittee < 3) {
                UIControlService.msg_growl("error", "MESSAGE.MIN_3");
                return;
            }
            vm.list = [];
            if (vm.rank1 == true) {
                if (vm.L1 == false) {
                    UIControlService.msg_growl("error", "MESSAGE.NO_EXCL1");
                    return;
                }
                else if (vm.L2 == false) {
                    UIControlService.msg_growl("error", "MESSAGE.NO_EXCL2");
                    return;
                }
                else {
                    for (var i = 0; i < vm.crApps.length; i++) {
                        var dt = {
                            LevelInfo: vm.crApps[i].level_info,
                            ID: vm.crApps[i].ID,
                            IsActive: vm.crApps[i].IsActive,
                            EmployeeID: vm.crApps[i].EmployeeID,
                            IsHighPriority: vm.crApps[i].IsHighPriority,
                            TotalEvaluation: vm.contractRequisitionId,
                            SysReference1: vm.crApps[i].SysReference1,
                            IsActive: true
                        }
                        vm.list.push(dt);
                    }
                }
            }

            else {
                for (var i = 0; i < vm.crApps.length; i++) {
                    var dt = {
                        ID: vm.crApps[i].ID,
                        LevelInfo: vm.crApps[i].level_info,
                        IsActive: vm.crApps[i].IsActive,
                        EmployeeID: vm.crApps[i].EmployeeID,
                        IsHighPriority: vm.crApps[i].IsHighPriority,
                        TotalEvaluation: vm.contractRequisitionId,
                        SysReference1: vm.crApps[i].SysReference1,
                        IsActive: true
                    }
                    vm.list.push(dt);
                }

            }
            for (var i = 0; i < vm.empNonAct.length; i++) {
                var dt = {
                    ID: vm.empNonAct[i].ID,
                    IsActive: vm.empNonAct[i].IsActive,
                    EmployeeID: vm.empNonAct[i].EmployeeID,
                    IsHighPriority: vm.empNonAct[i].IsHighPriority,
                    TotalEvaluation: vm.contractRequisitionId,
                    SysReference1: vm.empNonAct[i].SysReference1,
                    IsActive: false
                }
                vm.list.push(dt);
            }

            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SAVE_APPROVERS'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("Loading");
                    TotalEvaluasiJasaService.SaveCRApprovals(vm.list, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE_APPROVERS'));
                            sendMailTC();
                            $state.transitionTo('verifikasi-totalEval');
                        } else {
                            //UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_APPROVERS'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        //UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_APPROVERS'));
                    });
                }
            });  
        };

        vm.sendMailTC = sendMailTC;
        function sendMailTC() {
            TotalEvaluasiJasaService.sendMailCP({
                ID: vm.totalEval
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.EMAIL_SENT");
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }
    }
})();
