﻿(function () {
    'use strict';

    angular.module("app").controller("NegosiasiVHSChatCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiVHSService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiVHSService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.StepID = Number($stateParams.StepID);
        vm.NegoId = Number($stateParams.VHSNegoId);
        vm.VendorID = Number($stateParams.VendorID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        //vm.IsGenerate = Number($stateParams.IsGenerate);
        vm.Id = Number($stateParams.Id);
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.maxSize = 10;
        vm.init = init;
        vm.jLoad = jLoad;
        vm.nb = [];

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
            $translatePartialLoader.addPart('negosiasi');
            //UIControlService.loadLoading("Silahkan Tunggu...");
            //jLoad(1);
            loadTender();
            loadOvertime();
            //loadv();
        }


        vm.loadOvertime = loadOvertime;
        function loadOvertime() {
            NegosiasiVHSService.isOvertime({ ID: vm.StepID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.overtime = reply.data;
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        //tampil isi chat
       vm.jLoad = jLoad;
        function jLoad(current) {
            vm.nego = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            var tender = {
                VendorID: vm.VendorID,
                TenderStepDataID: vm.StepID
            }
            UIControlService.loadLoading("");
            NegosiasiVHSService.bychat(tender, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.nego = reply.data;
                    vm.VendorName = reply.data[0].VendorName;
                    vm.judul = reply.data[0].TenderName;
                    vm.vhsnegoid = reply.data[0].VHSNegoId;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        //load tender
        vm.loadTender = loadTender;
        function loadTender() {
            UIControlService.loadLoading("");
            NegosiasiVHSService.selectStep({ ID: vm.StepID }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    vm.TenderID = vm.step.TenderID;
                    vm.TenderRefID = vm.step.tender.TenderRefID;
                    vm.ProcPackType = vm.step.tender.ProcPackageType;

                    jLoad(1);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        //ambil data vendor
        vm.loadv = loadv;
        function loadv() {
            vm.lv = [];
            //vm.currentPage = current;
            // var offset = (current * 10) - 10;
            NegosiasiVHSService.select({
                column: vm.StepID,
                status: vm.TenderRefID,
                FilterType: vm.ProcPackType
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.lv = reply.data;
                    //ambil data isgenerate
                    for (var i = 0; i < vm.lv.length; i++) {
                        if (vm.lv[i].ID === vm.Id) {
                            vm.VendorName = vm.lv[i].VendorName;
                            vm.judul = vm.lv[i].TenderName;
                            vm.idnb = vm.lv[i].ID;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        //ke halaman detail penawaran
        vm.detailpenawaran = detailpenawaran;
        function detailpenawaran(flag) {
            $state.transitionTo('detail-penawaran-vhs', { TenderRefID: vm.TenderRefID, VendorID: vm.VendorID, ProcPackType: vm.ProcPackType, Id: vm.Id, StepID: vm.StepID });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('negosiasi-vhs', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, StepID: vm.StepID });
        }
        //tulis, insertchat
        vm.tulis = tulis;
        function tulis() {
            var data = {
                NegoId: vm.NegoId,
                VendorID: vm.VendorID,
                StepID: vm.StepID,
                Judul: vm.judul,
                VHSNegoId: vm.vhsnegoid
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-vhs/write-chat-vhs.html?v=1.000002',
                controller: 'WriteChatVHSCtrl',
                controllerAs: 'WriteChatVHSCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

    }
})();
//TODO


