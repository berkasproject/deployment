﻿(function () {
    'use strict';

    angular.module("app")
    .controller("DetailSubLineCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService',
        'NegosiasiService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService,
        NegosiasiService, $state, UIControlService, $uibModal, GlobalConstantService, $stateParams, $uibModalInstance) {
        var vm = this;
        vm.detail = item.item;
        vm.overtime = item.overtime;
        vm.VendorID = item.VendorID;
        vm.TenderRefID = item.TenderRefID;
        vm.ProcPackType = item.ProcPackType;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.textSearch = '';
        vm.listPengumuman = [];
        vm.currentPage = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi');

            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            loadData();
        }

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            NegosiasiService.selectLineCost({
                TenderRefID: vm.TenderRefID,
                VendorID: vm.VendorID,
                CRCESubId: vm.detail.ContractRequisitionCESubID,
                ProcPackType: vm.ProcPackType
            }, function (reply) {
                vm.ceLineOffers = reply.data;
                vm.offerTotalCost = 0;
                vm.ceSubTotalCost = 0;
                vm.negoTotalCost = 0;
                vm.ceSubRevTotalCost = 0;
                vm.ceLineOffers.forEach(function (sub) {
                    vm.offerTotalCost += sub.LineOfferCost;
                    vm.ceSubTotalCost += sub.LineCost;
                    vm.negoTotalCost += sub.LineNegoCost;

                    sub.LineRevCost = sub.QuantityNego * sub.UnitCost;
                    vm.ceSubRevTotalCost += sub.LineRevCost;
                });
                checkIsOpen(reply.data);
                UIControlService.unloadLoadingModal();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OFFER');
            });
        };




        vm.checkIsOpen = checkIsOpen;
        function checkIsOpen(data) {
            vm.IsOpenAll = false;
            var isOpenTrue = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].IsOpen === true) {
                    isOpenTrue = isOpenTrue + 1;
                }
            }
            if (isOpenTrue === data.length) {
                vm.IsOpenAll = true;
            }
        }

        vm.saveIsOpen = saveIsOpen;
        function saveIsOpen(data) {
            //console.info("data:" + JSON.stringify(data));
            var param = {
                IsOpen: data.IsOpen,
                ID: data.ID,
                NegoId: data.NegoId,
                CRCESubDetailId: data.CRCESubDetailId,
                SOEPDId: data.SOEPDId,
                UnitNegotiationPrice: data.UnitNegoCost,
                Quantity: data.QuantityNego
            };
            vm.savedata = [];
            vm.savedata.push(param);

            UIControlService.loadLoadingModal("");
            NegosiasiService.InsertOpen(vm.savedata,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                       init();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.unloadLoadingModal();
                   UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
               }
          );


        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.list = [];
            EvaluationTechnicalService.selectByEmployee({
                Keyword: vm.detail.EmployeeID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data;
                    console.info("data:" + JSON.stringify(vm.list));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Penilai" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        vm.loadOpenAll = loadOpenAll;
        function loadOpenAll() {
            //tanpa paging
            //console.info("leng" + JSON.stringify(vm.ceLineOffers.length));
            if (vm.IsOpenAll === true) {
                //console.info("bnr");
                for (var i = 0; i < vm.ceLineOffers.length; i++) {
                    vm.ceLineOffers[i].IsOpen = true;
                }
            }
            else if (vm.IsOpenAll === false) {
                //console.info("salah");
                for (var i = 0; i < vm.ceLineOffers.length; i++) {
                    vm.ceLineOffers[i].IsOpen = false;
                }
            }
            save();
            
        }

        vm.batal = batal;
        function batal() {
            //$uibModalInstance.dismiss('cancel');
            $uibModalInstance.close();
        };

        vm.save = save;
        function save() {
            vm.list = [];
            for (var i = 0; i < vm.ceLineOffers.length; i++) {
                if (vm.ceLineOffers[i].ID !== 0) {
                    var dt = {
                        IsOpen: vm.ceLineOffers[i].IsOpen,
                        ID: vm.ceLineOffers[i].ID,
                        NegoId: vm.ceLineOffers[i].NegoId,
                        CRCESubDetailId: vm.ceLineOffers[i].CRCESubDetailId,
                        SOEPDId: vm.ceLineOffers[i].SOEPDId,
                        UnitNegotiationPrice: vm.ceLineOffers[i].UnitNegoCost,
                        Quantity: vm.ceLineOffers[i].QuantityNego
                    };
                }
                else {
                    var dt = {
                        IsOpen: vm.ceLineOffers[i].IsOpen,
                        NegoId: vm.ceLineOffers[i].NegoId,
                        CRCESubDetailId: vm.ceLineOffers[i].CRCESubDetailId,
                        SOEPDId: vm.ceLineOffers[i].SOEPDId,
                        UnitNegotiationPrice: vm.ceLineOffers[i].UnitNegoCost,
                        Quantity: vm.ceLineOffers[i].QuantityNego
                    };
                }
                vm.list.push(dt);
            }
            UIControlService.loadLoadingModal("");
            NegosiasiService.InsertOpen(vm.list,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                       $uibModalInstance.close();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                   UIControlService.unloadLoadingModal();
               }
          );
        }

        vm.editQuantity = editQuantity;
        function editQuantity()
        {
            var item = {
                item: vm.detail,
                TenderRefID: vm.TenderRefID,
                VendorID: vm.VendorID,
                ProcPackType: vm.ProcPackType
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi/modal-edit-qty.html?v=1.000002',
                controller: 'EditQuantityNegoCtrl',
                controllerAs: 'editQtyCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

    }
}
)();