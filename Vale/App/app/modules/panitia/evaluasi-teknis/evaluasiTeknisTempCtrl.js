(function () {
	'use strict';

	angular.module("app").controller("EvaluationTechnicalCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'EvaluationTechnicalService', 'DataPengadaanService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$filter'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, EvaluationTechnicalService, DataPengadaanService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $filter) {

		var vm = this;
		var page_id = 141;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.evalsafety = [];
		vm.tenderStepData = {};
		vm.isForSafety = false;
		vm.userBisaMengatur = false;
		vm.allowAdd = true;
		vm.allowEdit = true;
		vm.allowDelete = true;
		vm.init = init;

		vm.jLoad = jLoad;
		//vm.loadAll = loadAll;
		//vm.ubah_aktif = ubah_aktif;
		//vm.tambah = tambah;
		//vm.edit = edit;

		function init() {
			$translatePartialLoader.addPart('evaluasi-teknis');
			UIControlService.loadLoading("MESSAGE.LOADING");

			DataPengadaanService.GetStepByID({ ID: vm.StepID }, function (reply) {
				vm.tenderStepData = reply.data;
				//vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
				EvaluationTechnicalService.isLess3Approved({
					ID: vm.StepID
				}, function (reply) {
					UIControlService.unloadLoading();
					if (reply.data === true) {
						jLoad();
					} else {
						UIControlService.msg_growl("error", 'MESSAGE.NOTIF_LESSTHAN3');
						backpengadaan();
					}
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.FAIL_GETDATA_APPROV');
				});
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
			});
		}


		vm.print = print;
		function print() {
			$state.transitionTo('evaluation-technical-print', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
		}

		vm.jLoad = jLoad;
		function jLoad() {

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
				if (vm.isAllowedEdit === false) {
					EvaluationTechnicalService.isEvaluator({
						TenderRefID: vm.TenderRefID
					}, function (reply) {
						vm.isAllowedEdit = reply.data;
					}, function (err) {
						UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
					});
				}
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			vm.evaltechnical = [];
			var tender = {
				ID: vm.StepID,
				tender: {
					TenderRefID: vm.TenderRefID,
					ProcPackageType: vm.ProcPackType
				}
			}
			UIControlService.loadLoading("MESSAGE.LOADING");
			EvaluationTechnicalService.getSummaryScore({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.evaltechnical = reply.data;
					EvaluationTechnicalService.isNeedTenderStepApproval({ ID: vm.StepID }, function (result) {
						if (result.data.FromSafety === undefined)
							vm.isNeedApproval = result.data;
						else {
							vm.isNeedApproval = result.data.Result;
							vm.isForSafety = true;
						}
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
					EvaluationTechnicalService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
						vm.isApprovalSent = result.data;
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
				UIControlService.unloadLoading();
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROV'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.SENDING');
					if (vm.isForSafety) {
						EvaluationTechnicalService.sendToApprovalSafety({ ID: vm.StepID }, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND');
								EvaluationTechnicalService.isNeedTenderStepApproval({ ID: vm.StepID }, function (result) {
									vm.isNeedApproval = result.data;
								}, function (err) {
									$.growl.error({ message: "Gagal mendapatkan data Approval" });
									UIControlService.unloadLoading();
								});
								EvaluationTechnicalService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
									vm.isApprovalSent = result.data;
								}, function (err) {
									$.growl.error({ message: "Gagal mendapatkan data Approval" });
									UIControlService.unloadLoading();
								});
							} else {
								$.growl.error({ message: "Send Approval Failed." });
								UIControlService.unloadLoading();
							}

						}, function (err) {
							$.growl.error({ message: "Gagal Akses API >" + err });
							UIControlService.unloadLoading();
						});
					} else {
						EvaluationTechnicalService.sendToApproval({ ID: vm.StepID }, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND');
								EvaluationTechnicalService.isNeedTenderStepApproval({ ID: vm.StepID }, function (result) {
									vm.isNeedApproval = result.data;
								}, function (err) {
									$.growl.error({ message: "Gagal mendapatkan data Approval" });
									UIControlService.unloadLoading();
								});
								EvaluationTechnicalService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
									vm.isApprovalSent = result.data;
								}, function (err) {
									$.growl.error({ message: "Gagal mendapatkan data Approval" });
									UIControlService.unloadLoading();
								});
							} else {
								$.growl.error({ message: "Send Approval Failed." });
								UIControlService.unloadLoading();
							}

						}, function (err) {
							$.growl.error({ message: "Gagal Akses API >" + err });
							UIControlService.unloadLoading();
						});
					}
				}
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			$translatePartialLoader.addPart('data-contract-requisition');
			var item = vm.StepID;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/evaluasi-teknis/detailApproval.html?v=1.000002',
				controller: "TechEvalApprvCtrl",
				controllerAs: "TechEvalApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () { });
		}

		vm.detail = detail;
		function detail(flag) {
			if (flag === 2) {
				$state.transitionTo('evaluasi-teknis-tim', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
			}
				/*
				else if (flag === 3) {
					$state.transitionTo('data-evaluator', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType });
				}
				*/
			else if (flag === 4) {
				var data = {
					tenderStepData: vm.tenderStepData,
					isCancelled: vm.tenderStepData.tender.IsCancelled,
					item: vm.evaltechnical,
					isAllowedEdit: vm.isAllowedEdit
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/panitia/evaluasi-teknis/FormEvaluator.html?v=1.000007',
					controller: 'FormEvaluator',
					controllerAs: 'FormEvaluator',
					resolve: {
						item: function () {
							return data;
						}
					}
				});
				modalInstance.result.then(function () {
					init();
				});
			}
		}

		vm.view = view;
		function view(data) {
			$state.transitionTo('equipment-evaluation-qualf-teknis', { TenderRefID: vm.TenderRefID, VendorID: data.VendorID, ProcPackType: vm.ProcPackType });
		}

		vm.detEvaluator = detEvaluator;
		function detEvaluator(data) {
			var data = {
				VendorName: data.VendorName,
				TenderStepDataID: vm.StepID,
				VendorID: data.VendorID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/evaluasi-teknis/detailEvaluatorTeknis.modal.html',
				controller: 'detailEvaluatorTeknisCtrl',
				controllerAs: 'dEvaluatorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
		}

		/*
        vm.Approval = Approval;
        function Approval() {
            var data = {
                item: vm.TenderRefID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/temporary/safetyEvaluation/DetailApproval.html',
                controller: 'DetailApprovalCtrl',
                controllerAs: 'DetailApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }
        */

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.tenderStepData.TenderID });
		}
	}
})();

