﻿(function () {
	'use strict';

	angular.module("app").controller("TechEvalApprvCtrl", ctrl);

	ctrl.$inject = ['$uibModalInstance', 'item', 'UIControlService', '$uibModal', 'EvaluationTechnicalService'];

	function ctrl($uibModalInstance, item, UIControlService, $uibModal, EvaluationTechnicalService) {
		var vm = this;
		vm.apprvs = [];
		vm.StepID = item;

		vm.getDetailApproval = getDetailApproval;
		function getDetailApproval() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			EvaluationTechnicalService.detailApproval({ ID: vm.StepID }, function (reply) {
			    UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.apprvs = reply.data;
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
				} else {
				    UIControlService.msg_growl('error', "Send Approval Failed.");
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
			    UIControlService.msg_growl('error', "Gagal Akses API >" + err);
			    UIControlService.unloadLoadingModal();
			});


			//RFQVHSService.detailApproval({
			//	ID: item
			//}, function (reply) {
			//	if (reply.status === 200) {
			//		UIControlService.unloadLoading();
			//		vm.apprvs = reply.data;
			//	} else {
			//		UIControlService.unloadLoading();
			//	}
			//}, function (err) {
			//	UIControlService.unloadLoading();
			//});
		}

		vm.closeDetaiApprv = closeDetaiApprv;
		vm.cancel = closeDetaiApprv;
		function closeDetaiApprv() {
		    $uibModalInstance.close();
		}


	}
})();