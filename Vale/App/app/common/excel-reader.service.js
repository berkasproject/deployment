﻿(function () {
    'use strict';

    angular.module('app').factory('ExcelReaderService', excelReaderService);

    excelReaderService.$inject = ['$upload', 'GlobalConstantService'];

    /* @ngInject */
    function excelReaderService($upload, GlobalConstantService) {
        
        var endpoint = GlobalConstantService.getConstant("api_endpoint");

        var service = {
            readExcel: readExcel
        };

        function readExcel(file, successCallback, errorCallback) {
            var param = {
                url: endpoint + '/readexcel',
                file: file,
                fields: {}
            };
            $upload.upload(param).then(successCallback, errorCallback);
        }

        return service;
    }
})();