﻿(function () {
	'use strict';

	angular.module('app').factory('SocketService', socketService);

	socketService.$inject = ['$rootScope', 'Hub', '$timeout'];

	/* @ngInject */
	function socketService($rootScope, Hub, $timeout) {
		var listener = {};
		//var socket = io.connect('http://142.40.33.118:1067', {
		//var socket = io.connect('http://localhost:1067', {
		//	'reconnection': true,
		//	'reconnectionDelay': 1000,
		//	'reconnectionDelayMax': 5000,
		//	'reconnectionAttempts': 5
		//});

		var socket = new Hub('eProcHub', {
			rootPath: 'http://localhost:55535/signalr',
			listeners: {
				'txServerDateTime': function (dateTime) {
				}
			},

			//server side methods
			//methods: ['txServerDateTime'],

			//handle connection error
			errorHandler: function (error) {
				console.error(error);
			},

			stateChanged: function (state) {
				switch (state.newState) {
					case $.signalR.connectionState.connecting:
						//your code here
						break;
					case $.signalR.connectionState.connected:
						//your code here
						break;
					case $.signalR.connectionState.reconnecting:
						//your code here
						break;
					case $.signalR.connectionState.disconnected:
						//your code here
						break;
				}
			}
		});

		var service = {
			on: onEventHandler,
			emit: emitEventHandler
		};

		function onEventHandler(eventName, callback) {
			socket.on(eventName, eventHandler);

			function eventHandler() {
				var args = arguments;

				$rootScope.$apply(function () {
					callback.apply(socket, args);
				});
			}
		}

		function emitEventHandler(eventName, data, callback) {
			//socket.emit(eventName, data, eventHandler);

			//function eventHandler() {
			//	var args = arguments;
			//	$rootScope.$apply(function () {
			//		if (callback) {
			//			callback.apply(socket, args);
			//		}
			//	});
			//}
		}

		return service;
	}
})();