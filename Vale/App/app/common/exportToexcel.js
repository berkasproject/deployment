﻿
(function () {
    'use strict';

    angular.module('app').factory('Excel', excel);

    excel.$inject = ['$window', '$http', '$q', '$rootScope', 'GlobalConstantService'];

    //@ngInject
    function excel($window, $http, $q, $rootScope, GlobalConstantService) {
        var uri = 'data:application/vnd.ms-excel;base64,',
           template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
           base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
           format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };

        function b64toBlob(b64Data, contentType, sliceSize) { //Supaya bisa diekspor di chrome jika datanya terlalu banyak
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        return {
            tableToExcel: function (tableId, worksheetName) {
                var table = document.querySelector(tableId),
                    ctx = { worksheet: worksheetName, table: table.innerHTML },
                    b64 = base64(format(template, ctx));

                var blob = b64toBlob(b64, "application/vnd.ms-excel");
                var blobUrl = URL.createObjectURL(blob);

                return blobUrl;
            }
        };
    }
})();