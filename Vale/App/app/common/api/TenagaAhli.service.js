(function () {
	'use strict';

	angular.module("app").factory("TenagaAhliService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

	    var endpoint = GlobalConstantService.getConstant("api_endpoint");
	    var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			all: all,
			update: update,
            insert: insert,
            editActive: editActive,
            selectCertificate: selectCertificate,
            editCertificateActive: editCertificateActive,
            updateExpertCertificate: updateExpertCertificate,
            insertExpertCertificate: insertExpertCertificate,
            selectVendor: selectVendor,
            GetAllNationalities: GetAllNationalities,
            getCRbyVendor: getCRbyVendor
		};

		return service;

		// implementation
		function all(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExperts/selectByID", param).then(successCallback, errorCallback);
		}
		function selectCertificate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificate/select", param).then(successCallback, errorCallback);
		}
		
		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExperts/update", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExperts/create", param).then(successCallback, errorCallback);
		} 
		function editActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExperts/updateActive", param).then(successCallback, errorCallback);
		} 
		function editCertificateActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificate/updateActive", param).then(successCallback, errorCallback);
		}
		function updateExpertCertificate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificate/update", param).then(successCallback, errorCallback);
		}
		function insertExpertCertificate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificate/create", param).then(successCallback, errorCallback);
		}

		function selectVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}

		function GetAllNationalities(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificate/getallnationalities").then(successCallback, errorCallback);
		}
		function getCRbyVendor(param, successCallback, errorCallback) {
		    //console.info("service");
		    GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
	}
})();