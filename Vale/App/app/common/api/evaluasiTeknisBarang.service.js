﻿(function () {
	'use strict';

	angular.module("app").factory("EvaluasiTeknisBarangService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			isEvaluator: isEvaluator,
			getEvaluator: getEvaluator,
			selectOfferEntries: selectOfferEntries,
			getOfferEntryDetailDocs: getOfferEntryDetailDocs,
			getEvaluationScoresByVendor: getEvaluationScoresByVendor,
			getRFQGoodsLimit: getRFQGoodsLimit,
			getEvaluationScore: getEvaluationScore,
			selectEvaluations: selectEvaluations,
			setEvaluationResult: setEvaluationResult,
			setEvaluationResultAll: setEvaluationResultAll,
			isCheckedAll: isCheckedAll,
			setEvaluationScore: setEvaluationScore,
			isLess3Approved: isLess3Approved,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			sendToApproval: sendToApproval,
			detailApproval: detailApproval
		};

		return service;

		function isEvaluator(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/isevaluator", param).then(successCallback, errorCallback);
		}

		function getEvaluator(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/getevaluator", param).then(successCallback, errorCallback);
		}

		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/isNeedTenderStepApproval", param).then(successCallback, errorCallback);
		}

		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/goodstechnicalevaluation/sendToApproval', param).then(successCallback, errorCallback);
		}

		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/goodstechnicalevaluation/isApprovalSent', param).then(successCallback, errorCallback);
		}

		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/goodstechnicalevaluation/detailApproval', param).then(successCallback, errorCallback);
		}

		function selectOfferEntries(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/selectofferentries", param).then(successCallback, errorCallback);
		}

		function getOfferEntryDetailDocs(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/getoedetaildocs", param).then(successCallback, errorCallback);
		}

		function getEvaluationScoresByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/getevaluationscorebyvendor", param).then(successCallback, errorCallback);
		}

		function getRFQGoodsLimit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/getrfqgoodslimit", param).then(successCallback, errorCallback);
		}

		function getEvaluationScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/getevaluationscore", param).then(successCallback, errorCallback);
		}

		function selectEvaluations(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/selectevaluations", param).then(successCallback, errorCallback);
		}

		function setEvaluationResult(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/setevaluationresult", param).then(successCallback, errorCallback);
		}

		function setEvaluationResultAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/setevaluationresultall", param).then(successCallback, errorCallback);
		}

		function isCheckedAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/ischeckedall", param).then(successCallback, errorCallback);
		}

		function setEvaluationScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/setevaluationscore", param).then(successCallback, errorCallback);
		}

		function isLess3Approved(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodstechnicalevaluation/isless3approved", param).then(successCallback, errorCallback);
		}
	}
})();