(function () {
	'use strict';

	angular.module("app").factory("AktaPendirianService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			Create: Create,
			Edit: Edit,
			Delete: Delete,
			GetById: GetById,
			GetByVendor: GetByVendor,
			GetCities: GetCities,
			GetCurrencies: GetCurrencies,
			GetStockTypes: GetStockTypes,
			GetVendorStocks: GetVendorStocks,
			CreateVendorStock: CreateVendorStock,
			EditVendorStock: EditVendorStock,
			DeleteVendorStock: DeleteVendorStock,
			EditLegalStock: EditLegalStock,
			isVerified: isVerified,
			GetDetailVendor: GetDetailVendor,
			getCRbyVendor: getCRbyVendor,
			getStockOption: getStockOption,
            cekPrakualifikasiVendor:cekPrakualifikasiVendor

		};
		return service;

		function getCRbyVendor(param, successCallback, errorCallback) {
			//console.info("service");
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}

		function GetDetailVendor(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/getDetailVendor").then(successCallback, errorCallback);
		}
		function isVerified(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}
		function Create(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/create", param).then(successCallback, errorCallback);
		}
		function Edit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/edit", param).then(successCallback, errorCallback);
		}
		function Delete(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/delete", param).then(successCallback, errorCallback);
		}
		function GetById(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/getLegalDocumentByID", param).then(successCallback, errorCallback);
		}
		function GetByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/getLegalDocumentByVendor", param).then(successCallback, errorCallback);
		}

		function GetCities(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/getCities").then(successCallback, errorCallback);
		}

		function GetCurrencies(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/currency/list").then(successCallback, errorCallback);
		}

		function GetStockTypes(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/reference/stock-unit").then(successCallback, errorCallback);
		}

		function GetVendorStocks(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorStock/getByVendor", param).then(successCallback, errorCallback);
		}

		function CreateVendorStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorStock/create", param).then(successCallback, errorCallback);
		}

		function EditVendorStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorStock/edit", param).then(successCallback, errorCallback);
		}

		function DeleteVendorStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorStock/delete", param).then(successCallback, errorCallback);
		}

		function EditLegalStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/legalDocument/editlegalstock", param).then(successCallback, errorCallback);
		}

		function getStockOption(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/reference/stock-option").then(successCallback, errorCallback);
		}
		function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
		}
	}
})();