﻿(function () {
    'use strict';

    angular.module("app").factory("TotalEvaluasiJasaService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            checkPriceEvaluation: checkPriceEvaluation,
            getDataTender: getDataTender,
            getDataStepTender: getDataStepTender,
            InsertTotalEvaluation: InsertTotalEvaluation,
            approve: approve,
            getDataEvaTechPrice: getDataEvaTechPrice,
            getMethodEvaluation: getMethodEvaluation,
            getFinalTotalEval: getFinalTotalEval,
            sendToApproval: sendToApproval,
            getDataVerification: getDataVerification,
            SelectEmployee: SelectEmployee,
            SaveCRApprovals: SaveCRApprovals,
            GetCRApprovals: GetCRApprovals,
            SetApprovalStatus: SetApprovalStatus,
            CekEmployee: CekEmployee,
            getLogin: getLogin,
            getLoginCP: getLoginCP,
            getReferenceType: getReferenceType,
            getStepTotalEvaluation: getStepTotalEvaluation,
            sendMailCP: sendMailCP,
            sendMail: sendMail,
            getDataForApprovalTC: getDataForApprovalTC,
            evaluateApprovalStatuses: evaluateApprovalStatuses
        };

        return service;

        // implementation
        function checkPriceEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/checkPriceEvaluation", param).then(successCallback, errorCallback);
        }
        function getDataForApprovalTC(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getDataForApprovalTC", param).then(successCallback, errorCallback);
        }
        function sendMail(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/SendMailApproval", param).then(successCallback, errorCallback);
        }
        function sendMailCP(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/SendMainCP", param).then(successCallback, errorCallback);
        }
        function getStepTotalEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/cekStep", param).then(successCallback, errorCallback);
        }
        function getReferenceType(successCallback, errorCallback) {
            var param = { Keyword: "APPROVAL_TYPE_TC" };
            GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
        }
        function getLoginCP(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getUserLoginCP").then(successCallback, errorCallback);
        }
        function getLogin(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getUserLogin", param).then(successCallback, errorCallback);
        }
        function CekEmployee(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/CekEmployee", param).then(successCallback, errorCallback);
        }
        function SetApprovalStatus(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/SetApprovalStatus", param).then(successCallback, errorCallback);
        }
        function GetCRApprovals(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/GetCRApprovals", param).then(successCallback, errorCallback);
        }
        function SaveCRApprovals(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/insertContractPlanning", param).then(successCallback, errorCallback);
        }
        function SelectEmployee(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/selectemployee", param).then(successCallback, errorCallback);
        }
        function getDataVerification(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getDataVerification", param).then(successCallback, errorCallback);
        }
        function sendToApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/insertApproval", param).then(successCallback, errorCallback);
        }
        function getFinalTotalEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getDataByTender", param).then(successCallback, errorCallback);
        }
        function getMethodEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getDataTenderPackage", param).then(successCallback, errorCallback);
        }
        function getDataEvaTechPrice(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/getDataTechPrice", param).then(successCallback, errorCallback);
        }
        function InsertTotalEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/insertupdate", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/approve", param).then(successCallback, errorCallback);
        }
        function getDataTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/tender/getinfo", param).then(successCallback, errorCallback);
        }
        function getDataStepTender(param, successCallback, errorCallback) {
            //{ID: vm.IDStepTender}
            GlobalConstantService.post(adminpoint + "/totalEvalution/stepbyid", param).then(successCallback, errorCallback);
        }
        function evaluateApprovalStatuses(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/totalEvalution/evaluateapprovalstatus").then(successCallback, errorCallback);
        }
    }
})();