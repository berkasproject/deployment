﻿(function () {
    'use strict';

    angular.module("app").factory("EvaluationQualificationService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            selectByVendor: selectByVendor,
            selectByCode: selectByCode,
            selectByQuestionnaire: selectByQuestionnaire,
            InsertDetail: InsertDetail,
            InsertChecklistDetail: InsertChecklistDetail,
            InsertQuestionnaireDetail: InsertQuestionnaireDetail,
            getStep: getStep

        };

        return service;

        function select(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/getByTender", param).then(successCallback, errorCallback);
        }
        function selectByVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/getByTenderVendor", param).then(successCallback, errorCallback);
        }
        function selectByCode(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/getByTenderVendorForm", param).then(successCallback, errorCallback);
        }
        function selectByQuestionnaire(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/getByTenderVendorQuestionnaire", param).then(successCallback, errorCallback);
        }
        function InsertDetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/insert", param).then(successCallback, errorCallback);
        }
        function InsertChecklistDetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/insertForm", param).then(successCallback, errorCallback);
        }
        function InsertQuestionnaireDetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/insertQuestionnaire", param).then(successCallback, errorCallback);
        }
        function getStep(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/qualification/getStep", param).then(successCallback, errorCallback);
        }

    }
})();