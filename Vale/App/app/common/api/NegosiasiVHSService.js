﻿(function () {
    'use strict';

    angular.module("app").factory("NegosiasiVHSService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        var vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var service = {
            GetSteps: GetSteps,
            isOvertime: isOvertime,
            select: select,
            selectStep: selectStep,
            insert: insert,
            aceptNego : aceptNego,
            bychat: bychat,
            insertchat: insertchat,
            itemall: itemall,
            update: update,
            updatedetail: updatedetail,
            cek: cek,
            updatedeal: updatedeal,
            CanReOpenNego: CanReOpenNego,
            ReOpenNegotiation: ReOpenNegotiation,
            editactive: editactive,
            bychatv: bychatv,
            insertchatv: insertchatv,
            itemallv: itemallv,
            updatev: updatev,
            updatedetailv: updatedetailv,
            updateitem: updateitem,
            InsertApproval: InsertApproval,
            sendMailToApprover: sendMailToApprover,
            GetApproval: GetApproval,
            GetNegoToApproval: GetNegoToApproval,
            updateApproval: updateApproval,
            sendMailToCE: sendMailToCE,
            sendMail: sendMail,
            loadStep: loadStep

            
        };

        return service;
        function loadStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/getTenderStep", param).then(successCallback, errorCallback);
        }
        function sendMail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/sendMail", param).then(successCallback, errorCallback);
        }
        function sendMailToCE(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/sendMailToCE", param).then(successCallback, errorCallback);
        }
        function updateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/updateApproval", param).then(successCallback, errorCallback);
        }
        function GetNegoToApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/GetNegoToApproval", param).then(successCallback, errorCallback);
        }
        function GetApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/GetApproval", param).then(successCallback, errorCallback);
        }
        function sendMailToApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/sendMailToApprover", param).then(successCallback, errorCallback);
        }
        function InsertApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/InsertApproval", param).then(successCallback, errorCallback);
        }


        //-- vendor
        function GetSteps(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorendpoint + "/VOE/tahapan/getsteps", param).then(successCallback, errorCallback);
        }

        // --- admin
        function isOvertime(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tahapan/GoodsnegosiasiVHS/getsteps", param).then(successCallback, errorCallback);
        }
        function selectStep(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi", param).then(successCallback, errorCallback);
        }
        function insert(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/Insert", param).then(successCallback, errorCallback);
        }
        function aceptNego(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/acceptnego", param).then(successCallback, errorCallback);
        }
        function bychat(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/ByChat", param).then(successCallback, errorCallback);
        }
        function insertchat(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/InsertChat", param).then(successCallback, errorCallback);
        }
        function itemall(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/ItemAll", param).then(successCallback, errorCallback);
        }
        function update(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/Update", param).then(successCallback, errorCallback);
        }
        function updatedetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/UpdateDetail", param).then(successCallback, errorCallback);
        }
        function editactive(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/EditActive", param).then(successCallback, errorCallback);
        }
        function CanReOpenNego(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/CanReOpenNego", param).then(successCallback, errorCallback);
        }
        function ReOpenNegotiation(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/ReOpenNegotiation", param).then(successCallback, errorCallback);
        }
        function updatedeal(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/UpdateDeal", param).then(successCallback, errorCallback);
        }
        function cek(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/VHSnegosiasi/Cek", param).then(successCallback, errorCallback);
        }
        // --- vendor
        function bychatv(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(vendorendpoint + "/VHSnegosiasi/ByChat", param).then(successCallback, errorCallback);
        }
        function insertchatv(param, successCallback, errorCallback) {
            //console.info("masuk service!! " + JSON.stringify(param));
            GlobalConstantService.post(vendorendpoint + "/VHSnegosiasi/InsertChat", param).then(successCallback, errorCallback);
        }
        function itemallv(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(vendorendpoint + "/VHSnegosiasi/ItemAll", param).then(successCallback, errorCallback);
        }
        function updatev(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(vendorendpoint + "/VHSnegosiasi/Update", param).then(successCallback, errorCallback);
        }
        function updatedetailv(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(vendorendpoint + "/VHSnegosiasi/UpdateDetail", param).then(successCallback, errorCallback);
        }
        function updateitem(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(vendorendpoint + "/VHSnegosiasi/UpdatePerItem", param).then(successCallback, errorCallback);
        }
    }
})();