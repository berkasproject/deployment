﻿(function () {
    'use strict';

    angular.module("app").factory("VendorNotActiveService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var admin = GlobalConstantService.getConstant("admin_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            SelectAll: SelectAll,
            editActive: editActive

        };
        return service;

        function SelectAll(param, successCallback, errorCallback) {
            GlobalConstantService.post(admin + "/vendorNotActive", param).then(successCallback, errorCallback);
        }
        function editActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(admin + "/editActive", param).then(successCallback, errorCallback);
        }
    }
})();