﻿(function () {
	'use strict';

	angular.module("app").factory("UploadFileConfigService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
			all: all,
			getByModule: byModule,
			getByPageName: byPageName,
			count: count,
			getTypes: getTypes,
			getSizes: getSizes,
			selectpagelayer: selectpagelayer,
			selectpagename: selectpagename,
			selecttypefile: selecttypefile,
			loadFileUploadConfig: loadFileUploadConfig
		};

		return service;

		// implementation

		function loadFileUploadConfig(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/configuration/loadFileUploadConfig', param).then(successCallback, errorCallback);
		}

		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/configuration/upload-config").then(successCallback, errorCallback);
		}

		function byModule(module, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/configuration/upload-config/" + module).then(successCallback, errorCallback);
		}

		function byPageName(page, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/configuration/upload-config-per-page",
            { Keyword: page }).then(successCallback, errorCallback);
		}

		function getTypes(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/configuration/upload-file-types").then(successCallback, errorCallback);
		}

		function getSizes(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/configuration/upload-file-sizes").then(successCallback, errorCallback);
		}

		function count(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/fileconf/count").then(successCallback, errorCallback);
		}

		function cektype(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/fileconf/cektype").then(successCallback, errorCallback);
		}

		function selectpagelayer(successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/fileconf/selectpagelayer").then(successCallback, errorCallback);
		}

		function selectpagename(successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/fileconf/selectpagename").then(successCallback, errorCallback);
		}

		function selecttypefile(successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/fileconf/selecttypefile").then(successCallback, errorCallback);
		}
	}
})();