(function () {
	'use strict';

	angular.module("app").factory("DataAdministrasiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			select: select,
			selectcontact: selectcontact,
			SelectBusinessField: SelectBusinessField,
			SelectComodity: SelectComodity,
			SelectBusinessEntity: SelectBusinessEntity,
			getCurrenciesVendor: getCurrenciesVendor,
			getCurrencies: getCurrencies,
			getAssociation: getAssociation,
			SelectState: SelectState,
			SelectCity: SelectCity,
			SelectDistrict: SelectDistrict,
			insert: insert,
			getSupplier: getSupplier,
			getTypeVendor: getTypeVendor,
			SelectRegion: SelectRegion,
			SelectCountry: SelectCountry,
			SelectVendorCommodity: SelectVendorCommodity,
			getCRbyVendor: getCRbyVendor,
			checkUsername: checkUsername,
			checkEmail: checkEmail,
			businessfieldValidation: businessfieldValidation,
            cekPrakualifikasiVendor:cekPrakualifikasiVendor
		};

		return service;

	    // implementation
		function checkEmail(param, successCallback, errorCallback) {
		    console.info(param);
		    GlobalConstantService.post(endpoint + "/administrasi/check-email", param).then(successCallback, errorCallback);
		}
		function checkUsername(param, successCallback, errorCallback) {
		    console.info(param);
		    GlobalConstantService.post(endpoint + "/checkUser", param).then(successCallback, errorCallback);
		}
		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/administrasi-select", param).then(successCallback, errorCallback);
		}

		function selectcontact(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
		}

		function SelectBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/businessField/goodsorservice", param).then(successCallback, errorCallback);
		}
		function SelectComodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/comodity/selectbybussinessfield", param).then(successCallback, errorCallback);
		}
		function SelectVendorCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/commodityall", param).then(successCallback, errorCallback);
		}
		function SelectBusinessEntity(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/business").then(successCallback, errorCallback);
		}
		function getCurrencies(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/administrasi-getCurrency").then(successCallback, errorCallback);
		}

		function getCurrenciesVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorstock/getID", param).then(successCallback, errorCallback);
		}

		function SelectRegion(param, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/continents", param)
                .then(successCallback, errorCallback);
		}

		function SelectCountry(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/countries")
                .then(successCallback, errorCallback);
		}

		function SelectState(country, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/states/" + country).then(successCallback, errorCallback);
		}

		function SelectCity(state, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/cities/" + state).then(successCallback, errorCallback);
		}

		function SelectDistrict(city, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/districts/" + city).then(successCallback, errorCallback);
		}

		function getAssociation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/association/search", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/update/vendor", param).then(successCallback, errorCallback);
		}

		function getSupplier(successCallback, errorCallback) {
			var param = { Keyword: "SUPPLIER_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getTypeVendor(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getTypeVendor(successCallback, errorCallback) {
		    var param = { Keyword: "VENDOR_TYPE" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function businessfieldValidation(param, successCallback, errorCallback) {
		    //console.info("param" + JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/administrasi/businessfieldValidation", param).then(successCallback, errorCallback);
		}
		function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
		}
	}
})();