﻿(function () {
	'use strict';

	angular.module("app").factory("TemplatePanitiaService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = { all: all, select: select };

		return service;

		// implementation
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/news").then(successCallback, errorCallback);
		}

		function find(id, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/detail/" + id).then(successCallback, errorCallback);
		}

		function list(keyword, offset, limit) {

		}

		function select(param, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/berita/select", param).then(successCallback, errorCallback);
		}

		function count(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/berita/count").then(successCallback, errorCallback);
		}

		function save(param, successCallback, errorCallback) {

		}

	}
})();