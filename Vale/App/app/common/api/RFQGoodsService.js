﻿(function () {
	'use strict';

	angular.module('app').factory('RFQGoodsService', service);

	service.$inject = ['GlobalConstantService'];

	function service(GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant('admin_endpoint');

		var service = {
		    SelectById: SelectById,
		    GenerateCode: GenerateCode, 
		    Select: Select,
		    Create: Create,
		    Update: Update,
		    SelectDoc: SelectDoc,
		    CreateDoc: CreateDoc,
		    UpdateDoc: UpdateDoc,
		    DeleteDoc: DeleteDoc,
		    UpdateMaterials: UpdateMaterial,
		    SelectAvailablePR: SelectAvailablePR,
		    SelectMaterial: SelectMaterial,
		    GetStates: GetStates,
		    GetCitiesByState: GetCitiesByState
		};

		return service;

		function SelectById(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/selectbyid', param).then(successCallback, errorCallback);
		}

		function Select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/select', param).then(successCallback, errorCallback);
		}

		function GenerateCode(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/generatecode').then(successCallback, errorCallback);
		}

		function Create(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/create', param).then(successCallback, errorCallback);
		}

		function Update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/update', param).then(successCallback, errorCallback);
		}

		function SelectDoc(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/selectdoc', param).then(successCallback, errorCallback);
		}

		function CreateDoc(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/createdoc', param).then(successCallback, errorCallback);
		}

		function UpdateDoc(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/updatedoc', param).then(successCallback, errorCallback);
		}

		function DeleteDoc(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/deletedoc', param).then(successCallback, errorCallback);
		}

		function UpdateMaterials(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/updatemtrl', param).then(successCallback, errorCallback);
		}

		function SelectAvailablePR(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/availablepr', param).then(successCallback, errorCallback);
		}

		function SelectMaterial(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/selectmaterial', param).then(successCallback, errorCallback);
		}

		function GetStates(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/getstates').then(successCallback, errorCallback);
		}

		function GetCitiesByState(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/rfqgoods/getcitiesbystate', param).then(successCallback, errorCallback);
		}
	}
})();