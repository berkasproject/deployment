(function () {
	'use strict';

	angular.module("app").factory("IzinUsahaService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			all: all,
			getClasification: getClasification,
			selectLicensi: selectLicensi,
			updateLicensi: updateLicensi,
			isVerified: isVerified,
			sendMail: sendEmail,
			selectcontact: selectcontact,
			getCRbyVendor: getCRbyVendor,
			deleteLic: deleteLic,
			cekDate: cekDate
        };

        return service;

        // implementation
        function cekDate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendorLicense/cekDate", param).then(successCallback, errorCallback);
        }
		function isVerified(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}

		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function getClasification(successCallback, errorCallback) {
			var param = { Keyword: "COMPANY_SCALE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function selectLicensi(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/license/select").then(successCallback, errorCallback);
		}

		function updateLicensi(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/update-license", param).then(successCallback, errorCallback);
		}

		function sendEmail(mail, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/registration/send-email", mail).then(successCallback, errorCallback);
		}

		function selectcontact(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
		}

		function getCRbyVendor(param, successCallback, errorCallback) {
			//console.info("service");
			//GlobalConstantService.post(vendorpoint + "/changerequest/getApprovedByVendorID", param).then(successCallback, errorCallback);
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}

		function deleteLic(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/deleteLicense", param).then(successCallback, errorCallback);
		}
	}
})();