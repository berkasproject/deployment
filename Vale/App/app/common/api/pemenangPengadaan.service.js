(function () {
    'use strict';

    angular.module("app")
    .factory("PemenangPengadaanService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            all: all
        };

        return service;
        // implementation
        function all(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/pengumumanPengadaan/select").then(successCallback, errorCallback);
        }

        function find(id, successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/detail/" + id).then(successCallback, errorCallback);
        }

        function list(keyword, offset, limit) {

        }



    }
})();