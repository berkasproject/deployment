﻿(function () {
    'use strict';

    angular.module("app").factory("ListDOAService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            editActive: editActive,
            cekData: cekData,
            insert: insert,
            update: update
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            //console.info("masuk service!! " + JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/DOA/select", param).then(successCallback, errorCallback);
        }

        function editActive(param, successCallback, errorCallback) {
            //console.info("serv:" + JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/business/editActive", param).then(successCallback, errorCallback);
        }

        function cekData(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/Business/cek", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/business/insert", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/business/update", param).then(successCallback, errorCallback);
        }
    }
})();