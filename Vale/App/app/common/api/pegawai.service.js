﻿(function () {
	'use strict';

	angular.module("app").factory("PegawaiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			all: all,
			all2: all2,
			Select: Select,
			Select2: Select2,
			//update: update,
			update2: update2
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(adminpoint + "/employee").then(successCallback, errorCallback);
		}
		function all2(successCallback, errorCallback) {
			GlobalConstantService.get(adminpoint + "/employee/all").then(successCallback, errorCallback);
		}

		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/employee/select", param).then(successCallback, errorCallback);
		}
		function Select2(param, successCallback, errorCallback) {
			//console.info("aa");
			GlobalConstantService.post(adminpoint + "/employee/select2", param).then(successCallback, errorCallback);
		}
        /*
		function update(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/employee/update", param).then(successCallback, errorCallback);
		}
        */
		function update2(param, successCallback, errorCallback) {
			//console.info("b");
			GlobalConstantService.post(adminpoint + "/employee/update2", param).then(successCallback, errorCallback);
		}
	}
})();