﻿(function () {
	'use strict';

	angular.module("app").factory("ApprovalForMin3Service", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			select: select,
			approve: approve,
			reject: reject,
			viewVendor: viewVendor
		};

		return service;

		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/tenderStepApproval/select", param).then(successCallback, errorCallback);
		}
		function approve(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/tenderStepApproval/approve", param).then(successCallback, errorCallback);
		}
		function reject(param, successCallback, errorCallback) {
			//console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/tenderStepApproval/reject", param).then(successCallback, errorCallback);
		}
		function viewVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderStepApproval/viewVendor", param).then(successCallback, errorCallback);
		}
	}
})();