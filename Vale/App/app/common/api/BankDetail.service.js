﻿(function () {
	'use strict';

	angular.module("app").factory("BankDetailService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			Select: Select,
			insert: insert,
			Update: Update,
			remove: remove,
			SelectVend: SelectVend,
			getCRbyVendor: getCRbyVendor,
			getCurrencies: getCurrencies
		};

		return service;

	    // implementation
		function getCurrencies(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/bankdetail/currency/list")
                .then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/bank").then(successCallback, errorCallback);
		}
		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/select", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/insert", param).then(successCallback, errorCallback);
		}

		function Update(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/update", param).then(successCallback, errorCallback);
		}

		function remove(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/remove", param).then(successCallback, errorCallback);
		}
		function SelectVend(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/getByVendorID", param).then(successCallback, errorCallback);
		}

		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
	}
})();