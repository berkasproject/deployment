﻿(function () {
	'use strict';

	angular.module("app").factory("JabatanPegawaiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			select: select,
			editActive: editActive,
			cekData: cekData,
			insert: insert,
            update: update
		};

		return service;

		// implementation
		function select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/jabatan/select", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/jabatan/insert", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/jabatan/update", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/jabatan/editActive", param).then(successCallback, errorCallback);
		}

		function cekData(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/jabatan/cek", param).then(successCallback, errorCallback);
		}
	}
})();