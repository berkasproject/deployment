﻿(function () {
	'use strict';

	angular.module('app').factory('ProcurementMethodService', procMethodSvc);

	procMethodSvc.$inject = ['GlobalConstantService'];

	//@ngInject
	function procMethodSvc(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('api_endpoint');

		//interfaces
		var service = {
			getTenderSteps: getTenderSteps,
			createMethod: createMethod,
			select: select,
			getProcMethodByID: getProcMethodByID,
			editMethod: editMethod,
			activateInactivate: activateInactivate,
			isInUse: isInUse,
			getAllowChange: getAllowChange
		};

		return service;

		function isInUse(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/isInUse', model).then(successCallback, errorCallback);
		}

		function getTenderSteps(getTenderStepsModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/getTenderSteps', getTenderStepsModel).then(successCallback, errorCallback);
		}

		function createMethod(createMethodModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/createMethod', createMethodModel).then(successCallback, errorCallback);
		}

		function select(selectModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/select', selectModel).then(successCallback, errorCallback);
		}

		function getProcMethodByID(getByIdModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/getByID', getByIdModel).then(successCallback, errorCallback);
		}

		function editMethod(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/editMethod', model).then(successCallback, errorCallback);
		}

		function activateInactivate(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/procMethod/activateInactivate', model).then(successCallback, errorCallback);
		}

		function getAllowChange(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/procMethod/getAllowChange", param).then(successCallback, errorCallback);
		}
	}
})();