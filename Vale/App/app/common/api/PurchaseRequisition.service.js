﻿(function () {
	'use strict';

	angular.module("app").factory("PurchaseRequisitionService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			getCommodity: getCommodity,
			getClasification: getClasification,
			isAllowedUploadItem: isAllowedUploadItem,
			insertUploadExcel: insertUploadExcel,
			deleteItemPR: deleteItemPR,
			getDataExcel: getDataExcel,
			viewVendor: viewVendor,
			selectDataPR: selectDataPR,
			getTypeTender: getTypeTender,
			getOptionsTender: getOptionsTender,
			getDeliveryTerms: getDeliveryTerms,
			getIncoTerms: getIncoTerms,
			getBidderMethod: getBidderMethod,
			getProcMethods: getProcMethods,
			getEvalMethod: getEvalMethod,
			getStateDelivery: getStateDelivery,
			getCityDelivery: getCityDelivery,
			getSteps: getSteps,
			getAllVendors: getAllVendors,
			insertPRRFQ: insertPRRFQ,
			selectPRRFQByID: selectPRRFQByID,
			updatePRRFQ: updatePRRFQ,
			getDataPROutstanding: getDataPROutstanding,
			getDataItemPR: getDataItemPR,
			selectMaterial: selectMaterial,
			selectConcessions: selectConcessions,
			selectHarmonizedCodes: selectHarmonizedCodes,
			selectPurchasingGroups: selectPurchasingGroups,
			updateMaterial: updateMaterial,
			updateCommodity: updateCommodity,
			updateConcessionCategory: updateConcessionCategory,
			updateHarmonizedCode: updateHarmonizedCode,
			updatePurchasingGroup: updatePurchasingGroup,
			selectDoc: selectDoc,
			insertDoc: insertDoc,
			updateDoc: updateDoc,
			deleteDoc: deleteDoc,
			sendApproval: sendApproval,
			Approval: Approval,
			Reject: Reject,
			Publish: Publish,
			generateCode: generateCode,
			getApprovalData: getApprovalData,
			selectcommite: selectcommite,
			selectposition: selectposition,
			selectemployee: selectemployee,
			insertCE: insertCE,
			GetApproval: GetApproval,
			getPaymentTerm: getPaymentTerm,
			getDataGoods: getDataGoods,
			getMailContent: getMailContent,
			sendMail: sendMail,
			getItemPR: getItemPR,
			getVendorEmail: getVendorEmail,
			sendMailAssosiasi: sendMailAssosiasi,
			getUserLogin: getUserLogin,
			insertStep: insertStep,
			cekEmails: cekEmails,
			getDataItemPRUpdate: getDataItemPRUpdate,
			getDataReviewer: getDataReviewer,
			getEmail: getEmail,
			sendMailRFQGoods: sendMailRFQGoods,
			viewVendorSave: viewVendorSave,
			selectFreight: selectFreight,
			cancelRFQ: cancelRFQ
		};

		return service;

		// implementation
		function cancelRFQ(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/cancelRFQ", param).then(successCallback, errorCallback);
		}
		function selectFreight(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/cekIncoFreight", param).then(successCallback, errorCallback);
		}
		function viewVendorSave(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/viewVendorSave', model).then(successCallback, errorCallback);
		}
		function sendMailRFQGoods(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/SendMail", param).then(successCallback, errorCallback);
		}
		function getEmail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/emailcontent/select", param).then(successCallback, errorCallback);
		}
		function getDataReviewer(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/getdataReviewer", param).then(successCallback, errorCallback);
		}
		function cekEmails(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/cekEmail", param).then(successCallback, errorCallback);
		}
		function insertStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updateTenderStep", param).then(successCallback, errorCallback);
		}
		function getUserLogin(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/GetUserLogin", param).then(successCallback, errorCallback);
		}
		function sendMailAssosiasi(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/send-email-Assosiasi", param).then(successCallback, errorCallback);
		}
		function getVendorEmail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/getVendorEmail", param).then(successCallback, errorCallback);
		}
		function getItemPR(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/getDataItemPR", param).then(successCallback, errorCallback);
		}
		function sendMail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/send-email", param).then(successCallback, errorCallback);
		}
		function getMailContent(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/getMailContent", param).then(successCallback, errorCallback);
		}
		function getDataGoods(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/getDataGoods", param).then(successCallback, errorCallback);
		}
		function getPaymentTerm(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/RFQVHS/getPymentTerm").then(successCallback, errorCallback);
		}
		function GetApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/GetApprovalEmployee", param).then(successCallback, errorCallback);
		}
		function insertCE(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/contractEngineergoodcommittee/insertCommittee", param).then(successCallback, errorCallback);
		}
		function selectemployee(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/contractEngineergood/employee", param).then(successCallback, errorCallback);
		}
		function selectposition(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/contractEngineergoodcommittee/selectcommitteAllPosition").then(successCallback, errorCallback);
		}
		function selectcommite(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/contractEngineergoodcommittee/selectcommitteAll', param).then(successCallback, errorCallback);
		}
		function generateCode(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/generatecode').then(successCallback, errorCallback);
		}
		function getCommodity(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/selectcommodity").then(successCallback, errorCallback);
		}

		function getClasification(successCallback, errorCallback) {
			var param = { Keyword: "COMPANY_SCALE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function isAllowedUploadItem(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PurchaseRequisition/isalloweduploaditem").then(successCallback, errorCallback);
		}

		function insertUploadExcel(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/PurchaseRequisition/insertuploadexcel", param).then(successCallback, errorCallback);
		}

		function deleteItemPR(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/PurchaseRequisition/deleteitempr", param).then(successCallback, errorCallback);
		}

		function getApprovalData(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/PurchaseRequisition/getApprovalData', model).then(successCallback, errorCallback);
		}

		function getDataExcel(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/PurchaseRequisition/selectitemprbypgr", param).then(successCallback, errorCallback);
		}

		function viewVendor(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/viewVendor', model).then(successCallback, errorCallback);
		}

		function selectDataPR(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/select', model).then(successCallback, errorCallback);
		}

		function getTypeTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_TYPE_RFQ" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getOptionsTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_OPTIONS_RFQ" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getDeliveryTerms(successCallback, errorCallback) {
			var param = { Keyword: "RFQ_DELIVERY_TERMS" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getIncoTerms(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/getIncoTerms").then(successCallback, errorCallback);
		}

		function getBidderMethod(successCallback, errorCallback) {
			var param = { Keyword: "BIDDER_METHOD" };
			GlobalConstantService.post(endpoint + "/reference/getbycodeOrdered", param).then(successCallback, errorCallback);
		}

		function getProcMethods(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/getProcMethods').then(successCallback, errorCallback);
		}

		function getEvalMethod(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/getEvaluationMethod').then(successCallback, errorCallback);
		}

		function getStateDelivery(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/PurchaseRequisition/GetFreighCostState').then(successCallback, errorCallback);
		}

		function getCityDelivery(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/getcitiesbystate', param).then(successCallback, errorCallback);
		}

		function getSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/RFQVHS/getStepsByMethod', param).then(successCallback, errorCallback);
		}

		function getAllVendors(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/getAllVendor', model).then(successCallback, errorCallback);
		}

		function insertPRRFQ(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/create', param).then(successCallback, errorCallback);
		}

		function selectPRRFQByID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/selectbyid', param).then(successCallback, errorCallback);
		}

		function updatePRRFQ(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rfqgoods/update', param).then(successCallback, errorCallback);
		}

		function getDataPROutstanding(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/PurchaseRequisition/selectitemprbypgr", param).then(successCallback, errorCallback);
		}

		function getDataItemPR(param, successCallback, errorCallback) {
			//param ID commodity
			GlobalConstantService.post(adminpoint + "/rfqgoods/availablepr", param).then(successCallback, errorCallback);
		}
		function getDataItemPRUpdate(param, successCallback, errorCallback) {
			//param ID commodity
			GlobalConstantService.post(adminpoint + "/rfqgoods/availableprUpdate", param).then(successCallback, errorCallback);
		}

		function selectMaterial(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/selectmaterial", param).then(successCallback, errorCallback);
		}

		function selectHarmonizedCodes(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/selectharmonizedcodes", param).then(successCallback, errorCallback);
		}

		function selectConcessions(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/selectconcessions", param).then(successCallback, errorCallback);
		}

		function selectPurchasingGroups(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/selectpurchasinggroups", param).then(successCallback, errorCallback);
		}

		function updateMaterial(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updatemtrl", param).then(successCallback, errorCallback);
		}

		function updateCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updatecommodity", param).then(successCallback, errorCallback);
		}

		function updateConcessionCategory(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updateconcessioncategory", param).then(successCallback, errorCallback);
		}

		function updateHarmonizedCode(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updateharmonizedcode", param).then(successCallback, errorCallback);
		}

		function updatePurchasingGroup(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updateitempurchasinggroup", param).then(successCallback, errorCallback);
		}

		function selectDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/selectdoc", param).then(successCallback, errorCallback);
		}

		function insertDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/createdoc", param).then(successCallback, errorCallback);
		}

		function updateDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/updatedoc", param).then(successCallback, errorCallback);
		}

		function deleteDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/deletedoc", param).then(successCallback, errorCallback);
		}

		function sendApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/sendforapproval", param).then(successCallback, errorCallback);
		}

		function Approval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/approve", param).then(successCallback, errorCallback);
		}

		function Reject(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/reject", param).then(successCallback, errorCallback);
		}

		function Publish(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/publish", param).then(successCallback, errorCallback);
		}
	}
})();