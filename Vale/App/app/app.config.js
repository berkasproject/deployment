﻿(function () {
	'use strict';

	angular.module('app').config(configuration);

	configuration.$inject = ['TitleProvider', 'KeepaliveProvider', 'IdleProvider', "$urlRouterProvider", "$ocLazyLoadProvider", "$translateProvider", '$translatePartialLoaderProvider'];

	function configuration(TitleProvider, KeepaliveProvider, IdleProvider, $urlRouterProvider, $ocLazyLoadProvider, $translateProvider, $translatePartialLoaderProvider) {

		var modulePath = "app/modules";
		var partialsPath = "app/partials";

		// ocLazyLoad configuration
		$ocLazyLoadProvider.config({
			debug: false,
			event: true
		});

		// ng-idle config
		IdleProvider.idle(1800);
		IdleProvider.timeout(5);
		KeepaliveProvider.interval(10);
		TitleProvider.enabled(false);

		// translation configuration
		$translatePartialLoaderProvider.addPart('main');
		$translateProvider.useLoader('$translatePartialLoader', {
		    urlTemplate: 'app/lang/{part}/locale-{lang}.json?cache=' + (new Date()).getTime()
		});
		$translateProvider.preferredLanguage(getCurrLang()); //untuk ganti bahasa
		$translateProvider.forceAsyncReload(true);
		$translateProvider.useSanitizeValueStrategy('sanitize');

		function getLoginState() {


			//return localStorage.getItem("login");
		}

		// state configuration
		//$urlRouterProvider.when('/rfqvhs', function ($location, $stateProvider) { redirectToLogin('/rfqvhs', $location, $stateProvider) });
		$urlRouterProvider.deferIntercept();
		$urlRouterProvider.otherwise('/home');

		function redirectToLogin(currUrl, $location, $stateProvider) {
			if (localStorage.getItem("sessEnd") == null || localStorage.getItem("sessEnd") == '') {
				localStorage.removeItem('eProcValeToken');
				localStorage.removeItem('eProcValeRefreshToken');
				localStorage.removeItem('roles');
				localStorage.removeItem('sessEnd');
				localStorage.removeItem('username');
				localStorage.removeItem('login');
				localStorage.removeItem('moduleLayer');
				//window.location.href = '/#/login';
				$location.path('/login' + currUrl);
			}

			var sessEnd = new Date(parseInt(localStorage.getItem("sessEnd")));
			if (new Date() < sessEnd) {
				$location.path(currUrl);
			} else {
				localStorage.removeItem('eProcValeToken');
				localStorage.removeItem('eProcValeRefreshToken');
				localStorage.removeItem('roles');
				localStorage.removeItem('sessEnd');
				localStorage.removeItem('username');
				localStorage.removeItem('login');
				localStorage.removeItem('moduleLayer');
				//window.location.href = '/#/login';
				$location.path('/login' + currUrl);
			}
		}

		function getCurrLang() {
			if (localStorage.getItem("currLang") == null || localStorage.getItem("currLang") == '')
				localStorage.setItem('currLang', 'id');

			return localStorage.getItem("currLang");
		}
	}
})();