﻿(function () {
	'use strict';

	angular.module("app").controller("evaluasiTeknisBarangPrintCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'EvaluasiTeknisBarangService', 'DataPengadaanService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$filter'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, EvaluasiTeknisBarangService,
        DataPengadaanService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $filter) {

	    var vm = this;
	    var loadmsg = "MESSAGE.LOADING";
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.evaluations = [];

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('evaluasi-teknis-barang');
		    UIControlService.loadLoading(loadmsg);
			loadData();
		}

		function loadData() {
		    UIControlService.loadLoading(loadmsg);
		    DataPengadaanService.GetStepByID({
		        ID: vm.StepID
		    }, function (reply) {
		        vm.tenderStepData = reply.data;
		        vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
		        EvaluasiTeknisBarangService.selectOfferEntries({
		            ID: vm.StepID,
		            TenderID: vm.tenderStepData.TenderID
		        }, function (reply) {
		            UIControlService.unloadLoading();
		            vm.evaluations = reply.data;
		            if (!vm.tenderStepData.tender.IsCancelled) {
		            	EvaluasiTeknisBarangService.isNeedTenderStepApproval({ ID: vm.StepID, TenderID: vm.tenderStepData.TenderID }, function (result) {
		            		vm.isNeedApproval = result.data;
		            	}, function (err) {
		            		$.growl.error({ message: "Gagal mendapatkan data Approval" });
		            		UIControlService.unloadLoading();
		            	});
		            	EvaluasiTeknisBarangService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
		            		vm.isApprovalSent = result.data;
		            	}, function (err) {
		            		$.growl.error({ message: "Gagal mendapatkan data Approval" });
		            		UIControlService.unloadLoading();
		            	});
		            }
		        }, function (error) {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
		        });
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
		    });
		};

		vm.detailApproval = detailApproval;
		function detailApproval() {
			$translatePartialLoader.addPart('data-contract-requisition');
			var item = vm.StepID;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/evaluasi-teknis-barang/detailApproval.html',
				controller: "GoodsTechEvalApprvCtrl",
				controllerAs: "GoodsTechEvalApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () { });
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPROVAL_CONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.SENDING');
					EvaluasiTeknisBarangService.sendToApproval({ ID: vm.StepID }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SEND_SUCC', "MESSAGE.SEND_SUCC_TITLE");
							EvaluasiTeknisBarangService.isNeedTenderStepApproval({ ID: vm.StepID, TenderID: vm.tenderStepData.TenderID }, function (result) {
								vm.isNeedApproval = result.data;
							}, function (err) {
								$.growl.error({ message: "Gagal mendapatkan data Approval" });
								UIControlService.unloadLoading();
							});
							EvaluasiTeknisBarangService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
								vm.isApprovalSent = result.data;
							}, function (err) {
								$.growl.error({ message: "Gagal mendapatkan data Approval" });
								UIControlService.unloadLoading();
							});
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.detailEvaluator = detailEvaluator;
		function detailEvaluator(dt) {
		    var item = {
		        VendorName: dt.VendorName,
                VendorID: dt.VendorID,
		        TenderStepDataID: vm.StepID,
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/evaluasi-teknis-barang/detailEvaluator.modal.html',
		        controller: 'detailEvaluatorCtrl',
		        controllerAs: 'dEvaluatorCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        loadContracts();
		    });
		};

		vm.dokumen = dokumen;
		function dokumen(dt) {
		    var item = {
		        VendorName: dt.VendorName,
		        GOEID: dt.ID
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/evaluasi-teknis-barang/detailDokumen.modal.html',
		        controller: 'detailDokumenPenawaranCtrl',
		        controllerAs: 'dDokumenCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {

		    });
		};

		vm.detail = detail;
		function detail(vendorID) {
            $state.transitionTo('detail-evaluasi-teknis-barang', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType, VendorID: vendorID });
		}

		vm.kembali = kembali;
		function kembali() {
		    $state.transitionTo('evaluasi-teknis-barang', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
		}

		vm.cetak = cetak;
		function cetak() {
		    html2canvas(document.getElementById('print'), {
		        onrendered: function (canvas) {
		            var data = canvas.toDataURL();
		            var docDefinition = {
		                content: [{
		                    image: data,
		                    width: 500
		                }]
		            };
		            pdfMake.createPdf(docDefinition).download(vm.tenderStepData.tender.TenderName+" (Hasil Evaluasi Teknis Barang).pdf");
		        }
		    });
		}

	}
})();

