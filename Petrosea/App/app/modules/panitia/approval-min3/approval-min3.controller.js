﻿(function () {
	'use strict';

	angular.module("app").controller("ApprovalMin3Ctrl", ctrl);

	ctrl.$inject = ['$filter', '$translatePartialLoader', 'ApprovalForMin3Service', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];

	function ctrl($filter, $translatePartialLoader, ApprovalForMin3Service, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {
		var vm = this;
		vm.init = init;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.textSearch = '';
		vm.columnApprovalStatus = '2'


		function init() {
			$translatePartialLoader.addPart('tender-step-approval');
			UIControlService.loadLoading("MESSAGE.LOADING");
			loadDataApproval(1);
		}

		vm.loadDataApproval = loadDataApproval;
		function loadDataApproval(current) {
			vm.dataApproval = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;

			if ($state.current.name === 'technical-evaluation-approval') {
				ApprovalForMin3Service.select({
					Keyword: vm.textSearch,
					column: 0,
					Offset: offset,
					Limit: vm.pageSize,
					Status: vm.columnApprovalStatus,
					Keyword4: 'technical'
				}, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.dataApproval = reply.data.List;
						vm.totalItems = reply.data.Count;
						vm.maxSize = vm.totalItems;
						//console.info("dataApproval:" + JSON.stringify(vm.dataApproval));
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			} else if ($state.current.name === 'price-evaluation-approval') {
				ApprovalForMin3Service.select({
					Keyword: vm.textSearch,
					column: 0,
					Offset: offset,
					Limit: vm.pageSize,
					Status: vm.columnApprovalStatus,
					Keyword4: 'price'
				}, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.dataApproval = reply.data.List;
						vm.totalItems = reply.data.Count;
						vm.maxSize = vm.totalItems;
						//console.info("dataApproval:" + JSON.stringify(vm.dataApproval));
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			} else {
				ApprovalForMin3Service.select({
					Keyword: vm.textSearch,
					column: 0,
					Offset: offset,
					Limit: vm.pageSize,
					Status: vm.columnApprovalStatus
				}, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.dataApproval = reply.data.List;
						vm.totalItems = reply.data.Count;
						vm.maxSize = vm.totalItems;
						//console.info("dataApproval:" + JSON.stringify(vm.dataApproval));
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			}
		}

		vm.GotoTenderInfo = GotoTenderInfo;
		function GotoTenderInfo(data) {
			$state.go('data-pengadaan-tahapan', { TenderRefID: data.TenderRefID, ProcPackType: data.ProcPackageType });
		}

		vm.changedFilter = changedFilter
		function changedFilter() {
			loadDataApproval(1);
		}

		vm.approve = approve;
		function approve(data) {
			//console.info("data:" + JSON.stringify(data));
			bootbox.confirm($filter('translate')('CONFIRM_APPROVE'), function (res) {
				if (res) {
					ApprovalForMin3Service.approve({
						Id: data.TenderStepDataId,
						ApprvType: data.ApprvType,
						ApprovalComment: "approve",
						ApproverLevelInfo: data.ApproverLevelInfo,
						IssuerId: data.IssuerId
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_APPROVE");
							loadDataApproval(vm.currentPage);
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.FAIL_APPROVE");
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.reject = reject;
		function reject(data) {
			console.info("data:" + JSON.stringify(data));
			ApprovalForMin3Service.reject({
				Id: data.TenderStepDataId,
				ApprvType: data.ApprvType,
				ApprovalComment: "reject"
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_REJECT");
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_REJECT");
				UIControlService.unloadLoading();
			});
		}

		vm.viewVendors = viewVendors;
		function viewVendors(list, flag) {
			var data = { item: list, act: flag };

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-min3/viewVendor.html',
				controller: 'viewVendorCtrl',
				controllerAs: 'viewVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//init();
				loadDataApproval(vm.currentPage);
			});
		}


		vm.approvalModal = approvalModal;
		function approvalModal(list, flag) {
			//console.info("flag:" + flag);
			//console.info("list:" + JSON.stringify(list));

			var data = { item: list, act: flag };

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-min3/approvalModal.html',
				controller: 'ApprovalMin3ModalCtrl',
				controllerAs: 'ApprovalMin3ModalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//init();
				loadDataApproval(vm.currentPage);
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType });
		}
	}
})();


