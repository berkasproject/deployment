(function () {
	'use strict';

	angular.module("app").controller("KPIVendorCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'KPIVendorService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, KPIVendorService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.newVendorCount = [];
		vm.modVendorCount = [];
		vm.arrNewVendor = [];

		function init() {
		    $translatePartialLoader.addPart('kpi-vendor');
		    getStartEndDate();
		    //grafikNewVendor();
		}

		function grafikNewVendor(param) {
		    vm.barang = []; vm.jasa = []; vm.bj = [];
		    for (var i = 0; i < 12; i++) {
		        //console.info("Goods ke-" + i + ": " + param[i].Goods);
		        vm.barang[i] = param[i].Goods;
		        vm.jasa[i] = param[i].Service;
		        vm.bj[i] = param[i].GoodsAndService;
		    }
		    vm.colours = ['#8FBC8F', '#DC143C', '#3498DB'];
		    vm.dataNewVendor = [
                vm.barang,vm.jasa,vm.bj
		    ]
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelNewVendor = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		        vm.datasetNewVendor = [{
		            label: "Barang"
		        }, { label: "Jasa" }, {label:"Barang dan Jasa"}];
		        vm.seriesNewVendor = ['Barang', 'Jasa', 'Barang dan Jasa'];

		        vm.optionNewVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: 1000,
		                        stepSize: 100
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Bulan'
		                    }
		                }]
		            }
		        };
		    }
		    else {
		        vm.labelNewVendor = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		        vm.datasetNewVendor = [{
		            label: "Goods"
		        }, { label: "Service" }, {label:"Goods and Service"}];
		        vm.seriesNewVendor = ['Goods', 'Service','Goods and Service'];

		        vm.optionNewVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Vendor Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: 1800,
		                        stepSize: 100
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Month'
		                    }
		                }]
		            }
		        };
		    }
		}

		function grafikModifyVendor(param) {
		    vm.barang = []; vm.jasa = []; vm.bj = [];
		    for (var i = 0; i < 12; i++) {
		        //console.info("Goods ke-" + i + ": " + param[i].Goods);
		        vm.barang[i] = param[i].Goods;
		        vm.jasa[i] = param[i].Service;
		        vm.bj[i] = param[i].GoodsAndService;
		    }
		    vm.modColor = ['#8FBC8F', '#DC143C', '#3498DB'];
		    vm.dataModVendor = [
                vm.barang, vm.jasa, vm.bj
		    ]
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelModVendor = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		        vm.datasetModVendor = [{
		            label: "Barang"
		        }, { label: "Jasa" }, {label:"Barang dan Jasa"}];
		        vm.seriesModVendor = ['Barang', 'Jasa', 'Barang dan Jasa'];

		        vm.optionModVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: 200,
		                        stepSize: 10
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Bulan'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelModVendor = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		        vm.datasetModVendor = [{
		            label: "Goods"
		        }, { label: "Service" }, {label:"Goods and Service"}];
		        vm.seriesModVendor = ['Goods', 'Service', 'Goods and Service'];

		        vm.optionModVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Vendor Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: 200,
		                        stepSize: 10
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Month'
		                    }
		                }]
		            }
		        };
		    }
		}

		function getDaysInMonth(m, y) {
		    return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
		}

		function getStartEndDate() {
		    var dateNow = new Date();
		    var yearNow = dateNow.getFullYear();

		    for (var i = 0; i < 12; i++) {
		        var lastDay = getDaysInMonth(i + 1, yearNow);

                //set startDate
		        var StartDate = new Date();
		        StartDate.setDate(1);
		        StartDate.setMonth(i);

		        //atur end date
		        var EndDate = new Date();
		        EndDate.setMonth(i);
		        EndDate.setDate(lastDay);
		        var bulan = i + 1;
		        getNewVendorCount(StartDate, EndDate, bulan);
		        getModifyVendorCount(StartDate, EndDate, bulan);
		    }
		    //console.info("array" + JSON.stringify(vm.newVendorCount));
		}

		function pushtoArray(param) {
		    //console.info("arrayBismillah" + JSON.stringify(param));
		    if (param.length == 12) {
		        var validArr = true;
		        for (var i = 0; i < param.length; i++) {
		            if (param[i] == null) {
		                var validArr = false;
		                i = param.length;
		            }
		        }
		        if (validArr == true) {
		            vm.arrayNewVendor = param;
		            grafikNewVendor(vm.arrayNewVendor);
		            //console.info("arrayBismillah" + JSON.stringify(vm.arrayNewVendor));
		        }
		    }
		}

		function getNewVendorCount(StartDate,EndDate,bulan) {
		    KPIVendorService.getNewVendorCount({
		        Date1: UIControlService.getStrDate(StartDate),
		        Date2: UIControlService.getStrDate(EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.newVendorCount[bulan-1] = reply.data;
		            vm.newVendorCount[bulan-1].bulan = "MONTH."+bulan;
		            pushtoArray(vm.newVendorCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETVENDOR");
		        UIControlService.unloadLoading();
		    });
		}


		function getModifyVendorCount(StartDate, EndDate, bulan) {
		    KPIVendorService.getModifyVendorCount({
		        Date1: UIControlService.getStrDate(StartDate),
		        Date2: UIControlService.getStrDate(EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.modVendorCount[bulan - 1] = reply.data;
		            vm.modVendorCount[bulan - 1].bulan = "MONTH." + bulan;
		            //console.info("mod" + JSON.stringify(vm.modVendorCount));
		            if (vm.modVendorCount.length == 12) {
		                var validArr = true;
		                for (var i = 0; i < vm.modVendorCount.length; i++) {
		                    if (vm.modVendorCount[i] == null) {
		                        var validArr = false;
		                        i = vm.modVendorCount.length;
		                    }
		                }
		                if (validArr == true) {
		                    vm.arrayModVendor = vm.modVendorCount;
		                    grafikModifyVendor(vm.arrayModVendor);
		                    console.info("arrayBismillah" + JSON.stringify(vm.arrayNewVendor));
		                }
		            }
		            //pushtoArray(vm.newVendorCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETMODIFY");
		        UIControlService.unloadLoading();
		    });
		}


		function getUsername() {
		    DashboardAdminService.getUsername({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.username = reply.data;
		            if (vm.username != 'admin') {
		                getEmpPos();
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETPOST");
		        UIControlService.unloadLoading();
		    });
		}
	}
})();

/*
.controller('dashboardCtrl', function( $scope, $rootScope, $state, $cookieStore, $http){ // alert("Tekan Tombol Refresh (F5)");
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    
    $scope.appSrcText = '';
    $scope.appPageSize = 10;
    $scope.appCurrPage = 1;
    $scope.approval = [];
    $scope.appCount = 0;

    $scope.init = function(){
        $rootScope.getSession().then(function(result){
            $rootScope.userSession = result.data.data;
            $rootScope.userLogin = $rootScope.userSession.session_data.username;
            $rootScope.authorize(bacaNotif());
            $scope.loadApproval(1);
        });
        //bacaNotif(); // AWN | old: $rootScope.readNotif();
    };   
    
    $scope.onSearchClick = function(appSrcText){
        $scope.appSrcText = appSrcText;
        $scope.loadApproval(1);
    };
    
    $scope.loadApproval = function(page){
        $rootScope.loadLoading('Silahkan Tunggu...');
        $scope.appCurrPage = page;
        $rootScope.authorize(
            $http.post($rootScope.url_api + 'approval/select/byuser', {
                pegawai_id: $rootScope.userSession.session_data.pegawai_id,
                offset: (page - 1) * $scope.appPageSize,
                limit: $scope.appPageSize,
                search: $scope.appSrcText
            }).success(function(reply) {
                if (reply.status === 200) {
                    $scope.approval = reply.result.data.result;
                    $scope.approval.forEach(function(a){
                        a.tgl_mulai = a.tgl_mulai ? $rootScope.convertTanggalWaktu(a.tgl_mulai) : '';
                        a.tgl_selesai = a.tgl_selesai ? $rootScope.convertTanggalWaktu(a.tgl_selesai) : '';
                    });
                    $scope.appCount = reply.result.data.count;
                }
                $rootScope.unloadLoading();
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                $rootScope.unloadLoading();
                return;
            })
        );
    };
    
    $scope.onMenujuAppClick = function(app){
        $state.transitionTo('approval-master', {
            flowpaket_id: app.flow_paket_id,
            paket_lelang_id: app.paket_id
        });
    };
    
    function bacaNotif(){ 
        
//        eb.send( auth, {sessionID: sess}, function( authReply ){ // AWN-Auth-Step4
//            if( authReply.status === 'ok' ){
//                $rootScope.userlogged = authReply.username; // AWN-Auth-Step5
//                
//                $rootScope.readNotif(); // AWN
//                
//            } else { // AWN-Auth-Step6
//                $rootScope.isLogged = false;
//                $rootScope.userLogged = "";
//                $state.transitionTo('login');                                                
//            }
//        }); // end: AWN-Auth-Step7            
    } // end bacaNotif     
});
*/