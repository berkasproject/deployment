(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalDeliveryDateCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'EvaluasiPenawaranBarangService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, EvaluasiPenawaranBarangService) {

        var vm = this;
        vm.ID = item.TenderStepDataID;
        vm.isAllowedEdit = item.isAllowedEdit;
        vm.vendorName = item.vendorName;
        var vendorId = item.vendorId;

        vm.init = init;
        function init() {
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("");
            EvaluasiPenawaranBarangService.getCurrentDeliveryDateApproval({
                TenderStepDataID: vm.ID,
                VendorID: vendorId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.approval = reply.data;
                vm.allowSendApprove = !vm.approval || vm.approval.ApprovalStatus === false;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DD_APPROVAL'));
            });
        }

        vm.sendApproval = sendApproval;
        function sendApproval() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_DELIVERY_DATE_APPROVAL'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    EvaluasiPenawaranBarangService.insertDeliveryDateApproval({
                        TenderStepDataID: vm.ID,
                        VendorID: vendorId
                }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SEND_APPROVAL'));
                        $uibModalInstance.close();
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_APPROVAL'));
                    });
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();