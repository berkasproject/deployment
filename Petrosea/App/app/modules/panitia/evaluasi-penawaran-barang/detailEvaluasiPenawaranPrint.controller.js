﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailEvaluasiPenawaranBarangPrintController", ctrl);

    ctrl.$inject = ['$state', 'Excel', '$timeout', '$uibModalInstance', '$filter', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranBarangService', 'DataPengadaanService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, Excel, $timeout, $uibModalInstance, $filter, item, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranBarangService, DataPengadaanService, UIControlService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.stepID = item.StepID;
        vm.tenderRefID = item.TenderRefID;
        vm.procPackType = item.ProcPackType;

        vm.tenderStepData = item.tenderStepData;
        vm.itemPRs = [];
        vm.offerEntries = item.offerEntries;
        vm.evaluation = item.evaluation;
        vm.evaluationVendors = item.evaluationVendors;
        vm.scoreDetails = item.scoreDetails;
        //vm.repeater = item.repeater;
        vm.otherCosts = item.otherCosts;
        vm.criterias = item.criterias;
        vm.epvOtherCosts = item.epvOtherCosts;
        vm.totalHistoricalValue = item.totalHistoricalValue;
        vm.totalHistoricalValueInUSD = item.totalHistoricalValueInUSD;
        vm.totalEPValue = item.totalEPValue;
        vm.totalEPValueInUSD = item.totalEPValueInUSD;
        vm.epvCurrencySymbol = item.epvCurrencySymbol;
        vm.epvRateToUSD = item.epvRateToUSD;

        var paymentTermsOptions = item.paymentTermsOptions;

        vm.init = init;
        function init() {

            var length = vm.offerEntries.length;
            vm.repeater = [];
            for (var i = 0; i < length * 4; i++) {
                vm.repeater.push(i);
            }

            loadData();
        };

        function loadData() {
            vm.offerEntries.forEach(function (entry) {
                for (var i = 0; i < paymentTermsOptions.length; i++) {
                    var paymentTerm = paymentTermsOptions[i];
                    if (paymentTerm.Id === entry.PaymentTerms) {
                        entry.PaymentTermText = paymentTerm.Code + ' - ' + paymentTerm.Name;
                        break;
                    }
                }
            });

            loadOEDetail();
        };

        vm.loadOEDetail = loadOEDetail;
        function loadOEDetail() {
            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiPenawaranBarangService.getItemPRs({
                ID: vm.tenderStepData.TenderID
            }, function (reply) {
                vm.itemPRs = reply.data;
                var count = vm.itemPRs.length;

                var loadCount = vm.offerEntries.length;
                vm.offerEntries.forEach(function (entry) {
                    EvaluasiPenawaranBarangService.getPagedOEDetail({
                        Parameter: entry.ID,
                        Keyword: "",
                        column: 0,
                        Offset: 0,
                        Limit: count
                    }, function (reply) {
                        loadCount--;
                        if (loadCount === 0) {
                            UIControlService.unloadLoadingModal();
                        }
                        var oeDetail = reply.data;
                        entry.GoodsOfferEntryDetails = oeDetail;
                        entry.GoodsOfferEntryDetails.forEach(function (det) {
                            det.TotalPriceInUSD = det.TotalPrice * entry.ExchangeRateToUSD;
                            det.TotalPriceInUSD = det.TotalPriceInUSD.toFixed(2);
                        });
                    }, function (error) {
                        loadCount--;
                        if (loadCount === 0) {
                            UIControlService.unloadLoadingModal();
                        }
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OFFERS');
                    });
                });
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_ITEM_PRS');
            });
        }

        vm.print = print;
        function print() {
            vm.exportHref = Excel.tableToExcel('#tableToExport', 'sheet1');
            $timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();