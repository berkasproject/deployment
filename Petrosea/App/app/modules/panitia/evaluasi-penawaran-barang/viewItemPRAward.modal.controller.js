(function () {
    'use strict';

    angular.module("app")
    .controller("viewItemPRAwardCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranBarangService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranBarangService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.itemPRs = [];
        vm.tenderName = item.tenderName;
        vm.vendorName = item.evaluationVendor.VendorName;

        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.count = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi-barang');
            loadItem();
        };       

        vm.loadItem = loadItem;
        function loadItem(){
            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiPenawaranBarangService.getAwardedItemPR({
                column: item.rfqGoodsID, //RFQGoodsId
                Status: item.evaluationVendor.VendorID, //VendorID
                Offset: (vm.pageNumber - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.itemPRs = reply.data.List;
                vm.count = reply.data.Count;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ITEM_PRS'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();