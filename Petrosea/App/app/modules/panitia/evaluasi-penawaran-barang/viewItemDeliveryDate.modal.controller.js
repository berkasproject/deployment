(function () {
    'use strict';

    angular.module("app")
    .controller("viewItemDeliveryDayeController", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranBarangService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranBarangService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.itemPRs = [];
        vm.tenderName = item.tenderName;
        vm.vendorName = item.vendorName;
        
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.count = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi-barang');
            loadItem();
        };       

        vm.loadItem = loadItem;
        function loadItem(){
            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiPenawaranBarangService.getItemDeliveryDate({
                IntParam1: item.tenderID, //TenderId
                IntParam2: item.vendorID, //VendorID
                Status: item.awardedOnly ? 1 : 0,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.itemPRs = reply.data.List;
                vm.count = reply.data.Count;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ITEM_PRS'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();