﻿(function () {
	'use strict';

	angular.module("app").controller("AccessLogCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'AccessLogService', '$stateParams', '$state', '$translate', '$translatePartialLoader'];
	function ctrl(UIControlService, AccessLogService, $stateParams, $state, $translate, $translatePartialLoader) {
		var vm = this;
		//vm.currentPage = Number($stateParams.page);
		vm.totalItems = 0;
		vm.maxSize = 10;
		vm.currPage = 1;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('login-rekanan');
			vm.listFilter = [
				{ Value: 1, Name: "SELECT.ACTIVITY_NAME" },
				{ Value: 2, Name: "SELECT.CREATED_BY" }];

			vm.filterBy = Number($stateParams.searchBy);
			vm.srchText = $stateParams.keyword;

			if (vm.filterBy === 0 || vm.filterBy === undefined) {
				vm.filterBy = 1;
			}

			jLoad();
		}

		vm.jLoad = jLoad;
		function jLoad() {
			UIControlService.loadLoading("Querying logs...");
			var offset = (vm.currPage * 10) - 10;
			AccessLogService.Select({
				Offset: offset,
				Limit: vm.maxSize,
				Keyword: vm.srchText,
				Status: 1,
				FilterType: vm.filterBy
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.logs = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Log Akses" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		// 見せる
		vm.show = show;
		function show() {
			$state.transitionTo('vendor-access-log', {
				searchBy: vm.filterBy,
				keyword: vm.srchText
			});
		}

		vm.gotoPage = gotoPage;
		function gotoPage(page) {
			$state.transitionTo('vendor-access-log', {
				searchBy: vm.filterBy,
				keyword: vm.srchText
			});
		}
	}

	angular.module("app").controller("AccessLogAdminCtrl", ctrlAdmin);

	ctrlAdmin.$inject = ['UIControlService', 'AccessLogService', '$stateParams', '$state', '$translate', '$translatePartialLoader'];
	function ctrlAdmin(UIControlService, AccessLogService, $stateParams, $state, $translate, $translatePartialLoader) {
		var vm = this;
		//vm.currentPage = Number($stateParams.page);
		vm.totalItems = 0;
		vm.maxSize = 10;
		vm.currPage = 1;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('login-rekanan');
			vm.listFilter = [
				{ Value: 1, Name: "SELECT.ACTIVITY_NAME" },
				{ Value: 2, Name: "SELECT.CREATED_BY" }];

			vm.filterBy = Number($stateParams.searchBy);
			vm.srchText = $stateParams.keyword;

			if (vm.filterBy === 0 || vm.filterBy === undefined) {
				vm.filterBy = 1;
			}

			jLoad();
		}

		vm.jLoad = jLoad;
		function jLoad() {
			UIControlService.loadLoading("Querying logs...");
			var offset = (vm.currPage * 10) - 10;
			AccessLogService.Select({
				Offset: offset,
				Limit: vm.maxSize,
				Keyword: vm.srchText,
				Status: 2,
				FilterType: vm.filterBy
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.logs = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Log Akses" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//番組
		vm.show = show;
		function show() {
			$state.transitionTo('admin-access-log', {
				searchBy: vm.filterBy,
				keyword: vm.srchText
			});
		}

		vm.gotoPage = gotoPage;
		function gotoPage(page) {
			$state.transitionTo('admin-access-log', {
				searchBy: vm.filterBy,
				keyword: vm.srchText
			});
		}
	}
})();