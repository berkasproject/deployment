(function () {
	'use strict';

	angular.module("app").controller("KPIContractCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$filter', '$translatePartialLoader', '$location', '$state', 'KPIContractService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $filter, $translatePartialLoader, $location, $state, KPIContractService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.arrKPI = [];
		vm.arrOntime = [];
		vm.arrSavingCost = [];

		vm.jenistender = null;
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.currentPageCP = 1;
		vm.currentPageCS = 1;

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    $translatePartialLoader.addPart('kpi-contract');
		    konfigurasiWaktu();
		    tenderStepCount();
		    dataContractPerformance(1);
		    dataCostSaving(1);
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		vm.konfigurasiWaktu = konfigurasiWaktu;
		function konfigurasiWaktu() {
		    vm.datenow = new Date();
		    vm.getYearNow = vm.datenow.getFullYear();
		    var mindateNow = new Date("January 01, " + vm.getYearNow + " 00:00:00");
		    var maxdateNow = new Date("December 31, " + vm.getYearNow + " 23:59:59");
		    //mindateNow.setYear(vm.getYearNow - 1);
		    vm.datepickeroptions = {
		        minMode: 'month',
		        maxDate: maxdateNow,
		        minDate: mindateNow
		    }
		    vm.datepickeroptionsTahun = {
		        minMode: 'year'
		    }
		    vm.ddTender =
            [
                { Value: 0, Name: "Normal Tender" },
                { Value: 1, Name: "Direct Award" }

            ]
		    if (localStorage.getItem("currLang") === 'id' || localStorage.getItem("currLang") === 'ID') {
			    vm.ddDurasi =
	            [
	                { Value: 0, Name: "Minggu" },
	                { Value: 1, Name: "Bulan" }

	            ]
        	}
		    else if (localStorage.getItem("currLang") === 'en' || localStorage.getItem("currLang") === 'EN') {
        		vm.ddDurasi =
	            [
	                { Value: 0, Name: "Week" },
	                { Value: 1, Name: "Month" }

	            ]
	        }
		    vm.jenistender = vm.ddTender[0];
		    vm.durasi = vm.ddDurasi[0];
		    vm.filterTahun = vm.datenow;
		    vm.tahun = vm.getYearNow;
		    weekByYear();
		}

		vm.filter = filter;
		function filter() {
		    var invalidFilter = false;
		    if (vm.jenistender == null || vm.durasi == null) {
		        invalidFilter = true;
		    }
		    else {
		        if (vm.durasi.Value == 0) { //minggu
		            if (vm.ma == null || vm.mb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ma.NumberOfWeek > vm.mb.NumberOfWeek) {
		                    invalidFilter = true;
		                }
		                else {
		                    vm.batasAtas = vm.ma.NumberOfWeek;
		                    vm.batasBawah = vm.mb.NumberOfWeek;
		                }
		            }
		        }
		        else if (vm.durasi.Value == 1) { //bulan         
		            if (vm.ba == null || vm.bb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ba > vm.bb) {
		                    invalidFilter = true;
		                }
		                else {
		                    var intBatasAtas = vm.ba.getMonth();
		                    var intBatasBawah = vm.bb.getMonth();
		                    vm.batasAtas = intBatasAtas + 1;
		                    vm.batasBawah = intBatasBawah + 1;
		                }
		            }
		            
		        }
		    }
		    if (invalidFilter == true) {
		        UIControlService.msg_growl("warning", "MESSAGE.FILTER_NOTVALID");
		        return;
		    }
		    else if (invalidFilter == false) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        dataKPI(1);
		        dataCostSaving(1);
		    }
		}

		vm.cekFilterTahun = cekFilterTahun;
		function cekFilterTahun() {
		    vm.tahun = vm.filterTahun.getFullYear();
		    if (vm.durasi.Value == 0) {
		        weekByYear();
		    }
		    else if (vm.durasi.Value == 1) {
		        var mindateNow = new Date("January 01, " + vm.tahun + " 00:00:00");
		        var maxdateNow = new Date("December 31, " + vm.tahun + " 23:59:59");
		        vm.datepickeroptions = {
		            minMode: 'month',
		            maxDate: maxdateNow,
		            minDate: mindateNow
		        }
		        vm.ba = mindateNow;
		        vm.bb = maxdateNow;
		    }
		}

		function grafikOntime(durasi,label,arr) {
		    vm.dataOntime = [arr];
		    var maxvalue = Math.max.apply(Math, arr);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(0);
		    }
		    else {
		        maxvalue = 10;
		    }
		    vm.colours = [
            {
                backgroundColor: '#000000',
                borderColor: '#b3e7fb',
                hoverBackgroundColor: '#b3e7fb',
                hoverBorderColor: '#b3e7fb'
            }
		    ];
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelOntime = label;
		        vm.datasetOntime = [{
		            label: "Ontime Contract"
		        }];

		        vm.optionOntime = {
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Contract'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelOntime = label;
		        vm.datasetOntime = [{
		            label: "Ontime Contract"
		        }];

		        vm.optionOntime = {
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Contract Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Month'
		                    }
		                }]
		            }
		        };
		    }
		}


		function grafikCostSaving(durasi, label, arr) {
		    vm.dataSC = [arr];
		    var maxvalue = Math.max.apply(Math, arr);
		    console.info("cs"+maxvalue);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(0);
		    }
		    else {
		        maxvalue = 10;
		    }
		    //console.info("step" + step);
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelSC = label;
		        vm.datasetSC = [{
		            label: "Cost Saving"
		        }];

		        vm.optionSC = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Cost Saving (%)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize:step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelSC = label;
		        vm.datasetSC = [{
		            label: "Cost Saving"
		        }];

		        vm.optionSC = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Cost Saving (%)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    }
		}

		function getDaysInMonth(m, y) {
		    return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
		}


		vm.dataKPI = dataKPI;
		function dataKPI(current) {
		    vm.arrKPI = [];
		    vm.currentPage = current;
		    var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    KPIContractService.dataKPI({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Offset: offset,
		        Limit: vm.maxSize,
		        FilterType: vm.jenistender.Value,
                Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.arrKPI = reply.data.List;
		            //console.info("arr" + JSON.stringify(vm.arrKPI));
		            vm.totalItems = Number(reply.data.Count);
		            dataGrafik(vm.batasAtas, vm.batasBawah, vm.totalItems, vm.durasi.Value);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.exportContract = exportContract;
		function exportContract() {
		    //console.info("proctype:" + vm.procType.RefID);
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    KPIContractService.exportContract({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        FilterType: vm.jenistender.Value,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            var list = reply.data;
		            console.info("list" + JSON.stringify(list.length));
		            vm.filterdata = [];
		            

		            for (var i = 0; i <= list.length - 1; i++) {
		                UIControlService.loadLoading("MESSAGE.LOADING");
		                var forExcel = {
		                    TenderCode: list[i].TenderCode,
		                    TenderName: list[i].TenderName,
                            ProjectTitle:list[i].ProjectTitle,
                            ContractDetail: list[i].ContractDetailsInfo,
		                    ContractStartDate: UIControlService.getStrDate(list[i].ContractStartDate),
		                    ContractEndDate: UIControlService.getStrDate(list[i].ContractEndDate),
		                    OEPrice:list[i].OEPriceToUSD,
		                    FinalValue: list[i].FinalValueToUSD
		                }
		                vm.filterdata.push(forExcel);
		            }
		            JSONToCSVConvertor(vm.filterdata, true, "KPI Contract Service");
                    


		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });


		}


		vm.dataCostSaving = dataCostSaving;
		function dataCostSaving(current) {
		    vm.dataCS = [];
		    vm.currentPageCS = current;
		    var offset = (vm.currentPageCS * vm.maxSize) - vm.maxSize;
		    KPIContractService.dataCostSaving({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        FilterType: vm.jenistender.Value,
		        Offset: offset,
		        Limit: vm.maxSize,
                Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.dataCS = reply.data.List;
		            vm.totalItemsCS = Number(reply.data.Count);
		            //console.info("dataCS" + JSON.stringify(vm.dataCS));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.dataContractPerformance = dataContractPerformance;
		function dataContractPerformance(current) {
		    vm.dataCP = [];
		    vm.currentPageCP = current;
		    var offset = (vm.currentPageCP * vm.maxSize) - vm.maxSize;
		    KPIContractService.dataContractPerformance({
		        Offset: offset,
		        Limit: vm.maxSize
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.dataCP = reply.data.List;
		            vm.totalItemsCP = Number(reply.data.Count);
		            //console.info("dataCP" + JSON.stringify(vm.dataCP));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.dataGrafik = dataGrafik;
		function dataGrafik(atas,bawah,totalItems,tipeDurasi) {
		    //console.info("atas" + atas);
		    //console.info("bawah" + bawah);
		    //console.info("totalItems" + totalItems);
		    KPIContractService.dataKPI({
		        column: vm.tahun,
		        IntParam1: atas,
		        IntParam2: bawah,
		        Offset: 0,
		        Limit: totalItems,
		        FilterType: vm.jenistender.Value,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.dataGrafikKPI = reply.data.List;
		            //console.info("arr" + JSON.stringify(vm.dataGrafikKPI));
		            vm.totalItems = Number(reply.data.Count);
		            vm.arrOntime = [];
		            vm.arrCostSaving = [];
		            vm.label = [];
		            for (var i = 0; i <= vm.dataGrafikKPI.length - 1; i++) {
		                vm.arrOntime[i] = vm.dataGrafikKPI[i].OntimeContract;
		                vm.arrCostSaving[i] = vm.dataGrafikKPI[i].CostSaving;
		                vm.label[i] = atas+i;
		            }
		            if (tipeDurasi == 0) {
		                var durasi = 'Week';
		            }
		            else {
		                var durasi = 'Month';
		            }
		            //console.info("arr" + JSON.stringify(vm.arrCostSaving));
		            grafikOntime(durasi, vm.label, vm.arrOntime);
		            grafikCostSaving(durasi, vm.label, vm.arrCostSaving);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.cekFilterDurasi = cekFilterDurasi;
		function cekFilterDurasi() {
		    var durasivalid = true;
		    if (vm.durasi.Value == 0) { //minggu
		        if (vm.ma.NumberOfWeek > vm.mb.NumberOfWeek) {
		            durasivalid = false;
		        }
		    }
		    else if (vm.durasi.Value == 1) { //bulan
                
		    }
		    if (durasivalid == false) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_FILTER1");
		        vm.mb = null;
		        return;
		    }
		}

		function weekByYear() {
		    KPIContractService.weekByYear({
                column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.weeks = reply.data;
		            //console.info("weeks" + JSON.stringify(vm.weeks));
		            for (var i = 0; i <= vm.weeks.length-1; i++) {
		                vm.weeks[i].label = "Week #" + vm.weeks[i].NumberOfWeek + " (" + UIControlService.convertDate(vm.weeks[i].StartDate) + " s/d " + UIControlService.convertDate(vm.weeks[i].EndDate) + ")";
		            }
		            vm.ma = vm.weeks[0];
		            vm.mb = vm.weeks[vm.weeks.length - 1];
		            vm.batasAtas = vm.ma.NumberOfWeek;
		            vm.batasBawah = vm.mb.NumberOfWeek;
		            dataKPI(1);
                    dataCostSaving(1);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAPOST");
		        UIControlService.unloadLoading();
		    });
		}

		function tenderStepCount() {
		    KPIContractService.tenderStepCount({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.tenderstepcount = reply.data;
		            console.info("tsc" + JSON.stringify(vm.tenderstepcount));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAPOST");
		        UIControlService.unloadLoading();
		    });
		}


		vm.exportExcel = exportExcel;
		function exportExcel() {
		    //console.info("proctype:" + vm.procType.RefID);
		    KPIContractService.dataKPI({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Offset: 0,
		        Limit: vm.totalItems,
		        FilterType: vm.jenistender.Value,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            var data = reply.data;
		            vm.alldata = data.List;
		            //console.info("datasampel:" + JSON.stringify(vm.alldata[1]));
		            vm.totalItemAlldata = Number(data.Count);
		            vm.filterdata = [];
		            //console.info("total semua data:" + JSON.stringify(vm.alldata.length));
		            for (var i = 0; i < vm.totalItemAlldata; i++) {
		                var forExcel = {
		                    Year: vm.alldata[i].Year,
		                    Durration: vm.alldata[i].Durration,
		                    ApprovedTender: vm.alldata[i].ApprovedTender,
		                    CreatedContract: vm.alldata[i].CreatedContract,
		                    OntimeContract: vm.alldata[i].OntimeContract,
		                    Currency: "USD",
		                    FinalContractValue: vm.alldata[i].FinalContractValue,
		                    BudgetedValue: vm.alldata[i].BudgetedContractValue,
		                    Local: vm.alldata[i].ContractLocal,
		                    National: vm.alldata[i].ContractNational,
		                    International: vm.alldata[i].ContractInternational
		                }
		                vm.filterdata.push(forExcel);
		            }
		            //console.info("dataForExcel:" + JSON.stringify(vm.filterdata));
		            JSONToCSVConvertor(vm.filterdata, true, "KPI Contract Service");
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });


		}

		vm.exportExcelDataCostSaving = exportExcelDataCostSaving;
		function exportExcelDataCostSaving() {
		    UIControlService.loadLoading("");
		    KPIContractService.dataCostSaving({
		        Offset: 0,
		        Limit: vm.totalItemsCS,
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        FilterType: vm.jenistender.Value,
                Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            var dataCS = reply.data.List;
		            var toExport = [];
		            var index = 1;

		            dataCS.forEach(function (cs) {
		                toExport.push({
                            No : index++,
                            Tender_Code: cs.TenderCode,
                            Tender_Name: cs.ContractDetailsInfo,
                            Tender_Type: cs.TenderType,
                            Requested_Date: cs.RequestedDate ? $filter("date")(cs.RequestedDate, 'dd/MM/yyyy') : '',
                            Awarded_Date: cs.EECCompletedDate ? $filter("date")(cs.EECCompletedDate, 'dd/MM/yyyy') : '',
                            Contract_No: cs.ContractNo,
                            Vendor_Name: cs.VendorName,
                            Vendor_Area: $filter("translate")(cs.VendorArea),
                            Start_Date: cs.DurrationStartDate ? $filter("date")(cs.DurrationStartDate, 'dd/MM/yyyy') : '',
                            Finish_Date: cs.DurrationFinishDate ? $filter("date")(cs.DurrationFinishDate, 'dd/MM/yyyy') : '',
                            Budget_Request_USD: $filter("currency")(cs.BudgetedContractValueToUSD,''),
                            Vendor_Offer_Entry_Price_USD: $filter("currency")(cs.OEPriceToUSD, ''),
                            Final_Price_USD: $filter("currency")(cs.FinalValueToUSD, ''),
                            Budget_Saving_USD: $filter("currency")(cs.BudgetSavingToUSD, ''),
                            Cost_Saving_USD: $filter("currency")(cs.ProcurementSavingToUSD, ''),
                            Contract_Engineer: cs.CEName,
		                });
		            });
		            JSONToCSVConvertor(toExport, true, "KPI Cost Savings");
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.exportExcelDataIndividualCostSaving = exportExcelDataIndividualCostSaving;
		function exportExcelDataIndividualCostSaving() {
		    UIControlService.loadLoading("");
		    KPIContractService.dataIndividualCostSaving({
		        Offset: -1,
		        Limit: -1,
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        FilterType: vm.jenistender.Value,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            var dataCS = reply.data.List;
		            var toExport = [];
		            var index = 1;

		            dataCS.forEach(function (cs) {
		                toExport.push({
		                    No: index++,
		                    Tender_Type: cs.TenderType,
		                    SAP_No: cs.ContractNo,
		                    Tender_Code: cs.TenderCode,
		                    Tender_Name: cs.ContractDetailsInfo,
		                    Contractor: cs.VendorName,
		                    Budget_Request_USD: $filter("currency")(cs.BudgetedContractValueToUSD, ''),
		                    Historical_Price_USD: $filter("currency")(cs.HistoricalValueToUSD, ''),
		                    EPV_USD: $filter("currency")(cs.EPVValueToUSD, ''),
		                    Historical_Prixe_USD: $filter("currency")(cs.HistoricalValueToUSD, ''),
		                    Initial_Proposal_USD: $filter("currency")(cs.OEPriceToUSD, ''),
		                    Negotiation_1_USD: $filter("currency")(cs.Negotiation1ToUSD, ''),
		                    Negotiation_2_USD: $filter("currency")(cs.Negotiation2ToUSD, ''),
		                    Negotiation_3_USD: $filter("currency")(cs.Negotiation3ToUSD, ''),
		                    Awarded_USD: $filter("currency")(cs.FinalValueToUSD, ''),
		                    Contract_Issued: cs.EECCompletedDate ? $filter("date")(cs.EECCompletedDate, 'dd/MM/yyyy') : '',
		                    Contract_Start_Date: cs.ContractStartDate ? $filter("date")(cs.ContractStartDate, 'dd/MM/yyyy') : '',
		                    Contract_End_Date: cs.ContractEndDate ? $filter("date")(cs.ContractEndDate, 'dd/MM/yyyy') : '',
		                    Duration_Days: cs.ContractDurrationDays,
		                    Duration_Years: cs.ContractDurrationYear,
		                    Cost_Reduction_USD: $filter("currency")(cs.CostReduction, ''),
		                    Cost_Avoidance_USD: $filter("currency")(cs.CostAvoidance, ''),
		                    Cost_Saving_USD: $filter("currency")(cs.CostSaving, ''),
		                    Annualized_Saving_USD: $filter("currency")(cs.AnnualizedSaving, ''),
		                    Contract_Engineer: cs.CEName,
		                });
		            });
		            JSONToCSVConvertor(toExport, true, "KPI Cost Savings");
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.exportExcelDataKPI = exportExcelDataKPI;
		function exportExcelDataKPI() {
		    UIControlService.loadLoading("");
		    KPIContractService.dataKPI({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Offset: 0,
		        Limit: vm.totalItems,
		        FilterType: vm.jenistender.Value,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            var datalist = reply.data.List;
		            var toExport = [];
		            var index = 1;

		            datalist.forEach(function (data) {
		                toExport.push({
		                    No: index++,
		                    Tahun: data.Year,
		                    Durasi: data.Durration,
		                    Approved_Tender: data.ApprovedTender,
		                    Created_Contract: data.CreatedContract,
		                    Ontime_Contract: data.OntimeContract + ' (' + data.PrecentageOntimeContract + '%)',
		                    Outstanding_Contract_Request: data.OutstandingContractReq,
		                    Contract_Productivity: data.ContractProductivity,
		                    Final_Contract_Value: $filter("currency")(data.FinalContractValue, '$'),
		                    Budgeted_Value: $filter("currency")(data.BudgetedContractValue, '$'),
		                    Contract_Value_Local: $filter("currency")(data.ContractLocal, '$'),
		                    Contract_Value_National: $filter("currency")(data.ContractNational, '$'),
		                    Contract_Value_International: $filter("currency")(data.ContractInternational, '$')
		                });
		            });
		            JSONToCSVConvertor(toExport, true, "Key Performance Indicator");
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAMODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ShowLabel, fileName) {
		    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		    var arrData = JSONData;
		    //console.info(arrData[0]);
		    var CSV = '';

		    //This condition will generate the Label/Header
		    if (ShowLabel) {
		        var row = "sep=," + '\n';

		        //This loop will extract the label from 1st index of on array
		        for (var index in arrData[0]) {

		            //Now convert each value to string and comma-seprated
		            row += index + ',';
		        }

		        row = row.slice(0, -1);
		        //console.info(row);
		        //append Label row with line break
		        CSV += row + '\r\n';
		        //console.info(CSV);
		    }

		    //1st loop is to extract each row
		    for (var i = 0; i < arrData.length; i++) {
		        var row = "";

		        //2nd loop will extract each column and convert it in string comma-seprated
		        for (var index in arrData[i]) {
		            row += '"' + arrData[i][index] + '",';
		        }

		        row.slice(0, row.length - 1);

		        //add a line break after each row
		        CSV += row + '\r\n';
		    }

		    if (CSV == '') {
		        alert("Invalid data");
		        return;
		    }

		    //Generate a file name
		    //var fileName = "KPI Contract Service";
		    var fileName = fileName;

		    //Initialize file format you want csv or xls
		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

		    // Now the little tricky part.
		    // you can use either>> window.open(uri);
		    // but this will not work in some browsers
		    // or you will not get the correct file extension    

		    //this trick will generate a temp <a /> tag
		    var link = document.createElement("a");
		    link.href = uri;

		    //set the visibility hidden so it will not effect on your web-layout
		    link.style = "visibility:hidden";
		    link.download = fileName + ".csv";

		    //this part will append the anchor tag and remove it after automatic click
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
		}


		function getUsername() {
		    DashboardAdminService.getUsername({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.username = reply.data;
		            if (vm.username != 'admin') {
		                getEmpPos();
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATAPOST");
		        UIControlService.unloadLoading();
		    });
		}
	}
})();
