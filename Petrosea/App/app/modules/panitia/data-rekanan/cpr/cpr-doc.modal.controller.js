﻿(function () {
    'use strict';

    angular.module("app")
    .controller("cprDocsModalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', 'item', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'CPRService', 'VPCPRDataService', 'VPEvaluationMthodService', 'UIControlService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, item, $uibModalInstance, $translate, $translatePartialLoader, $location,  CPRService, VPCPRDataService, VPEvaluationMthodService, UIControlService, UploaderService, UploadFileConfigService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        
        vm.doc = item.doc;
        vm.fileUpload;       
        vm.CPRDataId = vm.doc.VPCPRDataId
        vm.init = init;
        function init() {

            $translatePartialLoader.addPart('cpr');
            UploadFileConfigService.getByPageName("PAGE.ADMIN.CPR", function (response) {
                UIControlService.unloadLoadingModal();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal(); l
                return;
            });
        };

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        vm.save = save;
        function save() {
            if (!vm.doc.DocName) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NODOCNAME");
                return;
            }
            if (!vm.doc.DocUrl && !vm.fileUpload) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return;
            }

            if (vm.fileUpload) {
                uploadFile();
            } else {
                saveDocPart();
            }
        }

        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }

            return true;
        }

        function upload(file, config, types) {

            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            vm.Id = vm.CPRDataId;
            
            UploaderService.uploadSingleCPR(vm.Id,"", file, size, types,
            function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    var url = reply.data.Url;
                    var size = reply.data.FileLength;
                    vm.doc.DocUrl = url;                                        
                    
                    saveDocPart();
                   // $uibModalInstance.close(); // temporary fo test, remove then
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        }

        vm.saveDocPart = saveDocPart;
        function saveDocPart() {
            if (vm.doc.VPCPRDataId > 0) {
                //with cprdataId        
                console.log("id vpcpr:" + vm.doc.VPCPRDataId);
                VPCPRDataService.saveDocPostCPRId(vm.doc,
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status == 200) {
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE_DOC'));
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                });

            }
            else{
                //without cprdataId
                vm.doc.VPCPRDataId = null;
                VPCPRDataService.saveDocNonCPRId(vm.doc,
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status == 200) {
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE_DOC'));
                        var saveResult = reply.data;
                        $uibModalInstance.close(saveResult);
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                });
            }
        }       

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();