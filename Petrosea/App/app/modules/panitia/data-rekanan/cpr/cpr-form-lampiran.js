﻿
(function () {
    'use strict';

    angular.module("app")
            .controller("frmUploadCPR", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCPRDataService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, VPCPRDataService, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {

        var vm = this;
        vm.VendorId = item.VendorId;
        vm.VPCPRDataId = item.VPCPRDataId;


        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");
            console.info(vm.VendorId);
            UploadFileConfigService.getByPageName("PAGE.ADMIN.CPR", function (response) {
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
            });
        };


        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            vm.prefix = "CPR_" + item.VPCPRDataId + "_" + item.VendorId;
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }


            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleCPR(vm.VPCPRDataId, vm.prefix, file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    console.info("response:" + JSON.stringify(response));
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = url;
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        if (vm.flag == 0) {

                            vm.size = Math.floor(s)
                        }

                        if (vm.flag == 1) {
                            vm.size = Math.floor(s / (1024));
                        }

                        VPCPRDataService.inserturl({
                            VPCPRDataId: item.VPCPRDataId,
                            UrlDocument: vm.pathFile
                        }, function (reply) {
                            console.info("reply" + JSON.stringify(reply))
                            UIControlService.unloadLoadingModal();
                            if (reply.status === 200) {
                                UIControlService.msg_growl("success", "MESSAGE.SUCC_UPLOAD");
                                $uibModalInstance.close();
                            } else {
                                UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                                return;
                            }
                        },
                        function (err) {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                            UIControlService.unloadLoadingModal();
                        });
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                    UIControlService.unloadLoading();
                });
        }

    }
})();