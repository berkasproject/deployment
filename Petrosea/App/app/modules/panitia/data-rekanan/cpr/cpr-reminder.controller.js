﻿(function () {
    'use strict';

    angular.module("app").controller("CPRReminderCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCPRDataService',
        '$state', 'UIControlService', 'GlobalConstantService','$uibModal', '$stateParams'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, VPCPRDataService,
        $state, UIControlService, GlobalConstantService, $uibModal, $stateParams) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.AddFlag = false;

        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            $translatePartialLoader.addPart("cpr");
            loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            VPCPRDataService.cekRole(function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info(vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            UIControlService.loadLoading("");
            VPCPRDataService.getReminder({
                Keyword: vm.srcText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.contracts = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CONTRACTS'));
            });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            loadAwal();
        };
        
        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('cpr-vendor');
        };
    }
})();