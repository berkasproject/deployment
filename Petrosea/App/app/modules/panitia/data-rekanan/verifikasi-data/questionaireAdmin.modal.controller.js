﻿(function () {
    'use strict';

    angular.module("app").controller("QuestionaireAdminModalCtrl", ctrl);
    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VerifikasiDataService', 'UIControlService', 'item', '$uibModalInstance', '$uibModal', 'GlobalConstantService','$filter'];
    /* @ngInject */
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        VerifikasiDataService, UIControlService, item, $uibModalInstance, $uibModal, GlobalConstantService,$filter) {
        var vm = this;
        vm.VendorID = item.VendorID;

        vm.init = init;
        function init() {
            loadKuesionerDD();
        }

        vm.loadradio = loadradio;
        function loadradio(questionID, questiondetailID) {
            for (var i = 0; i <= vm.ddQuest.MstDDQuestionnaire.DDQuestion.length - 1; i++) {
                if (vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].ID == questionID) {
                    if (vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail.length > 0) {
                        for (var j = 0; j <= vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail.length - 1; j++) {
                            if (vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail[j].ID != questiondetailID) {
                                vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail[j].VendorComplianceQuestionnaire[0].Answer = null;
                            }
                        }
                    }
                }
            }
        }


        vm.loadKuesionerDD = loadKuesionerDD;
        function loadKuesionerDD() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            VerifikasiDataService.loadDDQuest({
                VendorID: vm.VendorID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.ddQuest = reply.data;
                    console.info("loadDDquest:" + JSON.stringify(vm.ddQuest));
                    if (vm.ddQuest.SendQuestionnaireDate != null) {
                        vm.IsSent = true;
                    }
                    console.info("issent?" + vm.IsSent);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan dokumen" });
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoadingModal();
            });
        }

        vm.simpan = simpan;
        function simpan() {
            bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_SAVE_DDQUEST') + '</h4>', function (res) {
                if (res) {
                    UIControlService.loadLoadingModal("MESSAGE.LOADING");
                    vm.ddQuest.VendorID = vm.VendorID;
                    VerifikasiDataService.answerQuest(vm.ddQuest, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SAVE', '');
                            UIControlService.unloadLoadingModal();
                            //initialize();
                            // $state.transitionTo('suratPernyataan');
                            $uibModalInstance.close('cancel');
                            //window.location.reload();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.ERR_SAVE', '');
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                    });

                    //SocketService.emit("daftarRekanan");
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
