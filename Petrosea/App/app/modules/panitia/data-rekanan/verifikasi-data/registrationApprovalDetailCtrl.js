﻿(function () {
	'use strict';

	angular.module("app").controller("registrationApprovalDetailCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'VerifikasiDataService', 'item', '$uibModalInstance'];

	function ctrl(UIControlService, VerifikasiDataService, item, $uibModalInstance) {
		var vm = this
		console.log(item);
		vm.init = init
		function init() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING")
			VerifikasiDataService.detailRegistrationApproval({
			    VendorID: item.VendorID,
                ID: item.ID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.apprvs = reply.data;
				} else {
					$.growl.error({ message: "Get Approval Failed." });
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}
	}
})()