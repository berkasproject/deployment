﻿(function () {
    'use strict';

    angular.module("app").controller("DetailApprovalCtrl", ctrl);
    ctrl.$inject = ['$uibModal', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VerifikasiDataService', 'UIControlService', 'GlobalConstantService', '$state', '$filter'];
    /* @ngInject */
    function ctrl($uibModal, $http, $translate, $translatePartialLoader, $location, SocketService,
        VerifikasiDataService, UIControlService, GlobalConstantService, $state, $filter) {
        var vm = this;
        vm.init = init;
        vm.aktifasi = "";
        vm.Keyword = "";
        vm.pageSize = 10;


        vm.listDropdown =
        [
            { Value: 0, Name: "SELECT.ALL" },
            { Value: 1, Name: "SELECT.APPROVED" },
            { Value: 2, Name: "SELECT.REJECTED" },
            { Value: 3, Name: "SELECT.ON_PROCESS" }
        ]

        vm.filter = vm.listDropdown[3];

        function init() {
            $translatePartialLoader.addPart('verifikasi-data');
            jLoad(1);
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.listApproval = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            VerifikasiDataService.selectApproval({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.Keyword,
                FilterType: vm.filter.Value
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listApproval = data.List;
                    vm.listApproval.forEach(function (data) {
                        data.ApprovalDate = UIControlService.convertDateTime(data.ApprovalDate);
                    });
                    vm.totalItems = Number(data.Count);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data ApprovalVendor" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        vm.cancel = cancel;
        function cancel() {
            $state.transitionTo('verifikasi-data');
        }

        vm.save = save;
        function save(flag, data) {
            vm.IdApprove = data.ID;
            vm.flagApproval = flag;
            vm.VendorID = data.VendorID;
            vm.IsActive = data.IsActive;
            if (data.IsActive === false) {
                vm.Active = $filter ('translate')('mengaktifkan');
            }
            else vm.Active = $filter ('translate')('menon-aktifkan');

            if (flag == true) {
                bootbox.confirm('<h3 class="afta-font">' + $filter ('translate')('MESSAGE.CONFIRM_APP') + vm.Active + $filter ('translate')('MESSAGE.THIS_VENDOR') + '</h3>', function (res) {
                    if (res) {
                        UIControlService.loadLoading("MESSAGE.LOADING");
                        VerifikasiDataService.editApproval({
                            ID: data.ID,
                            flagApprove: flag,
                            VendorID: data.VendorID,
                            IsActive: vm.Active == "mengaktifkan" ? true : false
                        }, function (reply) {
                            UIControlService.unloadLoading();
                            if (reply.status === 200) {
                                UIControlService.msg_growl("success", "MESSAGE.SUCC_APP");
                                sendMailApprove();
                                init();
                            }
                            else {
                                UIControlService.msg_growl("error", "MESSAGE.GAGAL_NONAKTIF");
                                return;
                            }
                        }, function (err) {

                            UIControlService.msg_growl("error", "MESSAGE.API");
                            UIControlService.unloadLoading();
                        });
                    }
                    else {
                        console.info("sorry");
                    }
                });
                
            }
            else if (flag == false) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailRejectApproval.html',
                    controller: 'DetailRejectApproval',
                    controllerAs: 'DetailRejectApproval'
                });
                modalInstance.result.then(function (data) {
                    vm.Description = data;
                    VerifikasiDataService.editApproval({
                        ID: vm.IdApprove,
                        flagApprove: vm.flagApproval,
                        VendorID: vm.VendorID,
                        IsActive: vm.IsActive
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "MESSAGE.SUCC_REJ");
                            sendMailApprove();
                            init();
                        }
                        else {
                            UIControlService.msg_growl("error", "MESSAGE.GAGAL_NONAKTIF");
                            return;
                        }
                    }, function (err) {

                        UIControlService.msg_growl("error", "MESSAGE.API");
                        UIControlService.unloadLoading();
                    });
                });
            }
        }

        vm.sendMailApprove = sendMailApprove;
        function sendMailApprove(current) {
            //console.info("curr "+current)
            vm.listApproval = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            VerifikasiDataService.sendMailApprove({
                Status: vm.VendorID,
                Keyword: vm.flagApproval == true ? "" : vm.Description,
                FilterType: vm.flagApproval == true ? 0 : 1
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

    }
})();
