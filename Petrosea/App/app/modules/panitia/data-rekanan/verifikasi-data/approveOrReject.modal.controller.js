﻿(function () {
    'use strict';

    angular.module("app").controller("ApproveOrRejectModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance','VerifikasiDataService','$filter'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, VerifikasiDataService,$filter) {
        //console.info("masuk modal");
        var vm = this;
        
        vm.VendorID = item.VendorID;
        vm.VendorName = item.VendorName;
        vm.remark = "";
        vm.valid = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-data');
        }

        vm.submitRemark = submitRemark;
        function submitRemark() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            VerifikasiDataService.submitReject({
                VendorID: vm.VendorID,
                VerificationRemark: vm.remark
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_REJECT'));
                    $uibModalInstance.close();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.SUCC_REJECT_ERR'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.SUCC_REJECT_ERR'));
            });
        }

        vm.approved = approved;
        function approved() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            VerifikasiDataService.sendRegistrationApproval({
                VendorID: vm.VendorID
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
                    $uibModalInstance.close();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();