﻿(function () {
    'use strict';

    angular.module("app").controller("ExportDataPerusahaanCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VerifiedSendService', 'VerifikasiDataService', 'SrtPernyataanService', 'RoleService', 'UIControlService', '$uibModal', '$stateParams', '$state', 'GlobalConstantService', 'DataAdministrasiService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, VerifiedSendService, VerifikasiDataService, SrtPernyataanService,
        RoleService, UIControlService, $uibModal, $stateParams, $state, GlobalConstantService, DataAdministrasiService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.pageSize = 10;
        vm.fullSize = 10;
        vm.init = init;
        vm.moduleId = Number($stateParams.moduleId);
        vm.id = Number($stateParams.VendorID);

        function init() {
            $translatePartialLoader.addPart('verifikasi-data');
            $translatePartialLoader.addPart('pengurus-perusahaan');
            $translatePartialLoader.addPart('akta-pendirian');
            $translatePartialLoader.addPart('data-izinusaha');
            $translatePartialLoader.addPart('data-administrasi');
            $translatePartialLoader.addPart('tenaga-ahli');
            $translatePartialLoader.addPart('surat-pernyataan');
            $translatePartialLoader.addPart('data-perlengkapan');
            $translatePartialLoader.addPart('data-pengalaman');
            $translatePartialLoader.addPart('bank-detail');
            $translatePartialLoader.addPart('other-docs');
            $translatePartialLoader.addPart('vendor-balance');
            datavendor();
            if (vm.moduleId == 1) {
                otherdoc();
            }
            else if (vm.moduleId == 2) {
                loadlicense();
            }
            else if (vm.moduleId == 3) {
                loadBankDetail();
            }
            else if (vm.moduleId == 4) {
                loadSaham();
                loadAkta();
            }
            else if (vm.moduleId == 5) {
                loadNeraca();
                loadCommodity();
            }
            else if (vm.moduleId == 6) {
                loadPengurus();
            }
            else if (vm.moduleId == 7) {
                loadPengalaman();
            }
            else if (vm.moduleId == 8) {
                loadTenagaAhli();
            }
            else if (vm.moduleId == 9) {
                loadBuilding();
                loadEquipmentVehicle();
                loadEquipmentTools();
            }
            else if (vm.moduleId == 10) {
                loadVendorCommodity();
            }
            else if (vm.moduleId == 11) {
                loadUrlStatementLetter();
            }
        }

        var indeks = "";
        vm.makePDF = makePDF;
        function makePDF() {
            var namaFile = '';
            if (vm.moduleId == 1) {
                namaFile = " - Dokumen Lain-lain.pdf";
            }
            else if (vm.moduleId == 2) {
                namaFile = " - Izin Usaha.pdf";
            }
            else if (vm.moduleId == 3) {
                namaFile = " - Bank Detail.pdf";
            }
            else if (vm.moduleId == 4) {
                namaFile = " - Akta Pendirian.pdf";
            }
            else if (vm.moduleId == 5) {
                namaFile = " - Neraca.pdf";
            }
            else if (vm.moduleId == 6) {
                namaFile = " - Pengurus Perusahaan.pdf";
            }
            else if (vm.moduleId == 7) {
                namaFile = " - Pengalaman";
            }
            else if (vm.moduleId == 8) {
                namaFile = " - Tenaga Ahli.pdf";
            }
            else if (vm.moduleId == 9) {
                namaFile = " - Perlengkapan";
            }
            else if (vm.moduleId == 10) {
                namaFile = " - Data Administrasi.pdf";
            }
            else if (vm.moduleId == 10) {
                namaFile = " - Persetujuan.pdf";
            }
            if (vm.moduleId == 1 || vm.moduleId == 2 || vm.moduleId == 3 || vm.moduleId == 4 || vm.moduleId == 5 || vm.moduleId == 6 || vm.moduleId == 8 || vm.moduleId == 10 || vm.moduleId == 11) {
                html2canvas(document.getElementById('print'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download(vm.VendorName + namaFile);
                    }
                });
            }

            if (vm.moduleId == 7) {
                html2canvas(document.getElementById('selesai'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download(vm.VendorName + namaFile+" (Pekerjaan Sudah Selesai).pdf");
                    }
                });
                html2canvas(document.getElementById('berlangsung'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download(vm.VendorName + namaFile + " (Pekerjaan Yang Sedang Berlangsung).pdf");
                    }
                });
            }


            if (vm.moduleId == 9) {
                html2canvas(document.getElementById('bangunan'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download(vm.VendorName + namaFile + " (Data Bangunan).pdf");
                    }
                });
                html2canvas(document.getElementById('kendaraan'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download(vm.VendorName + namaFile + " (Data Kendaraan).pdf");
                    }
                });
                html2canvas(document.getElementById('alat-berat'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download(vm.VendorName + namaFile + " (Data Alat Berat).pdf");
                    }
                });
            }

        }

        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
        
        vm.VendorName = '';
        vm.negara = "";
        vm.kecamatan = "";
        vm.benua = "";
        vm.provinsi = "";
        vm.kota = "";
        vm.email = "";
        vm.telp = "";
        vm.fax = "";
        vm.cp = [];
        vm.alamat = [];
        vm.datavendor = datavendor;
        vm.addressBranch = '-';
        function datavendor() {
            vm.addressBranch = '';
            vm.addressMain = '';
            VerifikasiDataService.select({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.verified = reply.data;
                    console.info("data" + JSON.stringify(vm.verified));
                    vm.VendorName = vm.verified[0].Vendor.VendorName;
                    if (vm.verified[0].Vendor.VerificationRemark != null) {
                        vm.verificationRemark = vm.verified[0].Vendor.VerificationRemark;
                    }
                    for (var i = 0; i < vm.verified.length ; i++) {
                        if (vm.verified[i].VendorContactType.Value === "VENDOR_CONTACT_TYPE_COMPANY") {
                            if (vm.verified[i].Contact.Address.State.Country.Code === 'ID') vm.cek = true;
                            vm.State = vm.verified[i].Contact.Address.State.Country.Code;
                            vm.negara = vm.verified[i].Contact.Address.State.Country.Name;
                            vm.benua = vm.verified[i].Contact.Address.State.Country.Continent.Name;
                            vm.provinsi = vm.verified[i].Contact.Address.State.Name;
                            if (vm.verified[i].Contact.Address.City != null) {
                                vm.kota = vm.verified[i].Contact.Address.City.Name;
                            }
                            if (vm.verified[i].Contact.Address.Distric != null) {
                                vm.kecamatan = vm.verified[i].Contact.Address.Distric.Name;
                            }
                            if (vm.verified[i].Contact.Fax !== null) {
                                vm.ld = vm.verified[i].Contact.Fax;
                                vm.fax = vm.verified[i].Contact.Fax;
                            }
                            if (vm.verified[i].Contact.Email !== null) {
                                vm.email = vm.verified[i].Contact.Email;
                            }
                            if (vm.verified[i].Contact.Phone !== null) {
                                vm.telp = vm.verified[i].Contact.Phone;
                            }
                        }
                        if (vm.verified[i].VendorContactType.Value === "VENDOR_OFFICE_TYPE_BRANCH") {
                            vm.addressBranch = vm.verified[i].Contact.Address.AddressInfo + vm.verified[i].Contact.Address.AddressDetail;
                            vm.alamat.push(vm.addressBranch);
                        }
                        if (vm.verified[i].VendorContactType.Value === "VENDOR_OFFICE_TYPE_MAIN") {
                            vm.addressMain = vm.verified[i].Contact.Address.AddressInfo + vm.verified[i].Contact.Address.AddressDetail;
                            vm.alamat.push(vm.addressMain);
                        }
                        if (vm.verified[i].VendorContactType.Value === "VENDOR_CONTACT_TYPE_PERSONAL") {
                            vm.cp.push(vm.verified[i].Contact);
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }




        vm.loadUrlStatementLetter = loadUrlStatementLetter;
        function loadUrlStatementLetter() {
            if (localStorage.getItem("currLang") === 'id') {
                vm.DocType = 4225;
            }
            else {
                vm.DocType = 4232;
            }
            UIControlService.loadLoading("MESSAGE.LOADING");
            VerifikasiDataService.DocConduct({
                DocType: vm.DocType,
                VendorId: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    if (data == null || data == "") {
                        vm.flagBC = true;
                        vm.titleBC = "File Tidak Tersedia";
                    } else if (data != null) {
                        var ada = false;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].DocumentUrl !== null && data[i].DocumentUrl !== "") {
                                ada = true;
                                vm.flagBC = false;
                                vm.titleBC = "Downloads";
                                vm.urlBusinessConduct = data[i].DocumentUrl;
                                i = data.length;
                            }
                        }
                        if (ada == false) {
                            vm.flagBC = true;
                            vm.titleBC = "File Tidak Tersedia";
                        }
                        /*
		                if (data[0].DocumentUrl == "") {
		                    vm.flagBC = true;
		                    vm.titleBC = "File Tidak Tersedia";
		                } else {
		                    vm.titleBC = "Downloads";
		                    vm.flagBC = false;
		                    vm.urlBusinessConduct = data[0].DocumentUrl;
		                }*/
                    }
                    loadAggreement();

                } else {
                    $.growl.error({ message: "Gagal mendapatkan dokumen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadAggreement = loadAggreement;
        function loadAggreement() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            VerifikasiDataService.DocConduct({
                DocType: 4226,
                VendorId: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    if (data == null || data == "") {
                        vm.flagAG = true;
                        vm.titleAG = "File Tidak Tersedia";
                    }
                    else if (data != null || data != "") {
                        if (data[0].DocumentUrl == "") {
                            vm.flagAG = true;
                            vm.titleAG = "File Tidak Tersedia";
                        } else {
                            vm.titleAG = "Downloads";
                            vm.flagAG = false;
                            vm.urlAggrement = data[0].DocumentUrl;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan dokumen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.dataComm = [];
        function loadCommodity() {
            vm.dataComm = [];
            VerifikasiDataService.VendorCommodity({
                VendorID: vm.id
            }, function (reply2) {
                UIControlService.unloadLoading();
                if (reply2.status === 200) {
                    for (var i = 0; i < reply2.data.length; i++) {
                        if (reply2.data[i].IsActive === true) vm.dataComm.push(reply2.data[i]);

                    }
                    console.info("datacomm:" + JSON.stringify(vm.dataComm));
                } else
                    UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
            });
        }


        vm.loadVendorCommodity = loadVendorCommodity;
        function loadVendorCommodity() {
            DataAdministrasiService.SelectVendorCommodity({ VendorID: vm.id }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listBussinesDetailField = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }


        vm.listBuilding = [];
        function loadBuilding() {
            //var offset = (current * vm.fullSize) - vm.fullSize;
            UIControlService.loadLoading(vm.msgLoading);
            VerifikasiDataService.selectBuilding({
                Status: vm.id, Ofsset: 0, Limit: 25
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.listBuilding = reply.data.List;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.listVehicle = [];
        function loadEquipmentVehicle() {
            //var offset = (current * vm.fullSize) - vm.fullSize;
            UIControlService.loadLoading(vm.msgLoading);
            VerifikasiDataService.selectVehicle({
                Status: vm.id, Ofsset: 0, Limit:25
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.listVehicle = reply.data.List;
                for (var i = 0; i < vm.listVehicle.length; i++) {
                    vm.listVehicle[i].MfgDate = UIControlService.getStrDate(vm.listVehicle[i].MfgDate);
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.listEquipmentTools = [];
        function loadEquipmentTools() {
            //var offset = (current * vm.fullSize) - vm.fullSize;
            UIControlService.loadLoading(vm.msgLoading);
            VerifikasiDataService.selectEquipment({
                Status: vm.id, Ofsset: 0, Limit: 25
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.listEquipmentTools = reply.data.List;
                for (var i = 0; i < vm.listEquipmentTools.length; i++) {
                    vm.listEquipmentTools[i].MfgDate = UIControlService.getStrDate(vm.listEquipmentTools[i].MfgDate);
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }


        vm.loadTenagaAhli = loadTenagaAhli;
        function loadTenagaAhli(current) {
            vm.vendorexperts = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            VerifikasiDataService.allTenagaahli({
                Status: vm.id,
                Offset: offset,
                Limit: 100,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorexperts = reply.data.List;
                    console.info("vendorexperts" + JSON.stringify(vm.vendorexperts));
                    //loadCertificate(vm.vendorexperts);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadPengalaman = loadPengalaman;
        function loadPengalaman() {
            UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
            VerifikasiDataService.selectExperience({
                Offset:0,
                Limit: 100,
                Keyword: '',
                column: 1,
                Status: vm.id
            }, function (reply) {
                if (reply.status === 200) {
                    vm.listFinishExp = reply.data.List;
                    for (var i = 0; i < vm.listFinishExp.length; i++) {
                        vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);
                    }
                    vm.totalItems = reply.data.Count;
                    UIControlService.unloadLoading();
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
            });

            UIControlService.loadLoading('MESSAGE.LOADING_VENDOREXPERIENCE');
            VerifikasiDataService.selectExperience({
                Offset: 0,
                Limit: 100,
                Keyword: '',
                column: 2,
                Status: vm.id
            }, function (reply) {
                if (reply.status === 200) {
                    vm.listCurrentExp = reply.data.List;
                    for (var i = 0; i < vm.listCurrentExp.length; i++) {
                        vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
                    }
                    vm.totalItems = reply.data.Count;
                    UIControlService.unloadLoading();
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
            });

        }



        vm.loadPengurus = loadPengurus;
        function loadPengurus() {
            VerifikasiDataService.GetByVendorComPer({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.compPersons = reply.data;
                    console.info("compers" + JSON.stringify(vm.compPersons));
                } else {
                    UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD');
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD');
                UIControlService.unloadLoading();
            });
        }

        vm.loadNeraca = loadNeraca;
        function loadNeraca() {
            loadBalanceUrl();
            vm.asset = 0;
            vm.hutang = 0;
            vm.modal = 0;
            VerifikasiDataService.selectNeraca({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorbalance = reply.data;
                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        if (vm.vendorbalance[i].WealthType.Name === "WEALTH_TYPE_ASSET") {
                            vm.listAsset = vm.vendorbalance[i];
                        }
                        if (vm.vendorbalance[i].WealthType.Name === "WEALTH_TYPE_DEBTH") {
                            vm.listDebth = vm.vendorbalance[i];
                        }
                    }
                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        for (var j = 0; j < vm.vendorbalance[i].subWealth.length; j++) {
                            if (vm.vendorbalance[i].subWealth[j].subCategory.length === 0) {
                                if (vm.vendorbalance[i].WealthType.RefID === 3097 && vm.vendorbalance[i].subWealth[j].IsActive === true) {
                                    if (vm.asset === 0) {
                                        vm.asset = vm.vendorbalance[i].subWealth[j].nominal;
                                    } else
                                        vm.asset = +vm.asset + +vm.vendorbalance[i].subWealth[j].nominal;

                                } else if (vm.vendorbalance[i].WealthType.RefID === 3099 && vm.vendorbalance[i].subWealth[j].IsActive === true) {
                                    if (vm.hutang === 0) {
                                        vm.hutang = vm.vendorbalance[i].subWealth[j].nominal;
                                    } else {
                                        vm.hutang = +vm.hutang + +vm.vendorbalance[i].subWealth[j].nominal;
                                    }


                                }
                            }
                            for (var k = 0; k < vm.vendorbalance[i].subWealth[j].subCategory.length; k++) {

                                if (vm.vendorbalance[i].subWealth[j].subCategory[k].WealthType === 3097 && vm.vendorbalance[i].subWealth[j].subCategory[k].IsActive === true) {
                                    if (vm.asset === 0) {
                                        vm.asset = vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                    } else
                                        vm.asset = +vm.asset + +vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;

                                } else if (vm.vendorbalance[i].subWealth[j].subCategory[k].WealthType === 3099 && vm.vendorbalance[i].subWealth[j].subCategory[k].IsActive === true) {
                                    if (vm.hutang === 0) {
                                        vm.hutang = vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;

                                    } else {
                                        vm.hutang = +vm.hutang + +vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                    }


                                }
                            }
                        }
                    }
                    vm.modal = +vm.asset - +vm.hutang;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadBalanceUrl = loadBalanceUrl;
        function loadBalanceUrl() {
            VerifikasiDataService.balanceDocUrl({ VendorID: vm.id }, function (reply) {
                if (reply.status === 200) {
                    if (reply.data != null) {
                        vm.balanceDocUrl = reply.data.DocUrl;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        vm.loadAkta = loadAkta;
        function loadAkta() {
            VerifikasiDataService.GetByVendor({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.Akta = reply.data;
                    vm.countPendirian = 0;
                    vm.countPerubahan = 0;
                    vm.countPengesahan = 0;
                    for (var i = 0; i < vm.Akta.length; i++) {
                        if (vm.Akta[i].DocumentType === 'LEGAL_DOC_PENDIRIAN') {
                            vm.countPendirian = 1;
                        } else if (vm.Akta[i].DocumentType === 'LEGAL_DOC_PERUBAHAN') {
                            vm.countPerubahan = 1;
                        } else if (vm.Akta[i].DocumentType === 'LEGAL_DOC_PENGESAHAN') {
                            vm.countPengesahan = 1;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadSaham = loadSaham;
        function loadSaham() {
            VerifikasiDataService.selectSaham({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.verifiedSaham = reply.data;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.otherdoc = otherdoc;
        function otherdoc() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            VerifikasiDataService.SelectVend({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.document = data;
                    vm.document.forEach(function (cr) {
                        cr.ValidDateConverted = convertDate(cr.ValidDate);
                    });
                } else {
                    $.growl.error({ message: "Gagal mendapatkan dokumen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadlicense = loadlicense;
        function loadlicense() {
            vm.currentPage = 1;
            var offset = (1 * 10) - 10;
            VerifikasiDataService.selectlicensi({
                Status: vm.id,
                Offset: offset,
                Limit: vm.fullSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.license = reply.data;
                    console.info("license:" + JSON.stringify(vm.license));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Izin Usaha Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadBankDetail = loadBankDetail;
        function loadBankDetail() {
            vm.currentPage = 1;
            var offset = (1 * 10) - 10;
            vm.bankdetail = [];
            VerifikasiDataService.selectBankDetail({
                VendorID: vm.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.bankdetail = reply.data;
                    console.info("bankdet:" + JSON.stringify(vm.bankdetail));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Detail Bank Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('proses-verifikasi', { id: vm.id });
        }





        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        }
    }
})();
//TODO