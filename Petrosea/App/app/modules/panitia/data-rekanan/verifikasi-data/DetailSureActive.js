(function () {
	'use strict';

	angular.module("app").controller("DetailSureActive", ctrl);
	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VerifikasiDataService', 'UIControlService', 'item', '$uibModalInstance', '$uibModal', 'GlobalConstantService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        VerifikasiDataService, UIControlService, item, $uibModalInstance, $uibModal, GlobalConstantService) {
		var vm = this;
		vm.VendorID = item.VendorID;
		vm.Description = item.Description;
		vm.act = item.act;
		vm.init = init;
		vm.aktifasi = "";
		vm.pageSize = 10;
		vm.Keyword = "";

		function init() {
			$translatePartialLoader.addPart('verifikasi-data');
		};

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		vm.save = save;
		function save() {
			VerifikasiDataService.editActive({
				IsActive: vm.act,
				VendorID: vm.VendorID,
				Description: vm.Description
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    if (reply.data == true) {
				        var msg = "";
				        if (vm.act === false) msg = "{{'Non-Aktifkan'|translate}}";
				        if (vm.act === true) msg = "{{'Aktifkan'|translate}} ";
				        UIControlService.msg_growl("success", "{{'MESSAGE.NOTIF'|translate}} " + msg);
				        SendEmail();
				        $uibModalInstance.close();
				    }
				    else {
				        UIControlService.msg_growl("warning", "MESSAGE.NO_APP_POST");
				    }
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_DEACTIVATE");
					return;
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.SendEmail = SendEmail;
		function SendEmail() {
		    VerifikasiDataService.sendMailActiveOrNot({
		        IsActive: vm.act,
		        VendorID: vm.VendorID,
		        Description: vm.Description
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            UIControlService.msg_growl("success", "{{'MESSAGE.EMAIL_SENT'}}" + msg);
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}

	}
})();
