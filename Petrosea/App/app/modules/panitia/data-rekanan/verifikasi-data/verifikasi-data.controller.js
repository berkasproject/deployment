(function () {
	'use strict';

	angular.module("app").controller("VerifikasiDataCtrl", ctrl);
	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$stateParams', 'VerifikasiDataService', 'UIControlService', '$uibModal', '$state', 'GlobalConstantService', 'Excel', '$timeout', '$filter'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $stateParams,
        VerifikasiDataService, UIControlService, $uibModal, $state, GlobalConstantService, Excel, $timeout, $filter) {
		var vm = this;
		vm.datarekanan = [];
		vm.status = Number($stateParams.status);
		vm.startDate = new Date($stateParams.startDate);
		vm.currentPage = 1;
		vm.fullSize = 10;
		vm.offset = (vm.currentPage * 10) - 10;
		vm.totalRecords = 0;
		vm.nCompany = '';
		vm.activator;
		vm.verificator;
		vm.menuhome = 0;
		vm.cmbStatus = 0;
		vm.rekanan_id = '';
		vm.flag = 0;
		var apipoint = GlobalConstantService.getConstant("api_endpoint");
		vm.date = "";
		vm.year = "";
		vm.datemonth = "";

		vm.waktuMulai1 = (vm.year - 1) + '-' + vm.datemonth;
		vm.waktuMulai2 = vm.date;

		vm.sStatus = -1;
		vm.thisPage = 12;
		vm.verificationPage = 130;
		vm.verifikasi = {};
		vm.isCalendarOpened = [false, false, false, false];
		//functions
		vm.init = init;
		vm.jLoad = jLoad;
		vm.openCalendar = openCalendar;
		vm.show = show;
		vm.add = add;
		vm.addVerifikasi = addVerifikasi;
		vm.listExpired = listExpired;
		vm.flagButton = true;
		vm.ViewModelVerified = {};
		vm.currentPage = 1;
		function init() {
			vm.listDropdown = [
				{ Value: 0, Name: "SELECT.ALL" },
				{ Value: 1, Name: "SELECT.NOT_ACTIVED" },
				{ Value: 2, Name: "SELECT.ACTIVED" },
				{ Value: 6, Name: "SELECT.END_ACTIVED" },
				{ Value: 3, Name: "SELECT.NOT_VERIFIED" },
				{ Value: 4, Name: "SELECT.VERIFIED" },
				{ Value: 8, Name: "SELECT.END_VERIFIED" },
				{ Value: 10, Name: "SELECT.NOT_ACTIVE" },
			]

			vm.listDropdown2 = [
				{ Value: 0, Name: "SELECT.ALL" },
				{ Value: 1, Name: "SELECT.QUEST_SENT" },
				{ Value: 2, Name: "SELECT.NEED_QUEST" },
				{ Value: 6, Name: "SELECT.NEED_REVIEW" },
				{ Value: 3, Name: "SELECT.ON_PROCESS" },
				{ Value: 4, Name: "SELECT.APPROVED" },
				{ Value: 5, Name: "SELECT.REJECTED" },
			]

			if (vm.status !== 0) {
				showFilteredData();
			}

			var getItemLocal = JSON.parse(localStorage.getItem('ViewModelVerified'));
			if (getItemLocal != null) {
				if (getItemLocal.Status != undefined) {
					for (var i = 0; i < vm.listDropdown.length; i++) {
						if (getItemLocal.Status.Value == vm.listDropdown[i].Value) {
							vm.Status = vm.listDropdown[i];
							vm.cmbStatus = getItemLocal.Status.Value;
						}
					}
				}
				if (getItemLocal.StartDate) vm.verifikasi.StartDate = new Date(Date.parse(getItemLocal.StartDate));
				if (getItemLocal.EndDate) vm.verifikasi.EndDate = new Date(Date.parse(getItemLocal.StartDate));
				if (getItemLocal.Keyword != "") vm.nCompany = getItemLocal.Keyword;
				if (vm.currentPage) vm.currentPage = getItemLocal.currentPage;
				localStorage.removeItem('ViewModelVerified');
			}
			$translatePartialLoader.addPart('verifikasi-data');
			jLoad(vm.currentPage);
			convertToDate();
			getUserLogin();
		};

		function showFilteredData() {
			vm.cmbStatus = vm.status;
			for (var i = 0; i < vm.listDropdown.length; i++) {
				if (vm.cmbStatus == vm.listDropdown[i].Value) {
					vm.Status = vm.listDropdown[i];
					i = vm.listDropdown.length;
				}
			}
			if (vm.cmbStatus !== 3) {
				vm.verifikasi.StartDate = new Date();
				vm.verifikasi.StartDate.setDate(1);
				vm.currentMonth = new Date().getMonth() + 1;
				vm.currentYear = new Date().getFullYear();
				vm.lastDay = getDaysInMonth(vm.currentMonth, vm.currentYear);
				vm.verifikasi.EndDate = new Date();
				vm.verifikasi.EndDate.setDate(vm.lastDay);
				//console.info("startdate" + vm.verifikasi.StartDate);
				//console.info("endate" + vm.verifikasi.EndDate);
				jLoad(1);
			}
		}


		function exportToExcel() { // ex: '#my-table'
			var exportHref = Excel.tableToExcel('#print', 'data rekanann');
			$timeout(function () { location.href = exportHref; }, 100); // trigger download
		}

		function getDaysInMonth(m, y) {
			return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
		}

		vm.getUserLogin = getUserLogin;
		function getUserLogin() {
			VerifikasiDataService.getUserLogin(function (reply) {
				if (reply.status === 200) {
					var data = reply.data;
					for (var i = 0; i < data.length; i++) {
						if (data[i] == 'LO' || data[i] === 'LA' || data[i] === 'LT') {
							vm.flagButton = false;
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data " });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.verifyEndDate = verifyEndDate;
		function verifyEndDate(selectedEndDate, selectedStartDate) {
			var convertedEndDate = UIControlService.getStrDate(selectedEndDate);
			var convertedStartDate = UIControlService.getStrDate(selectedStartDate);
			//console.info("selected end date" + JSON.stringify(convertedEndDate));
			//console.info("selected start date" + JSON.stringify(convertedStartDate));
			if (convertedEndDate < convertedStartDate) {
				UIControlService.msg_growl("warning", "MESSAGE.TGL_BATAS_AKHIR");
				vm.verifikasi.EndDate = " ";
				return;
			}

			//jLoad(1);

			//else {
			//  console.info("masak");
			//}
		}

		vm.verifyStartDate = verifyStartDate;
		function verifyStartDate(selectedEndDate, selectedStartDate) {
		    var convertedEndDate = UIControlService.getStrDate(selectedEndDate);
		    var convertedStartDate = UIControlService.getStrDate(selectedStartDate);
		    //console.info("selected end date" + JSON.stringify(convertedEndDate));
		    //console.info("selected start date" + JSON.stringify(convertedStartDate));
		    if (convertedEndDate < convertedStartDate) {
		        UIControlService.msg_growl("warning", "MESSAGE.TGL_BATAS_START");
		        vm.verifikasi.StartDate = " ";
		        return;
		    }

		    //jLoad(1);

		    //else {
		    //  console.info("masak");
		    //}
		}

		function show() {
			//console.info(vm.Status);
			//console.info("startdate" + vm.verifikasi.StartDate);
			//console.info("enddate" + vm.verifikasi.EndDate);
			if (vm.verifikasi.StartDate === undefined && vm.verifikasi.EndDate !== undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.ERR_DATE1");
				return;
			}
			if (vm.verifikasi.EndDate === undefined && vm.verifikasi.StartDate !== undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.ERR_DATE2");
				return;
			}
		    //if(vm.verifikasi.startDate)
			console.log(new Date(vm.verifikasi.startDate))
			console.log(new Date(vm.verifikasi.EndDate))
			if (vm.Status === undefined) {
				vm.cmbStatus = 0;
			}
			else {
				vm.cmbStatus = vm.Status.Value;
			}
			jLoad(1);
		}

		function jLoad(current) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.verifikasidata = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;

			var status2 = 0;
			if (vm.statusApprv != undefined) {
				vm.status2 = vm.statusApprv.Value;
			}


			VerifikasiDataService.all({
				Offset: offset,
				Limit: vm.fullSize,
				Keyword: vm.nCompany,
				Status: vm.cmbStatus,
				Date1: UIControlService.getStrDate(vm.verifikasi.StartDate),
				Date2: UIControlService.getStrDate(vm.verifikasi.EndDate),
				FilterType: vm.status2
			}, function (reply) {
				if (reply.status === 200) {
					var data = reply.data;
					vm.verifikasidata = data.List;
					console.log(vm.verifikasidata);
					if (vm.verifikasidata.length > 0) {
						for (var i = 0; i < vm.verifikasidata.length; i++) {
							vm.vendorID = vm.verifikasidata[i].VendorID;
							vm.IsViewDetail = vm.verifikasidata[i].IsViewDetail;
							loadContact(vm.vendorID, i);
						}
					}
					vm.totalItems = Number(data.Count);
					vm.flag = vm.cmbStatus;
					UIControlService.unloadLoading();
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Rekanan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.export = [];
		vm.exportDataVendor = exportDataVendor;
		function exportDataVendor() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.export = [];
			VerifikasiDataService.exportDataVendor2({
				Keyword: vm.nCompany,
				Status: vm.cmbStatus,
				Date1: UIControlService.getStrDate(vm.verifikasi.StartDate),
				Date2: UIControlService.getStrDate(vm.verifikasi.EndDate)
			}, function (reply) {
				if (reply.status === 200) {
					var data = reply.data;
					vm.export = data;
					console.info("export:" + JSON.stringify(vm.export));
					vm.filterdata = [];
					//console.info("total semua data:" + JSON.stringify(vm.alldata.length));

					var no = 0;
					for (var i = 0; i <= vm.export.length - 1; i++) {
						UIControlService.loadLoading("MESSAGE.LOADING");
						var bidang = '';
						var vendorSap = '';

						//area,email,telp
						var email = '';
						var telp = '';
						var area = '';
						if (vm.export[i].Contacts != null) {
							for (var a = 0; a <= vm.export[i].Contacts.length - 1; a++) {
								if (vm.export[i].Contacts[a].VendorContactType.Name == 'VENDOR_OFFICE_TYPE_MAIN' && vm.export[i].Contacts[a].IsActive == true) {
									area = vm.export[i].Contacts[a].Contact.Address.AddressDetail;
								}

								if (vm.export[i].Contacts[a].VendorContactType.Name == 'VENDOR_CONTACT_TYPE_COMPANY' && vm.export[i].Contacts[a].IsActive == true) {
									if (vm.export[i].Contacts[a].Contact.Email != null) {
										email = vm.export[i].Contacts[a].Contact.Email;
									}
									telp = vm.export[i].Contacts[a].Contact.Phone;
								}
							}
						}

						//asosiasi
						var asosiasi = '';
						if (vm.export[i].AssociationDetail != null) {
							asosiasi = vm.export[i].AssociationDetail.AssosiationName;
						}

						//status
						var status = '';
						if (vm.export[i].IsActived == null) {
							status = 'Belum Teraktivasi';
						}
						if (vm.export[i].IsActived == true && vm.export[i].VerifiedSendDate == null) {
							status = 'Teraktivasi';
						}
						if (vm.export[i].IsActived == false) {
							status = 'Tidak Teraktivasi';
						}
						if (vm.export[i].IsActived == true && vm.export[i].VerifiedSendDate != null && vm.export[i].Isverified == null) {
							status = 'Belum Terverifikasi';
						}
						if (vm.export[i].IsActived == true && vm.export[i].VerifiedSendDate != null && vm.export[i].Isverified == true) {
							status = 'Terverifikasi';
						}
						if (vm.export[i].IsActived == true && vm.export[i].VerifiedSendDate != null && vm.export[i].Isverified == 0) {
							status = 'Tidak Terverifikasi';
						} else {
							status = 'Tidak Aktif'
						}

						if (vm.export[i].commodity != null) {
							for (var j = 0; j <= vm.export[i].commodity.length - 1; j++) {



								var noIUJP = '';
								var noSKT = '';
								var noSIUP = '';
								var ijinPengamanan = '';
								var higiene = '';
								var tenagakerja = '';
								var bpjsKes = '';
								var bpjsTenagaKerja = '';
								var tdp = '';
								var ho = '';
								var iup = '';
								var rat = '';

								var bidangusaha = '';
								var kategoriBidang = '';
								var kb2 = '';
								var kodeKB2 = '';
								var skala = '';
								var teknikal = '';

								//bidangUsaha
								if (vm.export[i].commodity[j].CommodityID == null) {
									bidangusaha = vm.export[i].commodity[j].BusinessField.Name;
									if (vm.export[i].commodity[j].BusinessField.IsCore == false) {
										kategoriBidang = 'Non Inti';
										kb2 = 'N';
									}
									else if (vm.export[i].commodity[j].BusinessField.IsCore == true) {
										kategoriBidang = 'Inti';
										kb2 = 'I';
									}
								}
								else {
									bidangusaha = vm.export[i].commodity[j].BusinessField.Name + ' - ' + vm.export[i].commodity[j].Commodity.Name;
									kategoriBidang = 'Komoditi';
									kb2 = 'K';
								}

								if (vm.export[i].commodity[j].BusinessField.GoodsOrService == 3090) {
									kodeKB2 = 'Barang';
								}
								else if (vm.export[i].commodity[j].BusinessField.GoodsOrService == 3091) {
									kodeKB2 = 'Jasa';
								}
								else if (vm.export[i].commodity[j].BusinessField.GoodsOrService == 3092) {
									kodeKB2 = 'Barang & Jasa';
								}

								//kualifikasi
								if (vm.export[i].commodity[j].TechnicalReffS != null) {
									teknikal = vm.export[i].commodity[j].TechnicalReffS.Value;
								}

								if (vm.export[i].commodity[j].ScaleReffS != null) {
									if (vm.export[i].commodity[j].ScaleReffS.Value == 'SMALL_COMPANY') {
										skala = 'Kecil';
									}
									else if (vm.export[i].commodity[j].ScaleReffS.Value == 'MEDIUM_COMPANY') {
										skala = 'Menengah';
									}
									else if (vm.export[i].commodity[j].ScaleReffS.Value == 'LARGE_COMPANY') {
										skala = 'Besar';
									}
									else if (vm.export[i].commodity[j].ScaleReffS.Value == 'ALL_COMPANY') {
										skala = 'Semua';
									}
								}


								//ijinUsaha
								if (vm.export[i].commodity[j].License != null) {
									for (var k = 0; k <= vm.export[i].commodity[j].License.length - 1; k++) {
										if (vm.export[i].commodity[j].License[k].LicenseName == 'SKT / Tanda Registrasi Minerba') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												noSKT = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'IUJPU') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												noIUJP = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'SIUP') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												noSIUP = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'Ijin Pengamanan') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												ijinPengamanan = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'Surat Kelayakan Hygiene') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												higiene = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'Ijin Penyedia Jasa Tenaga Kerja') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												tenagakerja = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}
										if (vm.export[i].commodity[j].License[k].LicenseName == 'Ijin Penyedia Jasa Tenaga Kerja') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												tenagakerja = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'BPJS Kesehatan') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												bpjsKes = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'BPJS Ketenagakerjaan') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												bpjsTenagaKerja = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'TDP') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												tdp = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'HO/SITU/Ijin Domisili') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												ho = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'IUP') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												iup = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}

										if (vm.export[i].commodity[j].License[k].LicenseName == 'RAT (Untuk Koperasi)') {

											if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
												rat = vm.export[i].commodity[j].License[k].LicenseNo.toString();
											}

										}
									}
								}
								//no
								var no_convert = '';

								//no
								if (j == 0) {
									no_convert = no + 1;
								}
								else {
									no_convert = '';
								}
								//tipeVendor
								if (vm.export[i].VendorTypeID == 3090) {
									bidang = 'B';
								}
								else if (vm.export[i].VendorTypeID == 3091) {
									bidang = 'J';
								}
								else if (vm.export[i].VendorTypeID == 3092) {
									bidang = 'BJ';
								}

								//vendorCode SAP
								if (vm.export[i].SAPCode != null) {
									vendorSap = vm.export[i].SAPCode;
								}

								if (telp == null) {
									telp = '';
								}

								var forExcel = {
									No: no_convert,
									Bidang: bidang,
									VendorCode: vendorSap,
									Area: area,
									Entity: vm.export[i].businessName,
									Nama_Perusahaan: vm.export[i].VendorName,
									Email: email,
									No_Telp: telp.toString(),
									IUJP: noIUJP,
									SKT: noSKT,
									SIUP: noSIUP,
									IjinPengamanan: ijinPengamanan,
									SuratKelayakanHygiene: higiene,
									IjinPenyediaJasaTenagaKerja: tenagakerja,
									BPJSKesehatan: bpjsKes,
									BPJSKetenagakerjaan: " " + bpjsTenagaKerja,
									TDP: tdp,
									HO: ho,
									IUP: iup,
									RAT: rat,
									Klasifikasi: bidangusaha,
									Kategori_Bidang: kategoriBidang,
									KB2: kb2,
									Kode_KB2: kodeKB2,
									Kualifikasi2: skala,
									Kinerja: teknikal,
									Asosiasi: asosiasi,
									Status: status

								}
								vm.filterdata.push(forExcel);
							}
						}
						else {

							console.info("no" + no);
							//untuk yang belum isi bd.usaha

							var noIUJP = '';
							var noSKT = '';
							var noSIUP = '';
							var ijinPengamanan = '';
							var higiene = '';
							var tenagakerja = '';
							var bpjsKes = '';
							var bpjsTenagaKerja = '';
							var tdp = '';
							var ho = '';
							var iup = '';
							var rat = '';

							var bidangusaha = '';
							var kategoriBidang = '';
							var kb2 = '';
							var kodeKB2 = '';
							var skala = '';
							var teknikal = '';

							//no
							var no_convert = 0;
							no_convert = no + 1;


							//ijinUsaha
							/*
                            if (vm.export[i].commodity[j].License != undefined || vm.export[i].commodity[j].License != null) {
                                for (var k = 0; k <= vm.export[i].commodity[j].License.length - 1; k++) {
                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'SKT / Tanda Registrasi Minerba') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            noSKT = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'IUJPU') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            noIUJP = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'SIUP') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            noSIUP = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'Ijin Pengamanan') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            ijinPengamanan = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'Surat Kelayakan Hygiene') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            higiene = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'Ijin Penyedia Jasa Tenaga Kerja') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            tenagakerja = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }
                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'Ijin Penyedia Jasa Tenaga Kerja') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            tenagakerja = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'BPJS Kesehatan') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            bpjsKes = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'BPJS Ketenagakerjaan') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            bpjsTenagaKerja = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'TDP') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            tdp = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'HO/SITU/Ijin Domisili') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            ho = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'IUP') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            iup = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }

                                    if (vm.export[i].commodity[j].License[k].LicenseName == 'RAT (Untuk Koperasi)') {

                                        if (vm.export[i].commodity[j].License[k].LicenseNo != null) {
                                            rat = vm.export[i].commodity[j].License[k].LicenseNo.toString();
                                        }

                                    }
                                }
                            }
                            */

							//tipeVendor
							if (vm.export[i].VendorTypeID == 3090) {
								bidang = 'B';
							}
							else if (vm.export[i].VendorTypeID == 3091) {
								bidang = 'J';
							}
							else if (vm.export[i].VendorTypeID == 3092) {
								bidang = 'BJ';
							}

							//vendorCode SAP
							if (vm.export[i].SAPCode != null) {
								vendorSap = vm.export[i].SAPCode;
							}


							if (telp == null) {
								telp = '';
							}

							var forExcel = {
								No: no + 1,
								Bidang: bidang,
								VendorCode: vendorSap,
								Area: area,
								Entity: vm.export[i].businessName,
								Nama_Perusahaan: vm.export[i].VendorName,
								Email: email,
								No_Telp: telp.toString(),
								IUJP: noIUJP,
								SKT: noSKT,
								SIUP: noSIUP,
								IjinPengamanan: ijinPengamanan,
								SuratKelayakanHygiene: higiene,
								IjinPenyediaJasaTenagaKerja: tenagakerja,
								BPJSKesehatan: bpjsKes,
								BPJSKetenagakerjaan: " " + bpjsTenagaKerja,
								TDP: tdp,
								HO: ho,
								IUP: iup,
								RAT: rat,
								Klasifikasi: bidangusaha,
								Kategori_Bidang: kategoriBidang,
								KB2: kb2,
								Kode_KB2: kodeKB2,
								Kualifikasi2: skala,
								Kinerja: teknikal,
								Asosiasi: asosiasi,
								Status: status

							}
							vm.filterdata.push(forExcel);

						}
						no = no + 1;
						//vm.filterdata.push(forExcel);
					}
					//console.info("dataForExcel:" + JSON.stringify(vm.filterdata));
					JSONToCSVConvertor(vm.filterdata, true);

					UIControlService.unloadLoading();
				} else {
					$.growl.error({ message: "Gagal mendapatkan data export data vendor" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}


		vm.isSendQuest = false
		vm.sendQuestionnaire = sendQuestionnaire;
		function sendQuestionnaire(vendorID, vendorname) {
			bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_SENDQUEST') + vendorname + '?</h4>', function (res) {
				if (res) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					VerifikasiDataService.sendQuestionnaire({
						VendorID: vendorID
					}, function (reply) {
						if (reply.status === 200) {
							vm.isSendQuest = reply.data;
							console.info("issend?" + vm.isSendQuest);
							if (vm.isSendQuest == true) {
								UIControlService.msg_growl("success", "Berhasil mengirim kuesioner");
								init();
							}
							else {
								UIControlService.msg_growl("warning", "Kuesioner tidak ditemukan. Silakan membuat kuesioner terlebih dahulu.");
							}
							UIControlService.unloadLoading();
						} else {
							$.growl.error({ message: "Gagal mengirim kuesioner" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});

					//SocketService.emit("daftarRekanan");
				}
			});

		}

		vm.sendRegistrationApproval = sendRegistrationApproval
		function sendRegistrationApproval(data) {
			var item = {
				VendorID: data.VendorID,
				VendorName: data.VendorName
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/approveOrReject.modal.html',
				controller: 'ApproveOrRejectModalController',
				controllerAs: 'ApproveOrRejectModalCtrl',
				resolve: { item: function () { return item } }
			})
			modalInstance.result.then(function () {
				init()
			})

			//bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV'), function (yes) {
			//	if (yes) {
			//		UIControlService.loadLoading("MESSAGE.LOADING");
			//		VerifikasiDataService.sendRegistrationApproval({
			//			VendorID: data
			//		}, function (reply) {
			//			if (reply.status === 200) {
			//				UIControlService.unloadLoading();
			//				UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
			//				init();
			//			} else {
			//				UIControlService.unloadLoading();
			//				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
			//			}
			//		}, function (error) {
			//			UIControlService.unloadLoading();
			//			UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
			//		});
			//	}
			//});
		}

		vm.registrationApprovalDetail = registrationApprovalDetail;
		function registrationApprovalDetail(dt) {
			var item = {
				VendorID: dt
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/registrationApprovalDetail.html',
				controller: 'registrationApprovalDetailCtrl',
				controllerAs: 'registrationApprovalDetailCtrl',
				resolve: { item: function () { return item } }
			})
			modalInstance.result.then(function () {
				init()
			})
		}

		vm.exportDataPerusahaan = exportDataPerusahaan;
		function exportDataPerusahaan() {
			var headers = {};
			headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

			$http({
				method: 'POST',
				url: apipoint + '/verifiedvendor/exportDataPerusahaan',
				headers: headers,
				data: {
				},
				responseType: 'arraybuffer'
			}).success(function (data, status, headers) {
				var linkElement = document.createElement('a');
				var fileName = "Data Perusahaan.docx";

				try {
					var blob = new Blob([data], { type: headers('content-type') });
					var url = window.URL.createObjectURL(blob);
					linkElement.setAttribute('href', url);
					linkElement.setAttribute('download', fileName);

					var clickEvent = new MouseEvent("click", {
						"view": window,
						"bubbles": true,
						"cancelable": false
					});

					linkElement.dispatchEvent(clickEvent);
				} catch (e) {
					console.log(e);
				}
			});
		}


		vm.exportExcel = exportExcel;
		function exportExcel() {
			//console.info("total:" + vm.totalItems);
			var offset = (1 * 10) - 10;
			VerifikasiDataService.all({
				Offset: offset,
				Limit: vm.totalItems,
				Keyword: vm.nCompany,
				Status: vm.cmbStatus,
				Date1: UIControlService.getStrDate(vm.verifikasi.StartDate),
				Date2: UIControlService.getStrDate(vm.verifikasi.EndDate)
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.alldata = data.List;
					//console.info("datasampel:" + JSON.stringify(vm.alldata[1]));
					vm.totalItemAlldata = Number(data.Count);
					vm.filterdata = [];
					//console.info("total semua data:" + JSON.stringify(vm.alldata.length));
					for (var i = 0; i < vm.totalItemAlldata; i++) {
						if (vm.alldata[i].categoryvendor == null || vm.alldata[i].categoryvendor == undefined) {
							vm.alldata[i].categoryvendor = " ";
						}
						if (vm.alldata[i].Code == null) {
							vm.alldata[i].Code = " ";
						}
						if (vm.alldata[i].SAPCode == null) {
							vm.alldata[i].SAPCode = " ";
						}

						if (vm.alldata[i].business != null) {
							if (vm.alldata[i].business.Name == null) {
								vm.jenisPerusahaan = " ";
							}
							else if (vm.alldata[i].business.Name != null) {
								vm.jenisPerusahaan = vm.alldata[i].business.Name;
							}
						}
						else if (vm.alldata[i].business == null) {
							vm.jenisPerusahaan = " ";
						}
						if (vm.alldata[i].LegalNumber == null) {
							vm.alldata[i].LegalNumber = " ";
						}
						//console.info("jenis" + JSON.stringify(vm.alldata[i].business.Name));

						var forExcel = {
							VendorCode: vm.alldata[i].VendorID,
							VendorName: vm.alldata[i].VendorName,
							EProcurementVendorCode: vm.alldata[i].Code,
							FoundedDate: UIControlService.getStrDate(vm.alldata[i].FoundedDate),
							RegistrationDate: UIControlService.getStrDate(vm.alldata[i].CreatedDate),
							SAPCode: vm.alldata[i].SAPCode,
							BusinessType: vm.jenisPerusahaan,
							Category: vm.alldata[i].categoryvendor
						}
						vm.filterdata.push(forExcel);
					}
					//console.info("dataForExcel:" + JSON.stringify(vm.filterdata));
					JSONToCSVConvertor(vm.filterdata, true);
					vm.flag = vm.cmbStatus;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Rekanan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});


		}



		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ShowLabel) {
			//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
			var arrData = JSONData;
			//console.info(arrData[0]);
			var CSV = '';

			//This condition will generate the Label/Header
			if (ShowLabel) {
				var row = "sep=," + '\n';

				//This loop will extract the label from 1st index of on array
				for (var index in arrData[0]) {

					//Now convert each value to string and comma-seprated
					row += index + ',';
				}

				row = row.slice(0, -1);
				//console.info(row);
				//append Label row with line break
				CSV += row + '\r\n';
				//console.info(CSV);
			}

			//1st loop is to extract each row
			for (var i = 0; i < arrData.length; i++) {
				var row = "";

				//2nd loop will extract each column and convert it in string comma-seprated
				for (var index in arrData[i]) {
					row += '"' + arrData[i][index] + '",';
				}

				row.slice(0, row.length - 1);

				//add a line break after each row
				CSV += row + '\r\n';
			}

			if (CSV == '') {
				alert("Invalid data");
				return;
			}

			//Generate a file name
			var fileName = "Data Rekanan";

			//Initialize file format you want csv or xls
			var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

			// Now the little tricky part.
			// you can use either>> window.open(uri);
			// but this will not work in some browsers
			// or you will not get the correct file extension    

			//this trick will generate a temp <a /> tag
			var link = document.createElement("a");
			link.href = uri;

			//set the visibility hidden so it will not effect on your web-layout
			link.style = "visibility:hidden";
			link.download = fileName + ".csv";

			//this part will append the anchor tag and remove it after automatic click
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}


		vm.loadContact = loadContact;
		function loadContact(vendorID, i) {
			vm.addressBranch = '';
			vm.addressMain = '';
			VerifikasiDataService.select({
				VendorID: vendorID
			}, function (reply) {
				if (reply.status === 200) {
					vm.verified = reply.data;
					for (var j = 0; j < vm.verified.length; j++) {
						if (vm.verified[j].VendorContactTypeID == 20) {
							if (vm.verified[j].Contact.Address.State.CountryID == 360) {
								if (vm.verified[j].Contact.Address.DistrictID == 5101 || vm.verified[j].Contact.Address.DistrictID == 5102 || vm.verified[j].Contact.Address.DistrictID == 5104 || vm.verified[j].Contact.Address.DistrictID == 5108) {
									vm.verifikasidata[i]["keterangan"] = "Local";
									///console.info("lokal");
								} else {

									vm.verifikasidata[i]["keterangan"] = "National"; //console.info("nas");

								}
							} else {
								vm.verifikasidata[i]["keterangan"] = "International"; //console.info("intern");
							}
						}
					}
					//console.info("kethmm" + JSON.stringify(vm.verifikasidata[i]));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}


		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.verifikasi.StartDate) {
				vm.verifikasi.StartDate = UIControlService.getStrDate(vm.verifikasi.StartDate);
			}
			if (vm.verifikasi.EndDate) {
				vm.verifikasi.EndDate = UIControlService.getStrDate(vm.verifikasi.EndDate);
			}
		};

		function convertToDate() {
			if (vm.verifikasi.StartDate) {
				vm.verifikasi.StartDate = new Date(Date.parse(vm.verifikasi.StartDate));
			}
			if (vm.verifikasi.EndDate) {
				vm.verifikasi.EndDate = new Date(Date.parse(vm.verifikasi.EndDate));
			}
		}

		vm.formUpdateSAPCode = formUpdateSAPCode;
		function formUpdateSAPCode(id, sapcode) {
			var data = {
				VendorID: id,
				SAPCode: sapcode
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/updateSAPCode.html',
				controller: 'UpdateSAPCodeCtrl',
				controllerAs: 'UpdateSAPCodeCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(vm.currentPage);
			});
		}

		function add(act, data, name) {
			var data = {
				TenderName: name,
				act: act,
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailActified.html',
				controller: 'FormActivedVendorCtrl',
				controllerAs: 'FrmActivedVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(vm.currentPage);
			});
		}

		function addVerifikasi(data) {
			vm.ViewModelVerified = {
				Status: vm.Status,
				Keyword: vm.nCompany,
				StartDate: vm.verifikasi.StartDate,
				EndDate: vm.verifikasi.EndDate,
				currentPage: vm.currentPage
			}
			console.info(vm.ViewModelVerified);
			localStorage.setItem('ViewModelVerified', JSON.stringify(vm.ViewModelVerified));
			$state.transitionTo('proses-verifikasi', { id: data.VendorID });
		}

		function listExpired() {
			var data = {
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/expired-license.html',
				controller: 'ExpiredLicenseController',
				controllerAs: 'ExpiredLicenseController',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(vm.currentPage);
			});
		}

		vm.editActive = editActive;
		function editActive(active, data) {
			var data = {
				act: active,
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailAktivasiVendor.html',
				controller: 'FormActiveVendorCtrl',
				controllerAs: 'FrmActiveVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(vm.currentPage);
			});
		}

		vm.sendToSAP = sendToSAP
		function sendToSAP(data) {
			UIControlService.loadLoading("MESSAGE.LOADING")
			VerifikasiDataService.sendToSAP({
				Code: data.Code
			}, function (reply) {
				if (reply.status === 200) {
					var data = reply.data
					UIControlService.unloadLoading();
					init()
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Rekanan" })
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.msg_growl("error", err.InnerException.InnerException.ExceptionMessage);
				UIControlService.unloadLoading()
			});
		}

		vm.approval = approval
		function approval() {
			$state.transitionTo('detail-approval-vendor');
		}

		vm.Detailapproval = Detailapproval;
		function Detailapproval(data) {
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailModalApproval.html',
				controller: 'DetailModalApprovalCtrl',
				controllerAs: 'DetailModalApprovalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(vm.currentPage);
			});

		}

		vm.saveSAP = saveSAP;
		function saveSAP() {
			//console.info("sss");
			VerifikasiDataService.saveSAPCode(vm.verifikasidata, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					init();
				} else {
					$.growl.error({ message: "Gagal akses API" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
		vm.deleteInactiveVendor = deleteInactiveVendor;
		function deleteInactiveVendor(data) {
			//console.info("sss");
			//console.info("param" + vm.vendorID);
			bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_DELETEINACTIVE') + data.VendorName + '?</h4>', function (res) {
				if (res) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					VerifikasiDataService.deleteInactiveVendor({ VendorID: data.VendorID }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_DELETEDATAINACTIVEVENDOR");
							init();
						} else {
							$.growl.error({ message: "Gagal mendapatkan data Rekanan" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
					//SocketService.emit("daftarRekanan");
				}
			});
		}
	}
})();
