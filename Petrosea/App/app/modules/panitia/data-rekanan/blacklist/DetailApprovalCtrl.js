﻿(function () {
    'use strict';

    angular.module("app").controller("DetailApprovalBlacklistCtrl", ctrl);
    ctrl.$inject = ['$filter','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BlacklistService', 'UIControlService', 'GlobalConstantService', '$state', '$uibModal'];
    /* @ngInject */
    function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService,
        BlacklistService, UIControlService, GlobalConstantService, $state, $uibModal) {
        var vm = this;
        vm.init = init;
        vm.aktifasi = "";
        vm.Keyword = "";
        vm.pageSize = 10;

        vm.listDropdown =
            [
                { Value: 0, Name: "SELECT.ALL" },
                { Value: 1, Name: "SELECT.APPROVED" },
                { Value: 2, Name: "SELECT.REJECTED" },
                { Value: 3, Name: "SELECT.ON_PROCESS" }
            ]
        vm.filter = vm.listDropdown[3];
        function init() {
            $translatePartialLoader.addPart('blacklist-data');
            jLoad(1);
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.listApproval = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            BlacklistService.selectApproval({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.Keyword,
                FilterType: vm.filter.Value
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listApproval = data.List;
                    vm.listApproval.forEach(function (data) {
                        data.ApprovalDate = UIControlService.convertDateTime(data.ApprovalDate);
                    })
                    vm.totalItems = Number(data.Count);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data ApprovalVendor" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.detail = detail;
        function detail(data, type) {
            vm.flagStatus = false;
            if (data.BlacklistType.Name === "BLACKLIST_TYPE_NO") vm.flagStatus = true;
                

            var data = {
                flagStatus: vm.flagStatus,
                act: false,
                type: type,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/blacklist/FormBlacklist.html',
                controller: 'FormBlacklistCtrl',
                controllerAs: 'frmBlacklistCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }


        vm.cancel = cancel;
        function cancel() {
            $state.transitionTo('blacklist-data');
        }

        vm.save = save;
        function save(data) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            var data = {
                act: false,
                ID: data.ID,
                LType: data.LType,
                BlacklistId: data.BlacklistId,
                BlacklistType: data.BlacklistType,
                VendorID: data.VendorID,
                VendorName: data.VendorName
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/blacklist/DetailBlacklistRejectVendorCtrl.html',
                controller: 'FormWhitelistCtrl',
                controllerAs: 'FormWhitelistCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();

            });
           
        }

        vm.approve = approve;
        function approve(data) {
            bootbox.confirm($filter('translate')('Apakah anda yakin menyetujui blacklist vendor ini ? '), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    BlacklistService.editApproval({
                        ID: data.ID,
                        flagUpdate: true,
                        LType: data.LType,
                        BlacklistId: data.BlacklistId,
                        BlacklistType: data.BlacklistType
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            init();
                        }
                        else {
                            return;
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                    });

                }
            });
        }
    }
})();
