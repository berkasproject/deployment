﻿//(function () {
//    'use strict';

//    angular.module("app")
//    .controller("detailAppPengaduanCtrl", ctrl);

//    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'WarningLetterService'];
//    /* @ngInject */
//    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, WarningLetterService) {

//        var vm = this;

//        vm.ComplaintID = item.ComplaintID;
//        vm.remark = "";

//        vm.init = init;

//        function init() {            
//            $translatePartialLoader.addPart('master-complaint');
//        };

//        vm.proceedSent = proceedSent;
//        function proceedSent() {
//            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_TO_PROC'), function (yes) {
//                if (yes) {
//                    WarningLetterService.stateSent({
//                        ComplaintID: vm.ComplaintID,
//                        Remark:vm.remark
//                    }, function (reply) {
//                        UIControlService.unloadLoading();
//                        if (reply.status === 200) {
//                            $uibModalInstance.close();
//                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_SEND_DRAFT'));                            
//                        } else {
//                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_SEND_DRAFT'));
//                            UIControlService.unloadLoading();
//                        }
//                    }, function (err) {
//                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_SEND_DRAFT'));
//                        UIControlService.unloadLoading();
//                    });
//                }
//            });
//        }

//        vm.cancel = cancel;
//        function cancel() {
//            $uibModalInstance.close();
//        };
//    }
//})();