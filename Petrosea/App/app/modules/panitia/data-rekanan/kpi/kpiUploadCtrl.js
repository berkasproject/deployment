(function () {
    'use strict';

    angular.module("app").controller("KPIUploadCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'RoleService', 'UIControlService', '$uibModal', 'VPKPIService', 'UploadFileConfigService', 'ExcelReaderService', 'UploaderService', '$timeout', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        RoleService, UIControlService, $uibModal, VPKPIService, UploadFileConfigService, ExcelReaderService, UploaderService, $timeout, GlobalConstantService) {
        var vm = this;
        var page_id = 141;
        vm.lettername = "";
        vm.warningletter = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        //vm.pageSize = 5;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.kata = new Kata("");
        vm.init = init;

        vm.checkboxDoctype = [];
        vm.fileUpload = [];
        vm.tglSekarang = UIControlService.getDateNow("");
        vm.isvalid = [];
        vm.isuploaded = [];
        vm.uploadedfiles = [];
        vm.listExcelDEL = [];
        vm.listExcelPOH = [];
        vm.listExcelMDR = [];
        vm.listExcelHdrItm = [];

        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.VPKPIID = 0;

        vm.load = "";


        function init() {
            $translatePartialLoader.addPart('vp-kpi');
            loadDocType();
            alldocs(1);
            //docsbyID(24);
            cfgFile();
        }


        vm.counter = 0;
        vm.onTimeout = function () {
            vm.counter = vm.counter + 20;
            mytimeout = $timeout(vm.onTimeout, 1000);
        }
        var mytimeout = $timeout(vm.onTimeout, 1000);

        vm.stop = function () {
            $timeout.cancel(mytimeout);
        }

        vm.detailUpload = detailUpload;
        function detailUpload(data) {
            //console.info("detail" + JSON.stringify(data));
            var senddata = {
                VPKPIID: data.VPKPIID,
                DocType: data.DocType
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/kpi/detailUpload.html',
                controller: 'DetailUploadVendorKPICtrl',
                controllerAs: 'DetailUploadVendorKPICtrl',
                resolve: { item: function () { return senddata; } }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        function cfgFile() {
            //UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.VENDORPERFORMANCE.KPI", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = [];
                    vm.list = response.data;
                    //console.info("cfgfile" + JSON.stringify(vm.list));
                    for (var i = 0; i < vm.list.length; i++) {
                        if (vm.list[i].Name == "xls" || vm.list[i].Name == "xlsx") vm.idUploadConfigs.push(vm.list[i]);

                    }
                    vm.idFileTypes = UIControlService.generateFilterStrings(vm.idUploadConfigs);
                    //console.info("idfiletypes" + JSON.stringify(vm.idFileTypes));
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("err", "MESSAGE.ERR_NOFILE");
                return
            }
            else {
                UIControlService.loadLoading("MESSAGE.LOADING_SAVE");
                if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
                }
            }
        }


        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        vm.cariwarningLetter = cariwarningLetter;
        function cariwarningLetter() {
            init();
        }


        vm.submitfile = submitfile;
        function submitfile() {
            //console.info("lenngth:" + vm.fileUpload.length);
            /*
            if (vm.checkboxDoctype.length < vm.docTypes.length) {
                UIControlService.msg_growl("warning", "Lengkapi checkbox agar data dapat diproses");
                return;
            }*/

            gotosave();
        }

        function gotosave() {
            for (var i = 0; i <= vm.docTypes.length - 1; i++) {
                //console.info("isvalid?" +i+ vm.isvalid[i]);
                if (vm.fileUpload[vm.docTypes[i].RefID] != undefined) {
                    if (vm.isvalid[vm.docTypes[i].RefID] == false) {
                        UIControlService.msg_growl("warning", "MESSAGE.NO_VALID");
                        return;
                    }
                    else {
                        if (vm.VPKPIID == 0) {
                            saveVPKPI();
                        }
                        else {
                            savedocs(vm.VPKPIID);
                        }
                        return;
                    }
                }
            }
            /*
            for (var i = 0; i <= vm.docTypes.length - 1; i++) {
                if (vm.fileUpload[i] === undefined) {
                    UIControlService.msg_growl("error", "MSG_NOFILE");
                    return;
                }

                if (UIControlService.validateFileType(vm.fileUpload[i], vm.idUploadConfigs)) {
                    upload(vm.fileUpload[i], vm.idFileSize, vm.idFileTypes, vm.tglSekarang);

                }
            }*/
            console.info("vpkpiID:" + vm.VPKPIID);
        }

        vm.saveVPKPI = saveVPKPI;
        function saveVPKPI() {
            VPKPIService.saveVPKPI({}, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.VPKPIID = reply.data;
                    console.info("vpkpiID:" + vm.VPKPIID);
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_ME2N");
                    savedocs(vm.VPKPIID);
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DOCS");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.selectUpload = selectUpload;
        function selectUpload(fileupload, doctype) {
            /*
            if (fileupload === undefined) {
                UIControlService.msg_growl("error", "MSG_NOFILE");
                return;
            }

            if (UIControlService.validateFileType(fileupload, vm.idUploadConfigs)) {
                upload(fileupload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);

            }*/
            ReadFile(fileupload, doctype, 1, 0);
        }

        function savedocs(vpkpiID) {
            //console.info("length:"+vm.docTypes.length);
            //console.info("dt" + JSON.stringify(vm.docTypes[vm.docTypes.length - 1].RefID));
            for (var i = 0; i <= vm.docTypes.length - 1; i++) {
                /*
                if (vm.fileUpload[vm.docTypes[i].RefID] === undefined) {
                    UIControlService.msg_growl("error", "MSG_NOFILE");
                    return;
                }*/
                if (vm.fileUpload[vm.docTypes[i].RefID] != undefined) {
                    if (UIControlService.validateFileType(vm.fileUpload[vm.docTypes[i].RefID], vm.idUploadConfigs)) {
                        //console.info("i: " + i);
                        ReadFile(vm.fileUpload[vm.docTypes[i].RefID], vm.docTypes[i].RefID, 2, vpkpiID); //simpan lalu save
                        upload(vm.fileUpload[vm.docTypes[i].RefID], vm.idFileSize, vm.idFileTypes, vm.tglSekarang, vpkpiID, vm.docTypes[i].RefID);
                        vm.fileUpload[vm.docTypes[i].RefID] = undefined;
                        vm.isuploaded[vm.docTypes[i].RefID] = true;
                        break;
                    }
                }
            }
            init();
            //window.location.reload();
        }


        function upload(file, config, filters, dates, VPKPIID, doctype) {
            //console.info("doctype:" + doctype);
            //console.info("file"+JSON.stringify(file));
            var size = config.Size;

            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.pathFile = url;
                        saveVPKPIDocs(url, fileName, VPKPIID, doctype);
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

        }


        function saveVPKPIDocs(url, filename, VPKPIID, doctype) {
            VPKPIService.saveVPKPIDocs({
                FileName: filename,
                DocUrl: url,
                VPKPIID: VPKPIID,
                DocType: doctype
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_ME2N");
                    //$state.transitionTo('master-rate');
                }
                /* geje
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE_DOCS");
                    return;
                }*/
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.hapus = hapus;
        function hapus(refid) {
            VPKPIService.hapus({
                Status: refid,
                column: vm.VPKPIID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.DEL_ME2N");
                    vm.isuploaded[refid] = false;
                    //$state.transitionTo('master-rate');
                }
                /* geje
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE_DOCS");
                    return;
                }*/
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }


        vm.ReadFile = ReadFile;
        function ReadFile(fileUpload, doctype, act, id) {
            //console.info("i" + i);
            ExcelReaderService.readExcel(fileUpload,
                function (reply) {
                    if (reply.status === 200) {
                        var excelContents = reply.data;
                        var sheet1 = excelContents[Object.keys(excelContents)[0]];
                        vm.list = [];
                        vm.list = sheet1;
                        if (doctype == 4350) { //ME2N
                            if (act == 1) {
                                if (sheet1[0].Column1 == 'Document Date' &&
                                    sheet1[0].Column2 == 'Purchasing Doc. Type' &&
                                    sheet1[0].Column3 == 'Purchasing Document' &&
                                    sheet1[0].Column4 == 'Vendor/supplying plant' &&
                                    sheet1[0].Column5 == 'Item' &&
                                    sheet1[0].Column6 == 'Material' &&
                                    sheet1[0].Column7 == 'Short Text' &&
                                    sheet1[0].Column8 == 'Storage Location' &&
                                    sheet1[0].Column9 == 'Deletion Indicator' &&
                                    sheet1[0].Column10 == 'Req. Tracking Number' &&
                                    sheet1[0].Column11 == 'Purchasing Group' &&
                                    sheet1[0].Column12 == 'Release status') {
                                    for (var j = 1; j <= sheet1.length - 1; j++) {
                                        if (sheet1[j].Column11 == null || sheet1[j].Column3 == null) {
                                            UIControlService.msg_growl("error", "MESSAGE.ME2N_NOVALID");
                                            vm.isvalid[doctype] = false;
                                            return;
                                        }
                                    }
                                    UIControlService.msg_growl("success", "MESSAGE.ME2N_VALID");
                                    vm.isvalid[doctype] = true;
                                }
                                else {
                                    UIControlService.msg_growl("error", "MESSAGE.ME2N_NOVALID");
                                }
                            }
                        }
                        else if (doctype == 4351) { //ME80FN-DelSch
                            if (act == 1) {
                                if (sheet1[0].Column1 == 'Purchasing Document' &&
                                    sheet1[0].Column2 == 'Item' &&
                                    sheet1[0].Column3 == 'Vendor' &&
                                    sheet1[0].Column4 == 'Material' &&
                                    sheet1[0].Column5 == 'Short Text' &&
                                    sheet1[0].Column6 == 'Scheduled Quantity' &&
                                    sheet1[0].Column7 == 'Order Unit' &&
                                    sheet1[0].Column8 == 'Qty Delivered' &&
                                    sheet1[0].Column9 == 'Purchase Order Date' &&
                                    sheet1[0].Column10 == 'Delivery Date' &&
                                    sheet1[0].Column11 == 'Stat.-Rel. Del. Date') {
                                    /*
                                    for (var i = 1; i <= sheet1.length - 1; i++) {
                                        if (sheet1[i].Column11 == null || sheet1[i].Column3 == null) {
                                            UIControlService.msg_growl("error", "Dokumen yang diupload tidak valid. Silakan mengunggah kembali");
                                            return;
                                        }
                                    }*/
                                    UIControlService.msg_growl("success", "MESSAGE.DEL_VALID");
                                    vm.isvalid[doctype] = true;
                                }
                                else {
                                    vm.isvalid[doctype] = false;
                                    UIControlService.msg_growl("error", "MESSAGE.DEL_NOVALID");
                                }
                            }
                            else if (act == 2) {
                                //console.info("dellength" + JSON.stringify(sheet1.length));
                                vm.listExcelDEL = [];
                                for (var j = 1; j <= sheet1.length - 1; j++) {
                                    var simpan = {
                                        VPKPIID: vm.VPKPIID,
                                        PurchasingDoc: sheet1[j].Column1,
                                        Item: sheet1[j].Column2,
                                        VendorSAP: sheet1[j].Column3,
                                        Material: sheet1[j].Column4,
                                        ShortText: sheet1[j].Column5,
                                        ScheduledQty: sheet1[j].Column6,
                                        OrderUnit: sheet1[j].Column7,
                                        QtyDelivered: sheet1[j].Column8,
                                        PODate: sheet1[j].Column9,
                                        DeliveryDate: sheet1[j].Column10,
                                        StartRelDelDate: sheet1[j].Column11
                                    }
                                    vm.listExcelDEL.push(simpan);

                                }
                                saveSAPDEL(vm.listExcelDEL, id);
                            }
                        }
                        else if (doctype == 4352) { //ME80FN-POH
                            if (act == 1) {
                                if (sheet1[0].Column1 == 'Purchasing Document' &&
                                    sheet1[0].Column2 == 'Item' &&
                                    sheet1[0].Column3 == 'Material Doc. Year' &&
                                    sheet1[0].Column4 == 'Material Document' &&
                                    sheet1[0].Column5 == 'Material Doc.Item' &&
                                    sheet1[0].Column6 == 'Movement Type' &&
                                    sheet1[0].Column7 == 'Posting Date' &&
                                    sheet1[0].Column8 == 'Quantity' &&
                                    sheet1[0].Column9 == 'Order Unit' &&
                                    sheet1[0].Column10 == 'Quantity in OPUn' &&
                                    sheet1[0].Column11 == 'Order Price Unit' &&
                                    sheet1[0].Column12 == 'Amount in LC' &&
                                    sheet1[0].Column13 == 'Local currency' &&
                                    sheet1[0].Column14 == 'Amount' &&
                                    sheet1[0].Column15 == 'Currency') {
                                    /*
                                    for (var i = 1; i <= sheet1.length - 1; i++) {
                                        if (sheet1[i].Column11 == null || sheet1[i].Column3 == null) {
                                            UIControlService.msg_growl("error", "Dokumen yang diupload tidak valid. Silakan mengunggah kembali");
                                            return;
                                        }
                                    }*/
                                    UIControlService.msg_growl("success", "MESSAGE.POH_VALID");
                                    vm.isvalid[doctype] = true;
                                }
                                else {
                                    vm.isvalid[doctype] = false;
                                    UIControlService.msg_growl("error", "MESSAGE.POH_NOVALID");
                                }
                            }
                            else if (act == 2) {
                                //console.info("pohlength" + JSON.stringify(sheet1));
                                vm.listExcelPOH = [];
                                for (var j = 1; j <= sheet1.length - 1; j++) {
                                    var simpan = {
                                        VPKPIID: vm.VPKPIID,
                                        PurchasingDoc: sheet1[j].Column1,
                                        Item: sheet1[j].Column2,
                                        PostingDate: sheet1[j].Column7,
                                        Quantity: sheet1[j].Column8,
                                        OrderUnit: sheet1[j].Column9
                                    }
                                    vm.listExcelPOH.push(simpan);

                                }
                                saveSAPPOH(vm.listExcelPOH, id);
                            }
                        }
                        else if (doctype == 4353) { //MKVZ
                            if (act == 1) {
                                if (sheet1[0].Column1 == 'Account group' &&
                                    sheet1[0].Column2 == 'Vendor' &&
                                    sheet1[0].Column3 == 'Name of vendor' &&
                                    sheet1[0].Column4 == 'Street' &&
                                    sheet1[0].Column5 == 'City' &&
                                    sheet1[0].Column6 == 'Country' &&
                                    sheet1[0].Column7 == 'Postal Code' &&
                                    sheet1[0].Column8 == 'Telephone' &&
                                    sheet1[0].Column9 == 'Salesperson' &&
                                    sheet1[0].Column10 == 'Search term' &&
                                    sheet1[0].Column11 == 'Order currency' &&
                                    sheet1[0].Column12 == 'Terms of Payment' &&
                                    sheet1[0].Column13 == 'Purch. Organization' &&
                                    sheet1[0].Column14 == 'Purch. Org. Descr.' &&
                                    sheet1[0].Column15 == 'Central purchasing block' &&
                                    sheet1[0].Column16 == 'Block function' &&
                                    sheet1[0].Column17 == 'Central deletion flag') {
                                    /*
                                    for (var i = 1; i <= sheet1.length - 1; i++) {
                                        if (sheet1[i].Column11 == null || sheet1[i].Column3 == null) {
                                            UIControlService.msg_growl("error", "Dokumen yang diupload tidak valid. Silakan mengunggah kembali");
                                            return;
                                        }
                                    }*/
                                    UIControlService.msg_growl("success", "MESSAGE.MKVZ_VALID");
                                    vm.isvalid[doctype] = true;
                                }
                                else {
                                    vm.isvalid[doctype] = false;
                                    UIControlService.msg_growl("error", "MESSAGE.MKVZ_NOVALID");
                                }
                            }
                        }
                        else if (doctype == 4354) { //QM11(MDR)
                            if (act == 1) {
                                if (sheet1[0].Column1 == 'Exception' &&
                                    sheet1[0].Column2 == 'Notification' &&
                                    sheet1[0].Column3 == 'Notification date' &&
                                    sheet1[0].Column4 == 'Notification Status' &&
                                    sheet1[0].Column5 == 'Description' &&
                                    sheet1[0].Column6 == 'Created by' &&
                                    sheet1[0].Column7 == 'Created on' &&
                                    sheet1[0].Column8 == 'Completion by date' &&
                                    sheet1[0].Column9 == 'Code group' &&
                                    sheet1[0].Column10 == 'Coding Code' &&
                                    sheet1[0].Column11 == 'Purchasing Group' &&
                                    sheet1[0].Column12 == 'Vendor' &&
                                    sheet1[0].Column13 == 'Purchasing Document' &&
                                    sheet1[0].Column14 == 'Item purchasing doc.' &&
                                    sheet1[0].Column15 == 'Material' &&
                                    sheet1[0].Column16 == 'Material Description' &&
                                    sheet1[0].Column17 == 'Complaint quantity' &&
                                    sheet1[0].Column18 == 'Ref. quantity' &&
                                    sheet1[0].Column19 == 'Unit of measure' &&
                                    sheet1[0].Column20 == 'Priority text') {

                                    for (var j = 1; j <= sheet1.length - 1; j++) {
                                        if (sheet1[j].Column11 == null || sheet1[j].Column13 == null) {
                                            UIControlService.msg_growl("error", "MESSAGE.DOC_NOVALID");
                                            vm.isvalid[doctype] = false;
                                            return;
                                        }
                                    }
                                    UIControlService.msg_growl("success", "MESSAGE.QM11_VALID");
                                    vm.isvalid[doctype] = true;
                                }
                                else {
                                    vm.isvalid[doctype] = false;
                                    UIControlService.msg_growl("error", "MESSAGE.QM11_NOVALID");
                                }
                            }
                            else if (act == 2) {
                                //console.info("mdrlength" + JSON.stringify(sheet1));
                                vm.listExcelMDR = [];
                                for (var j = 1; j <= sheet1.length - 1; j++) {
                                    var simpan = {
                                        VPKPIID: vm.VPKPIID,
                                        Notification: sheet1[j].Column2,
                                        NotificationDate: sheet1[j].Column3,
                                        Description: sheet1[j].Column5,
                                        PurchasingGroup: sheet1[j].Column11,
                                        VendorSAP: sheet1[j].Column12,
                                        PurchasingDoc: sheet1[j].Column13,
                                        ItemPurchDoc: sheet1[j].Column14,
                                        Material: sheet1[j].Column15
                                    }
                                    vm.listExcelMDR.push(simpan);

                                }
                                saveSAPMDR(vm.listExcelMDR, id);
                            }
                        }
                        else if (doctype == 4445) {
                            //console.info("hdritm");
                            if (act == 1) {
                                UIControlService.msg_growl("success", "MESSAGE.HDR_VALID");
                                vm.isvalid[doctype] = true;
                            }
                            else if (act == 2) {
                                vm.listExcelHdrItm = [];
                                for (var j = 1; j <= sheet1.length - 1; j++) {
                                    var simpan = {
                                        VPKPIID: vm.VPKPIID,
                                        PurchasingDoc: sheet1[j].Column1,
                                        Item: sheet1[j].Column2,
                                        VendorSAP: sheet1[j].Column3,
                                        MaterialGroup: sheet1[j].Column4,
                                        Material: sheet1[j].Column5,
                                        PurchOrg: sheet1[j].Column6,
                                        PurchasingGroup: sheet1[j].Column7,
                                        Plant: sheet1[j].Column8,
                                        StorageLoc: sheet1[j].Column9,
                                        DocumentDate: sheet1[j].Column10,
                                        OrderQty: sheet1[j].Column11,
                                        OrderUnit: sheet1[j].Column12,
                                        NetOrderValue: sheet1[j].Column13,
                                        Currency: sheet1[j].Column14,
                                        OrderPriceUnit: sheet1[j].Column15,
                                        PurchasingDocType: sheet1[j].Column16,
                                        SupplyingPlant: sheet1[j].Column17,
                                        ShortText: sheet1[j].Column18,
                                        GRBasedInvVerif: sheet1[j].Column19,
                                        PurchasingDocCategory: sheet1[j].Column20
                                    }
                                    vm.listExcelHdrItm.push(simpan);

                                }
                                saveSAPHdrItm(vm.listExcelHdrItm, id);
                            }
                        }
                    }
                });
        }

        function saveSAPDEL(arrdel, vpkpiid) {
            UIControlService.loadLoading("");
            VPKPIService.saveSAPSPdel(arrdel, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 || reply.status == 204) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_DEL");
                    //$state.transitionTo('master-rate');
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
                    return;
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
            });
            /*
            for (var d = 0; d <= arrdel.length - 1; d++) {
                UIControlService.unloadLoadingModal();
                vm.load = d;
                if (arrdel[d].Material == null) {
                    arrdel[d].Material = " ";
                }
                //console.info("arrdel" + JSON.stringify(arrdel));
                VPKPIService.saveSAPSPdel({
                    VPKPIID: vpkpiid,
                    PurchasingDoc: arrdel[d].PurchasingDocument,
                    Item: arrdel[d].Item,
                    VendorSAP: arrdel[d].VendorSAP,
                    Material: arrdel[d].Material,
                    ShortText: arrdel[d].ShortText,
                    ScheduledQty: arrdel[d].ScheduledQuantity,
                    OrderUnit: arrdel[d].OrderUnit,
                    QtyDelivered: arrdel[d].QtyDelivered,
                    PODate: arrdel[d].PurchaseOrderDate,
                    DeliveryDate: arrdel[d].DeliveryDate,
                    StartRelDelDate: arrdel[d].StartRelDelDate
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200 || reply.status == 204) {
                        UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                        //$state.transitionTo('master-rate');
                    }
                    else {
                        UIControlService.msg_growl("error", "MSG_ERR_SAVE_SAP");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                    UIControlService.unloadLoadingModal();
                });
            }*/
        }

        function saveSAPHdrItm(arrHdrItm, vpkpiid) {
            UIControlService.loadLoading("");
            VPKPIService.saveSAPSPhdritm(arrHdrItm, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 || reply.status == 204) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_HDR");
                    //$state.transitionTo('master-rate');
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
                    return;
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
            });
            /*
            for (var d = 0; d <= arrHdrItm.length - 1; d++) {
                //console.info("d hdr" + arrHdrItm[d].PurchasingDoc);
                VPKPIService.saveSAPSPhdritm({
                    VPKPIID: vpkpiid,
                    PurchasingDoc: arrHdrItm[d].PurchasingDoc,
                    Item: arrHdrItm[d].Item,
                    VendorSAP: arrHdrItm[d].VendorSAP,
                    MaterialGroup: arrHdrItm[d].MaterialGroup,
                    Material: arrHdrItm[d].Material,
                    PurchOrg: arrHdrItm[d].PurchOrg,
                    PurchasingGroup: arrHdrItm[d].PurchasingGroup,
                    Plant: arrHdrItm[d].Plant,
                    StorageLoc: arrHdrItm[d].StorageLoc,
                    DocumentDate: arrHdrItm[d].DocumentDate,
                    OrderQty: arrHdrItm[d].OrderQty,
                    OrderUnit: arrHdrItm[d].OrderUnit,
                    NetOrderValue: arrHdrItm[d].NetOrderValue,
                    Currency: arrHdrItm[d].Currency,
                    OrderPriceUnit: arrHdrItm[d].OrderPriceUnit,
                    PurchasingDocType: arrHdrItm[d].PurchasingDocType,
                    SupplyingPlant: arrHdrItm[d].SupplyingPlant,
                    ShortText: arrHdrItm[d].ShortText,
                    GRBasedInvVerif: arrHdrItm[d].GRBasedInvVerif,
                    PurchasingDocCategory: arrHdrItm[d].PurchasingDocCategory
                }, function (reply) {
                    //UIControlService.unloadLoadingModal();
                    if (reply.status === 200 || reply.status == 204) {
                        UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                        //$state.transitionTo('master-rate');
                    }
                    else {
                        UIControlService.msg_growl("error", "MSG_ERR_SAVE_SAP");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                    UIControlService.unloadLoadingModal();
                });
            }*/
        }
        function saveSAPPOH(arrpoh, vpkpiid) {
            UIControlService.loadLoading("");
            VPKPIService.saveSAPSPpoh(arrpoh, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 || reply.status == 204) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_POH");
                    //$state.transitionTo('master-rate');
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
                    return;
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
            });
            //console.info("arrpoh" + JSON.stringify(arrpoh));
            /*
            for (var d = 0; d <= arrpoh.length - 1; d++) {
                //console.info("poh data ke" + d + " :" + JSON.stringify(arrpoh[d]));
                VPKPIService.saveSAPSPpoh({
                    VPKPIID: vpkpiid,
                    PurchasingDoc: arrpoh[d].PurchasingDocument,
                    Item: arrpoh[d].Item,
                    PostingDate: arrpoh[d].PostingDate,
                    Quantity: arrpoh[d].Quantity,
                    OrderUnit: arrpoh[d].OrderUnit
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200 || reply.status == 204) {
                        UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                        //$state.transitionTo('master-rate');
                    }
                    else {
                        UIControlService.msg_growl("error", "MSG_ERR_SAVE_SAP");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                    UIControlService.unloadLoadingModal();
                });
            }*/
        }
        function saveSAPMDR(arrmdr, vpkpiid) {
            UIControlService.loadLoading("");
            VPKPIService.saveSAPSPmdr(arrmdr, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 || reply.status == 204) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_QM11");
                    //$state.transitionTo('master-rate');
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
                    return;
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
            });
            //console.info("arrmdr" + JSON.stringify(arrmdr));
            /*
            for (var d = 0; d <= arrmdr.length - 1; d++) {

                VPKPIService.saveSAPSPmdr({
                    VPKPIID: vpkpiid,
                    Notification: arrmdr[d].Notification,
                    NotificationDate: arrmdr[d].NotificationDate,
                    Description: arrmdr[d].Description,
                    PurchasingGroup: arrmdr[d].PurchasingGroup,
                    VendorSAP: arrmdr[d].VendorSAP,
                    PurchasingDoc: arrmdr[d].PurchasingDocument,
                    ItemPurchDoc: arrmdr[d].ItemPurchasingDocument,
                    Material: arrmdr[d].Material
                }, function (reply) {
                    //UIControlService.unloadLoadingModal();
                    if (reply.status === 200 || reply.status == 204) {
                        UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                        //$state.transitionTo('master-rate');
                    }
                    else {
                        UIControlService.msg_growl("error", "MSG_ERR_SAVE_SAP");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                    UIControlService.unloadLoadingModal();
                });
            }*/
        }

        function savedataSAP(arrsave, vpkpiid) {
            for (var d = 0; d <= arrsave.length - 1; d++) {

                VPKPIService.saveSAPSP({
                    VPKPIID: vpkpiid,
                    PurchasingDoc: arrsave[d].PurchasingDocument,
                    Item: arrsave[d].Item,
                    VendorSAP: arrsave[d].Vendor,
                    Material: arrsave[d].Material,
                    ShortText: arrsave[d].ShortText,
                    ScheduledQty: arrsave[d].ScheduledQuantity,
                    OrderUnit: arrsave[d].OrderUnit,
                    QtyDelivered: arrsave[d].QtyDelivered,
                    PODate: arrsave[d].PurchaseOrderDate,
                    DeliveryDate: arrsave[d].DeliveryDate,
                    StartRelDelDate: arrsave[d].StartRelDelDate,
                    PostingDate: arrsave[d].PostingDate,
                    Quantity: arrsave[d].Quantity,
                    Notification: arrsave[d].Notification,
                    PurchasingGroup: arrsave[d].PurchasingGroup,
                    ItemPurchDoc: arrsave[d].ItemPurchasingDoc
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200 || reply.status == 204) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_MKVZ");
                        //$state.transitionTo('master-rate');
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SAP");
                    UIControlService.unloadLoadingModal();
                });
            }
            //docsbyID(vpkpiid);
        }

        vm.loadDocType = loadDocType;
        function loadDocType() {
            VPKPIService.docTypes({
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.docTypes = reply.data;
                    //console.info("docTypes" + JSON.stringify(vm.docTypes));
                    for (var i = vm.docTypes[0].RefID; i <= vm.docTypes[vm.docTypes.length - 1].RefID; i++) {
                        //vm.checkboxDoctype[i] = false;
                        vm.isvalid[i] = false;
                        if (vm.VPKPIID == 0) {
                            vm.isuploaded[i] = false;
                        }
                    }

                    //console.info("isuploaded" + JSON.stringify(vm.isuploaded));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data document types" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.docsbyID = docsbyID;
        function docsbyID(vpkpiID) {
            //console.info("vpkpiiduf:" + vpkpiID);
            VPKPIService.vpkpidocsByID({
                VPKPIID: vpkpiID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.uploadedfiles = reply.data;
                    //console.info("uf:" + JSON.stringify(vm.uploadedfiles));

                } else {
                    $.growl.error({ message: "Gagal mendapatkan data document types" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.alldocs = alldocs;
        function alldocs(current) {
            //console.info("vpkpiiduf:" + vpkpiID);
            vm.currentPage = current;
            VPKPIService.vpkpidocs({
                Limit: vm.pageSize,
                Offset: vm.pageSize * (vm.currentPage - 1),
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.uploadedfiles = reply.data;
                    vm.count = reply.data.Count;
                    //console.info("uf:" + JSON.stringify(vm.uploadedfiles));

                } else {
                    $.growl.error({ message: "Gagal mendapatkan data document types" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}


