(function () {
    'use strict';

    angular.module("app").controller("KPICtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'RoleService', 'UIControlService', '$uibModal', '$state','VPKPIService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        RoleService, UIControlService, $uibModal, $state, VPKPIService) {
        var vm = this;
        vm.totalItems = 10;
        vm.currentPage = 1;
        vm.init = init;
        vm.keyword = "";
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        function init() {
          $translatePartialLoader.addPart('vp-kpi');
           // UIControlService.loadLoading("Silahkan Tunggu...");
           // jLoad(1);
            supplierPerformance(1);
        }

        vm.formupload = function () {
            $state.transitionTo('upload-sap-supplierperformance');
        };

        vm.supplierPerformance = supplierPerformance;
        function supplierPerformance(current) {
            UIControlService.unloadLoading("");
            vm.currentPage = current;
            VPKPIService.supplierPerformance({
                Limit: vm.pageSize,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Keyword: vm.keyword
            }, function (reply) {
                UIControlService.unloadLoading("");
                if (reply.status === 200) {
                    vm.supplierPerform = reply.data.List;
                    vm.totalItems = reply.data.Count;
                    //console.info("sp:" + JSON.stringify(vm.totalItems));

                } else {
                    $.growl.error({ message: "Gagal mendapatkan data document types" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.countScore= countScore;
        function countScore(data) {
            console.info("parm"+data.VendorID);
            VPKPIService.countScore({
                column: data.VendorID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.lastScore = reply.data;
                    //console.info("lastScore:" + JSON.stringify(vm.lastScore));
                    window.location.reload();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
            });
        };

        vm.sinkronisasi = sinkronisasi;
        function sinkronisasi() {
            VPKPIService.sinkronisasiVendor(function (reply) {
                if (reply.status == 200) {
                    vm.jumlahvendor = reply.data;
                    //console.info("jumlahvendor:" + JSON.stringify(vm.jumlahvendor));
                    //init();
                    window.location.reload();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
            });
        };

        vm.viewHistory = viewHistory;
        function viewHistory(data) {
            //console.info("data:" + JSON.stringify(data));
            var senddata = {
                VendorID:data.VendorID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/kpi/scoreHistory.html',
                controller: 'ScoreHistoryVendorKPICtrl',
                controllerAs: 'ScoreHistoryVendorKPICtrl',
                resolve: { item: function () { return senddata; } }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }


    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

