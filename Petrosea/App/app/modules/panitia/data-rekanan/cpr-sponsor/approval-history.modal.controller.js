﻿(function () {
    'use strict';

    angular.module("app")
    .controller("cprSponsorAppHistoryCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'VPCPRSponsorService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, VPCPRSponsorService) {

        var vm = this;

        vm.ContractName = item.ContractName;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            vm.list = [];
            UIControlService.loadLoadingModal("");
            VPCPRSponsorService.getapprovalhistories({
                VPCPRDataId: item.VPCPRDataId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.list = reply.data;
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APP'));
                UIControlService.unloadLoadingModal();
            });
        }
     
        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.close();
        };
    }
})();