﻿(function () {
    'use strict';

    angular.module("app")
    .controller("cprSponsorModal", ctrl);

    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCPRSponsorService', 'UIControlService', 'item', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, VPCPRSponsorService, UIControlService, item, GlobalConstantService) {

        var vm = this;
        vm.VPCPRDataId = item.VPCPRDataId;
        vm.type = item.IsVhsCpr;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        var kr = [];
        var krLv1 = [];
        var krLv2 = [];
        var krLv3 = [];
        var vendorName = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");

            VPCPRSponsorService.selectbyid({
                VPCPRDataId: vm.VPCPRDataId
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vpdata = reply.data[0];
                    vendorName = vm.vpdata.VendorName;
                    loadDocs();

                    for (var i = 0; i < vm.vpdata.VPCPRDataDetails.length; i++) {
                        kr.push(vm.vpdata.VPCPRDataDetails[i]);
                    }

                    for (var i = 0; i < kr.length; i++) {
                        if (kr[i].Level === 1) {
                            krLv1.push(kr[i]);
                        }
                        else if (kr[i].Level === 2) {
                            krLv2.push(kr[i]);
                        }
                        else if (kr[i].Level === 3) {
                            krLv3.push(kr[i]);
                        }
                    }

                    for (var i = 0; i < krLv2.length; i++) {
                        krLv2[i].sub = [];
                        for (var j = 0; j < krLv3.length; j++) {
                            if (krLv3[j].Parent === krLv2[i].CriteriaId) {
                                krLv2[i].sub.push(krLv3[j]);
                            }
                        }
                    }

                    for (var i = 0; i < krLv1.length; i++) {
                        krLv1[i].sub = [];
                        for (var j = 0; j < krLv2.length; j++) {
                            if (krLv2[j].Parent === krLv1[i].CriteriaId) {
                                krLv1[i].sub.push(krLv2[j]);
                            }
                        }
                    }

                    vm.kriteria = krLv1;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
            });
        };

        function loadDocs() {
            VPCPRSponsorService.selectDocsCPRId({
                VPCPRDataId: vm.VPCPRDataId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoading();
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();