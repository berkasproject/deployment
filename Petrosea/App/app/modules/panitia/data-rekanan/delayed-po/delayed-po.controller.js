﻿(function () {
    'use strict';

    angular.module("app").controller("DelayedPOController", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        '$state', 'UIControlService', 'GlobalConstantService', '$uibModal', '$stateParams', 'DelayedPOService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService,
        $state, UIControlService, GlobalConstantService, $uibModal, $stateParams, DelayedPOService) {
        var vm = this;

        var srcText = '';
        var column = "1";

        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("delayed-po");
            loadAwal();
        };

        vm.cari = cari;
        function cari(parSrcText, parColumn) {
            srcText = parSrcText;
            column = parColumn;
            vm.pageNumber = 1;
            loadAwal();
        }

        vm.loadAwal = loadAwal;
        function loadAwal() {
            UIControlService.loadLoading("");
            DelayedPOService.Select({
                Keyword: srcText,
                column: column,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.hdritmList = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        /*
        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            loadAwal();
        };
        */

        vm.createWL = createWL;
        function createWL(hdritm) {
            bootbox.confirm($filter("translate")("CONFIRM_CREATE_WL"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    DelayedPOService.CreateWL({
                        VendorSAP: hdritm.VendorSAP,
                        PurchasingDoc: hdritm.PurchasingDoc
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", 'MESSAGE.SUCC_CREATE_WL');
                        } else {
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_CREATE_WL');
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_CREATE_WL');
                        if (err.Message.substr(0, 4) === 'ERR_') {
                            UIControlService.msg_growl("error", 'MESSAGE.' + err.Message);
                        }
                    });
                }
            });
        };

        vm.createWLAll = createWLAll;
        function createWLAll() {
            bootbox.confirm($filter("translate")("CONFIRM_CREATE_WL_ALL"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    DelayedPOService.CreateWLAll({
                        Keyword: srcText,
                        column: column,
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        loadAwal();
                        UIControlService.msg_growl("notice", 'MESSAGE.SUCC_CREATE_WL');
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_CREATE_WL');
                    });
                }
            });
        };

        vm.detailDels = detailDels;
        function detailDels(hdritm) {
            var item = {
                VendorSAP: hdritm.VendorSAP,
                PurchasingDoc: hdritm.PurchasingDoc,
                VendorName: hdritm.VendorName,
                RFQCode: hdritm.RFQCode
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/delayed-po/detail-del.modal.html',
                controller: 'DetailDelayedDeliveryCtrl',
                controllerAs: 'detdelCtrl',
                resolve: { item: function () { return item; } }
            });
        }
    }
})();