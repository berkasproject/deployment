﻿(function () {
    'use strict';

    angular.module("app").controller("WarningLetterAppCtrl", ctrl);

    ctrl.$inject = ['$filter','Excel','$timeout','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, Excel, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService, WarningLetterService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        var page_id = 141;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.jLoad = jLoad;
        vm.wlApp = [];
        vm.searchBy = "1";
        vm.search = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-warningLetter');            
            jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            cekRole();
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            WarningLetterService.selectWLApproval({
                Offset:  vm.maxSize * (vm.currentPage - 1),
                Limit: vm.maxSize,
                Keyword: vm.search,
                column: vm.searchBy
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;                  
                    vm.wlApp = data.List;
                    vm.count = data.Count;
                    vm.totalItems = Number(data.Count);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DATA_APPROVAL'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        }
        vm.cariWLApproval = cariWLApproval;
        function cariWLApproval() {
            init();
        }
        vm.cekRole = cekRole;
        function cekRole() {
            WarningLetterService.cekRole(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.role = reply.data;
                    console.info("role: "+ vm.role);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_ROLE'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        };


        //Detail Warning Letter
        vm.detailWarningLetter = detailWarningLetter;
        function detailWarningLetter(data) {            
            console.info("into wl template");
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter-approval/detailTemplateWL.html',
                controller: 'detailTemplateCtrl',
                controllerAs: 'detailTemplateCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            })
        }

        //Detail CPR
        vm.lihatDetailCPR = lihatDetailCPR;
        function lihatDetailCPR(data) {
            if (data.Type == 2) {
                var lempar = {
                    flagRole: 0,
                    VPCPRDataId: data.VPCPRDataID,
                    IsVhsCpr: 1
                };
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-list.modal.html?v=1.000002',
                    controller: 'cprListModal',
                    controllerAs: 'cprListModal',
                    resolve: {
                        item: function () {
                            return lempar;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    //loadAwal();
                });
            } else if (data.Type == 3) {
                var lempar = {
                    flagRole: 0,
                    VPVHSDataId: data.VPVHSDataID,
                    IsVhsCpr: 2
                };
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-list.modal.html?v=1.000002',
                    controller: 'vhsListModal',
                    controllerAs: 'vhsListModal',
                    resolve: {
                        item: function () {
                            return lempar;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    //loadAwal();
                });
            }
        }

        //Detail List Approval / Reject Process
        vm.detailApproval = detailApproval;
        function detailApproval(dt, data, approvalType) {
            var item = {
                WLID: data.WarningLetterID,
                flag: data.flagEmp,
                Status: dt,
                approvalType: approvalType
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter-approval/detailApproval.modal.html?v=1.000002',
                controller: 'detailAppWLApprovalCtrl',
                controllerAs: 'detailAppWLCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        // Approve by L1 Procurement Support
        /*
        vm.stateApproveByL1Support = stateApproveByL1Support;
        function stateApproveByL1Support(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L1_SUPP'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL1Support({
                        LetterID: data.WarningLetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        */

        // Approve by Approval PM
        /*
        vm.stateApproved = stateApproved;
        function stateApproved(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApproved({
                        LetterID: data.WarningLetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        */

        // Approve by L1 Procurement
        /*
        vm.stateApproveByL1 = stateApproveByL1;
        function stateApproveByL1(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L1'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL1({
                        LetterID: data.WarningLetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        */

        // Approve by L3 Dir. Of SCM
        /*
        vm.stateApproveByL3 = stateApproveByL3;
        function stateApproveByL3(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L3'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL3({
                        LetterID: data.WarningLetterID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.jLoad(1);
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        */
    }
})();
