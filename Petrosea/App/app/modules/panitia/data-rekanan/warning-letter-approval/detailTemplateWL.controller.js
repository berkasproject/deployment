﻿(function () {
    'use strict';

    angular.module("app").controller("detailTemplateCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'RoleService', 'UIControlService', 'item', '$uibModal', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, WarningLetterService,
        RoleService, UIControlService, item, $uibModal, $uibModalInstance) {
        var vm = this;
        var page_id = 141;

        vm.isAdd = item.act;
        vm.Area;
        vm.LetterID;
        vm.Description = "";
        vm.action = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        
        vm.init = init;
        vm.warningLetter = {};
        vm.isCalendarOpened = [false, false, false, false];

        function init() {            
            if (item.item.WarningLetter.Type == 1)
                vm.GoodOrService = 'Pengaduan';
            else if (item.item.WarningLetter.Type == 2)
                vm.GoodOrService = 'CPR';
            else if (item.item.WarningLetter.Type == 3)
                vm.GoodOrService = 'VHS';
            else if (item.item.WarningLetter.Type == 4)
                vm.GoodOrService = 'PO';

            vm.CreatedDate = item.item.CreatedDate;
            vm.VendorName = item.item.Vendor.VendorName;
            vm.WarningType = item.item.ComplainType;
            vm.LetterType = item.item.LetterType;
            vm.StartDate = item.item.StartDate;
            vm.EndDate = item.item.EndDate;
            vm.Area = item.item.Area;
            vm.Description = item.item.Description;
            vm.ReporterName = item.item.ReporterName;
            vm.Reporter = item.item.DepartmentName;
            vm.Status = item.item.AppStatus.Name;
            if (item.item.department == null) {
                vm.DeptCode = "-";
            }
            else {
                vm.DeptCode = item.item.department.department.DepartmentCode;
            }

            vm.TemplateDoc = item.item.WarningLetter.TemplateDoc;
            vm.businessType = item.item.Vendor.business.Name;
            
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };      


    }
})();