﻿(function () {
    'use strict';

    angular.module("app").controller("VPVHSReminderCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPVHSDataService',
        '$state', 'UIControlService', 'GlobalConstantService','$uibModal', '$stateParams'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, VPVHSDataService,
        $state, UIControlService, GlobalConstantService, $uibModal, $stateParams) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.AddFlag = false;

        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            VPVHSDataService.cekRole(function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info(vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            UIControlService.loadLoading("");
            VPVHSDataService.getReminder({
                Keyword: vm.srcText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.contracts = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CONTRACTS'));
            });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            loadAwal();
        };
        
        vm.buatCPR = buatCPR;
        function buatCPR(vhsAwardId) {
            $state.transitionTo('add-vendor-performance-vhs', { type: 2, act: 0, id: 0, vhsawardid: vhsAwardId });
        };

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('vendor-performance-vhs');
        };
    }
})();