﻿(function () {
    'use strict';

    angular.module("app").controller("VPVHSCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPVHSDataService',
        '$state', 'UIControlService', 'UploadFileConfigService', 'UploaderService', 'GlobalConstantService',
        '$uibModal', '$stateParams', 'WarningLetterService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, VPVHSDataService,
        $state, UIControlService, UploadFileConfigService, UploaderService, GlobalConstantService,
        $uibModal, $stateParams, WarningLetterService) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        //vm.AddFlag = false;

        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            VPVHSDataService.cekRole(function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info(vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            UIControlService.loadLoading("");
            VPVHSDataService.select({
                Keyword: vm.srcText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.vpvhs = reply.data.List;
                vm.count = reply.data.Count;
                /*if (vm.vpvhs) {
                    if (vm.cpr[0].SysReference.Name == "STS_DRAFT")
                        vm.AddFlag = true;
                    else
                        vm.AddFlag = false;
                } else {
                    vm.AddFlag = false;
                }*/
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            loadAwal();
        };

        vm.approve = approve;
        function approve(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_PUBLISH_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPVHSDataService.publish({
                        VPVHSDataId: VPdata.VPVHSDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            /*
                            if (VPdata.SysReference.Name == "STS_DRAFT") {
                                if (VPdata.Score < 60) {
                                    //VPVHSDataService.insertWL({
                                    //    VendorId: VPdata.VendorId,
                                    //    ProjManager: VPdata.ProjManager,
                                    //    DepartmentHolder: VPdata.DepartmentHolder,
                                    //    IsVhsCpr: VPdata.IsVhsCpr
                                    //}, function (reply) {
                                    //    if (reply.status === 200) {
                                    //        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.INSERT_WL_SUCC'));
                                    //    } else {
                                    //        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT_WL'));
                                    //    }
                                    //}, function (err) {
                                    //    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT_WL'));
                                    //});
                                    saveToWL(VPdata);
                                }
                            }
                            */
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.APPROVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APPROVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APPROVE_CPR'));
                    });
                }
            });
        };

        vm.reject = reject;
        function reject(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_REJECT_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPVHSDataService.reject({
                        VPVHSDataId: VPdata.VPVHSDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.REJECT_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_CPR'));
                    });
                }
            });
        };

        /*
        vm.saveToWL = saveToWL;
        function saveToWL(VPdata) {

            WarningLetterService.insertWarningLetterByScoreVHS({
                VendorID: VPdata.VendorId,
                ReporterID: VPdata.DepartmentHolder,
                IDContract: VPdata.VHSAwardId

            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    console.log("succ. cpr-vhs data saved to WL.");
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoadingModal();
            });
        }
        */

        vm.switchActive = switchActive;
        function switchActive(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_DELETE_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPVHSDataService.switchactive({
                        VPVHSDataId: VPdata.VPVHSDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SWITCH_ACTIVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SWITCH_ACTIVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SWITCH_ACTIVE_CPR'));
                    });
                }
            });
        };

        vm.sendToVendor = sendToVendor;
        function sendToVendor(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_PUBLISH_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPVHSDataService.sendToVendor({
                        VPVHSDataId: VPdata.VPVHSDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.PUBLISH_TO_VENDOR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_PUBLISH_TO_VENDOR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_PUBLISH_TO_VENDOR'));
                    });
                }
            });
        };

        vm.detail = detail;
        function detail(wl, flagRole) {
            var lempar = {
                flagRole: flagRole,
                VPVHSDataId: wl.VPVHSDataId,
                IsVhsCpr: wl.IsVhsCpr
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-list.modal.html?v=1.000002',
                controller: 'vhsListModal',
                controllerAs: 'vhsListModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.appHistory = appHistory;
        function appHistory(wl) {
            var lempar = {
                VPVHSDataId: wl.VPVHSDataId,
                ContractName: wl.ContractNo + ' - ' + wl.TanderName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/approval-history.modal.html',
                controller: 'cprvAppHistoryCtrl',
                controllerAs: 'listAppCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () { });
        };

        vm.upload = upload;
        function upload(wl) {
            var lempar = {
                VPVHSDataId: wl.VPVHSDataId,
                VendorId: wl.VendorId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-list-lampiran.html',
                controller: 'frmUploadCPRV',
                controllerAs: 'frmUploadCPRV',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
            //$state.transitionTo("vendor-performance-vhs-docs", { vpvhsId: wl.VPVHSDataId });
        };

        vm.tambahform = tambahform;
        function tambahform(type, act, id) {
            if (vm.AddFlag == true) {
                UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.FORBID_ADD_DATA'));
            } else
                $state.transitionTo('add-vendor-performance-vhs', { type: type, act: act, id: id });
        };

        vm.updateData = updateData;
        function updateData(type, act, id) {
            $state.transitionTo('add-vendor-performance-vhs', { type: type, act: act, id: id });
        };

        vm.reminder = reminder;
        function reminder() {
            $state.transitionTo('vendor-performance-vhs-reminder');
        };
    }
})();