﻿(function () {
    'use strict';

    angular.module("app")
    .controller("vhsListModal", ctrl);

    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'GlobalConstantService', 'VPVHSDataService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, GlobalConstantService, VPVHSDataService, UIControlService, item) {

        var vm = this;
        vm.VPVHSDataId = item.VPVHSDataId;
        vm.type = item.IsVhsCpr;
        vm.flagRole = item.flagRole;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        var kr = [];
        var krLv1 = [];
        var krLv2 = [];
        var krLv3 = [];
        var vendorName = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            VPVHSDataService.selectbyid({
                VPVHSDataId: vm.VPVHSDataId
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vpdata = reply.data[0];
                    vendorName = vm.vpdata.VendorName;
                    loadDocs();

                    for (var i = 0; i < vm.vpdata.VPVHSDataDetails.length; i++) {
                        kr.push(vm.vpdata.VPVHSDataDetails[i]);
                    }

                    for (var i = 0; i < kr.length; i++) {
                        if (kr[i].Level === 1) {
                            krLv1.push(kr[i]);
                        }
                        else if (kr[i].Level === 2) {
                            krLv2.push(kr[i]);
                        }
                        else if (kr[i].Level === 3) {
                            krLv3.push(kr[i]);
                        }
                    }

                    for (var i = 0; i < krLv2.length; i++) {
                        krLv2[i].sub = [];
                        for (var j = 0; j < krLv3.length; j++) {
                            if (krLv3[j].Parent === krLv2[i].CriteriaId) {
                                krLv2[i].sub.push(krLv3[j]);
                            }
                        }
                    }

                    for (var i = 0; i < krLv1.length; i++) {
                        krLv1[i].sub = [];
                        for (var j = 0; j < krLv2.length; j++) {
                            if (krLv2[j].Parent === krLv1[i].CriteriaId) {
                                krLv1[i].sub.push(krLv2[j]);
                            }
                        }
                    }

                    vm.kriteria = krLv1;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
            });
        };

        function loadDocs() {
            UIControlService.loadLoadingModal("");
            VPVHSDataService.getDocs({
                VPVHSDataId: vm.VPVHSDataId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoadingModal();
            });
        };

        vm.printForm = printForm;
        function printForm(formEEC, wl) {
            $uibModalInstance.close();

            var innerContents = document.getElementById('formCPR').innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Contractor Performance Review VHS - ' + vendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();

            //$uibModalInstance.close();
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();