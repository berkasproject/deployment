﻿(function () {
    'use strict';

    angular.module("app")
    .controller("listRemarkController", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'WarningLetterService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, WarningLetterService) {

        var vm = this;

        vm.endpointAttachment = GlobalConstantService.getConstant('api') + "/";

        vm.WLID = item.LetterID;
        vm.DetailKontrak = item.DetailKontrak;
        vm.list = [];
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('master-warningLetter');
            loadData();
        };

        function loadData() {
            vm.list = [];
            UIControlService.loadLoadingModal("");
            WarningLetterService.GetRemarks({
                LetterID: vm.WLID
            }, function (reply) {
                UIControlService.unloadLoadingModal();                 
                vm.list = reply.data;
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_REMARKS'));
                UIControlService.unloadLoadingModal();
            });
        }
     
        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.close();
        };
    }
})();