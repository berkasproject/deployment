﻿(function () {
    'use strict';

    angular.module("app")
    .controller("listAppCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'WarningLetterService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, WarningLetterService) {

        var vm = this;

        vm.WLID = item.LetterID;        
        vm.crApps = [];
        vm.employeeFullName = "";
        vm.employeeID = 0;
        vm.information = "";
        vm.flagEmp = item.flag;
        vm.flagActive = true;
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('master-warningLetter');
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            vm.crApps = [];
            WarningLetterService.getApproval({
                WarningLetterID: vm.WLID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {                 
                    vm.list = reply.data;
                    for (var i = 0; i < vm.list.length; i++) {
                        vm.crApps.push({
                            IsActive: vm.list[i].IsActive,
                            ID: vm.list[i].ID,
                            EmployeeID: vm.list[i].EmployeeIDApp,
                            ApprovalDate: UIControlService.convertDateTime(vm.list[i].ApprovalDate),
                            ApprovalStatus: vm.list[i].AppStatusName,
                            Remark: vm.list[i].Remark,
                            EmployeeFullName: vm.list[i].MstEmployee.FullName + ' ' + vm.list[i].MstEmployee.SurName,
                            EmployeePositionName: vm.list[i].MstEmployee.PositionName,
                            EmployeeDepartmentName: vm.list[i].MstEmployee.DepartmentName,
                            IsHighPriority: vm.list[i].IsHighPriority
                        });
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_APP'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        }
     
        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.close();
        };
    }
})();