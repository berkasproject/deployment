﻿(function () {
	'use strict';

	angular.module("app").controller("DetailCRVendorCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'SocketService', 'PermintaanUbahDataService', 'UIControlService', 'item', '$uibModalInstance'];
	function ctrl($translatePartialLoader, SocketService, PUbahDataService, UIControlService, item, $uibModalInstance) {
		var vm = this;
		vm.CRID = item.ChangeRequestID;
		vm.CREndDate = item.EndChangeDate;
		vm.listCR = [];
		vm.VendorName = item.VendorName;
		vm.CRDate = item.ChangeRequestDate;
		vm.isCalendarOpened = [false];

		vm.dateNow = new Date();
		vm.timezone = vm.dateNow.getTimezoneOffset();
		vm.timezoneClient = vm.timezone / 60;

		vm.minDate = {
			minDate: Date.now()
			//maxDate: Date.now()
		};

		vm.rejectCR = false;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("permintaan-ubah-data");
			console.info("item" + JSON.stringify(item));
			loadData();
			if (!(vm.CREndDate === null)) {
				vm.CREndDate = new Date(Date.parse(vm.CREndDate));

				//angular detectBrowser
				vm.isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
				vm.isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
				vm.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0; // At least Safari 3+: "[object HTMLElementConstructor]"
				vm.isChrome = !!window.chrome && !vm.isOpera; // Chrome 1+
				vm.isIE = /*@cc_on!@*/ false || !!document.documentMode; // At least IE6
				//console.info("isChrome?" + vm.isChrome);
				//console.info("timezoneClient" + vm.timezoneClient);

				if (vm.isChrome === true) {
					var objappVersion = navigator.appVersion; var objAgent = navigator.userAgent; var objbrowserName = navigator.appName; var objfullVersion = '' + parseFloat(navigator.appVersion); var objBrMajorVersion = parseInt(navigator.appVersion, 10); var objOffsetName, objOffsetVersion, ix;
					if ((objOffsetVersion = objAgent.indexOf("Chrome")) != -1) { objbrowserName = "Chrome"; objfullVersion = objAgent.substring(objOffsetVersion + 7); }
					if ((ix = objfullVersion.indexOf(";")) != -1) objfullVersion = objfullVersion.substring(0, ix);
					if ((ix = objfullVersion.indexOf(" ")) != -1) objfullVersion = objfullVersion.substring(0, ix); objBrMajorVersion = parseInt('' + objfullVersion, 10);
					if (isNaN(objBrMajorVersion)) { objfullVersion = '' + parseFloat(navigator.appVersion); objBrMajorVersion = parseInt(navigator.appVersion, 10); };
					if (objBrMajorVersion >= 58) {
						vm.CREndDate = new Date(vm.CREndDate.setHours(vm.CREndDate.getHours()));
						vm.CREndDate.setSeconds(0);
						vm.CREndDate.setMilliseconds(0);
					}
					else if (objBrMajorVersion <= 57) {
						vm.CREndDate = new Date(vm.CREndDate.setHours(vm.CREndDate.getHours() + vm.timezoneClient));
						vm.CREndDate.setSeconds(0);
						vm.CREndDate.setMilliseconds(0);
					}
				}
			}
		};

		function loadData() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			PUbahDataService.getDetailDataCR({ ChangeRequestID: vm.CRID }, function (reply) {
				//console.info("data:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listCR = data;
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_DATA");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.approvedCR = approvedCR;
		function approvedCR(IsApprove) {
			if (vm.CREndDate === null) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_CHANGE_DATE");
				return false;
			}

			var newdetail = [];
			//console.info(JSON.stringify(vm.listCR));
			for (var i = 0; i < vm.listCR.length; i++) {
				var approve = 0;
				if (vm.listCR[i].IsApproved === true) {
					approve = 1;
				}
				var dt = {
					DetailCRID: vm.listCR[i].DetailCRID,
					IsApproved: approve
				}
				newdetail.push(dt);
			}
			vm.CREndDate = new Date(vm.CREndDate.setHours(vm.CREndDate.getHours() - vm.timezoneClient));
			UIControlService.loadLoading("MESSAGE.LOADING");
			PUbahDataService.openLockCR({
				ChangeRequestID: vm.CRID,
				IsActive: IsApprove,
				ChangeRequestDataDetails: newdetail,
				EndChangeDate: vm.CREndDate
			}, function (reply) {
				console.info("endate:" + JSON.stringify(vm.CREndDate));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_APPROVE");
					SocketService.emit("PermintaanUbahData");
					$uibModalInstance.close();
					//$uibModalInstance.dismiss('cancel');
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_DATA");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_APPROVE");
				SocketService.emit("PermintaanUbahData");
				$uibModalInstance.close();
				//$uibModalInstance.dismiss('cancel');
			});
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		}

		vm.rejectCR = rejectCR;
		function rejectCR() {
			console.info("masuk rejectCR");
			vm.rejectCR = true;
		}

		vm.cancelrejectCR = cancelrejectCR;
		function cancelrejectCR() {
			console.info("cancel rejectCR");
			vm.rejectCR = "";
		}

		vm.rejectCRfinal = rejectCRfinal;
		function rejectCRfinal() {
			PUbahDataService.rejectCR({
				ChangeRequestID: vm.CRID,
				ApprovalComment: vm.remarkRejectCR
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_REJECT");
					//SocketService.emit("PermintaanUbahData");
					$uibModalInstance.close();
					//$uibModalInstance.dismiss('cancel');
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_REJECT");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", err);
				//SocketService.emit("PermintaanUbahData");
				$uibModalInstance.close();
				//$uibModalInstance.dismiss('cancel');
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();