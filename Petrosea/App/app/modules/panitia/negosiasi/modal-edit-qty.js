﻿(function () {
    'use strict';

    angular.module("app")
    .controller("EditQuantityNegoCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams, $uibModalInstance) {
        var vm = this;
        vm.detail = item.item;
        vm.VendorID = item.VendorID;
        vm.TenderRefID = item.TenderRefID;
        vm.ProcPackType = item.ProcPackType;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi');
            loadData();
        }

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            NegosiasiService.selectLineCost({
                TenderRefID: vm.TenderRefID,
                VendorID: vm.VendorID,
                CRCESubId: vm.detail.ContractRequisitionCESubID,
                ProcPackType: vm.ProcPackType
            }, function (reply) {
                vm.ceLineOffers = reply.data;
                vm.offerTotalCost = 0;
                vm.ceSubTotalCost = 0;
                vm.negoTotalCost = 0;
                vm.ceLineOffers.forEach(function (sub) {
                    vm.offerTotalCost += sub.LineOfferCost;
                    vm.ceSubTotalCost += sub.LineCost;
                    vm.negoTotalCost += sub.LineNegoCost;
                });
                UIControlService.unloadLoadingModal();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OFFER');
            });
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.list = [];
            EvaluationTechnicalService.selectByEmployee({
                Keyword: vm.detail.EmployeeID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data;
                    console.info("data:" + JSON.stringify(vm.list));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Penilai" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.batal = batal;
        function batal() {
            //$uibModalInstance.dismiss('cancel');
            $uibModalInstance.close();
        };

        vm.save = save;
        function save() {
            vm.list = [];
            for (var i = 0; i < vm.ceLineOffers.length; i++) {
                if (vm.ceLineOffers[i].ID !== 0) {
                    var dt = {
                        //IsOpen: vm.ceLineOffers[i].IsOpen,
                        ID: vm.ceLineOffers[i].ID,
                        NegoId: vm.ceLineOffers[i].NegoId,
                        //CRCESubDetailId: vm.ceLineOffers[i].CRCESubDetailId,
                        //SOEPDId: vm.ceLineOffers[i].SOEPDId,
                        //UnitNegotiationPrice: vm.ceLineOffers[i].UnitNegoCost,
                        Quantity: vm.ceLineOffers[i].QuantityNego
                    };
                }
                else {
                    var dt = {
                        IsOpen: vm.ceLineOffers[i].IsOpen,
                        NegoId: vm.ceLineOffers[i].NegoId,
                        CRCESubDetailId: vm.ceLineOffers[i].CRCESubDetailId,
                        SOEPDId: vm.ceLineOffers[i].SOEPDId,
                        UnitNegotiationPrice: vm.ceLineOffers[i].UnitNegoCost,
                        Quantity: vm.ceLineOffers[i].QuantityNego
                    };
                }
                vm.list.push(dt);
            }
            NegosiasiService.EditQuantityNego(vm.list,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                       $uibModalInstance.close();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                   UIControlService.unloadLoadingModal();
               }
          );
        }
    }
}
)();