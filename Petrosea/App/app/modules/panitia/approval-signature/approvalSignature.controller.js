﻿(function () {
    'use strict';

    angular.module("app").controller("approvalSignatureCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'dataContractManagementService', '$filter', '$rootScope', '$state'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, dataContractManagementService, $filter, $rootScope, $state) {
        var vm = this;
        vm.action = "0";
        vm.Date = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.isCalendarOpened = [false];
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('approval-signature');
            UIControlService.loadLoading("Loading...");
            vm.jLoad(1);
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };


        vm.act = act;
        function act(action) {
            vm.Date = "";
            vm.keyword = "";
            vm.action = action;
            vm.jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            dataContractManagementService.selectCMCommittee({
                FilterType: Number(vm.action),
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword: vm.keyword,
                DateNull1: vm.Date1,
                DateNull2: vm.Date2
            }, function (reply) {
                vm.dataContract = reply.data.List;
                vm.totalItems = reply.data.Count;
                UIControlService.unloadLoading();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                //$rootScope.unloadLoading();
            });
        }

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            vm.jLoad(1);
        }

        vm.detail = detail;
        function detail(id) {
            $state.go('approval-signature-request', { id: id});
        }





    }
})();