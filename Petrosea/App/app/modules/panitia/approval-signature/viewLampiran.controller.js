﻿(function () {
    'use strict';

    angular.module("app")
    .controller("viewLampiranCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$state', 'item', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService', '$window', '$stateParams', 'GlobalConstantService', '$uibModalInstance', '$sce'];
    /* @ngInject */
    function ctrl($http, $filter, $state, item, $uibModal, $translate, $translatePartialLoader, $location, UIControlService, $window, $stateParams, GlobalConstantService, $uibModalInstance, $sce) {
        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var loadingCount;
        vm.totalItems = 0;
        vm.currentPage = 0;
        vm.pageSize = 5;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("dashboard-vendor");
            vm.ContractDocumentURL = item.data.DocumentURL;
            vm.urlFile = vm.folderFile + vm.ContractDocumentURL;
            vm.urlFileAslie = "https://docs.google.com/gview?url=" + vm.urlFile + "&embedded=true"

            vm.urlFileAslie = $sce.trustAsResourceUrl(vm.urlFileAslie);
            console.log(vm.urlFileAslie);


        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();