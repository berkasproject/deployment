﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailContractCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$state', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'dataContractManagementService', 'UIControlService', '$window', '$stateParams', 'GlobalConstantService', '$q', 'SocketService'];
    /* @ngInject */
    function ctrl($http, $filter, $state, $uibModal, $translate, $translatePartialLoader, $location, dataContractManagementService, UIControlService, $window, $stateParams, GlobalConstantService,$q,SocketService) {
        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var loadingCount;
        vm.jumlahHari = 0;
        vm.allowApprove = false;
        vm.dateNow = Date.now();

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.approved = false;
        vm.init = init;
        var username = localStorage.getItem("username");
        console.log(username)

        function init() {
            $translatePartialLoader.addPart("dashboard-vendor");
            UIControlService.loadLoading(loadmsg);
            loadCM();
            loadCommitteeLogin();
        }

        function loadCM() {
            dataContractManagementService.getCMbyID({
                IntParam1: Number($stateParams.id)
            }, function (reply) {
                vm.dataContract = reply.data.List[0];
                if (vm.dataContract) {
                    loadForum();
                }
                if (vm.dataContract.StartDateDuration && vm.dataContract.EndDateDuration) {
                    vm.jumlahHari = getDays(vm.dataContract.EndDateDuration, vm.dataContract.StartDateDuration);
                    if (new Date(vm.dataContract.EndDateDuration) < vm.dateNow && vm.dataContract.StatusName == "Review") {
                        insertHistory(vm.dataContract);
                    }
                }
            }, function (error) {
                unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
            });
        }

        function insertHistory(data) {
            var stats = parseInt(data.StatusManagement) + 1;
            dataContractManagementService.insertHistory({
                ContractManagementID: Number($stateParams.id),
                Status: stats,
                Remark: null
            }, function (reply) {
                if (reply.status === 200) {
                    vm.init();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
            });
        }

        function loadCommitteeLogin() {
            dataContractManagementService.getCommitteeLogin({
                IntParam1: Number($stateParams.id)
            }, function (reply) {
                vm.committeeLogin = reply.data;
            }, function (error) {
                unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
            });
        }
        function loadForum() {
            dataContractManagementService.getForum({
                IntParam1: Number($stateParams.id)
            }, function (reply) {
                vm.dataForum = reply.data.List[0];
                
                if (vm.dataForum) {
                    vm.forumID = vm.dataForum.ForumID;
                    loadDetailForum($stateParams.id);
                    loadPenerimaChat();
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
            });
        }

        function loadPenerimaChat() {
            var defer = $q.defer();
            dataContractManagementService.getPenerimaChat({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.penerimaChat = data;

                    UIControlService.unloadLoading();

                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'ERROR.GAGAL_API')
                defer.reject(false);
            })
            return defer.promise;
        }
        
        SocketService.on('realtimeReviewCommittee', function (reply) {
            var penerimaChat = reply.penerimaChat;
            var dataPilih = $.grep(penerimaChat, function (n) { return n.CommitteeOrVendorName == username; });

            if (dataPilih.length != 0) {
                loadDetailForum(reply.ContractManagementID)

            }
        });

        function loadDetailForum(id) {
            dataContractManagementService.getDetailForum({
                IntParam1: id
            }, function (reply) {
                vm.dataDetailForum = reply.data.List;
                if (vm.dataDetailForum) {
                    for (var i = 0; i < vm.dataDetailForum.length; i++) {
                        if (vm.dataDetailForum[i].ContractCommitteeID == vm.committeeLogin) {
                            vm.allowApprove = true;
                            if (vm.dataDetailForum[i].Status == "1") {
                                vm.approved = true;
                                break;
                            }
                        }
                    }
                    UIControlService.unloadLoading();
                }
            }, function (error) {
                unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
            });
        }
        function getDays(endDt, startDt) {
            var end = new Date(endDt);
            var start = new Date(startDt);

            var ONE_DAY = 1000 * 60 * 60 * 24;
            var differenceMs = Math.abs(end - start);
            return Math.round(differenceMs / ONE_DAY);

        }

        vm.lampiran = lampiran;
        function lampiran(id) {
            var data = {
                CMID: id
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/dashboard/detailLampiran.html',
                controller: 'LampiranCtrl',
                controllerAs: 'LampiranCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                },
                backdrop: 'static'
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.review = review;
        function review() {
            var data = {
                ForumID: vm.forumID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-signature/approveCommittee.html',
                controller: 'approveCommitteeCtrl',
                controllerAs: 'approveCommitteeCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                },
                backdrop: 'static'
            });
            modalInstance.result.then(function () {
                vm.init();
                SocketService.emit("realtimeReview", { ContractManagementID: $stateParams.id, penerimaChat: vm.penerimaChat.List });
                SocketService.emit("realtimeReviewVendor", { ContractManagementID: $stateParams.id, penerimaChat: vm.penerimaChat.List });


            });
        }

        vm.approve = approve;
        function approve() {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_APPROVE') + '<h3>', function (reply) {
                if (reply) {
                    UIControlService.loadLoading(loadmsg);
                    dataContractManagementService.approveForumCommittee({
                        ForumID: vm.forumID
                    }, function (reply2) {
                        if (reply2.status === 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_APPROVE');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_APPROVE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_APPROVE');
                    });
                }
            });
        };

        vm.back = back;
        function back() {
            $state.transitionTo('approval-signature');
        }
    }

})();

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}