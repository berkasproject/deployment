﻿(function () {
	'use strict'

	angular.module("app").controller("DetEmailRegretCtrl", ctrl)

	ctrl.$inject = ['item', '$translatePartialLoader', 'UIControlService', 'ContractSignOffService', '$uibModal', '$uibModalInstance']

	function ctrl(item, $translatePartialLoader, UIControlService, ContractSignOffService, $uibModal, $uibModalInstance) {
		var vm = this

		vm.keterangan = ''

		if (item.selVendorId == null) {
			vm.selVendorId = 0
		} else {
			vm.selVendorId = item.selVendorId
		}

		vm.item = item
		vm.list = {}
		vm.selVendors = []
		vm.isSelectAll = false

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('monitoring-kontrak')
			getRegretLetter()
		}

		vm.selectAll = selectAll
		function selectAll() {
			vm.selVendors = []
			if (vm.isSelectAll) {
				if (vm.list.Vendor.IsNeedQuestionnaire !== true || vm.list.Vendor.IsNeedQuestionnaire === null) {
					vm.list.Vendor.flagButton = true
					vm.selVendors.push(vm.list.Vendor.VendorID)
				}

				for (var i = 0; i < vm.list.VendorLooser.length; i++) {
					if (vm.list.VendorLooser[i].IsNeedQuestionnaire !== true || vm.list.VendorLooser[i].IsNeedQuestionnaire === null) {
						vm.list.VendorLooser[i].flagButton = true
						vm.selVendors.push(vm.list.VendorLooser[i].VendorID)
					}
				}
			} else {
				vm.list.Vendor.flagButton = false

				for (var i = 0; i < vm.list.VendorLooser.length; i++) {
					vm.list.VendorLooser[i].flagButton = false
				}
			}
		}

		vm.addRemoveVendor = addRemoveVendor
		function addRemoveVendor(data) {
			if (data.flagButton)
				vm.selVendors.push(data.VendorID);
			else
				vm.isSelectAll = false
			for (var i = 0; i < vm.selVendors.length; i++) {
				if (vm.selVendors[i] == data.VendorID && !data.flagButton) {
					vm.selVendors.splice(i, 1);
				}
			}
		}

		vm.initDetail = initDetail
		function initDetail() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING')
			ContractSignOffService.getEmailByVendor({
				TenderStepID: vm.item.item.TenderStepData.ID,
				VendorID: vm.selVendorId,
				IsActive: vm.selVendorId === vm.item.item.VendorID
			}, function (reply) {
				UIControlService.unloadLoadingModal()
				if (reply.status === 200) {
					vm.keterangan = reply.data
				}
			});
		}

		vm.getRegretLetter = getRegretLetter
		function getRegretLetter() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING')
			ContractSignOffService.getRegretLetter({
				Keyword: vm.item.item.TenderStepData.tender.TenderCode,
				column: 2
			}, function (reply) {
				UIControlService.unloadLoadingModal()
				if (reply.status === 200) {
					vm.list = reply.data
				}
			}, function (error) {
				UIControlService.unloadLoading()
			})
		}

		vm.cancelDetail = cancelDetail
		function cancelDetail() {
			$uibModalInstance.close();
		}

		vm.saveMail = saveMail
		function saveMail() {
			UIControlService.loadLoadingModal('MESSAGE.SAVING')
			ContractSignOffService.saveMail({
				TenderStepID: vm.item.item.TenderStepData.ID,
				VendorID: vm.selVendorId,
				keterangan: vm.keterangan
			}, function (reply) {
				UIControlService.unloadLoading()
				if (reply.status === 200) {
					UIControlService.unloadLoadingModal()
					UIControlService.msg_growl('success', "SAVE_SUCCESS");
				}
			}, function (error) {
				UIControlService.unloadLoading()
			})
		}

		vm.sendMail = sendMail
		function sendMail() {
			UIControlService.loadLoadingModal('MESSAGE.SENDING_MAIL')
			ContractSignOffService.sendToSubmittingVendor({
				ID: vm.list.ID,
				VendorIDs: vm.selVendors
			}, function (reply) {
				UIControlService.unloadLoadingModal()
				if (reply.status === 200) {
					UIControlService.unloadLoadingModal()
					UIControlService.msg_growl('success', "EMAIL_SENT")
					init()
				}
			}, function (error) {
				UIControlService.unloadLoadingModal()
				UIControlService.msg_growl('error', "FAILURE_SENDING_MAIL")
				init()
			});
		}

		vm.seeEmailDetail = seeEmailDetail
		function seeEmailDetail(vendId, vendName, isSent) {
			vm.item.selVendorId = vendId
			vm.item.selVendorName = vendName
			vm.item.selIsSent = isSent
			var modalInstance = $uibModal.open({
				templateUrl: 'detailEmail.html',
				controller: 'DetEmailRegretCtrl',
				controllerAs: 'DetEmailRegretCtrl',
				resolve: {
					item: function () {
						return vm.item;
					}
				}
			})
			modalInstance.result.then(function () {
			}, function () {
				vm.keterangan = ''
				vm.selVendorId = 0
			})
		}
	}
})()