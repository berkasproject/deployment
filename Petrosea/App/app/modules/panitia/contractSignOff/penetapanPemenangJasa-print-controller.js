﻿(function () {
    'use strict';

    angular.module("app").controller("ContractSignOffPrintController", ctrl);

    ctrl.$inject = ['ApprovalNoSAPService', '$state', '$uibModal', '$filter', 'UploadFileConfigService', 'UIControlService', '$translatePartialLoader', 'ContractSignOffService', 'UploaderService', '$stateParams', 'GlobalConstantService', 'DataPengadaanService'];

    function ctrl(ApprovalNoSAPService, $state, $uibModal, $filter, UploadFileConfigService, UIControlService, $translatePartialLoader, ContractSignOffService, UploaderService, $stateParams, GlobalConstantService, DataPengadaanService) {
        var vm = this;

        vm.IDTender = Number($stateParams.TenderRefID);
        vm.IDStepTender = Number($stateParams.StepID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.isStartDateOpened = false;
        vm.isEndDateOpened = false;
        vm.filePath;
        vm.flagEmp = 0;
        vm.isCalendarOpened = [false, false, false, false];
        vm.isCalendarOpened1 = [false, false, false, false];

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };
        vm.openCalendar1 = openCalendar1;
        function openCalendar1(index) {
            vm.isCalendarOpened1[index] = true;
        };

        vm.cetak = cetak;
        function cetak() {
            html2canvas(document.getElementById('print'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download(vm.step.tender.TenderName + " (Penandatanganan Kontrak).pdf");
                }
            });
        }


        vm.init = init;
        function init() {
            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.IDTender,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            $translatePartialLoader.addPart('contract-signoff');
            loadLogin();
            loadTender();
            vm.flagTemp = 0;
            loadTypeSizeFile();
            UIControlService.loadLoading("MESSAGE.LOADING");
            ContractSignOffService.getSignOff({
                TenderStepID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.detail = reply.data;
                    for (var i = 0; i < vm.detail.length; i++) {
                        if (vm.detail[i].ApprovalStatusReff == null) {
                            vm.detail[i].Status = 'Draft';
                            vm.detail[i].flagTemp = 0;
                        }
                        else if (vm.detail[i].ApprovalStatusReff != null) {
                            cekEmployee(vm.detail[i].ID, vm.detail[i]);
                            vm.detail[i].Status = vm.detail[i].ApprovalStatusReff.Value;
                            vm.detail[i].flagTemp = 1;
                        }
                        vm.detail[i].ContractStartDate = new Date(Date.parse(vm.detail[i].ContractStartDate));
                        vm.detail[i].ContractEndDate = new Date(Date.parse(vm.detail[i].ContractEndDate));

                        shortenSummary(vm.detail[i]);
                    }

                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function shortenSummary(data) {
            data.SummaryShort = data.Summary;
            if (data.SummaryShort != null) {
                if (data.SummaryShort.length > 25) {
                    data.SummaryShort.length = data.SummaryShort.length.substring(0, 25) + "...";
                }
            }
        }

        vm.loadTender = loadTender;
        function loadTender() {
            ContractSignOffService.Step({
                TenderStepID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                    vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
                    console.info(vm.step);
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.sendToApproveL1L2 = sendToApproveL1L2
        function sendToApproveL1L2(data) {
            vm.ID = data.ID;
            bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV_L1_L2'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    var dt = {
                        TenderStepDataId: vm.IDStepTender,
                        VendorID: data.VendorID
                    };
                    ApprovalNoSAPService.sendToApproveL1L2(dt, function (reply) {
                        if (reply.status === 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
                            init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                    });
                }
            });
        }

        vm.detailApprovalL1L2 = detailApprovalL1L2;
        function detailApprovalL1L2(dt) {
            var item = {
                TenderStepDataId: vm.IDStepTender,
                VendorID: dt.VendorID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-no-SAPcode/approval-no-SAPcode.modal.html',
                controller: 'detailApprovalNoSAPCtrl',
                controllerAs: 'detailApprovalNoSAPCtrl',
                resolve: { item: function () { return item } }
            })
            modalInstance.result.then(function (ID, filter, flag) {
                init()
            })
        }

        vm.loadLogin = loadLogin;
        function loadLogin() {
            ContractSignOffService.getLogin({
                TenderStepID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.login = reply.data;
                    console.info(vm.login);
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.openStartDate = openStartDate;
        function openStartDate() {
            if (vm.isStartDateOpened) vm.isStartDateOpened = false;
            else vm.isStartDateOpened = true;
        }

        vm.openEndDate = openEndDate;
        function openEndDate() {
            if (vm.isEndDateOpened) vm.isEndDateOpened = false;
            else vm.isEndDateOpened = true;
        }

        vm.selectUpload = selectUpload;
        function selectUpload() {
            console.info(vm.filePath);
            //	//var test = vm.pathUpload;
        }

        function validateFileType(file, allowedFileTypes) {
            //console.info(JSON.stringify(allowedFileTypes));
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function typefile(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }

            var selectedFileType = file[0].type;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
            //console.info("tipefile: " + selectedFileType);
            if (selectedFileType === "application/pdf") {
                selectedFileType = "pdf";
            } else if (selectedFileType === "application/msword") {
                selectedFileType = "doc";
            } else if (selectedFileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || selectedFileType == "vnd.openxmlformats-officedocument.wordprocessingml.document") {
                selectedFileType = "docx";
            } else {
                selectedFileType = selectedFileType;
            }
            return selectedFileType;
            //console.info("file:" + selectedFileType);
        }

        vm.uploadFile = uploadFile;
        function uploadFile(data) {
            var folder = "SIGNOFF_" + vm.IDStepTender + data.VendorName;

            if (data.AwardValue == "" || data.AwardValue == null || data.AwardValue == undefined) {
                UIControlService.msg_growl("error", "MESSAGE.APPV_BUDGET_FILL");
                return;
            } else if (data.ContractNo == "" || data.ContractNo == null || data.ContractNo == undefined) {
                UIControlService.msg_growl("error", "MESSAGE.CONTRACTNO_FILL");
                return;
            } else if (data.ContractStartDate == "" || data.ContractStartDate == null || data.ContractStartDate == undefined) {
                UIControlService.msg_growl("error", "MESSAGE.CONTRACTSTART_FILL");
                return;
            } else if (data.ContractEndDate == "" || data.ContractEndDate == null || data.ContractEndDate == undefined) {
                UIControlService.msg_growl("error", "MESSAGE.CONTRACTEND_FILL");
                return;
            }

            if (data.filePath === undefined) { //null
                if (data.UploadURL != null) {
                    saveSignOff(data);
                } else if (data.UploadURL == null) {
                    UIControlService.msg_growl("error", "MESSAGE.DOC_FILL");
                    return;
                }
            } else if (data.filePath !== undefined) {
                var tipefileupload = typefile(data.filePath, vm.idUploadConfigs);
                if (validateFileType(data.filePath, vm.idUploadConfigs)) {
                    upload(data, vm.idFileSize, vm.idFileTypes, folder, tipefileupload);
                }
            }
        }

        function upload(file, config, filters, folder, tipefile, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            } else if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            //console.info(file[0].size + ":" + file[0].type);
            var tipesize_file = tipefile + " / " + Math.floor(file.filePath[0].size / 1024) + " KB";
            console.info(tipesize_file);

            UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFile(file.filePath, "UPLOAD_DIRECTORIES_ADMIN", size, filters, folder, function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_UPLOAD");
                    file.UploadURL = url;
                    saveSignOff(file);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (response) {
                //UIControlService.msg_growl("error", "MESSAGE.API")
                UIControlService.unloadLoading();
            });
        }

        function loadTypeSizeFile() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.TENDER.CONTRACTSIGNOFF", function (response) {
                UIControlService.unloadLoadingModal();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
                return;
            });
        }

        vm.openSummary = openSummary;
        function openSummary(data) {
            var item = {
                Summary: data.Summary,
                VendorName: data.VendorName,
                isReadOnly: !vm.isAllowedEdit || !(data.Status === 'Draft' || data.Status === 'REJECT')
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contractSignOff/contractSummary.modal.html',
                controller: 'contractSignOffSummaryController',
                controllerAs: 'contSummCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function (summary) {
                data.Summary = summary;
                shortenSummary(data);
            });
        }

        //vm.saveSignOff = saveSignOff;
        function saveSignOff(data) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            ContractSignOffService.save({
                VendorID: data.VendorID,
                ContractEndDate: UIControlService.getStrDate(data.ContractEndDate),
                ContractNo: data.ContractNo,
                ContractStartDate: UIControlService.getStrDate(data.ContractStartDate),
                Summary: data.Summary,
                UploadURL: data.UploadURL,
                TenderStepID: vm.IDStepTender,
                AwardValue: data.AwardValue,
                ID: data.ID
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("success", 'MESSAGE.SAVE_SUCCESS_MESSAGE', "MESSAGE.SAVE_SUCCESS_TITLE");
                    window.location.reload();
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.sendToApprove = sendToApprove;
        function sendToApprove(data) {
            vm.ID = data.ID;
            bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    var dt = {
                        ID: data.ID,
                        TenderStepID: vm.IDStepTender,
                        flagEmp: 1
                    };
                    ContractSignOffService.SendApproval(dt, function (reply) {
                        if (reply.status === 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
                            sendMail();
                            init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                    });
                }
            });
        }

        function sendMail() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            ContractSignOffService.SendEmail({
                ID: vm.ID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {

                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
            });
        }


        vm.cekEmployee = cekEmployee;
        function cekEmployee(Id, reff) {
            ContractSignOffService.CekEmployee({
                ID: Id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    reff.flagEmp = reply.data;
                }
            }, function (err) {
                // UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.detailApproval = detailApproval;
        function detailApproval(dt, data) {
            var item = {
                ID: data.ID,
                flag: data.flagEmp,
                Status: dt
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contractSignOff/detailApproval.modal.html',
                controller: 'detailApprovalSignOffCtrl',
                controllerAs: 'detailApprovalSignOffCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                init();
            });
        };

        vm.back = back;
        function back() {
            $state.transitionTo('penandatanganan-kontrak', { TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.step.tender.ProcPackageType });
        }
    }
})();