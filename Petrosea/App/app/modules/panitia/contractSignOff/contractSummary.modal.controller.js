﻿(function () {
    'use strict';

    angular.module("app")
    .controller("contractSignOffSummaryController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$state', 'UIControlService', '$uibModalInstance', 'GlobalConstantService', 'item'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $state, UIControlService, $uibModalInstance, GlobalConstantService, item) {
        var vm = this;

        vm.Summary = item.Summary;
        vm.VendorName = item.VendorName;
        vm.isReadOnly = item.isReadOnly;

        function init() {
            
        }

        vm.save = save;
        function save() {
            $uibModalInstance.close(vm.Summary);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
            //$uibModalInstance.close();
        };
    }
})();;