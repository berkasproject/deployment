(function () {
    'use strict';

    angular.module("app")
    .controller("detailEvaluatorTeknisCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', '$uibModal', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluationTechnicalService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, $uibModal, item, $translate, $translatePartialLoader, $location, SocketService, EvaluationTechnicalService, UIControlService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.evaluationTechnicals = [];
        vm.vendorName = item.VendorName;

        vm.init = init;
        function init() {
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal(loadmsg);
            EvaluationTechnicalService.allTechnicalScoreByVendor({
                TenderStepDataID: item.TenderStepDataID,
                VendorID: item.VendorID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.evaluationTechnicals = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EVALUATORS'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EVALUATORS'));
            });
        }        

        vm.detScore = detScore;
        function detScore(data) {
            var data = {
                EvaluatorID: data.EvaluatorID,
                EvaluatorName: data.EvaluatorName,
                VendorName: item.VendorName,
                TenderStepDataID: item.TenderStepDataID,
                VendorID: item.VendorID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-teknis/detailEvaluatorTeknisSkor.modal.html',
                controller: 'detailEvaluatorTeknisSkorCtrl',
                controllerAs: 'dEvaluatorSkorCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();