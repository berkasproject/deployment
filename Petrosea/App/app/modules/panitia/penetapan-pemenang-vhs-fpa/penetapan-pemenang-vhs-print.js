﻿(function () {
	'use strict';

	angular.module("app").controller("PPGVHSPrintCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'UIControlService', 'DataPengadaanService', 'PenetapanPemenangVHSservice', '$stateParams'];
	function ctrl($translatePartialLoader, UIControlService, DataPengadaanService, PenetapanPemenangVHSservice, $stateParams) {
		var vm = this
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.jumlahlembar = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('negosiasi');
			$translatePartialLoader.addPart('vhs-award');
			UIControlService.loadLoading("MESSAGE.LOADING");

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			vm.flagExcel = false;
			getUserLogin();
			jLoad(1);
		}

		vm.loadTaxCode = loadTaxCode;
		function loadTaxCode(data) {
			PenetapanPemenangVHSservice.selectTaxCode(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listTaxCode = reply.data;
					for (var x = 0; x < reply.data.length; x++) {
						if (data.TaxCode == reply.data[x].ID) {
							data.selectTaxCode = reply.data[x];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.getUserLogin = getUserLogin;
		function getUserLogin() {
			PenetapanPemenangVHSservice.CekRequestor({
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.flagSave = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.detail[0].TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			var tender = {
				column: vm.StepID,
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}

			PenetapanPemenangVHSservice.select(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.detail = reply.data.List;

					for (var i = 0; i < vm.detail.length; i++) {
						loadTaxCode(vm.detail[i]);
						if (vm.detail[i].ApprovalStatusReff == null) {
							vm.detail[i].Status = 'Draft';
							vm.detail[i].flagTemp = 0;
						} else if (vm.detail[i].ApprovalStatusReff != null) {
							//cekEmployee(vm.detail[i].ID, vm.detail[i]);
							vm.detail[i].Status = vm.detail[i].ApprovalStatusReff.Value;
							if (vm.detail[i].ApprovalStatusReff.Name == "CR_APPROVED") vm.flagExcel = true;

							vm.detail[i].flagTemp = 1;
						}
						vm.detail[i].StartContractDate = new Date(Date.parse(vm.detail[i].StartContractDate));
						vm.detail[i].ExpDate = new Date(Date.parse(vm.detail[i].ExpDate));
					}
					vm.count = reply.data.Count;
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});

			PenetapanPemenangVHSservice.SelectStepAdmin(tender, function (reply) {
				vm.step = reply.data;
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
			});
		}
	}
})()