﻿(function () {
	'use strict';

	angular.module("app").controller("editTemplateCtrl", ctrl);

	ctrl.$inject = ['TemplateDokumenService', 'item', 'UIControlService', '$uibModalInstance'];

	function ctrl(TemplateDokumenService, item, UIControlService, $uibModalInstance) {
		var vm = this;

		vm.init = init;
		function init() {
			selectTemplateByID();
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.close();
		}

		vm.saveTemplate = saveTemplate;
		function saveTemplate() {
			UIControlService.loadLoadingModal('LOADING.UPDATE.MESSAGE');
			TemplateDokumenService.saveTemplate({
				TemplateID: item.ID,
				TemplateDescription: vm.TemplateDescription
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("info", 'NOTIFICATION.UPDATE.SUCCESS.MESSAGE', "NOTIFICATION.UPDATE.SUCCESS.TITLE");
					$uibModalInstance.close();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.UPDATE.ERROR.MESSAGE', "NOTIFICATION.UPDATE.ERROR.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.UPDATE.ERROR.MESSAGE', "NOTIFICATION.UPDATE.ERROR.TITLE");
			});
		}

		function selectTemplateByID() {
			UIControlService.loadLoading('LOADING.GETTEMPLATES.MESSAGE');
			TemplateDokumenService.selectTemplateByID({
				TemplateID: item.ID
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					vm.TemplateDescription = reply.data.TemplateDescription;
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETTEMPLATES.ERROR', "NOTIFICATION.GETTEMPLATES.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETTEMPLATES.ERROR', "NOTIFICATION.GETTEMPLATES.TITLE");
			});
		}
	}
})();