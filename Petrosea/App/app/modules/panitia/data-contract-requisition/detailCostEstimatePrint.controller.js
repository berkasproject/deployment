﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailCostEstimatePrintCtrl", ctrl);

    ctrl.$inject = ['$state', 'Excel', '$timeout', '$uibModalInstance', '$filter', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataContractRequisitionService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, Excel, $timeout, $uibModalInstance, $filter, item, $translate, $translatePartialLoader, $location, SocketService, DataContractRequisitionService, UIControlService, GlobalConstantService) {

        var vm = this;
        var contractRequisitionId = item.contractRequisitionId;

        vm.ceLines = [];

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal("");
            DataContractRequisitionService.CELineForExport({
                ContractRequisitionId: contractRequisitionId,
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.ceLines = [];
                var currentSubName = "";
                var index = 1;
                reply.data.forEach(function (line) {
                    if (line.SubName != currentSubName) {
                        currentSubName = line.SubName;
                        vm.ceLines.push({
                            Name: currentSubName
                        });
                    }
                    line.Index = index++;
                    vm.ceLines.push(line);
                });
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_CELINE');
            });
        }

        vm.print = print;
        function print() {
            vm.exportHref = Excel.tableToExcel('#tableToExport', 'sheet1');
            $timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();