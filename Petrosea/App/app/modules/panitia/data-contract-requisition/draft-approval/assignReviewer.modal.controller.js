(function () {
    'use strict';

    angular.module("app")
    .controller("assignReviewerCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'DataContractRequisitionService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, DataContractRequisitionService, GlobalConstantService) {

        var vm = this;
        var contractRequisitionId = item.contractRequisitionId;
        var loadmsg = "MESSAGE.LOADING";

        vm.item = item;
        vm.crRevs = [];
        vm.contractReq = {};

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            DataContractRequisitionService.GetCRReviewers({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.tenderCode = reply.data.TenderCode;
                vm.projectTitle = reply.data.ProjectTitle;
                vm.statusName = reply.data.StatusName;
                vm.isProcess1 = vm.statusName === "CR_PROCESS_1";
                vm.crRevs = reply.data.ContractRequisitionReviewers;
                vm.crRevs.forEach(function (crr) {
                    if (crr.ReviewStatus !== null) {
                        crr.ReviewStatus = crr.ReviewStatus === true ? 'APPROVED' : 'REJECTED';
                    }
                });
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_REVIEWER'));
            });
        }

        vm.selectNewReviwer = selectNewReviwer;
        function selectNewReviwer() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-contract-requisition/formContractRequisition.selectEmployeeModal.html',
                controller: 'selectEmployeeModal',
                controllerAs: 'selectEmployeeCtrl',
            });
            modalInstance.result.then(function (selectedReviewer) {

                for (var i = 0; i < vm.crRevs.length; i++) {
                    if (selectedReviewer.EmployeeID === vm.crRevs[i].EmployeeId) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_ADD_REVIEWER'));
                        return;
                    }
                }

                bootbox.confirm(($filter('translate')('MESSAGE.CONF_ADD_REVIEWER') + "<br/><br/>" + selectedReviewer.FullName + " " + selectedReviewer.SurName + " - " + selectedReviewer.PositionName),
                    function (yes) {
                        if (yes) {
                            addReviewer(selectedReviewer.EmployeeID);
                        }
                    }
                );
            });
        }

        function addReviewer(employeeId) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            DataContractRequisitionService.AddReviewer({
                ContractRequisitionId: contractRequisitionId,
                EmployeeId: employeeId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_ADD_REVIEWER'));
                loadData();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_ADD_REVIEWER'));
            });
        }

        vm.deleteReviewer = deleteReviewer;
        function deleteReviewer(rev) {

            bootbox.confirm(($filter('translate')('MESSAGE.CONF_DELETE_REVIEWER') + "<br/><br/>" + rev.EmployeeFullName + " - " + rev.EmployeePositionName),
                function (yes) {
                    if (yes) {
                        UIControlService.loadLoadingModal("MESSAGE.LOADING");
                        DataContractRequisitionService.DeleteReviewer({
                            ID: rev.ID,
                        }, function (reply) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_DELETE_REVIEWER'));
                            loadData();
                        }, function (error) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DELETE_REVIEWER'));
                        });
                    }
                }
            );
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();