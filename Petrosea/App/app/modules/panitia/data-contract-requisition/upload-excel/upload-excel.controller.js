﻿(function() {
    'use strict';

    angular.module("app").controller("UploadContractController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UploadContractService',
         'GlobalConstantService', 'UIControlService', '$uibModal', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UploadContractService,
         GlobalConstantService, UIControlService, $uibModal, $filter) {

        var vm = this;

        vm.init = init;
        function init() {

        };    

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload)) {
                upload(vm.fileUpload);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload(file) {
            UIControlService.loadLoading("");
            UploadContractService.UploadExcel(file,
                function (reply) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("notice", "SUCC_UPLOAD");
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "ERR_UPLOAD")
                    if (error.data.Message.substr(0, 4) === "ERR_") {
                        UIControlService.msg_growl("error", error.data.Message);
                    }
                }
            );
        }
    }
})();
