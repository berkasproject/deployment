﻿(function () {
	'use strict';

	angular.module("app").controller("evalTVPrintCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'UIControlService', 'DataPengadaanService', '$stateParams', 'EvaluasiTeknisVHSService'];
	
	/* @ngInject */
	function ctrl($translatePartialLoader, UIControlService, DataPengadaanService, $stateParams, EvaluasiTeknisVHSService) {
		var vm = this
		var loadmsg = "MESSAGE.LOADING";
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.evaluations = [];
		vm.jumlahlembar = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('evaluasi-teknis-vhs');
			UIControlService.loadLoading(loadmsg);
			loadData();
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.tenderStepData.tender.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		function loadData() {
			UIControlService.loadLoading(loadmsg);
			DataPengadaanService.GetStepByID({
				ID: vm.StepID
			}, function (reply) {
				vm.tenderStepData = reply.data;
				vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
				EvaluasiTeknisVHSService.selectOfferEntries({
					ID: vm.StepID,
					TenderID: vm.tenderStepData.TenderID
				}, function (reply) {
					UIControlService.unloadLoading();
					vm.evaluations = reply.data;
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
				});
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};
	}
})()