(function () {
	'use strict';

	angular.module("app").controller("evaluasiTeknisVHSCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'EvaluasiTeknisVHSService', 'DataPengadaanService', 'UIControlService', '$uibModal', '$state', '$stateParams', '$filter'];
	function ctrl($translatePartialLoader, EvaluasiTeknisVHSService, DataPengadaanService, UIControlService, $uibModal, $state, $stateParams, $filter) {
		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.evaluations = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('evaluasi-teknis-vhs');
			UIControlService.loadLoading(loadmsg);
			loadData();
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('evaluasi-teknis-vhs-print', {
				TenderRefID: vm.TenderRefID,
				StepID: vm.StepID,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			$translatePartialLoader.addPart('data-contract-requisition');
			var item = vm.StepID;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/evaluasi-teknis-vhs/detailApproval.html',
				controller: "VHSFPATechEvalApprvCtrl",
				controllerAs: "VHSFPATechEvalApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () { });
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPROVAL_CONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.LOADING');
					EvaluasiTeknisVHSService.sendToApproval({ ID: vm.StepID }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
							EvaluasiTeknisVHSService.isNeedTenderStepApproval({ ID: vm.StepID, TenderID: vm.tenderStepData.TenderID }, function (result) {
								vm.isNeedApproval = result.data;
							}, function (err) {
								$.growl.error({ message: "Gagal mendapatkan data Approval" });
								UIControlService.unloadLoading();
							});
							EvaluasiTeknisVHSService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
								vm.isApprovalSent = result.data;
							}, function (err) {
								$.growl.error({ message: "Gagal mendapatkan data Approval" });
								UIControlService.unloadLoading();
							});
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		function loadData() {
			UIControlService.loadLoading(loadmsg);
			DataPengadaanService.GetStepByID({
				ID: vm.StepID
			}, function (reply) {
				vm.tenderStepData = reply.data;
				vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
				EvaluasiTeknisVHSService.selectOfferEntries({
					ID: vm.StepID,
					TenderID: vm.tenderStepData.TenderID
				}, function (reply) {
					UIControlService.unloadLoading();
					vm.evaluations = reply.data;
					EvaluasiTeknisVHSService.isNeedTenderStepApproval({ ID: vm.StepID, TenderID: vm.tenderStepData.TenderID }, function (result) {
						vm.isNeedApproval = result.data;
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
					EvaluasiTeknisVHSService.isApprovalSent({ TenderStepDataID: vm.StepID }, function (result) {
						vm.isApprovalSent = result.data;
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});

				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
				});
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.detailEvaluator = detailEvaluator;
		function detailEvaluator(dt) {
			var item = {
				VendorName: dt.VendorName,
				VendorID: dt.VendorID,
				TenderStepDataID: vm.StepID,
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/evaluasi-teknis-vhs/detailEvaluator.modal.html',
				controller: 'detailEvaluatorVHSCtrl',
				controllerAs: 'dEvaluatorCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function () {
				loadContracts();
			});
		};

		vm.detail = detail;
		function detail(vendorID) {
			$state.transitionTo('detail-evaluasi-teknis-vhs', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType, VendorID: vendorID });
		}

		vm.kembali = kembali;
		function kembali() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.tenderStepData.TenderID });
		}
	}
})();

