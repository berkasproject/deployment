﻿(function () {
	'use strict';

	angular.module("app").controller("ApprovalVendRegModalCtrl", ctrl);

	ctrl.$inject = ['item', 'UIControlService', '$uibModalInstance', 'VendorRegistrationApprovalService'];
	function ctrl(item, UIControlService, $uibModalInstance, VendorRegistrationApprovalService) {
		var vm = this;
		vm.init = init;
		vm.item = item.item;

		function init() {
			//UIControlService.loadLoading("MESSAGE.LOADING");
		}

		vm.action = action;
		function action() {
			var data = {
				ID: vm.item.ID,
				ApprovalComment: vm.comment,
				VendorID: vm.item.VendorID
			};
			VendorRegistrationApprovalService.reject(data, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_REJECT");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_REJECT");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_REJECT");
				//console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}
	}
})()