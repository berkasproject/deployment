﻿(function () {
	'use strict';

	angular.module("app").controller("ApprovalVendRegCommodityModalCtrl", ctrl);

	ctrl.$inject = ['item', 'UIControlService', '$uibModalInstance', 'VendorRegistrationApprovalService'];
	function ctrl(item, UIControlService, $uibModalInstance, VendorRegistrationApprovalService) {
	    var vm = this;
	    vm.pageSize = 10;
		vm.init = init;
		vm.item = item.item;
		console.log(vm.item);

		function init() {
		    loadCommodity(1);
		}


		vm.loadCommodity = loadCommodity;
		function loadCommodity(current) {
		    vm.vendcommodityList = [];
		    vm.currentPage = current;
		    var offset = (current * 10) - 10;
		    VendorRegistrationApprovalService.loadCommodity({
		        Offset: offset,
		        Limit: vm.pageSize,
		        IntParam1: vm.item.VendorID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
                    console.log(reply)
		            vm.vendcommodityList = reply.data;
		            vm.totalItems = vm.vendcommodityList.length;
		            vm.maxSize = vm.totalItems;

		            for (var i = 0; i < vm.vendcommodityList.length; i++) {
		                if (vm.vendcommodityList[i].GoodsOrService == 3090) {
		                    vm.vendcommodityList[i].GoodsOrService = "VENDOR_TYPE_GOODS";
		                } else if (vm.vendcommodityList[i].GoodsOrService == 3091) {
		                    vm.vendcommodityList[i].GoodsOrService = "VENDOR_TYPE_SERVICE";
		                } else if (vm.vendcommodityList[i].GoodsOrService == 3092) {
		                    vm.vendcommodityList[i].GoodsOrService = "VENDOR_TYPE_GOODSANDSERVICE";
		                } else {
		                    vm.vendcommodityList[i].GoodsOrService = "";
		                }
		            }
		        } else {
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        if (err.data == null) {
		            UIControlService.unloadLoading();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            UIControlService.unloadLoading();
		        }
		    });
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}
	}
})()