﻿(function () {
	'use strict';

	angular.module("app").controller("RegistrationApprovalCtrl", ctrl);

	ctrl.$inject = ['VendorRegistrationApprovalService', '$translatePartialLoader', 'UIControlService', '$filter', '$uibModal', 'VerifikasiDataService'];

	function ctrl(VendorRegistrationApprovalService, $translatePartialLoader, UIControlService, $filter, $uibModal, VerifikasiDataService) {
		var vm = this
		vm.columnApprovalStatus = '2'
		vm.pageSize = 10;

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('tender-step-approval');
			UIControlService.loadLoading("MESSAGE.LOADING");
			loadDataApproval(1);
		}

		vm.registrationApprovalDetail = registrationApprovalDetail;
		function registrationApprovalDetail(data) {
			var item = {
			    VendorID: data.VendorID,
                ID: data.ID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/registrationApprovalDetail.html',
				controller: 'registrationApprovalDetailCtrl',
				controllerAs: 'registrationApprovalDetailCtrl',
				resolve: { item: function () { return item } }
			})
			modalInstance.result.then(function () {
				init()
			})
		}

		vm.loadDataApproval = loadDataApproval;
		function loadDataApproval(current) {
			vm.dataApproval = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			VendorRegistrationApprovalService.select({
				Keyword: vm.textSearch,
				Offset: offset,
				Limit: vm.pageSize,
				Status: vm.columnApprovalStatus
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.dataApproval = reply.data.List;
					vm.totalItems = reply.data.Count;
					vm.maxSize = vm.totalItems;
					//console.info("dataApproval:" + JSON.stringify(vm.dataApproval));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changedFilter = changedFilter
		function changedFilter() {
			loadDataApproval(1);
		}

		vm.approvalModal = approvalModal;
		function approvalModal(list, flag) {
			var data = { item: list, act: flag };

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/vendorRegistrationApproval/vendor-registration-approval.modal.html',
				controller: 'ApprovalVendRegModalCtrl',
				controllerAs: 'ApprovalVendRegModalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//init();
				loadDataApproval(vm.currentPage);
			});
		}

		vm.commodityDetail = commodityDetail;
		function commodityDetail(list, flag) {
		    var data = { item: list, act: flag };

		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/vendorRegistrationApproval/vendor-registration-approval-commodity.modal.html',
		        controller: 'ApprovalVendRegCommodityModalCtrl',
		        controllerAs: 'ApprovalVendRegCommodityModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        //init();
		        loadDataApproval(vm.currentPage);
		    });
		}

		vm.approve = approve;
		function approve(data) {
			//console.info("data:" + JSON.stringify(data));
			bootbox.confirm($filter('translate')('CONFIRM_APPROVE'), function (res) {
				if (res) {
					VendorRegistrationApprovalService.approve({
						ID: data.ID,
						VendorID: data.VendorID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_APPROVE");
							loadDataApproval(vm.currentPage);
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.FAIL_APPROVE");
						UIControlService.unloadLoading();
					});
				}
			});
		}
	}
})()