﻿(function () {
	'use strict';

	angular.module("app").controller("vhsfpaTechEvalResulDetermineCtrl", ctrl);

	ctrl.$inject = ['$stateParams', 'UIControlService', 'VhsFpaEvalResDetermineService', '$state', '$translatePartialLoader', '$filter'];

	function ctrl($stateParams, UIControlService, VhsFpaEvalResDetermineService, $state, $translatePartialLoader, $filter) {
		var vm = this;
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.StepID = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.results = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("vhsfpa-technical-eval-result");
			getStepData();
		}

		vm.Determine = determine;
		function determine() {
			//bootbox.confirm("MESSAGE.DETERMINECONFIRM", function (res) {
			//	if (res) {
			UIControlService.loadLoading('MESSAGE.SENDING');
			VhsFpaEvalResDetermineService.determine({
				Vendors: vm.Vendors,
				TenderStepDataId: vm.TenderStepDataId,
				IsInformedToVendor: vm.IsInformedToVendor,
				SummaryText: vm.SummaryText
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'NOTIFICATION.DETERMINATION.SUCCESS.MESSAGE', "NOTIFICATION.DETERMINATION.SUCCESS.TITLE");
					init();
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
				} else {
					$.growl.error({ message: "Send Approval Failed." });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
			//	}
			//});
		}

		vm.publish = publish;
		function publish() {

			bootbox.confirm($filter('translate')('MESSAGE.DETERMINECONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.PUBLISHING');
					VhsFpaEvalResDetermineService.publish({
						Vendors: vm.Vendors,
						TenderStepDataId: vm.TenderStepDataId,
						IsInformedToVendor: vm.IsInformedToVendor,
						SummaryText: vm.SummaryText
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'NOTIFICATION.DETERMINATION.SUCCESS.MESSAGE', "NOTIFICATION.DETERMINATION.SUCCESS.TITLE");
							init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Publish Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', {
				TenderRefID: vm.TenderRefID,
				ProcPackType: vm.ProcPackType,
                TenderID: vm.TenderID
			});
		}

		vm.listDropdown = [
			{ Value: true, Name: "PASSED" },
			{ Value: false, Name: "FAILED" }
		];

		function getStepData() {
			VhsFpaEvalResDetermineService.GetStepData({
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}, function (reply) {
				var result = reply.data;
				vm.StartDate = result.StartDate;
				vm.EndDate = result.EndDate;
				vm.TenderID = result.TenderID;
				vm.TenderName = result.TenderName;
				vm.Vendors = result.Vendors;
				vm.TenderStepDataId = result.TenderStepDataId;
				vm.IsInformedToVendor = result.IsInformedToVendor;
				vm.SummaryText = result.SummaryText;
				vm.IsPublished = result.IsPublished;

				if (result.VHSorFPA == "FPA") vm.title = "TITLE.FPA";
				else if (result.VHSorFPA == "VHS") vm.title = "TITLE.VHS";
				else vm.title = "TITLE";

				UIControlService.unloadLoading();
			}, function (error) {
				UIControlService.unloadLoading();
				// UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
			});
		}
	}
})();