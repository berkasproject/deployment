﻿(function () {
    'use strict';

    angular.module("app")
    .controller("reTenderCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModal', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PaketPengadaanCEService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModal, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, PaketPengadaanCEService, UIControlService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        var tenderPackID = item.TenderPackID;

        vm.tenderPackageCR = {};
        vm.tenderSteps = [];
        vm.selectedStep = {};
        vm.retenderVendors = [];

        vm.allowedSteps = {

        }

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal("");
            PaketPengadaanCEService.SelectByID({
                ID: tenderPackID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.tenderPackageCR = reply.data;
                vm.tenderSteps = reply.data.TenderPackageCRSteps;
                vm.tenderSteps[0].TenderStepName = 'ULANG_DARI_PEMBUATAN_PAKET';
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };
        
        vm.pilihVendor = pilihVendor;
        function pilihVendor() {
            var item = {
                TenderPackID: tenderPackID,
                editable: true
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/paket-pengadaan-ce/reTenderPilihVendor.modal.html',
                controller: 'reTenderPilihVendorController',
                controllerAs: 'rePVCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function (result) {
                vm.retenderVendors = result
            });
        }

        vm.save = save;
        function save() {

            if (!vm.tenderPackageCR.IsInternalOnly && !vm.selectedStep.Order) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_SELECTED_STEP');
                return;
            }

            if (!vm.tenderPackageCR.IsInternalOnly && vm.selectedStep.Order !== 1) {
                if (vm.selectedStep.FormTypeUrl === 'pendaftaran-lelang' ||
                    vm.selectedStep.FormTypeUrl === 'penandatanganan-kontrak') {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_CANNOT_START_FROM_THIS_STEP');
                    return;
                }

                if (vm.retenderVendors.length === 0) {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_SELECTED_VENDOR');
                    return;
                }
            }

            var confMessage = vm.selectedStep.Order === 1 || vm.tenderPackageCR.IsInternalOnly ?
                'CONFIRM_RECREATE_PACK' : 'CONFIRM_RETENDER';
            bootbox.confirm($filter('translate')('MESSAGE.' + confMessage), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");

                    if (vm.selectedStep.Order === 1 || vm.tenderPackageCR.IsInternalOnly) {
                        PaketPengadaanCEService.ReCreatePackage({
                            TenderPackID: tenderPackID
                        }, function (reply) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("notice", 'MESSAGE.SUCC_RECREATE_PACK');
                            $uibModalInstance.close();
                        }, function (error) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_RECREATE_PACK');
                        });
                    } else {
                        PaketPengadaanCEService.ReTenderPackage({
                            TenderPackID: tenderPackID,
                            ReTenderFrom: vm.selectedStep.Order,
                            RetenderVendors: vm.retenderVendors
                        }, function (reply) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("notice", 'MESSAGE.SUCC_RE_TENDER');
                            $uibModalInstance.close();
                        }, function (error) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_RE_TENDER');
                            if (error.Message.substr(0, 4) === "ERR_") {
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.' + error.Message));
                            }
                        });
                    }
                }
            })
        };
                

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();