(function () {
    'use strict';

    angular.module("app")
    .controller("paketPengadaanCECtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PaketPengadaanCEService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, PaketPengadaanCEService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;

        vm.contractRequisition = [];

        vm.statusLabels = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('paket-pengadaan-ce');
            vm.loadContracts();
        };

        vm.onSearchClick = onSearchClick;
        function onSearchClick(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            vm.loadContracts();
        }

        vm.onFilterTypeChange = onFilterTypeChange;
        function onFilterTypeChange(column) {
            vm.column = column;
        }

        vm.loadContracts = loadContracts;
        function loadContracts() {
            UIControlService.loadLoading(loadmsg);
            PaketPengadaanCEService.SelectCR({
                Keyword: vm.keyword,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.column
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.contractRequisition = reply.data.List;
                vm.contractRequisition.forEach(function (cr) {
                    cr.PublishedDateConverted = convertDate(cr.PublishedDate);
                });
                vm.totalItems = reply.data.Count;
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.detail = detail;
        function detail(dt) {
            $state.transitionTo('atur-paket-pengadaan', { contractRequisitionId: dt.ContractRequisitionID, tenderPackId : dt.ID });
        };

        vm.dokumen = dokumen;
        function dokumen(dt) {
            $state.transitionTo('contract-requisition-docs-ce', { contractRequisitionId: dt.ContractRequisitionID });
        };

        vm.revisiOE = revisiOE;
        function revisiOE(dt) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REVISE_OE'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading(loadmsg);
                    PaketPengadaanCEService.ReviseOE({
                        ID: dt.ID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", 'MESSAGE.SUCC_REVISE_OE');
                        loadContracts();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_REVISE_OE');
                        if (error.substring(0, 4) === 'ERR_') {
                            UIControlService.msg_growl("error", 'MESSAGE.' + error);
                        }
                    });
                }
            });
        }

        vm.retender = retender;
        function retender(dt) {
            var item = {
                TenderPackID : dt.ID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/paket-pengadaan-ce/reTenderPaket.modal.html?v=1.000003',
                controller: 'reTenderCtrl',
                controllerAs: 'reTenderCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadContracts();
            });
        }

        vm.cancelVariation = cancelVariation;
        function cancelVariation(dt) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CANCEL_VARIATION'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    PaketPengadaanCEService.CancelVariation({
                        ID: dt.ID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", 'MESSAGE.SUCC_CANCEL_VARIATION');
                        loadContracts();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_CANCEL_VARIATION');
                    });
                }
            })
        }

        vm.lihatVendor = lihatVendor;
        function lihatVendor(dt) {
            var item = {
                TenderPackID: dt.ID,
                editable: false
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/paket-pengadaan-ce/reTenderPilihVendor.modal.html',
                controller: 'reTenderPilihVendorController',
                controllerAs: 'rePVCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function (result) {
                vm.retenderVendors = result
            });
        }

        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
    }
})();