﻿(function () {
	'use strict';

	angular.module("app").controller("verKDCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'verKDService', '$state', 'UIControlService', '$stateParams'];
	function ctrl($translatePartialLoader, verKDService, $state, UIControlService, $stateParams) {
		var vm = this;

		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);

		vm.init = init;
		function init() {
			console.info(vm.IDTender);
			$translatePartialLoader.addPart('verifikasi-kelengkapan-dokumen');
			loadStep();
			loadData();
		}

		function loadData() {
			vm.data = [];
			UIControlService.loadLoading("");
			verKDService.select({
				Status: vm.IDTender,
				FilterType: vm.ProcPackType,
				column: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					reply.data.forEach(function (dt) {
						dt.OEDate = UIControlService.convertDateTime(dt.OEDate);
						dt.VerifiedDate = UIControlService.convertDateTime(dt.VerifiedDate);
						vm.data.push(dt);
					});
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function loadStep() {
			UIControlService.loadLoading("");
			verKDService.Step({
				ID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.step = data;
					UIControlService.loadLoading("");
					verKDService.isless3approved({
						ID: vm.IDStepTender
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.data == true) {
							loadData();
						} else {
							UIControlService.msg_growl("error", 'LESS_3_NOT_APPROVED');
							//backpengadaan();
						}

					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_STEP_APPROVAL');
					});
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.pindah = pindah;
		function pindah(data) {
			$state.transitionTo('detail-kd', { VendorID: data.VendorID, TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType });
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('verifikasi-dokumen-vhs-print', {
				TenderRefID: vm.IDTender,
				StepID: vm.IDStepTender,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, TenderID: vm.step.TenderID });
		}

	}
})();