(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'TenderVerificationService', 'UIControlService', 'RequisitionListService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, TenderVerificationService, UIControlService, RequisitionListService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var contractRequisitionId = item.contractRequisitionId;
        var loadmsg = "MESSAGE.LOADING";

        vm.item = item;
        vm.crApps = [];
        vm.employeeFullName = "";
        vm.employeeID = 0;
        vm.approverIDs = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');

            UIControlService.loadLoadingModal(loadmsg);
            CommonEngineService.GetLoggedEmployee(function (reply) {
                if (reply.status === 200) {
                    vm.employeeFullName = reply.data.FullName + ' ' + reply.data.SurName;
                    vm.employeeID = reply.data.EmployeeID;
                    vm.approverIDs.push(vm.employeeID);
                    CommonEngineService.GetDelegationEmployeeIds(function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            var delegationEmployeeIds = reply.data;
                            delegationEmployeeIds.forEach(function (deId) {
                                vm.approverIDs.push(deId);
                            });
                            vm.loadData();
                        } else {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
            });
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal(loadmsg);
            TenderVerificationService.GetCRApprovals({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.ProjectTitle = reply.data.ProjectTitle;
                    vm.StatusName = reply.data.StatusName;
                    vm.crApps = reply.data.ContractRequisitionApprovals;
                    vm.crApps.forEach(function (cra) {
                        cra.ApprovalDate = UIControlService.convertDate(cra.ApprovalDate);
                        cra.isToApprove = isCRToApprove(cra);
                        if (cra.ApprovalStatus !== null) {
                            cra.ApprovalStatus = cra.ApprovalStatus === true ? 'APPROVED' : 'REJECTED';
                        }
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
            });
        }

        function isCRToApprove(cra) {
            for (var i=0; i < vm.approverIDs.length; i++)
            {
                if (cra.EmployeeID === vm.approverIDs[i] && cra.ApprovalStatus === null)
                {
                    return true;
                }
            }
            return false;
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();