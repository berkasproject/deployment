﻿(function () {
    'use strict';

    angular.module("app")
    .controller("CancelPOCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'DataMonitoringService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, DataMonitoringService) {

        var vm = this;
        vm.Keyword = "";
        vm.init = init;
        function init() {
            vm.ID = item;
        };

        vm.save = save;
        function save() {
            if (vm.Keyword == "") {
                UIControlService.msg_growl('error', "Alasan tidak boleh kosong");
                return;
            }
            else {
                DataMonitoringService.cancelPO({ ID: vm.ID, Remark: vm.Keyword, IsRepeatNego: vm.RepeatNego },
                                  function (reply) {
                                      if (reply.status === 200) {
                                          UIControlService.msg_growl("success", "MESSAGE.SUC_CANCEL_PO");
                                          $uibModalInstance.close();
                                      }
                                  },
                                  function (err) {
                                      UIControlService.unloadLoadingModal();
                                  }
                              );
            }

        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();