﻿(function () {
    'use strict';

    angular.module("app")
            .controller("frmUploadCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.IdUpload = item.data.ID;
        vm.act = item.act;
        vm.Id = item.Id;
        vm.DetailVendorMonitoringPO = item.data.DetailVendorMonitoringPO;
        vm.action = "";
        vm.pathFile;
        vm.Description;
        vm.fileUpload;
        vm.size;
        vm.name;
        vm.type;
        vm.flag;
        vm.selectedForm;
        var allowed = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('monitoring-po');
            console.info(item.data);
            if (item.act == 0) {
                loadLibrary();
            }
            else generateFile();
        }
        vm.generateFile = generateFile;
        function generateFile() {
        UploadFileConfigService.getByPageName("PAGE.ADMIN.MONITORINGPO", function (response) {
            UIControlService.unloadLoading();
            if (response.status == 200) {
                vm.name = response.data.name;
                vm.idUploadConfigs = response.data;
                vm.idFileTypes = generateFilterStrings(response.data);
                vm.idFileSize = vm.idUploadConfigs[0];

            } else {
                UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                return;
            }
        }, function (err) {
            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
            UIControlService.unloadLoading();
            return;
        });
    }

    //get tipe dan max.size file - 2
    function generateFilterStrings(allowedTypes) {
        var filetypes = "";
        for (var i = 0; i < allowedTypes.length; i++) {
            filetypes += "." + allowedTypes[i].Name + ",";
        }
        return filetypes.substring(0, filetypes.length - 1);
    }

        vm.loadLibrary = loadLibrary;
        function loadLibrary() {
            DataMonitoringService.selectLibraryShipping({},
                  function (reply) {
                      UIControlService.unloadLoadingModal();
                      if (reply.status === 200) {
                          vm.librarySI = reply.data;

                      }
                      else {
                          UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
                          return;
                      }
                  },
                  function (err) {
                      UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                      UIControlService.unloadLoadingModal();
                  }
              );
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        vm.saveAll = saveAll;
        function saveAll(data) {
            DataMonitoringService.InsertDoc({
                Keyword: data,
                Status: vm.DetailVendorMonitoringPO
            },
                  function (reply) {
                      UIControlService.unloadLoadingModal();
                      if (reply.status === 200) {
                          UIControlService.msg_growl("success", "MESSAGE.SAVE_FILE");
                          $uibModalInstance.close();

                      }
                      else {
                          UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
                          return;
                      }
                  },
                  function (err) {
                      UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                      UIControlService.unloadLoadingModal();
                  }
              );
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            

            UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleMonitoringPO(vm.Id, file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = url;
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        if (vm.flag == 0) {
                            
                            vm.size = Math.floor(s)
                        }

                        if (vm.flag == 1) {
                            vm.size = Math.floor(s/(1024));
                        }
                        DataMonitoringService.InsertDoc({
                            Keyword: vm.pathFile,
                            Status: vm.DetailVendorMonitoringPO
                        },
                  function (reply) {
                      UIControlService.unloadLoadingModal();
                      if (reply.status === 200) {
                          UIControlService.msg_growl("success", "MESSAGE.SAVE_UPLOAD");
                          $uibModalInstance.close();

                      }
                      else {
                          UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
                          return;
                      }
                  },
                  function (err) {
                      UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                      UIControlService.unloadLoadingModal();
                  }
              );
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                    UIControlService.unloadLoading();
                });

            

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();