﻿(function () {
    'use strict';

    angular.module("app")
            .controller("AddFPAUploadItemCtrl", ctrl);

    ctrl.$inject = ['ExcelReaderService', '$timeout', 'Excel', 'item','$uibModalInstance', '$state', '$stateParams', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl(ExcelReaderService, $timeout, Excel, item, $uibModalInstance, $state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.action = "";
        vm.pathFile;
        vm.Description;
        vm.fileUpload;
        vm.size;
        vm.name;
        vm.type;
        vm.flag;
        vm.selectedForm;
        vm.data = item.Item;
        vm.listItem = item.Item.listItem;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadTypeSizeFile();
        }

        function loadTypeSizeFile() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.CONTRACTREQUISITION.DOCS", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                return;
            });
        }

        vm.Export = Export;
        function Export(tableId) {
            vm.exportHref = Excel.tableToExcel(tableId, 'sheet name');
            $timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
        }

        //get tipe dan max.size file - 2
        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        //vm.fileUpload;
        function selectUpload() {
            if (vm.fileUpload[0].type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                vm.type = "xlsx";
            else if (vm.fileUpload[0].type == "application/vnd.ms-excel")
                vm.type = "xls";
            else if (vm.fileUpload[0].type == "application/pdf")
                vm.type = "pdf";
            else if (vm.fileUpload[0].type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                vm.type = "docx";
            else if (vm.fileUpload[0].type =="application/msword")
                vm.type = "doc";
            else if (vm.fileUpload[0].type == "image/png")
                vm.type = "png";
            else if (vm.fileUpload[0].type == "image/jpeg")
                vm.type = "jpg";
        }
        /*start upload */
        vm.uploadFile = uploadFile;
        function uploadFile() {

            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }            
            /*
            var selectedFileType = file[0].type;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
            
            if (selectedFileType === "vnd.ms-excel") {
                selectedFileType = "xls";
            }
            else if (selectedFileType === "vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                selectedFileType = "xlsx";
            }
            else {
                selectedFileType = selectedFileType;
            }
            vm.type = selectedFileType;
            console.info("filenew:" + selectedFileType);
            //jika excel
            if (selectedFileType === "vnd.ms-excel")
                var allowed = false;

            
            for (var i = 0; i < allowedFileTypes.length; i++) {
                
                if (allowedFileTypes[i].Name == selectedFileType) {
                    allowed = true;
                   
                    return allowed;
                }
            }
            if (!allowed) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_INVALID_FILETYPE");
                return false;
            }
            */
            return true;
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = url;
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        if (vm.flag == 0) {
                            
                            vm.size = Math.floor(s)
                        }

                        if (vm.flag == 1) {
                            vm.size = Math.floor(s/(1024));
                        }

                         
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

            

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };


        vm.ReadFile = ReadFile;
        function ReadFile() {
            ExcelReaderService.readExcel(vm.fileUpload,
                function (reply) {
                    if (reply.status === 200) {
                        var excelContents = reply.data;
                        var sheet1 = excelContents[Object.keys(excelContents)[0]];
                        vm.list = [];
                        vm.list = sheet1;
                        vm.dataplusplus = 1;
                        vm.flag = 0;
                        vm.listExcel = [];
                        cekExcel(sheet1);

                    }
                });
        }

        vm.cekExcel = cekExcel;
        function cekExcel(data) {
            vm.newExcel = [];
            for (var i = 2; i < vm.list.length; i++) {
                vm.iplus = i;
                var objExcel = {
                    MaterialCode: vm.list[i].Column1,
                    ItemDescrip: vm.list[i].Column2,
                    Manufacture: vm.list[i].Column3,
                    PartNo: vm.list[i].Column4,
                    Estimate: vm.list[i].Column5,
                    Unit: vm.list[i].Column6,
                    Currency: vm.list[i].Column7,
                    UnitPrice: vm.list[i].Column8,
                    LeadTime: vm.list[i].Column9,
                    CountryOfOrigin: vm.list[i].Column10,
                    Remark: vm.list[i].Column11,
                    VHSAwardId: vm.data.VHSAwardId
                };
                vm.newExcel.push(objExcel);
                if (i == (vm.list.length - 1)) {
                    DataFPAService.cekExcel(vm.newExcel, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            if (reply.data =="") {
                                var data = {
                                    listItem: vm.newExcel,
                                    fileUpload: vm.fileUpload
                                }
                                $uibModalInstance.close(data);
                            }
                            else {
                                UIControlService.msg_growl("error", reply.data);
                                UIControlService.unloadLoading();
                            }
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                    });
                }

            }
        }



    }
})();