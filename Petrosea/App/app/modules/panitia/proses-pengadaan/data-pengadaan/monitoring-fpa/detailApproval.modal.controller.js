(function () {
	'use strict';

	angular.module("app")
    .controller("detailApprovalCtrl", ctrl);

	ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'DataFPAService'];
	/* @ngInject */
	function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, DataFPAService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.ID = item.data;
		vm.crApps = [];
		vm.employeeFullName = "";
		vm.employeeID = 0;
		vm.information = "";
		vm.flagEmp = item.flag;
		vm.flagReject = item.flagReject;



		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('verifikasi-tender');
            if(item.flagReject != true)
			loadData();
		};

		vm.loadData = loadData;
		function loadData() {
			vm.crApps = [];
			UIControlService.loadLoading(loadmsg);
			DataFPAService.GetListApproval({
				Status: vm.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = reply.data;
					for (var i = 0; i < vm.list.length; i++) {
						vm.crApps.push({
							ID: vm.list[i].ID,
							EmployeeID: vm.list[i].EmployeeID,
							ApprovalDate: UIControlService.convertDateTime(vm.list[i].ApprovalDate),
							ApprovalStatus: vm.list[i].ApprovalStatus,
							Remark: vm.list[i].Remark,
							EmployeeFullName: vm.list[i].MstEmployee.FullName + ' ' + vm.list[i].MstEmployee.SurName,
							EmployeePositionName: vm.list[i].MstEmployee.PositionName,
							EmployeeDepartmentName: vm.list[i].MstEmployee.DepartmentName,
						});
					}
				} else {
					UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
			});
		}


		vm.approve = approve;
		function approve() {
			sendApproval(1);
		}

		vm.reject = reject;
		function reject() {
			sendApproval(0);
		}

		function sendApproval(approvalStatus) {
			UIControlService.loadLoadingModal(loadmsg);
			DataFPAService.UpdateAddendumApproval({
			    ID: vm.ID.ID,
                AddendumId: vm.ID.AddendumId,
			    ApprovalStatus: false,
				Remark: vm.information
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    UIControlService.msg_growl("success", "Approval Success");
				    sendMailToApproval();
					$uibModalInstance.close();
				} 
			}, function (error) {
				UIControlService.unloadLoadingModal();
			});
		}

		vm.sendMailToApproval = sendMailToApproval;
		function sendMailToApproval() {
		    DataFPAService.sendMailToApproval({
		        AddendumId: vm.ID.AddendumId,
                approvalStatus: false
		    },
            function (reply) {
                UIControlService.msg_growl("success", 'Email Sent !!!');
                UIControlService.unloadLoading();
            },
            function (error) {
                UIControlService.unloadLoading();
            });
		};

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();