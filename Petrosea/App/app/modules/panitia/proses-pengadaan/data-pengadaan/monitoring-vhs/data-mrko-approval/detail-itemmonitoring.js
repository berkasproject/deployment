﻿(function () {
    'use strict';

    angular.module("app").controller("DetailItemMRKOCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'DataVHSService'];
    /* @ngInject */
    function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, DataVHSService) {

        var vm = this;
        vm.ID = item.item.IdUpload;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.DocName = "";
        vm.DocumentUrl = "",
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.init = init;
        function init() {
            vm.item = item.item;
            vm.Periode1 = UIControlService.convertDate(item.Periode1);
            vm.Periode2 = UIControlService.convertDate(item.Periode2);
            vm.DataDocList = item.item.UploadDataDetailMRKO;
            vm.totalItems = item.item.Count;
            vm.jumlahUSD = item.item.TotalInUSD;
            $translatePartialLoader.addPart('add-adendum');
        };

        vm.loadPaket = loadPaket;
        function loadPaket(current) {
            console.info(current);
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.pageSize) - vm.pageSize;
            UIControlService.loadLoading(loadmsg);
            DataVHSService.DetailDocUpload({
                Status: vm.ID,
                Offset: offset,
                Limit: vm.pageSize
            },
                   function (response) {
                       UIControlService.unloadLoading();
                       if (response.status == 200) {
                           vm.DataDocList = response.data.List;
                           
                       }
                   },
                   function (response) {
                       UIControlService.unloadLoading();
                   });
        }

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.back = back;
        function back() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
