﻿(function () {
    'use strict';

    angular.module("app").controller("dataMRKOCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.id = 0;
        vm.maxSize = 10;
        vm.listVHS = [];
        vm.searchBy = 0;
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('data-mrko');
            getDataForApproval(1);
        };

        vm.getDataForApproval = getDataForApproval;
        function getDataForApproval() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.getDataForApproval({
                Keyword: vm.keyword,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.searchBy
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listVHS = reply.data;
                    vm.VHSdata = data;
                    vm.totalItems = Number(data.Count);

                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
        };


        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.Approval = Approval;
        function Approval(data) {
            var data = {
                ID: data.ID,
                MonitoringVHSMRKOId: data.MonitoringVHSMRKOId,
                Status:1
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko-approval/DetailApproval.html',
                controller: "detailApprovalMRKOCtrl",
                controllerAs: "detailApprovalMRKOCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.toDetail = toDetail;
        function toDetail(data) {
            var item = {
                Periode1: data.monitoringVHSMRKO.Periode1,
                Periode2 : data.monitoringVHSMRKO.Periode2,
                item: data.monitoringVHSMRKO
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko/detail-itemmonitoring.html',
                controller: 'DetailItemMRKOCtrl',
                controllerAs: 'DetailItemMRKOCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();

            });
        }

        vm.approve = approve;
        function approve(data) {
            bootbox.confirm($filter('translate')('Apakah anda yakin ingin menyetujui pembuatan MRKO ini ?'), function (yes) {
                if (yes) {
                    DataVHSService.SendApproval({
                        ID: data.ID,
                        ApprovalStatus: true,
                        Remark: "",
                        MonitoringVHSMRKOId: data.MonitoringVHSMRKOId
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", $filter('translate')('Berhasil Approval'));
                            init();
                        } 
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                    });
                }
            });
        }
    }
})();
