﻿(function () {
    'use strict';

    angular.module("app").controller("PriceCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        vm.fileUpload;
        vm.datalempar = item.datalempar;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.DocName = "";
        vm.DocumentUrl = "",
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.DetailDocList = [];
        vm.init = init;


        vm.AdditionalValue = vm.datalempar.AdditionalValue;
        vm.BudgetContract = vm.datalempar.BudgetContract;
        vm.TypeAddendum = vm.datalempar.TypeAddendum;
        vm.StartDate = vm.datalempar.StartDate;
        vm.EndDate = vm.datalempar.EndDate;
        vm.Duration = vm.datalempar.Duration;
        vm.Requestor = vm.datalempar.Requestor;
        vm.RequestDate = vm.datalempar.RequestDate;
        vm.DocUrl = vm.datalempar.DocUrl;
        vm.DocName = vm.datalempar.DocName;
        vm.VendorID = vm.datalempar.VendorID;
        vm.TenderStepID = vm.datalempar.TenderStepID;
        vm.Remask = vm.datalempar.Remask;


        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        var id = vm.datalempar.id;
        vm.id = vm.datalempar.id;
        var negoid = vm.datalempar.negoid;
        vm.negoid = vm.datalempar.negoid;

        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadPaket();
        };

        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.DetailDoc({
                Status: vm.negoid,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.column
            },
                   function (response) {
                       if (response.status == 200) {
                           var DataDocList = response.data;
                           vm.DataDocList = DataDocList;
                           vm.DetailDocList = response.data.List;

                           vm.totalItems = Number(DataDocList[0].Count);
                           UIControlService.unloadLoading();
                           console.info("TenderStepID :" + JSON.stringify(vm.TenderStepID));
                           console.info("VendorID :" + JSON.stringify(vm.VendorID));
                       } else {
                           UIControlService.handleRequestError(response.data);
                       }
                   },
                   function (response) {
                       UIControlService.handleRequestError(response.data);
                       UIControlService.unloadLoading();
                   });
        }

        vm.save = save;
        function save() {

        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss();
        };

        vm.Export = Export;
        function Export(tableId, index) {
            var iplus = index + 1;
            JSONToCSVConvertor(tableId, true, iplus);
        }

        vm.JSONToCSVConvertor = JSONToCSVConvertor;
        function JSONToCSVConvertor(JSONData, ShowLabel, idata) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = JSONData;
            console.info(arrData[0]);
            var CSV = '';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "sep=," + '\n';

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);
                console.info(row);
                //append Label row with line break
                CSV += row + '\r\n';
                console.info(CSV);
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);
                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "Dokumen Addendum";

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        vm.upload = upload;
        function upload() {
            var lempar = {
                datalempar: {
                    negoid: negoid,
                    id: id,
                    AddendumCode: vm.AddendumCode,
                    TypeAddendum: vm.TypeAddendum,
                    BudgetContract: vm.BudgetContract,
                    AdditionalValue: vm.AdditionalValue,
                    RequestDate: vm.RequestDate,
                    StartDate: vm.StartDate,
                    EndDate: vm.EndDate,
                    Requestor: vm.Requestor,
                    VendorID: vm.VendorID,
                    TenderStepID: vm.TenderStepID,
                    VHSAwardId: vm.id,
                    Remask: vm.Remask,
                    Duration: vm.Duration,
                    NegoId: vm.NegoId,
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/upload-file-vhs.html',
                controller: 'UploadPriceCtrl',
                controllerAs: 'UploadPriceCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();

            });

        };
        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
    }
})();
