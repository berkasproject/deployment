﻿(function () {
    'use strict';

    angular.module("app").controller("UploadMRKO", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService', 'ExcelReaderService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService, ExcelReaderService) {

        var vm = this;
        vm.fileUpload;
        //vm.datalempar = item.datalempar;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.DocName = "";
        vm.DocumentUrl = "",
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.DetailDocList = [];
        vm.init = init;
        vm.tglSekarang = UIControlService.getDateNow("");

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
            $translatePartialLoader.addPart('detail-dataupload-po');
            $translatePartialLoader.addPart('dataupload-po');
            loadTypeSizeFile();
            loadUploadDataMRKO(1);
        };

        vm.loadUploadDataMRKO = loadUploadDataMRKO;
        function loadUploadDataMRKO(current) {
            vm.currentPage = current;
            UIControlService.loadLoading(loadmsg);
            DataVHSService.selectUploadDataMRKO({
                Status: vm.negoid,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.column
            },
                   function (response) {
                       if (response.status == 200) {
                           vm.listDoc = response.data.List;
                           vm.totalItems = Number(response.data.Count)
                           UIControlService.unloadLoading();
                       } else {
                           UIControlService.handleRequestError(response.data);
                       }
                   },
                   function (response) {
                       UIControlService.handleRequestError(response.data);
                       UIControlService.unloadLoading();
                   });
        }

        function loadTypeSizeFile() {
            UploadFileConfigService.getByPageName("PAGE.ADMIN.MASTER.ITEMPR", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                }
            }, function (err) {
                UIControlService.unloadLoading();
                return;
            });
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("error", "MSG_NOFILE");
                return;
            }

            if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
            }
        }

        function upload(file, config, filters, dates, callback) {
            //console.info(file);
            var size = config.Size;

            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.pathFile = url;
                        uploadSave(fileName, vm.pathFile);
                    } else {
                        UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

        }

        vm.uploadSave = uploadSave;
        function uploadSave(filename, url) {
            DataVHSService.InsertFile({
                FileName: filename,
                DocUrl: url
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                    vm.IdUpload = reply.data.ID;
                    saveExcelContent();
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }

        function saveExcelContent() {
            vm.newExcel = [];
            ExcelReaderService.readExcel(vm.fileUpload,
                function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        var excelContents = reply.data;
                        var Sheet1 = excelContents[Object.keys(excelContents)[0]]; /*untuk baca nama sheet*/
                        console.info(Sheet1);
                        var countproperty = numAttrs(Sheet1[0]);
                        for (var a = 1; a < Sheet1.length; a++) {
                            if (Sheet1[a].Column2 !== null) {
                                var objExcel = {
                                    StorageLocation: Sheet1[a].Column2,
                                    PostingDate1: Sheet1[a].Column3,
                                    Reservation: Sheet1[a].Column4,
                                    MovementType: Sheet1[a].Column5,
                                    SpecialStock: Sheet1[a].Column6,
                                    VendorCode: Sheet1[a].Column7,
                                    VendorName: Sheet1[a].Column8,
                                    MaterialDocument: Sheet1[a].Column9,
                                    MaterialDocItem: Sheet1[a].Column10,
                                    Material: Sheet1[a].Column11,
                                    MaterialDescription: Sheet1[a].Column12,
                                    Qty: Sheet1[a].Column13,
                                    Unit: Sheet1[a].Column14,
                                    Currency: Sheet1[a].Column15,
                                    UnitPrice: Sheet1[a].Column16,
                                    TotalValue: Sheet1[a].Column17,
                                    IdUpload: vm.IdUpload,
                                    UnitPricePIR: Sheet1[a].Column18,
                                    TotalValuePIR: Sheet1[a].Column19,
                                    VarianceMRKO: Sheet1[a].Column20,
                                    UserIdReservation: Sheet1[a].Column21,
                                    UsernameReservation: Sheet1[a].Column22,
                                    UserIdGI: Sheet1[a].Column23,
                                    UsernameGI: Sheet1[a].Column24,
                                    CostCenter: Sheet1[a].Column25,
                                    OrderId: Sheet1[a].Column26,
                                    WBSElement: Sheet1[a].Column27,
                                    GLAcount: Sheet1[a].Column28,
                                    Network: Sheet1[a].Column29,
                                    TaxCodeInMatDoc: Sheet1[a].Column30,
                                    TaxCodeInPur: Sheet1[a].Column31,
                                    ReservationItem: Sheet1[a].Column32,
                                    SlocDesc: Sheet1[a].Column33,
                                    Manufacturer: Sheet1[a].Column34,
                                    PartNo: Sheet1[a].Column35,
                                    ContractNumber: Sheet1[a].Column36,
                                    ItemNoInContractDoc: Sheet1[a].Column37,
                                    PurchGroup: Sheet1[a].Column38,
                                };
                                vm.newExcel.push(objExcel);
                            }
                        }
                        uploadFileExcel(vm.newExcel);
                    }
                });
        }

        vm.uploadFileExcel = uploadFileExcel;
        function uploadFileExcel(data) {
            DataVHSService.InsertExcel(data, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                    vm.fileUpload = null;
                    vm.init();
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }
       
        vm.numAttrs = numAttrs;
        function numAttrs(obj) {
            var count = 0;
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    ++count;
                }
            }
            return count;
        }

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.deleteFile = deleteFile;
        function deleteFile(id) {
            DataVHSService.DeleteFile({ ID: id }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MSG_SUCCESS_SAVE");
                    vm.init();
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.toDetail = toDetail;
        function toDetail(id) {
            var item = {
                ID: id
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko/detail-itemmrko-modal.html',
                controller: 'DetailItemMRKOModalCtrl',
                controllerAs: 'DetailItemMRKOCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();

            });

        };

        vm.toCompare = toCompare;
        function toCompare(id) {
            DataVHSService.Compare({ ID: id }, function (reply) {
                if (reply.status === 200) {
                    if (reply.data != "") {
                        UIControlService.msg_growl("error", reply.data);
                    }
                    else {
                        UIControlService.msg_growl("success", "Success Compare");
                    }
                    vm.init();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }
    }
})();
