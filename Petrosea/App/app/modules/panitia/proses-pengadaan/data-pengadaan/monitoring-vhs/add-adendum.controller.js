﻿(function () {
    'use strict';

    angular.module("app").controller("AddVHSCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.fileUpload;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.VHSdata;
        vm.listVHS = [];
        vm.currencyList = [];
        vm.init = init;
        var id = Number($stateParams.id);
        vm.id = Number($stateParams.id);
        var negoid = Number($stateParams.negoid);
        vm.negoid = Number($stateParams.negoid);
        vm.isCalendarOpened = [false, false, false];
        vm.datetoStart;
        vm.datetoEnd;
        vm.VA;
        vm.Remask = '';
        vm.AdditionalValue = 0;
        vm.Budget_Val = 0;
        vm.TypeAddendum = 1;
        vm.StartDate;
        vm.EndDate;
        vm.Duration = 0;
        vm.Requestor = 1;
        vm.RequestDate = '';
        vm.DocUrl = "";
        vm.DocName = "";
        vm.DetailCheck = 0;
        vm.AddendumMonth = "Months";
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadTypeSizeFile(1);
            loadCurrencies(1);
            loadCountry(1);
            loadData(1);
            loadPaket();

        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.AddAddendum({
                Offset: vm.pageSize * (vm.currentPage - 1),
                Status: vm.id,
                Limit: vm.pageSize,
                Column: vm.column
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listVHS = reply.data.List;
                    vm.VHSdata = data;
                    vm.totalItems = Number(data.Count);
                    vm.VendorID = vm.VHSdata.VendorID;
                    vm.TenderStepID = vm.VHSdata.TenderStepID;
                    vm.EndDate = vm.VHSdata.EndDate;
                    vm.datetoEnd = "";
                    if (vm.StartDate == null) {
                        vm.datetoStart = vm.VHSdata.FinishContractDate;
                    } else {
                        vm.datetoStart = vm.EndDate;
                    }
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        function loadTypeSizeFile() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.CONTRACTREQUISITION.DOCS", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }
        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }
        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }
        vm.save = save;
        function save() {
            //if (!vm.RequestDate || !vm.AddendumCode) {
            //    UIControlService.msg_growl("error", "Lengkapi Data Yang Kosong");
            //    UIControlService.unloadLoading();
            //    return;
            //}
            //if (!vm.DocUrl && !vm.fileUpload) {

            //    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
            //    UIControlService.unloadLoading();
            //    return;

            //}

            //if (vm.fileUpload) {
            //    uploadFile();
            //}
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }
        vm.ChangeDuration = ChangeDuration;
        function ChangeDuration() {
            
            vm.EndDate = UIControlService.getStrDate(UIControlService.addMonth(vm.AddendumMonth));
        }


        /*proses upload file*/
        vm.uploadFile = uploadFile;
        function uploadFile() {
            var folder = "Addendum_" + vm.id + vm.DocName;
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("error", "MESSAGE.MSG_NOFILE");
                return;
            }

            if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder);
            }
        }
        function upload(file, config, filters, folder, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, folder,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var nameDoc = "Addendum_" + vm.id;

                        vm.DocUrl = url;
                        vm.DocName = nameDoc;
                        vm.pathFile = vm.folderFile + url;
                        // console.info("sendata:" + JSON.stringify(fileName));

                        UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPLOAD");
                        saveProcess(url, nameDoc, size);

                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

        }
        /* end proses upload*/


        function saveProcess(docurl, docname, docsize) {
            if (vm.TypeAddendum == 1 || vm.TypeAddendum == 3 || vm.TypeAddendum == 4) {
                vm.datetoStart = vm.VHSdata.FinishContractDate;
                vm.datetoEnd = vm.VHSdata.FinishContractDate;

            }
            if (vm.TypeAddendum == 3) {
                vm.AdditionalValue = vm.PriceIDR;
            }
            var senddata = {
                AddendumCode: vm.AddendumCode,
                TypeAddendum: vm.TypeAddendum,
                BudgetContract: vm.Budget_Val,
                AdditionalValue: vm.AdditionalValue,
                RequestDate: vm.RequestDate,
                DocUrl: docurl,
                DocName: docname,
                StartDate: vm.datetoStart,
                EndDate: vm.datetoEnd,
                Requestor: vm.Requestor,
                VendorID: vm.VendorID,
                TenderStepID: vm.TenderStepID,
                VHSAwardId: vm.id,
                Remask: vm.Remask,
                Duration: vm.Duration,

                MaterialCode: vm.material_code,
                ItemDescrip: vm.item_des,
                Manufacture: vm.manufacture,
                PartNo: vm.part_no,
                Estimate: vm.estimation,
                Unit: vm.uop,
                Currency: vm.currency,
                PriceIDR: vm.PriceIDR,
                LeadTime: vm.eadtim,
                Remark: vm.Remask,
                CountryOfOrigin: vm.country,
                NegoId: vm.negoid,

            }
            DataVHSService.CreateAddendum(senddata, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.MESSAGE_SUCCESS");
                    $uibModalInstance.close();

                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.MESSAGE_FAILED");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }
        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.loadCurrencies = loadCurrencies;
        function loadCurrencies() {
            DataVHSService.Currency(
                function (response) {
                    if (response.status == 200) {
                        vm.currencyList = response.data;
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }
        function loadCountry() {
            DataVHSService.Country(
                function (response) {
                    if (response.status == 200) {
                        vm.NegaraList = response.data;
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }

        vm.modalPrice = modalPrice;
        function modalPrice() {
            var lempar = {
                datalempar: {
                    negoid: negoid,
                    id: id,
                    AddendumCode: vm.AddendumCode,
                    TypeAddendum: vm.TypeAddendum,
                    BudgetContract: vm.Budget_Val,
                    AdditionalValue: vm.AdditionalValue,
                    RequestDate: vm.RequestDate,
                    StartDate: vm.datetoStart,
                    EndDate: vm.datetoEnd,
                    Requestor: vm.Requestor,
                    VendorID: vm.VendorID,
                    TenderStepID: vm.TenderStepID,
                    VHSAwardId: vm.id,
                    Remask: vm.Remask,
                    Duration: vm.Duration,

                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/price.modal.html',
                controller: 'PriceCtrl',
                controllerAs: 'PriceCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();

            });

        };
        vm.getMon = getMon;
        function getMon(duration) {
            var date = new Date(vm.datetoStart);
            var oldDate = date.getDate();
            var oldMonth = date.getMonth();
            var oldYear = date.getFullYear();

            var newMonth = oldMonth + parseInt(duration);
            var datetoEnd = new Date(oldYear, newMonth, oldDate);

            vm.datetoEnd = new Date(oldYear, newMonth, oldDate);
            vm.EndDate = vm.datetoEnd;
            return datetoEnd;
        };
        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.DetailDoc({
                Status: vm.negoid,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: 1
            },
                   function (response) {
                       if (response.status == 200) {
                           var DataDocList = response.data;
                           vm.DataDocList = DataDocList;
                           vm.DetailDocList = response.data.List;

                           vm.totalItems = Number(DataDocList[0].Count);
                           UIControlService.unloadLoading();
                       } else {
                           UIControlService.handleRequestError(response.data);
                       }
                   },
                   function (response) {
                       UIControlService.handleRequestError(response.data);
                       UIControlService.unloadLoading();
                   });
        }

        vm.modalNewItem = modalNewItem;
        function modalNewItem() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/add-newitem-adendum.html',
                controller: 'AddVHSNewItemCtrl',
                controllerAs: 'AddVHSNewItemCtrl'
            });
            modalInstance.result.then(function (data) {
                vm.DataDocList[0].push({
                    MaterialCode: data.material_code,
                    ItemDescrip: data.item_des,
                    Manufacture: data.manufacture,
                    PartNo: data.part_no,
                    Estimate: data.estimation,
                    Unit: data.uop,
                    Currency: data.currency,
                    PriceIDR: data.PriceIDR,
                    LeadTime: data.eadtim,
                    CountryOfOrigin: data.country,
                    Remark: data.Remark
                });

            });
        }

        vm.upload = upload;
        function upload() {
            console.info("dd");
            var datalempar = {
                negoid: negoid,
                id: id,
                AddendumCode: vm.AddendumCode,
                TypeAddendum: vm.TypeAddendum,
                BudgetContract: vm.Budget_Val,
                AdditionalValue: vm.AdditionalValue,
                RequestDate: vm.RequestDate,
                StartDate: vm.RequestDate,
                EndDate: vm.EndDate,
                Requestor: vm.Requestor,
                VendorID: vm.VendorID,
                TenderStepID: vm.TenderStepID,
                VHSAwardId: vm.id,
                Remask: vm.Remask,
                Duration: vm.Duration,
                NegoId: negoid,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/upload-file-vhs.html',
                controller: 'UploadPriceCtrl',
                controllerAs: 'UploadPriceCtrl',
                resolve: {
                    item: function () {
                        return datalempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                

            });

        };

    }
})();
