﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalMRKOCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'DataVHSService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, DataVHSService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.ID = item.ID;
        vm.MonitoringVHSMRKOId = item.MonitoringVHSMRKOId;
        vm.flag = item.flag;
        vm.Status = item.Status;
        vm.crApps = [];
        vm.employeeFullName = "";
        vm.employeeID = 0;
        vm.information = "";
        vm.flagEmp = item.flag;

        console.info(item);
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');
            loadApproval();
        };


        vm.loadApproval = loadApproval;
        function loadApproval(approvalStatus) {
            UIControlService.loadLoadingModal(loadmsg);
            DataVHSService.GetApproval({
                Status: vm.MonitoringVHSMRKOId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.listApproval = reply.data;
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();