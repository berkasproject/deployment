﻿(function () {
	'use strict';

	angular.module("app").controller("DetailItemMRKOCtrl", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'DataVHSService'];
	/* @ngInject */
	function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, DataVHSService) {

		var vm = this;
		vm.ID = item.item.IdUpload;
		var loadmsg = "MESSAGE.LOADING";
		vm.currentPage = 1;
		vm.DocName = "";
		vm.DocumentUrl = "",
        vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.maxSize = 10;
		vm.init = init;
		function init() {
			vm.item = item.item;
			vm.Periode1 = UIControlService.convertDate(item.Periode1);
			vm.Periode2 = item.Periode2;
			vm.DataDocList = item.item.UploadDataDetailMRKO;
			vm.totalItems = item.item.Count;
			vm.jumlahUSD = item.item.TotalInUSD;
			vm.TotalRealCurrency = item.item.TotalRealCurrency
			$translatePartialLoader.addPart('add-adendum');
			loadPaket(1);
		};

		vm.loadPaket = loadPaket;
		function loadPaket(current) {
			vm.currentPage = current;
			var offset = (vm.currentPage * vm.pageSize) - vm.pageSize;
			UIControlService.loadLoading(loadmsg);
			DataVHSService.ViewToCreate({
				Status: item.ID,
				Offset: offset,
				Limit: vm.pageSize,
				Date1: UIControlService.getStrDate(item.Periode1),
				Date2: UIControlService.getStrDate(item.Periode2),
				FilterType: 0
			}, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.data = response.data;
					for (var i = 0; i < vm.data.length; i++) {
						if (vm.data[i].ID == item.ID) {
							vm.DataDocList = vm.data[i].UploadDataDetailMRKO;
							vm.totalItems = vm.data[i].Count;
							vm.jumlahUSD = vm.data[i].TotalInUSD;
						}

					}

				}
			}, function (response) {
				UIControlService.unloadLoading();
			});
		}

		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.back = back;
		function back() {
			$uibModalInstance.dismiss('cancel');
		}
	}
})();
