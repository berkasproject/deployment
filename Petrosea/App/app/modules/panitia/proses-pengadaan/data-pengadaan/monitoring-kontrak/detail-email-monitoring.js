﻿(function () {
    'use strict';

    angular.module("app").controller("DetEmailMonCtrl", ctrl);

    ctrl.$inject = ['$uibModalInstance', 'item','$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataKontrakService', 'UIControlService'];
    /* @ngInject */
    function ctrl($uibModalInstance, item, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataKontrakService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.list = item.item;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.searchBy = 0;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('monitoring-kontrak');
            getContent();
        };

        vm.getContent = getContent;
        function getContent() {
            UIControlService.loadLoading(loadmsg);
            DataKontrakService.getContent({
                Status: vm.list.TenderStepID

            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    for (var i = 0; i < reply.data.length; i++) {
                        if (reply.data[i].IsAward === true) vm.ContentAward = reply.data[i].EmailContent;
                        else vm.ContentLooser = reply.data[i].EmailContent;
                    }
                }
            },
            function (error) {
                UIControlService.unloadLoading();
            });
                        
        };

        vm.sentMail = sentMail;
        function sentMail() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            DataKontrakService.InsertEmail({
                ID: vm.list.ID,
                EmailContentAward: vm.ContentAward,
                EmailContentLooser: vm.ContentLooser
            },
           function (reply) {
               UIControlService.unloadLoadingModal();
               if (reply.status === 200) {
                   UIControlService.msg_growl('success', "MESSAGE.EMAIL");
                   $uibModalInstance.close();
               }
               else {
                   UIControlService.unloadLoading();
               }
           },
           function (error) {
               UIControlService.unloadLoading();
               UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
           });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.close();
        }
    }
})();
