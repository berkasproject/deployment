﻿(function () {
    'use strict';

    angular.module("app").controller("dataUpload", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataKontrakService', 'UIControlService', 'UploadFileConfigService', 'ExcelReaderService', 'GlobalConstantService', 'UploaderService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $translate, $translatePartialLoader, $location, SocketService, DataKontrakService, UIControlService, UploadFileConfigService, ExcelReaderService, GlobalConstantService, UploaderService) {

        var vm = this;
        //
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.fileUpload;
        vm.maxSize = 10;
        vm.DocName = "";
        vm.currentPage = 1;
        vm.listItemPO = [];
        vm.totalItems = 0;
        vm.keyword = "";
        vm.ID = Number($stateParams.id);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm. dateNow = $filter('date')(1, 'dd/MM/yyyy');
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('detail-dataupload-po');
            $translatePartialLoader.addPart('dataupload-po');
            UIControlService.loadLoading("MESSAGE.LOADING");
            loadTypeSizeFile();
            loadData(1);
        }
        vm.loadData = loadData;
        function loadData(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            DataKontrakService.selectUploadPOKontrak({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.keyword
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listItemPO = data.List;
                    vm.totalItems = Number(data.Count);
                } else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        function loadTypeSizeFile() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.MASTER.ITEMPR", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoading();
                return;
            });
        }
        
        function numAttrs(obj) {
            var count = 0;
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    ++count;
                }
            }
            return count;
        }

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDateTime(date);
        }

        vm.toDetail = toDetail;
        function toDetail(id) {
            $state.transitionTo('detail-dataupload-po-contract', { id: id });
        }

        vm.toCompare = toCompare;
        function toCompare(id) {
            $state.transitionTo('compare-dataupload-po', { id: id });
        }

        //start upload
        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("error", "MSG_NOFILE");
                return;
            }

            if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
                
            }            
        }

        function upload(file, config, filters, dates, callback) {
            //console.info(file);
            var size = config.Size;

            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }
            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.pathFile = url;
                        uploadSave(fileName, vm.pathFile);
                    } else {
                        UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                    UIControlService.unloadLoading();
                });

        }

        function saveExcelContent(IdSAP) {
            vm.newExcel = [];
            ExcelReaderService.readExcel(vm.fileUpload,
                function (reply) {
                    if (reply.status === 200) {
                        var excelContents = reply.data;
                        var Sheet1 = excelContents[Object.keys(excelContents)[0]]; /*untuk baca nama sheet*/
                        console.info(Sheet1);
                        var countproperty = numAttrs(Sheet1[0]);
                        for (var a = 1; a < Sheet1.length; a++) {
                            if (Sheet1[a].Column2 !== null) {
                                var objExcel = {
                                    PurchDoc: Sheet1[a].Column1,
                                    Item: Sheet1[a].Column2,
                                    Type: Sheet1[a].Column3,
                                    Cat: Sheet1[a].Column4,
                                    Pgr: Sheet1[a].Column5,
                                    SAPCode: Sheet1[a].Column6,
                                    VendorName: Sheet1[a].Column7,
                                    ShortText: Sheet1[a].Column8,
                                    A: Sheet1[a].Column9,
                                    POrg: Sheet1[a].Column10,
                                    Agreement: Sheet1[a].Column11,
                                    ItemPO: Sheet1[a].Column12,
                                    DocDate: Sheet1[a].Column13,
                                    NetPrice: Sheet1[a].Column14,
                                    Currency: Sheet1[a].Column15,
                                    ToBeDel: Sheet1[a].Column16,
                                    ToBeInv: Sheet1[a].Column17,
                                    NetValue: Sheet1[a].Column18,
                                    IdSAPContract: IdSAP
                                };
                                if (!(objExcel.DocDate === null) && !(objExcel.DocDate === undefined)) {
                                    if (Number.isInteger(objExcel.DocDate)) {
                                        var date = new Date(1900, 0, 1);
                                        date.setDate(date.getDate() + objExcel.DocDate - 2);
                                        objExcel.DocDate = UIControlService.getStrDate(date);
                                    } else {
                                        objExcel.DocDate = UIControlService.convertDateFromExcel(objExcel.DocDate);
                                    }
                                }
                                else { objExcel.DocDate = null; }
                                console.info(objExcel);
                                vm.newExcel.push(objExcel);
                            }
                        }
                        uploadFileExcel(vm.newExcel);
                    }
                });
        }

        vm.tglSekarang = UIControlService.getDateNow("");
        function uploadSave(filename,url) {
            DataKontrakService.InsertFile({
                FileName: filename,
                DocUrl: url
            }, function (reply) {
                if (reply.status === 200) {
                    saveExcelContent(reply.data.ID);
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.toDetail1 = toDetail1;
        function toDetail1(id) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            DataKontrakService.Compare({ ID: id }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    if (reply.data != "") {
                        UIControlService.msg_growl("error", reply.data);
                    }
                    else {
                        UIControlService.msg_growl("success", "MSG_SUC_COMPARE");
                    }
                    vm.init();
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.uploadFileExcel = uploadFileExcel;
        function uploadFileExcel(data) {
            DataKontrakService.Insert(data, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MSG_SUC_SAVE");
                    UIControlService.unloadLoading();
                    vm.init();
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.deleteFile = deleteFile;
        function deleteFile(id) {
            UIControlService.loadLoading("Silahkan tunggu . . . ");
            DataKontrakService.DeleteFile({ ID: id }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("success", "MSG_SUCCESS_DELETE");
                    vm.init();
                }
                else {
                    UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }
    }
})();
