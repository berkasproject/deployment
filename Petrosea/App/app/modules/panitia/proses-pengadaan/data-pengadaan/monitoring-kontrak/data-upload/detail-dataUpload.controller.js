﻿(function () {
    'use strict';

    angular.module("app").controller("detaildataUpload", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataKontrakService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataKontrakService, UIControlService) {


        var vm = this;

        vm.currentPage = 1;
        vm.fileUpload;
        vm.keyword = "";
        vm.maxSize = 10;
        vm.currentPage = 1;
        vm.listItemPO = [];
        vm.totalItems = 0;
        vm.ID = Number($stateParams.id);
        vm.column = 1;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('detail-dataupload-po');
            loadData(1);
           
        }
        var loadmsg = "MESSAGE.LOADING";
        vm.loadData = loadData;
        function loadData(current) {
            vm.listItemPO = [];
            vm.currentPage = current;
            console.info(vm.currentPage);
            UIControlService.loadLoading(loadmsg);
            DataKontrakService.DetailPO({
                Offset: (vm.currentPage * vm.maxSize) - vm.maxSize,
                Limit: vm.maxSize,
                Parameter: vm.ID,
                Column: vm.column
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listItemPO = data.List;
                    for (var i = 0; i < vm.listItemPO.length; i++) {
                        if (vm.listItemPO[i].DocDate !== null) { vm.listItemPO[i].DocDate = UIControlService.getStrDate(vm.listItemPO[i].DocDate); }
                        
                    }
                    vm.totalItems = Number(data.Count);
                } else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


    }
})();
