﻿(function () {
	'use strict'

	angular.module("app").controller("contractMonitoringReminderCtrl", ctrl)

	ctrl.$inject = ['UIControlService', 'CtrReminderService', '$translatePartialLoader']
	function ctrl(UIControlService, CtrReminderService, $translatePartialLoader) {
		var vm = this
		vm.currentPage = 1
		vm.keyword = ""
		vm.pageSize = 10
		vm.selectedColumn = 1
		vm.dataRole = localStorage.getItem('roles').split(',')
		vm.lihatStatus = false
		vm.dataSelected = [];

		vm.init = init
		function init() {
		    $translatePartialLoader.addPart('contract-reminder')
		    for (var i = 0; i < vm.dataRole.length; i++) {
		        if (vm.dataRole[i] == 'APPLICATION.ROLE_CE' || vm.dataRole[i] == 'APPLICATION.ROLE_BUYER') {
		            vm.lihatStatus = true
		        }
		    }
			jLoad(1)
		}
		
		vm.initVHS = initVHS
		function initVHS() {
			$translatePartialLoader.addPart('contract-reminder')
			jLoadVHS(1)
		}

		vm.jLoad = jLoad
		function jLoad(currPage) {
			var offset = (currPage * 10) - 10
			UIControlService.loadLoading("MESSAGE.LOADING")
			CtrReminderService.getReminder({
				Keyword: vm.keyword,
				Offset: offset,
				Limit: vm.pageSize,
				FilterType: vm.selectedColumn
			}, function (response) {
				UIControlService.unloadLoading()
				if (response.status === 200) {
				    vm.reminderList = response.data.List;
				   
				    if (vm.dataSelected.length != 0) {
				        for (var i = 0; i < vm.reminderList.length; i++) {
				            var cekData = $.grep(vm.dataSelected, function (n) { return n.ID == vm.reminderList[i].ID; });
				            if (cekData.length != 0) {
				                vm.reminderList[i].ApprovalStatus = cekData[0].ApprovalStatus
				            }
				        }
				    }
					vm.totalItems = Number(response.data.Count)
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
				UIControlService.unloadLoading()
			});
		}

		vm.jLoadVHS = jLoadVHS
		function jLoadVHS(currPage) {
			var offset = (currPage * 10) - 10
			UIControlService.loadLoading("MESSAGE.LOADING")
			CtrReminderService.getReminderVHS({
				Keyword: vm.keyword,
				Offset: offset,
				Limit: vm.pageSize,
				FilterType: vm.selectedColumn
			}, function (response) {
				UIControlService.unloadLoading()
				if (response.status === 200) {
					vm.reminderList = response.data.List;
					vm.totalItems = Number(response.data.Count)
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
				UIControlService.unloadLoading()
			});
		}

		vm.selectPush = selectPush
		function selectPush(data) {
		    console.log(data);
		    if (vm.dataSelected.length == 0) {
		        vm.dataSelected.push({ ID: data.ID, ApprovalStatus: data.ApprovalStatus })
		    } else {
		        var cekData = $.grep(vm.dataSelected, function (n) { return n.ID == data.ID; });
		        if (cekData == 0) {
		            vm.dataSelected.push({ ID: data.ID, ApprovalStatus: data.ApprovalStatus })
		        } else {
		            var indexData = vm.dataSelected.map(function (e) { return e.ID; }).indexOf(data.ID);
		            vm.dataSelected[indexData].ApprovalStatus = data.ApprovalStatus;
		        }

		    }

		    console.log(vm.dataSelected);

		}

		vm.save = save
		function save() {
		    if (vm.dataSelected == 0) {
		        UIControlService.msg_growl("warning", "Tidak Ada Yang Di Simpan")
		        return;
		    }
		    UIControlService.loadLoading("MESSAGE.LOADING")
		    CtrReminderService.saveContract({
		        SelectedData: vm.dataSelected,
		        username: localStorage.getItem('username')
		    }, function (response) {
		        UIControlService.unloadLoading()
		        if (response.status === 200) {
		            UIControlService.msg_growl("success", "MESSAGE.SUCCESS")
		            UIControlService.unloadLoading()
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
		            UIControlService.unloadLoading()
		        }
		    }, function (response) {
		        UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
		        UIControlService.unloadLoading()
		    })


		}
	}
})()