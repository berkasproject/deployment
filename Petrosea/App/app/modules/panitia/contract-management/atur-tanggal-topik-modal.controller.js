﻿(function () {
    'use strict';

    angular.module("app").controller("aturTanggalTopikModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce', '$stateParams', '$q'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService, $sce, $stateParams, $q) {

        var vm = this;
        vm.startDate = new Date();
        vm.endDate = new Date(); 
        vm.isCalendarOpened = [false, false];

        vm.dateNow = new Date();
        vm.timezone = vm.dateNow.getTimezoneOffset();
        vm.timezoneClient = vm.timezone / 60;


        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        vm.init = init;
        function init() {
            console.log(item)
            loadHistoryCMID().then(function () {
                jLoad()
            })
        }

        

        function loadHistoryCMID() {
            var defer = $q.defer();
            dataContractManagementService.getHistoryContractManagement({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.history = data;
                    defer.resolve(true)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                    defer.reject(false);
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                defer.reject(false);
            });
            return defer.promise;
        }

        function jLoad() {
            UIControlService.loadLoadingModal('LOADING');

            dataContractManagementService.getTanggalTopik({
                ContractManagementID: $stateParams.id
            }, function (reply) {

                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    if (reply.data.StartDateDuration != null && reply.data.EndDateDuration != null) {
                        vm.startDate = new Date(reply.data.StartDateDuration);
                        vm.endDate = new Date(reply.data.EndDateDuration);

                    }
                    console.log(reply.data)

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

                    UIControlService.unloadLoadingModal();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoadingModal();

            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

        vm.simpan = simpan;
        function simpan() {
           
            if (vm.startDate == "") {
                UIControlService.msg_growl("error", "MESSAGE.START_DATE_EMPTY");
                return;
            }

            if (vm.endDate  == "") {
                UIControlService.msg_growl("error", "MESSAGE.END_DATE_EMPTY");
                return;
            }

            if (new Date(vm.startDate) < new Date()) {
                UIControlService.msg_growl("error", "MESSAGE.START_DATE_MORE");
                return;
            }

            if (vm.endDate < vm.startDate) {
                UIControlService.msg_growl("error", "MESSAGE.END_DATE_LESS");
                return;
            }

            

            var startDate = new Date(vm.startDate);
            var endDate = new Date(vm.endDate);
            startDate.setHours(startDate.getHours() - vm.timezoneClient);
            endDate.setHours(endDate.getHours() - vm.timezoneClient);

            UIControlService.loadLoadingModal('LOADING');
            
            dataContractManagementService.updateTanggalTopik({
                ContractManagementID: $stateParams.id,
                StartDateDuration: startDate,
                EndDateDuration: endDate
            }, function (reply) {
                if (reply.status == 200) {
                    $uibModalInstance.close();

                    UIControlService.unloadLoadingModal();
                   
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

                    UIControlService.unloadLoadingModal();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoadingModal();

            })
        }

    }
})();