﻿(function () {
    'use strict';

    angular.module("app").controller("remarkCloseProjectCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce', '$stateParams', '$q'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService, UIControlService, item, $uibModalInstance, GlobalConstantService, $sce, $stateParams, $q) {

        var vm = this;
        vm.isRemark = item.remark;
        vm.tenderName = item.TenderName;
        vm.remark = "";
        vm.category = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("approval-signature");
            if (vm.isRemark == 1) {
                vm.remark = item.dataRemark;
            }
        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal('LOADING');

            if (vm.isRemark == 1) {
                dataContractManagementService.insertRemarkCompleted({
                    HistoryCMID: item.HistoryCMID,
                    remark: vm.remark
                }, function (reply) {
                    if (reply.status == 200) {
                        UIControlService.msg_growl("success", "MESSAGE.BERHASIL_SIMPAN");
                        $uibModalInstance.close();
                        UIControlService.unloadLoadingModal();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    UIControlService.unloadLoadingModal();
                })
            } else if (vm.isRemark == 0) {
                if (vm.remark == "") {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.KET");
                    return;
                }
                dataContractManagementService.closeProject({
                    ContractManagementID: item.CMID,
                    remark: vm.remark
                }, function (reply) {
                    if (reply.status == 200) {
                        UIControlService.msg_growl("success", "MESSAGE.BERHASIL_SIMPAN");
                        $uibModalInstance.close();
                        UIControlService.unloadLoadingModal();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    UIControlService.unloadLoadingModal();
                })
            } else if (vm.isRemark == 2) {
                //if (vm.category == "") {
                //    UIControlService.unloadLoadingModal();
                //    UIControlService.msg_growl("error", "MESSAGE.KATEGORI");
                //    return;
                //}
                dataContractManagementService.extTender({
                    ContractManagementID: item.CMID,
                    Category: Number(vm.category)
                }, function (reply) {
                    if (reply.status == 200) {
                        UIControlService.msg_growl("success", "MESSAGE.BERHASIL_SIMPAN");
                        $uibModalInstance.close();
                        UIControlService.unloadLoadingModal();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    UIControlService.unloadLoadingModal();
                })
            }
        }

    }
})();