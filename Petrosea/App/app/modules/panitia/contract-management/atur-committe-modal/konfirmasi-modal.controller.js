﻿(function () {
    'use strict';

    angular.module("app").controller("konfirmasiModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService
        , UIControlService, item, $uibModalInstance, GlobalConstantService, $sce) {

        var vm = this;
        vm.item = item;
        

        vm.init = init;
        function init() {
            console.log(item)

        }

        vm.konfirmasi = konfirmasi;
        function konfirmasi() {
            $uibModalInstance.close();

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();