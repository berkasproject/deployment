﻿(function () {
    'use strict';

    angular.module("app").controller("aturCommitteCariPegawaiModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce','$filter'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService
        , UIControlService, item, $uibModalInstance, GlobalConstantService, $sce,$filter) {

        var vm = this;
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.pageSize = 10;
        vm.data = [];

        vm.init = init;
        function init() {
            jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            UIControlService.loadLoadingModal('LOADING');
            dataContractManagementService.selectEmployee({
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword: vm.keyword,
            }, function (reply) {
                vm.data = reply.data.List;
                vm.totalItems = reply.data.Count;
                UIControlService.unloadLoadingModal();

            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.pilihEmployee = pilihEmployee;
        function pilihEmployee(data) {
           $uibModalInstance.close(data);    
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();