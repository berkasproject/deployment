﻿(function () {
    'use strict';

    angular.module("app").controller("aturComitteController", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'dataContractManagementService', '$filter', '$rootScope', '$state','$stateParams','$q'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, dataContractManagementService, $filter, $rootScope, $state,$stateParams,$q) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.data = [];
        vm.dataEmployee = [];
        var username = localStorage.getItem('username');

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("atur-commite");
            //console.log('Permulaan')
            loadHistoryCMID().then(function(reply){
                jLoad()
            })
        }

        function loadHistoryCMID() {
            var defer = $q.defer();
            dataContractManagementService.getHistoryContractManagement({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.history = data;
                    defer.resolve(true)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                    defer.reject(false);
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                defer.reject(false);
            });
            return defer.promise;
        }

        function jLoad() {
            UIControlService.loadLoading('LOADING');

            dataContractManagementService.getContractCommittee({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoading();
                    vm.data = reply.data.List;
                    vm.totalItems = reply.data.Count;

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

                    UIControlService.unloadLoading();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();

            })
        }

        vm.contractManagement = contractManagement;
        function contractManagement(id) {
            $state.transitionTo('contract-management', { id: id })
        }

        vm.cariPegawai = cariPegawai;
        function cariPegawai() {
            var data = {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/atur-committe-modal/atur-committe-cari-pegawai.html',
                controller: 'aturCommitteCariPegawaiModalController',
                controllerAs: 'aturCommitteCariPegawaiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                //console.log(reply)
                vm.dataEmployee = reply;
            });
        }

        vm.hapus = hapus;
        function hapus(data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/atur-committe-modal/konfirmasi-modal.html',
                controller: 'konfirmasiModalController',
                controllerAs: 'konfirmasiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                //console.log(reply)
                var index = vm.data.findIndex(function (res) {
                    return res.EmployeeID === data.EmployeeID;
                });
                //console.log(index)
                vm.data.splice(index, 1);
                //console.log(vm.data)
            });
            
            
        }

        vm.tambahEmployee = tambahEmployee;
        function tambahEmployee() {
            if (vm.dataEmployee.length != 0) {
                var cekData = $.grep(vm.data, function (n) { return n.EmployeeID == vm.dataEmployee.EmployeeID; });
                //console.log(username)
                if (vm.dataEmployee.Username == username) {
                    UIControlService.msg_growl("error", "MESSAGE.EMPLOYEE_SELF");
                    return;
                }
                if (cekData.length == 0) {
                    vm.data.push(vm.dataEmployee);
                    vm.dataEmployee = [];
                    //console.log(vm.data)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.EMPLOYEE_EXIST");
                }
            } else {
                UIControlService.msg_growl("error", "MESSAGE.PILIH_EMPLOYEE");

            }
        }

        vm.back = back;
        function back() {
            $state.transitionTo('contract-management',{id:$stateParams.id})
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoading('LOADING');

            dataContractManagementService.insertContractManagementCommitte({
                ContractManagementID: $stateParams.id,
                DataInsert: vm.data
            }, function (response) {
                if (response.status == 200) {
                    UIControlService.msg_growl("notice", "MESSAGE.SUCCESS_INSERT_CONTRACT_MANAGAMENT_COMMITTE");
                    UIControlService.unloadLoading();

                    back();
                }else{
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    UIControlService.unloadLoading();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();

            })
        }

    }
})();