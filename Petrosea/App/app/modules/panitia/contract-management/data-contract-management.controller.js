﻿(function () {
    'use strict';

    angular.module("app").controller("dataContractManagementController", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'dataContractManagementService', '$filter', '$rootScope', '$state', '$q', '$window'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, dataContractManagementService, $filter, $rootScope, $state, $q, $window) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        
        vm.FormatDateTime = UIControlService;
        vm.dateNow = Date.now();

        vm.startDate = new Date();
        vm.endDate = new Date();
        vm.isCalendarOpened = [false, false];

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("data-contract-management");
            getTypeTender().then(function(){
                jLoad(1);
            }) 
        }

        vm.dataStatus = [];
        function getTypeTender() {
            var defer = $q.defer();
            dataContractManagementService.getTypeTender(function (reply) {
                if (reply.status == 200) {
                    console.log(reply.data)
                    vm.dataStatus = reply.data.List;
                    defer.resolve(true);
                }
            }, function (err) {
                defer.reject();
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();
            })
            return defer.promise;
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading('LOADING');
            vm.currentPage = current;
            dataContractManagementService.getContractManagementFull({
                Keyword: vm.keyword,
                FilterType: vm.action,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                DateNull1: UIControlService.getStrDate(vm.startDate),
                DateNull2: UIControlService.getStrDate(vm.endDate)
            }, function (reply) {
                UIControlService.unloadLoading();

                vm.data = reply.data.List;
                vm.totalItems = reply.data.Count;

                if (vm.data.length > 0) {
                    vm.data.forEach(function (item) {
                        if ((new Date(item.endDateTender) < vm.dateNow) && item.ProcPackageType != 4190) {
                            if (item.StatusManagement != 4508 && item.StatusManagement != 4501 && item.StatusManagement != 5411) {
                                console.info(item.StatusManagement);
                                expiredTender(item.ContractManagementID);
                            }
                        }
                    });
                }

            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoading();

                //$rootScope.unloadLoading();
            });
        }

        function expiredTender(id) {
            dataContractManagementService.expiredTender({
                ContractManagementID: id
            }, function (reply) {
                vm.init();
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.contractManagement = contractManagement;
        function contractManagement(id) {
            $state.transitionTo('contract-management',{id:id})
        }

        vm.act = act;
        function act() {
            vm.keyword = "";
        }

        vm.publish = publish;
        function publish(data) {
            if (!data.templateDone) {
                UIControlService.msg_growl("error", "MESSAGE.DATATEMPLATE");
            } else if (!data.forumDone) {
                UIControlService.msg_growl("error", "MESSAGE.DATAFORUM");
            } else if (!data.CommitteeDone) {
                UIControlService.msg_growl("error", "MESSAGE.DATACOMMITTEE");
            } else if (data.StartDateDuration == null) {
                UIControlService.msg_growl("error", "MESSAGE.DURASI");
            }
            else if (data.templateDone && data.forumDone && data.CommitteeDone && data.StartDateDuration != null) {
                bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.PUBLISH_TENDER_INI') + ' ' + data.TenderName + '?<h3>', function (reply) {
                    if (reply) {
                        //UIControlService.loadLoading(loadmsg);
                        sendMail(data).then(function () {
                            insertHistory(data);
                        });
                    }
                });
            }
        }

        vm.distributionSimpan = distributionSimpan;
        function distributionSimpan(data) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.GANTI_STATUS_SIGNATURE') + ' ' + data.TenderName + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);

                    insertHistory(data);
                }
            });
        }

        function sendMail(param) {
            var defer = $q.defer();
            UIControlService.loadLoading();

            dataContractManagementService.sendMail({
                ContractManagementID: param.ContractManagementID,
                dataContractManagement: param
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.BERHASIL_UBAH_STATUS");
                    defer.resolve(true);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    defer.reject();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();
                defer.reject();
            });
            return defer.promise;
        }

        function insertHistory(data) {
            var stats = parseInt(data.StatusManagement) + 1;
            dataContractManagementService.insertHistory({
                ContractManagementID: data.ContractManagementID,
                Status: stats,
                Remark: null,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.BERHASIL_UBAH_STATUS");
                    jLoad(1);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();
            });
        }

        vm.detailApproval = detailApproval;
        function detailApproval(data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/detail-approval-modal.html',
                controller: 'detailApprovalModalController',
                controllerAs: 'detailApprovalModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
            });
        }

        vm.signature = signature;
        function signature(id) {
            var param = {
                CMID: id,
                ubah: 1
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/signature-modal.html',
                controller: 'signatureModalCtrl',
                controllerAs: 'signatureModalCtrl',
                resolve: {
                    item: function () {
                        return param;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                vm.init();
            });
        }

        vm.viewSignature = viewSignature;
        function viewSignature(id) {
            var param = {
                CMID: id,
                ubah: 0
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/signature-modal.html',
                controller: 'signatureModalCtrl',
                controllerAs: 'signatureModalCtrl',
                resolve: {
                    item: function () {
                        return param;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                vm.init();
            });
        }

        vm.extTender = extTender;
        function extTender(CMID) {
            var data = {
                CMID: CMID,
                remark: 2
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/remark-closeProject.html',
                controller: 'remarkCloseProjectCtrl',
                controllerAs: 'remarkCloseProjectCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                vm.init();
            });
        }

        vm.printEsaf = printEsaf;
        vm.dataESAF = [];
        function printEsaf(toPrint, data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/e-saf-modal.html',
                controller: 'eSafCtrl',
                controllerAs: 'eSafCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                vm.init();
            });
        }
    }
 })();