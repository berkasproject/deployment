﻿(function () {
    'use strict';

    angular.module("app").controller("signatureModalCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'dataContractManagementService', '$filter', '$rootScope', '$state', 'item', '$uibModalInstance'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, dataContractManagementService, $filter, $rootScope, $state, item, $uibModalInstance) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.ubah = item.ubah;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 5;
        vm.FormatDateTime = UIControlService;
        var dataInsertSignature = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("data-contract-management");
            UIControlService.loadLoadingModal('LOADING');
            jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            dataContractManagementService.getAllSignature({
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                IntParam1: item.CMID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.dataSignature = reply.data.List;
                vm.totalItems = reply.data.Count;
                if (vm.dataSignature.length > 0) {
                    vm.dataSignature.forEach(function (item) {

                        if (dataInsertSignature.length > 0) {
                            for (var a = 0; a < dataInsertSignature.length; a++) {
                                if (item.ContractSignatureID == dataInsertSignature[a].ContractSignatureID) {
                                    item.Status = dataInsertSignature[a].Status;
                                }
                            }
                        }

                        if (item.Status == "1") {
                            item.Status = true;
                        } else {
                            item.Status = false;
                        }
                    });
                }
                console.info(vm.dataSignature);
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.check = check;
        function check(data) {
            var stats = data.Status == true ? "1" : null;
            
            var param = {
                ContractSignatureID: data.ContractSignatureID,
                ContractManagementID: item.CMID,
                Status: stats
            };
            dataInsertSignature.push(param);
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal('LOADING');
            dataContractManagementService.insertSignature({
                ContractSignature: dataInsertSignature
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                $uibModalInstance.close();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();