﻿(function () {
    'use strict';
    angular.module("app").
    controller("summaryModalController", ctrl)

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce', '$stateParams'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService, $sce, $stateParams) {

        var vm = this;
        vm.summary = '';
        
        vm.init = init;
        function init() {
            vm.summary = item.Summary;
            vm.Status = item.Status;
        }

        vm.save = save;
        function save() {
            UIControlService.loadLoadingModal();

            dataContractManagementService.updateSummaryContractManagement({
                Summary: vm.summary,
                ContractManagementID: $stateParams.id
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();

                }
            }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_INSERT");

            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }
    }
})();