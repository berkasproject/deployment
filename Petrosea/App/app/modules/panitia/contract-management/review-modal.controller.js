﻿(function () {
    'use strict';

    angular.module("app").
    controller("reviewModalController", ctrl)

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce', '$stateParams', 'SocketService'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService, $sce,$stateParams,SocketService) {

        var vm = this;
        vm.checkVendor = false;
        vm.checkCommittee = true;
        vm.review = item.buyerName+'<br>Tanggapan:';
        vm.penerimaChat = item.penerimaChat;

        vm.init = init;
        function init() {
            //SocketService.emit("realtimeReview", { ContractManagementID: $stateParams.id });

            console.log('ada init')
        }

        vm.simpan = simpan;
        function simpan() {

            if (!vm.checkCommittee && !vm.checkVendor) {
                UIControlService.msg_growl("error", "MESSAGE.CHECK_NULL");
                return;
            }
            UIControlService.loadLoadingModal();
            
            dataContractManagementService.insertDetailForumContract({
                Review: vm.review,
                ForumID: item.forumID,
                checkCommittee: vm.checkCommittee,
                checkVendor: vm.checkVendor
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                $uibModalInstance.close();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.generate = generate;
        function generate(param) {
            UIControlService.loadLoadingModal();
            dataContractManagementService.generateDetailForum({
                ForumID: item.forumID,
                ContractManagementID: $stateParams.id,
                Status: param
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.review = '';
                    if (param == 'committee') {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].ReviewFor != null) {
                                vm.review += data[i].Review + '<br><br>';

                            } else {
                                vm.review += '<b>' + data[i].CommitteeName + ' - ' + UIControlService.convertDateTime(data[i].CreatedDate) + '</b> <br>' + data[i].Review + '<br><br>';
                            }
                        }
                    }
                    if (param == 'vendor') {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].ReviewFor != null) {
                                vm.review += data[i].Review + '<br><br>';

                            } else {
                                vm.review += '<b>'+ data[i].VendorName + ' - ' + UIControlService.convertDateTime(data[i].CreatedDate) + '</b> <br>' + data[i].Review + '<br><br>';
                            }
                        }
                    }
                    vm.review += item.buyerName + '<br>Tanggapan:';
                    console.log(vm.review)
                }

            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();