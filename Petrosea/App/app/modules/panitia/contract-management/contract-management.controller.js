﻿(function () {
    'use strict';

    angular.module("app").controller("contractManagementController", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'dataContractManagementService', '$filter', '$rootScope', '$state', '$stateParams', 'UploadFileConfigService', 'UploaderService', '$q', 'SocketService'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, dataContractManagementService, $filter, $rootScope, $state, $stateParams, UploadFileConfigService, UploaderService, $q, SocketService) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.dataTemplate = null;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.fileUpload;
        vm.nama_dokumen = '';
        vm.contentForum = '';
        vm.titleForum = '';
        vm.dataDetailForumCM = [];
        vm.FormatDateTime = UIControlService;
        vm.selectedTemplate = '';
        vm.dataContractSignature = [];
        var username = localStorage.getItem("username");
        console.log(username)
        vm.disableUpload = false;
        vm.dateNow = Date.now();
        vm.init = init;

        function init() {
            console.log('Permulaan')
            $translatePartialLoader.addPart("data-contract-management");
            $translatePartialLoader.addPart("atur-commite");
            $translatePartialLoader.addPart("dashboard-vendor");
            UIControlService.loadLoading();
            
            

            fileUploadConfig().then(function (resp) {
                loadHistoryCMID().then(function (response) {
                    loadContractSignature(1).then(function(response){
                        loadDetailLampiran(1);
                    }) 
                });
            });   
        }

        vm.loadDetailLampiran = loadDetailLampiran;
        function loadDetailLampiran(current) {
            vm.currentPage = current;

            dataContractManagementService.getDetailTemplate({
                IntParam1: $stateParams.id,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    var data = reply.data;
                    vm.data = data.List;

                    for(var i = 0; i < vm.data.length; i++){
                        if (vm.data[i].status == 4506) {
                            vm.fileUpload = "";
                            vm.disableUpload = true;
                        }
                    }
                    vm.totalItems = data.Count;
                    if (vm.data.length != 0) {
                        
                        var dataPilih = $.grep(vm.data, function (n) { return n.IsActive; });

                        if (dataPilih.length == 0) {
                            var index = vm.data.length - 1;

                            vm.dataTemplate = {
                                ContractDocumentURL: vm.data[index].DocumentURL,
                                DocumentName: vm.data[index].DocumentName,
                                ID: vm.data[index].ContractDocumentID
                            }
                        } else {
                            vm.dataTemplate = {
                                ContractDocumentURL: dataPilih[0].DocumentURL,
                                DocumentName: dataPilih[0].DocumentName,
                                ID: dataPilih[0].ContractDocumentID
                            }
                        }
                        console.log(dataPilih)

                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
            });
        }

        function loadHistoryCMID() {
            var defer = $q.defer();
            dataContractManagementService.getHistoryContractManagement({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.history = data;
                    if (vm.history.StartDateDuration && vm.history.EndDateDuration) {
                        vm.jumlahHari = getDays(vm.history.EndDateDuration, vm.history.StartDateDuration);
                    }
                    if (new Date(vm.history.EndDateDuration) < vm.dateNow && vm.history.StatusValue == "Review") {
                        insertHistoryJustReload(vm.history);
                    }
                    if (vm.history.Status == 4502 || vm.history.Status == 4503 || vm.history.Status == 4504) {
                        loadPenerimaChat().then(function (reply) {
                            loadForumContractManagement().then(function (reply) {
                                    defer.resolve(true)
                            })
                        })
                    } else {
                        defer.resolve(true)

                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                    defer.reject(false);
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                defer.reject(false);
            });
            return defer.promise;
        }

        function loadPenerimaChat() {
            var defer = $q.defer();
            dataContractManagementService.getPenerimaChat({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    console.log(data);
                    vm.penerimaChat = data;
                    //setTimeout(function () {
                    //    SocketService.emit("realtimeReview", { ContractManagementID: $stateParams.id, penerimaChat: vm.penerimaChat.List });
                    //}, 2000);
                    loadDetailForumCM($stateParams.id)
                    
                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'ERROR.GAGAL_API')
                defer.reject(false);
            })
            return defer.promise;
        }

        function getDays(endDt, startDt) {
            var end = new Date(endDt);
            var start = new Date(startDt);

            var ONE_DAY = 1000 * 60 * 60 * 24;
            var differenceMs = Math.abs(end - start);
            return Math.round(differenceMs / ONE_DAY);

        }

        function loadForumContractManagement() {
            var defer = $q.defer();
            dataContractManagementService.getForumContractManagement({
                ContractManagementID: $stateParams.id
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    console.log(data);
                    vm.contentForum = data.ContentForum;
                    vm.titleForum = data.TitleForum;
                    vm.forumID = data.ForumID;
                    vm.buyerName = data.BuyerName;
                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'ERROR.GAGAL_API')
                defer.reject(false);
            })
            return defer.promise;
        }

        SocketService.on('realtimeReview', function (reply) {
            var penerimaChat = reply.penerimaChat;
            var dataPilih = $.grep(penerimaChat, function (n) { return n.CommitteeOrVendorName == username; });
            console.log(dataPilih)

            if (dataPilih.length != 0) {
                console.log(reply)
                loadDetailForumCM(reply.ContractManagementID)

            }
        });

        function loadDetailForumCM(id) {
            var defer = $q.defer();
            dataContractManagementService.getDetailForumCM({
                IntParam1: id
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.dataDetailForumCM = data;
                    UIControlService.unloadLoading();

                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'ERROR.GAGAL_API')
                defer.reject(false);
            })
            return defer.promise;
        }

        function loadContractSignature(current) {
            var defer = $q.defer();
            vm.currentPage = current;
            dataContractManagementService.getContractSignature({
                IntParam1: $stateParams.id,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.dataContractSignature = data;
                    console.log(data)
                    defer.resolve(true)
                } else {
                    defer.reject
                }
            }, function (err) {
                defer.reject(true)
            })

            return defer.promise;

        }


        function fileUploadConfig() {
            var defer = $q.defer();
            UploadFileConfigService.getByPageName("PAGE.ADMIN.DETAILTEMPLATE", function (response) {
                if (response.status == 200) {
                    //console.info(response);
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                    defer.resolve(true)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    defer.reject(false)
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                defer.reject(false)
            });
            return defer.promise;
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.pilihTemplate = pilihTemplate;
        function pilihTemplate() {
            var data = {Status: vm.history.Status};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/pilih-template-modal.html',
                controller: 'pilihTemplateModalController',
                controllerAs: 'pilihTemplateModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                vm.dataTemplate = reply;
            });
        }

        vm.summary = summary;
        function summary() {
            var data = {Summary: vm.history.Summary, Status: vm.history.Status};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/summary-modal.html',
                controller: 'summaryModalController',
                controllerAs: 'summaryModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                loadHistoryCMID();
            });
        }

        vm.lampiran = lampiran;
        function lampiran() {
            var data = {Status: vm.history.Status};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/lampiran-modal.html',
                controller: 'lampiranModalController',
                controllerAs: 'lampiranModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
            });
        }

        vm.postingTopik = postingTopik;
        function postingTopik() {
            var data = {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/posting-topik-modal.html',
                controller: 'postingTopikModalController',
                controllerAs: 'postingTopikModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
            });
        }

        vm.aturTanggalTopik = aturTanggalTopik;
        function aturTanggalTopik() {
            var data = {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/atur-tanggal-topik-modal.html',
                controller: 'aturTanggalTopikModalController',
                controllerAs: 'aturTanggalTopikModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                loadHistoryCMID();
            });
        }

        vm.detailTemplate = detailTemplate;
        function detailTemplate(data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/detail-template-modal.html',
                controller: 'detailTemplateModalController',
                controllerAs: 'detailTemplateModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
            });
        }

        vm.review = review;
        function review() {
            var data = { forumID: vm.forumID, buyerName: vm.buyerName,penerimaChat: vm.penerimaChat };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/review-modal.html',
                controller: 'reviewModalController',
                controllerAs: 'reviewModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                //SocketService.emit("realtimeReview", { ContractManagementID: $stateParams.id, penerimaChat: vm.penerimaChat.List });
                loadDetailForumCM($stateParams.id)
                SocketService.emit("realtimeReviewVendor", { ContractManagementID: $stateParams.id, penerimaChat: vm.penerimaChat.List });
                SocketService.emit("realtimeReviewCommittee", { ContractManagementID: $stateParams.id, penerimaChat: vm.penerimaChat.List });
            });
        }

        vm.aturComitte = aturComitte;
        function aturComitte(data) {

            $state.transitionTo('atur-comitte', { id: $stateParams.id })
        }

        vm.aturSignature = aturSignature;
        function aturSignature() {
            $state.transitionTo('atur-signature', { id: $stateParams.id, page: 2 })
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {

            if (vm.nama_dokumen == "") {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_NAMA_DOKUMEN");
                return;
            }
            if (vm.dataTemplate == null) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_PILIH_TEMPLATE");
                return;
            }

            UIControlService.loadLoading("MESSAGE.LOADING");
            if (!(vm.fileUpload == null || vm.fileUpload == '')) {
                if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {

                    upload(1, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
                } else {

                    UIControlService.unloadLoading();
                }
            } else {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
                return;

            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_FILE");
                return false;
                UIControlService.unloadLoadingModal();
            }
            else return true;
        }

        vm.upload = upload;
        function upload(id, file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleDetailTemplate(id, file, size, filters, function (response) {
                //console.info("response:" + JSON.stringify(response));
                if (response.status == 200) {
                    console.info(response);
                    var url = response.data.Url;
                    vm.pathFile = url;
                    vm.name = response.data.FileName;
                    var s = response.data.FileLength;
                    vm.DocUrl = vm.pathFile;
                    //console.info(vm.DocUrl);
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s);
                        //  console.info(vm.size);
                    }

                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }

                    vm.simpan(vm.DocUrl);
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    return;
                }
            }, function (response) {
                console.info(response);
                UIControlService.msg_growl("error", "MESSAGE.API")
                UIControlService.unloadLoading();
            });
        }

        vm.simpan = simpan;
        function simpan(url) {
            dataContractManagementService.insertDetailTemplate({
                ContractManagementID: $stateParams.id,
                DocumentName: vm.nama_dokumen,
                DocumentURL: url,
                ContractDocumentID: vm.dataTemplate.ID,
                status: vm.history.Status
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.BERHASIL_SIMPAN");
                    init();
                    vm.nama_dokumen = '';
                    vm.fileUpload;
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_SIMPAN");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.akhiriForum = akhiriForum;
        function akhiriForum() {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.AKHIRI_REVIEW_INI') + ' ' + vm.history.TenderName + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);

                    insertHistory(vm.history);
                }
            });
        }

        

        function insertHistoryJustReload(data) {
            var stats = parseInt(data.Status) + 1;
            UIControlService.loadLoading();
            dataContractManagementService.insertHistory({
                ContractManagementID: $stateParams.id,
                Status: stats,
                Remark: null,
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.REVIEW_SUDAH_SELESAI");
                    window.location.reload();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();
            });
        }

        function insertHistory(data) {
            var stats = parseInt(data.Status) + 1;
            UIControlService.loadLoading();
            dataContractManagementService.insertHistory({
                ContractManagementID: $stateParams.id,
                Status: stats,
                Remark: null,
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.BERHASIL_UBAH_STATUS");
                    $state.transitionTo('data-contract-management');
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();
            });
        }

        vm.saveDetailTemplate = saveDetailTemplate;
        function saveDetailTemplate() {
            
            var boleh = false;

            for (var i = 0; i < vm.data.length; i++) {
                if (vm.data[i].statusValue == "Approved") {
                    boleh = true;
                }
            }

            if (!boleh) {
                UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DULU");
                return;
            }

            if (vm.history.Summary == null) {
                UIControlService.msg_growl("error", "MESSAGE.SUMMARY_NULL");
                return;
            }

            if (vm.selectedTemplate == '') {
                UIControlService.msg_growl("error", "MESSAGE.SELECTED_RADIO");
                return;
            }

            if (vm.dataContractSignature.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.SIGNATURE_NULL");
                return;
            }

            

            console.log(vm.selectedTemplate)
            var jsonSelectedTemplate = JSON.parse(vm.selectedTemplate);

            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.APAKAH_ANDA_YAKIN') + ' ' + jsonSelectedTemplate.DocumentName + ' ' + $filter('translate')('MESSAGE.SEBAGAI_DOKUMEN') + ' ' +vm.history.TenderName + ' ?<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);

                    
                    dataContractManagementService.updateDetailTemplateIsActive({
                        DetTemplateID: jsonSelectedTemplate.DetTemplateID
                    }, function (reply) {
                        if (reply.status == 200) {
                            insertHistory(vm.history);
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                        return;
                    })
                }
            });
            
        }

        vm.remark = remark;
        function remark(historyID) {
            var data = {
                HistoryCMID: historyID,
                TenderName: vm.history.TenderName,
                remark: 1,
                dataRemark: vm.history.Remark
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/remark-closeProject.html',
                controller: 'remarkCloseProjectCtrl',
                controllerAs: 'remarkCloseProjectCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                loadHistoryCMID();
            });
        }

        vm.closeProject = closeProject;
        function closeProject() {
            var data = {
                CMID: Number($stateParams.id),
                TenderName: vm.history.TenderName,
                remark: 0
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/remark-closeProject.html',
                controller: 'remarkCloseProjectCtrl',
                controllerAs: 'remarkCloseProjectCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                loadHistoryCMID();
            });
        }

        vm.back = back;
        function back(){
            $state.transitionTo('data-contract-management');
        }

    }
})();