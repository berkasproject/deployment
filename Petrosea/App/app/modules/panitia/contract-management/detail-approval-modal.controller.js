﻿(function () {
    'use strict';

    angular.module("app").controller("detailApprovalModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce','$q'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService
        , UIControlService, item, $uibModalInstance, GlobalConstantService, $sce,$q) {

        var vm = this;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.FormatDateTime = UIControlService;
        vm.data = [];
        vm.status = item.StatusManagement;

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal();
            jLoad(1);
        }

        function jLoad(current) {
            vm.currentPage = current;
            dataContractManagementService.selectApprover({
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                IntParam1: item.ContractManagementID
            }, function (reply) {
                UIControlService.unloadLoadingModal();

                vm.data = reply.data.List;
                vm.totalItems = reply.data.Count;
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.aturSignature = aturSignature;
        function aturSignature() {
            $uibModalInstance.dismiss('dismiss');
            $state.transitionTo('atur-signature', { id: item.ContractManagementID, page :1 })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();