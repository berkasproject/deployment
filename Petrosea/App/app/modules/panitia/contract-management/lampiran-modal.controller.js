﻿(function () {
    'use strict';

    angular.module("app").controller("lampiranModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$filter', '$q', 'UploadFileConfigService', 'UploaderService', '$stateParams'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService, $filter, $q, UploadFileConfigService, UploaderService,$stateParams) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.nama_dokumen = '';
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.docs = [];
        vm.fileUpload;
        vm.Status = item.Status;

        vm.init = init;
        function init() {
            //console.log(item)
            UIControlService.loadLoadingModal('LOADING');

            fileUploadConfig().then(function () {
                jLoad(1)
            })
        }

        function fileUploadConfig() {
            var defer = $q.defer();
            UploadFileConfigService.getByPageName("PAGE.ADMIN.DETAILLAMPIRAN", function (response) {
                if (response.status == 200) {
                    //console.info(response);
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                    defer.resolve(true);
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    defer.reject(false);
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                defer.reject(false);

            });

            return defer.promise;
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        function jLoad(current) {
            
            vm.currentPage = current;
            dataContractManagementService.selectLampiran({
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                IntParam1: $stateParams.id
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                
                vm.docs = reply.data.List;
                vm.totalItems = reply.data.Count;


            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {

            if (vm.nama_dokumen == "") {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_NAMA_DOKUMEN");
                return;
            }

            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            if (!(vm.fileUpload == null || vm.fileUpload == '')) {
                if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                    
                    upload(1, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
                } else {

                    UIControlService.unloadLoadingModal();
                }
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
                return;
                
            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_FILE");
                return false;
                UIControlService.unloadLoadingModal();
            }
            else return true;
        }

        vm.upload = upload;
        function upload(id, file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoadingModal("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleDetailLampiran(id, file, size, filters, function (response) {
                //console.info("response:" + JSON.stringify(response));
                if (response.status == 200) {
                    //console.info(response);
                    var url = response.data.Url;
                    vm.pathFile = url;
                    vm.name = response.data.FileName;
                    var s = response.data.FileLength;
                    vm.DocUrl = vm.pathFile;
                    //console.info(vm.DocUrl);
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s);
                        //  console.info(vm.size);
                    }

                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }

                    vm.simpan(vm.DocUrl);
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    return;
                }
            }, function (response) {
                //console.info(response);
                UIControlService.msg_growl("error", "MESSAGE.API")
                UIControlService.unloadLoadingModal();
            });


        }

        vm.simpan = simpan;
        function simpan(url) {
            dataContractManagementService.insertLampiran({
                ContractManagementID: $stateParams.id,
                DocumentName: vm.nama_dokumen,
                DocumentURL: url
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.BERHASIL_SIMPAN");
                    jLoad(1);
                    vm.nama_dokumen = '';
                    vm.fileUpload;
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_SIMPAN");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.hapus = hapus;
        function hapus(data){
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.DELETEFILE') + ' ' + data.DocumentName + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    dataContractManagementService.UpdateIsActiveLampiran({
                        DetAttachmentID: data.DetAttachmentID,
                        IsActive: false
                    }, function (reply) {
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "MESSAGE.BERHASIL_DELETE");
                            jLoad(1);
                            UIControlService.unloadLoadingModal();
                        } else {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                            return;
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                        UIControlService.unloadLoadingModal();
                    });

                }
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();