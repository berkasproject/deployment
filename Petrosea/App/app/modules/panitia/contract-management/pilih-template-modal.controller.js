﻿(function () {
    'use strict';

    angular.module("app").controller("pilihTemplateModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$uibModal','$filter'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService,$uibModal,$filter) {

        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.businessField = "";
        vm.status = "";
        vm.tenderType = "";
        vm.statusManagement = item.Status;
       
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
       

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('dokumen-kontrak');
            getBusinessField();
            jLoad(1);
        }

        vm.ubahbf = ubahbf;
        function ubahbf(businessField) {
            vm.businessField = businessField;
        }

        //vm.ubahStatus = ubahStatus;
        //function ubahStatus(status) {
        //    vm.status = status;
        //    vm.jLoad(1);
        //}

        vm.ubahTenderType = ubahTenderType;
        function ubahTenderType(tenderType) {
            vm.tenderType = tenderType;
        }

        vm.act = act;
        function act(action) {
            vm.businessField = "";
            vm.status = "";
            vm.keyword = "";
            vm.tenderType = "";
            vm.action = action;
            vm.jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.businessField = vm.businessField == "" ? "0" : vm.businessField;
            vm.tenderType = vm.tenderType == "" ? "0" : vm.tenderType;

            vm.currentPage = current;
            UIControlService.loadLoadingModal('LOADING');
            DokumenKontrakService.selectCM({
                FilterType: Number(vm.action),
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword: vm.keyword,
                IntParam1: Number(vm.businessField),
                IntParam2: Number(vm.tenderType),
                IsActive: vm.status
            }, function (reply) {
                vm.docs = reply.data.List;
                vm.totalItems = reply.data.Count;
                UIControlService.unloadLoadingModal();


            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoadingModal();

                //$rootScope.unloadLoading();
            });
        }

        function getTenderType() {
            DokumenKontrakService.getTenderType(function (reply) {
                vm.dataTenderType = reply.data.List;
                if (vm.dataTenderType) {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoading();
            });
        }

        function getBusinessField() {
            DokumenKontrakService.getBusinessField(function (reply) {
                vm.dataBusinessField = reply.data.List;
                if (vm.dataBusinessField) {
                    getTenderType();
                }
            }, function (err) {
                console.info(err);
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
            });
        }

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            vm.jLoad(1);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

        vm.pilihTemplate = pilihTemplate;
        function pilihTemplate(data) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CHOOSE') + ' ' + data.DocumentName + '?<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    $uibModalInstance.close(data);
                }
            });
        }

        vm.detail = detail;
        function detail(data) {
            console.log(data);
            var data = data;
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/pilih-template-detail-modal.html',
                controller: 'pilihTemplateDetailModalController',
                controllerAs: 'pilihTemplateDetailModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }




    }
})();