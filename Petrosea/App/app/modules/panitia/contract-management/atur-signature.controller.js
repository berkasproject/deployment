﻿(function () {
    'use strict';

    angular.module("app").controller("aturSignatureController", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'dataContractManagementService', '$filter', '$rootScope', '$state', '$stateParams', '$q'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, dataContractManagementService, $filter, $rootScope, $state, $stateParams, $q) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.data = [];
        vm.dataEmployee = [];
        vm.page = $stateParams.page;

        vm.init = init;
        function init() {
            console.log('Permulaan')
            $translatePartialLoader.addPart("atur-commite");
            loadHistoryCMID().then(function (reply) {
                loadContractSignature(1).then(function(reply){
                    if(vm.data.length == 0){
                        jLoad(1)
                    } 
                }) 
            })
        }

        vm.loadContractSignature = loadContractSignature;
        function loadContractSignature(current) {
            var defer = $q.defer();
            vm.currentPage = current;
            dataContractManagementService.getContractSignature({
                IntParam1: $stateParams.id,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.data = data;
                    console.log(data)
                    defer.resolve(true)
                } else {
                    defer.reject
                }
            }, function (err) {
                defer.reject(true)
            })

            return defer.promise;

        }

        function loadHistoryCMID() {
            var defer = $q.defer();
            dataContractManagementService.getHistoryContractManagement({
                IntParam1: $stateParams.id
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.history = data;
                    defer.resolve(true)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                    defer.reject(false);
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.GAGAL_API");
                defer.reject(false);
            });
            return defer.promise;
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading('LOADING');
            vm.currentPage = current;

            dataContractManagementService.selectApprover({
                IntParam1: $stateParams.id,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoading();
                    vm.data = reply.data.List;
                    vm.totalItems = reply.data.Count;

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

                    UIControlService.unloadLoading();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();

            })
        }

        vm.contractManagement = contractManagement;
        function contractManagement(id) {
            $state.transitionTo('contract-management', { id: id })
        }

        vm.cariPegawai = cariPegawai;
        function cariPegawai() {
            var data = {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/atur-signature-modal/atur-signature-cari-pegawai.html',
                controller: 'aturSignatureCariPegawaiModalController',
                controllerAs: 'aturSignatureCariPegawaiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                vm.dataEmployee = reply;
            });
        }

        vm.hapus = hapus;
        function hapus(data) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/contract-management/atur-signature-modal/konfirmasi-modal.html',
                controller: 'konfirmasiModalController',
                controllerAs: 'konfirmasiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                var index = vm.data.findIndex(function (res) {
                    return res.EmployeeID === data.EmployeeID;
                });
                console.log(index)
                vm.data.splice(index, 1);
                console.log(vm.data)
            });


        }

        vm.tambahEmployee = tambahEmployee;
        function tambahEmployee() {
            if (vm.dataEmployee.length != 0) {
                var cekData = $.grep(vm.data, function (n) { return n.EmployeeID == vm.dataEmployee.EmployeeID; });
                if (cekData.length == 0) {
                    vm.data.push({
                    DetForumID: 0,
                    ForumID: 0,
                    Review: null,
                    ReviewFor: null,
                    ContractCommitteeID: null,
                    VendorID: null,
                    Status: "1",
                    CreatedBy: null,
                    CreatedDate: "0001-01-01T00:00:00",
                    ModifiedBy: null,
                    ModifiedDate: "2020-02-18T18:26:33.457",
                    IsActive: null,
                    Employee_id: vm.dataEmployee.EmployeeID,
                    EmployeeID: vm.dataEmployee.EmployeeID,
                    VendorName: null,
                    checkCommittee: false,
                    checkVendor: false,
                    CommitteeName: null,
                    BuyerName: null,
                    CommitteeOrVendorName: vm.dataEmployee.FullName,
                    JabatanCommitteeOrVendor: vm.dataEmployee.DepartmentName,
                    DepartementCommitteeOrVendor: vm.dataEmployee.PositionName,
                    ContractCommittee: null,
                    vendor: null,
                    ForumContractManagement: null
                    })
                    vm.dataEmployee = [];
                    console.log(vm.data)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.EMPLOYEE_EXIST");
                }
            } else {
                UIControlService.msg_growl("error", "MESSAGE.PILIH_EMPLOYEE");

            }
        }

        vm.back = back;
        function back() {
            if (vm.page == 1) {
                $state.transitionTo('data-contract-management')
            } else {
                $state.transitionTo('contract-management', {id:$stateParams.id})

            }
        }

        vm.simpan = simpan;
        function simpan() {
           
            UIControlService.loadLoading('LOADING');
            dataContractManagementService.insertContractSignature({
                ContractManagementID: $stateParams.id,
                DataInsertSignature: vm.data
            }, function (response) {
                if (response.status == 200) {
                    UIControlService.msg_growl("notice", "MESSAGE.SUCCESS_INSERT_CONTRACT_MANAGAMENT_SIGNATURE");
                    UIControlService.unloadLoading();

                    back();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                    UIControlService.unloadLoading();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");
                UIControlService.unloadLoading();

            })
        }

    }
})();