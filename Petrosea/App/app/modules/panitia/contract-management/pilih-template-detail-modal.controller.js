﻿(function () {
    'use strict';

    angular.module("app").controller("pilihTemplateDetailModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService','$sce'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService,$sce) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.ContractDocumentURL = item.ContractDocumentURL;
        vm.urlFile = vm.folderFile + vm.ContractDocumentURL;
        vm.urlFileAslie = "https://docs.google.com/gview?url=" + vm.urlFile + "&embedded=true"

        vm.urlFileAslie = $sce.trustAsResourceUrl(vm.urlFileAslie);
        console.log(vm.urlFileAslie);

        vm.init = init;
        function init() {
            console.log(item)

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();