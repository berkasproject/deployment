﻿(function () {
    'use strict';

    angular.module("app").controller("postingTopikModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'dataContractManagementService', 'DokumenKontrakService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce','$stateParams'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, dataContractManagementService,
		DokumenKontrakService, UIControlService, item, $uibModalInstance, GlobalConstantService, $sce,$stateParams) {

        var vm = this;
        vm.judul = '';
        vm.topic = '';
       
        vm.init = init;
        function init() {
            //console.log(item)
            jLoad();
        }

        function jLoad() {
            UIControlService.loadLoadingModal('LOADING');

            dataContractManagementService.getForumContractManagement({
                ContractManagementID: $stateParams.id
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    var data = reply.data;
                    if (data != null) {
                        vm.judul = data.TitleForum;
                        vm.topic = data.ContentForum;
                    }

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

            })
        }

        vm.konfirmasi = konfirmasi;
        function konfirmasi() {
            UIControlService.loadLoadingModal('LOADING');
            if (vm.judul == "" || vm.topic == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("warning", "MESSAGE.JUDULTOPIC");
                return;
            }

            dataContractManagementService.updateForumContractManagement({
                ContractManagementID: $stateParams.id,
                ContentForum: vm.topic,
                TitleForum: vm.judul
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    UIControlService.msg_growl("notice", "MESSAGE.BERHASIL_SIMPAN");
                    $uibModalInstance.close();

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERROR_API");

            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

    }
})();