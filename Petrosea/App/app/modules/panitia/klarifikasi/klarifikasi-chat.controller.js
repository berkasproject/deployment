﻿(function () {
	'use strict';

	angular.module("app").controller("ClarificationChatCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'ClarificationChatAdminService', 'GlobalConstantService', 'UIControlService', '$uibModal', '$state', '$stateParams'];
	function ctrl($translatePartialLoader, ClarificationChatAdminService, GlobalConstantService, UIControlService, $uibModal, $state, $stateParams) {
		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.tenderRefID = Number($stateParams.TenderRefID);
		vm.stepID = Number($stateParams.StepID);
		vm.procPackType = Number($stateParams.ProcPackType);
		vm.vendorID = Number($stateParams.VendorID);
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.stepInfo = {};
		vm.chats = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("klarifikasi");

			ClarificationChatAdminService.IsAllowed({
				ID: vm.stepID
			}, function (reply) {
				vm.isAllowed = reply.data;
				//loadChats(1);
			}, function (error) {
				//UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'ERR_LOAD_STEP');
			});

			UIControlService.loadLoading("");
			ClarificationChatAdminService.GetStepInfo({
				ID: vm.stepID
			}, function (reply) {
				vm.stepInfo = reply.data;
				ClarificationChatAdminService.GetVendorInfo({
					VendorID: vm.vendorID
				}, function (reply) {
					vm.vendor = reply.data;
					loadChats(1);
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'ERR_LOAD_VENDOR');
				});
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'ERR_LOAD_STEP');
			});
		}

		//tampil isi chat
		vm.loadChats = loadChats;
		function loadChats(current) {
			vm.currentPage = current;
			UIControlService.loadLoading("");
			ClarificationChatAdminService.GetChats({
				column: vm.stepID, //tenderStepDataId
				Status: vm.vendorID, //vendorId
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.chats = reply.data.List;
				vm.totalItems = reply.data.Count;
			}, function (err) {
				UIControlService.msg_growl("error", "ERR_LOAD_CHATS");
				UIControlService.unloadLoading();
			});
		}

		vm.tulis = tulis;
		function tulis() {
			var data = {
				StepID: vm.stepID,
				VendorID: vm.vendorID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/klarifikasi/write-chat.modal.html?v=1.000002',
				controller: 'WriteClarificationChatCtrl',
				controllerAs: 'wccCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				/*
                if (vm.chats.length === 0) {
                    ClarificationChatAdminService.SendMailToVendor({
                        TenderStepDataID: vm.stepID,
                        VendorID: vm.vendorID
                    }, function (reply) {
                        UIControlService.msg_growl("notice", 'SUCC_SEND_EMAIL');
                    }, function (error) {
                        UIControlService.msg_growl("error", 'ERR_SEND_EMAIL');
                        UIControlService.msg_growl("error", error.Message);
                    });
                }
                */
				loadChats(vm.currentPage);
			});
		}

		vm.back = back;
		function back(id) {
			$state.transitionTo('klarifikasi', { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
		};
	}
})();


