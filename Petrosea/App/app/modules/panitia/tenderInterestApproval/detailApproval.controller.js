﻿(function () {
    'use strict';

    angular.module("app").controller("DetailTenderInterestApprovalCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'TenderInterestApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, TenderInterestApprovalService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;
        vm.Remark = '';

        function init() {
            $translatePartialLoader.addPart('contract-signoff');
            //$translatePartialLoader.addPart('contract-variation');
            //console.info("item:" + JSON.stringify(item));
            vm.data = item.data;
            vm.isApprove = item.isApprove;

        }


        vm.action = action;
        function action() {
            TenderInterestApprovalService.approve({
                TenderInterestReviewID: vm.data.ID,
                ID: vm.data.TenderInterestApprovalID,
                IsApprove: vm.isApprove,
                Remark: vm.Remark,
                TenderStepDataID:vm.data.TenderStepDataID
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "Berhasil memproses data");
                    $uibModalInstance.close();
                    //init();
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "Gagal memproses data");
                UIControlService.unloadLoading();
            });
        }

        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }


    }
})();
//TODO


