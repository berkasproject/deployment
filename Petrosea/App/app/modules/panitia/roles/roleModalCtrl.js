﻿(function () {
	'use strict';

	angular.module("app").controller("roleModalCtrl", roleModalCtrl);

	roleModalCtrl.$inject = ['$stateParams', 'UIControlService', 'RoleService', '$state'];

	function roleModalCtrl($stateParams, UIControlService, RoleService, $state) {
		var vm = this;
		vm.ID = Number($stateParams.RoleID);

		vm.init = init;
		function init() {
			loadRole();
		}

		vm.loadRole = loadRole;
		function loadRole() {
			UIControlService.loadLoading("Silahkan Tunggu...");
			RoleService.getRoleData({
				RoleID: vm.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.Name = reply.data.RoleName;
					vm.Modules = reply.data.Modules;
					vm.AvailModules = reply.data.AvailModules;
				} else {
					UIControlService.msg_growl("error", 'NOTIFICATION.GETUSER.ERROR.MESSAGE', "NOTIFICATION.GETUSER.ERROR.MESSAGE");
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.addModule = addModule;
		function addModule() {
			if (vm.selectedAvailModule === null) return false;

			vm.Modules.push(vm.selectedAvailModule);
			vm.AvailModules = RoleService.remove(vm.AvailModules, vm.selectedAvailModule);
		}

		vm.removeModule = removeModule;
		function removeModule() {
			if (vm.selectedModule === null) return false;

			vm.AvailModules.push(vm.selectedModule);
			vm.Modules = RoleService.remove(vm.Modules, vm.selectedModule);
		}

		vm.saveRole = saveRole;
		function saveRole() {
			if (vm.Name === null || vm.Name === '') {
				return false;
			}
			UIControlService.loadLoadingModal('LOADING.SAVE.USER');
			RoleService.saveRole({
				RoleID: vm.ID,
				RoleName: vm.Name,
				Modules: vm.Modules
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'NOTIFICATION.CHANGE.SUCCESS.MESSAGE', 'NOTIFICATION.CHANGE.SUCCESS.MESSAGE');
					$state.transitionTo('role-management');
				} else {
					UIControlService.msg_growl("error", 'NOTIFICATION.CHANGE.ERROR.MESSAGE', "NOTIFICATION.CHANGE.ERROR.MESSAGE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", err.Message);
			});
		}

		vm.toMasterRole = toMasterRole;
		function toMasterRole() {
			$state.transitionTo('role-management');
		}
	}
})();