﻿(function () {
    'use strict';

    angular.module("app").controller("PPPrequalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader','SocketService', 'PPPrequalService', 'UIControlService', '$state', '$stateParams', '$uibModal', 'GlobalConstantService', 'AssessmentPrequalService', '$timeout'];

    function ctrl($http, $translate, $translatePartialLoader, SocketService, PPPrequalService, UIControlService, $state, $stateParams, $uibModal, GlobalConstantService, AssessmentPrequalService, $timeout) {
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var vm = this;
        vm.data = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.jLoad = jLoad;
        vm.PrequalStepID = Number($stateParams.SetupStepID);
        vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
            $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
            jLoad(1);
            loadPrequalEntry();
        }

        vm.loadPrequalEntry = loadPrequalEntry;
        function loadPrequalEntry() {
            PPPrequalService.selectPrequalStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.step = reply.data;
                    vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                    vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
                    UIControlService.unloadLoading();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
            });
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.list = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            PPPrequalService.GetAll({
                Status: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.list = reply.data;
                    console.info("data:" + JSON.stringify(vm.list));
                    vm.list.forEach(function (list) {
                        list.CreatedDate = UIControlService.convertDateTime(list.CreatedDate);
                    });
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.pdfassesment = pdfassesment;
        function pdfassesment(dt) {
            var headers = {};
            headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

            $http({
                method: 'POST',
                url: adminpoint + '/vendorEntryPrequalAdmin/assessmentReport',
                headers: headers,
                data: {
                    VendorPrequalID: dt.VendorPrequalID,
                    PrequalSetupStep: {
                        PrequalSetupID: vm.PrequalSetupID
                    }
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                var linkElement = document.createElement('a');
                var fileName = "Lembar Verifikasi dan Assessment " + dt.VendorPrequal.VendorName + ".docx";

                try {
                    var blob = new Blob([data], { type: headers('content-type') });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute('download', fileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);
                } catch (e) {
                    console.log(e);
                }
            });
        }
        /*
        vm.pdfassesment = pdfassesment;
        function pdfassesment(current) {
            //console.info("curr "+current)
            PPPrequalService.assessmentReport({
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.list = reply.data;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API" + err });
                UIControlService.unloadLoading();
            });
        }
        */
        vm.aturAssessment = aturAssessment;
        function aturAssessment(data) {
            console.info("data:"+JSON.stringify(data));
            var senddata = {
                PrequalSetupID: vm.PrequalSetupID,
                PrequalStepID:vm.PrequalStepID,
                data: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/pemasukkan-prakualifikasi/aturAssessment.html',
                controller: 'AturAssessmentCtrl',
                controllerAs: 'AturAssessmentCtrl',
                resolve: { item: function () { return senddata; } }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.uploadAssessment = uploadAssessment;
        function uploadAssessment(data) {
            console.info("data:" + JSON.stringify(data));
            var senddata = {
                PrequalSetupID: vm.PrequalSetupID,
                PrequalStepID: vm.PrequalStepID,
                data: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/pemasukkan-prakualifikasi/uploadAssessment.html',
                controller: 'UploadAssessmentCtrl',
                controllerAs: 'UploadAssessmentCtrl',
                resolve: { item: function () { return senddata; } }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.backpengadaan = backpengadaan;
        function backpengadaan() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType });
        }


        vm.kas = 0;
        vm.logamMulia = 0;
        vm.deposito = 0;
        vm.stokBarang = 0;
        vm.kendaraan2 = 0;
        vm.kendaraan4 = 0;
        vm.piutang = 0;
        vm.hutangGaji = 0;
        vm.hutangSewa = 0;
        vm.hutangAsuransi = 0;
        vm.hutangCicilan = 0;
        vm.hutangPinjamanBank = 0;
        vm.lain2 = 0;

        vm.asset = 0;
        vm.hutang = 0;
        vm.modal = 0;
        vm.printAss = printAss;
        function printAss(dt) {
            //vm.exportAss = exportPO;
            //vm.popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');

            AssessmentPrequalService.LoadAssessmentReport({
                VendorPrequalID: dt.VendorPrequalID,
                PrequalSetupStep: {
                    PrequalSetupID: vm.PrequalSetupID
                }

            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataReport = reply.data;
                    console.info("balance:" + JSON.stringify(vm.dataReport));
                    vm.dataReport.contact.forEach(function (data) {
                        if (data.VendorContactTypeID == 20) {
                            vm.Phone = data.Contact.Phone;
                            vm.Fax = data.Contact.Fax;
                            vm.Email = data.Contact.Email;
                        }
                        if (data.VendorContactTypeID == 1042) {
                            if (data.Contact.Address.StateID != null) {
                                if (data.Contact.Address.CityID != null) {
                                    if (data.Contact.Address.DistrictID != null) vm.addressPrequal = data.Contact.Address.AddressInfo + ", " + data.Contact.Address.Distric.Name + ", " + data.Contact.Address.City.Name + ", " + data.Contact.Address.State.Name + ", " + data.Contact.Address.State.Country.Name;
                                    else vm.addressPrequal = data.Contact.Address.AddressInfo + ", " + data.Contact.Address.City.Name + ", " + data.Contact.Address.State.Name + ", " + data.Contact.Address.State.Country.Name;
                                }
                                else vm.addressPrequal = data.Contact.Address.AddressInfo + ", " + data.Contact.Address.State.Name + ", " + data.Contact.Address.State.Country.Name;
                            }
                            else vm.addressPrequal = data.Contact.Address.AddressInfo;
                        }
                    });

                    for (var i = 0; i < vm.dataReport.balance.length; i++) {
                        //if (vm.dataReport.balance[i].WealthType.Name == "WEALTH_TYPE_ASSET") {
                        if (vm.dataReport.balance[i].subWealth.length != 0) {
                            for (var j = 0; j < vm.dataReport.balance[i].subWealth.length; j++) {
                                if (vm.dataReport.balance[i].subWealth[j].subCategory.length > 0) {
                                    for (var k = 0; k < vm.dataReport.balance[i].subWealth[j].subCategory.length; k++) {
                                        if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_CASH_TYPE1") {
                                            vm.kas = vm.kas + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_CASH_TYPE2") {
                                            vm.logamMulia = vm.logamMulia + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_CASH_TYPE3") {
                                            vm.deposito = vm.deposito + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_DEBTHSTOCK_TYPE1") {
                                            vm.hutangAsuransi = vm.hutangAsuransi + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_DEBTHSTOCK_TYPE2") {
                                            vm.hutangGaji = vm.hutangGaji + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_DEBTHSTOCK_TYPE3") {
                                            vm.hutangSewa = vm.hutangSewa + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].SubCOA.Name == "SUB_DEBTHSTOCK_TYPE4") {
                                            vm.hutangCicilan = vm.hutangCicilan + vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                    }
                                }
                            }
                            //}
                            /*
                            if (vm.dataReport.balance[i].Wealth.Name == "WEALTH_TYPE_DEBTH") {
                                vm.listDebth = vm.dataReport.balance[i];
                            }*/
                        }
                    }
                    //console.info("asset:" + JSON.stringify(vm.listAsset));

                    for (var i = 0; i < vm.dataReport.balance.length; i++) {
                        for (var j = 0; j < vm.dataReport.balance[i].subWealth.length; j++) {
                            if (vm.dataReport.balance[i].subWealth[j].subCategory.length === 0) {
                                if (vm.dataReport.balance[i].WealthType.RefID === 3097 && vm.dataReport.balance[i].subWealth[j].IsActive === true) {
                                    if (vm.asset === 0) {
                                        vm.asset = vm.dataReport.balance[i].subWealth[j].nominal;
                                        console.info(vm.asset);
                                    }
                                    else
                                        vm.asset = +vm.asset + +vm.dataReport.balance[i].subWealth[j].nominal;
                                    console.info(vm.asset);

                                }
                                else if (vm.dataReport.balance[i].WealthType.RefID === 3099 && vm.dataReport.balance[i].subWealth[j].IsActive === true) {
                                    if (vm.hutang === 0) {
                                        vm.hutang = vm.dataReport.balance[i].subWealth[j].nominal;
                                        console.info(vm.hutang);
                                    }
                                    else {
                                        vm.hutang = +vm.hutang + +vm.dataReport.balance[i].subWealth[j].nominal;
                                        console.info(vm.hutang);
                                    }


                                }
                            }
                            else {
                                for (var k = 0; k < vm.dataReport.balance[i].subWealth[j].subCategory.length; k++) {
                                    console.info(vm.dataReport.balance[i].subWealth[j].subCategory[k]);
                                    if (vm.dataReport.balance[i].subWealth[j].subCategory[k].Wealth.RefID === 3097 && vm.dataReport.balance[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.asset === 0) {
                                            vm.asset = vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                            console.info(vm.asset);
                                        }
                                        else
                                            vm.asset = +vm.asset + +vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                        console.info(vm.asset);

                                    }
                                    else if (vm.dataReport.balance[i].subWealth[j].subCategory[k].WealthType === 3099 && vm.dataReport.balance[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.hutang === 0) {
                                            vm.hutang = vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                            console.info(vm.hutang);
                                        }
                                        else {
                                            vm.hutang = +vm.hutang + +vm.dataReport.balance[i].subWealth[j].subCategory[k].Nominal;
                                            console.info(vm.hutang);
                                        }


                                    }
                                }
                            }

                        }
                    }

                    vm.modal = +vm.asset - +vm.hutang;

                    $timeout(function () {
                        vm.innerContents = document.getElementById(vm.exportAss).innerHTML;
                        vm.popupWindow.document.open();
                        vm.popupWindow.document.write('<html><head><title>Lembar Verifikasi Berkas</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + vm.innerContents + '</body></html>');
                        vm.popupWindow.document.close();
                    }, 3000);
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });


            //$uibModalInstance.close();
        }
    }
})();


