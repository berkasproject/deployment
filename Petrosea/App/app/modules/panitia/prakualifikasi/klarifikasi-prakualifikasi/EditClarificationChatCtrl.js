﻿(function () {
	'use strict';

	angular.module("app").controller("EditClarificationChatCtrl", ctrl);

	ctrl.$inject = ['item', '$uibModalInstance', 'UIControlService', 'PrequalificationClarificationChatAdminService'];
	function ctrl(item, $uibModalInstance, UIControlService, PrequalificationClarificationChatAdminService) {
		var vm = this;
		vm.message = item.Message;

		vm.simpan = simpan;
		function simpan(IsSent) {
			UIControlService.loadLoadingModal("");
			if (vm.message == "" || vm.message == " ") {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "ERR_BLANK_MESSAGE");
			} else {
				PrequalificationClarificationChatAdminService.EditChat({
					ID: item.ID,
					Message: vm.message
				}, function (reply) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("success", "SUCC_EDIT_MESSAGE");
					$uibModalInstance.close();
				}, function (err) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "ERR_EDIT_MESSAGE");
				});
			}
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss();
		}
	}
})();