﻿(function () {
	'use strict';

	angular.module("app").controller("PrequalClrChatCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'PrequalificationClarificationChatAdminService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($translatePartialLoader, PrequalificationClarificationChatAdminService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {
		var vm = this;

		vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
		vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.PrequalVendorID = Number($stateParams.PrequalVendorID);
		vm.IsPeriodOpen = false;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.stepInfo = {};
		vm.chats = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("klarifikasi-prakualifikasi");
			UIControlService.loadLoading("");
			PrequalificationClarificationChatAdminService.GetVendorInfo({
				ID: vm.PrequalVendorID
			}, function (reply) {
				vm.vendor = reply.data;
				PrequalificationClarificationChatAdminService.GetStepInfo({
					PrequalStepID: vm.PrequalStepID
				}, function (reply) {
					vm.stepInfo = reply.data;
					PrequalificationClarificationChatAdminService.GetIsPeriodOpen({
						ID: vm.PrequalStepID
					}, function (reply) {
						vm.IsPeriodOpen = reply.data;
						loadChats(1);
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'ERR_GETPERIODOPEN');
					})
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'ERR_STEPINFO');
				})

			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'ERR_LOAD_VENDOR');
			});
		}

		vm.sendDraft = function (ID) {
			UIControlService.loadLoading("SEND_MESSAGE");
			PrequalificationClarificationChatAdminService.SendDraft({
				ID: ID //tenderStepDataId
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.init();
			}, function (err) {
				UIControlService.msg_growl("error", "ERR_LOAD_CHATS");
				UIControlService.unloadLoading();
			});

		}
		vm.deleteDraft = function (ID) {
			UIControlService.loadLoading("MESSAGE.DELETING");
			PrequalificationClarificationChatAdminService.DeleteDraft({
				ID: ID //tenderStepDataId
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.init();
			}, function (err) {
				UIControlService.msg_growl("error", "ERR_LOAD_CHATS");
				UIControlService.unloadLoading();
			});
		}

		//tampil isi chat
		vm.loadChats = loadChats;
		function loadChats(current) {
			vm.currentPage = current;
			UIControlService.loadLoading("");
			PrequalificationClarificationChatAdminService.GetChats({
				column: vm.PrequalStepID, //tenderStepDataId
				Status: vm.PrequalVendorID, //vendorId
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.chats = reply.data.List;

				for (var i = 0; i < vm.chats.length; i++) {
					vm.chats[i].MessageHtml = vm.chats[i].Message.replace(/(?:\r\n|\r|\n)/g, '<br>');
				}

				vm.totalItems = reply.data.Count;
			}, function (err) {
				UIControlService.msg_growl("error", "ERR_LOAD_CHATS");
				UIControlService.unloadLoading();
			});
		}

		vm.tulis = tulis;
		function tulis() {
			var data = {
				PrequalStepID: vm.PrequalStepID,
				PrequalVendorID: vm.PrequalVendorID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/klarifikasi-prakualifikasi/write-chat.modal.html',
				controller: 'WriteClarificationChatCtrl',
				controllerAs: 'wccCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				if (vm.chats.length === 0) {
					PrequalificationClarificationChatAdminService.SendMailToVendor({
						PrequalSetupStepID: vm.PrequalStepID,
						PrequalVendorID: vm.PrequalVendorID
					}, function (reply) {
						UIControlService.msg_growl("notice", 'SUCC_SEND_EMAIL');
					}, function (error) {
						UIControlService.msg_growl("error", 'ERR_SEND_EMAIL');
					});
				}
				loadChats(vm.currentPage);
				//loadChats(vm.currentPage);
			}, function () {
				loadChats(vm.currentPage);
			});
		}

		vm.editDraft = editDraft;
		function editDraft(dataChat) {
			var data = {};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/klarifikasi-prakualifikasi/edit-chat.modal.html',
				controller: 'EditClarificationChatCtrl',
				controllerAs: 'EditClarificationChatCtrl',
				resolve: {
					item: function () {
						return dataChat;
					}
				}
			});
			modalInstance.result.then(function () {
				if (vm.chats.length === 0) {
					PrequalificationClarificationChatAdminService.SendMailToVendor({
						PrequalSetupStepID: vm.PrequalStepID,
						PrequalVendorID: vm.PrequalVendorID
					}, function (reply) {
						UIControlService.msg_growl("notice", 'SUCC_SEND_EMAIL');
					}, function (error) {
						UIControlService.msg_growl("error", 'ERR_SEND_EMAIL');
					});
				}
				loadChats(vm.currentPage);
				//loadChats(vm.currentPage);
			});
		}

		vm.back = back;
		function back(id) {
			$state.transitionTo('klarifikasi-hasil-prakualifikasi', { PrequalSetupID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepID });
		};
	}
})();


