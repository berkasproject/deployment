﻿(function () {
	'use strict';

	angular.module("app").controller("summaryModalCtrl", ctrl);

	ctrl.$inject = ['hasilPrakualApprvSvc', 'UIControlService', 'item', '$uibModalInstance', '$translatePartialLoader'];

	function ctrl(hasilPrakualApprvSvc, UIControlService, item, $uibModalInstance, $translatePartialLoader) {
		var vm = this;
		vm.isCanEdit = item.IsCanEdit;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('hasil-prakualifikasi-approval');
			getSummary();
		}

		vm.close = close;
		function close() {
			$uibModalInstance.dismiss('cancel');
		}

		function getSummary() {
			hasilPrakualApprvSvc.getSummary({
				PrequalSetupStepID: item.PrequalSetupStepID
			}, function (reply) {
				if (reply.status === 200) {
					vm.SummaryText = reply.data;
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETSUMMARY.ERROR');
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETSUMMARY.ERROR');
			});
		}

		vm.saveSummary = saveSummary;
		function saveSummary() {
			hasilPrakualApprvSvc.saveSummary({
				PrequalSetupStepID: item.PrequalSetupStepID,
				SummaryText: vm.SummaryText
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", 'NOTIFICATION.SAVESUMMARY.SUCCESS');
					$uibModalInstance.close('close');
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.SAVESUMMARY.ERROR');
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.SAVESUMMARY.ERROR');
			});
		}
	}
})()