﻿(function () {
	'use strict';

	angular.module("app").controller("hasilPrakualifikasiApprovalCtrl", ctrl);

	ctrl.$inject = ['$stateParams', '$translatePartialLoader', 'hasilPrakualApprvSvc', 'UIControlService', '$filter', '$uibModal'];
	/* @ngInject */
	function ctrl($stateParams, $translatePartialLoader, hasilPrakualApprvSvc, UIControlService, $filter, $uibModal) {
		var vm = this;
		vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
		vm.PrequalStepName = ""
		vm.PrequalSetupName = "";
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.Vendors = []
		vm.IsApprovalSent = false;
		vm.IsNeedApproval = false;
		vm.IsIssuer = false;
		vm.formOpen = false;
		vm.approvalComment = ""
		vm.init = init;
		vm.VendorIDs = [];
		vm.SelectedVendorIDs = [];

		function init() {
			$translatePartialLoader.addPart("hasil-prakualifikasi-approval");
			getStep();
			isIssuer();
			//console.info(vm.IsIssuer);
			//openForm();
			//console.info(vm.formOpen);
			getVendors();
			//getApproval();
		}

		vm.changeAll = changeAll;
		function changeAll() {
			vm.SelectedVendorIDs = [];

			for (var i = 0; i < vm.Vendors.length; i++) {
				if (vm.IsPublishAll) {
					if (vm.Vendors[i].ApprovalResult === null || vm.Vendors[i].ApprovalResult === 'REJECT') {
						vm.Vendors[i].ApprovalSent = true;
						vm.SelectedVendorIDs.push(vm.Vendors[i].VendorPrequalID);
					}
				} else {
					if (vm.Vendors[i].ApprovalResult === null || vm.Vendors[i].ApprovalResult === 'REJECT')
						vm.Vendors[i].ApprovalSent = false;
				}
			}
		}

		function isIssuer() {
			hasilPrakualApprvSvc.isIssuer({
				PrequalSetupStepID: vm.PrequalStepID
			}, function (reply) {
				if (reply.status === 200) {
					vm.IsIssuer = reply.data
					//return reply.data;
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETISISSUER.ERROR', "NOTIFICATION.GETISISSUER.TITLE");
				}
			});
		}
		function getApproval() {
			hasilPrakualApprvSvc.getApproval({
				PrequalSetupStepID: vm.PrequalStepID
			}, function (reply) {
				if (reply.status === 200) {
					console.info(reply.data);
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETAPPROVAL.ERROR', "NOTIFICATION.GETAPPROVAL.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETAPPROVAL.ERROR', "NOTIFICATION.GETAPPROVAL.TITLE");
			});
		}

		function getStep() {
			hasilPrakualApprvSvc.getStep({
				ID: vm.PrequalStepID
			}, function (reply) {
				if (reply.status === 200) {
					vm.PrequalStepName = reply.data.PrequalStepName;
					vm.PrequalSetupName = reply.data.PrequalSetupName;
					vm.tanggalMulai = reply.data.StartDate;
					vm.tanggalSelesai = reply.data.EndDate;
					vm.formOpen = reply.data.IsOpen;
					vm.StartDate = UIControlService.convertDateTime(vm.tanggalMulai);
					vm.EndDate = UIControlService.convertDateTime(vm.tanggalSelesai);
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETSTEP.ERROR', "NOTIFICATION.GETSTEP.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETSTEP.ERROR', "NOTIFICATION.GETSTEP.TITLE");
			});
		}

		//vm.sendApproval = sendApproval;
		vm.sendApproval = function () {
			if (vm.SelectedVendorIDs.length == 0) {
				UIControlService.msg_growl("error", 'NOTIFICATION.SELECTVENDOR.ERROR', "NOTIFICATION.SELECTVENDOR.TITLE");
			} else {
				bootbox.confirm($filter('translate')('MESSAGE.SENDAPPROVECONFIRM'), function (res) {
					if (res) {
						hasilPrakualApprvSvc.sendApproval({
							PrequalSetupStepID: vm.PrequalStepID,
							PrequalVendorIDs: vm.SelectedVendorIDs
						}, function (reply) {
							if (reply.status === 200) {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("success", 'NOTIFICATION.SENDAPPROVAL.SUCCESS', "NOTIFICATION.SENDAPPROVAL.TITLE");
								//init();
								window.location.reload();
							} else {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", 'NOTIFICATION.SENDAPPROVAL.ERROR', "NOTIFICATION.SENDAPPROVAL.TITLE");
							}
						}, function (err) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'NOTIFICATION.GETVENDOR.ERROR', "NOTIFICATION.GETVENDOR.TITLE");
						});
					}
				});
			}
		}

		vm.approve = function () {
			bootbox.confirm($filter('translate')('MESSAGE.APPROVECONFIRM'), function (res) {
				if (res) {
					hasilPrakualApprvSvc.approve({
						Approvals: vm.Vendors,
						ApprovalComment: vm.approvalComment
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("success", 'NOTIFICATION.APPROVE.SUCCESS', "NOTIFICATION.APPROVE.TITLE");
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'NOTIFICATION.APPROVE.ERROR', "NOTIFICATION.APPROVE.TITLE");
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'NOTIFICATION.APPROVE.ERROR', "NOTIFICATION.APPROVE.TITLE");
					});
				}
			});
		}

		vm.reject = function () {
			bootbox.confirm($filter('translate')('MESSAGE.REJECTCONFIRM'), function (res) {
				if (res) {
					hasilPrakualApprvSvc.reject({
						Approvals: vm.Vendors,
						PrequalSetupStepID: vm.PrequalStepID,
						ApprovalComment: vm.approvalComment
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("success", 'NOTIFICATION.REJECT.SUCCESS', "NOTIFICATION.REJECT.TITLE");
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'NOTIFICATION.REJECT.ERROR', "NOTIFICATION.REJECT.TITLE");
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'NOTIFICATION.REJECT.ERROR', "NOTIFICATION.REJECT.TITLE");
					});
				}
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			vm.VendorsView = vm.Vendors.slice((vm.currentPage - 1) * vm.maxSize, ((vm.currentPage - 1) * vm.maxSize) + vm.maxSize);
		}

		vm.addSendApproval = function (VendorID) {
			vm.IsPublishAll = false;
			var idx = vm.SelectedVendorIDs.indexOf(VendorID);

			if (idx > -1) {
				vm.SelectedVendorIDs.splice(idx, 1);
			} else {
				vm.SelectedVendorIDs.push(VendorID);
			}
			console.info(vm.SelectedVendorIDs);
		}

		vm.showSummaryModal = showSummaryModal;
		function showSummaryModal() {
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/hasil-prakualifikasi-approval/summaryModal.html',
				controller: "summaryModalCtrl",
				controllerAs: "summaryModalCtrl",
				resolve: {
					item: {
						PrequalSetupStepID: vm.PrequalStepID,
						IsCanEdit: vm.IsIssuer
					}
				}
			});

			modalInstance.result.then(function () { init(); });
		}

		vm.DetailApproval = function (VendorPrequalID) {
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/hasil-prakualifikasi-approval/detailApproval.html',
				controller: "hasilPrakualApprvCtrl",
				controllerAs: "hasilPrakualApprvCtrl",
				resolve: {
					item: {
						VendorPrequalID: VendorPrequalID,
						PrequalStepID: vm.PrequalStepID
					}
				}
			});

			modalInstance.result.then(function () { init(); });
		}

		vm.submitApproval = function () {
			hasilPrakualApprvSvc.submitApproval({
				Approvals: vm.Vendors,
				PrequalSetupStepID: vm.PrequalStepID
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", 'NOTIFICATION.SUBMITAPPROVAL.SUCCESS', "NOTIFICATION.SUBMITAPPROVAL.TITLE");
					getVendors();
				}
			});
		}

		function getVendors() {
			hasilPrakualApprvSvc.getVendors({
				//Offset: (vm.currentPage - 1) * vm.maxSize,
				//Limit: vm.maxSize,
				//Keyword: vm.keyword,
				Status: vm.PrequalSetupID,
				IntParam1: vm.PrequalStepID,
			}, function (reply) {
				if (reply.status === 200) {
					vm.Vendors = reply.data.List;
					vm.VendorsView = vm.Vendors.slice((vm.currentPage - 1) * vm.maxSize, ((vm.currentPage - 1) * vm.maxSize) + vm.maxSize);
					vm.passedItems = vm.Vendors.map(a=>a.IsPassed).filter(function (a) { return a === true }).length;
					vm.eliminatedItems = vm.Vendors.map(a=>a.IsPassed).filter(function (a) { return a === false }).length;

					//for (var i = 0; i < vm.Vendors.length; i++) {
					//if (vm.Vendors[i].IsCheckedNpwp) {
					//	vm.SelectedVendorIDs.push(vm.Vendors[i].VendorID);
					//}
					//if (vm.IsIssuer) {
					//  hasilPrakualApprvSvc.getStatus({
					//    HeaderID: vm.PrequalStepID
					//  }, function (reply) {
					//    if (reply.status === 200) {
					//      console.info(reply);
					//      if (reply.data == "ONPROCESS" || reply.data=="APPROVE") {
					//        vm.IsApprovalSent = true;
					//      }
					//      else {
					//        vm.IsApprovalSent = false;
					//      }
					//    }
					//    else {
					//      UIControlService.unloadLoading();
					//      UIControlService.msg_growl("error", 'NOTIFICATION.GETSTATUS.ERROR', "NOTIFICATION.GETSTATUS.TITLE");
					//    }
					//  },
					//function (err) {
					//  UIControlService.unloadLoading();
					//  UIControlService.msg_growl("error", 'NOTIFICATION.GETSTATUS.ERROR', "NOTIFICATION.GETSTATUS.TITLE");
					//});
					//}
					//else {
					//  hasilPrakualApprvSvc.isNeedApproval({
					//    //params - getstatus
					//    PrequalSetupStepID: vm.PrequalStepID
					//  }, function (reply) {
					//    if (reply.status === 200) {
					//        vm.IsNeedApproval = reply.data;
					//    }
					//    else {
					//      UIControlService.unloadLoading();
					//      UIControlService.msg_growl("error", 'NOTIFICATION.GETSTATUS.ERROR', "NOTIFICATION.GETSTATUS.TITLE");
					//    }
					//  },
					//function (err) {
					//  UIControlService.unloadLoading();
					//  UIControlService.msg_growl("error", 'NOTIFICATION.GETSTATUS.ERROR', "NOTIFICATION.GETSTATUS.TITLE");
					//});
					//}
					//}

					//hasilPrakualApprvSvc.getStatus({
					//}, function (result) {

					//}, function (err) {

					//});
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
					//vm.SelectedVendorIDs = [];
					//console.info(vm.Vendors);
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETVENDOR.ERROR', "NOTIFICATION.GETVENDOR.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETVENDOR.ERROR', "NOTIFICATION.GETVENDOR.TITLE");
			});
		}
	}
})();