(function () {
	'use strict';
	angular.module("app")
    .controller("PrequalEvaluationEquipmentController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'PrequalEvaluationService', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, PrequalEvaluationService, $http, $uibModal, $translate, $translatePartialLoader, $location, UIControlService) {
	    var vm = this;

	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
	    vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
	    vm.VendorID = Number($stateParams.VendorID);        

	    vm.totalCostEquip = 0;
	    vm.totalCostVehic = 0;
	    vm.totalDeprEquip = 0;
	    vm.totalDeprVehic = 0;

		vm.init = init;
		function init() {
            $translatePartialLoader.addPart('evaluasi-prakualifikasi');
		    loadStep();
        }

		function loadStep() {

		    PrequalEvaluationService.GetStepData({
		        ID: vm.PrequalStepID
		    }, function (reply) {
		        vm.step = reply.data;
		        vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
		        vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
		    }, function (err) {

		    });

		    PrequalEvaluationService.GetVendorName({
		        VendorID: vm.VendorID
		    }, function (reply) {
		        vm.VendorName = reply.data;
		    }, function (err) {

		    });

		    loadVehicle();
		}

		vm.listVehicle = [];
		function loadVehicle() {
		    UIControlService.loadLoading("");
		    PrequalEvaluationService.GetVehicles({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		    },
            function (reply) {
                UIControlService.unloadLoading();
                vm.listVehicle = reply.data;
                //calculateDepreciation(vm.listVehicle);
                vm.totalCostVehic = calculateTotalCost(vm.listVehicle);                
                vm.totalDeprVehic = calculateTotalDepreciated(vm.listVehicle)
                loadEquipment();
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETVEHICLE");
            });
		}

        vm.listEquipment = [];
        function loadEquipment() {
            UIControlService.loadLoading("");
            PrequalEvaluationService.GetEquipments({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorID: vm.VendorID
            },
            function (reply) {
                UIControlService.unloadLoading();
                vm.listEquipment = reply.data;
                //calculateDepreciation(vm.listEquipment);
                vm.totalCostEquip = calculateTotalCost(vm.listEquipment);
                vm.totalDeprEquip = calculateTotalDepreciated(vm.listEquipment)
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETEQUIP");
            });
        }

        /*
        function calculateDepreciation(equipments) {
            equipments.forEach(function (eq) {
                eq.Depreciation = 0;
                eq.DepreciationPercent = 0;
                eq.Depreciated = 0;
                if (eq.Cost > 0) {
                    var life = eq.Life > 0 ? eq.Life : 1;
                    eq.Depreciation = (eq.Cost - eq.Salvage) / life;
                    eq.DepreciationPercent = eq.Depreciation * 100 / eq.Cost;
                    eq.Depreciated = eq.Cost - eq.Depreciation;
                }
            });
        }
        */

        function calculateTotalCost(equipments) {
            var totalCost = 0;
            equipments.forEach(function (eq) {
                totalCost += eq.Cost;
            });
            return totalCost;
        }

        function calculateTotalDepreciated(equipments) {
            var totalDepreciated = 0;
            equipments.forEach(function (eq) {
                totalDepreciated += eq.Depreciated;
            });
            return totalDepreciated;
        }
	}
})();