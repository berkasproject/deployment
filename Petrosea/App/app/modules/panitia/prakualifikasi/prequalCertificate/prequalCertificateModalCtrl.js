﻿(function () {
	'use strict';

	angular.module("app").controller("prequalCertificateModalCtrl", ctrl);

	ctrl.$inject = ['UploadFileConfigService', 'UploaderService', 'UIControlService', 'item', '$uibModalInstance', 'PrequalCertificateService'];

	function ctrl(UploadFileConfigService, UploaderService, UIControlService, item, $uibModalInstance, PrequalCertificateService) {
		var vm = this;
		vm.VendorID = item.VendorID;
		vm.prequalCode = item.PrequalCode
		vm.detail = {};
		vm.Vendors = item.Vendors;
		vm.stepId = item.PrequalSetupStepID;
		vm.init = init;
		function init() {
			loadConfig();
		}

		vm.loadConfig = loadConfig;
		function loadConfig() {
			UploadFileConfigService.getByPageName("PAGE.ADMIN.PREQCERT", function (response) {
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
			});
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.UploadVendorCertificate = UploadVendorCertificate;
		function UploadVendorCertificate() {
			if (vm.fileUpload === undefined || vm.fileUpload === null) {
				UIControlService.msg_growl("error", "MESSAGE.NO_DOC");
				return;
			} else {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			}
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss();
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
			vm.prefix = "Certificate_Prequal_" + vm.VendorID;
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}

			UploaderService.uploadSinglePrequalCert(vm.prequalCode.replace('#', '_'), vm.prefix, file, size, filters, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.pathFile = url;
					vm.name = response.data.FileName;
					vm.detail = {
						FilePath: vm.pathFile,
						FileName: vm.name
					};

					var s = response.data.FileLength;
					if (vm.flag == 0) vm.size = Math.floor(s);
					else if (vm.flag == 1) vm.size = Math.floor(s / 1024);

					PrequalCertificateService.UpdateVendorPreqCert({
						Vendors: vm.Vendors,
						PrequalSetupStepID: vm.stepId,
						VendorID: vm.VendorID,
						CertificateUrl: vm.pathFile
					}, function (reply) {
						$uibModalInstance.close(vm.detail);
					});
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});
		}
	}
})();