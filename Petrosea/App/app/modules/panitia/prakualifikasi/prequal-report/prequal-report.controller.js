(function () {
	'use strict';

	angular.module("app").controller("PrequalReportCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'PrequalReportService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, PrequalReportService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.arrKPI = [];
		vm.arrOntime = [];
		vm.arrSavingCost = [];

		vm.jenistender = null;
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.currentPageCP = 1;
		vm.currentPageCS = 1;

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    prequalCreated();
            
		    $translatePartialLoader.addPart('prequal-report');
            
		}

        vm.prequalCreated=prequalCreated;
		function prequalCreated(startdate,enddate) {
		    PrequalReportService.prequalCreated(function (reply) {
		        if (reply.status === 200) {
		            vm.prequalCreatedList = reply.data;
		            console.info("prequalCreated:" + JSON.stringify(vm.prequalCreatedList));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
		        UIControlService.unloadLoading();
		    });
		}

		vm.detail = detail;
		function detail(data) {
		    $state.go('detail-laporan-prakualifikasi', { PrequalSetupID: data.ID })
		}


	}
})();
