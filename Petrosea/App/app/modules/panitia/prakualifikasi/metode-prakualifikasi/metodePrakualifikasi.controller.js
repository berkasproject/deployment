﻿(function () {
	'use strict';
	angular.module("app").controller("PrequalMethodController", ctrl);
	ctrl.$inject = ['$uibModal', '$filter','$translatePartialLoader', 'MetodePrakualifikasiService', 'UIControlService'];

	function ctrl($uibModal, $filter, $translatePartialLoader, MetodePrakualifikasiService, UIControlService) {
		var vm = this;

		vm.methods = {};
		//vm.allowAdd = true;
		//vm.allowEdit = true;
		//vm.allowDelete = true;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.totalItems = 0;
		//vm.goodsOrService = 0;

		vm.loadMethods = loadMethods;
		function loadMethods() {
		    $translatePartialLoader.addPart('metode-prakualifikasi');
		    UIControlService.loadLoading("LOADING");
		    MetodePrakualifikasiService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword
			}, function (reply) {
				vm.methods = reply.data.List;
				vm.totalItems = reply.data.Count;
				UIControlService.unloadLoading();
			}, function (err) {
			    UIControlService.msg_growl("error", "ERR_LOAD");
			    UIControlService.unloadLoading();
			});
		}

        /*
		function getUserLogin() {
		    UIControlService.loadLoading("Silahkan Tunggu...");
		    PurchReqService.getUserLogin({
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            var data = reply.data;
		            console.info("login" + data);
		            if (data == 'L1' || data == 'L2' || data == 'L3' || data == 'L4') {
		                vm.allowChange = false;
		            }
		            else { vm.allowChange = true; }
		        } else {
		            UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        UIControlService.msg_growl("error", "Gagal akses API");
		        UIControlService.unloadLoading();
		    });
		}
        */

		vm.addMethod = addMethod;
		function addMethod() {
		    editMethod(0);
		}

		vm.editMethod = editMethod;
		function editMethod(id) {
			var post = id;
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/prakualifikasi/metode-prakualifikasi/metodePrakualifikasiForm.html',
				controller: editMethodCtrl,
				controllerAs: 'editMethodCtrl',
				resolve: { item: function () { return post; } }
			});

			modalInstance.result.then(function () {
				loadMethods();
			});
		}

		vm.remove = remove;
		function remove(method) {
		    bootbox.confirm($filter('translate')('CONF_DELETE') + '<br/>' + method.PrequalMethodName, function (yes) {
		        if (yes) {
		            UIControlService.loadLoading('LOADING');
		            MetodePrakualifikasiService.remove({
		                PrequalMethodId: method.PrequalMethodId
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("notice", "SUCC_REMOVE");
		                loadMethods();
		            }, function (err) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "ERR_REMOVE");
		            });
		        }
		    });
		}

        /*
		vm.activate = activate;
		function activate(id) {
			var post = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'activate.html',
				controller: activateCtrl,
				controllerAs: 'activateCtrl',
				resolve: { item: function () { return post; } }
			});

			modalInstance.result.then(function (data) {
			    if (data == 1) window.location.reload();
			});
		}
        */

		vm.viewDetail = viewDetail;
		function viewDetail(id) {
			var post = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'viewMethodDetail.html',
				controller: methodDetailCtrl,
				controllerAs: 'methodDetailCtrl',
				resolve: { item: function () { return post; } }
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			loadMethods();
		}
	}
})();

var editMethodCtrl = function (item, $uibModalInstance, MetodePrakualifikasiService, UIControlService) {
	var vm = this;

	vm.steps = [];
	vm.formTypes = {};
	vm.prequalSteps = {};
	vm.selectedFormType = '';
	vm.selectedStep = 0;
	vm.goodsOrService = '';
	vm.methodName = '';
	vm.lockGoodsService = true;

	vm.init = init;
	function init() {
	    
	    MetodePrakualifikasiService.getformtypes(function (reply) {
	        vm.formTypes = reply.data;
	    }, function (err) {
	        UIControlService.msg_growl("error", "ERR_LOAD_FORM_TYPES");
	    });

	    if (item > 0) {
	        UIControlService.loadLoadingModal("LOADING");
	        MetodePrakualifikasiService.getbyid({
	            PrequalMethodId: item
	        }, function (reply) {
	            UIControlService.unloadLoadingModal();
	            vm.steps = reply.data.PrequalMethodDetails;
	            vm.methodName = reply.data.PrequalMethodName;
	        }, function (err) {
	            UIControlService.unloadLoadingModal();
	            UIControlService.msg_growl("error", "ERR_LOAD_DETAIL");
	        });
	    }
	}

	vm.changeFormType = changeFormType;
	function changeFormType() {
	    vm.selectedStep = null;
	    UIControlService.loadLoadingModal("LOADING");
	    MetodePrakualifikasiService.stepsbyformtype({
	        FormTypeID: vm.selectedFormType
	    }, function (reply) {
	        UIControlService.unloadLoadingModal();
	        vm.prequalSteps = reply.data;
	    }, function (err) {
	        UIControlService.unloadLoadingModal();
	        UIControlService.msg_growl("error", "ERR_LOAD_STEPS");
	    });
	}

	vm.addStep = addStep;
	function addStep() {
	    if (!vm.selectedStep) {
	        UIControlService.msg_growl("error", "ERR_NO_SELECTED_STEP");
	        return;
	    }

	    for (var i = 0; i < vm.steps.length; i++) {
	        if (vm.steps[i].PrequalStepID == vm.selectedStep.PrequalStepID) {
	            UIControlService.msg_growl("error", "ERR_SELECTED_STEP_EXIST");
	            return;
	        }
	    }

	    vm.steps.push({
	        PrequalStepID: vm.selectedStep.PrequalStepID,
	        PrequalStepName: vm.selectedStep.PrequalStepName,
	    });
	}

	vm.moveUp = moveUp;
	function moveUp(indexNo) {
		var temp1 = vm.steps[indexNo];
		var temp2 = vm.steps[indexNo - 1];

		vm.steps[indexNo] = temp2;
		vm.steps[indexNo - 1] = temp1;
	}

	vm.moveDown = moveDown;
	function moveDown(indexNo) {
		var temp1 = vm.steps[indexNo];
		var temp2 = vm.steps[indexNo + 1];

		vm.steps[indexNo] = temp2;
		vm.steps[indexNo + 1] = temp1;
	}

	vm.removeStep = removeStep;
	function removeStep(indexNo) {
		vm.steps.splice(indexNo, 1);
	}

	vm.saveMethod = saveMethod;
	function saveMethod() {
	    if (!vm.methodName) {
	        UIControlService.msg_growl("error", "ERR_NO_NAME");
			return;
		}

		if (vm.steps.length === 0) {
		    UIControlService.msg_growl("error", "ERR_NO_STEPS");
		    return;
		}

		var sequence = 1;
		vm.steps.forEach(function (step) {
		    step.Sequence = sequence++;
		});

		var savePrequalMethod = item > 0 ? MetodePrakualifikasiService.update : MetodePrakualifikasiService.insert;

		UIControlService.loadLoadingModal("LOADING");
		savePrequalMethod({
			PrequalMethodId: item,
			PrequalMethodName: vm.methodName,
			PrequalMethodDetails: vm.steps
		}, function (reply) {
		    UIControlService.msg_growl("notice", "SUCC_SAVE");
		    $uibModalInstance.close();
		}, function (err) {
		    UIControlService.unloadLoadingModal();
		    UIControlService.msg_growl("error", "ERR_SAVE");
		});
	}

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.dismiss('cancel');
	}
};

var methodDetailCtrl = function (item, $uibModalInstance, MetodePrakualifikasiService, UIControlService) {
	var vm = this;

	vm.methodName = '';
	vm.stages = [];

	MetodePrakualifikasiService.getbyid({
		PrequalMethodId: item
	}, function (reply) {
        vm.methodName = reply.data.PrequalMethodName;
		vm.stages = reply.data.PrequalMethodDetails;
	}, function (err) {
	    UIControlService.msg_growl("error", "ERR_LOAD_DETAIL");
	});

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.close();
	}
}