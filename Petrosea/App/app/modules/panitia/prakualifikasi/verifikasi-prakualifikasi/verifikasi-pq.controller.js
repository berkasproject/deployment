(function () {
	'use strict';

	angular.module("app").controller("PrequalVerificationCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'PrequalVerificationService', 'UIControlService', '$stateParams'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, PrequalVerificationService, UIControlService, $stateParams) {

		var vm = this;
		vm.init = init;

		vm.verifikasi = [];
		vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.PrequalSetupID = Number($stateParams.PrequalSetupID);

		vm.jenistender = null;
		vm.durasi = null;

		vm.maxSize = 10;

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    //console.info("setupID:" + vm.PrequalSetupID + "stepID:" + vm.PrequalStepID);
		    dataverifikasi();
		}


		function dataverifikasi() {
		    PrequalVerificationService.dataverifikasi({
                //column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.dataverifikasi = reply.data;
		            
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		vm.verificationdetail = function verificationdetail(param) {
		    $state.transitionTo('detail-verifikasi-prakualifikasi', {PrequalVerificationID: param.ID, VendorEntryPrequalID: param.VendorEntryPrequalID, VendorPrequalID:param.VendorPrequalID});
		}

	}
})();
