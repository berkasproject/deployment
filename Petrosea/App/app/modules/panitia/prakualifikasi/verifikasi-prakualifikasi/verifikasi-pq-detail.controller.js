(function () {
	'use strict';

	angular.module("app").controller("PrequalVerificationDetailCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'PrequalVerificationService', 'UIControlService', '$stateParams', 'GlobalConstantService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, PrequalVerificationService, UIControlService, $stateParams, GlobalConstantService) {

		var vm = this;
		vm.init = init;

		vm.verifikasi = [];
		vm.VendorEntryPrequalID = Number($stateParams.VendorEntryPrequalID);
		vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
		vm.PrequalVerificationID = Number($stateParams.PrequalVerificationID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";


		vm.maxSize = 10;

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    //console.info("setupID:" + vm.PrequalSetupID + "stepID:" + vm.PrequalStepID);
		    detailverifikasi();
		}


		function detailverifikasi() {
		    PrequalVerificationService.detaildataverifikasi({
                //column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.detailverifikasi = reply.data;
		            //console.info("detail:" + JSON.stringify(vm.detailverifikasi));
		            vm.CompanyCountryName = "";
		            vm.Provinsi = "";
		            vm.Kota = "";
		            vm.Kecamatan = "";
		            vm.website = "";
		            vm.phone = "";
		            vm.fax = "";
		            vm.email = "";
		            vm.alamatkantor = "";
		            vm.alamatkantor2 = "";
		            vm.alternatifkantor = "";
		            vm.alternatifkantor2 = "";

		            //Administrasi
		            for (var i = 0; i <= vm.detailverifikasi.Administration[0].VendorContact.length - 1; i++) {
		                if (vm.detailverifikasi.Administration[0].VendorContact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
		                    vm.CompanyCountryName = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.State.Country.Name;
		                    vm.Provinsi = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.AddressDetail;
		                    vm.Kota = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.City.Name;
		                    vm.Kecamatan = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.Distric.Name;
		                    vm.website = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Website;
		                    vm.phone = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Phone;
		                    vm.fax = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Fax;
		                    vm.email = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Email;
		                }
		                else if (vm.detailverifikasi.Administration[0].VendorContact[i].VendorContactType.Name == "VENDOR_OFFICE_TYPE_MAIN" && vm.detailverifikasi.Administration[0].VendorContact[i].IsPrimary==null) {
		                    vm.alamatkantor = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.AddressInfo;
		                    vm.alamatkantor2 = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.AddressDetail + ", " + vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.State.Country.Name;
		                }
		                else if (vm.detailverifikasi.Administration[0].VendorContact[i].VendorContactType.Name == "VENDOR_OFFICE_TYPE_MAIN" && vm.detailverifikasi.Administration[0].VendorContact[i].IsPrimary != null) {
		                    vm.alternatifkantor = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.AddressInfo;
		                    vm.alternatifkantor2 = vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.AddressDetail + ", " + vm.detailverifikasi.Administration[0].VendorContact[i].Contact.Address.State.Country.Name;
		                }
		            }


		            vm.asset = 0;
		            vm.hutang = 0;
		            vm.modal = 0;
		            //Neraca
		            for (var i = 0; i < vm.detailverifikasi.Balance.length; i++) {
		                if (vm.detailverifikasi.Balance[i].WealthType.Name == "WEALTH_TYPE_ASSET") {
		                    vm.listAsset = vm.detailverifikasi.Balance[i];
		                }
		                if (vm.detailverifikasi.Balance[i].WealthType.Name == "WEALTH_TYPE_DEBTH") {
		                    vm.listDebth = vm.detailverifikasi.Balance[i];
		                }
		            }
		            for (var i = 0; i < vm.detailverifikasi.Balance.length; i++) {
		                for (var j = 0; j < vm.detailverifikasi.Balance[i].subWealth.length; j++) {
		                    if (vm.detailverifikasi.Balance[i].subWealth[j].subCategory.length === 0) {
		                        if (vm.detailverifikasi.Balance[i].WealthType.RefID === 3097 && vm.detailverifikasi.Balance[i].subWealth[j].IsActive === true) {
		                            if (vm.asset === 0) {
		                                vm.asset = vm.detailverifikasi.Balance[i].subWealth[j].nominal;
		                                console.info(vm.asset);
		                            }
		                            else
		                                vm.asset = vm.asset +vm.detailverifikasi.Balance[i].subWealth[j].nominal;
		                                console.info(vm.asset);

		                        }
		                        else if (vm.detailverifikasi.Balance[i].WealthType.RefID === 3099 && vm.detailverifikasi.Balance[i].subWealth[j].IsActive === true) {
		                            if (vm.hutang === 0) {
		                                vm.hutang = vm.detailverifikasi.Balance[i].subWealth[j].nominal;
		                                console.info(vm.hutang);
		                            }
		                            else {
		                                vm.hutang = vm.hutang +vm.detailverifikasi.Balance[i].subWealth[j].nominal;
		                                console.info(vm.hutang);
		                            }


		                        }
		                    }
		                    else {
		                        for (var k = 0; k < vm.detailverifikasi.Balance[i].subWealth[j].subCategory.length; k++) {
		                            console.info(vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k]);
		                            if (vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].Wealth.RefID === 3097 && vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].IsActive === true) {
		                                if (vm.asset === 0) {
		                                    vm.asset = vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].Nominal;
		                                    console.info(vm.asset);
		                                }
		                                else
		                                    vm.asset = vm.asset + vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].Nominal;
		                                console.info(vm.asset);

		                            }
		                            else if (vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].WealthType === 3099 && vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].IsActive === true) {
		                                if (vm.hutang === 0) {
		                                    vm.hutang = vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].Nominal;
		                                    console.info(vm.hutang);
		                                }
		                                else {
		                                    vm.hutang = vm.hutang +vm.detailverifikasi.Balance[i].subWealth[j].subCategory[k].Nominal;
		                                    console.info(vm.hutang);
		                                }


		                            }
		                        }
		                    }

		                }
		            }
		            vm.modal = +vm.asset - +vm.hutang;


		            vm.akta = [];
		            vm.terakhir = [];
		            vm.kemenkumham = [];
		            //Akta
		            for (var i = 0; i <= vm.detailverifikasi.LegalDoc.length - 1; i++) {
		                if (vm.detailverifikasi.LegalDoc[i].DocumentType==3116) {
		                    vm.terakhir.push(vm.detailverifikasi.LegalDoc[i]);
		                }
		                else if (vm.detailverifikasi.LegalDoc[i].DocumentType == 3115) {
		                    vm.akta.push(vm.detailverifikasi.LegalDoc[i]);
		                }
		                else if (vm.detailverifikasi.LegalDoc[i].DocumentType == 3117) {
		                    vm.kemenkumham.push(vm.detailverifikasi.LegalDoc[i]);
		                }
		            }
		            console.info("kemenkumham" + JSON.stringify(vm.kemenkumham));

		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}
        vm.verificationdetail = function verificationdetail(param) {
		    $state.transitionTo('detail-verifikasi-prakualifikasi', {PrequalVerificationID: param.ID, VendorEntryPrequalID: param.VendorEntryPrequalID, VendorPrequalID:param.VendorPrequalID});
		}

	}
})();
