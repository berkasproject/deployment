﻿(function () {
    'use strict';

    angular.module("app")
    .controller("MigrasiPrakualCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'MigrasiPrakualService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, MigrasiPrakualService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams) {

        var vm = this;

        vm.PrequalStepID = Number($stateParams.PrequalStepID);
        vm.PrequalSetupID = Number($stateParams.PrequalSetupID);

        vm.pageSize = 10;
        vm.migrasi = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');
            UIControlService.loadLoading("");
            vm.flagcheckAll = false;
            vm.arrayCheckMigrasi = [];
            loadStep();
            cekProsesMigrasi();
            loadMigrasi(1);
        }
        vm.loadStep = loadStep;
        function loadStep() {
            MigrasiPrakualService.GetStepData({
                Status: vm.PrequalStepID
            }, function (reply) {
                vm.step = reply.data;
                vm.isEntry = reply.data.IsNotStarted;
                vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
            }, function (err) {

            });
        }

        vm.loadMigrasi = loadMigrasi;
        function loadMigrasi(current) {
            vm.currentPage = current;
            UIControlService.loadLoading("");
            MigrasiPrakualService.GetDataMigrasi({
                Status: vm.PrequalStepID,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize
            }, function (reply) {
                vm.migrasi = reply.data.List;
                vm.totalItems = Number(reply.data.Count);
                vm.migrasi.forEach(function (migrasi) {
                    if (!(migrasi.StatusReff.Name == "MIGRASI_DRAFT" || migrasi.StatusReff.Name == "MIGRASI_REJECT")) migrasi.IsCheck = true;
                    vm.arrayCheckMigrasi.forEach(function (listArrayCheck) {
                        if (listArrayCheck.ID == migrasi.ID)
                            migrasi.IsCheck = true;
                    });
                });
                UIControlService.unloadLoading();
            }, function (err) {
                UIControlService.unloadLoading("");
            });
        }

        vm.loadMigrasiAll = loadMigrasiAll;
        function loadMigrasiAll() {
            UIControlService.loadLoading("");
            MigrasiPrakualService.GetDataMigrasi({
                Status: vm.PrequalStepID,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: 0
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    for (var i = 0; i < data.List.length; i++) {
                        if (data.List[i].ID == 0 || data.List[i].StatusReff.Name == "MIGRASI_REJECT") vm.arrayCheckMigrasi.push(data.List[i]);
                        if (i == (Number(data.List.length) - 1)) {
                            loadMigrasi(vm.currentPage);
                        }
                    }


                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_DATA');
            });
                        
        };

        vm.Checked = Checked;
        function Checked(data) {
            if (data.IsCheck == true) vm.arrayCheckMigrasi.push(data);
            else {
                for (var i = 0; i < vm.arrayCheckMigrasi.length; i++) {
                    if (vm.arrayCheckMigrasi[i].VendorPrequalId == data.VendorPrequalId) vm.arrayCheckMigrasi.splice(i, 1);
                    break;
                }
            }
        }

        vm.CheckedAll = CheckedAll;
        function CheckedAll() {
            if (vm.IsCheck == true) loadMigrasiAll(1);
            else {
                vm.arrayCheckMigrasi = [];
                loadMigrasi(vm.currentPage);
            }
        }

        vm.sentApproval = sentApproval;
        function sentApproval() {
            if (vm.arrayCheckMigrasi.length == 0) {
                UIControlService.msg_growl('error', "MESSAGE.NO_VENDOR_SELECTED");
                return;
            }
            else {
                MigrasiPrakualService.sentApproval(vm.arrayCheckMigrasi, function (reply) {
                    if (reply.status == 200) {
                        UIControlService.msg_growl('success', "MESSAGE.SUCC_SEND_APPROVAL");
                        sendMail(reply.data);
                        init();
                    }
                }, function (err) {
                });
            }
        }

        vm.detailApproval = detailApproval;
        function detailApproval(data) {
            var item = data;
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/migrasi-prakualifikasi/detailApproval.modal.html',
                controller: 'detailApprovalCtrl',
                controllerAs: 'detailApprovalCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.cekProsesMigrasi = cekProsesMigrasi;
        function cekProsesMigrasi(ID) {
            UIControlService.loadLoading("Memeriksa Proses Migrasi");
            MigrasiPrakualService.cekProsesMigrasi({
                Status: vm.PrequalSetupID,
                FilterType: vm.PrequalStepID
            }, function (reply) {
                UIControlService.loadLoading("Loading Proses Migrasi");
                if (reply.status === 200) {
                    UIControlService.msg_growl('success', "Migrasi Data Vendor Selesai");
                    init();
                }
            }, function (err) {

            });
        }

        vm.sendMail = sendMail;
        function sendMail(ID) {
            MigrasiPrakualService.sendMail({
                Status: ID,
                FilterType: vm.PrequalStepID
            }, function (reply) {
                UIControlService.msg_growl('success', "MESSAGE.EMAIL_SENT");
            }, function (err) {

            });
        }

        vm.detailKelengkapan = detailKelengkapan;
        function detailKelengkapan(data) {
            var item = data;
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/migrasi-prakualifikasi/detailKelengkapanVendor.modal.html',
                controller: 'detailKelengkapanCtrl',
                controllerAs: 'detailKelengkapanCtrl',
                resolve: { item: function () { return item; } }
            });
        };


    }
})();;