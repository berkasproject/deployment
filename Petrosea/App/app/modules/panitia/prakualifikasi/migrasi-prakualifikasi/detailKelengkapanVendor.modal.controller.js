(function () {
    'use strict';

    angular.module("app")
    .controller("detailKelengkapanCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'MigrasiPrakualService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, MigrasiPrakualService) {

        var vm = this;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');
            $translatePartialLoader.addPart('permintaan-ubah-data');
            loadDetail();
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.loadDetail = loadDetail;
        function loadDetail() {
            MigrasiPrakualService.GetDetailAss({ Status: item.VendorPrequalId, FilterType: item.PrequalSetupStepId }, function (reply) {
                if (reply.status == 200) {
                    vm.list = reply.data;
                }
            }, function (err) {
            });
        }
    }
})();