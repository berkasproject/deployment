﻿(function () {
  'use strict';

  angular.module("app")
  .controller("formPengumumanPQCtrl", ctrl);

  ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
      '$state', 'UIControlService', 'item', '$uibModalInstance',
      'UploadFileConfigService', 'UploaderService', '$uibModal', 'GlobalConstantService', 'PrequalResultAnnouncementService'];
  function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService,
      $state, UIControlService, item, $uibModalInstance, UploadFileConfigService,
      UploaderService, $uibModal, GlobalConstantService, PrequalResultAnnouncementService) {

    var vm = this;
    vm.data = item;
    vm.init = init;
    function init() {
      console.info("item:" + JSON.stringify(item));
      vm.act = item.Act;
      vm.update = item.update;
      if (item.data != null) {
        console.info(item.data);
        vm.Description = item.data.Description;
        vm.ForPassedVendor = item.data.ForPassedVendor;
        vm.ForFailedVendor = item.data.ForFailedVendor;
        vm.IsPublish = item.data.IsPublish;
      }
      loadAnnouncement();
    }

    function loadAnnouncement() {
      PrequalResultAnnouncementService.announc({
        PrequalSetupStepID: item.PrequalSetupStep.PrequalStepID
      }, function (reply) {
        UIControlService.unloadLoadingModal();
        if (reply.status === 200) {
          vm.announc = reply.data;

        }
      }, function (err) {
        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
        UIControlService.unloadLoadingModal();
      });
    };

    function generateFilterStrings(allowedTypes) {
      var filetypes = "";
      for (var i = 0; i < allowedTypes.length; i++) {
        filetypes += "." + allowedTypes[i].Name + ",";
      }
      return filetypes.substring(0, filetypes.length - 1);
    }

    vm.uploadFile = uploadFile;
    function uploadFile() {
      upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
    };

    function validateFileType(file, allowedFileTypes) {
      if (!file || file.length == 0) {
        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
        return false;
      }
      return true;
    }


    vm.upload = upload;
    function upload(file, config, filters, callback) {
      vm.prefix = "Pengumuman Hasil Prakualifikasi_" + vm.data.PrequalSetupStep.PrequalStepID;
      var size = config.Size;
      var unit = config.SizeUnitName;

      if (unit == 'SIZE_UNIT_KB') {
        size *= 1024;
        vm.flag = 0;
      }

      if (unit == 'SIZE_UNIT_MB') {
        size *= (1024 * 1024);
        vm.flag = 1;
      }

      UploaderService.uploadSinglePrequalResultAnnc(vm.SetupStepID, vm.prefix, file, size, filters,
          function (response) {
            UIControlService.unloadLoading();
            if (response.status == 200) {
              var url = response.data.Url;
              vm.pathFile = url;
              vm.name = response.data.FileName;
              var s = response.data.FileLength;

              //update();
            } else {
              UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
              return;
            }
          },
          function (response) {
            UIControlService.msg_growl("error", "MESSAGE.API")
            UIControlService.unloadLoading();
          });
    }

    vm.openDataCommVendor = openDataCommVendor;
    function openDataCommVendor() {
      var data = {
        IsLocal: vm.IsLocal,
        IsNational: vm.IsNational,
        IsInternational: vm.IsInternational,
      };
      data.getData = vm.viewvendor;
      data.IsPublish = vm.IsPublish;
      data.ID = vm.ID;

      var modalInstance = $uibModal.open({
        templateUrl: 'app/modules/panitia/prakualifikasi/pengumuman-prakualifikasi/viewVendor.html',
        controller: 'ViewVendorCtrl',
        controllerAs: 'ViewVendorCtrl',
        resolve: {
          item: function () {
            return data;
          }
        }
      });
      modalInstance.result.then(function (dataVendor) {
        vm.viewvendor = dataVendor;
      });
    }

    vm.saveAnnouncement = function () {
      console.info(vm.Description);

      if (vm.ForPassedVendor == true || vm.ForFailedVendor == true) {
        PrequalResultAnnouncementService.createAnnounc({
          PrequalSetupStepID: item.PrequalSetupStep.ID,
          Description: vm.Description,
          ForPassedVendor: vm.ForPassedVendor,
          ForFailedVendor: vm.ForFailedVendor,
          DocUrl: ""
        }, function (reply) {
          UIControlService.unloadLoadingModal();
          UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_INSERT_DATA'));
          $uibModalInstance.close();
        }, function (err) {
          UIControlService.unloadLoadingModal();
        });

      }
      else {
        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.SELECT_TEMPLATE'));
      }



    }

    vm.updateAnnouncement = function () {
      PrequalResultAnnouncementService.updateAnnounc({
        ID: item.data.ID,
        Description: vm.Description,
        ForPassedVendor: vm.ForPassedVendor,
        ForFailedVendor: vm.ForFailedVendor,
        DocUrl: ""
      }, function (reply) {
        UIControlService.unloadLoadingModal();
        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_INSERT_DATA'));
        $uibModalInstance.close();
      }, function (err) {
        UIControlService.unloadLoadingModal();
      });
    }

    vm.saveData = saveData;
    function saveData() {
      if (vm.act == true && item.data == null) {
        PrequalResultAnnouncementService.createAnnounc({
          PrequalSetupStepID: item.PrequalSetupStep.ID,
          Description: vm.Description,
          ForPassedVendor: vm.ForPassedVendor,
          ForFailedVendor: vm.ForFailedVendor,
          DocUrl: ""
        }, function (reply) {
          UIControlService.unloadLoadingModal();
          UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_INSERT_DATA'));
          $uibModalInstance.close();
        }, function (err) {
          UIControlService.unloadLoadingModal();
        });
      }
      else if (vm.act == true && item.data != null) {
        PrequalResultAnnouncementService.updateAnnounc({
          ID: item.data.ID,
          Description: vm.Description,
          ForPassedVendor: vm.ForPassedVendor,
          ForFailedVendor: vm.ForFailedVendor,
          DocUrl: ""
        }, function (reply) {
          UIControlService.unloadLoadingModal();
          UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_INSERT_DATA'));
          $uibModalInstance.close();
        }, function (err) {
          UIControlService.unloadLoadingModal();
        });
      }
    }

    vm.batal = batal;
    function batal() {
      $uibModalInstance.dismiss('cancel');
    };
  }
})();