﻿(function () {
    'use strict';

    angular.module("app").controller("ViewVendorCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', 'UIControlService', '$uibModalInstance', 'AturPrakualService', 'item'];

    function ctrl($translatePartialLoader, UIControlService, $uibModalInstance, AturPrakualService, item) {
        var vm = this;

        vm.vendors = null;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.keyword = '';
        vm.ID = item.ID;
        vm.ubahArea = item.ubahArea;
        vm.IsPublish = item.IsPublish;
        vm.IsLocal = item.IsLocal;
        vm.IsNational = item.IsNational;
        vm.IsInternational = item.IsInternational;
        vm.selectedVendor = [];
        vm.getData = item.getData;
        vm.init = init;
        vm.contactVendor = [];

        
        vm.bf = 0;
        vm.cs = 0;
        vm.tr = 0;
        if (item.BusinessField != 0) {
            vm.bf = item.BusinessField.ID;
        }

        if (item.TechnicalRefID != 0) {
            vm.tr = item.TechnicalRefID.RefID;
        }

        if (item.CompanyScale != 0) {
            vm.cs = item.CompanyScale.RefID;
        }
        console.info("bf" + vm.bf);
        console.info("tr" + vm.tr);
        console.info("cs" + vm.cs);
        function init() {
            console.info("ubaharea:" + JSON.stringify(vm.ubahArea));
            console.info("ischeckall?" + vm.IsCheckAll);
            if (vm.IsPublish != true) {
                if (vm.getData.length == 0) {
                    //if (vm.IsLocal == true) SaveSelectedLocal();
                    //else showVendor(1);
                    showVendor(1);
                }
                else {
                    vm.contactVendor = vm.getData;
                    if (vm.ubahArea == false) {
                        if (vm.getData.length > 0) {
                            console.info("length" + vm.getData.length);
                            vm.selectedVendor = vm.getData;
                            if (item.ID != 0) {
                                if (vm.IsPublish == true) {
                                    getVendor();
                                }
                                else {
                                    showVendor(1);
                                }
                            }
                            else {
                                vm.vendors = vm.selectedVendor;
                                showVendor(1);
                            }
                        }
                        else {
                            console.info("sini");
                            UIControlService.loadLoadingModal("LOADING.LOAD");
                            AturPrakualService.viewVendor({
                                IsLokal: false,
                                IsNasional: false,
                                IsInternasional: false,
                                Keyword: vm.keyword,
                                contactVendor: vm.contactVendor,
                                BusinessFieldID: vm.bf,
                                CompanyScale: vm.cs,
                                TechnicalRefID: vm.tr
                            }, function (reply) {
                                if (reply.status === 200) {
                                    vm.selectedVendor = reply.data.List;
                                }
                            }, function (err) {
                                UIControlService.unloadLoadingModal();
                            });
                            showVendor(1);
                        }
                    }
                    else {
                        console.info("hmm");
                            UIControlService.loadLoadingModal("LOADING.LOAD");
                            AturPrakualService.viewVendor({
                            IsLokal: false,
                            IsNasional: false,
                            IsInternasional: false,
                            Keyword: vm.keyword,
                            contactVendor: vm.contactVendor,
                            BusinessFieldID: vm.bf,
                            CompanyScale: vm.cs,
                            TechnicalRefID: vm.tr
                            }, function (reply) {
                            if (reply.status === 200) {
                                vm.selectedVendor = reply.data.List;
                                }
                                }, function (err) {
                            UIControlService.unloadLoadingModal();
                                });
                            showVendor(1);
                    }
                }
            }
            else {
                getVendor(1);
            }
            
        }

        vm.getVendor = getVendor;
        function getVendor(current) {
            UIControlService.loadLoadingModal("LOADING.LOAD");
            vm.currentPage = current;
            AturPrakualService.getVendor({
                ID: item.ID,
                Keyword: vm.keyword,
                Offset: vm.maxSize * (vm.currentPage - 1),
                Limit: vm.maxSize
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vendors = reply.data.List;
                    vm.totalItems = Number(reply.data.Count);
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.SaveSelectedLocal = SaveSelectedLocal;
        function SaveSelectedLocal() {
            UIControlService.loadLoadingModal("LOADING.LOAD");
            if (vm.ID == 0) {
                var id = 0;
            }
            else {
                var id = vm.ID;
            }
            AturPrakualService.viewVendor({
                IsLokal: true,
                IsNasional: false,
                IsInternasional: false,
                ID :0,
                Keyword: vm.keyword,
                contactVendor: vm.contactVendor,
                BusinessFieldID: vm.bf,
                CompanyScale: vm.cs,
                TechnicalRefID: vm.tr
            }, function (reply) {
                if (reply.status === 200) {
                    //vm.selectedVendor = reply.data.List;
                    showVendor(1);
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.showVendor = showVendor;
        function showVendor(current) {
            UIControlService.loadLoadingModal("LOADING.LOAD");
            vm.currentPage = current;
            AturPrakualService.viewVendor({
                IsLokal: item.IsLocal,
                IsNasional: item.IsNational,
                IsInternasional: item.IsInternational,
                ID: vm.ID,
                Keyword: vm.keyword,
                contactVendor: vm.contactVendor,
                BusinessFieldID: vm.bf,
                CompanyScale: vm.cs,
                TechnicalRefID:vm.tr
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vendors = reply.data.List;
                    console.info("vendors" + JSON.stringify(vm.vendors));
                    vm.totalItems = Number(reply.data.Count);

                    if (vm.IsCheckAll == false || (vm.ubahArea==true&&vm.IsCheckAll==undefined)) {
                        vm.vendors.forEach(function (datavendor) {
                            datavendor.IsCheck = false;
                        })
                    }
                    else if (vm.IsCheckAll == true) {
                        vm.vendors.forEach(function (datavendor) {
                            datavendor.IsCheck = true;
                        })
                    }


                    if (vm.selectedVendor.length == vm.vendors.length) {
                        vm.IsCheckAll = true;
                    }

                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.save = save;
        function save() {
            $uibModalInstance.close(vm.selectedVendor);

        }

        vm.checkAll = checkAll;
        function checkAll() {
            if (vm.IsCheckAll == true) {
                UIControlService.loadLoadingModal("LOADING.LOAD");
                AturPrakualService.viewVendor({
                    IsLokal: vm.IsLocal,
                    IsNasional: vm.IsNational,
                    IsInternasional: vm.IsInternational,
                    Keyword: vm.keyword,
                    contactVendor: vm.contactVendor,
                    BusinessFieldID: vm.bf,
                    CompanyScale: vm.cs,
                    TechnicalRefID: vm.tr
                }, function (reply) {
                    if (reply.status === 200) {
                        vm.selectedVendor = reply.data.List;
                        console.info("selected vendor checkall:" + reply.data.Count);
                        vm.vendors.forEach(function (datavendor) {
                            datavendor.IsCheck = true;
                        })
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
            else {
                vm.selectedVendor = [];
                //if (vm.IsLocal == true) {
                    UIControlService.loadLoadingModal("LOADING.LOAD");
                    AturPrakualService.viewVendor({
                        IsLokal: false,
                        IsNasional: false,
                        IsInternasional: false,
                        Keyword: vm.keyword,
                        contactVendor: vm.contactVendor,
                        BusinessFieldID: vm.bf,
                            CompanyScale: vm.cs,
                                TechnicalRefID : vm.tr
                    }, function (reply) {
                        if (reply.status === 200) {
                            vm.selectedVendor = reply.data.List;
                            showVendor(1);
                        }
                    }, function (err) {
                        UIControlService.unloadLoadingModal();
                    });
                //}
            }
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.selectvendor = selectvendor;
        function selectvendor(data) {
            if (data.IsCheck == true) {
                vm.selectedVendor.push(data);
            }
            else {
                for (var i = 0; i < vm.selectedVendor.length; i++) {
                    if (vm.selectedVendor[i].VendorID == data.VendorID && data.IsCheck == false) {
                        vm.selectedVendor.splice(i, 1);
                    }
                }

            }
        }
    }
})();