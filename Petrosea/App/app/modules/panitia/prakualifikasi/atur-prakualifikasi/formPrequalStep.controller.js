﻿(function () {
    'use strict';

    angular.module("app").controller('FormSetupCtrl', ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'UploaderService', '$translatePartialLoader', 'AturPrakualService', 'UIControlService', '$uibModal', 'UploadFileConfigService', 'GlobalConstantService'];
    function ctrl($state, $stateParams, UploaderService, $translatePartialLoader, AturPrakualService, UIControlService, $uibModal, UploadFileConfigService, GlobalConstantService) {
        var vm = this;
        vm.Id = Number($stateParams.Id);
        vm.prequalSteps = [];
        vm.isEdit = false;
        vm.allowEdit = true;
        vm.viewvendor = [];
        vm.init = init;
        vm.IsLocal = false;
        vm.IsNational = false;
        vm.IsInternational = false;
        vm.listKelengkapan = [];
        vm.setupName = "";
        vm.listClassification = [];
        vm.Commitees = [];
        vm.pathFile = null;
        vm.DocUrl = "";
        vm.IsPublish = false;
        vm.flagK3L = 0;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.ubahArea = false;

        function init() {
            $translatePartialLoader.addPart('prequal-setup');
            if (vm.Id != 0) {
                getPrequalById();
            }
            else {
                getPrequalMethod();
                getVPEvalMethod();
                getPrequalCode();
                loadDetailPrequal();
            }
            loadEmailContent();
            loadConfig();
            getTechnicalClassification();
            getCompanyScale();
            getBusinessField();
            
        }

        vm.getPrequalById = getPrequalById;
        function getPrequalById() {
            AturPrakualService.getPrequalById({ID : vm.Id},function (response) {
                if (response.status == 200) {
                    vm.dataPrequal = response.data;
                    //console.info("dataPQ:" + JSON.stringify(vm.dataPrequal));
                    vm.prequalCode = vm.dataPrequal.PrequalCode;
                    vm.setupName = vm.dataPrequal.Name;
                    vm.DocUrl = vm.dataPrequal.DocUrl == null ? "" : vm.dataPrequal.DocUrl;
                    vm.IsLocal = vm.dataPrequal.IsLocal;
                    vm.IsNational = vm.dataPrequal.IsNational;
                    vm.IsInternational = vm.dataPrequal.IsInternational;
                    vm.viewvendor = vm.dataPrequal.viewVendor;
                    vm.Commitees = vm.dataPrequal.Commitees;
                    vm.listKelengkapan = vm.dataPrequal.ListKelengkapan;
                    vm.IsOpen = vm.dataPrequal.IsOpen;
                    vm.IsPublish = vm.dataPrequal.IsPublish == null ? false : vm.dataPrequal.IsPublish;
                    vm.EmailContent = vm.dataPrequal.AnnouncementText;
                    getPrequalMethod();
                    getVPEvalMethod();
                    loadDetailPrequal();
                    getCompanyScale();
                    getTechnicalClassification();
                    getBusinessField();
                    cekarea();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.loadEmailContent = loadEmailContent;
        function loadEmailContent() {
            AturPrakualService.emailContent(function (response) {
                if (response.status == 200) {
                    vm.EmailContent = response.data;
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
            
        };

        vm.getCompanyScale = getCompanyScale;
        function getCompanyScale() {
            AturPrakualService.getCompanyScale(function (response) {
                if (response.status == 200) {
                    vm.listcompanyscale = response.data;
                    if (vm.Id != 0) {
                        for (var i = 0; i <= vm.listcompanyscale.length - 1; i++) {
                            if (vm.dataPrequal.CompanyScale == vm.listcompanyscale[i].RefID) {
                                vm.CompanyScale = vm.listcompanyscale[i];
                                i=vm.listcompanyscale.length;
                            }
                        }
                    }
                    //console.info("companyscale:" + JSON.stringify(vm.listcompanyscale));
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.listSelectedBusinessField = [];
        vm.pilihbidangusaha = false;
        vm.addBField = addBField;
        function addBField(data) {
            console.info("bfieldpilih:" + JSON.stringify(data));
            if (data != undefined) {
                vm.listSelectedBusinessField.push({
                    BusinessFieldID: data.ID
                });
                vm.pilihbidangusaha = true;
            }
            //console.info("listBField:" + JSON.stringify(vm.listSelectedBusinessField));
        }

        vm.deleteRow = deleteRow;
        function deleteRow(index) {
            var idx = index - 1;
            var _length = vm.listSelectedBusinessField.length; // panjangSemula
            vm.listSelectedBusinessField.splice(idx, 1);
        };

        vm.getBusinessField = getBusinessField;
        function getBusinessField() {
            AturPrakualService.getBusinessField(function (response) {
                if (response.status == 200) {
                    vm.listbfield = response.data;
                    console.info("bfield" + JSON.stringify(vm.listbfield));
                    /*
                    if (vm.Id == 0) {
                        for (var i = 0; i <= vm.listbfield.length - 1; i++) {
                            if (vm.listbfield[i].ID == 2054) {
                                vm.listbfield.splice(i, 1);
                            }
                        }
                    }*/
                    if (vm.Id != 0) {
                        for (var i = 0; i <= vm.listbfield.length - 1; i++) {
                            if (vm.dataPrequal.ListBusinessFields[0].BusinessFieldID == vm.listbfield[i].ID) {
                                vm.BusinessField = vm.listbfield[i];
                                addBField(vm.BusinessField);
                                i = vm.listbfield.length;
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };
        vm.getTechnicalClassification=getTechnicalClassification;
        function getTechnicalClassification() {
            AturPrakualService.getTechnicalClassification(function (response) {
                if (response.status == 200) {
                    vm.listtechnicalclassification = response.data;
                    if (vm.Id != 0) {
                        for (var i = 0; i <= vm.listtechnicalclassification.length - 1; i++) {
                            if (vm.dataPrequal.TechnicalRefID == vm.listtechnicalclassification[i].RefID) {
                                vm.TechnicalRefID = vm.listtechnicalclassification[i];
                                i=vm.listtechnicalclassification.length;
                            }
                        }
                    }
                   // console.info("tech Classf:" + JSON.stringify(vm.listtechnicalclassification));
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };
        vm.lihatvendor = false;
        vm.cekarea = cekarea;
        function cekarea() {
            //console.info("islocal:"+vm.IsLocal);
            if (vm.IsLocal == true || vm.IsNational == true || vm.IsInternational == true) {
                vm.lihatvendor = true;
            }
            else if (vm.IsLocal == false && vm.IsNational == false && vm.IsInternational == false) {
                vm.lihatvendor = false;
            }
            //console.info("lihatvendor:" + vm.lihatvendor);
        }

        vm.loadConfig = loadConfig;
        function loadConfig() {
            UploadFileConfigService.getByPageName("PAGE.ADMIN.PREQANNC", function (response) {
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
            });
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        function getPrequalCode() {
            AturPrakualService.getPrequalCode(function (reply) {
                UIControlService.loadLoadingModal('LOADING.PREQUALMETHOD');
                if (reply.status === 200) {
                    vm.prequalCode = reply.data;
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALCODE.ERROR', "NOTIFICATION.GET.PREQUALCODE.TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALCODE.ERROR', "NOTIFICATION.GET.PREQUALCODE.TITLE");
            });
        }

        function getPrequalMethod() {
            AturPrakualService.getPrequalMethod(function (reply) {
                UIControlService.loadLoadingModal('LOADING.PREQUALMETHOD');
                if (reply.status === 200) {
                    vm.prequalMethods = reply.data;
                    if (vm.Id != 0) {
                        vm.prequalMethods.forEach(function (data) {
                            if (data.PrequalMethodId == vm.dataPrequal.PrequalMethodID) {
                                vm.selectedPrequalMethod = data.PrequalMethodId;
                                change();
                            }
                        });
                    }
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
            });
        }

        function getVPEvalMethod() {
            AturPrakualService.getVPEvalMethod(function (reply) {
                UIControlService.loadLoadingModal('LOADING.VP_EVAL_METHOD');
                if (reply.status === 200) {
                    vm.vpEvalMethods = reply.data;
                    if (vm.Id != 0) {
                        vm.vpEvalMethods.forEach(function (list) {
                            if (list.VPEvaluationMethodId == vm.dataPrequal.VPEvalMethodID) {
                                vm.selectedVPEvalMethod = list.VPEvaluationMethodId;
                            }
                        });
                    }
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'NOTIFICATION.GET.VP_EVAL_METHOD.ERROR', "NOTIFICATION.GET.VP_EVAL_METHOD.TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'NOTIFICATION.GET.VP_EVAL_METHOD.ERROR', "NOTIFICATION.GET.VP_EVAL_METHOD.TITLE");
            });
        }

        vm.openDataCommVendor = openDataCommVendor;
        function openDataCommVendor() {
            if (vm.CompanyScale == undefined) {
                vm.CompanyScale = 0;
            }
            if (vm.TechnicalRefID == undefined) {
                vm.TechnicalRefID = 0;
            }
            if (vm.BusinessField == undefined) {
                vm.BusinessField = 0;
            }
            if (vm.Id != 0) {
                if (vm.IsLocal != vm.dataPrequal.IsLocal || vm.IsNational != vm.dataPrequal.IsNational || vm.IsInternational != vm.dataPrequal.IsInternational) {
                    vm.ubahArea = true;
                }
            }
            var data = {
                IsLocal: vm.IsLocal,
                IsNational: vm.IsNational,
                IsInternational: vm.IsInternational,
                BusinessField: vm.BusinessField,
                CompanyScale: vm.CompanyScale,
                TechnicalRefID:vm.TechnicalRefID
            };
            data.getData = vm.viewvendor;
            data.IsPublish = vm.IsPublish;
            data.ID = vm.Id;
            data.ubahArea = vm.ubahArea;

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/atur-prakualifikasi/viewVendor.html',
                controller: 'ViewVendorCtrl',
                controllerAs: 'ViewVendorCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (dataVendor) {
                vm.viewvendor = dataVendor;
            });
        }

        vm.selectedDetailPrequal = {};
        vm.listDetailPrequal = [];
        vm.loadDetailPrequal = loadDetailPrequal;
        function loadDetailPrequal() {
            AturPrakualService.loadDetailPrequal(function (response) {
                if (response.status == 200) {
                    vm.listDetailPrequal = response.data;
                    if (vm.Id != 0) {
                        vm.listDetailPrequal.forEach(function (list) {
                            vm.dataPrequal.ListClassification.forEach(function (data) {
                                if (data.ClassificationRefId == list.RefID) {
                                    list.IsCheck = true;
                                    checkClassification(list);
                                }
                            })
                        })
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.loadKelengkapan = loadKelengkapan;
        function loadKelengkapan() {
            var item = {
                listKelengkapan: vm.listKelengkapan,
                IsPublish: vm.IsPublish,
                listClassification: vm.listClassification
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/atur-prakualifikasi/detailKelengkapanPrequal.modal.html',
                controller: 'detKelPrequalCtrl',
                controllerAs: 'detKelPrequalCtrl',
                resolve: { item: function () { return item; } }
               
            });
            modalInstance.result.then(function (data) {
                vm.listKelengkapan = data;
            });
        }
        
        vm.change = change;
        function change() {
            vm.flagPemasukkan = false;
            AturPrakualService.getPrequalStep({ PrequalMethodID: vm.selectedPrequalMethod }, function (reply) {
                UIControlService.loadLoadingModal('LOADING.PREQUALMETHOD');
                if (reply.status === 200) {
                    vm.prequalSteps = reply.data;
                    vm.prequalSteps.forEach(function (data) {
                        if (data.FormTypeURL == "pemasukkan-prakualifikasi") {
                            vm.flagPemasukkan = true;
                        }
                    });
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
            });
        }

        vm.setReviewer = setReviewer;
        function setReviewer() {

            var item = {
                //item: data
                item: vm.Commitees,
                IsReadOnly: vm.IsPublish
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/atur-prakualifikasi/setPrequalReviewer.html',
                controller: 'CommitteeModalCtrl',
                controllerAs: 'CommitteeModalCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function (detail) {
                vm.Commitees = detail;
                //loadRFQVHS();
            });
        }

        vm.createPrequalMethod = createPrequalMethod;
        function createPrequalMethod() {
            if (vm.setupName.trim() == '' || vm.prequalSteps.length == 0 || vm.selectedPrequalMethod == null || vm.selectedPrequalMethod == 0 || vm.selectedVPEvalMethod == null || vm.selectedVPEvalMethod == 0) {
                return false;
            }

            if (vm.isEdit == true) {
                AturPrakualService.editPrequalMethod({
                    ID: vm.ID,
                    Name: vm.setupName,
                    PrequalMethodID: vm.selectedPrequalMethod,
                    VPEvalMethodID: vm.selectedVPEvalMethod,
                    Commitees: vm.Commitees
                }, function (reply) {
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_EDIT");
                    } else {
                    }
                }, function (err) {
                    $.growl.error({ message: "Gagal Akses API >" + err });
                    //$rootScope.unloadLoading();
                });
                return;
            }

            AturPrakualService.createPrequalMethod({
                PrequalCode: vm.prequalCode,
                Name: vm.setupName,
                PrequalMethodID: vm.selectedPrequalMethod,
                VPEvalMethodID: vm.selectedVPEvalMethod,
                Commitees: vm.Commitees
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_CREATE");
                } else {
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                //$rootScope.unloadLoading();
            });
        }


        vm.closeModal = closeModal;
        function closeModal() {
        };

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.setupName == "" || vm.setupName==null) {
                UIControlService.msg_growl("error", "MESSAGE.NO_SETUPNAME");
                return;
            }
            if(vm.Id == 0 && (vm.fileUpload == undefined || vm.fileUpload == null)){
                UIControlService.msg_growl("error", "MESSAGE.NO_DOC");
                return;
            }
            else if(vm.fileUpload){
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
            }
            else if(vm.Id !== 0 && (vm.fileUpload == undefined || vm.fileUpload == null)){
                prosesSimpan();
            }
        }

        vm.prosesSimpan = prosesSimpan;
        function prosesSimpan() {
            if (vm.IsLocal == false && vm.IsNational == false && vm.IsInternational == false) {
                UIControlService.msg_growl("error", "MESSAGE.NO_AREA");
                return;
            }
            console.info("classf" + JSON.stringify(vm.listClassification));
            if (vm.IsLocal == true && vm.viewvendor.length == 0) {
                AturPrakualService.viewVendor({
                    IsLokal: true,
                    IsNasional: false,
                    IsInternasional: false,
                    Keyword: '',
                    contactVendor: []
                }, function (reply) {
                    if (reply.status === 200) {
                        vm.viewvendor = reply.data.List;
                        /*
                        if (vm.listClassification.length == 0) {
                            UIControlService.msg_growl("error", "MESSAGE.NO_KLASIFIKASI");
                            return;
                        }*/
                        if (vm.selectedPrequalMethod == undefined) {
                            UIControlService.msg_growl("error", "MESSAGE.NO_METHOD");
                            return;
                        }
                        else if (vm.selectedVPEvalMethod == undefined) {
                            UIControlService.msg_growl("error", "MESSAGE.NO_EVALMETHOD");
                            return;
                        }
                        else if (vm.Commitees.length == 0 && vm.flagPemasukkan == true) {
                            UIControlService.msg_growl("error", "MESSAGE.NO_REVIEWER");
                            return;
                        }
                        else if (vm.flagPemasukkan == true && vm.listKelengkapan.length == 0) {
                            UIControlService.msg_growl("error", "MESSAGE.NO_COMPLETENESS");
                            return;
                        }
                        else save();
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.IsLocal == false && vm.viewvendor.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_VENDOR");
                return;
            }/*
            else if (vm.listClassification.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_KLASIFIKASI");
                return;
            }*/
            else if (vm.selectedPrequalMethod == undefined) {
                UIControlService.msg_growl("error", "MESSAGE.NO_METHOD");
                return;
            }
            else if (vm.selectedVPEvalMethod == undefined) {
                UIControlService.msg_growl("error", "MESSAGE.NO_EVALMETHOD");
                return;
            }
            else if (vm.Commitees.length == 0 && vm.flagPemasukkan == true) {
                UIControlService.msg_growl("error", "MESSAGE.NO_REVIEWER");
                return;
            }
            else if (vm.flagPemasukkan == true && vm.listKelengkapan.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_COMPLETENESS");
                return;
            }
            else save();
        }

        vm.save = save;
        function save() {
            if (vm.TechnicalRefID == undefined) {
                vm.TechnicalRefID = null;
            }
            else {
                vm.TechnicalRefID = vm.TechnicalRefID.RefID;
            }
            if (vm.CompanyScale == undefined) {
                vm.CompanyScale = null;
            }
            else {
                vm.CompanyScale = vm.CompanyScale.RefID;
            }
            //console.info("listbf" + JSON.stringify(vm.listSelectedBusinessField));
            if (vm.pathFile == undefined) {
                vm.pathFile = vm.DocUrl;
            }
            AturPrakualService.createPrequalMethod({
                ID: vm.Id,
                PrequalCode: vm.prequalCode,
                Name: vm.setupName,
                DocUrl: vm.pathFile,
                IsLocal: vm.IsLocal,
                IsNational: vm.IsNational,
                IsInternational: vm.IsInternational,
                viewVendor: vm.viewvendor,
                ListClassification: vm.listClassification,
                PrequalMethodID: vm.selectedPrequalMethod,
                VPEvalMethodID: vm.selectedVPEvalMethod,
                Commitees: vm.Commitees,
                ListKelengkapan: vm.listKelengkapan,
                IsOpen: vm.IsOpen,
                AnnouncementText: vm.EmailContent,
                CompanyScale: vm.CompanyScale,
                TechnicalRefID: vm.TechnicalRefID,
                ListBusinessFields: vm.listSelectedBusinessField
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_CREATE");
                    $state.go('prequal-setup');
                } else {
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                //$rootScope.unloadLoading();
            });
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            vm.prefix = "Prakualifikasi_" + vm.prequalCode;
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UploaderService.uploadSinglePrequalAnnc(vm.prequalCode, vm.prefix, file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = url;
                        console.info(vm.pathFile);
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        if (vm.flag == 0) vm.size = Math.floor(s);
                        else if (vm.flag == 1) vm.size = Math.floor(s / (1024));
                        prosesSimpan();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });
        }

        vm.checkClassification = checkClassification;
        function checkClassification(data) {
            if (data.IsCheck == true) {
                vm.listClassification.push({
                    ClassificationRefId: data.RefID
                });
            }
            else {
                for (var i = 0; i < vm.listClassification.length; i++) {
                    if (vm.listClassification[i].ClassificationRefId == data.RefID) {
                        vm.listClassification.splice(i, 1);
                    }
                }

            }
        }

        vm.listBusinessField = [];
        vm.selectedbfield = selectedbfield;
        function selectedbfield(data) {
            if (data!= undefined) {
                vm.listBusinessField.push({
                    BusinessFieldID: data.ID
                });
            }
                /*
            else {
                for (var i = 0; i < vm.listClassification.length; i++) {
                    if (vm.listClassification[i].ClassificationRefId == data.RefID) {
                        vm.listClassification.splice(i, 1);
                    }
                }

            }
            */
        }

        vm.back = back;
        function back() {
            $state.go('prequal-setup');
        }


    }
})();