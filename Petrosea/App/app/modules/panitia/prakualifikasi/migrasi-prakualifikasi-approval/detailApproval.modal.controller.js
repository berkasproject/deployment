(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'MigrasiPrakualService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, MigrasiPrakualService) {

        var vm = this;
        vm.Remark = "";
        vm.init = init;
        function init() {
            console.info(item);
            $translatePartialLoader.addPart('verifikasi-tender');
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reject = reject;
        function reject() {
            if (vm.Remark == "") {
                UIControlService.msg_growl('error', "MESSAGE.NO_REASON");
                return;
            }
            else {
                MigrasiPrakualService.reject({ Status : item.PrequalMigrasiModel.ID, Keyword: vm.Remark },
                    function (reply) {
                        if (reply.status === 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('success', 'MESSAGE.SUCC_REJECT');
                            $uibModalInstance.close();
                        }
                    },
                    function (err) {
                        // UIControlService.msg_growl("error", "Gagal Akses Api!!");
                    }
                );
            }
        }
    }
})();