﻿(function () {
    'use strict';
    
    angular.module("app").controller("AuctionModalCtrl", ctrl);

    ctrl.$inject = ['UIControlService', '$uibModalInstance', 'item', 'AuctionService', 'UploadFileConfigService', 'UploaderService', '$translatePartialLoader'];

    function ctrl(UIControlService, $uibModalInstance, item, AuctionService, UploadFileConfigService, UploaderService, $translatePartialLoader) {
        var vm = this;
        vm.init = init;
        vm.isDobCalendarOpened = false;
        vm.tenderStepID = item.TenderStepID;
        vm.vendorID = item.VendorID;
        vm.batasBawah = item.BatasBawah;
        vm.isAdd = item.act;

        console.info("item: " + JSON.stringify(item));

        function init() {
            UIControlService.loadLoadingModal('MESSAGE.LOADING');
            $translatePartialLoader.addPart('auction');
            if (vm.isAdd === false) {
                
                vm.tanggal = new Date(item.Auction.PostDate);
                vm.remark = item.Auction.Summary;
                vm.docUrl = item.Auction.DocUrl;
                console.info("vm.docUrl: " + JSON.stringify(vm.docUrl));
            }
            loadVendor();
            UploadFileConfigService.getByPageName("PAGE.ADMIN.TENDER.AUCTION", function (response) {
                UIControlService.unloadLoadingModal()
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.unloadLoadingModal()
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                }
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
            });
        }

        function loadVendor() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            AuctionService.getBestBid({
                BatasBawah: vm.batasBawah,
                AuctionID: item.Auction.ID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    if (reply.data.length > 0) {
                        vm.auctionDetail = reply.data;
                        vm.auctionID = vm.auctionDetail[0].AuctionID;
                    }
                } else {
                    UIControlService.msg_growl('success', "MESSAGE.ERR_LOAD_VENDOR_AUCTION");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_VENDOR_AUCTION");
                UIControlService.unloadLoading();
            });
        }

        vm.openDobCalendar = openDobCalendar;
        function openDobCalendar() {
            vm.isDobCalendarOpened = true;
        }

        vm.save = save;
        function save() {
            if (vm.tanggal === undefined || vm.remark === null ||
                vm.tanggal === "" || vm.remark === "") {
                UIControlService.msg_growl('error', "MESSAGE.NOTNULL");
                return;
            }
            if (!vm.file && !vm.docUrl) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return;
            }

            uploadAndSave();
        }

        function uploadAndSave() {
            if (vm.file) {
                uploadFile(vm.file);
            } else {
                saveToDB(vm.docUrl);
            }
        }

        function uploadFile(file) {
            if (validateFileType(file, vm.idUploadConfigs)) {
                upload(file, vm.idFileSize, vm.idFileTypes);
            }
        }

        function validateFileType(file, idUploadConfigs) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload(file, config, types) {

            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            UIControlService.loadLoadingModal('MASSAGE.LOADING');
            UploaderService.uploadSingleFileAuction(vm.auctionID, vm.tenderStepID, file, size, types, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    var url = reply.data.Url;
                    saveToDB(url);
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
            });
        };

        function saveToDB(url) {
            UIControlService.loadLoadingModal('MASSAGE.LOADING');
            AuctionService.inputSummary({
                ID: vm.auctionID,
                TenderStepID: vm.tenderStepID,
                Summary: vm.remark,
                PostDate: vm.tanggal,
                DocUrl: url,
                AuctionDetails: vm.auctionDetail
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_SUMMARY");
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_SUMMARY");
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
            });
        };

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }
    }
})()