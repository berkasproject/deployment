﻿(function () {
    'use strict';

    angular.module("app").controller("ViewDetailCtrl", ctrl);

    ctrl.$inject = ['UIControlService', '$uibModalInstance', 'item', '$filter', '$translatePartialLoader', '$timeout'];

    function ctrl(UIControlService, $uibModalInstance, item, $filter, $translatePartialLoader, $timeout) {
        var vm = this;
        vm.tenderName = item.TenderName;
        vm.auctionDetail = item.AuctionDetail;
        vm.auction = item.Auction;
        vm.endDate = item.DateEnd;
        vm.isOverTime = item.IsOverTime;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('auction');
            vm.bestVendorName = item.BestVendorName;
            vm.bestBidPrice = item.BestBidPrice;
            vm.biddingInformation = vm.auction.BiddingInformation;
            vm.biddingInformationName = vm.auction.BiddingInformationName;
            if (vm.isOverTime === false) {
                getCountDown();
            }
        }

        vm.getCountDown = getCountDown;
        function getCountDown() {
            console.info("vm.isOverTime: " + vm.isOverTime);
            if (vm.isOverTime === false) {
                //vm.countDown = vm.distance;
                vm.endCount = new Date(vm.endDate).getTime();
                vm.date = new Date(vm.endDate);

                vm.now = new Date().getTime();
                vm.distance = vm.endCount - vm.now;
                console.info("vm.distance: " + vm.distance);
                vm.days = Math.floor(vm.distance / (1000 * 60 * 60 * 24));
                vm.hour = Math.floor((vm.distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                vm.minutes = Math.floor((vm.distance % (1000 * 60 * 60)) / (1000 * 60));
                vm.seconds = Math.floor((vm.distance % (1000 * 60)) / 1000);

                vm.distance--;
                vm.destroyCountDown = $timeout(vm.getCountDown, 1000);
                if (vm.distance < 0) {
                    vm.isOverTime = true;
                }
            }
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()