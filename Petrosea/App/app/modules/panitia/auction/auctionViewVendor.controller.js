﻿(function () {
    'use strict';

    angular.module("app").controller("ViewVendorCtrl", ctrl);

    ctrl.$inject = ['UIControlService', '$uibModalInstance'];

    function ctrl(UIControlService, $uibModalInstance) {
        var vm = this;
        vm.init = init;
        function init() {
            
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()