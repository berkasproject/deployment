﻿(function () {
    'use strict';

    angular.module("app").controller("AuctionController", ctrl);

    ctrl.$inject = ['UIControlService', '$uibModal', 'EvaluasiPenawaranBarangService', 'DataPengadaanService', '$stateParams', 'AuctionService', '$filter', 'GlobalConstantService', '$translatePartialLoader', 'EvaluasiPenawaranVHSService', 'EvaluasiHargaJasaService', 'TotalEvaluasiJasaService'];

    function ctrl(UIControlService, $uibModal, EvaluasiPenawaranBarangService, DataPengadaanService, $stateParams, AuctionService, $filter, GlobalConstantService, $translatePartialLoader, EvaluasiPenawaranVHSService, EvaluasiHargaJasaService, TotalEvaluasiJasaService) {
        var vm = this;

        vm.stepID = Number($stateParams.StepID);
        vm.tenderRefID = Number($stateParams.TenderRefID);
        vm.procPackType = Number($stateParams.ProcPackType);

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.isOverTime = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("auction");
            jLoad();
        }

        function jLoad() {
            UIControlService.loadLoading('MESSAGE.LOADING');
            DataPengadaanService.GetStepByID({
                ID: vm.stepID
            }, function (reply) {
                vm.tenderStepData = reply.data;
                vm.tenderID = vm.tenderStepData.TenderID;
                vm.startDate = vm.tenderStepData.StartDate;
                vm.endDate = vm.tenderStepData.EndDate;
                vm.tenderName = vm.tenderStepData.tender.TenderName;
                vm.tenderStepID = vm.tenderStepData.ID;
                vm.status = vm.tenderStepData.StatusName;
                console.info(vm.status)

                vm.dateEnd = new Date(vm.endDate);
                vm.dateToday = new Date();
                if (vm.dateToday > vm.dateEnd) {
                    vm.isOverTime = true;
                }

                console.info("vm.isOverTime: " + vm.isOverTime);

                // 3168 = VHS, 4189 = ContractRequisition, 4190 = Goods
                AuctionService.getEvaluationID({
                    TenderID: vm.tenderID,
                    Status: vm.procPackType
                }, function (reply) {
                    vm.evaluationStep = reply.data[0];
                    vm.evaluationStepName = vm.evaluationStep.step.TenderStepName;
                    console.info(vm.evaluationStep);
                    if (vm.procPackType === 3168) {
                        console.info("1");
                        loadEvaVHS();
                    } else if (vm.procPackType === 4189) {
                        console.info("2");
                        loadEvaCE();
                    } else {
                        console.info("3");
                        loadEvaGoods();
                    }
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });

        }

        function loadEvaVHS() {
            EvaluasiPenawaranVHSService.getByTenderStepData({
                ID: vm.evaluationStep.ID,
                TenderID: vm.tenderID
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.evaluasi = reply.data;
                getBiddingInformation();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        }

        function loadEvaCE() {
            if (vm.evaluationStepName == "Evaluasi Harga Jasa") {
                EvaluasiHargaJasaService.getByTenderStepData({
                    ID: vm.evaluationStep.ID,
                    TenderID: vm.tenderID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    vm.evaluasi = reply.data;
                    console.info(vm.evaluasi);
                    getBiddingInformation();
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                });
            } else {
                TotalEvaluasiJasaService.getFinalTotalEval({
                    TenderStepID: vm.evaluationStep.ID
                }, function (reply) {
                    if (reply.status === 200) {
                        UIControlService.unloadLoading();
                        vm.evaluasi= reply.data;
                        console.info(vm.evaluasi);
                        getBiddingInformation();
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoading();
                });
            }
        }

        function loadEvaGoods() {
            EvaluasiPenawaranBarangService.getByTenderStepData({
                ID: vm.evaluationStep.ID,
                TenderID: vm.tenderID
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.evaluasi = reply.data;
                getBiddingInformation();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        }

        function getAuction() {
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.getAuction({
                TenderStepID: vm.tenderStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    if (reply.data.length > 0) {
                        var data = reply.data[0];
                        vm.dataAuction = data;
                        vm.auctionID = data.ID;
                        vm.remark = data.Summary;
                        vm.docUrl = data.DocUrl;
                        vm.minimumPrice = data.MinimumPrice;
                        vm.biddingInformation = data.BiddingInformation;
                        vm.biddingInformationName = data.BiddingInformationName;
                        vm.isPublish = data.IsPublish;
                        vm.isActive = data.IsActive;
                        if (vm.minimumPrice) {
                            vm.batasAtas = vm.minimumPrice;
                        }
                    } else {
                        vm.auctionID = 0;
                    }
                    
                    getHPS();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_AUCTION');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_AUCTION');
            });
        }

        function getAuctionDetail() {
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.getAuctionDetail({
                BatasBawah: vm.minimumPrice,
                AuctionID: vm.auctionID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    if (reply.data.length > 0) {
                        vm.dataAuctionDetail = reply.data;
                        getBestBid();
                    } else {
                        vm.dataAuctionDetail = null;
                    }
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        }

        function getBestBid() {
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.getBestBid({
                AuctionID: vm.auctionID,
                BatasBawah: vm.minimumPrice
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    if (reply.data.length > 0) {
                        var data = reply.data[0];
                        vm.dataAuctionDetails = reply.data;
                        vm.bestVendorName = data.VendorName;
                        vm.bestBidPrice = $filter('currency')(data.BidPrice, "Rp. ");
                        vm.bestTime = data.SubmitDate;
                    }
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_BEST_BID');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_BEST_BID');
            });
        }

        function getHPS() {
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.getHPS({
                TenderID: vm.tenderID,
                Status: vm.procPackType
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.hps = $filter('currency')(reply.data.OfferTotalCost, "Rp. ");
                    getAuctionDetail();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_HPS');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_HPS');
            });
        }

        function getBiddingInformation() {
            
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.getBiddingInformation(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listBiddingInformation = reply.data.List;
                    getAuction();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_BID_INFO');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_BID_INFO');
            });
        }

        vm.saveMinimumPrice = saveMinimumPrice;
        function saveMinimumPrice() {
            if (vm.batasAtas === null || vm.batasAtas === '' || vm.batasAtas === undefined) {
                return UIControlService.msg_growl("error", 'MESSAGE.ERR_NULL_BATASATAS');
            }
            if (vm.biddingInformation === null || vm.biddingInformation === '' || vm.biddingInformation === undefined) {
                return UIControlService.msg_growl("error", 'MESSAGE.ERR_NULL_BIDDINGINFORMATION');
            }
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.saveMinimumPrice({
                ID: vm.auctionID,
                TenderStepID: vm.tenderStepID,
                MinimumPrice: vm.batasAtas,
                BiddingInformation: vm.biddingInformation
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", 'MESSAGE.SUCC_BATAS_BAWAH');
                    jLoad();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_BATAS_BAWAH');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
            });
        }

        vm.publishSummary = publishSummary;
        function publishSummary() {
            UIControlService.loadLoading('MESSAGE.LOADING');
            AuctionService.publishSummary({
                ID: vm.auctionID,
                MinimumPrice: vm.minimumPrice,
                TenderStepID: vm.tenderStepID,
                Status: vm.procPackType
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", 'MESSAGE.SUCC_SUMMARY');
                    jLoad();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_SUMMARY');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
            });
        }

        vm.addSummary = addSummary;
        function addSummary() {
            var post = {
                act: true,
                Auction: vm.dataAuction,
                AuctionDetail: vm.dataAuctionDetail,
                BatasBawah: vm.minimumPrice
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/auction/auction.modal.html',
                controller: 'AuctionModalCtrl',
                controllerAs: 'AuctionModalCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                vm.init();
            })
        }

        vm.editSummary = editSummary;
        function editSummary() {
            var post = {
                act: false,
                Auction: vm.dataAuction,
                AuctionDetail: vm.dataAuctionDetail,
                BatasBawah: vm.minimumPrice
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/auction/auction.modal.html',
                controller: 'AuctionModalCtrl',
                controllerAs: 'AuctionModalCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                vm.init();
            })
        }

        vm.viewDetail = viewDetail;
        function viewDetail() {
            var post = {
                TenderName: vm.tenderName,
                Auction: vm.dataAuction,
                AuctionDetail: vm.dataAuctionDetail,
                DateEnd: vm.endDate,
                IsOverTime: vm.isOverTime,
                BestBidPrice: vm.bestBidPrice,
                BestVendorName: vm.bestVendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/auction/auctionViewDetail.html',
                controller: 'ViewDetailCtrl',
                controllerAs: 'ViewDetailCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                vm.init();
            })
        }

        vm.viewVendor = viewVendor;
        function viewVendor() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/auction/auctionViewVendor.html',
                controller: 'ViewVendorCtrl',
                controllerAs: 'ViewVendorCtrl'
            })

            modalInstance.result.then(function () {
                vm.init();
            })
        }
    }
})()