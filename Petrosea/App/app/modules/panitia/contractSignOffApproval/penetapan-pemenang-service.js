(function () {
	'use strict';

	angular.module("app").controller("ContractSignOffApprovalController", ctrl);

	ctrl.$inject = ['$state', '$uibModal', '$filter', 'UploadFileConfigService', 'UIControlService', '$translatePartialLoader', 'ContractSignOffService', 'UploaderService', '$stateParams', 'GlobalConstantService'];

	function ctrl($state, $uibModal, $filter, UploadFileConfigService, UIControlService, $translatePartialLoader, ContractSignOffService, UploaderService, $stateParams, GlobalConstantService) {
		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isStartDateOpened = false;
		vm.isEndDateOpened = false;
		vm.filePath;
		vm.flagEmp = 0;
		vm.maxSize = 10;
		vm.offset = 0;
		vm.Keyword = "";
		vm.approvalStatus = "all";
		vm.init = init;
		function init() {
			//loadTypeSizeFile();
			$translatePartialLoader.addPart('contract-signoff');
			loadData(1);

		}

		vm.awardReport = awardReport;
		function awardReport(tenderStepID, vendorId) {
			$state.transitionTo('award-report', { TenderStepDataID: tenderStepID, ProcPackType: 4189, VendorID: vendorId });
		}

		vm.loadData = loadData;
		function loadData(current) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = current;
			ContractSignOffService.getApprovalSignOff({
				Offset: vm.maxSize * (vm.currentPage - 1),
				Limit: vm.maxSize,
				Keyword: vm.Keyword,
				Keyword2: vm.approvalStatus
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.detail = reply.data.List;
					console.info("detail" + JSON.stringify(vm.detail));
					vm.totalItems = Number(reply.data.Count);
					for (var i = 0; i < vm.detail.length; i++) {
						if (vm.detail[i].ApprovalStatusReff == null) {
							vm.detail[i].flagEmp = vm.detail[i].flagEmp;
							vm.detail[i].Status = 'Draft';
							vm.detail[i].flagTemp = 0;
						}
						else if (vm.detail[i].ApprovalStatusReff != null) {
							vm.detail[i].flagEmp = vm.detail[i].flagEmp;
							vm.detail[i].Status = vm.detail[i].ApprovalStatusReff.Value;
							vm.detail[i].flagTemp = 1;
						}
						vm.detail[i].ContractStartDate = new Date(Date.parse(vm.detail[i].ContractStartDate));
						vm.detail[i].ContractEndDate = new Date(Date.parse(vm.detail[i].ContractEndDate));
					}
					UIControlService.unloadLoading();

				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		vm.cekEmployee = cekEmployee;
		function cekEmployee(Id, reff) {
			ContractSignOffService.CekEmployee({
				ID: Id
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					reff.flagEmp = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval(dt, data) {
			console.info("id" + data.ID);
			console.info("flag" + data.flagEmp);
			console.info("dt" + dt);
			var item = {
				ID: data.ID,
				flag: data.flagEmp,
				Status: dt
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/contractSignOff/detailApproval.modal.html?v=1.000002',
				controller: 'detailApprovalSignOffCtrl',
				controllerAs: 'detailApprovalSignOffCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function () {
				init();
			});
		};

		vm.approve = approve;
		function approve(dt, data) {
			console.info("emp" + data.ID);
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVE_CONTRACT_SIGNOFF'), function (yes) {
				if (yes) {
					vm.ID = data.ID;
					ContractSignOffService.SendApproval({
						ID: data.ID,
						Status: 1,
						flagEmp: data.flagEmp
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APPROVAL'));
							sendMailApproval();
							init();
						} else {
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
						}
					}, function (error) {
						UIControlService.unloadLoadingModal();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
					});
				}
			});
		}

		vm.sendMailApproval = sendMailApproval;
		function sendMailApproval() {
			ContractSignOffService.SendEmail({
				ID: vm.ID,
				Status: 1
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) vm.init();
			}, function (error) {
				UIControlService.unloadLoadingModal();
			});
		}

		vm.contractAwardForm = contractAwardForm;
		function contractAwardForm(data) {
			var item = {
				data: data
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/contractSignOffApproval/contractAwardForm.html?v=1.000002',
				controller: 'ContractAwardFormCtrl',
				controllerAs: 'ContractAwardFormCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function () {
				init();
			});
		};
	}
})();