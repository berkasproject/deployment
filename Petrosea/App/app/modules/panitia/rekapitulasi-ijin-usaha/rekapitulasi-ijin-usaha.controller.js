(function () {
	'use strict';

	angular.module("app").controller("RekapitulasiIjinUsahaCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'RekapitulasiIjinUsahaService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, RekapitulasiIjinUsahaService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.maxSize = 10;
		vm.currentPage = 1;

		vm.data = [];

		function init() {
		    $translatePartialLoader.addPart('rekapitulasi-ijin-usaha');
		    dataRekap();
		}

		vm.dataRekap = dataRekap;
		function dataRekap(current) {
		    vm.data = [];
		    vm.currentPage = current;
		    var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    RekapitulasiIjinUsahaService.rekapIjinUsaha({
		        Offset: offset,
		        Limit: vm.maxSize
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            vm.data = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            //console.info("data:" + JSON.stringify(vm.data));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
		        UIControlService.unloadLoading();
		    });
		}

	}
})();
