(function () {
    'use strict';

    angular.module("app")
    .controller("viewDetailTahapanStepDateAppCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'TenderStepDateApprovalService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, TenderStepDateApprovalService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var tenderId = item.TenderId;

        vm.steps = [];
        vm.tenderName = item.TenderName;
        vm.tenderCode = item.TenderCode;

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal("");
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("");
            TenderStepDateApprovalService.getsteps({
                ID: tenderId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.steps = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_STEP'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_STEP'));
            });
        }
               
        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();