(function () {
	'use strict';

	angular.module("app").controller("EvaluationSafetyCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'EvalSafetyService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, EvalSafetyService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		var page_id = 141;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.evalsafety = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.userBisaMengatur = false;
		vm.allowAdd = true;
		vm.allowEdit = true;
		vm.allowDelete = true;
		vm.kata = new Kata("");
		vm.init = init;
		vm.tenderTemp;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		//vm.loadDepartemen = loadDepartemen;
		vm.cariEvalSafety = cariEvalSafety;
		vm.jLoad = jLoad;
		//vm.loadAll = loadAll;
		//vm.ubah_aktif = ubah_aktif;
		//vm.tambah = tambah;
		//vm.edit = edit;

		function init() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			$translatePartialLoader.addPart('safety-evaluation');
			vm.jLoad(1);
		}

		vm.cariEvalSafety = cariEvalSafety;
		function cariEvalSafety() {
			vm.jLoad(1);
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.evalsafety = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			var tender = {
				ID: vm.StepID,
				tender: {
					TenderRefID: vm.TenderRefID,
					ProcPackageType: vm.ProcPackType
				}
			}

			vm.tenderTemp = tender;

			EvalSafetyService.select(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.evalsafety = reply.data;
				    vm.StartDate = UIControlService.convertDateTime(vm.evalsafety[0].TenderStepData.StartDate);
				    vm.EndDate = UIControlService.convertDateTime(vm.evalsafety[0].TenderStepData.EndDate);

				    EvalSafetyService.isLess3Approved({
				        ID: vm.StepID
				    }, function (reply) {
				        if (reply.data === false) {
				            UIControlService.msg_growl("error", 'MESSAGE.NOTIF_LESSTHAN3');
				            kembali();
				        }
				    }, function (error) {
				        UIControlService.msg_growl("error", 'MESSAGE.FAIL_GETDATA_APPROVAL');
				    });

					EvalSafetyService.isNeedTenderStepApproval(tender, function (result) {
						vm.isNeedApproval = result.data;

						if (!vm.isNeedApproval) {
							EvalSafetyService.isApprovalSent({ ID: vm.StepID }, function (result2) {
								vm.isApprovalSent = result2.data;
							});
						}
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval"});
						UIControlService.unloadLoading();
					});

				} else {
					$.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.detail = detail;
		function detail(id, vendorID, flag) {
			if (flag === true) {
				$state.transitionTo('detail-evaluasi-safety', { StepID: vm.StepID, VendorID: vendorID });
			}
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			UIControlService.loadLoading('MESSAGE.SENDING');
			EvalSafetyService.sendToApproval({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
					EvalSafetyService.isNeedTenderStepApproval(vm.tenderTemp, function (result) {
						vm.isNeedApproval = result.data;
						//console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
					init();
				} else {
					$.growl.error({ message: "Send Approval Failed." });
					UIControlService.unloadLoading();
				}
			}, function (err) {
			    $.growl.error({ message: "Send Approval Failed." });
				UIControlService.unloadLoading();
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.StepID;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/safetyEvaluation/detailApproval.html',
				controller: "safetyEvalApprvCtrl",
				controllerAs: "safetyEvalApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.Approval = Approval;
		function Approval() {
			var data = {
				item: vm.TenderRefID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/safetyEvaluation/DetailApproval.html',
				controller: 'DetailApprovalCtrl',
				controllerAs: 'DetailApprovalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}


		vm.print = print;
		function print() {
		    $state.transitionTo('evaluasi-safety-print', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
		}

		vm.kembali = kembali;
		function kembali() {
		    $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.evalsafety[0].TenderStepData.TenderID});
		}
	}
})();
//TODO

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}

