(function () {
    'use strict';

    angular.module("app").controller("DetailEvaluationSafetyCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'EvalSafetyService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, EvalSafetyService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        var page_id = 141;
        vm.StepID = Number($stateParams.StepID);
        vm.VendorID = Number($stateParams.VendorID);
        vm.evalsafety = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        //vm.userBisaMengatur = false;
        //vm.allowAdd = true;
        //vm.allowEdit = true;
        //vm.allowDelete = true;
        vm.isEvaluator = false;
        vm.kata = new Kata("");
        vm.init = init;
        //vm.loadDepartemen = loadDepartemen;
        vm.cariEvalSafety = cariEvalSafety;
        vm.jLoad = jLoad;
        vm.ProcPackageType = 0;
        vm.TenderRefID = 0;

        if (localStorage.getItem("currLang")=='id'||localStorage.getItem("currLang")=='ID'){
            vm.question_list =
            [
                {
                    id: "Apakah diagram organisasi yang ditawarkan secara khusus untuk proyek yang sedang di tenderkan telah di lampiran?"
                },
                {
                    id: "Apakah tugas dan tanggung jawab orang-orang yang terlibat dalam proyek di jabarkan dengan jelas?"
                },
                {
                    id: "Apakah kontraktor telah menunjuk seorang penanggung jawab operasional yang bertanggung jawab terhadap implementasi kebijakan SHE dan / atau implementasi F Bukti SHE??"
                },
                {
                    id: "Apakah orang yang berperan sebagai penanggung jawab operasional memiliki sertifikat POP dan / atau basis Penambangan / Minyak & Gas K3, atau sertifikat K3 dari Departemen Tenaga Kerja?"
                },
                {
                    id: "Apakah daftar alat sebagaimana yang di minta dalam document tender telah dicantumkan?"
                },
                {
                    id: "Apakah pekerja kontraktor memiliki sertifikat kompetensi dari pemerintah sebagaimana yang diminta dalam dokument tender?"
                },
                {
                    id: "Apakah equipment yang ditawarkan dalam proposal tender masih layak operasi?"
                },
                {
                    id: "Apakah dokumen-dokumen yg menunjukkan Komitmen & Tanggung Jawab Manajemen SHE kontraktor secara khusus untuk proyek yang ditenderkan sudah dilampirkan?"
                },
                {
                    id: "Apakah prosedur kerja standar terdokumentasi yang mencakup ruang lingkup pekerjaan kontraktor sudah terlampir?"
                },
                {
                    id: "Apakah pekerja kontraktor melakukan pemeriksaan medis untuk memastikan bahwa semua pekerja secara medis sesuai dengan tugas yang harus mereka lakukan?"
                }

            ];
        }
        else if (localStorage.getItem("currLang")=='en'||localStorage.getItem("currLang")=='EN'){
            vm.question_list =
            [
                {
                    id: "Are the organizational diagrams offered specifically for the project being tendered already attached?"
                },
                {
                    id: "Are the tasks and responsibilities of the people involved in the project clearly explained?"
                },
                {
                    id: "Has the contractor appointed an operational person in charge of SHE policy and / or SHE Exhibit F implementation?"
                },
                {
                    id: "Does the person who plays the operational responsibility have a POP certificate and / or K3 Mining / Oil & Gas basis , or certificate K3 from Manpower Dept.?"
                },
                {
                    id: "Is the list of tools as requested in the tender document included?"
                },
                {
                    id: "Does the contractor worker have a competency certificate from the government as requested in the tender document?"
                },
                {
                    id: "Is the equipment offered in the tender proposal still worth the operation?"
                },
                {
                    id: "Are the documents in showing contractor's Management SHE Commitment & Responsibility specifically for the project being tendered already attached?"
                },
                {
                    id: "Are the documented standard work procedures covering contractor scope of work already attached?"
                },
                {
                    id: "Does the contractor worker have Medical check up to ensure that all workers are medically fit for the tasks they are to undertake?"
                }
            ];
        }
        function init() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            $translatePartialLoader.addPart('safety-evaluation');
            loadStep();
        }

        vm.loadStep = loadStep;
        function loadStep() {

            vm.questions = [];
            //console.info("curr "+current)
            vm.tender = [];
            var tender = {
                ID: vm.StepID
            };
            EvalSafetyService.getStep(tender, function (reply) {
                //console.info("data:"+JSON.stringify(reply));
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.tender = reply.data;
                    vm.StartDate = UIControlService.convertDateTime(vm.tender.StartDate);
                    vm.EndDate = UIControlService.convertDateTime(vm.tender.EndDate);
                    vm.TenderRefID = vm.tender.tender.TenderRefID;
                    vm.ProcPackageType = vm.tender.tender.ProcPackageType;
                    vm.IsOvertime = vm.tender.IsOvertime;
                    EvalSafetyService.isAllowedSeeDetail({
                        TenderRefID: vm.TenderRefID
                    }, function (reply) {
                        if (reply.data === true) {
                            EvalSafetyService.isEvaluator({
                                TenderRefID: vm.TenderRefID
                            }, function (reply) {
                                vm.isEvaluator = reply.data;
                            }, function (err) {
                                UIControlService.msg_growl('error', "MESSAGE.FAIL_GETEVALUATOR");
                            });
                            loadSafe();
                            jLoad(vm.TenderRefID, vm.ProcPackageType);
                        } else {
                            UIControlService.msg_growl('error', "MESSAGE.ERR_EVALUATOR");
                            UIControlService.unloadLoading();
                            vm.kembali();
                        }
                    }, function (err) {
                        UIControlService.msg_growl('error', "MESSAGE.ERR_EVALUATOR");
                        UIControlService.unloadLoading();
                    });
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
                UIControlService.unloadLoading();
            });
        }

        vm.cariEvalSafety = cariEvalSafety;
        function cariEvalSafety() {
            vm.jLoad(1);
        }

        vm.loadSafe = loadSafe;
        function loadSafe() {
            //console.info("curr "+current)
            vm.safety = [];
            var tender = {
                ID: vm.StepID,
                tender: {
                    TenderRefID: vm.TenderRefID,
                    ProcPackageType: vm.ProcPackageType
                }
            }
            EvalSafetyService.select(tender, function (reply) {
                //console.info("data:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.safety = reply.data;
                    for (var i = 0; i < vm.safety.length; i++) {
                        if (vm.safety[i].VendorID == vm.VendorID) {
                            var data = vm.safety[i];
                            vm.safety = [];
                            vm.safety.push(data);
                            return;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(ref, type) {
            //console.info("curr "+current)
            //console.info(JSON.stringify(vm.ProcPackageType));
            vm.evalsafety = [];
            var tender = {
                Status: ref,
                FilterType: type,
                column: vm.VendorID
            }
            EvalSafetyService.selectDetail(tender, function (reply) {
                //console.info("data:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.evalsafety = reply.data;
                    //console.info(JSON.stringify(vm.evalsafety));
                    if(vm.evalsafety.length !== 0){
                        for (var i = 0; i < vm.question_list.length; i++) {
                            for (var y = 0; y < vm.evalsafety.length; y++) {
                                if (i === ((vm.evalsafety[y].Question) - 1))
                                    vm.questions.push({
                                        ID:vm.evalsafety[y].ID,
                                        EvaluationSafetyID: vm.evalsafety[y].EvaluationSafetyID,
                                        no: Number(i + 1),
                                        question: vm.question_list[i].id,
                                        IsValid: vm.evalsafety[i].IsValid,
                                        Description: vm.evalsafety[i].Description,
                                        safety: {
                                            Status: vm.evalsafety[y].safety.Status,
                                            TenderStepDataID: vm.StepID,
                                            VendorID: vm.VendorID
                                        }
                                    });
                            }

                        }
                    }
                    else{
                        for (var i = 0; i < vm.question_list.length; i++) {
                            vm.questions.push({
                                    EvaluationSafetyID:0,
                                    no: Number(i + 1),
                                    question: vm.question_list[i].id,
                                    IsValid: false,
                                    Description: "",
                                    safety: {
                                        Status: 0,
                                        TenderStepDataID: vm.StepID,
                                        VendorID: vm.VendorID
                                    }
                                });
                        }
                    }
                    //console.info(JSON.stringify(vm.questions));
                    
                    
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal mendapatkan data Evalasi Safety" });
                UIControlService.unloadLoading();
            });
        }

        vm.simpan = simpan;
        function simpan(data) {

            vm.list = [];
            for (var i = 0; i < vm.questions.length; i++) {
                vm.list.push({
                    ID: vm.questions[i].ID,
                    EvaluationSafetyID: vm.questions[i].EvaluationSafetyID,
                    Question: i+1,
                    IsValid: vm.questions[i].IsValid,
                    Description: vm.questions[i].Description,
                    safety: {
                        Status: vm.questions[i].safety.Status,
                        TenderStepDataID: vm.StepID,
                        VendorID: vm.VendorID
                    }
                });
            }
            //console.info(JSON.stringify(vm.list));
            EvalSafetyService.InsertDetail(vm.list,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                       init();
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.SUCC_SAVE");
                   UIControlService.unloadLoadingModal();
               }
          );
        }

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('evaluasi-safety', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackageType});
        }



    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

