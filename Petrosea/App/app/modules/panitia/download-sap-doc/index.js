﻿(function () {
	'use strict';

	angular.module("app").controller("DownloadSAPDocCtrl", ctrl)

	ctrl.$inject = ['GoodsAwardService', '$stateParams', 'GlobalConstantService', '$http', 'FileSaver']
	function ctrl(GoodsAwardService, $stateParams, GlobalConstantService, $http, FileSaver) {
		var vm = this
		vm.StepID = Number($stateParams.TenderStepDataID);
		vm.VendorID = Number($stateParams.VendorID);
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		vm.init = init
		function init() {
			GoodsAwardService.checkAuth(function (reply) {
			}, function (err) {
			});
		}

		vm.downloadDoc = downloadDoc
		function downloadDoc() {
			var headers = {};
			headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

			$http({
				method: 'POST',
				url: endpoint + '/GoodsAward/downloadSAPDoc',
				headers: headers,
				data: { VendorID: vm.VendorID, TenderStepID: vm.StepID },
				responseType: 'arraybuffer'
			}).success(function (data, status, hdr) {
				headers = hdr();

				var contentType = headers['content-type'];
				var linkElement = document.createElement('a');
				var fileName = "";

				try {
					var blob = new Blob([data], { type: contentType });
					FileSaver.saveAs(blob, 'download');
				} catch (e) {
					console.log(e);
				}
			});


			//GoodsAwardService.downloadDoc({
			//	VendorID: vm.VendorID,
			//	TenderStepID: vm.StepID
			//}, function (reply) {
			//	try {
			//		var blob = new Blob(reply.data.DataBytes);
			//		FileSaver.saveAs(blob, 'Prequal_Certificate_' + '.pdf');
			//	} catch (e) {
			//		console.log(e);
			//	}
			//}, function (err) {
			//});
		}
	}
})()