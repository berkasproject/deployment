﻿(function () {
    'use strict';

    angular.module("app").controller('masterWbsOacController', ctrl);

    ctrl.$inject = ['UIControlService', '$translatePartialLoader', 'wbsOacService', '$uibModal'];

    function ctrl(UIControlService, $translatePartialLoader, wbsOacService, $uibModal) {
        var vm = this;

        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;

        vm.keyword = '';
        vm.totalItems = 0;
        vm.maxSize = 10;
        vm.currentPage = 1;

        vm.dataWbsOac = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-wbs-oac');
            loadWbsOac(1);
        }

        vm.loadWbsOac = loadWbsOac;
        function loadWbsOac(current) {
            UIControlService.loadLoading('MESSAGE.LOADING');
            wbsOacService.select({
                Offset: (vm.currentPage - 1) * vm.maxSize,
                Limit: vm.maxSize,
                Keyword: vm.keyword
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataWbsOac = reply.data.List;
                    vm.totalItems = reply.data.Count;
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.GET_WBSOAC_ERROR', "MESSAGE.GET_WBSOAC_TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
            });
        }

        vm.addWbsOac = addWbsOac;
        function addWbsOac() {
            var modalInstance = $uibModal.open({
                templateUrl: 'addModalWbsOac.html',
                controller: addWbsOacCtrl,
                controllerAs: 'addWbsOacCtrl'
            });

            modalInstance.result.then(function () {
                loadWbsOac();
            });
        }

        vm.editWbsOac = editWbsOac;
        function editWbsOac(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'editModalWbsOac.html',
                controller: editWbsOacCtrl,
                controllerAs: 'editWbsOacCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                loadWbsOac();
            });
        }

        vm.inactivateWbsOac = inactivateWbsOac;
        function inactivateWbsOac(id, is_used) {
            if (is_used) {
                UIControlService.msg_growl("warning", 'MESSAGE.ERROR_IS_USED.MESSAGE', "MESSAGE.ERROR_IS_USED.TITLE");

                return
            }
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'inactivateModalWbsOac.html',
                controller: inactivateWbsOacCtrl,
                controllerAs: 'inactivateWbsOacCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                loadWbsOac();
            });
        }

        vm.activateWbsOac = activateWbsOac;
        function activateWbsOac(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'activateModalWbsOac.html',
                controller: activateWbsOacCtrl,
                controllerAs: 'activateWbsOacCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                loadWbsOac();
            });
        }

        vm.searchData = searchData;
        function searchData() {
            loadWbsOac(1);
        }

        vm.pageChanged = pageChanged;
        function pageChanged() {
            loadWbsOac(1);
        }
    }
})()

var addWbsOacCtrl = function (UIControlService, $uibModalInstance, wbsOacService) {
    var vm = this;

    vm.wbsCode = '';
    vm.deskripsi = '';

    vm.createWbsOac = createWbsOac;
    function createWbsOac() {
        if (vm.wbsCode == '' || vm.wbsCode == undefined) {
            UIControlService.msg_growl("error", 'MESSAGE.WBSCODE_EMPTY.MESSAGE', "MESSAGE.WBSCODE_EMPTY.TITLE");

            return
        }
        if (vm.deskripsi == '' || vm.deskripsi == undefined) {
            UIControlService.msg_growl("error", 'MESSAGE.DESCRIPTION_EMPTY.MESSAGE', "MESSAGE.DESCRIPTION_EMPTY.TITLE");
            return
        }

        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        wbsOacService.insert({
            WBSCode: vm.wbsCode,
            Description: vm.deskripsi
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.ADD_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal()
                UIControlService.msg_growl("error", 'MESSAGE.ADD_ERROR_MESSAGE', "MESSAGE.ADD_ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close()
    }
}

var editWbsOacCtrl = function (item, UIControlService, $uibModalInstance, wbsOacService) {
    var vm = this;

    vm.wbsCode = '';
    vm.deskripsi = '';

    UIControlService.loadLoadingModal('MESSAGE.LOADING');
    wbsOacService.getByID({
        ID: item
    }, function (reply) {
        if (reply.status === 200) {
            vm.wbsCode = reply.data.WBSCode;
            vm.deskripsi = reply.data.Description;
            UIControlService.unloadLoadingModal();
        } else {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        }
    }, function (err) {
        UIControlService.unloadLoadingModal();
        UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
    });

    vm.updateWbsOacSave = updateWbsOacSave;
    function updateWbsOacSave() {
        if (vm.wbsCode == '' || vm.wbsCode == undefined) {
            UIControlService.msg_growl("error", 'MESSAGE.WBSCODE_EMPTY.MESSAGE', "MESSAGE.WBSCODE_EMPTY.TITLE");

            return
        }
        if (vm.deskripsi == '' || vm.deskripsi == undefined) {
            UIControlService.msg_growl("error", 'MESSAGE.DESCRIPTION_EMPTY.MESSAGE', "MESSAGE.DESCRIPTION_EMPTY.TITLE");
            return
        }

        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        wbsOacService.update({
            ID: item,
            WBSCode: vm.wbsCode,
            Description: vm.deskripsi
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.EDIT_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.EDIT_ERROR_MESSAGE', "MESSAGE.EDIT_ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var inactivateWbsOacCtrl = function (item, UIControlService, $uibModalInstance, wbsOacService) {
    var vm = this;

    UIControlService.loadLoadingModal('MESSAGE.LOADING');
    vm.inactivateWbsOacSave = inactivateWbsOacSave;
    function inactivateWbsOacSave() {
        wbsOacService.inactivate({
            ID: item
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.INACTIVATE_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.INACTIVATE_ERROR_MESSAGE', "MESSAGE.ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var activateWbsOacCtrl = function (item, UIControlService, $uibModalInstance, wbsOacService) {
    var vm = this;

    UIControlService.loadLoadingModal('MESSAGE.LOADING');
    vm.activateWbsOacSave = activateWbsOacSave;
    function activateWbsOacSave() {
        wbsOacService.activate({
            ID: item
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.ACTIVATE_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ACTIVATE_ERROR_MESSAGE', "MESSAGE.ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}