﻿(function () {
    'use strict';

    angular.module("app").controller('commodityController', ctrl);

    ctrl.$inject = ['UIControlService', '$translatePartialLoader', 'CommodityService', '$uibModal'];

    function ctrl(UIControlService, $translatePartialLoader, CommodityService, $uibModal) {
        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.keyword = "";

        vm.dataCommodity = [];

        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-commodity');
            loadSteps(1);
        }

        vm.loadSteps = loadSteps;
        function loadSteps(current) {
            UIControlService.unloadLoading('MESSAGE.GET_COMMODITIES');
            CommodityService.select({
                Offset: (vm.currentPage - 1) * vm.maxSize,
                Limit: vm.maxSize,
                Keyword: vm.keyword
            }, function (reply) {
                if (reply.status === 200) {
                    vm.dataCommodity = reply.data.List;
                    vm.totalItems = reply.data.Count;
                    UIControlService.unloadLoading();
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "MESSAGE.GET_COMMODITIES_ERROR", "MESSAGE.GET_COMMODITIES_TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.GET_API_ERROR", "MESSAGE.GET_API_ERROR_TITLE");
            });
        }

        vm.searchData = searchData;
        function searchData() {
            loadSteps(1);
        }

        vm.addCommodity = addCommodity;
        function addCommodity() {
            var modalInstance = $uibModal.open({
                templateUrl: 'addModalCommodity.html',
                controller: addCommodityCtrl,
                controllerAs: 'addCommodityCtrl'
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.editCommodity = editCommodity;
        function editCommodity(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'editModalCommodity.html',
                controller: editCommodityCtrl,
                controllerAs: 'editCommodityCtrl',
                resolve: { item: function () { return post; }}
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.inactivateCommodity = inactivateCommodity;
        function inactivateCommodity(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'inactivateModalCommodity.html',
                controller: inactivateCommodityCtrl,
                controllerAs: 'inactivateCommodityCtrl',
                resolve: { item: function () { return post;} }
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.activateCommodity = activateCommodity;
        function activateCommodity(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'activateModalCommodity.html',
                controller: activateCommodityCtrl,
                controllerAs: 'activateCommodityCtrl',
                resolve: { item: function () { return post; } }
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.pageChanged = pageChanged;
        function pageChanged() {
            loadSteps();
        }
    }
})();

var addCommodityCtrl = function (UIControlService, $uibModalInstance, CommodityService) {
    var vm = this;

    vm.namaKomoditas;
    vm.deskripsi;

    vm.createCommodity = createCommodity;
    function createCommodity() {
        if (vm.namaKomoditas.trim() === '') {
            return false;
        }

        UIControlService.unloadLoadingModal('MESSAGE.LOADING');
        CommodityService.create({
            Name: vm.namaKomoditas,
            Description: vm.deskripsi
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", "MESSAGE.ADD_SUCCESS_MESSAGE", "MESSAGE.ADD_SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ADD_ERROR_MESSAGE", "MESSAGE.ADD_ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", "MESSAGE.GET_API_ERROR", "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var editCommodityCtrl = function (UIControlService, item, $uibModalInstance, CommodityService) {
    var vm = this;

    vm.namaKomoditas = '';
    vm.deskripsi = '';

    CommodityService.getByID({
        ID: item
    }, function (reply) {
        if (reply.status === 200) {
            vm.namaKomoditas = reply.data.Name;
            vm.deskripsi = reply.data.Description;
            UIControlService.unloadLoadingModal();
        } else {
            UIControlService.unloadLoadingModal()
            UIControlService.msg_growl("error", 'MESSAGE.GET_COMMODITY_ERROR', "MESSAGE.GET_COMMODITY_TITLE");
        }
    }, function (err) {
        UIControlService.unloadLoadingModal()
        UIControlService.msg_growl("error", "MESSAGE.GET_API_ERROR", "MESSAGE.GET_API_ERROR_TITLE");
    });

    vm.updateCommodity = updateCommodity;
    function updateCommodity() {
        if (vm.namaKomoditas.trim() === '') {
            return false;
        }

        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        CommodityService.update({
            ID: item,
            Name: vm.namaKomoditas,
            Description: vm.deskripsi
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", "MESSAGE.EDIT_SUCCESS_MESSAGE", "MESSAGE.EDIT_SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.EDIT_ERROR_MESSAGE", "MESSAGE.EDIT_ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", "MESSAGE.GET_API_ERROR", "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var inactivateCommodityCtrl = function (UIControlService, item, $uibModalInstance, CommodityService) {
    var vm = this;

    vm.inactivateCommodity = inactivateCommodity;
    function inactivateCommodity() {
        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        CommodityService.inactivate({
            ID: item
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.INACTIVATE_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.INACTIVATE_ERROR_MESSAGE', "MESSAGE.ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", "MESSAGE.GET_API_ERROR", "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var activateCommodityCtrl = function (UIControlService, item, $uibModalInstance, CommodityService) {
    var vm = this;

    vm.activateKomoditas = activateKomoditas;
    function activateKomoditas() {
        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        CommodityService.activate({
            ID: item
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal()
                UIControlService.msg_growl("success", 'MESSAGE.ACTIVATE_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ACTIVATE_ERROR_MESSAGE', "MESSAGE.ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", "MESSAGE.GET_API_ERROR", "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}