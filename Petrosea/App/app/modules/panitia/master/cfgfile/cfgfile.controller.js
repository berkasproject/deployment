﻿(function () {
    'use strict';

    angular.module("app").controller("CfgfileCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'CfgfileService', 'RoleService', 'UIControlService', '$uibModal'];
    function ctrl($state, $http, $translate, $translatePartialLoader, $location, SocketService, CfgfileService,
        RoleService, UIControlService, $uibModal) {

        var vm = this;
        var page_id = 141;

        vm.cfgfile = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.kata = new Kata("");
        vm.init = init;

        vm.cariFile = cariFile;
        vm.jLoad = jLoad;


        function init() {
            $translatePartialLoader.addPart('cfgfile');
            UIControlService.loadLoading("Silahkan Tunggu...");
            jLoad(1);
        }

        vm.cariFile = cariFile;
        function cariFile() {
            vm.jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.cfgfile = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            CfgfileService.select({ Offset: offset, Limit: vm.pageSize, Keyword: vm.kata.srcText }, function (reply) {
                //console.info("data:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.cfgfile = data.List;
                    vm.totalItems = Number(data.Count);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan cfg file" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.tambah = tambah;
        function tambah(data) {
            console.info("masuk form add/edit");
            var data = {
                act: true,
                item: data,
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/cfgfile/formCfgfile.html',
                controller: 'cfgfileModalCtrl',
                controllerAs: 'formCfgfileCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.detail = detail;
        function detail(data) {
            $state.transitionTo('detail-cfgfile', { PageName: data.PageName });
        };

    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}
