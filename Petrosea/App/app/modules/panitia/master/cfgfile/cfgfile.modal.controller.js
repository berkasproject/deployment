﻿(function () {
    'use strict';

    angular.module("app").controller("cfgfileModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'CfgfileService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, CfgfileService,
		RoleService, UIControlService, item, $uibModalInstance) {

        var vm = this;
        //console.info("masuuk modal : " + JSON.stringify(item));
        vm.isAdd = item.act;
        vm.PageName = item.PageName;
        vm.action = "";
        vm.types = [];
        vm.CfgModel = [];
        vm.tipe = [];
        vm.cfgfile = [];

        vm.unitSize = 0;
        vm.FileSizeId = 0;
        vm.cfgfilesize = [];
        vm.filesize;

        var cfgFile = [];

        vm.init = init;
        function init() {
            console.info(vm.isAdd);
            loadAwal();
        }

        function loadAwal() {
            if (vm.isAdd == true) {

            } else if (vm.isAdd == false) {
                console.info(vm.PageName);
                selectByPagename();
                getType();
            }
        }

        function getType() {
            CfgfileService.getType(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.cfgfiletype = reply.data;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan sizeunit" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                UIControlService.unloadLoading();
            });
        };

        vm.onChange = onChange;
        function onChange() {
            vm.unitSize = vm.filesize.SizeUnit;
            getSizeUnit();
        };

        function getSize() {
            CfgfileService.getSize(
                function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        vm.cfgfilesize = reply.data;
                        if (!vm.isAdd) {
                            for (var i = 0; i < vm.cfgfilesize.length; i++) {
                                if (vm.cfgfilesize[i].FileSizeID === vm.FileSizeId) {
                                    vm.filesize = vm.cfgfilesize[i];
                                    vm.unitSize = vm.filesize.SizeUnit;
                                    getSizeUnit();
                                    break;
                                }
                            }

                        }
                    } else {
                        $.growl.error({ message: "Gagal mendapatkan cfg file" });
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    console.info("error:" + JSON.stringify(err));
                    UIControlService.unloadLoading();
                });
        };

        function getSizeUnit() {
            CfgfileService.getSizeUnit(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.sizeunit = reply.data.List;
                    for (var i = 0; i < vm.sizeunit.length; i++) {
                        if (vm.sizeunit[i].RefID === vm.unitSize) {
                            vm.size_unit = vm.sizeunit[i];
                            break;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan sizeunit" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                UIControlService.unloadLoading();
            });
        };

        function selectByPagename() {
            CfgfileService.selectbypagename({
                PageName: vm.PageName,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.cfgfile = reply.data;
                    vm.cfgFile = vm.cfgfile;
                    vm.page_name = vm.cfgfile[0].PageName;
                    vm.cfgfile.forEach(function (krit) {
                        cfgFile.push(krit);
                    });

                } else {
                    $.growl.error({ message: "Gagal mendapatkan cfg file" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                UIControlService.unloadLoading();
            });
        };

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.simpan = simpan;
        function simpan() {
            if (vm.isAdd) {

            } else if (!vm.isAdd) {
                var datasimpan = [];
                if (!vm.filetype) {
                    UIControlService.msg_growl("error", "NO FILE TYPE");
                }
                for (var i = 0; i < cfgFile.length; i++) {
                    if (vm.filetype.FileTypeID == cfgFile[i].FileTypeID)
                        UIControlService.msg_growl("error", "FILE TYPE IS AVAILABLE");
                }

                CfgfileService.insertnewfiletype({
                    FileTypeID: vm.filetype.FileTypeID,
                    FileSizeID: cfgFile[0].FileSizeID,
                    PageLayerID: cfgFile[0].PageLayerID,
                    ModuleID: cfgFile[0].ModuleID,
                    PageName: vm.PageName
                }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        $uibModalInstance.close();
                        UIControlService.msg_growl("notice", "INSERT FILE SUCCESS");
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.msg_growl("error", "INSERT FILE FAILED");
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "INSERT FILE FAILED");
                    UIControlService.unloadLoading();
                });
            }
        }

    }
})();