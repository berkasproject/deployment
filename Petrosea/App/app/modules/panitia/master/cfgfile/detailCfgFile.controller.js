﻿(function () {
    'use strict';

    angular.module("app").controller("detailCfgfileCtrl", ctrl);

    ctrl.$inject = ['$state', '$filter', '$stateParams', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'CfgfileService', 'RoleService', 'UIControlService', '$uibModal'];
    function ctrl($state, $filter, $stateParams, $http, $translate, $translatePartialLoader, $location, SocketService, CfgfileService,
        RoleService, UIControlService, $uibModal) {

        var vm = this;
        var lang;
        var page_id = 141;
        var PageName = $stateParams.PageName;
        vm.PageName = PageName;

        vm.cfgfile = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.kata = new Kata("");
        vm.init = init;

        vm.jLoad = jLoad;


        function init() {
            $translatePartialLoader.addPart('cfgfile');
            UIControlService.loadLoading("Silahkan Tunggu...");
            jLoad(1)
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            CfgfileService.selectbypagename({
                PageName: PageName,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.cfgfile = reply.data;
                    getSize();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan cfg file" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                UIControlService.unloadLoading();
            });
        }

        vm.onChange = onChange;
        function onChange() {
            vm.unitSize = vm.filesize.SizeUnit;
            getSizeUnit();
        };

        function getSize() {
            CfgfileService.getSize(
                function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        vm.cfgfilesize = reply.data;
                        if (!vm.isAdd) {
                            for (var i = 0; i < vm.cfgfilesize.length; i++) {
                                if (vm.cfgfilesize[i].FileSizeID === vm.cfgfile[0].FileSizeID) {
                                    vm.filesize = vm.cfgfilesize[i];
                                    vm.unitSize = vm.filesize.SizeUnit;
                                    getSizeUnit();
                                    break;
                                }
                            }

                        }
                    } else {
                        $.growl.error({ message: "Gagal mendapatkan cfg file" });
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    console.info("error:" + JSON.stringify(err));
                    UIControlService.unloadLoading();
                });
        };

        function getSizeUnit() {
            CfgfileService.getSizeUnit(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.sizeunit = reply.data.List;
                    for (var i = 0; i < vm.sizeunit.length; i++) {
                        if (vm.sizeunit[i].RefID === vm.unitSize) {
                            vm.size_unit = vm.sizeunit[i];
                            break;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan sizeunit" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                UIControlService.unloadLoading();
            });
        };

        vm.back = back
        function back() {
            $state.transitionTo('cfgfile');
        };

        vm.tambah = tambah;
        function tambah(data) {
            console.info("masuk form add/edit");
            var data = {
                act: false,
                PageName: PageName
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/cfgfile/formCfgfile.html',
                controller: 'cfgfileModalCtrl',
                controllerAs: 'formCfgfileCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.simpan = simpan
        function simpan() {
            CfgfileService.updateFileSize({
                FileSizeID: vm.filesize.FileSizeID,
                PageName: PageName
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", "SUCCESS MERUBAH UKURAN FILE");
                    UIControlService.loadLoading("Silahkan Tunggu...");
                    vm.jLoad(1);
                } else {
                    UIControlService.msg_growl("error", "GAGAL MERUBAH UKURAN FILE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('GAGAL MERUBAH UKURAN FILE'));
            });
        };

        vm.switchActive = switchActive
        function switchActive(data) {
            var pesan = "";

            switch (lang) {
                case 'id': pesan = 'Anda yakin untuk ' + (data.IsActive ? 'mengaktifkan' : 'menonaktifkan') + ' tipe file "' + data.Type.Name + '"?'; break;
                default: pesan = 'Are you sure want to ' + (data.IsActive ? 'deaktivate' : 'activate') + ' this file type : "' + data.Type.Name + '" ?'; break;
            }

            bootbox.confirm(pesan, function (yes) {
                if (yes) {
                    CfgfileService.switchActive({
                        FileTypeID: data.FileTypeID,
                        PageName: data.PageName
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "MESSAGE.SUCC_TOGGLE_ACTIVATION");
                            vm.jLoad(1);
                        } else {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_TOGGLE_ACTIVATION");
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_TOGGLE_ACTIVATION'));
                    });
                }
            })
        };
    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}
