﻿(function () {
	'use strict';

	angular.module("app").controller("DataVendorCtrl", ctrl);

	ctrl.$inject = ['UIControlService', '$uibModalInstance', 'PurchaseRequisitionService','item'];

	function ctrl(UIControlService, $uibModalInstance, PurchReqService, items) {
		var vm = this;

		vm.vendors = null;
		vm.currentPage = 1;
		vm.maxSize = 20;
		vm.keyword = '';

		vm.getAllVendors = getAllVendors;
		function getAllVendors() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			PurchReqService.getAllVendors({
				Vendors: items.Vendor,
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword,
				CommodityID: items.CommodityID
			}, function (reply) {
				if (reply.status === 200) {
					vm.vendors = reply.data.List;
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'MESSAGE.FAIL_GET_VENDOR', "MESSAGE.ERR_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.FAIL_GET_VENDOR', "MESSAGE.ERR_TITLE");
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			getAllVendors();
		}

		vm.selectVendor = selectVendor;
		function selectVendor(selectedVendor) {
		    $uibModalInstance.close(selectedVendor);
		}

		vm.cancel = cancel;
		function cancel() {
		    $uibModalInstance.close();
		}
	}
})();