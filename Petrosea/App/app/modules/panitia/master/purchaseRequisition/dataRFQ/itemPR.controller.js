﻿(function () {
    'use strict';

    angular.module("app")
    .controller("ItemPRCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PurchaseRequisitionService', '$state',
        'UIControlService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PurchReqService, $state, UIControlService,
        item, $uibModalInstance) {
        var vm = this;
        ////console.info(JSON.stringify(item));
        vm.maxSize = 10;
        vm.pageSize = 10;
        vm.dataChecked = [];
        vm.flag = item.flag;
        vm.currentPage = 1;
        vm.totalItems = 0;
        vm.textDate = null;
        vm.textSearch = "";
        vm.listItemPR = [];
        vm.listCheckedPR = item.item;
        vm.CommodityID = item.CommodityID;
        vm.CommodityName = item.CommodityName;
        vm.ManufacturerID = item.ManufacturerID;
        vm.RFQId = item.RFQId;
        vm.init = init;
        vm.listItemPRRFQ = [];
        function init() {
            console.info(item);
            $translatePartialLoader.addPart("purchase-requisition");
            if (vm.RFQId !== 0) {
                if (item.item.length != 0) jLoadDataItem(1);
					else vm.jLoad(1);
            }
            else {
                vm.jLoad(1);
            }
        }

        vm.checkbox = checkbox;
        function checkbox() {
            vm.dataflag = 1;
            for (var x = 0; x < vm.listItemPR.length; x++) {
                if (vm.listItemPR[x].IsUsed == false || vm.listItemPR[x].IsUsed == null) {
                    vm.dataflag = 0;
                }
                if (x == (vm.listItemPR.length - 1)) {
                    if (vm.dataflag == 0) vm.isCekAll = false;
                    else vm.isCekAll = true;

                }
            }
        }

        vm.Checked = Checked;
        function Checked(dt) {
            //console.info(JSON.parse(localStorage.getItem('checked-itempr')));
            vm.dataChecked = JSON.parse(localStorage.getItem('checked-itempr'));
            vm.check = { ID: dt.ID };
            if (dt.IsUsed == true) {
                if (vm.dataChecked == null) vm.dataChecked = [];
                vm.dataChecked.push(vm.check);
            }
            else {
                for (var i = 0; i < vm.dataChecked.length; i++) {
                    if (vm.dataChecked[i].ID == dt.ID) {
                        vm.dataChecked.splice(i, 1);
                    }
                }
            }
            localStorage.removeItem('checked-itempr');
            localStorage.setItem('checked-itempr', JSON.stringify(vm.dataChecked));
            //console.info(JSON.parse(localStorage.getItem('checked-itempr')));
        }

        vm.CheckedAll = CheckedAll;
        function CheckedAll() {
            vm.dataChecked = [];
            if (vm.isCekAll == true) {
                localStorage.setItem('checked-all-itempr', JSON.stringify(true));
                for (var j = 0; j < vm.listItemPR.length; j++) {
                    vm.listItemPR[j].IsUsed= true;
                }
                loadPaketAll(1);
            }
            else {
                localStorage.removeItem('checked-itempr');
                localStorage.removeItem('checked-all-itempr');
                vm.jLoad(1);
            }
        }

        vm.loadPaketAll = loadPaketAll;
        function loadPaketAll(current) {
            vm.currentPage = current;
            UIControlService.loadLoading("Loading. . .");
            var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
            PurchReqService.getDataItemPR({
                Status: vm.CommodityID,
                Offset: offset,
                Limit: 0
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.all = reply.data.List;
                    for (var i = 0; i < vm.all.length; i++) {
                        vm.dataChecked.push({ ID: vm.all[i].ID });
                        vm.all[i].IsUsed = true;
                        if ((vm.all.length - 1) == i) {
                            localStorage.removeItem('checked-itempr');
                            localStorage.setItem('checked-itempr', JSON.stringify(vm.dataChecked));
                            vm.jLoad(1);
                        }
                    }

                }


                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
                        function (error) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                        });
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            if (vm.RFQId == 0) vm.listItemPR = [];
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
            PurchReqService.getDataItemPR({
                Status: vm.CommodityID,
                IntParam1: vm.ManufacturerID,
                Offset: offset,
                Limit: vm.maxSize
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataItemOutstand = data.List;
                    /*if (vm.RFQId == 0)*/ vm.totalItems = Number(data.Count);
                    vm.dataChecked = JSON.parse(localStorage.getItem('checked-itempr'));
                    //console.info(vm.dataChecked);
                    vm.IsCheck = JSON.parse(localStorage.getItem('checked-all-itempr'));
                    if (vm.dataItemOutstand.length > 0) {
                        for (var i = 0; i < vm.dataItemOutstand.length; i++) {
                            //console.info(vm.listItemPR[i].IsUsed);
                            if (vm.dataChecked != null) {
                                for (var j = 0; j < vm.dataChecked.length; j++) {
                                    if (vm.dataChecked[j].ID == vm.dataItemOutstand[i].ID) {
                                        vm.dataItemOutstand[i].IsUsed = true;
                                        //console.info(vm.listItemPR[i].IsUsed);
                                        break;
                                    }
                                }
                            }
                            else {
                                for (var j = 0; j < vm.listCheckedPR.length; j++) {
                                    //console.info(vm.listCheckedPR[j]);
                                    //console.info(vm.listItemPR[i]);
                                    if (vm.listCheckedPR[j].ID == vm.dataItemOutstand[i].ID) {
                                        vm.dataItemOutstand[i].IsUsed = true;
                                        break;
                                    }
                                }
                            }

                            if (vm.dataItemOutstand[i].InputDate === null) { vm.dataItemOutstand[i].InputDate = "-"; }
                            else { vm.dataItemOutstand[i].InputDate = UIControlService.getStrDate(vm.dataItemOutstand[i].InputDate); }

                            if (vm.dataItemOutstand[i].ApprovalDate === null) { vm.dataItemOutstand[i].ApprovalDate = "-"; }
                            else { vm.dataItemOutstand[i].ApprovalDate = UIControlService.getStrDate(vm.dataItemOutstand[i].ApprovalDate); }

                            if (vm.dataItemOutstand[i].DeliveryDate === null) { vm.dataItemOutstand[i].DeliveryDate = "-"; }
                            else { vm.dataItemOutstand[i].DeliveryDate = UIControlService.getStrDate(vm.dataItemOutstand[i].DeliveryDate); }

                            vm.flagItem = true;
                            vm.listItemPR.forEach(function (dataItem) {
                                if (dataItem.ID == vm.dataItemOutstand[i].ID) {
                                    vm.flagItem = false;
                                }
                            });
                            if(vm.flagItem == true) vm.listItemPR.push(vm.dataItemOutstand[i]);
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                ////console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                UIControlService.unloadLoadingModal();
            });
            
        }

        vm.jLoadDataItem = jLoadDataItem;
        function jLoadDataItem(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
            PurchReqService.getDataItemPRUpdate({
                Status: vm.CommodityID,
                IntParam1: vm.ManufacturerID,
                Offset: offset,
                Limit: vm.maxSize,
                FilterType: vm.RFQId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listItemPR = data.List;
                    vm.totalItems = Number(data.Count);
                    console.info(vm.totalItems);
                    vm.dataChecked = JSON.parse(localStorage.getItem('checked-itempr'));
                    //console.info(vm.dataChecked);
                    vm.IsCheck = JSON.parse(localStorage.getItem('checked-all-itempr'));
                    if (vm.dataChecked == null) vm.dataChecked = [];
                    if (vm.listItemPR.length > 0) {
                        for (var i = 0; i < vm.listItemPR.length; i++) {
                            //console.info(vm.listItemPR[i]);
                            if (vm.dataChecked.length !== 0) {
                                for (var j = 0; j < vm.dataChecked.length; j++) {
                                    if (vm.dataChecked[j].ID == vm.listItemPR[i].ID) {
                                        vm.listItemPR[i].IsUsed = true;
                                        //console.info(vm.listItemPR[i].IsUsed);
                                        break;
                                    }
                                }
                            }
                            if (vm.listItemPR[i].IsUsed == true) {
                                var foundCheck = $.map(vm.listCheckedPR, function (val) {
                                    return (val.ID == vm.listItemPR[i].ID) ? val : null;
                                });
                                //console.info(vm.listCheckedPR);
                                //console.info(vm.listItemPR[i]);
                                if (foundCheck.length == 0) {
                                    vm.listItemPR[i].IsUsed = false;
                                }
                                else {
                                    var foundCheck = $.map(vm.dataChecked, function (val) {
                                        return (val.ID == vm.listItemPR[i].ID) ? val : null;
                                    });
                                    //console.info(vm.listItemPR[i]);
                                    if (foundCheck.length == 0) {
                                        Checked(vm.listItemPR[i]);
                                    }
                                }
                            }
                            if (vm.listItemPR[i].InputDate === null) { vm.listItemPR[i].InputDate = "-"; }
                            else { vm.listItemPR[i].InputDate = UIControlService.getStrDate(vm.listItemPR[i].InputDate); }

                            if (vm.listItemPR[i].ApprovalDate === null) { vm.listItemPR[i].ApprovalDate = "-"; }
                            else { vm.listItemPR[i].ApprovalDate = UIControlService.getStrDate(vm.listItemPR[i].ApprovalDate); }

                            if (vm.listItemPR[i].DeliveryDate === null) { vm.listItemPR[i].DeliveryDate = "-"; }
                            else { vm.listItemPR[i].DeliveryDate = UIControlService.getStrDate(vm.listItemPR[i].DeliveryDate); }

                            if (i == (vm.listItemPR.length - 1)) {
                                if (vm.flag === 1) jLoad(vm.currentPage);
                            }
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                ////console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                UIControlService.unloadLoadingModal();
            });
        }

        /*
        vm.getItem = getItem;
        function getItem(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;

            PurchReqService.getItemPR({
                ID: vm.RFQId
            }, function (reply) {
                ////console.info("dta:"+JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listItemPR = data.List;
                    vm.totalItems = Number(data.Count);
                    for (var i = 0; i < vm.listItemPR.length; i++) {
                        if (vm.listItemPR[i].InputDate === null) { vm.listItemPR[i].InputDate = "-"; }
                        else { vm.listItemPR[i].InputDate = UIControlService.getStrDate(vm.listItemPR[i].InputDate); }

                        if (vm.listItemPR[i].ApprovalDate === null) { vm.listItemPR[i].ApprovalDate = "-"; }
                        else { vm.listItemPR[i].ApprovalDate = UIControlService.getStrDate(vm.listItemPR[i].ApprovalDate); }

                        if (vm.listItemPR[i].DeliveryDate === null) { vm.listItemPR[i].DeliveryDate = "-"; }
                        else { vm.listItemPR[i].DeliveryDate = UIControlService.getStrDate(vm.listItemPR[i].DeliveryDate); }
                    }
                    //console.info(vm.flag);
                    if (vm.flag == 1) {
                        jLoadDataItem(1);
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                ////console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                UIControlService.unloadLoadingModal();
            });
        }
        */

        /*
        vm.choosePR = choosePR;
        function choosePR(row) {
            vm.listCheckedPR.push(row);
        }
        */

        vm.checkAll = checkAll;
        vm.isCekAll;
        function checkAll() {
            //console.info(vm.isCekAll);
            for (var i = 0; i < vm.listItemPR.length; i++) {
                if (vm.isCekAll === true) {
                    vm.listItemPR[i].IsUsed = true;
                    //vm.listCheckedPR.push(vm.listItemPR[i]);
                }
                else {
                    vm.listItemPR[i].IsUsed = false;
                }
            }
            //vm.jLoad(1);
        }

        vm.simpanItem = simpanItem;
        function simpanItem(dataitem) {
            vm.item = JSON.parse(localStorage.getItem('checked-itempr'));
            $uibModalInstance.close(vm.item);
        };

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();