(function () {
	'use strict';

	angular.module("app").controller("PurchaseRequisitionCtrl", ctrl);

	ctrl.$inject = ['$filter', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PurchaseRequisitionService', '$state', 'UIControlService', '$uibModal'];
	function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService, PurchReqService,
        $state, UIControlService, $uibModal) {
		var vm = this;
		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.textKeyword = '';
		vm.colKeyword = 1;
		vm.PRList = [];
		vm.totalItems = 0;
		vm.EmailAssosiasi = [];
		vm.rfqGoodsApprvls = [];
		vm.totalItemsApprvls = 0;
		vm.columnApprovalStatus = '2'
		vm.EmailVendor = [];
		vm.init = init();
		vm.flagButton = true;
		function init() {
			localStorage.removeItem('checked-itempr');
			localStorage.removeItem('checked-all-itempr');
			$translatePartialLoader.addPart("purchase-requisition");
			getUserLogin();
		}

		vm.getUserLogin = getUserLogin;
		function getUserLogin() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			PurchReqService.getUserLogin({
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					for (var i = 0; i < data.length; i++) {
						if (data[i] == 'R0') {
							vm.flagButton = false;
							jLoad(vm.currentPage);
							break;
						}
					}
				} else {
					//UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				// UIControlService.msg_growl("error", "Gagal akses API");
				UIControlService.unloadLoading();
			});
		}

		vm.changedFilter = changedFilter
		function changedFilter() {
			getApprovalData(1);
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.PRList = [];
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = current;
			var offset = (current * 10) - 10;

			PurchReqService.selectDataPR({
				Offset: offset,
				Limit: vm.maxSize,
				Keyword: vm.textKeyword,
				column: Number(vm.colKeyword),
				Status: vm.columnApprovalStatus
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.PRList = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					//UIControlService.msg_growl("error", "Gagal Mendapatkan Data Freight");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//UIControlService.msg_growl("error", "Gagal akses API");
				UIControlService.unloadLoading();
			});
		}

		vm.menujuApproval = menujuApproval;
		function menujuApproval() {
			$state.transitionTo('rfqGoods-draft-approval');
		}

		vm.formViewRFQ = formViewRFQ;
		function formViewRFQ(ID) {
			$state.transitionTo('purchase-requisition-formviewrfq', { RFQID: ID, flag: 0 });
		}

		vm.formInputRFQ = formInputRFQ;
		function formInputRFQ(ID) {
			$state.transitionTo('purchase-requisition-formrfq', { RFQID: ID, flag: 1 });
		}

		vm.detailRFQ = detailRFQ;
		function detailRFQ(ID) {
			$state.transitionTo('purchase-requisition-detailrfq', { RFQID: ID });
		}

		vm.formPROutStanding = formPROutStanding;
		function formPROutStanding() {
			$state.transitionTo('purcreq-pr-outstanding');
		}

		vm.getApprovalData = getApprovalData;
		function getApprovalData(current) {
			vm.currentPage = current;
			$translatePartialLoader.addPart("purchase-requisition");
			//UIControlService.loadLoading('LOADING.GETRFQ.MESSAGE');
			PurchReqService.getApprovalData({
				Offset: (vm.currentPage * vm.maxSize) - vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword,
				Column: vm.column,
				Status: vm.columnApprovalStatus
			}, function (reply) {
				if (reply.status === 200) {
					vm.rfqGoodsApprvls = [];
					vm.cekApproval = true;
					vm.rfqGoodsApprvls = reply.data.List;
					vm.totalItemsApprvls = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					//UIControlService.msg_growl("error", 'NOTIFICATION.GETRFQ.ERROR', "NOTIFICATION.GETRFQ.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				//UIControlService.msg_growl("error", 'NOTIFICATION.GETRFQ.ERROR', "NOTIFICATION.GETRFQ.TITLE");
			});
		}

		/* tambah vendor */
		vm.openFormDokumen = openFormDokumen;
		function openFormDokumen(isAdd, data) {
			var senddata = { data: data, isAdd: isAdd };
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataRFQ/uploadDokumen.html',
				controller: 'UploadDokumenCtrl',
				controllerAs: 'uploadDokCtrl',
			    backdrop: 'static',
				resolve: {
					item: function () {
						return senddata;
					}
				}
			});
			modalInstance.result.then(function (dataVendor) {
				vm.jLoad(1);
			});
		}

		//send approval
		//vm.sendApproval = sendApproval;
		//function sendApproval(data) {
		//	vm.rfqSelect = data;
		//	bootbox.confirm($filter('translate')('SEND_FOR_APPROVAL'), function (yes) {
		//		if (yes) {
		//			UIControlService.loadLoading("Loading. . .");
		//			PurchReqService.sendApproval({ ID: data.ID }, function (reply) {
		//				UIControlService.unloadLoading();
		//				if (reply.status === 200) {
		//					UIControlService.msg_growl("success", "SUCC_SEND_APPROVAL");
		//					sendMailRFQ(0);
		//					vm.jLoad(1);
		//				}
		//				else {
		//					// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
		//					return;
		//				}
		//			}, function (err) {
		//				if (err[0] == 'ERROR.NO_ITEMPR') {
		//					UIControlService.msg_growl("error", "ERROR.NO_ITEMPR");
		//				}
		//				//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		//				UIControlService.unloadLoading();
		//			});
		//		}
		//	});
		//}

		vm.sendMailRFQ = sendMailRFQ;
		function sendMailRFQ(flag) {
			PurchReqService.sendMailRFQGoods({ Status: vm.rfqSelect.ID, FilterType: flag }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "EMAIL_SENT");
				}
				else {
					// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.CancelRFQ = CancelRFQ;
		function CancelRFQ(data) {
			bootbox.prompt({
				title: $filter('translate')('CONFIRM_CANCEL_RFQ'),
				buttons: {
					confirm: { label: "Cancel RFQ!", className: 'btn-danger' },
					cancel: { label: "Close" }
				},
				callback: function (res) {
					if (res != null) {
						UIControlService.loadLoading('LOADING.MESSAGE');
						PurchReqService.cancelRFQ({ Remark: res, ID: data.ID }, function (reply) {
							if (reply.status === 200) {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("notice", 'NOTIFICATION.CANCELRFQ.SUCCESS');
								getUserLogin();
							} else {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", 'NOTIFICATION.CANCELRFQ.ERROR');
							}
						}, function (err) {
							if (err[0] == 'ERROR.CANCEL_REASON') {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", 'NOTIFICATION.CANCELRFQ.ERROR_REASON');
							} else {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", 'NOTIFICATION.CANCELRFQ.ERROR');
							}
						});
					}
				}
			});
		}

		function checkweekday(param, duration) {
			var dat = new Date(param);
			if (duration == 0) {
				dat.setDate(dat.getDate() + 0);
			}
			else {
				for (var i = 1; i <= duration; i++) {
					dat.setDate(dat.getDate() + parseInt(1));
					var dateTemp = dat;
					var day_dateTemp = dateTemp.getDay();
					if (day_dateTemp === 6) {
						dat.setDate(dat.getDate() + 2);
					}
					else if (day_dateTemp === 0) {
						dat.setDate(dat.getDate() + 1);
					}
					else {
						dat.setDate(dat.getDate() + 0);
					}
				}
			}
			return dat;
		}

		vm.RePublish = RePublish;
		function RePublish(data) {
			vm.IdRFQ = data.ID;
			PurchReqService.Publish({
				ID: data.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listStepTender = reply.data;
					for (var i = 0; i < vm.listStepTender.length; i++) {
						if (i > 0) vm.listStepTender[i].StartDate = UIControlService.getStrDate(checkweekday(vm.listStepTender[i - 1].EndDate, 0));
						vm.listStepTender[i].EndDate = UIControlService.getStrDate(checkweekday(vm.listStepTender[i].StartDate, parseInt(vm.listStepTender[i].Duration)));
						if ((vm.listStepTender.length - 1) == i) {
							PurchReqService.insertStep(vm.listStepTender, function (reply) {
								UIControlService.unloadLoading();
								if (reply.status === 200) {
									UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
									init();
								}
							}, function (err) {
								// UIControlService.msg_growl("error", "MESSAGE.API");
								UIControlService.unloadLoading();
							});
						}
					}
				} else {
					// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoadingModal();
			});
		}

		//send approval
		vm.Publish = Publish;
		function Publish(data) {
			vm.IdRFQ = data.ID;

			bootbox.confirm($filter('translate')('RFQGOODS_PUBLISH_CONFIRM' + ' ' + data.RFQName + 'RFQGOODS_PUBLISH_CONFIRM_KODE' + ' ' + data.RFQCode), function (yes) {
				if (yes) {
					PurchReqService.Publish({
						ID: data.ID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							vm.listStepTender = reply.data;
							for (var i = 0; i < vm.listStepTender.length; i++) {
								if (i > 0) vm.listStepTender[i].StartDate = UIControlService.getStrDate(checkweekday(vm.listStepTender[i - 1].EndDate, 0));
								vm.listStepTender[i].EndDate = UIControlService.getStrDate(checkweekday(vm.listStepTender[i].StartDate, parseInt(vm.listStepTender[i].Duration)));
								if ((vm.listStepTender.length - 1) == i) {
									PurchReqService.insertStep(vm.listStepTender, function (reply) {
										UIControlService.unloadLoading();
										if (reply.status === 200) {
											UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
											sendEmail();
											init();
										}
									}, function (err) {
										// UIControlService.msg_growl("error", "MESSAGE.API");
										UIControlService.unloadLoading();
									});
								}
							}
						} else {
							// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
							return;
						}
					}, function (err) {
						//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
						UIControlService.unloadLoadingModal();
					});
				}
			});
		}


		//vm.DetailApproval = DetailApproval;
		//function DetailApproval(data, flag, idApproval) {
		//	vm.flagApproval = flag;
		//	var data = {
		//		RFQID: data,
		//		Status: flag,
		//		IDApproval: idApproval
		//	}
		//	var modalInstance = $uibModal.open({
		//		templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataRFQ/detailApproval.modal.html',
		//		controller: 'detailApprovalCtrl',
		//		controllerAs: 'detailApprovalCtrl',
		//		resolve: {
		//			item: function () {
		//				return data;
		//			}
		//		}
		//	});
		//	modalInstance.result.then(function (ID, filter, flag) {
		//		if (vm.flagApproval != undefined) {
		//			getApprovalData();
		//			PurchReqService.sendMailRFQGoods({ Status: ID, FilterType: filter, IsApprove: flag }, function (reply) {
		//				UIControlService.unloadLoading();
		//				if (reply.status === 200) {
		//					UIControlService.msg_growl("success", "EMAIL_SENT");
		//				}
		//				else {
		//					return;
		//				}
		//			}, function (err) {
		//				UIControlService.unloadLoadingModal();
		//			});

		//		}
		//	});
		//}

		function loadVendorEmail() {
			PurchReqService.getVendorEmail({
				ID: vm.IdRFQ
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data.List;
					vm.dataEmailAssosiasi = data;
					if (vm.dataEmailAssosiasi.length == 0) {
						loadReviewer();
					}
					for (var i = 0; i < vm.dataEmailAssosiasi.length; i++) {
						vm.EmailAssosiasi.push(vm.dataEmailAssosiasi[i].Email);
						if (i == (vm.dataEmailAssosiasi.length - 1)) {
							sendEmailAssosiasi();
						}
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadDataTender = loadDataTender;
		function loadDataTender() {
			vm.IdGoods = vm.IdRFQ;
			vm.Email = [];
			PurchReqService.getDataGoods({
				ID: vm.IdRFQ
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.dataGoods = data;
					if (vm.dataGoods.Emails != null) {
						if (vm.dataGoods.Emails != "") {
							var dataemail = vm.dataGoods.Emails.split(',');
							console.info(dataemail);
							for (var i = 0; i < dataemail.length; i++) {
								vm.Email.push(dataemail[i]);
							}
						}
					}
					if (vm.dataGoods.contactVendorComm != null) {
						for (var i = 0; i < vm.dataGoods.contactVendorComm.length; i++) {
							vm.EmailVendor.push(vm.dataGoods.contactVendorComm[i]);
							if (i == (vm.dataGoods.contactVendorComm.length - 1)) {
								sendEmail();
							}
						}
					}

					else if (vm.dataGoods.contactVendor.length !== 0) {
						for (var i = 0; i < vm.dataGoods.contactVendor.length; i++) {
							vm.EmailVendor.push(vm.dataGoods.contactVendor[i]);
							if (i == (vm.dataGoods.contactVendor.length - 1)) {
								sendEmail();
							}
						}
					}
					else {
						sendEmail();
					}



				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.sendEmail = sendEmail;
		function sendEmail() {
			var email = {
				EmailContent: 'Pengumuman Tender Barang',
				Id: vm.IdRFQ,
				isHtml: true,
				addresses: [],
				addressesVendor: []
			};

			//UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
			PurchReqService.sendMail(email, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					UIControlService.msg_growl("notice", "EMAIL_SENT");
					loadReviewer();
				}
			}, function (response) {
				//UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
				init();
			});
		}


		vm.sendEmailAssosiasi = sendEmailAssosiasi;
		function sendEmailAssosiasi() {
			var email = {
				EmailContent: 'Pengumuman Tender Barang',
				Id: vm.IdGoods,
				isHtml: true,
				addresses: vm.EmailAssosiasi
			};

			//UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
			PurchReqService.sendMailAssosiasi(email, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					UIControlService.msg_growl("notice", "EMAIL_SENT");
					loadReviewer();
					//init();
					//GetApprovalGoods();
				} else {
					init();
					//UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				//UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
				init();
			});
		}

		vm.loadReviewer = loadReviewer;
		function loadReviewer() {
			vm.EmailReviewer = [];
			PurchReqService.getDataReviewer({
				Status: vm.IdRFQ
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					if (reply.data.Message == "No Reviewer") init();
					else {
						UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT_REVIEWER");
						init();
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.GetApprovalGoods = GetApprovalGoods;
		function GetApprovalGoods() {
			vm.EmailReviewer = [];
			vm.EmailRequestor = [];
			PurchReqService.GetApproval({
				ID: vm.IdGoods
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = reply.data;
					if (vm.list.length != 0) {
						vm.EmailRequestor.push(vm.list[0].requestor.Email);
						for (var i = 0; i < vm.list.length; i++) {
							if (i == 0) vm.NameReviewer = vm.list[i].employee.FullName + ' ' + vm.list[i].employee.SurName;
							else vm.NameReviewer += ', ' + vm.list[i].employee.FullName + ' ' + vm.list[i].employee.SurName;
							vm.EmailReviewer.push(vm.list[i].employee.Email);
							if (i == (vm.list.length - 1)) {
								sendMailCE();
							}
						}
					}
				} else {
					//UIControlService.msg_growl("error", $filter('translate') ('MESSAGE.ERR_LOAD_APPROVERS'));
				}
			}, function (error) {
				UIControlService.unloadLoading();
				// UIControlService.msg_growl("error", $filter('translate') ('MESSAGE.ERR_LOAD_APPROVERS'));
			});
		}

		vm.sendMailCE = sendMailCE;
		function sendMailCE() {
			ContractEngineerService.getMailContent({
				EmailContent: 'Notifikasi Contract Engineer',
				TenderName: vm.dataGoods.RFQName,
			}, function (response) {
				if (response.status == 200) {
					var email = {
						subject: response.data.Subject,
						mailContent: response.data.MailContent,
						isHtml: true,
						addresses: vm.EmailReviewer
					};

					//UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
					ContractEngineerService.sendMail(email, function (response) {
						UIControlService.unloadLoading();
						if (response.status == 200) {
							UIControlService.msg_growl("notice", "EMAIL_SENT");
							sendEmailRequestor();
						} else {
							UIControlService.handleRequestError(response.data);
						}
					}, function (response) {
						UIControlService.handleRequestError(response.data);
						UIControlService.unloadLoading();
					});
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
			});
		}

		vm.sendEmailRequestor = sendEmailRequestor;
		function sendEmailRequestor() {
			ContractEngineerService.getMailContent1({
				EmailContent: 'Notifikasi Requestor pengadaan',
				TenderName: vm.project,
				VendorName: vm.NameReviewer
			}, function (response) {
				if (response.status == 200) {
					var email = {
						subject: response.data.Subject,
						mailContent: response.data.MailContent,
						isHtml: true,
						addresses: vm.EmailRequestor
					};

					//UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
					ContractEngineerService.sendMail(email, function (response) {
						UIControlService.unloadLoading();
						if (response.status == 200) {
							UIControlService.msg_growl("notice", "EMAIL_SENT");
							init();
						} else {
							UIControlService.handleRequestError(response.data);
						}
					}, function (response) {
						UIControlService.handleRequestError(response.data);
						UIControlService.unloadLoading();
					});
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
			});
		}

	}
})();