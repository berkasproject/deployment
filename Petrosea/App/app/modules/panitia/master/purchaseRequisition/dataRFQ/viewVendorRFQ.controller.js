﻿(function () {
	'use strict';

	angular.module('app').controller('viewVendorRFQCtrl', ctrl);

	ctrl.$inject = ['model', 'PurchaseRequisitionService', 'UIControlService', '$uibModalInstance'];

	function ctrl(model, PurchReqService, UIControlService, $uibModalInstance) {
		var vm = this;
		console.info(model);
		vm.vendors = null;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.flag = model.flag;
		vm.listvendors = [];
		vm.init = init;
		vm.listSaveVendor = [];
		vm.flagData = model.flagData;

		function init() {
			UIControlService.loadLoadingModal('LOADING.VIEW_VENDOR');
			if (model.flagData != 2) {
				if (model.IsLocal == true) {
					if (model.getData == undefined) {
						vm.getData = [];
						jloadvendor(1, []);
					} else {
						if (model.getData.length == 0) {
							jloadvendor(1, []);
						} else {
							vm.getData = model.getData;
							vm.listSaveVendor = model.getData;
							jloadvendor(1, model.getData);
						}
					}
				} else {
					if (model.getData == undefined) {
						vm.getData = [];
						jloadvendor(1, []);
					} else {
						if (model.getData.length == 0) {
							vm.getData = [];
							jloadvendor(1, []);
						}
						else {
							vm.getData = model.getData;
							vm.listSaveVendor = model.getData;
							jloadvendor(1, model.getData);
						}
					}
				}
			} else {
				jloadvendorSave(1);
			}
		}

		vm.checkAll = checkAll;
		function checkAll(data) {
			if (data == true) {
				UIControlService.loadLoadingModal("MESSAGE.LOADING");
				PurchReqService.viewVendor({
					CommodityID: model.CommodityID,
					IsLocal: model.IsLocal,
					IsNational: model.IsNational,
					IsInternational: model.IsInternational,
					CompScale: model.CompScale,
					Keyword: vm.keyword,
					Offset: (vm.currentPage * 10) - 10,
					contactVendor: []
				}, function (reply) {
					if (reply.status === 200) {
						vm.vendors = [];
						vm.listSaveVendor = reply.data.List;
						jloadvendor(1, []);
					}
				}, function (err) {
					UIControlService.unloadLoadingModal();
				});
			} else {
				vm.listSaveVendor = [];
				init();
			}
		}



		vm.jloadvendorSave = jloadvendorSave;
		function jloadvendorSave(current) {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			vm.currentPage = current;
			PurchReqService.viewVendorSave({
				ID: model.ID,
				Keyword: vm.keyword,
				Limit: 0,
				Offset: (vm.currentPage * 10) - 10
			}, function (reply) {
				if (reply.status === 200) {
					vm.vendors = reply.data.List;
					vm.totalItems = Number(reply.data.Count);
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
			});
		}

		vm.jloadvendor = jloadvendor;
		function jloadvendor(current, dataSave) {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			vm.currentPage = current;

			if (model.VendorLocationID == null) {
				model.VendorLocationID = {};
				model.VendorLocationID.StateID = null;
			}

			PurchReqService.viewVendor({
				CommodityID: model.CommodityID,
				IsLocal: model.IsLocal,
				IsNational: model.IsNational,
				IsInternational: model.IsInternational,
				CompScale: model.CompScale,
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword,
				contactVendor: dataSave,
				ProjectLocationID: model.ProjectLocationID.StateID,
				VendorLocationID: model.VendorLocationID.StateID
			}, function (reply) {
				if (reply.status === 200) {
					vm.vendors = reply.data.List;
					if (vm.listSaveVendor.length == 0) {

						//vm.vendors.forEach(function (data) {
						//	if (data.IsLocal == 1) vm.listSaveVendor.push(data);
						//});
					} else {
						vm.listSaveVendor.forEach(function (save) {
							vm.vendors.forEach(function (data) {
								if (save.VendorID == data.VendorID)
									data.IsCheck = true;
							});
						});
					}
					vm.totalItems = Number(reply.data.Count);
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
			});
		}


		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
		vm.save = save;
		function save() {
			$uibModalInstance.close(vm.listSaveVendor);
		};

		vm.change = change;
		function change(data) {
			if (data.IsCheck == true) vm.listSaveVendor.push(data);
			for (var i = 0; i < vm.listSaveVendor.length; i++) {
				if (vm.listSaveVendor[i].VendorID == data.VendorID && data.IsCheck == false) {
					vm.listSaveVendor.splice(i, 1);
				}
			}


		}
	}
})();