(function () {
	'use strict';

	angular.module("app")
    .controller("detailApprovalCtrl", ctrl);

	ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'PurchaseRequisitionService'];
	/* @ngInject */
	function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, PurchReqService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.ID = item.RFQID;
		vm.Status = item.Status;
		vm.crApps = [];
		vm.employeeFullName = "";
		vm.employeeID = 0;
		vm.information = "";
		vm.flagEmp = item.flag;
		vm.IDApproval = item.IDApproval;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('purchase-requisition');
			loadData();
		};

		//vm.loadData = loadData;
		//function loadData() {
		//	vm.crApps = [];
		//	UIControlService.loadLoading(loadmsg);
		//	PurchReqService.GetApproval({
		//		ID: vm.ID
		//	}, function (reply) {
		//		UIControlService.unloadLoading();
		//		if (reply.status === 200) {
		//			vm.list = reply.data;
		//			for (var i = 0; i < vm.list.length; i++) {
		//				vm.crApps.push({
		//					IsActive: vm.list[i].IsActive,
		//					ID: vm.list[i].ID,
		//					EmployeeID: vm.list[i].EmployeeID,
		//					ApprovalDate: UIControlService.convertDateTime(vm.list[i].ApprovalDate),
		//					ApprovalStatus: vm.list[i].ApprovalStatus,
		//					Remark: vm.list[i].Remark,
		//					EmployeeFullName: vm.list[i].employee.FullName + ' ' + vm.list[i].employee.SurName,
		//					EmployeePositionName: vm.list[i].employee.PositionName,
		//					EmployeeDepartmentName: vm.list[i].employee.DepartmentName,
		//					LevelInfo: vm.list[i].employee.LevelInfo,
		//					DelegatedEmployeeName: vm.list[i].DelegateEmployeeName,
		//					IsHighPriority: vm.list[i].IsHighPriority
		//				});
		//			}
		//		} else {
		//			$uibModalInstance.close();
		//		}
		//	}, function (error) {
		//		$uibModalInstance.close();
		//		UIControlService.unloadLoading();
		//	});
		//}


		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};

		//send approval
		vm.Approval = Approval;
		function Approval() {
			PurchReqService.Approval({ ID: vm.ID, Remark: vm.information, IDApproval: vm.IDApproval }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_APPROVE");
					$uibModalInstance.close(vm.ID, 1, true);
				} else {
					$uibModalInstance.close();
					return;
				}
			}, function (err) {
				$uibModalInstance.close();
				UIControlService.unloadLoadingModal();
			});
		}

		//send Reject
		vm.Reject = Reject;
		function Reject() {
			PurchReqService.Reject({ ID: vm.ID, Remark: vm.information, IDApproval: vm.IDApproval }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_REJECT");
					$uibModalInstance.close(vm.ID, 1, false);
				} else {
					$uibModalInstance.close();
					return;
				}
			}, function (err) {
				$uibModalInstance.close();
				UIControlService.unloadLoadingModal();
			});
		}
	}
})();