﻿(function () {
    'use strict';

    angular.module("app")
    .controller("ViewDuplicateItemPRCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PurchaseRequisitionService', '$state',
        'UIControlService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PurchReqService, $state, UIControlService,
        item, $uibModalInstance) {
        var vm = this;
        
        vm.init = init;
        function init() {
            vm.duplicateItems = item.duplicateItems;
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();