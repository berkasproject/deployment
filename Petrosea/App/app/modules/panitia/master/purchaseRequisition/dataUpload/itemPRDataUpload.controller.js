(function () {
    'use strict';

    angular.module("app")
    .controller("UploadItemPRCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$filter', '$location', '$uibModal', 'PurchaseRequisitionService', '$state',
        'UIControlService', 'UploadFileConfigService', 'ExcelReaderService', 'UploaderService'];
    function ctrl($http, $translate, $translatePartialLoader, $filter, $location, $uibModal, PurchaseRequisitionService, $state, UIControlService,
        UploadFileConfigService, ExcelReaderService, UploaderService) {
        var vm = this;
        vm.fileUpload;
        vm.selectedSearchBy = 0;
        vm.maxSize = 10;
        vm.currentPage = 1;
        vm.textSearch = "";
        vm.textDate = null;
        vm.listItemPR = [];
        vm.totalItems = 0;
        vm.message = "";
        //vm.isL1 = false;
        vm.isAllowedUploadItem = false;
        vm.Checklist = false;

        vm.init = init;
        function init() {
            vm.Buyer = [];
            vm.ChecklistItemPR = [];
            $translatePartialLoader.addPart("purchase-requisition");
            getUserLogin();
            loadTypeSizeFile();
            vm.jLoad(1);
            getBuyer();
            getDataPlant();
        }

        vm.dataBuyer = [];
        function getBuyer(){
            PurchaseRequisitionService.getBuyer(function (reply) {
                if (reply.status == 200) {
                    vm.dataBuyer = reply.data;
                }
            }, function (err) {
                $.growl.error({ message: "Gagal mendapatkan data Buyer" });
                UIControlService.unloadLoading();
            });
        }

        vm.changeBuyer = changeBuyer;
        function changeBuyer(data) {
            for (var i = 0; i < vm.dataBuyer.length; i++) {
                if (vm.dataBuyer[i].EmployeeID == vm.Buyer) {
                    vm.Buyer = vm.dataBuyer[i].EmployeeID;
                }
            }
        }

        function getUserLogin() {
            /*
            PurchaseRequisitionService.getUserLogin({
            }, function (reply) {
                if (reply.status === 200) {
                    vm.isL1 = false;
                    var data = reply.data;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] === 'L1') {
                            vm.isL1 = true;
                            break;
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
            });
            */

            PurchaseRequisitionService.isAllowedUploadItem(function (reply) {
                vm.isAllowedUploadItem = reply.data;
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
            });
        }

        vm.changeCombo = changeCombo;
        function changeCombo() {
            console.info("by:" + vm.selectedSearchBy);
            if (vm.selectedSearchBy === '0') {
                vm.jLoad(1);
            }
            vm.textSearch = "";
            vm.textDate = null;
        }

        vm.openCalendar = openCalendar;
        vm.isCalendarOpened = [false, false, false, false];
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };
        
        vm.dataPlant = dataPlant;
        function dataPlant(PlantCode) {
            vm.PlantCode = PlantCode;
            if (PlantCode != null || PlantCode != '') {
                console.log(PlantCode);
                vm.textSearch = vm.PlantCode
                vm.selectedSearchBy = '8';
                vm.jLoad(1);
            }
        }

        function getDataPlant() {
            PurchaseRequisitionService.getDataPlant({},function (reply) {
                console.info(reply);
                vm.dataPlantCode = reply.data.List;
                if (vm.dataPlantCode) {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
            
            PurchaseRequisitionService.getDataExcel({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.textSearch,
                Date1: UIControlService.getStrDate(vm.textDate),
                column: vm.selectedSearchBy
            }, function (reply) {
                //console.info("dta:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listItemPR = data.List;
                    //console.info("itemPR:" + JSON.stringify(vm.listItemPR));
                    for (var i = 0; i < vm.listItemPR.length; i++) {
                        vm.listItemPR[i].Checklist = false;
                        if (vm.listItemPR[i].CommodityName != null) {
                            vm.listItemPR[i].commArr = vm.listItemPR[i].CommodityName.split(" ");
                            vm.listItemPR[i].CommodityCode = vm.listItemPR[i].commArr[0] + " " + vm.listItemPR[i].commArr[1];
                            console.info("comCode" + JSON.stringify(vm.listItemPR[i].CommodityCode));
                        }
                        if (vm.listItemPR[i].InputDate === null) { vm.listItemPR[i].InputDate = "-"; }
                        else { vm.listItemPR[i].InputDate = UIControlService.getStrDate(vm.listItemPR[i].InputDate); }

                        if (vm.listItemPR[i].ApprovalDate === null) { vm.listItemPR[i].ApprovalDate = "-"; }
                        else { vm.listItemPR[i].ApprovalDate = UIControlService.getStrDate(vm.listItemPR[i].ApprovalDate); }

                        if (vm.listItemPR[i].DeliveryDate === null) { vm.listItemPR[i].DeliveryDate = "-"; }
                        else { vm.listItemPR[i].DeliveryDate = UIControlService.getStrDate(vm.listItemPR[i].DeliveryDate); }
                    }
                    vm.totalItems = Number(data.Count);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOADDATA");
                UIControlService.unloadLoading();
            });
        }

        function loadTypeSizeFile() {
            //UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.MASTER.ITEMPR", function (response) {
                //UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
                //UIControlService.unloadLoading();
                return;
            });
        }

        /*count of property object*/
        function numAttrs(obj) {
            var count = 0;
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    ++count;
                }
            }
            return count;
        }

        /*start upload */
        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_NOFILE");
                return;
            }
            UIControlService.loadLoading(vm.message);
            ExcelReaderService.readExcel(vm.fileUpload,
                function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        var excelContents = reply.data;
                        var Sheet1 = excelContents[Object.keys(excelContents)[0]]; /*untuk baca nama sheet*/
                        var countproperty = numAttrs(Sheet1[0]);
                        //console.info("excel:" + JSON.stringify(Sheet1[0]) + numAttrs(Sheet1[0]));
                        vm.columns = [];
                        vm.columns['ColChck'] = null; vm.columns['ColInputDate'] = null; vm.columns['ColApprovalDate'] = null; vm.columns['COlDeletionInd'] = null;
                        vm.columns['ColPurchaseReq'] = null; vm.columns['ColRequisnItem'] = null; vm.columns['ColPurchGroup'] = null; vm.columns['ColRequisitioner'] = null; vm.columns['ColName'] = null;
                        vm.columns['ColShortText'] = null; vm.columns['ColMaterial'] = null; vm.columns['ColQuantity'] = null; vm.columns['ColUnitMeasure'] = null; vm.columns['ColTrackingNumber'] = null;
                        vm.columns['ColTotalValue'] = null; vm.columns['ColUrgentNeed'] = null; vm.columns['ColPlant'] = null; vm.columns['ColTypeRequest'] = null; vm.columns['ColPurchasingOrg'] = null;
                        vm.columns['ColItemCategory'] = null; vm.columns['ColMaterialGroup'] = null; vm.columns['ColAgreement'] = null; vm.columns['ColDesiredVendor'] = null; vm.columns['ColFixedVendor'] = null;
                        vm.columns['ColAcctAssgtCat'] = null; vm.columns['ColSupplyingPlant'] = null; vm.columns['ColTime'] = null; vm.columns['ColStorLocation'] = null; vm.columns['ColProcessingStat'] = null;
                        vm.columns['ColAgreementItem'] = null; vm.columns['ColMPNMaterial'] = null; vm.columns['ColDeliveryDate'] = null; vm.columns['ColInfoRecord'] = null; vm.columns['ColFileDocument'] = null;
                        vm.columns['ColPOLongText'] = null; vm.columns['ColIsUsed'] = null; vm.columns['ColManufacturer1'] = null; vm.columns['ColManufacturer2'];
                        vm.columns['ColItemText'] = null; vm.columns['ColMaterialPOText'] = null; vm.columns['ColHeaderNote'] = null;
                        vm.columns['ColItemNote'] = null; vm.columns['ColDeliveryText'] = null; vm.columns['ColNoteForBuyer'] = null; vm.columns['ColMfrNo'] = null; vm.columns['ColCurr'] = null;
                        vm.columns['ColHistoricalCost'] = null; vm.columns['ColEPVCost'] = null;

                        for (var i = 1; i <= countproperty ; i++) {

                            if (!Sheet1[0]['Column' + i]) {
                                continue;
                            }

                            //if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'Chck') { vm.columns['ColChck'] = i; } //???
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PREQ_DATE') { vm.columns['ColInputDate'] = i; } //???
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'REL_DATE') { vm.columns['ColApprovalDate'] = i;  } //???
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'DELETE_IND') { vm.columns['ColDeletionInd'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PR NUMBER') { vm.columns['ColPurchaseReq'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PREQ_ITEM') { vm.columns['ColRequisnItem'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PUR_GROUP') { vm.columns['ColPurchGroup'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'REQUISITIONER') { vm.columns['ColRequisitioner'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'CREATED_BY') { vm.columns['ColName'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'SHORT_TEXT') { vm.columns['ColShortText'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MATERIAL') { vm.columns['ColMaterial'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'QUANTITY') { vm.columns['ColQuantity'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'UNIT') { vm.columns['ColUnitMeasure'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'TRACKINGNO') { vm.columns['ColTrackingNumber'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'VALUE_ITEM') { vm.columns['ColTotalValue'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PRIO_URGENCY') { vm.columns['ColUrgentNeed'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PLANT') { vm.columns['ColPlant'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'DOC_TYPE') { vm.columns['ColTypeRequest'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PURCH_ORG') { vm.columns['ColPurchasingOrg'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'ITEM_CATEGORY') { vm.columns['ColItemCategory'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MATL_GROUP') { vm.columns['ColMaterialGroup'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'DES_VENDOR') { vm.columns['ColDesiredVendor'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'AGREEMENT') { vm.columns['ColAgreement'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'FIXED_VEND') { vm.columns['ColFixedVendor'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'ACCTASSCAT') { vm.columns['ColAcctAssgtCat'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'SUPPL_PLNT') { vm.columns['ColSupplyingPlant'] = i;  }
                            //if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'Time') { vm.columns['ColTime'] = i;  } //???
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'STOR_LOCATION') { vm.columns['ColStorLocation'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'PROC_STAT') { vm.columns['ColProcessingStat'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'AGMT_ITEM') { vm.columns['ColAgreementItem'] = i;  }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MPN_MATERIAL') { vm.columns['ColMPNMaterial'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'DELIV_DATE') { vm.columns['ColDeliveryDate'] = i; }
                            //if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MATERIAL PO TEXT') { vm.columns['ColPOLongText'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'INFO_REC') { vm.columns['ColInfoRecord'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MFR NAME') { vm.columns['ColManufacturer1'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MFR NAME 2') { vm.columns['ColManufacturer2'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'ITEM TEXT') { vm.columns['ColItemText'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MATERIAL PO TEXT') { vm.columns['ColMaterialPOText'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'HEADER NOTE') { vm.columns['ColHeaderNote'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'ITEM NOTE') { vm.columns['ColItemNote'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'DELIVERY TEXT') { vm.columns['ColDeliveryText'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'NOTE FOR BUYER') { vm.columns['ColNoteForBuyer'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'MFR_NO') { vm.columns['ColMfrNo'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'CURRENCY') { vm.columns['ColCurr'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'HISTORICAL_COST') { vm.columns['ColHistoricalCost'] = i; }
                            if (Sheet1[0]['Column' + i].trim().toUpperCase() === 'EPV_COST') { vm.columns['ColEPVCost'] = i; }

                            if (//!(vm.columns['ColChck'] === null) &&
                                !(vm.columns['ColInputDate'] === null) &&
                                !(vm.columns['ColApprovalDate'] === null) &&
                                !(vm.columns['COlDeletionInd'] === null) &&
                                !(vm.columns['ColPurchaseReq'] === null) &&
                                !(vm.columns['ColRequisnItem'] === null) &&
                                !(vm.columns['ColPurchGroup'] === null) &&
                                !(vm.columns['ColRequisitioner'] === null) &&
                                !(vm.columns['ColName'] === null) &&
                                !(vm.columns['ColShortText'] === null) &&
                                !(vm.columns['ColMaterial'] === null) &&
                                !(vm.columns['ColQuantity'] === null) &&
                                !(vm.columns['ColUnitMeasure'] === null) &&
                                !(vm.columns['ColTrackingNumber'] === null) &&
                                !(vm.columns['ColTotalValue'] === null) &&
                                !(vm.columns['ColUrgentNeed'] === null) &&
                                !(vm.columns['ColPlant'] === null) &&
                                !(vm.columns['ColTypeRequest'] === null) &&
                                !(vm.columns['ColPurchasingOrg'] === null) &&
                                !(vm.columns['ColItemCategory'] === null) &&
                                !(vm.columns['ColMaterialGroup'] === null) &&
                                !(vm.columns['ColAgreement'] === null) &&
                                !(vm.columns['ColDesiredVendor'] === null) &&
                                !(vm.columns['ColFixedVendor'] === null) &&
                                !(vm.columns['ColAcctAssgtCat'] === null) &&
                                !(vm.columns['ColSupplyingPlant'] === null) &&
                                //!(vm.columns['ColTime'] === null) &&
                                !(vm.columns['ColStorLocation'] === null) &&
                                !(vm.columns['ColProcessingStat'] === null) &&
                                !(vm.columns['ColAgreementItem'] === null) &&
                                !(vm.columns['ColMPNMaterial'] === null) &&
                                !(vm.columns['ColDeliveryDate'] === null) &&
                                //!(vm.columns['ColPOLongText'] === null) &&
                                !(vm.columns['ColInfoRecord'] === null) &&
                                !(vm.columns['ColManufacturer1'] === null) &&
                                !(vm.columns['ColManufacturer2'] === null) &&
                                !(vm.columns['ColItemText'] === null) &&
                                !(vm.columns['ColMaterialPOText'] === null) &&
                                !(vm.columns['ColHeaderNote'] === null) &&
                                !(vm.columns['ColItemNote'] === null) &&
                                !(vm.columns['ColDeliveryText'] === null) &&
                                !(vm.columns['ColNoteForBuyer'] === null) &&
                                !(vm.columns['ColMfrNo'] === null) &&
                                !(vm.columns['ColCurr'] === null) &&
                                !(vm.columns['ColHistoricalCost'] === null) &&
                                !(vm.columns['ColEPVCost'] === null)
                                ) {
                                break;
                            }
                        }
                        //cek jika ada kolom excel tidak sesuai format
                        if (//(vm.columns['ColChck'] === null) ||
                            //(vm.columns['ColInputDate'] === null) ||
                            //(vm.columns['ColApprovalDate'] === null) ||
                            //(vm.columns['ColDeletionInd'] === null) ||
                            (vm.columns['ColPurchaseReq'] === null) ||
                            (vm.columns['ColRequisnItem'] === null) ||
                            //(vm.columns['ColPurchGroup'] === null) ||
                            (vm.columns['ColRequisitioner'] === null) ||
                            //(vm.columns['ColName'] === null) ||
                            (vm.columns['ColShortText'] === null) ||
                            (vm.columns['ColMaterial'] === null) ||
                            (vm.columns['ColQuantity'] === null) ||
                            (vm.columns['ColUnitMeasure'] === null) ||
                            (vm.columns['ColTrackingNumber'] === null) ||
                            (vm.columns['ColTotalValue'] === null) ||
                            //(vm.columns['ColUrgentNeed'] === null) ||
                            (vm.columns['ColPlant'] === null) ||
                            //(vm.columns['ColTypeRequest'] === null) ||
                            (vm.columns['ColPurchasingOrg'] === null) ||
                            (vm.columns['ColItemCategory'] === null) ||
                            //(vm.columns['ColMaterialGroup'] === null) ||
                            //(vm.columns['ColAgreement'] === null) ||
                            //(vm.columns['ColDesiredVendor'] === null) ||
                            //(vm.columns['ColFixedVendor'] === null) ||
                            //(vm.columns['ColAcctAssgtCat'] === null) ||
                            //(vm.columns['ColSupplyingPlant'] === null) ||
                            //(vm.columns['ColTime'] === null) ||
                            (vm.columns['ColStorLocation'] === null) ||
                            //(vm.columns['ColProcessingStat'] === null) ||
                            //(vm.columns['ColAgreementItem'] === null) ||
                            (vm.columns['ColMPNMaterial'] === null) ||
                            (vm.columns['ColDeliveryDate'] === null) ||
                            //(vm.columns['ColPOLongText'] === null) ||
                            //(vm.columns['ColInfoRecord'] === null) ||
                            //(vm.columns['ColManufacturer1'] === null) ||
                            //(vm.columns['ColManufacturer2'] === null) ||
                            //(vm.columns['ColItemText'] === null) ||
                            //(vm.columns['ColMaterialPOText'] === null) ||
                            //(vm.columns['ColHeaderNote'] === null) ||
                            //(vm.columns['ColItemNote'] === null) ||
                            //(vm.columns['ColDeliveryText'] === null) ||
                            //(vm.columns['ColNoteForBuyer'] === null) ||
                            //(vm.columns['ColMfrNo'] === null) ||
                            //(vm.columns['ColHistoricalCost'] === null) ||
                            //(vm.columns['ColEPVCost'] === null)
                            (vm.columns['ColCurr'] === null)
                            ) {
                            UIControlService.msg_growl("warning", "ITEMPRUPLOAD.MSG_ERR_EXCEL");
                            return;
                        };
                        vm.newExcel = []
                        for (var a = 1; a < Sheet1.length; a++) {
                            /*
                            var isused;
                            var material = Sheet1[a]['Column' + vm.ColMaterial];
                            if (material) {
                                isused = true;
                            } else {
                                isused = false;
                            }
                            */

                            if (!Sheet1[a]['Column' + vm.columns['ColPurchaseReq']]) { //Data Kosong
                                continue;
                            }

                            //cek PGRCode
                            //if (!Sheet1[a]['Column' + vm.columns['ColPurchGroup']]) {
                            //    UIControlService.msg_growl("warning", "ITEMPRUPLOAD.MSG_ERR_EXCEL_NO_PGR");
                            //    return;
                            //}

                            //convert date input
                            var dateinput = Sheet1[a]['Column' + vm.columns['ColInputDate']];
                            var dateapproval = Sheet1[a]['Column' + vm.columns['ColApprovalDate']];
                            var datedelivery = Sheet1[a]['Column' + vm.columns['ColDeliveryDate']];
                           
                            if (!(dateinput === null) && !(dateinput === undefined)) {
                                if (Number.isInteger(dateinput)) {
                                    var date = new Date(1900, 0, 1);
                                    date.setDate(date.getDate() + dateinput - 2);
                                    dateinput = UIControlService.getStrDate(date);
                                } else {
                                    dateinput = UIControlService.convertDateFromExcel(dateinput);
                                }
                            }
                            else { dateinput = null; }

                            //convert approval date
                            if (!(dateapproval === null) && !(dateapproval === undefined)) {
                                if (Number.isInteger(dateapproval)) {
                                    var date = new Date(1900, 0, 1);
                                    date.setDate(date.getDate() + dateapproval - 2);
                                    dateapproval = UIControlService.getStrDate(date);
                                } else {
                                    dateapproval = UIControlService.convertDateFromExcel(dateapproval);
                                }
                            }
                            else { dateapproval = null; }

                            //convert  delivery date
                            if (!(datedelivery === null) && !(datedelivery === undefined)) {
                                if (Number.isInteger(datedelivery)) {
                                    var date = new Date(1900, 0, 1);
                                    date.setDate(date.getDate() + datedelivery - 2);
                                    datedelivery = UIControlService.getStrDate(date);
                                } else {
                                    datedelivery = UIControlService.convertDateFromExcel(datedelivery);
                                }
                            }
                            else { datedelivery = null; }
                            //console.info(Sheet1[a]['Column' + vm.ColInputDate] + UIControlService.getStrDate(Sheet1[a]['Column' + vm.ColInputDate]));
                            var objExcel = {
                                Chck: null,//Sheet1[a]['Column' + vm.columns['ColChck']],
                                InputDate: dateinput,
                                ApprovalDate: dateapproval,
                                DeletionInd: Sheet1[a]['Column' + vm.columns['ColDeletionInd']],
                                PurchaseReq: Sheet1[a]['Column' + vm.columns['ColPurchaseReq']],
                                RequisnItem: Sheet1[a]['Column' + vm.columns['ColRequisnItem']],
                                PurchGroup: Sheet1[a]['Column' + vm.columns['ColPurchGroup']],
                                Requisitioner: Sheet1[a]['Column' + vm.columns['ColRequisitioner']],
                                Name: Sheet1[a]['Column' + vm.columns['ColName']],
                                ShortText: Sheet1[a]['Column' + vm.columns['ColShortText']],
                                Material: Number(Sheet1[a]['Column' + vm.columns['ColMaterial']]),
                                Quantity: Math.round(Number(Sheet1[a]['Column' + vm.columns['ColQuantity']])),
                                UnitMeasure: Sheet1[a]['Column' + vm.columns['ColUnitMeasure']],
                                TrackingNumber: Sheet1[a]['Column' + vm.columns['ColTrackingNumber']],
                                TotalValue: Sheet1[a]['Column' + vm.columns['ColTotalValue']],
                                UrgentNeed: Sheet1[a]['Column' + vm.columns['ColUrgentNeed']],
                                Plant: Sheet1[a]['Column' + vm.columns['ColPlant']],
                                TypeRequest: Sheet1[a]['Column' + vm.columns['ColTypeRequest']],
                                PurchasingOrg: Sheet1[a]['Column' + vm.columns['ColPurchasingOrg']],
                                ItemCategory: Sheet1[a]['Column' + vm.columns['ColItemCategory']],
                                MaterialGroup: Sheet1[a]['Column' + vm.columns['ColMaterialGroup']],
                                Agreement: Sheet1[a]['Column' + vm.columns['ColAgreement']],
                                DesiredVendor: Sheet1[a]['Column' + vm.columns['ColDesiredVendor']],
                                FixedVendor: Sheet1[a]['Column' + vm.columns['ColFixedVendor']],
                                AcctAssgtCat: Sheet1[a]['Column' + vm.columns['ColAcctAssgtCat']],
                                SupplyingPlant: Sheet1[a]['Column' + vm.columns['ColSupplyingPlant']],
                                Time: null,//new Date(Sheet1[a]['Column' + vm.columns['ColTime']]),
                                StorLocation: Sheet1[a]['Column' + vm.columns['ColStorLocation']],
                                ProcessingStat: Sheet1[a]['Column' + vm.columns['ColProcessingStat']],
                                AgreementItem: Number(Sheet1[a]['Column' + vm.columns['ColAgreementItem']]),
                                MPNMaterial: Sheet1[a]['Column' + vm.columns['ColMPNMaterial']],
                                DeliveryDate: datedelivery,
                                InfoRecord: Sheet1[a]['Column' + vm.columns['ColInfoRecord']],
                                POLongText: Sheet1[a]['Column' + vm.columns['ColPOLongText']],
                                Manufacturer1: Sheet1[a]['Column' + vm.columns['ColManufacturer1']],
                                Manufacturer2: Sheet1[a]['Column' + vm.columns['ColManufacturer2']],
                                ItemText: Sheet1[a]['Column' + vm.columns['ColItemText']],
                                MaterialPOText: Sheet1[a]['Column' + vm.columns['ColMaterialPOText']],
                                HeaderNote: Sheet1[a]['Column' + vm.columns['ColHeaderNote']],
                                ItemNote: Sheet1[a]['Column' + vm.columns['ColItemNote']],
                                DeliveryText: Sheet1[a]['Column' + vm.columns['ColDeliveryText']],
                                NoteForBuyer: Sheet1[a]['Column' + vm.columns['ColNoteForBuyer']],
                                MfrNo: Sheet1[a]['Column' + vm.columns['ColMfrNo']],
                                Currency: Sheet1[a]['Column' + vm.columns['ColCurr']],
                                UnitCostHistorical: Number(Sheet1[a]['Column' + vm.columns['ColHistoricalCost']]),
                                UnitCostEPV: Number(Sheet1[a]['Column' + vm.columns['ColEPVCost']]),
                                FileDocument: '',
                                IsUpload: true
                            }
                            vm.newExcel.push(objExcel);
                        }
                        console.info(vm.newExcel);
                        uploadSave(vm.newExcel);
                    }
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", error.data.ExceptionMessage);
                });
        }

        vm.tglSekarang = UIControlService.getDateNow("");
        function uploadSave(data) {
            UIControlService.loadLoading(vm.message);
            PurchaseRequisitionService.insertUploadExcel(data, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "ITEMPRUPLOAD.MSG_SUC_SAVE");

                    vm.duplicateItems = [];
                    var duplicateItems = reply.data;
                    if (duplicateItems.length > 0) {
                        UIControlService.msg_growl("warning", duplicateItems.length + " item tidak dapat diupload karena sudah terdapat item lain dengan PR dan Req. Number yang sama");
                        vm.duplicateItems = duplicateItems;
                    }
                    //if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                    //    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
                    //}
                    //$state.transitionTo('master-rate');
                    vm.jLoad(1);
                }
                else {
                    UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_SAVE");
                UIControlService.unloadLoading();
            });
        }

        vm.hapusItemPR = hapusItemPR;
        function hapusItemPR(id) {
            swal({
                icon: "warning",
                text: $filter('translate')('ITEMPRUPLOAD.CONFIRM_DEL'),
                buttons: {
                    cancel: {
                        text: "Batal",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                        className: "red-bg",
                    },
                    confirm: {
                        text: "Hapus Data",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    },
                },
                closeOnClickOutside: false
            }).then((value) => {
                if (value) {
                    UIControlService.loadLoading(vm.message);
                    PurchaseRequisitionService.deleteItemPR({
                        ID: id,
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "ITEMPRUPLOAD.MSG_SUC_DEL");
                            vm.jLoad(vm.currentPage);
                        }
                        else {
                            UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_DEL");
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_DEL");
                        UIControlService.unloadLoading();
                    });
                }
            });
            //bootbox.confirm($filter('translate')('ITEMPRUPLOAD.CONFIRM_DEL'), function (yes) {
            //    if (yes) {
            //        UIControlService.loadLoading(vm.message);
            //        PurchaseRequisitionService.deleteItemPR({
            //            ID: id,
            //        }, function (reply) {
            //            UIControlService.unloadLoading();
            //            if (reply.status === 200) {
            //                UIControlService.msg_growl("success", "ITEMPRUPLOAD.MSG_SUC_DEL");
            //                vm.jLoad(vm.currentPage);
            //            }
            //            else {
            //                UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_DEL");
            //            }
            //        }, function (err) {
            //            UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_DEL");
            //            UIControlService.unloadLoading();
            //        });
            //    }
            //});
        }

        vm.viewDuplicates = viewDuplicates;
        function viewDuplicates() {
            var data = {
                duplicateItems: vm.duplicateItems
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/purchaseRequisition/dataUpload/viewDuplicates.modal.html',
                controller: 'ViewDuplicateItemPRCtrl',
                controllerAs: 'vDupCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (dataitem) {
            });
        }

        //Masukkin data item PR ke Variabel
        vm.ChecklistItemPR = [];
        vm.CheckItemPR = CheckItemPR;
        function CheckItemPR(itemPR, flag) {
            vm.data = [];
            vm.data[0] = itemPR;
            if (flag == true) {
                for (var i = 0; i < vm.data.length; i++) {
                    vm.ChecklistItemPR.push(vm.data[i]);
                }
                console.log(vm.ChecklistItemPR);
            }
            else if (flag == false) {
                for (var i = 0; i < vm.ChecklistItemPR.length; i++) {
                    if (vm.ChecklistItemPR[i].ID == vm.data[0].ID) {
                        vm.ChecklistItemPR.splice(i, 1);
                    }
                }
                console.log(vm.ChecklistItemPR);
            }
        }

        vm.UpdateRFQBuyer = UpdateRFQBuyer;
        function UpdateRFQBuyer() {
            UIControlService.loadLoading();
            if (vm.ChecklistItemPR.length == 0 || vm.ChecklistItemPR == null) {
                UIControlService.msg_growl("error", "ITEMPRUPLOAD.CHECKLIST_NULL");
                UIControlService.unloadLoading();
            }
            else if (vm.Buyer.length == 0 || vm.Buyer == null) {
                UIControlService.msg_growl("error", "ITEMPRUPLOAD.BUYER_NULL");
                UIControlService.unloadLoading();
            } else {
                PurchaseRequisitionService.UpdateRFQBuyer({
                    PICEmployeeID: vm.Buyer,
                    ObjectItemPr: vm.ChecklistItemPR
                }, function (reply) {
                    if (reply.status == 200) {
                        UIControlService.msg_growl("success", "ITEMPRUPLOAD.SUCCESS_UPDATEBUYER");
                        UIControlService.unloadLoading();
                        init();
                    } else {
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                });
            }
        }

        /*
        function upload(file, config, filters, dates, callback) {
            //console.info(file);
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ITEMPR",size, filters, dates,
                function (response) {
                    console.info("upload:" + JSON.stringify(response));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = vm.folderFile + url;
                        UIControlService.msg_growl("success", "ITEMPRUPLOAD.MSG_SUC_UPLOADE");

                    } else {
                        UIControlService.msg_growl("error", "ITEMPRUPLOAD.MSG_ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

        }
        */
    }
})();