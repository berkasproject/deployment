﻿(function () {
	'use strict';

	angular.module("app").controller("mstPaymentTerm", ctrl);

	ctrl.$inject = ['UIControlService', 'MstPaymentTermService', '$uibModal', '$translatePartialLoader'];

	function ctrl(UIControlService, MstPaymentTermService, $uibModal, $translatePartialLoader) {
		var vm = this;
		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.column = 1;
		vm.keyword = '';

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('master-paymentTerm');
			selectPaymentTerm();
		}

		vm.ActivateInactivate = ActivateInactivate
		function ActivateInactivate(id) {
			UIControlService.loadLoading('LOADING.SAVEPAYMENTTERM.MESSAGE');
			MstPaymentTermService.ActivateInactivate({
				Id: id
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", 'NOTIFICATION.SAVEPAYMENTTERM.SUCCESS');
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.SAVEPAYMENTTERM.ERROR');
				}
				init();
			}, function (err) {
				if (err == 'ERR_IS_IN_USE') {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("warning", 'ERR_IS_IN_USE');
					return;
				}

				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.SAVEPAYMENTTERM.ERROR');
				init();
			});
		}

		vm.editPaymentTerm = editPaymentTerm;
		function editPaymentTerm(id) {
			var item = {
				ID: id
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/PaymentTerm/paymentTermAdd.html',
				controller: 'addPaymentTerm',
				controllerAs: 'addPaymentTerm',
				resolve: { item: function () { return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		function selectPaymentTerm() {
			UIControlService.loadLoading('LOADING.GETPAYMENTTERMS.MESSAGE');
			MstPaymentTermService.selectPaymentTerm({
				Limit: vm.maxSize,
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Column: vm.column,
				Keyword: vm.keyword,
			}, function (reply) {
				if (reply.status === 200) {
					vm.paymentTerms = reply.data.List;
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETPAYMENTTERMS.ERROR');
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETPAYMENTTERMS.ERROR');
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			init();
		}
	}
})();