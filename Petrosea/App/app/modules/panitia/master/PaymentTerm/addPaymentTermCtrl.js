﻿(function () {
	'use strict';

	angular.module("app").controller("addPaymentTerm", ctrl);

	ctrl.$inject = ['item', 'UIControlService', 'MstPaymentTermService', '$uibModalInstance'];

	function ctrl(item, UIControlService, MstPaymentTermService, $uibModalInstance) {
		var vm = this;
		vm.Id = item.ID

		vm.init = init;
		function init() {
			getPaymentTerm();
		}

		function getPaymentTerm() {
			UIControlService.loadLoading('LOADING.GETPAYMENTTERM.MESSAGE');
			MstPaymentTermService.getPaymentTerm({
				Id: vm.Id
			}, function (reply) {
				if (reply.status === 200) {
					vm.paymentTerm = reply.data;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETPAYMENTTERMS.ERROR');
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETPAYMENTTERMS.ERROR');
			});
		}

		vm.cancel = cancel
		function cancel() {
			$uibModalInstance.close();
		}

		vm.savePaymentTerm = savePaymentTerm
		function savePaymentTerm() {
			UIControlService.loadLoading('LOADING.SAVEPAYMENTTERM.MESSAGE');
			MstPaymentTermService.savePaymentTerm({
				Id: vm.paymentTerm.Id,
				Code: vm.paymentTerm.Code,
				Name: vm.paymentTerm.Name
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", 'NOTIFICATION.SAVEPAYMENTTERM.SUCCESS');
					$uibModalInstance.close();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.SAVEPAYMENTTERM.ERROR');
				}
			}, function (err) {
				if (err == 'ERR_CODE_DUPLICATE') {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("warning", 'ERR_CODE_DUPLICATE');
					return;
				}

				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.SAVEPAYMENTTERM.ERROR');
			});
		}
	}
})()