﻿(function () {
	'use strict'

	angular.module("app").controller("addLicenseCtrl", ctrl)

	ctrl.$inject = ['$uibModal', 'UIControlService', '$translatePartialLoader', 'mstLicenseService', '$uibModalInstance', 'item']
	function ctrl($uibModal, UIControlService, $translatePartialLoader, mstLicenseService, $uibModalInstance, item) {
		var vm = this
		vm.Name = item.Name
		vm.ID = item.ID
		if (vm.ID !== 0) {
		    vm.formName = 'EDIT_LICENSE';
		} else {
		    vm.formName = 'ADD_LICENSE';
		}
		
	    vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.close();
		}

		vm.init = init
		function init() {
		    $translatePartialLoader.addPart('master-license')
		}

		vm.saveLicense = saveLicense
		function saveLicense() {
			if (vm.Name == null || vm.Name == null) {
				UIControlService.msg_growl("warning", "MSG.WARN_LICENSENAME")
				return
			}

			if (item.ID !== 0) {
				mstLicenseService.editLicense({
					Name: vm.Name,
					ID: vm.ID
				}, function (reply) {
					UIControlService.unloadLoading()
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MSG.SAVE_SUCCESS")
						$uibModalInstance.close()
					} else {
						UIControlService.msg_growl("error", "MSG.SAVE_FAILED")
						UIControlService.unloadLoading()
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MSG.SAVE_FAILED")
					UIControlService.unloadLoading()
				})
			}
			else {
				mstLicenseService.saveLicense({
					Name: vm.Name,
				}, function (reply) {
					UIControlService.unloadLoading()
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MSG.SAVE_SUCCESS")
						$uibModalInstance.close()
					} else {
						UIControlService.msg_growl("error", "MSG.SAVE_FAILED")
						UIControlService.unloadLoading()
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MSG.SAVE_FAILED")
					UIControlService.unloadLoading()
				})
			}
		}
	}
})()