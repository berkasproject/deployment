﻿(function() {
    'use strict';

    angular.module("app").controller("OtherCostController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'OtherCostService',
         'GlobalConstantService', 'UIControlService', '$uibModal', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, OtherCostService,
         GlobalConstantService, UIControlService, $uibModal, $filter) {

        var vm = this;
        vm.listAllData = [];
        vm.txtSearch = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('other-cost');
            jLoad(1);
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("");
            vm.currentPage = current;
            var offset = vm.maxSize * (current - 1);
            OtherCostService.Select({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.txtSearch
            }, function (reply) {
                //console.info("data_asoc:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                var data = reply.data;
                vm.listAllData = data.List;
                vm.totalItems = data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
                UIControlService.unloadLoading();
            });
        }

        vm.searchData = searchData;
        function searchData() {
            jLoad(1);
        }

        vm.forminput = forminput;
        function forminput(data) {
            var data = data ? {
                ID: data.ID
            } : {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/other-cost/otherCostForm.modal.html',
                controller: 'OtherCostModalCtrl',
                controllerAs: 'ocMCtrl',
                resolve: {
                    item: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function() {
                vm.jLoad(vm.currentPage);
            });
        };

        vm.remove = remove;
        function remove(dt) {
            bootbox.confirm($filter("translate")("MESSAGE.CONF_DELETE_OTHER_COST"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    OtherCostService.DeleteOtherCost({
                        ID: dt.ID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", "MESSAGE.SUCC_DELETE_OTHER_COST");
                        vm.jLoad(vm.currentPage);
                    }, function (err) {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_DELETE_OTHER_COST");
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
    }
})();
