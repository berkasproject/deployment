﻿(function () {
	'use strict'

	angular.module("app").controller("addDelegationCtrl", ctrl)

	ctrl.$inject = ['$uibModal', 'UIControlService', 'delegationService', '$uibModalInstance']
	function ctrl($uibModal, UIControlService, delegationService, $uibModalInstance) {
		var vm = this
		vm.isCalendarOpened = [false, false, false, false]

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.close();
		}

		vm.init = init
		function init() {
			delegationService.IsAdmin({}, function (reply) {
				UIControlService.unloadLoading()
				if (reply.status === 200) {
					vm.isAdmin = reply.data
					if (vm.isAdmin == false) {
						delegationService.GetEmpData({}, function (reply2) {
							UIControlService.unloadLoading()
							if (reply2.status === 200) {
								vm.datapegawai = reply2.data
							} else {
								UIControlService.msg_growl("error", "MESSAGE.GETROLE_FAILED")
								UIControlService.unloadLoading()
							}
						}, function (err) {
							UIControlService.msg_growl("error", "MESSAGE.GETROLE_FAILED")
							UIControlService.unloadLoading()
						})
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETROLE_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.GETROLE_FAILED")
				UIControlService.unloadLoading()
			})
		}

		vm.addDelegationTo = addDelegationTo
		function addDelegationTo() {
			var data = {
				act: false
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/temporary/susunan-contract-engineer/form-commite-employee.html',
				controller: 'FormCommitteeEmployeeCtrl',
				controllerAs: 'FormCommitteeEmployeeCtrl',
				resolve: {
					item: function () {
						return data
					}
				}
			})
			modalInstance.result.then(function (dataitem) {
				vm.datapegawaiTo = dataitem
			})
		}

		vm.verifyEndDate = verifyEndDate;
		function verifyEndDate(selectedEndDate, selectedStartDate) {
			var convertedEndDate = UIControlService.getStrDate(selectedEndDate)
			var convertedStartDate = UIControlService.getStrDate(selectedStartDate)

			if (selectedStartDate == null) {
				return
			}

			if (convertedEndDate < convertedStartDate) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_DEADLINE")
				vm.EndDate = " "
			}
		}

		vm.addDelegationFrom = addDelegationFrom
		function addDelegationFrom() {
			var data = {
				act: false
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/temporary/susunan-contract-engineer/form-commite-employee.html',
				controller: 'FormCommitteeEmployeeCtrl',
				controllerAs: 'FormCommitteeEmployeeCtrl',
				resolve: {
					item: function () {
						return data
					}
				}
			});
			modalInstance.result.then(function (dataitem) {
				vm.datapegawai = dataitem
			})
		}

		vm.saveDelegation = saveDelegation
		function saveDelegation() {
			if (vm.datapegawai == null || vm.datapegawaiTo == null) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_SELECTEMPLOYEE")
				return
			}

			if (vm.StartDate == null || vm.EndDate == null) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_SELECTDATE")
				return
			}

			if (vm.datapegawai.employeeID == vm.datapegawaiTo.employeeID) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_SAMEEMPLOYEE")
				return
			}

			delegationService.saveDelegation({
				DelegationFrom: vm.datapegawai.employeeID,
				DelegationTo: vm.datapegawaiTo.employeeID,
				StartDate: vm.StartDate,
				FinishDate: vm.EndDate
			}, function (reply) {
				UIControlService.unloadLoading()
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SAVE_SUCCESS")
					$uibModalInstance.close()
				} else {
					UIControlService.msg_growl("error", "MESSAGE.SAVE_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.SAVE_FAILED")
				UIControlService.unloadLoading()
			})
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true
		};
	}
})()