﻿(function () {
	'use strict'

	angular.module("app").controller("addEmployeeCtrl", ctrl)

	ctrl.$inject = ['$uibModal']
	function ctrl($uibModal) {
		var vm = this

		function init() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = 1;
			jLoad(vm.currentPage);
		}

		function jLoad(current) {
			//console.info("curr "+current)
			vm.detailEmployee = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			PurchReqService.selectemployee({
				Offset: offset,
				Limit: vm.pageSize,
				Keyword: vm.searchText
			}, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.detailEmployee = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
	}
})()