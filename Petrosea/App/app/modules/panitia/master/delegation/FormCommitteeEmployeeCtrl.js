﻿(function () {
	'use strict'

	angular.module("app").controller("FormCommitteeEmployeeCtrl", ctrl)

	ctrl.$inject = ['UIControlService', 'delegationService', '$uibModalInstance']
	function ctrl(UIControlService, delegationService, $uibModalInstance) {
		var vm = this
		vm.searchText = ""
		vm.fullSize = 10

		vm.init = init;
		function init() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = 1
			jLoad(vm.currentPage)
		}

		vm.pegawai = pegawai
		function pegawai(data) {
			$uibModalInstance.close(data);
		}

		vm.jLoad = jLoad
		function jLoad(current) {
			//console.info("curr "+current)
			vm.detailEmployee = []
			vm.currentPage = current;
			var offset = (current * 10) - 10
			delegationService.selectemployee({
				Offset: offset,
				Limit: vm.fullSize,
				Keyword: vm.searchText
			}, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading()
				if (reply.status === 200) {
					var data = reply.data
					vm.detailEmployee = data.List
					vm.totalItems = Number(data.Count)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Master Departemen" })
					UIControlService.unloadLoading()
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err))
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading()
			});
		}

		vm.onSearchSubmit = onSearchSubmit
		function onSearchSubmit(searchText) {
			vm.searchText = searchText
			jLoad(vm.currentPage)
		}
	}
})()