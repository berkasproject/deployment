﻿(function () {
	'use strict';

	angular.module("app").controller("BudgetTypeController", ctrl);

	ctrl.$inject = ['$uibModal', 'budgetTypeService', '$translatePartialLoader', 'UIControlService'];

	function ctrl($uibModal, budgetTypeService, $translatePartialLoader, UIControlService) {
		var vm = this
		vm.keyword = '';
		vm.pageSize = 10;
		vm.currentPage = 1;
		$translatePartialLoader.addPart("budget-type");

		vm.addTypes = addTypes
		function addTypes() {
			var item = {
				ID: 0
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/budget-type/budgetTypeModal.html',
				controller: 'budgetTypeModalCtrl',
				controllerAs: 'budgetTypeModalCtrl',
				resolve: { item: function () { return item } }
			});

			modalInstance.result.then(function () {
				loadTypes();
			});
		}

		vm.editType = editType
		function editType(data) {
			var item = {
				ID: data
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/budget-type/budgetTypeModal.html',
				controller: 'budgetTypeModalCtrl',
				controllerAs: 'budgetTypeModalCtrl',
				resolve: { item: function () { return item } }
			});

			modalInstance.result.then(function () {
				loadTypes();
			});
		}

		vm.inactivate = inactivate
		function inactivate(id) {
			budgetTypeService.inactivate({ ID: id.ID }, function (reply) {
			    if (reply.status === 200) {
			        if (id.IsActive) {
			            UIControlService.msg_growl("success", 'MESSAGE.SUCCESS_INACTIVE');
			            loadTypes();
			        } else {
			            UIControlService.msg_growl("success", 'MESSAGE.SUCCESS_ACTIVE');
			            loadTypes();
			        }
				} else {
			        UIControlService.msg_growl("error", 'MESSAGE.FAIL_INACTIVATE');
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", err.Message);
			});
		}

		vm.loadTypes = loadTypes
		function loadTypes() {
			var offset = (vm.currentPage * 10) - 10;

			budgetTypeService.loadTypes({
				Keyword: vm.keyword,
				column: 0,
				Offset: offset,
				Limit: vm.pageSize,
			}, function (reply) {
				if (reply.status === 200) {
					vm.types = reply.data.List;
					vm.totalItems = reply.data.Count;
				} else {

				}
			}, function (err) {

			})
		}
	}
})()