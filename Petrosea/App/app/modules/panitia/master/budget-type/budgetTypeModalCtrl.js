﻿(function () {
	'use strict';

	angular.module("app").controller("budgetTypeModalCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'budgetTypeService', '$translatePartialLoader', '$uibModalInstance', 'item'];

	function ctrl(UIControlService, budgetTypeService, $translatePartialLoader, $uibModalInstance, item) {
		var vm = this

		console.log(item.ID);
		vm.init = init
		function init() {
		    $translatePartialLoader.addPart("budget-type");
		    if (item.ID == 0) {
		        vm.Code = ''
		        vm.formName = 'ADD_BUDGET_TYPE';
		        return
		    } else {
		        vm.formName = 'EDIT_BUDGET_TYPE';
		    }

			budgetTypeService.getByID({ ID: item.ID }, function (reply) {
				if (reply.status === 200) {
					vm.Code = reply.data.Code
				} else {
					UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD', "MESSAGE.NOTIF_ERR_LOAD");
				}
			}, function (err) {
				UIControlService.msg_growl("error", err.Message);
			});
		}

		vm.editCode = editCode
		function editCode() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');

			budgetTypeService.editCode({
				Code: vm.Code,
				ID: item.ID
			}, function (reply) {
				if (reply.status === 200) {
				    UIControlService.msg_growl("success", 'MESSAGE.SAVE_SUCCESS');
				    $uibModalInstance.close();
				    location.reload();
					return
				} else {
				    UIControlService.msg_growl("error", 'MESSAGE.SAVE_FAILED');
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", err.Message);
			});

			$uibModalInstance.close();
		}

		vm.cancel = cancel;
		function cancel() {
		    $uibModalInstance.close();
		}
	}
})()