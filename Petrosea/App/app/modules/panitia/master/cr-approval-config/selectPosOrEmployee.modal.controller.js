﻿(function () {
	'use strict';

	angular.module("app").controller("selPosOrEmpCtrl", ctrl);

	ctrl.$inject = ['item', '$filter', '$uibModalInstance', '$translatePartialLoader', 'CRApprovalConfigService', 'UIControlService', '$uibModal'];
	/* @ngInject */
	function ctrl(item, $filter, $uibModalInstance, $translatePartialLoader, CRApprovalConfigService, UIControlService, $uibModal) {
		var vm = this;
		vm.datapegawai;
		vm.detail = [];
		vm.empNonAct = [];
		vm.crOrPendaftaran = 0;
		vm.item = item;

		

		vm.onBatalClick = function () {
			$uibModalInstance.dismiss('cancel');
		};

		vm.deleteRow = deleteRow;
		function deleteRow(data, index) {
			if (data.EmployeeId != 0) {
				var data = {
					Id: data.EmployeeId,
					IsActive: false
				};
				vm.empNonAct.push(data);
			}

			var idx = index - 1;
			var _length = vm.detail.length; // panjangSemula

			vm.detail.splice(idx, 1);
		}

		vm.onSaveClick = function () {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');

			if (vm.BudgetTypeID == null && vm.crOrPendaftaran == 0) {
				UIControlService.msg_growl("error", 'MESSAGE.BUDGET_TYPE');
				UIControlService.unloadLoadingModal();
				return
			}

			if (vm.detail.length < 1) {
				UIControlService.msg_growl("error", 'MESSAGE.EMPLOYEE');
				UIControlService.unloadLoadingModal();
				return
			}

			CRApprovalConfigService.Save({
				ID: item.ID,
				BudgetTypeID: vm.crOrPendaftaran == 0 ? vm.BudgetTypeID : 0,
				Employees: vm.detail,
                crOrPendaftaran: vm.crOrPendaftaran
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'MESSAGE.SUCC_SAVE');
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE');
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", err.Message);
			});
		}

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('data-contract-requisition');

			if (item.ID != 0) {
			    CRApprovalConfigService.GetById({
			        ID: item.ID
			    }, function (reply) {
			        UIControlService.unloadLoading();
			        var data = reply.data;
			        vm.BudgetTypeID = data.BudgetTypeID;
			        vm.crOrPendaftaran = data.BudgetTypeID == 0 ? 1 : 0;
			        if (vm.crOrPendaftaran == 0) {
			            budgetTypes();
			        }
			        vm.detail = [];

			        for (var i = 0; i < data.Employees.length; i++) {
			            var data1 = {
			                ID: 0,
			                EmployeeName: data.Employees[i].FullName,
			                EmployeePosition: data.Employees[i].PositionName,
			                EmployeeId: data.Employees[i].EmployeeID,
			                LevelInfo: data.Employees[i].LevelInfo,
			                employee: {
			                    ID: data.Employees[i].EmployeeID,
			                    FullName: data.Employees[i].FullName,
			                    PositionName: data.Employees[i].PositionName,
			                    LevelInfo: data.Employees[i].LevelInfo
			                },
			                IsActive: true,
			                email: data.Employees[i].Email
			            }
			            vm.detail.push(data1);
			        }

			    }, function (err) {
			        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
			        UIControlService.unloadLoading();
			    });
			} else {
			    budgetTypes();
			}
		};

		vm.pendaftaranVndr = pendaftaranVndr;
		function pendaftaranVndr() {
		    UIControlService.msg_growl("error", "MESSAGE.PENDAFTARANVENDOR");
		}

		function budgetTypes() {
		    CRApprovalConfigService.getActiveBudgetTypes({ ID: item.ID }, function (reply) {
		        if (reply.status === 200) {
		            vm.listBudgetType = reply.data;
		        } else {
		            UIControlService.msg_growl("error", 'NOTIFICATION.GET.BUDGETTYPES.ERROR', "NOTIFICATION.GET.BUDGETTYPES.TITLE");
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", 'NOTIFICATION.GET.BUDGETTYPES.ERROR', "NOTIFICATION.GET.BUDGETTYPES.TITLE");
		    });
		}

		vm.addEmployee = addEmployee;
		function addEmployee() {
			vm.addEmp = 0;
			vm.ListEmp = [];
			vm.act = true;

			if (vm.detail.length === 0) {
				var data1 = {
					ID: 0,
					EmployeeName: vm.datapegawai.FullName,
					EmployeePosition: vm.datapegawai.PositionName,
					EmployeeId: vm.datapegawai.EmployeeID,
					LevelInfo: vm.datapegawai.LevelInfo,
					employee: {
						ID: vm.datapegawai.EmployeeID,
						FullName: vm.datapegawai.FullName,
						PositionName: vm.datapegawai.PositionName,
						LevelInfo: vm.datapegawai.LevelInfo,
					},
					IsActive: true
				}
				vm.detail.push(data1);
			} else {
				for (var x = 0; x < vm.detail.length; x++) {
					if (vm.detail[x].employee.ID === vm.datapegawai.EmployeeID) {
						vm.addEmp = 1;
						UIControlService.msg_growl("warning", "Pegawai Telah pilih"); return;
					}
				}
				var data1 = {
					ID: 0,
					EmployeeName: vm.datapegawai.FullName,
					EmployeePosition: vm.datapegawai.PositionName,
					EmployeeId: vm.datapegawai.EmployeeID,
					LevelInfo: vm.datapegawai.LevelInfo,
					employee: {
						ID: vm.datapegawai.EmployeeID,
						FullName: vm.datapegawai.FullName,
						PositionName: vm.datapegawai.PositionName,
						LevelInfo: vm.datapegawai.LevelInfo,
					},
					IsActive: true,
					email: vm.datapegawai.Email
				}
				vm.detail.push(data1);
			}
		}

		vm.tambah = tambah;
		function tambah() {
			var data = {
				act: false
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/cr-approval-config/form-commite-employee.html',
				controller: 'FormCommEmpCtrl',
				controllerAs: 'FormCommEmpCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (dataitem) {
				vm.datapegawai = dataitem;
				for (var i = 0; i < vm.datapegawai.length; i++) {
					vm.addPegawai.position = vm.datapegawai[i].position;
					vm.addPegawai.employee = vm.datapegawai[i].employee;
					vm.addPegawai.email = vm.datapegawai[i].employee.Email;
					vm.detail.push(vm.addpegawai[i]);
				}
				console.info(vm.detail);
			});
		}
	}
})();