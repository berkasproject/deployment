﻿(function () {
	'use strict';

	angular.module("app").controller("FormCommEmpCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'CRApprovalConfigService', '$uibModalInstance'];

	/* @ngInject */
	function ctrl(UIControlService, CRApprovalConfigService, $uibModalInstance) {
		var vm = this;
		vm.pageSize = 10;

		vm.init = init;
		function init() {
			UIControlService.loadLoading("Silahkan Tunggu...");
			vm.currentPage = 1;
			jLoad(vm.currentPage);
		}

		vm.onSearchSubmit = onSearchSubmit;
		function onSearchSubmit(searchText) {
			vm.searchText = searchText;
			jLoad(vm.current)
		};

		vm.pegawai = pegawai;
		function pegawai(data) {
			$uibModalInstance.close(data);
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.detailEmployee = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			CRApprovalConfigService.SelectEmployee({
				Offset: offset,
				Limit: vm.pageSize,
				Keyword: vm.searchText
			}, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.detailEmployee = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Master Employee" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.batal = batal;
		function batal() {
		    $uibModalInstance.close('cancel');
		}
	}
})()