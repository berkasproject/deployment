﻿(function () {
	'use strict';

	angular.module("app").controller("CRApprovalConfigController", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'CRApprovalConfigService', 'UIControlService', '$uibModal', '$filter'];
	function ctrl($translatePartialLoader, CRApprovalConfigService, UIControlService, $uibModal, $filter) {

		var vm = this;
		vm.listAllData = [];
		vm.txtSearch = "";
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.maxSize = 10;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('cr-approval-config');
			jLoad(1);
			cekPendaftaranVendor();
		};

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("");
			vm.currentPage = current;
			var offset = vm.maxSize * (current - 1);
			CRApprovalConfigService.Select({
				Offset: offset,
				Limit: vm.maxSize,
				Keyword: vm.txtSearch
			}, function (reply) {
				//console.info("data_asoc:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				var data = reply.data;
				vm.listAllData = data.List;
				vm.totalItems = data.Count;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
				UIControlService.unloadLoading();
			});
		}

		function cekPendaftaranVendor() {
		    CRApprovalConfigService.cekPendaftaranVendor({}, function (reply) {
		        //console.info("data_asoc:" + JSON.stringify(reply));
		        UIControlService.unloadLoading();
		        var data = reply.data;
		        vm.dataPendaftaranVendor = data.Count;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
		        UIControlService.unloadLoading();
		    });
		}

		vm.searchData = searchData;
		function searchData() {
			jLoad(1);
		}

		vm.forminput = forminput;
		function forminput(data) {
			if (data==null) {
				var item = {
				    ID: 0,
                    dataPendaftaranVendor: vm.dataPendaftaranVendor
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/panitia/master/cr-approval-config/selectPosOrEmployee.modal.html',
					controller: 'selPosOrEmpCtrl',
					controllerAs: 'selPosOrEmpCtrl',
					resolve: { item: function () { return item; } }
				});

				modalInstance.result.then(function () {
					vm.init();
				});
			} else {
				var item = {
					ID: data.ID
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/panitia/master/cr-approval-config/selectPosOrEmployee.modal.html',
					controller: 'selPosOrEmpCtrl',
					controllerAs: 'selPosOrEmpCtrl',
					resolve: { item: function () { return item; } }
				});

				modalInstance.result.then(function () {
					vm.init();
				});
			}

			//var data = data ? {
			//	ID: data.ID
			//} : {};
			//var modalInstance = $uibModal.open({
			//	templateUrl: 'app/modules/panitia/master/cr-approval-config/crApprovalConfigForm.modal.html',
			//	controller: 'CRAppConfigModalCtrl',
			//	controllerAs: 'crAppCfgMCtrl',
			//	resolve: {
			//		item: function () {
			//			return data;
			//		}
			//	}
			//});
			//modalInstance.result.then(function () {
			//	vm.jLoad(vm.currentPage);
			//});
		};

		vm.remove = remove;
		function remove(dt) {
			bootbox.confirm($filter("translate")("MESSAGE.CONF_DELETE_CFG"), function (yes) {
				if (yes) {
					UIControlService.loadLoading("");
					CRApprovalConfigService.DeleteCfg({
						ID: dt.ID
					}, function (reply) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("notice", "MESSAGE.SUCC_DELETE_CFG");
						vm.init();
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.ERR_DELETE_CFG");
						UIControlService.unloadLoading();
					});
				}
			});
		}
	}
})();
