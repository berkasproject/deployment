﻿(function () {
    'use strict';

    angular.module("app").controller("QuestTemplateFormCtrl", ctrl);

    ctrl.$inject = ['GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MstQuestTemplateService', 'UIControlService'];
    function ctrl(GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, MstQuestTemplateService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.pageSize = 10;
        vm.init = init;
        vm.QuestionnaireID = Number($stateParams.questId);
        vm.RiskCriteria = {};
        vm.kuesioner = [];
        vm.listDDquestion = [];
        vm.RiskCriteriaID;
        vm.QuestType = "Vendor Assessment Report";
        vm.Status = '';
        vm.IsUseTableLayout = false;
        vm.IsForPrequal = false;
        vm.IsShowNumber = false;

        vm.jenisKonten = 0;
        vm.disableTipeJawaban = false;

        function init() {
            $translatePartialLoader.addPart('master-template-kuesioner');
            if (vm.QuestionnaireID != 0) {
                dataQuestById();
                questAnswerType();
            }
            else {
                riskCriteria();
                questAnswerType();
            }
        }

        vm.loadIsUseTable = loadIsUseTable;
        function loadIsUseTable() {
            console.info("isusetabel:" + vm.IsUseTableLayout);
        }

        function dataQuestById() {
            MstQuestTemplateService.dataQuestById({ ID: vm.QuestionnaireID }, function (reply) {
                if (reply.status == 200) {
                    vm.dataQuest = reply.data;
                    console.info("byId:" + JSON.stringify(vm.dataQuest));
                    vm.QuestName = vm.dataQuest.DDQuestName;
                    vm.RiskCriteriaID = vm.dataQuest.RiskCriteriaID;
                    vm.listDDquestion = vm.dataQuest.DDQuestion;
                    vm.Status = vm.dataQuest.StatusData.Value;
                    vm.IsUseTableLayout = vm.dataQuest.IsUseTableLayout;
                    vm.IsForPrequal = vm.dataQuest.IsForPrequal;
                    console.info("isuse?" + vm.IsUseTableLayout);
                    riskCriteria();
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                UIControlService.unloadLoading();
            });
        }

        function riskCriteria() {
            MstQuestTemplateService.riskCriteria(function (reply) {
                if (reply.status === 200) {
                    vm.riskCriteria = reply.data;
                    if (vm.QuestionnaireID != 0) {
                        //console.info("riskCriteriaID" + vm.RiskCriteriaID);
                        for (var i = 0; i <= vm.riskCriteria.length - 1; i++) {
                            if (vm.riskCriteria[i].ID == vm.RiskCriteriaID) {
                                vm.RiskCriteria = vm.riskCriteria[i];
                                i = vm.riskCriteria.length - 1;
                            }
                        }
                    }
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                UIControlService.unloadLoading();
            });
        }

        vm.loadRiskCriteria = loadRiskCriteria;
        function loadRiskCriteria() {
            console.info("load" + JSON.stringify(vm.RiskCriteria));
        }

        vm.questAnswerType = [];
        vm.questAnswerTypeHasAnswer = [];
        function questAnswerType() {
            MstQuestTemplateService.questAnswerType(function (reply) {
                if (reply.status === 200) {
                    vm.questAnswerType = reply.data;
                    //console.info("questAnswerType:" + JSON.stringify(vm.questAnswerType));
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                UIControlService.unloadLoading();
            });
        }

        vm.ubahTipeJawaban = ubahTipeJawaban;
        function ubahTipeJawaban() {
            vm.drawTable = false;
            vm.drawOption = false;
        }

        vm.ubahKonten = ubahKonten;
        function ubahKonten() {
            vm.answerType = {};
            vm.disableTipeJawaban = false;
            vm.drawTable = false;
            vm.drawOption = false;
            if (vm.jenisKonten == 2) {
                MstQuestTemplateService.questAnswerTypeByAnswer({IsHasAnswer:false},function (reply) {
                    if (reply.status === 200) {
                        vm.answerType = reply.data[0];
                    }
                    UIControlService.unloadLoading();
                }, function (error) {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                    UIControlService.unloadLoading();
                });
                vm.disableTipeJawaban = true;
            }
            else {
                MstQuestTemplateService.questAnswerTypeByAnswer({ IsHasAnswer: true }, function (reply) {
                    if (reply.status === 200) {
                        vm.questAnswerType= reply.data;
                        //console.info("questAnswerType:" + JSON.stringify(vm.questAnswerType));
                    }
                    UIControlService.unloadLoading();
                }, function (error) {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                    UIControlService.unloadLoading();
                });
            }
        }
        vm.questionDetail = [];
        vm.tambahKonten = tambahKonten;
        function tambahKonten() {
            vm.ddQuestionDetail = [];
            var rowCount = 0;
            if (vm.arrayRow.length > 0) {
                rowCount = vm.arrayRow.length;
            }
            if (vm.questionDetail.length > 0) {
                for (var k = 0; k <= vm.questionDetail.length - 1; k++) {
                    var mappingQuestDetail = {
                        QuestionDetail: vm.questionDetail[k]
                    }
                    vm.ddQuestionDetail.push(mappingQuestDetail);
                }
            }
            if (vm.answerType.AnswerTypeName == 'Text') {
                //default memilih Text
                var mappingQuestDetail = {
                    QuestionDetail: ''
                }
                vm.ddQuestionDetail.push(mappingQuestDetail);
            }
            var ddquestion = {
                Question: vm.Question,
                QuestionAnswerTypeID: vm.answerType.ID,
                RowCount: rowCount,
                ArrayRow: vm.arrayRow,
                IsShowNumber:vm.IsShowNumber,
                Order: 0,
                NumberInQuest:0,
                DDQuestionDetail: vm.ddQuestionDetail,
                QuestionAnswerType: vm.answerType
            }
            vm.listDDquestion.push(ddquestion);
            setNumberInQuest(vm.listDDquestion);
            //vm.kuesioner.push(vm.listDDquestion);
            //console.info("listDDquest:" + JSON.stringify(vm.listDDquestion));
            //console.info("kuesioner:" + JSON.stringify(vm.kuesioner));
            vm.questionDetail = [];
            
        }


        function setNumberInQuest(list) {
            var jumlahNote = 0;
            if (list.length > 1) {
                for (var i = 0; i <= list.length - 1; i++) {
                    list[i].Order = i + 1;
                    if (list[i].QuestionAnswerType.IsHasAnswer == false) {
                        jumlahNote = jumlahNote + 1;
                    }
                    if (list[i].QuestionAnswerType.IsHasAnswer == true) {
                        list[i].NumberInQuest = (i + 1) - jumlahNote;
                    }
                }
            }
            else if (list.length == 1) {
                if (list[0].QuestionAnswerType.IsHasAnswer == true) {
                    list[0].NumberInQuest = 1;
                }
            }
            console.info("numberInQuest list:" + JSON.stringify(list));
            vm.listDDquestion= list;
        }

        vm.drawTable = false;
        vm.row = 0;
        vm.column = 0;
        vm.arrayRow = [];
        vm.arrayColumn = [];
        vm.buatTabel = buatTabel;
        function buatTabel() {
            vm.drawTable = false;
            vm.arrayRow = [];
            vm.arrayColumn = [];
            if (vm.row > 0 && vm.column > 0) {
                for (var i = 1; i <= vm.row; i++) {
                    vm.arrayRow.push(i);
                }
                for (var j = 1; j <= vm.column; j++) {
                    vm.arrayColumn.push(j);
                }
                vm.drawTable = true;
                //console.info("arrayRow:" + JSON.stringify(vm.arrayRow));
            }
        }

        vm.drawOption = false;
        vm.arrayOption = [];
        vm.buatOption = buatOption;
        function buatOption() {
            vm.drawOption = false;
            vm.arrayOption = [];
            if (vm.column > 0) {
                for (var j = 1; j <= vm.column; j++) {
                    vm.arrayOption.push(j);
                }
                vm.drawOption = true;
            }
        }

        vm.resetTabel = resetTabel;
        function resetTabel() {
            if (vm.row > 0 && vm.column > 0) {
                vm.row = 0;
                vm.column = 0;
                vm.arrayRow = [];
                vm.arrayColumn = [];
                vm.drawTable = false;
            }
        }

        vm.hapusKonten = hapusKonten;
        function hapusKonten(data) {
            vm.listDDquestion = remove(vm.listDDquestion, data);
            setNumberInQuest(vm.listDDquestion);
        }


        function remove(list, obj) {
            var index = list.indexOf(obj);
            if (index >= 0) {
                list.splice(index, 1);
            } else {
                UIControlService.msg_growl('error', "ERRORS.OBJECT_NOT_FOUND");
            }
            return list;
        }

        vm.simpan = simpan;
        function simpan() {
            if (vm.QuestName == " " || vm.QuestName == "" || vm.QuestName == undefined || vm.QuestName == null) {
                UIControlService.msg_growl('warning', "MESSAGE.NO_QUESTNAME");
                return
            }
            //else if (vm.RiskCriteria == null || vm.RiskCriteria == undefined) {
            //    UIControlService.msg_growl('warning', "MESSAGE.NO_CRIT");
            //    return
            //}
            else {
                if (vm.QuestionnaireID == 0) {
                    var parameter = {
                        DDQuestName: vm.QuestName,
                        RiskCriteriaID: 0,
                        IsUseTableLayout: vm.IsUseTableLayout,
                        IsForPrequal:vm.IsForPrequal,
                        DDQuestion: vm.listDDquestion
                    }
                    console.info("parameter:" + JSON.stringify(parameter));
                    MstQuestTemplateService.insertQuest(parameter, function (reply) {
                        if (reply.status === 200) {
                            UIControlService.msg_growl('success', "MESSAGE.SUCC_SAVE");
                            $state.transitionTo('master-questionnaire-template');
                        }
                        UIControlService.unloadLoading();
                    }, function (error) {
                        UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
                        UIControlService.unloadLoading();
                    });
                }
                else {
                    var parameter = {
                        ID:vm.QuestionnaireID,
                        DDQuestName: vm.QuestName,
                        RiskCriteriaID: vm.RiskCriteria.ID,
                        IsUseTableLayout: vm.IsUseTableLayout,
                        IsForPrequal:vm.IsForPrequal,
                        DDQuestion: vm.listDDquestion
                    }
                    console.info("parameter:" + JSON.stringify(parameter));
                    MstQuestTemplateService.editQuest(parameter, function (reply) {
                        if (reply.status === 200) {
                            UIControlService.msg_growl('success', "MESSAGE.SUCC_SAVE");
                            $state.transitionTo('master-questionnaire-template');
                        }
                        UIControlService.unloadLoading();
                    }, function (error) {
                        UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
                        UIControlService.unloadLoading();
                    });
                }
            }
        }

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('master-questionnaire-template');
        }
    }
})();
//TODO