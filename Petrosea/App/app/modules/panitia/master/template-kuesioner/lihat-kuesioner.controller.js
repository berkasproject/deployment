﻿(function () {
    'use strict';

    angular.module("app").controller("LihatKuesionerCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'MstQuestTemplateService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, MstQuestTemplateService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('master-template-kuesioner');
            dataQuestById();

        }

        function dataQuestById() {
            MstQuestTemplateService.dataQuestById({ ID: item.ID }, function (reply) {
                if (reply.status == 200) {
                    vm.dataQuest = reply.data;
                    console.info("byId:" + JSON.stringify(vm.dataQuest));
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                UIControlService.unloadLoading();
            });
        }

        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }


    }
})();
//TODO


