(function () {
	'use strict';

	angular.module("app").controller("MstQuestTemplateCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'MstQuestTemplateService', 'UIControlService', '$filter', '$uibModal'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, MstQuestTemplateService, UIControlService, $filter, $uibModal) {

		var vm = this;
		vm.init = init;

		vm.maxSize = 10;
		vm.currentPage = 1;

		vm.data = [];
		function init() {
		    $translatePartialLoader.addPart('master-template-kuesioner');
		    //dataWorkloadProc();
		    //grafikNewVendor();
		    dataQuest(1);
		}

		vm.quest = [];
		vm.totalItems = 0;
		vm.dataQuest = dataQuest;
		function dataQuest(current) {
		    vm.currentPage = current;
		    var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    MstQuestTemplateService.dataQuest({
		        Offset: offset,
		        Limit: vm.maxSize
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.quest = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("quests:" + JSON.stringify(vm.quest));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.publish = publish;
		function publish(data) {

		    bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_PUBLISH') + data.DDQuestName + $filter('translate')('MESSAGE.SURE_PUBLISH2'), function (res) {
		        if (res) {
		            UIControlService.loadLoading("MESSAGE.LOADING");
		            MstQuestTemplateService.publishQuest({
		                ID: data.ID
		            }, function (reply) {
		                if (reply.status === 200) {
		                    UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
		                    init();
		                }
		                UIControlService.unloadLoading();
		            }, function (error) {
		                UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		                UIControlService.unloadLoading();
		            });

		            //SocketService.emit("daftarRekanan");
		        }
		    });


		}

		vm.lihat = lihat;
		function lihat(id) {
		    var item = {
		        ID: id
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/master/template-kuesioner/lihat-kuesioner.html',
		        controller: 'LihatKuesionerCtrl',
		        controllerAs: 'LihatKuesionerCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}

		vm.tambah = tambah;
		function tambah() {
		    $state.transitionTo('questionnaire-template-form', { questId: 0 });
		}
		vm.ubah = ubah;
		function ubah(id) {
		    $state.transitionTo('questionnaire-template-form', { questId: id });
		}

	}
})();
