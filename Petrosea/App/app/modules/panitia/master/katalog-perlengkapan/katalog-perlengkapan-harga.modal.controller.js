﻿(function () {
    'use strict';

    angular.module("app").controller("CatalogueEquipmentCostModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$filter',
        'CatalogueEquipmentService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $filter,
		CatalogueEquipmentService, UIControlService, item, $uibModalInstance) {
        //console.info("masuk modal");
        var vm = this;

        vm.ID = item.ID;
        vm.Name = item.Name;
        vm.Costs = [];

        vm.init = init;
        function init() {
            loadCosts();
        }

        function loadCosts() {
            UIControlService.loadLoadingModal("");
            CatalogueEquipmentService.GetCosts({
                CatalogueEquipmentID: vm.ID,
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.Costs = reply.data;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_LOAD_COST");
            });
        }

        vm.tambahCost = tambahCost;
        function tambahCost() {
            UIControlService.loadLoadingModal("");
            CatalogueEquipmentService.SaveCost({
                CatalogueEquipmentID: vm.ID,
                Cost: vm.Cost,
                Year: vm.Year
            }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "SUCC_SAVE_COST");
		        vm.Cost = vm.Year = null;
		        loadCosts();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_SAVE_COST");
		    });
        }

        vm.hapusCost = hapusCost;
        function hapusCost(data) {
            bootbox.confirm($filter('translate')('CONFIRM_DEL_COST'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    CatalogueEquipmentService.RemoveCost({
                        CatalogueEquipmentID: vm.ID,
                        Year: data.Year
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", 'SUCC_DEL_COST');
                        loadCosts();
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'ERR_DEL_COST');
                    });
                }
            });
        };

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();