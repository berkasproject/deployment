﻿(function() {
    'use strict';

    angular.module("app").controller("CatalogueEquipmentCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'CatalogueEquipmentService',
         'GlobalConstantService', 'UIControlService', '$uibModal', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, CatalogueEquipmentService,
         GlobalConstantService, UIControlService, $uibModal, $filter) {

        var vm = this;
        vm.listAllData = [];
        vm.txtSearch = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-katalog-perlengkapan');
            jLoad(1);
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("");
            vm.currentPage = current;
            var offset = vm.maxSize * (current - 1);
            CatalogueEquipmentService.SelectCatalogues({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.txtSearch
            }, function (reply) {
                //console.info("data_asoc:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                var data = reply.data;
                vm.listAllData = data.List;
                vm.totalItems = data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "ERR_LOAD");
                UIControlService.unloadLoading();
            });
        }

        vm.searchData = searchData;
        function searchData() {
            jLoad(1);
        }

        vm.forminput = forminput;
        function forminput(data) {
            var item = data ? {
                ID: data.ID,
                Name: data.Name
            } : {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/katalog-perlengkapan/katalog-perlengkapan.modal.html',
                controller: 'CatalogueEquipmentModalCtrl',
                controllerAs: 'catEqMCtrl',
                resolve: {
                    item: function() {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function() {
                vm.jLoad(vm.currentPage);
            });
        };

        vm.formharga = formharga;
        function formharga(data) {
            var item = {
                ID: data.ID,
                Name: data.Name
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/katalog-perlengkapan/katalog-perlengkapan-harga.modal.html',
                controller: 'CatalogueEquipmentCostModalCtrl',
                controllerAs: 'catEqCMCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
        };

        vm.hapus = hapus;
        function hapus(data) {
            bootbox.confirm($filter('translate')('CONFIRM_DEL') + '<br/><br/>' + data.Name, function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    CatalogueEquipmentService.RemoveCatalogue({
                        ID : data.ID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", $filter('translate')('SUCC_DEL'));
                        vm.jLoad(vm.currentPage);
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('ERR_DEL'));
                    });
                }
            });
        };
    }
})();
