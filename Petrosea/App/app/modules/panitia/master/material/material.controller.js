﻿(function() {
    'use strict';

    angular.module("app").controller("MasterMaterialCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MasterMaterialService',
         'GlobalConstantService', 'UIControlService', '$uibModal', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, MasterMaterialService,
         GlobalConstantService, UIControlService, $uibModal, $filter) {

        var vm = this;
        vm.listAllData = [];
        vm.txtSearch = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.commodityId = "0";
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {

            MasterMaterialService.GetFormTemplate(function (reply) {
                vm.DocUrlTemplate = reply.data[0].DocUrl;
                vm.DocUrlMarcTemplate = reply.data[1].DocUrl;
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('ERR_GET_TEMPLATE'));
            });

            MasterMaterialService.GetAllCommodities(function (reply) {
                vm.commodities = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", "ERR_LOAD_COMMODITIES");
            });

            $translatePartialLoader.addPart('master-material');
            jLoad(1);
        };

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("LOADING");
            vm.currentPage = current;
            var offset = vm.maxSize * (current - 1);
            MasterMaterialService.SelectMaterials({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.txtSearch,
                Parameter: vm.commodityId
            }, function (reply) {
                //console.info("data_asoc:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                var data = reply.data;
                vm.listAllData = data.List;
                vm.totalItems = data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "ERR_LOAD");
                UIControlService.unloadLoading();
            });
        }

        vm.searchData = searchData;
        function searchData() {
            jLoad(1);
        }

        vm.forminput = forminput;
        function forminput(data) {
            var data = data ? {
                Code: data.Code,
                Description: data.Description,
                CommodityID: data.CommodityID,
                TariffCode: data.TariffCode,
                ConcessionCategory: data.ConcessionCategory
            } : {};
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/material/material.modal.html?v=1.000002',
                controller: 'MasterMaterialModalCtrl',
                controllerAs: 'mmmCtrl',
                resolve: {
                    item: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function() {
                vm.jLoad(vm.currentPage);
            });
        };

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        vm.selectUploadMarc = selectUploadMarc;
        function selectUploadMarc(fileUploadMarc) {
            vm.fileUploadMarc = fileUploadMarc;
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload)) {
                upload(vm.fileUpload);
            }
        }

        vm.uploadFileMarc = uploadFileMarc;
        function uploadFileMarc() {
            if (validateFileType(vm.fileUploadMarc)) {
                uploadMarc(vm.fileUploadMarc);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload(file) {
            UIControlService.loadLoading("");
            MasterMaterialService.InsertExcel(file,
                function (reply) {
                    UIControlService.msg_growl("notice", "SUCC_UPLOAD");
                    UIControlService.msg_growl("notice", $filter('translate')("NEW_MATERIAL") + " : " + reply.data[0]);
                    UIControlService.msg_growl("notice", $filter('translate')("UPDATED_MATERIAL") + " : " + reply.data[1]);
                    UIControlService.unloadLoading();
                    vm.commodityId = "0";
                    vm.jLoad(1);
                },function (error) {
                    UIControlService.msg_growl("error", "ERR_UPLOAD")
                    UIControlService.unloadLoading();
                }
            );
        }

        function uploadMarc(file) {
            UIControlService.loadLoading("");
            MasterMaterialService.InsertMarc(file,
                function (reply) {
                    UIControlService.msg_growl("notice", "SUCC_UPLOAD");
                    UIControlService.msg_growl("notice", $filter('translate')("NEW_MATERIAL") + " : " + reply.data[0]);
                    UIControlService.msg_growl("notice", $filter('translate')("UPDATED_MATERIAL") + " : " + reply.data[1]);
                    UIControlService.unloadLoading();
                    vm.commodityId = "0";
                    vm.jLoad(1);
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "ERR_UPLOAD")
                    console.info(JSON.stringify(error));
                    if (error.data.Message.substr(0, 4) === "ERR_") {
                        var messages = error.data.Message.split(" ");
                        UIControlService.msg_growl("error", messages[1], messages[0]);
                    }
                }
            );
        }
    }
})();
