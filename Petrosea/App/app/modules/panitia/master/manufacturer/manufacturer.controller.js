﻿(function () {
    'use strict';

    angular.module("app").controller('manufacturerController', ctrl);

    ctrl.$inject = ['UIControlService', '$translatePartialLoader', 'ManufacturerService', '$uibModal'];

    function ctrl(UIControlService, $translatePartialLoader, ManufacturerService, $uibModal) {
        var vm = this;

        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;

        vm.keyword = '';
        vm.totalItems = 0;
        vm.maxSize = 10;
        vm.currentPage = 1;

        vm.dataManufacturer = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-manufacturer');
            loadSteps(1);
        }

        vm.loadSteps = loadSteps;
        function loadSteps(current) {
            UIControlService.loadLoading('MESSAGE.LOADING');
            ManufacturerService.select({
                Offset: (vm.currentPage - 1) * vm.maxSize,
                Limit: vm.maxSize,
                Keyword: vm.keyword
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataManufacturer = reply.data.List;
                    vm.totalItems = reply.data.Count;
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error",'MESSAGE.GET_MANUFACTURER_ERROR',"MESSAGE.GET_MANUFACTURER_TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
            });
        }

        vm.addManufacturer = addManufacturer;
        function addManufacturer() {
            var modalInstance = $uibModal.open({
                templateUrl: 'addModalManufacturer.html',
                controller: addManufacturerCtrl,
                controllerAs: 'addManufacturerCtrl'
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.editManufacturer = editManufacturer;
        function editManufacturer(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'editModalManufacturer.html',
                controller: editManufacturerCtrl,
                controllerAs: 'editManufacturerCtrl',
                resolve: { item: function () { return post; }}
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.inactivateManufacturer = inactivateManufacturer;
        function inactivateManufacturer(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'inactivateModalManufacturer.html',
                controller: inactivateManufacturerCtrl,
                controllerAs: 'inactivateManufacturerCtrl',
                resolve: { item: function () { return post; }}
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.activateManufacturer = activateManufacturer;
        function activateManufacturer(id) {
            var post = id;
            var modalInstance = $uibModal.open({
                templateUrl: 'activateModalManufacturer.html',
                controller: activateManufacturerCtrl,
                controllerAs: 'activateManufacturerCtrl',
                resolve: { item: function () { return post; }}
            });

            modalInstance.result.then(function () {
                loadSteps();
            });
        }

        vm.searchData = searchData;
        function searchData() {
            loadSteps(1);
        }

        vm.pageChanged = pageChanged;
        function pageChanged() {
            loadSteps(1);
        }
    }
})()

var addManufacturerCtrl = function (UIControlService, $uibModalInstance, ManufacturerService) {
    var vm = this;

    vm.namaJenisPemasok = '';
    vm.deskripsi = '';

    vm.createManufacturer = createManufacturer;
    function createManufacturer() {
        if (vm.namaJenisPemasok === '' || vm.namaJenisPemasok === null) {
            return false
        }

        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        ManufacturerService.insert({
            ManufacturerName: vm.namaJenisPemasok,
            Description: vm.deskripsi
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.ADD_SUCCESS_MESSAGE', "MESSAGE.ADD_SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal()
                UIControlService.msg_growl("error", 'MESSAGE.ADD_ERROR_MESSAGE', "MESSAGE.ADD_ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close()
    }
}

var editManufacturerCtrl = function (item, UIControlService, $uibModalInstance, ManufacturerService) {
    var vm = this;

    vm.namaJenisPemasok = '';
    vm.deskripsi = '';

    UIControlService.loadLoadingModal('MESSAGE.LOADING');
    ManufacturerService.getByID({
        ID: item
    }, function (reply) {
        if (reply.status === 200) {
            vm.namaJenisPemasok = reply.data.ManufacturerName;
            vm.deskripsi = reply.data.Description;
            UIControlService.unloadLoadingModal();
        } else {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_MANUFACTURER_ERROR', "MESSAGE.GET_MANUFACTURER_TITLE");
        }
    }, function (err) {
        UIControlService.unloadLoadingModal();
        UIControlService.msg_growl("error",'MESSAGE.GET_API_ERROR',"MESSAGE.GET_API_ERROR_TITLE");
    });

    vm.updateManufacturerSave = updateManufacturerSave;
    function updateManufacturerSave() {
        if (vm.namaJenisPemasok === '' || vm.namaJenisPemasok === null) {
            return false;
        }

        UIControlService.loadLoadingModal('MESSAGE.LOADING');
        ManufacturerService.update({
            ID: item,
            ManufacturerName: vm.namaJenisPemasok,
            Description: vm.deskripsi
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.EDIT_SUCCESS_MESSAGE', "MESSAGE.EDIT_SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.EDIT_ERROR_MESSAGE', "MESSAGE.EDIT_ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var inactivateManufacturerCtrl = function (item, UIControlService, $uibModalInstance, ManufacturerService) {
    var vm = this;

    UIControlService.loadLoadingModal('MESSAGE.LOADING');
    vm.inactivateManufacturerSave = inactivateManufacturerSave;
    function inactivateManufacturerSave() {
        ManufacturerService.inactivate({
            ID: item
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.INACTIVATE_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.INACTIVATE_ERROR_MESSAGE', "MESSAGE.ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal;
    function closeModal() {
        $uibModalInstance.close();
    }
}

var activateManufacturerCtrl = function (item, UIControlService, $uibModalInstance, ManufacturerService) {
    var vm = this;

    UIControlService.loadLoadingModal('MESSAGE.LOADING');
    vm.activateManufacturerSave = activateManufacturerSave;
    function activateManufacturerSave() {
        ManufacturerService.activate({
            ID: item
        }, function (reply) {
            if (reply.status === 200) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", 'MESSAGE.ACTIVATE_SUCCESS_MESSAGE', "MESSAGE.SUCCESS_TITLE");
                $uibModalInstance.close();
            } else {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ACTIVATE_ERROR_MESSAGE', "MESSAGE.ERROR_TITLE");
            }
        }, function (err) {
            UIControlService.unloadLoadingModal();
            UIControlService.msg_growl("error", 'MESSAGE.GET_API_ERROR', "MESSAGE.GET_API_ERROR_TITLE");
        });
    }

    vm.closeModal = closeModal; 
    function closeModal() {
        $uibModalInstance.close();
    }
}