﻿(function () {
    'use strict';

    angular.module("app").controller("dokKontrakCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'DokumenKontrakService', '$filter', '$rootScope', '$state'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, DokumenKontrakService, $filter, $rootScope, $state) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.businessField = "";
        vm.status = "";
        vm.tenderType = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('dokumen-kontrak');
            UIControlService.loadLoading("Loading...");
            getBusinessField();
            vm.jLoad(1);
        }

        vm.ubahbf = ubahbf;
        function ubahbf(businessField) {
            vm.businessField = businessField;
        }

        vm.ubahStatus = ubahStatus;
        function ubahStatus(status) {
            vm.status = status;
        }

        vm.ubahTenderType = ubahTenderType;
        function ubahTenderType(tenderType) {
            vm.tenderType = tenderType;
        }

        vm.act = act;
        function act(action) {
            vm.businessField = "";
            vm.status = "";
            vm.keyword = "";
            vm.tenderType = "";
            vm.action = action;
            UIControlService.loadLoading("Loading...");
            vm.jLoad(1);
        }
        
        function getTenderType() {
            DokumenKontrakService.getTenderType(function (reply) {
                vm.dataTenderType = reply.data.List;
                if (vm.dataTenderType) {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoading();
            });
        }

        function getJenisTender() {
            DokumenKontrakService.getJenisTender(function (reply) {
                vm.TenderType = reply.data.List;
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoading();
            });
        }

        function getBusinessField() {
            DokumenKontrakService.getBusinessField(function (reply) {
                vm.dataBusinessField = reply.data.List;
                if (vm.dataBusinessField) {
                    getTenderType();
                }
            }, function (err) {
                console.info(err);
                    //$.growl.error({ message: "Gagal mendapatkan data plant" });
                });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {

            if (vm.businessField == "") {
                vm.businessField = "0";
            }
            if (vm.tenderType == "") {
                vm.tenderType = "0";
            }
            vm.currentPage = current;
            DokumenKontrakService.select({
                FilterType: Number(vm.action),
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword: vm.keyword,
                IntParam1: Number(vm.businessField),
                IntParam2: Number(vm.tenderType),
                Keyword2: vm.status
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.docs = reply.data.List;
                vm.totalItems = reply.data.Count;

            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                UIControlService.unloadLoading();
            });
        }

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            UIControlService.loadLoading("Loading...");
            vm.jLoad(1);
        }

        vm.tambah = tambah;
        function tambah() {
            $state.transitionTo('dokumen-kontrak-form', { id: 0 });
        }

        vm.edit = edit;
        function edit(id) {
            $state.transitionTo('dokumen-kontrak-form', { id: id });
        }

        vm.aktif = aktif;
        function aktif(data) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.AKTIF') + ' ' + data.DocumentName + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    DokumenKontrakService.aktif({
                        ID: data.ID
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.BERHASIL_AKTIF');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.GAGAL_AKTIF');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.GAGAL_AKTIF');
                    });
                }
            });
        };

        vm.nonAktif = nonAktif;
        function nonAktif(data) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.NONAKTIF') + ' ' + data.DocumentName + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    DokumenKontrakService.nonAktif({
                        ID: data.ID
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.BERHASIL_NONAKTIF');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.GAGAL_NONAKTIF');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.GAGAL_NONAKTIF');
                    });
                }
            });
        };


    }
})();