﻿(function () {
	'use strict';

	angular.module("app").controller("dokKontrakFormCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'DokumenKontrakService', '$filter', '$rootScope', '$stateParams', 'UploadFileConfigService', 'VerifiedSendService', 'UploaderService', '$state'];
	/* @ngInject */
	function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, DokumenKontrakService, $filter, $rootScope, $stateParams, UploadFileConfigService, VerifiedSendService, UploaderService, $state) {
		var vm = this;
		var DocUrl;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.DocumentName = "";
		vm.businessField = "";
		vm.tenderType = "";
		vm.jenisTender = "";
		vm.status = "";
		vm.isAdd = 0;
		vm.klik = 0;
		vm.id = 0;
		vm.idFileTypes;
		vm.idFileSize;
		vm.idUploadConfigs;
		vm.pathFile;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.fileUpload;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('dokumen-kontrak');
			UIControlService.loadLoading("Loading...");
			getBusinessField();
			if ($stateParams.id == "0") {
				vm.isAdd = 1;
			}
			//vm.jLoad(1);

			UploadFileConfigService.getByPageName("PAGE.ADMIN.CONTRACTDOCUMENT", function (response) {
				if (response.status == 200) {
					UIControlService.unloadLoading();
					//console.info(response);
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", "Error!");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}
		vm.ubahbf = ubahbf;
		function ubahbf(businessField) {
			vm.businessField = businessField;
		}

		vm.ubahTenderType = ubahTenderType;
		function ubahTenderType(tenderType) {
			vm.tenderType = tenderType;
		}

		vm.ubahJenisTender = ubahJenisTender;
		function ubahJenisTender(jenisTender) {
			vm.jenisTender = jenisTender;
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}


		vm.selectUpload = selectUpload;
		function selectUpload() {

		}

		vm.uploadFile = uploadFile;
		function uploadFile() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			if (vm.klik > 0) {
				return;
			}
			if (vm.DocumentName == "") {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.DOCUMENTNAME");
				return;
			}

			if (vm.businessField == "") {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.BUSINESSFIELD");
				return;
			}

			if (vm.tenderType == "") {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.TENDERTYPE");
				return;
			}

			if (vm.jenisTender == "") {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.JENISTENDER");
				return;
			}


			vm.klik += 1;

			if (!(vm.fileUpload == null || vm.fileUpload == '')) {
				if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					//console.info(vm.fileUpload, vm.idUploadConfigs);
					upload(1, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
				} else {
					vm.klik = 0;
					return false;
					UIControlService.unloadLoadingModal();
				}
			} else {
				vm.klik = 0;
				if (vm.isAdd == 1) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
					return;
				} else {
					vm.simpan('');
				}
			}
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.NO_FILE");
				return false;
				UIControlService.unloadLoadingModal();
			}
			else return true;
		}

		vm.upload = upload;
		function upload(id, file, config, filters, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}

			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleContractDocument(id, file, size, filters, function (response) {
				//console.info("response:" + JSON.stringify(response));
				if (response.status == 200) {
					//console.info(response);
					var url = response.data.Url;
					vm.pathFile = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;
					vm.DocUrl = vm.pathFile;
					//console.info(vm.DocUrl);
					if (vm.flag == 0) {
						vm.size = Math.floor(s);
						//  console.info(vm.size);
					}

					if (vm.flag == 1) {
						vm.size = Math.floor(s / (1024));
					}

					vm.simpan(vm.DocUrl);
				} else {
					UIControlService.msg_growl("error", "Error!");
					return;
				}
			}, function (response) {
				//console.info(response);
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoadingModal();
			});


		}

		function getTenderType() {
			DokumenKontrakService.getTenderType(function (reply) {
				vm.dataTenderType = reply.data.List;
				if (vm.dataTenderType) {
					getJenisTender();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal mendapatkan data plant" });
				UIControlService.unloadLoading();
			});
		}

		function getJenisTender() {
			DokumenKontrakService.getJenisTender(function (reply) {
				vm.dataJenisTender = reply.data.List;
				if (vm.dataJenisTender) {
					if (vm.isAdd == 0) {
						loadData();
					}

				}
			}, function (err) {
				//$.growl.error({ message: "Gagal mendapatkan data plant" });
				UIControlService.unloadLoading();
			});
		}

		function getBusinessField() {
			DokumenKontrakService.getBusinessField(function (reply) {
				vm.dataBusinessField = reply.data.List;
				if (vm.dataBusinessField) {
					getTenderType();
				}
			}, function (err) {
				//console.info(err);
				//$.growl.error({ message: "Gagal mendapatkan data plant" });
			});
		}

		vm.simpan = simpan;
		function simpan(url) {
			if (url != '') {
				DocUrl = url;
			}


			DokumenKontrakService.save({
				create: vm.isAdd,
				ID: vm.id,
				DocumentName: vm.DocumentName,
				BusinessFieldID: Number(vm.businessField),
				TenderTypeID: Number(vm.tenderType),
				JenisTenderID: Number(vm.jenisTender),
				ContractDocumentURL: DocUrl
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.BERHASIL_SIMPAN");
					$state.transitionTo('dokumen-kontrak', {});
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GAGAL_SIMPAN");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.RedirectDocument = RedirectDocument;
		function RedirectDocument() {
			$state.transitionTo("dokumen-kontrak-view-word", { id: $stateParams.id })
		}

		vm.loadData = loadData;
		function loadData() {
			UIControlService.loadLoading();

			DokumenKontrakService.getData({
				IntParam1: Number($stateParams.id)
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.data = reply.data.List;
					if (vm.data) {
						vm.DocumentName = vm.data[0].DocumentName;
						vm.businessField = vm.data[0].BusinessFieldID.toString();
						vm.tenderType = vm.data[0].TenderTypeID.toString();
						vm.jenisTender = vm.data[0].JenisTenderID.toString();
						DocUrl = vm.data[0].ContractDocumentURL;
						vm.id = vm.data[0].ID;
						vm.Dokumen_Url = vm.data[0].ContractDocumentURL;
					}

				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.back = back;
		function back() {
		    $state.transitionTo("dokumen-kontrak");
		}
	}
})();