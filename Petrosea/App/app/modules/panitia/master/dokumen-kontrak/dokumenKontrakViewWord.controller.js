﻿(function () {
    'use strict';

    angular.module("app").controller("dokKontrakViewWordCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'DokumenKontrakService', '$filter', '$rootScope', '$stateParams', '$state','$sce'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, DokumenKontrakService, $filter, $rootScope, $stateParams, $state,$sce) {
        var vm = this;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.DocumentName = "";
        vm.businessField = "";
        vm.tenderType = "";
        vm.jenisTender = "";
        vm.status = "";
        vm.isAdd = 0;
        vm.klik = 0;
        vm.id = 0;
        vm.idFileTypes;
        vm.idFileSize;
        vm.idUploadConfigs;
        vm.pathFile;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.fileUpload;
        vm.idData = Number($stateParams.id);

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('dokumen-kontrak');
            DokumenKontrakService.getData({
                IntParam1: Number($stateParams.id)
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.data = reply.data.List;
                    if (vm.data) {
                        vm.ContractDocumentURL = vm.data[0].ContractDocumentURL;
                        vm.urlFile = vm.folderFile + vm.ContractDocumentURL;
                        vm.urlFileAslie = "https://docs.google.com/gview?url=" + vm.urlFile + "&embedded=true"

                        vm.urlFileAslie = $sce.trustAsResourceUrl(vm.urlFileAslie);
                        console.log(vm.urlFileAslie);
                    }

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
    }
})();
    