(function () {
    'use strict';

    angular.module("app").controller("tambahUbahPlantCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'PlantService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, PlantService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.create = item.create;
        vm.plant = "";
        vm.search_term = "";
        vm.PlantID = 0;
        if (vm.create == 0) {
            vm.plant = item.plant.Plant;
            vm.search_term = item.plant.SearchTerm1;
            vm.PlantID = item.plant.PlantID;
        }

        vm.klik = 0;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-plant');
        }

        vm.save = save;
        function save() {
            vm.klik += 1;
            if (vm.klik > 1) {
                return;
            }
            if (vm.plant == "") {
                vm.klik = 0;
                UIControlService.msg_growl("error", "MESSAGE.ERR_PLANT");
                return;
            }
            if (vm.search_term == "") {
                vm.klik = 0;
                UIControlService.msg_growl("error", "MESSAGE.ERR_ST");
                return;
            }
                
            PlantService.save({
                PlantID: vm.PlantID,
                Plant: vm.plant,
                SearchTerm1: vm.search_term,
                create: vm.create
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC");
                    $uibModalInstance.close();
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    return;
                }
            }, function (err) {
                console.info(err);
                UIControlService.msg_growl( "error", "Gagal Akses API >" + err );
            });    

            
        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        }

    }
})();
//TODO


