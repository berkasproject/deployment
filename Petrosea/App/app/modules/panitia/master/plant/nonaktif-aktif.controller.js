(function () {
    'use strict';

    angular.module("app").controller("nonAktifCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'PlantService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, PlantService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.nonAktif = item.nonAktifkan;
        

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-plant');
        }
        vm.ya = ya;
        function ya() {
            if (vm.nonAktif == 1) {
                if (item.Plant.IsUsed == 1) {
                    UIControlService.msg_growl("error", "MESSAGE.IS_USED");
                } else {
                    save();
                }
            }
            save();
        }

        function save() {
            PlantService.saveAktif({
                PlantID: item.Plant.PlantID,
                nonAktif: vm.nonAktif
            }, function (reply) {
                if (reply.status == 200) {
                    if (reply.config.data.nonAktif == 1) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_NONAKTIF");
                        $uibModalInstance.close();
                    } else if (reply.config.data.nonAktif == 0) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_AKTIF");
                        $uibModalInstance.close();
                    }
                }
                
                else {
                    //$.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    //$rootScope.unloadLoading();
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl( "error", "Gagal Akses API >" + err );
                return;
            });
        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        }

    }
})();
//TODO


