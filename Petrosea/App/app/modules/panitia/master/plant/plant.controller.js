(function () {
    'use strict';

    angular.module("app").controller("plantController", ctrl);

    ctrl.$inject = ['$http', '$state', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PlantService', 'UIControlService'];

    function ctrl($http, $state, $uibModal, $translate, $translatePartialLoader, $location, SocketService, PlantService, UIControlService) {
        var vm = this;
        var page_id = 158;

        vm.berita = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.keyword = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-plant');
            vm.jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            PlantService.select({
                Keyword: vm.keyword,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                vm.plants = reply.data.List;
                vm.totalItems = reply.data.Count;
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                $rootScope.unloadLoading();
            });
        }

        //function loadOpsiViewer() {
        //    //alert("load");
        //    vm.listviewer = [];
        //    NewsService.getOpsiViewer(function (reply) {
        //        UIControlService.unloadLoading();
        //        vm.listviewer = reply.data.List;
        //        //console.info("listOpsi" + JSON.stringify(vm.listviewer));
        //    }, function (err) {
        //        UIControlService.msg_growl("error", "MESSAGE.API");
        //        UIControlService.unloadLoading();
        //    });
        //}

        vm.cariPlant = cariPlant;
        function cariPlant(keyword) {
            vm.keyword = keyword;
            vm.jLoad(1);
        }

        vm.remHtml = remHtml;
        function remHtml(text) {
            return text ? String(text).replace(/<[^>]+>/gm, '') : '';
        }

        vm.createPlant = createPlant;
        function createPlant() {
            var post = {
                create: 1
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/plant/tambah-plant.html',
                controller: 'tambahUbahPlantCtrl',
                controllerAs: 'tambahUbahPlantCtrl',
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.updatePlant = updatePlant;
        function updatePlant(data) {
            var post = {
                create: 0,
                plant: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/plant/tambah-plant.html',
                controller: 'tambahUbahPlantCtrl',
                controllerAs: 'tambahUbahPlantCtrl',
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.nonAktif = nonAktif;
        function nonAktif(data) {
            var post = {
                nonAktifkan: 1,
                Plant: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/plant/nonaktif-aktif.html',
                controller: 'nonAktifCtrl',
                controllerAs: 'nonAktifCtrl',
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.aktif = aktif;
        function aktif(data) {
            var post = {
                nonAktifkan: 0,
                Plant: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/plant/nonaktif-aktif.html',
                controller: 'nonAktifCtrl',
                controllerAs: 'nonAktifCtrl',
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }
    }
})();

//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}