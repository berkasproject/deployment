﻿(function () {
    'use strict';

    angular.module("app").controller("MasterQualificationModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$uibModal',
        'MasterQualificationService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $uibModal,
		MasterQualificationService, UIControlService, item, $uibModalInstance) {

        var vm = this;

        vm.init = init;
        function init() {
            vm.ID = item.ID;
            vm.MinAmountInti = item.MinAmountInti;
            vm.MinAmountNonInti = item.MinAmountNonInti;
            vm.QualificationName = item.QualificationName;
        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

        vm.simpan = simpan;
        function simpan() {

            if (!vm.MinAmountInti || !vm.MinAmountNonInti) {
                UIControlService.msg_growl("error", "ERR_INCOMPLETE");
                return;
            }

            UIControlService.loadLoadingModal("");
            MasterQualificationService.UpdateQualification({
                ID: vm.ID,
                MinAmountInti: vm.MinAmountInti,
                MinAmountNonInti: vm.MinAmountNonInti
            }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "SUCC_SAVE");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_SAVE");
                if (error.Message.substr(0, 4) === "ERR_") {
                    UIControlService.msg_growl("error", error.Message);
                }
		    });
        }
    }
})();