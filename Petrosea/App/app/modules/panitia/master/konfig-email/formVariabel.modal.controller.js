﻿(function() {
    'use strict';

    angular.module("app").controller("FormVariabelCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', 'item', '$uibModalInstance', 'SocketService', 'UIControlService', 'KontenEmailService'];
    function ctrl($http, $translate, $translatePartialLoader, item, $uibModalInstance, SocketService, UIControlService, KontenEmailService) {

        var vm = this;

        vm.variabel_email_global = {};

        vm.init = init;
        function init() {
            vm.variabel_email_global = item;
            vm.variabel_email_global.namaAwal = item.Name;
        };

        vm.onCancelClick = onCancelClick;
        function onCancelClick() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.onSimpanClick = onSimpanClick;
        function onSimpanClick() {

            if (!vm.variabel_email_global.Name || !vm.variabel_email_global.Value) {
                UIControlService.msg_growl("error", "ERR_INCOMPLETE");
                return;
            }

            var saveServiceAPI;
            if (vm.variabel_email_global.ID) {
                saveServiceAPI = KontenEmailService.updateGlobalVariable;
            } else {
                saveServiceAPI = KontenEmailService.insertGlobalVariable;
            }

            UIControlService.loadLoadingModal("LOADING");
            saveServiceAPI({
                ID: vm.variabel_email_global.ID,
                Name: vm.variabel_email_global.Name,
                Value: vm.variabel_email_global.Value
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", "SUCC_SAVE_VAR");
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", "ERR_SAVE_VAR");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_SAVE_VAR");
                if (err.Message === "ERR_DUPLICATE_VARIABLE") {
                    UIControlService.msg_growl("error", "ERR_DUPLICATE_VARIABLE");
                }
                return;
            });
        };
    }
})();