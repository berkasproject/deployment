﻿(function() {
    'use strict';

    angular.module("app")
        .controller("KonfigVarGlobalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$state', '$uibModal', '$translatePartialLoader', '$filter', 'UIControlService', 'KontenEmailService'];
    function ctrl($http, $translate, $state, $uibModal, $translatePartialLoader, $filter, UIControlService, KontenEmailService) {
            
        var vm = this;
        
        vm.variabel_email_globals = [];
        vm.filter = '';
        vm.count = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init(){
            $translatePartialLoader.addPart('konfig-email');
            loadVariables();
        };

        vm.onPageChanged = onPageChanged;
        function onPageChanged(currentPage){
            vm.currentPage = currentPage;
            loadVariables();
        };

        vm.onFilterChanged = onFilterChanged;
        function onFilterChanged() {
            vm.currentPage = 1;
            loadVariables();
        };

        vm.onEditClick = onEditClick;
        function onEditClick(variabel_email_global){
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/konfig-email/formVariabel.modal.html',
                controller: 'FormVariabelCtrl',
                controllerAs: 'formVariabelCtrl',
                resolve: {
                    item: function() {
                        return {
                            ID: variabel_email_global.ID,
                            Name: variabel_email_global.Name,
                            Value: variabel_email_global.Value
                        };
                    }
                }
            });
            modalInstance.result.then(function() {
                loadVariables();
            });  
        };

        vm.onTambahClick = onTambahClick;
        function onTambahClick(){
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/konfig-email/formVariabel.modal.html',
                controller: 'FormVariabelCtrl',
                controllerAs: 'formVariabelCtrl',
                resolve: {
                    item: function () {
                        return {
                            ID: null,
                            Name: '',
                            Value: ''
                        };
                    }
                }
            });
            modalInstance.result.then(function () {
                loadVariables();
            });
        };

        vm.onDeleteClick = onDeleteClick;
        function onDeleteClick(variabel_email_global) {
            bootbox.confirm($filter('translate')('CONFIRM_DELETE'),function(yes){
                if (yes) {
                    UIControlService.loadLoading("LOADING");
                    KontenEmailService.deleteGlobalVariable({
                        ID: variabel_email_global.ID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "SUCC_DELETE_VARIABLES");
                            loadVariables();
                        } else {
                            UIControlService.msg_growl("error", "ERR_DELETE_VARIABLES");
                        }
                    }, function (err) {
                        UIControlService.loadLoading();
                        UIControlService.msg_growl("error", "ERR_DELETE_VARIABLES");
                        return;
                    });
                };
            });
        };

        function loadVariables() {
            UIControlService.loadLoading("LOADING");
            KontenEmailService.selectGlobalVariable({
                Keyword: vm.filter,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if(reply.status === 200){
                    vm.variabel_email_globals = reply.data.List;
                    vm.count = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", "ERR_LOAD_VARIABLES");
                }
            }, function (err) {
                UIControlService.loadLoading();
                UIControlService.msg_growl("error", "ERR_LOAD_VARIABLES");
                return;
            });
        }
    }
})();

/*
var deleteVariabelCtrl = function($scope, $modalInstance, $http, variabel_email_global, $cookieStore, $rootScope) {
    $scope.variabel_email_global = {};
    $scope.init = function() {
        $scope.variabel_email_global = variabel_email_global;
    };
    
    $scope.onCancelClick = function() {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.onConfirmClick = function() {
        $rootScope.authorize(
            //itp.mailconfig.deleteVariable
            $http.post($rootScope.url_api + 'mailconfig/varlok/delete',
            {
                id_variabel_global:$scope.variabel_email_global.id_variabel_global
            }).success(function(reply){
                if (reply.status === 200){
                    $.growl.notice({title: "[INFO]", message: "Variabel berhasil dihapus"});
                    $modalInstance.close();
                } else {
                    $.growl.error({title: "[WARNING]", message: "Variabel gagal dihapus"});
                }
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            })
        );
    };
};
*/