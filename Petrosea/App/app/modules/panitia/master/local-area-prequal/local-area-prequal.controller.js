﻿(function () {
	'use strict';

	angular.module("app").controller("LocalAreaPrequalController", ctrl);

	ctrl.$inject = ['$filter', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'LocalAreaPrequalService', 'UIControlService', '$uibModal'];
	function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService, LocalAreaPrequalService,UIControlService, $uibModal) {

		var vm = this;

		vm.localarea = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.Keyword = "";

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('master-localarea');
			UIControlService.loadLoading("Silahkan Tunggu...");
			jLoad(1);

		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.localarea = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			LocalAreaPrequalService.select({
				Offset: offset,
				Limit: vm.pageSize,
				Keyword: vm.Keyword
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.localarea = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.ubah_aktif = ubah_aktif;
		function ubah_aktif(data, active) {
		    bootbox.confirm($filter('translate')('Apakah anda yakin menghapus data ?'), function (yes) {
		        if (yes) {
		            UIControlService.loadLoading("Silahkan Tunggu");
		            LocalAreaPrequalService.deleteData({
		                ID: data.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.status === 200) {
		                    var msg = "";
		                    if (active === false) msg = " NonAktifkan ";
		                    if (active === true) msg = "Aktifkan ";
		                    UIControlService.msg_growl("success", "Data Berhasil di " + msg);
		                    jLoad(1);
		                }
		            }, function (err) {
		                UIControlService.unloadLoading();
		            });
		        }
		    });

		}

		vm.tambah = tambah;
		function tambah(data) {
			var data = {
				act: 1,
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/local-area-prequal/FormEditLocalAreaPrequal.html',
				controller: 'EditLocalAreaCtrl',
				controllerAs: 'EditLocalAreaCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(1);
			});
		}

		vm.edit = edit;
		function edit(data) {
			var data = {
				act: 0,
				item: data
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/master/local-area-prequal/FormEditLocalAreaPrequal.html',
			    controller: 'EditLocalAreaCtrl',
			    controllerAs: 'EditLocalAreaCtrl',
			    resolve: {
			        item: function () {
			            return data;
			        }
			    }
			});
			modalInstance.result.then(function () {
			    vm.jLoad(1);
			});
		}

		vm.view = view
		function view(data) {
		    var data = {
		        act: 2,
		        item: data
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/master/local-area-prequal/FormEditLocalAreaPrequal.html',
		        controller: 'EditLocalAreaCtrl',
		        controllerAs: 'EditLocalAreaCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        vm.jLoad(1);
		    });
		}
	}
})();
//TODO

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}

