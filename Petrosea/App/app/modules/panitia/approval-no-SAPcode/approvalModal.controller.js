﻿(function () {
	'use strict';

	angular.module("app").controller("ApprovalNoSAPModalCtrl", ctrl);

	ctrl.$inject = ['item', 'ApprovalNoSAPService', 'UIControlService', '$uibModal', '$uibModalInstance'];
	function ctrl(item, ApprovalNoSAPService, UIControlService, $uibModal, $uibModalInstance) {
		var vm = this;
		vm.init = init;
		vm.item = item.item;
		vm.act = item.act;
		// vm.jLoad = jLoad;

		function init() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			loadDataApproval();
		}

		vm.loadDataApproval = loadDataApproval;
		function loadDataApproval() {
			vm.dataApproval = [];
			ApprovalNoSAPService.select({
				Keyword: null,
				column: 0,
				Offset: 0,
				Limit: 10
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.dataApproval = reply.data.List;
					//console.info("dataApproval:" + JSON.stringify(vm.dataApproval));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_APPROVE");
				UIControlService.unloadLoading();
			});
		}

		vm.action = action;
		function action() {
			var data = {
				ID: vm.item.ID,
				TenderStepDataId: vm.item.TenderStepDataId,
				ApprovalComment: vm.comment,
				VendorID: vm.item.VendorID
			};
			ApprovalNoSAPService.reject(data, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_REJECT");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_REJECT");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_REJECT");
				//console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoadingModal();
			});
		}

		vm.approve = approve;
		function approve() {
			var data = {
				ID: vm.item.ID,
				TenderStepDataId: vm.item.TenderStepDataId,
				ApprovalComment: vm.comment,
				VendorID: vm.item.VendorID
			};
			ApprovalNoSAPService.approve(data, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_APPROVE");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_APPROVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_APPROVE");
				//console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}
	}
})();
//TODO


