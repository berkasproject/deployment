﻿(function () {
	'use strict';

	angular.module("app").controller("detailApprovalNoSAPCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'UIControlService', 'ApprovalNoSAPService', 'item', '$uibModalInstance'];

	function ctrl($translatePartialLoader, UIControlService, ApprovalNoSAPService, item, $uibModalInstance) {
		var vm = this

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('tender-step-approval')
			UIControlService.loadLoadingModal("MESSAGE.LOADING")
			ApprovalNoSAPService.detailApproval({
				TenderStepDataId: item.TenderStepDataId,
				VendorID: item.VendorID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.apprvs = reply.data;
				} else {
					$.growl.error({ message: "Get Approval Failed." });
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}
	}
})()