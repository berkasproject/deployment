﻿(function () {
	'use strict';

	angular.module("app").controller("dataUploadMigrasiFPA", ctrl);

	ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'dataUploadMigrasiFPAService', 'UIControlService', 'UploadFileConfigService', 'ExcelReaderService', 'GlobalConstantService', 'UploaderService'];
	/* @ngInject */
	function ctrl($state, $stateParams, $http, $filter, $translate, $translatePartialLoader, $location, SocketService, dataUploadMigrasiFPAService, UIControlService, UploadFileConfigService, ExcelReaderService, GlobalConstantService, UploaderService) {

		var vm = this;
		var loadmsg = "LOADING";
		vm.currentPage = 1;
		vm.fileUpload;
		vm.maxSize = 10;
		vm.DocName = "";
		vm.currentPage = 1;
		vm.listItemPO = [];
		vm.totalItems = 0;
		vm.keyword = "";
		vm.ID = Number($stateParams.id);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.init = init;
		vm.tglSekarang = UIControlService.getDateNow("");
		function init() {
			$translatePartialLoader.addPart('detail-dataupload-po');
			$translatePartialLoader.addPart('dataupload-po');
			UIControlService.loadLoading("LOADING");
			loadTypeSizeFile();
		}
		vm.loadData = loadData;
		function loadData(current) {
			//console.info("curr "+current)
			UIControlService.loadLoading("LOADING");
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			dataUploadMigrasiFPAService.Select({
				Offset: offset,
				Limit: vm.maxSize
			}, function (reply) {
				//console.info("data:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listItemPO = data.List;

					for (var i = 0; i < vm.listItemPO.length; i++) {
						if (vm.listItemPO[i].PODate === null) { vm.listItemPO[i].PODate = "-"; }
						else { vm.listItemPO[i].PODate = UIControlService.getStrDate(vm.listItemPO[i].PODate); }
						if (vm.listItemPO[i].PODueDate === null) { vm.listItemPO[i].PODueDate = "-"; }
						else { vm.listItemPO[i].PODueDate = UIControlService.getStrDate(vm.listItemPO[i].PODueDate); }
					}
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		function loadTypeSizeFile() {
			UIControlService.loadLoading("LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.ADMIN.MIGRASIVHSFPA", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoading();
				return;
			});
		}
		/*count of property object*/
		function numAttrs(obj) {
			var count = 0;
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					++count;
				}
			}
			return count;
		}
		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.toDetail = toDetail;
		function toDetail(id) {
			$state.transitionTo('detail-dataupload-po', { id: id });
		}
		vm.toCompare = toCompare;
		function toCompare(id) {
			$state.transitionTo('compare-dataupload-po', { id: id });
		}

		//start upload
		vm.uploadFile = uploadFile;
		function uploadFile() {
			if (vm.fileUpload === undefined) {
				UIControlService.msg_growl("error", "MSG_NOFILE");
				return;
			}

			if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);

			}
		}

		function upload(file, config, filters, dates, callback) {
			//console.info(file);
			var size = config.Size;

			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoading("LOADING");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                	//console.info("upload:" + JSON.stringify(response.data));
                	UIControlService.unloadLoading();
                	if (response.status == 200) {
                		var url = response.data.Url;
                		var fileName = response.data.FileName;
                		vm.pathFile = url;
                		saveExcelContent(fileName, vm.pathFile);
                	} else {
                		UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                	UIControlService.unloadLoading();
                });

		}

		function saveExcelContent(fileName, PathFile) {
			vm.newExcel = [];
			UIControlService.loadLoading("LOADING");
			ExcelReaderService.readExcel(vm.fileUpload,
                function (reply) {
                	UIControlService.unloadLoading();
                	if (reply.status === 200) {
                		var excelContents = reply.data;
                		var Sheet1 = excelContents[Object.keys(excelContents)[0]]; /*untuk baca nama sheet*/
                		console.info(Sheet1);
                		var countproperty = numAttrs(Sheet1[0]);
                		for (var i = 1; i < Sheet1.length; i++) {
                			if (Sheet1[i].Column1 !== null) {
                				var objExcel = {
                					RFQCode: Sheet1[i].Column1,
                					RFQName: Sheet1[i].Column2,
                					SAPContractNo: Sheet1[i].Column3,
                					VendorCode: Sheet1[i].Column4,
                					StartContractDate: Sheet1[i].Column5,
                					Duration: Sheet1[i].Column6.toFixed(0),
                					TaxCode: Sheet1[i].Column7,
                					Material: Sheet1[i].Column8,
                					POLongText: Sheet1[i].Column9,
                					ManufacturerName: Sheet1[i].Column10,
                					PartNumber: Sheet1[i].Column11,
                					AnnualUsage: Sheet1[i].Column12,
                					UnitOfMeasure: Sheet1[i].Column13,
                					Currency: Sheet1[i].Column14,
                					UnitPrice: Sheet1[i].Column15,
                					TotalPrice: Sheet1[i].Column16,
                					LeadTime: Sheet1[i].Column17,
                					CountryOfOrigin: Sheet1[i].Column18,
                					PgrCode: Sheet1[i].Column19,
                					BudgetContract: Sheet1[i].Column20,
                					SpendingContract: Sheet1[i].Column21,
                					RemainingBudget: Sheet1[i].Column22
                				};
                				vm.newExcel.push(objExcel);
                			}
                		}
                		uploadFileExcel(vm.newExcel);
                	}
                });
		}

		vm.uploadFileExcel = uploadFileExcel;
		function uploadFileExcel(data) {
			UIControlService.loadLoading("LOADING");
			dataUploadMigrasiFPAService.Insert(data, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MSG_SUC_SAVE");
					vm.init();
				}
				else {
					UIControlService.msg_growl("error", "MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoading();
			});
		}

	}
})();
