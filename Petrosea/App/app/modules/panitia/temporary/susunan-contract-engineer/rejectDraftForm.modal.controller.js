(function () {
    'use strict';

    angular.module("app")
    .controller("contractEngineerRejectCRDraftModalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'ContractEngineerService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, ContractEngineerService) {

        var vm = this;
        var contractRequisitionID = item.contractRequisitionID;
        vm.tenderCode = item.tenderCode;
        vm.tenderName = item.tenderName;
        vm.remark = "";

        vm.init = init;
        function init() {
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reject = reject;
        function reject() {
            bootbox.confirm($filter('translate')('CONFIRM.REJECT_CRDRAFT'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    ContractEngineerService.rejectByCE({
                        ContractRequisitionId: contractRequisitionID,
                        CEApprovalRemark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_REJECT_CRDRAFT'));
                        $uibModalInstance.close();
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_CRDRAFT'));
                    });
                }
            });
        }
    }
})();