﻿(function () {
    'use strict';

    angular.module("app").controller("ContractEngineerCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$filter',
        'ContractEngineerService', 'RoleService', 'UIControlService', '$uibModal', '$state'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $filter,
        ContractEngineerService, RoleService, UIControlService, $uibModal, $state) {

        var vm = this;
        var page_id = 141;
        vm.data = "";
        vm.list = [];
        vm.contract = {};
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.fullSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.kata = new Kata("");
        vm.init = init;
        vm.selectedRFQ = "";
        vm.jLoad = jLoad;
        vm.tambah = tambah;
        vm.isCalendarOpened = [false, false, false, false];
        vm.flag = false;

        vm.statusLabels = [];
        vm.statusLabels["CR_DRAFT"] = '';
        vm.statusLabels["CR_PROCESS_0.5"] = 'STATUS.ON_PROCESS';
        vm.statusLabels["CR_PROCESS_1"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_PROCESS_2"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_PROCESS_3"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_REJECT_0.5"] = 'STATUS.REJECTED';
        vm.statusLabels["CR_REJECT_1"] = '';
        vm.statusLabels["CR_REJECT_2"] = '';
        vm.statusLabels["CR_REJECT_3"] = '';
        vm.statusLabels["CR_APPROVED"] = 'STATUS.APPROVED';

        function init() {
            $translatePartialLoader.addPart('contract-engineer');
            //UIControlService.loadLoading("Silahkan Tunggu...");
            convertToDate();
            jLoad(1,null,"");

        }

        vm.jLoad = jLoad;
        function jLoad(current, colom, input, type, key) {
            key = UIControlService.getStrDate(key);
            //console.info(key);
            if ((colom === '0' || colom === '1' || colom === '2' || colom === '7') && input === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.NO_KEYWORD"); return;
               
            }
            else if (colom === '4' && key === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.NO_DATE"); return;
            }
            else if ((colom === '3' || colom === '5') && type === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.NO_KEYWORD"); return;
            }
            vm.list = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            if (colom === '4') {
                vm.contract = {
                    Offset: offset,
                    Limit: vm.pageSize,
                    Date1: key,
                    column: colom
                }
            }
            else if (colom === '3' || colom === '5') {
                vm.contract = {
                    Offset: offset,
                    Limit: vm.pageSize,
                    Status: type,
                    column: colom
                }
            }
            else {
                if (colom === 6) colom = 0;
                vm.contract = {
                    Offset: offset,
                    Limit: vm.pageSize,
                    Keyword: input == null ? "": input,
                    column: colom
                }
            }
            UIControlService.loadLoading("");
            ContractEngineerService.select(vm.contract, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data.List;
                    //console.info(vm.list);
                    vm.totalItems = data.Count;
                    vm.totalItems = Number(data.Count);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Susunan Contract Engineer" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.add = add;
        function add(data) {
            vm.input = undefined;
            vm.selectedType = undefined;
            vm.SetDate = undefined;
            if (data === '6') {
                vm.flag = false;
            }
            else vm.flag = true;
        }


        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        function convertAllDateToString() { // TIMEZONE (-)
            if (vm.SetDate) {
                vm.SetDate = UIControlService.getStrDate(vm.SetDate);
            }
        };

        //Supaya muncul di date picker saat awal load
        function convertToDate() {
            if (vm.SetDate) {
                vm.SetDate = new Date(Date.parse(vm.SetDate));
            }
        }
        
        vm.Dokumen = Dokumen;
        function Dokumen(data) {
            $state.transitionTo('contract-requisition-contract', { contractRequisitionId: data.ContractRequisitionID });
        }

        vm.tambah = tambah;
        function tambah(data) {
            //console.info("masuk form add/edit");
            var data = {
                act: 1,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/departemen/formDepartemen.html',
                controller: 'departemenModalCtrl',
                controllerAs: 'formDepartCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.commite = commite;
        function commite(data) {
            //console.info(data);
            var data = {
                act: 0,
                item: data,
                dataTemp : data.flag
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/temporary/susunan-contract-engineer/commite-modal.html?v=1.000002',
                controller: 'CommitteeCEModalCtrl',
                controllerAs: 'CommitteeModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.sendToApprove = sendToApprove;
        function sendToApprove(dt) {
            bootbox.confirm($filter('translate')('CONFIRM.SEND_FOR_APPROVAL'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    ContractEngineerService.approveByCE({
                        ContractRequisitionId: dt.ContractRequisitionID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
                        init();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                        if (error.Message.substr(0, 4) === "ERR_") {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.' + error.Message));
                        }
                    });
                }
            });
        }

        vm.reject = reject;
        function reject(dt) {
            var data = {
                contractRequisitionID: dt.ContractRequisitionID,
                tenderCode: dt.requisition.TenderCode,
                tenderName: dt.requisition.ProjectTitle
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/temporary/susunan-contract-engineer/rejectDraftForm.modal.html',
                controller: 'contractEngineerRejectCRDraftModalCtrl',
                controllerAs: 'rejectCRDraftCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.detailContract = detailContract;
        function detailContract(dt) {
            //console.info("ss");
            $state.transitionTo('detail-contract-requisition-contract', { contractRequisitionId: dt.ContractRequisitionID });
        };

        vm.detailApproval = detailApproval;
        function detailApproval(dt) {
            console.log(dt.ContractRequisitionID);
            var item = { contractRequisitionId: dt.ContractRequisitionID };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-contract-requisition/detailApproval.modal.html?v=1.000002',
                controller: 'viewDraftApprovalCtrl',
                controllerAs: 'viewDAppCtrl',
                resolve: { item: function () { return item; } }
            });
        };
    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

