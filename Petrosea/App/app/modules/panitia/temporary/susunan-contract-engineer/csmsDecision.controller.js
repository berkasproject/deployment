(function () {
    'use strict';

    angular.module("app")
    .controller("csmsDecisionCtrl", ctrl);
    
    ctrl.$inject = ['$state', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataContractRequisitionService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataContractRequisitionService, UIControlService) {

        var vm = this;
        var contractRequisitionId = Number($stateParams.contractRequisitionId);
        vm.contractRequisitionId = Number($stateParams.contractRequisitionId);
        var loadmsg = "";

        vm.projectTitle = "";
        vm.contractSponsor = "";
        vm.projectManager = 0;
        vm.projectManagerName = "";
        vm.operatingOrCapital = "0";
        vm.departmentId = 0;
        vm.departmentName = "";
        vm.departments = [];

        vm.csms = {
            Area: "",
            IsSupervised: "0",
            IsAssessed: "0",
            IsHighRisk: "0"
        };
        vm.isTenderVerification = true;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            $translate.refresh().then(function () {
                loadmsg = $filter('translate')('MESSAGE.LOADING');
            });
            vm.loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            if (contractRequisitionId > 0) {
                UIControlService.loadLoading(loadmsg);
                DataContractRequisitionService.SelectCSMS({
                    ContractRequisitionId: contractRequisitionId
                }, function (reply) {
                    if (reply.status === 200) {
                        vm.csms = reply.data;
                        vm.csms.IsSupervised = vm.csms.IsSupervised ? "1" : "0";
                        vm.csms.IsAssessed = vm.csms.IsAssessed ? "1" : "0";
                        vm.csms.IsHighRisk = vm.csms.IsHighRisk ? "1" : "0";

                        var contractRequisition = vm.csms.ContractRequisition;
                        vm.projectTitle = contractRequisition.ProjectTitle;
                        vm.contractSponsor = contractRequisition.ContractSponsor;
                        vm.projectManager = contractRequisition.ProjectManager;
                        vm.projectManagerName = contractRequisition.ProjectManagerName;
                        vm.departmentId = contractRequisition.DepartmentID;
                        vm.departmentName = contractRequisition.DepartmentName;
                        vm.locationStateName = contractRequisition.LocationStateName;
                        vm.locationCityName = contractRequisition.LocationCityName;
                        if (contractRequisition.OperatingOrCapital !== null) {
                            vm.OperatingOrCapital = contractRequisition.OperatingOrCapital + '';
                        }
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CSMS'));
                    }
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CSMS'));
                });
            }
        };

        vm.cancel = cancel;
        function cancel() {
            if (contractRequisitionId > 0) {
                $state.transitionTo('detail-contract-requisition-contract', { contractRequisitionId: contractRequisitionId });
            } else {
                $state.transitionTo('data-contract-requisition-contract');
            }
        };
    }
})();