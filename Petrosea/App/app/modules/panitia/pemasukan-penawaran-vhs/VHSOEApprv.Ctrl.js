﻿(function () {
	'use strict';

	angular.module("app").controller("VHSOEApprvCtrl", ctrlApproval);

	ctrlApproval.$inject = ['item', 'UIControlService', 'PPVHSAdminService', '$uibModalInstance', '$translate','$translatePartialLoader'];
	function ctrlApproval(item, UIControlService, PPVHSAdminService, $uibModalInstance, $translate, $translatePartialLoader) {
		var vm = this;
		vm.apprvs = [];
		vm.StepID = item;

		vm.getDetailApproval = getDetailApproval;
		function getDetailApproval() {
			$translatePartialLoader.addPart('pemasukkan-penawaran-vhs');
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			PPVHSAdminService.detailApproval({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.apprvs = reply.data;
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
				} else {
					$.growl.error({ message: "Get Approval Failed." });
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoadingModal();
			});
		}

		vm.closeDetailApprv = closeDetailApprv;
		vm.cancel = closeDetailApprv;
		function closeDetailApprv() {
			$uibModalInstance.close();
		}
	}
})();