(function () {
	'use strict';

	angular.module("app").controller("detailApprovalSignOffCtrl", ctrl);

	ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'GoodsAwardService'];
	/* @ngInject */
	function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, GoodsAwardService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.IDApproval = item.IDApproval;
		vm.ID = item.ID;
		vm.flag = item.flag;
		vm.Status = item.Status;
		vm.crApps = [];
		vm.employeeFullName = "";
		vm.employeeID = 0;
		vm.information = "";
		vm.flagEmp = item.flag;
		vm.TenderRefID = item.TenderRefID

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('goods-award');
			//$translatePartialLoader.addPart('verifikasi-tender');
			loadData();
		};

		vm.loadData = loadData;
		function loadData() {
			vm.crApps = [];
			UIControlService.loadLoading(loadmsg);
			GoodsAwardService.GetApproval({
				ID: vm.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = reply.data;
					for (var i = 0; i < vm.list.length; i++) {
						vm.crApps.push({
							IsActive: vm.list[i].IsActive,
							ID: vm.list[i].ID,
							EmployeeID: vm.list[i].EmployeeID,
							ApprovalDate: UIControlService.convertDateTime(vm.list[i].ApprovalDate),
							ApprovalStatus: vm.list[i].ApprovalStatus,
							Remark: vm.list[i].Remark,
							EmployeeFullName: vm.list[i].MstEmployee.FullName + ' ' + vm.list[i].MstEmployee.SurName,
							EmployeePositionName: vm.list[i].MstEmployee.PositionName,
							EmployeeDepartmentName: vm.list[i].MstEmployee.DepartmentName,
							LevelInfo: vm.list[i].MstEmployee.LevelInfo,
							DelegatedEmployeeName: vm.list[i].DelegateEmployeeName,
							IsHighPriority: vm.list[i].IsHighPriority
						});
					}
				} else {
					UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
			});
		}

		vm.approve = approve;
		function approve() {
			sendApproval(1);
		}

		vm.reject = reject;
		function reject() {
			sendApproval(0);
		}

		function sendApproval(approvalStatus) {
			vm.flagApprove = approvalStatus;
			console.info("idApproval" + vm.IDApproval);
			UIControlService.loadLoadingModal(loadmsg);

			GoodsAwardService.approve({
				IDApproval: vm.IDApproval,
				ID: vm.ID,
				Status: approvalStatus,
				Remark: vm.information,
				flagEmp: vm.flagEmp,
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUC_APPROVE'));
					console.info(vm.flagApprove);
					$uibModalInstance.close(vm.ID, vm.flagApprove);

				} else {
					UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
				}
			}, function (error) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
			});
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
		function sendMail() {
			UIControlService.loadLoading("MESSAGE.LOADING_SEND_EMAIL");
			GoodsAwardService.sendMail({ ID: vm.ID }, function (response) {
				console.info(response);
				UIControlService.unloadLoading();
				if (response.status == 200) {
					UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT");
					$uibModalInstance.close();
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
			});
		}
	}
})();