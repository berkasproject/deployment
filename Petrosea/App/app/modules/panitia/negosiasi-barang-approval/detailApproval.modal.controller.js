(function () {
	'use strict';

	angular.module("app")
    .controller("detailApprovalGoodsAppCtrl", ctrl);

	ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'NegosiasiBarangService'];
	/* @ngInject */
	function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, NegosiasiBarangService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.ID = item.ID;
		vm.flag = item.flag;
		vm.flagNego = item.flagNego;
		vm.Status = item.Status;
		vm.crApps = [];
		vm.employeeFullName = "";
		vm.employeeID = 0;
		vm.information = "";
		vm.flagEmp = item.flag;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('verifikasi-tender');
			$translatePartialLoader.addPart('negosiasi-barang');
			loadData();
		};

		vm.loadData = loadData;
		function loadData() {
			vm.crApps = [];
			UIControlService.loadLoading(loadmsg);
			if (vm.Status == 0) {
				var data = {
					GoodsNegoId: vm.ID
				}
			}
			else {
				var data = {
					GoodsNegoId: vm.ID.GoodsNegoId
				}
			}
			NegosiasiBarangService.GetApproval(data, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = reply.data;
					vm.list.forEach(function (data) {
						data.ApprovalDate = UIControlService.convertDateTime(data.ApprovalDate);
					})
				} else {
					UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
			});
		}

		//vm.approve = approve;
		//function approve() {
		//	sendApproval(1);
		//}

		//vm.reject = reject;
		//function reject() {
		//	sendApproval(0);
		//}

		//function sendApproval(approvalStatus) {
		//	vm.ID.ApprovalStatus = false;
		//	vm.ID.flagNego = vm.flagNego;
		//	UIControlService.loadLoadingModal(loadmsg);
		//	NegosiasiBarangService.updateApproval(vm.ID, function (reply) {
		//		UIControlService.unloadLoadingModal();
		//		if (reply.status === 200) {
		//			UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCCAPPROVAL'));
		//			sendMail();
		//			$uibModalInstance.close();

		//		} else {
		//			UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
		//		}
		//	}, function (error) {
		//		UIControlService.unloadLoadingModal();
		//		UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
		//	});
		//}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
		function sendMail() {
			vm.ID.ApprovalStatus = false;
			UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
			NegosiasiBarangService.sendMailToBuyer(vm.ID,
                function (response) {
                	console.info(response);
                	UIControlService.unloadLoading();
                	if (response.status == 200) {
                		UIControlService.msg_growl("notice", "MESSAGE.SEND_EMAIL");
                	} else {
                		UIControlService.handleRequestError(response.data);
                	}
                },
                function (response) {
                	UIControlService.handleRequestError(response.data);
                	UIControlService.unloadLoading();
                });
		}
	}
})();