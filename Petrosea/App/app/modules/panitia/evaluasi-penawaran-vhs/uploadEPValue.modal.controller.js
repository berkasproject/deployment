﻿(function () {
    'use strict';

    angular.module("app")
    .controller("uploadVHSEPValueModalController", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', 'item', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranVHSService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, item, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranVHSService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var tenderId = item.tenderId;
        var tenderName = item.tenderName;

        vm.fileUpload;
        var templateJSON;

        vm.init = init;
        function init() {
            templateJSON = [];
        };

        vm.downloadTemplate = downloadTemplate;
        function downloadTemplate() {
            UIControlService.loadLoadingModal("");
            EvaluasiPenawaranVHSService.getItemPRs({
                ID: tenderId,
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                templateJSON = [];

                var itemPRs = reply.data;
                itemPRs.forEach(function (item) {
                    templateJSON.push({
                        ID: item.ID,
                        Material: item.Material,
                        MaterialDescription: item.MaterialDescription,
                        ManufacturerName: item.ManufacturerName,
                        PartNumber: item.PartNumber,
                        AnnualUsage: item.AnnualUsage,
                        UnitOfMeasure: item.UnitOfMeasure,
                        UnitCostEPV: 0
                    });
                })

                JSONToCSVConvertor(templateJSON);
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_ITEM_PRS');
            });
        }

        function JSONToCSVConvertor(JSONData) {

            var ShowLabel = true;

            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = JSONData;
            var CSV = '';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "sep=," + '\n';

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {
                    //console.info(index);
                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }
                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }
                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "Daftar Nilai EPV - " + tenderName;

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload)) {
                upload(vm.fileUpload);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function parseCSVToJSON(file, delimiter) {

            var strDelimiter = delimiter;
            // Create a regular expression to parse the CSV values.
            var objPattern = new RegExp((
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
            // Create an array to hold our data. Give the array
            // a default empty first row.
            var arrData = [[]];
            // Create an array to hold our individual pattern
            // matching groups.
            var arrMatches = null;
            // Keep looping over the regular expression matches
            // until we can no longer find a match.
            while (arrMatches = objPattern.exec(file)) {
                // Get the delimiter that was found.
                var strMatchedDelimiter = arrMatches[1];
                // Check to see if the given delimiter has a length
                // (is not the start of string) and if it matches
                // field delimiter. If id does not, then we know
                // that this delimiter is a row delimiter.
                if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
                    // Since we have reached a new row of data,
                    // add an empty row to our data array.
                    arrData.push([]);
                }
                // Now that we have our delimiter out of the way,
                // let's check to see which kind of value we
                // captured (quoted or unquoted).
                if (arrMatches[2]) {
                    // We found a quoted value. When we capture
                    // this value, unescape any double quotes.
                    var strMatchedValue = arrMatches[2].replace(
                    new RegExp("\"\"", "g"), "\"");
                } else {
                    // We found a non-quoted value.
                    var strMatchedValue = arrMatches[3];
                }
                // Now that we have our value string, let's add
                // it to the data array.
                arrData[arrData.length - 1].push(strMatchedValue);
            }
            var objArray = [];
            for (var i = 1; i < arrData.length; i++) {
                objArray[i - 1] = {};
                for (var k = 0; k < arrData[0].length && k < arrData[i].length; k++) {
                    var key = arrData[0][k];
                    objArray[i - 1][key] = arrData[i][k]
                }
            }

            var items = []
            objArray.forEach(function (obj) {
                if (obj.ID) {
                    items.push({
                        ID: obj.ID,
                        UnitCostEPV: Number(obj.UnitCostEPV)
                    });
                }
            });
            return items;
        }

        function upload(file) {

            var items = parseCSVToJSON(file, ',');
            if (items.length === 0) {
                items = parseCSVToJSON(file, ';');
            }
            if (items.length > 0) {
                UIControlService.loadLoadingModal("");
                EvaluasiPenawaranVHSService.saveItemPRSaveEPValues(items, function (reply) {
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();
                    UIControlService.msg_growl("notice", 'MESSAGE.SUCC_SAVE_EPV')
                }, function (error) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE_EPV');
                });
            } else {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_FILE_EPV');
            }
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();