(function () {
    'use strict';

    angular.module("app")
    .controller("viewVHSItemPRAwardCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranVHSService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranVHSService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.itemPRs = [];
        vm.tenderName = item.tenderName;
        vm.vendorName = item.evaluationVendor.VendorName;

        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.count = 0;

        vm.init = init;
        function init() {
            loadItem();
        };       

        vm.loadItem = loadItem;
        function loadItem(){
            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiPenawaranVHSService.getAwardedItemPR({
                column: item.rfqvhsID, //RFQVHSId
                Status: item.evaluationVendor.VendorID, //VendorID
                Offset: (vm.pageNumber - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.itemPRs = reply.data.List;
                vm.count = reply.data.Count;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ITEM_PRS'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();