﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailEvaluasiPenawaranVHSController", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranVHSService', 'DataPengadaanService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranVHSService, DataPengadaanService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.stepID = Number($stateParams.StepID);
        vm.tenderRefID = Number($stateParams.TenderRefID);
        vm.procPackType = Number($stateParams.ProcPackType);

        vm.tenderStepData = {};
        vm.itemPRs = [];
        vm.offerEntries = [];
        vm.evaluation = [];
        vm.evaluationVendors = [];
        vm.scoreDetails = [];
        vm.repeater = [];
        vm.rfqvhs = {};
        vm.otherCosts = [];
        vm.criterias = [];
        vm.epvOtherCosts = [];
        
        vm.keyword = "";
        vm.column = 1;
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.count = 0;

        vm.isProcess;
        vm.isItemize = false;
        vm.paymentTermsOptions = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-penawaran-vhs');
            UIControlService.loadLoading("");

            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.tenderRefID,
                ProcPackageType: vm.procPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            DataPengadaanService.getEvaluator({
                TenderRefID: vm.tenderRefID
            }, function (reply) {
                if (reply) {
                    vm.evaluatorName = reply.data;
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl('error', "MESSAGE.ERR_NOT_EVALUATOR");
                    vm.kembali();
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "MESSAGE.ERR_GET_EVALUATOR");
                UIControlService.msg_growl('error', "MESSAGE.ERR_NOT_EVALUATOR");
                vm.kembali();
            });

            EvaluasiPenawaranVHSService.isItemize({
                ID: vm.stepID
            }, function (reply) {
                vm.isItemize = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_TENDER_OPTION');
            });

            EvaluasiPenawaranVHSService.getPaymentTermOptions({
                ID: vm.stepID
            }, function (reply) {
                vm.paymentTermsOptions = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_PAYMENT_TERMS');
            });

            EvaluasiPenawaranVHSService.getCurrencyOptions(function (reply) {
                vm.currencyOptions = reply.data;
                loadEPV();
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_CURRENCIES');
            });

            loadTotalHistoryAndEPV();
            loadData();
            /*
            EvaluasiPenawaranVHSService.isless3approved({
                ID: vm.stepID
            }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.data === true) {
                    DataPengadaanService.StepIsNotStarted({
                        ID: vm.stepID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.data === false) {
                            loadData();
                        } else {
                            UIControlService.msg_growl("error", 'MESSAGE.STEP_IS_NOT_STARTED');
                            $state.transitionTo('evaluasi-penawaran-vhs', { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_STEP_TIME');
                    });
                } else {
                    UIControlService.msg_growl("error", 'LESS_3_NOT_APPROVED');
                    $state.transitionTo('evaluasi-penawaran-vhs', { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_STEP_APPROVAL');
            });
            */
        };
        
        function loadEPV() {
            EvaluasiPenawaranVHSService.getEPVByStep({
                ID: vm.stepID
            }, function (reply) {
                if (reply.data) {
                    var epv = reply.data;
                    vm.epvCurrencyId = epv.CurrencyId;
                    vm.epvCurrencySymbol = epv.CurrencySymbol;
                    vm.epvRateToUSD = epv.RateToUSD;
                }
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_EPV');
            });
        }

        function loadTotalHistoryAndEPV() {
            EvaluasiPenawaranVHSService.getTotalHistoricalValue({
                ID: vm.stepID
            }, function (reply) {
                vm.totalHistoricalValue = reply.data.TotalValueHistorical;
                vm.totalHistoricalValueInUSD = reply.data.TotalValueHistoricalInUSD;
                vm.totalEPValue = reply.data.TotalValueEPV;
                vm.totalEPValueInUSD = reply.data.TotalValueEPVInUSD;

                historicalCostChange();
                epvCostChange();

            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_HISTORY_TOTAL');
            });
        }

        vm.uploadSPV = uploadSPV;
        function uploadSPV() {
            if (vm.tenderStepData.TenderID) {
                var item = {
                    tenderId: vm.tenderStepData.TenderID,
                    tenderName: vm.tenderStepData.tender.TenderCode + " - " + vm.tenderStepData.tender.TenderName
                };
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/evaluasi-penawaran-vhs/uploadEPValue.modal.html',
                    controller: 'uploadVHSEPValueModalController',
                    controllerAs: 'uploadEPVModalCtrl',
                    resolve: { item: function () { return item; } }
                });
                modalInstance.result.then(function () {
                    loadPagedOEDetail();
                });
            }
        }

        vm.epvCurrencyChange = epvCurrencyChange;
        function epvCurrencyChange() {
            UIControlService.loadLoading("");
            EvaluasiPenawaranVHSService.saveEPV({
                TenderStepDataId: vm.stepID,
                CurrencyId: vm.epvCurrencyId
            }, function (reply) {
                UIControlService.unloadLoading();
                loadEPV();
                loadPagedOEDetail();
                loadTotalHistoryAndEPV();
                UIControlService.msg_growl("notice", 'MESSAGE.SUCC_SAVE_EPV_CURRENCY')
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE_EPV_CURRENCY');
            });
        }

        function loadData() {
            UIControlService.loadLoading("");

            DataPengadaanService.StepHasEnded({
                ID: vm.stepID
            }, function (reply) {
                vm.isProcess = true;
                if (reply.data === true) {
                    vm.isProcess = false;
                }
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_STEP_TIME');
            });

            EvaluasiPenawaranVHSService.getrfqvhs({
                ID: vm.stepID
            }, function (reply) {
                vm.rfqvhs = reply.data;
                vm.isFPA = (vm.rfqvhs.RFQType === 2);
                DataPengadaanService.GetStepByID({
                    ID: vm.stepID
                }, function (reply) {
                    vm.tenderStepData = reply.data;
                    //vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
                        EvaluasiPenawaranVHSService.getOfferEntries({
                            ID: vm.tenderStepData.TenderID
                        }, function (reply) {
                            vm.offerEntries = reply.data;
                            vm.offerEntries.forEach(function (entry) {
                                entry.OfferTotalCostInUSD = entry.OfferTotalCost * entry.ExchangeRateToUSD;
                                entry.OfferTotalCostInUSD = entry.OfferTotalCostInUSD.toFixed(2);

                                entry.OfferTotalCostOriginalInUSD = entry.OfferTotalCostOriginal * entry.ExchangeRateToUSD;
                                entry.OfferTotalCostOriginalInUSD = entry.OfferTotalCostOriginalInUSD.toFixed(2);
                                /*
                                entry.VHSOfferEntryDetails[0].detail.forEach(function (det) {
                                    det.TotalPriceInUSD = det.TotalPrice * entry.ExchangeRateToUSD;
                                    det.TotalPriceInUSD = det.TotalPriceInUSD.toFixed(2);
                                });
                                */
                                //entry.FreightCostPercent = entry.FreightDeliveryCost;
                                setFreightDeliveryCostAndTimes(entry);
                            });
                            calculateAllTotalPurchaseValue();
                            var length = vm.offerEntries.length;
                            vm.repeater = [];
                            for (var i = 0; i < length * 3; i++) {
                                vm.repeater.push(i);
                            }
                            UIControlService.unloadLoading();
                            loadPagedOEDetail();
                            loadEvaluationData();
                        }, function (error) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OFFERS');
                        });
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.loadPagedOEDetail = loadPagedOEDetail;
        function loadPagedOEDetail() {
            EvaluasiPenawaranVHSService.getPagedItemPRs({
                Parameter: vm.tenderStepData.TenderID,
                Keyword: vm.keyword,
                column: vm.column,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
                Limit: vm.pageSize
            }, function (reply) {
                vm.itemPRs = reply.data.List;
                vm.count = reply.data.Count;
                vm.offerEntries.forEach(function (entry) {
                    EvaluasiPenawaranVHSService.getPagedOEDetail({
                        Parameter: entry.ID,
                        Keyword: vm.keyword,
                        column: vm.column,
                        Offset: (vm.pageNumber - 1) * vm.pageSize,
                        Limit: vm.pageSize
                    }, function (reply) {
                        var oeDetail = reply.data;
                        entry.VHSOfferEntryDetails[0].detail = oeDetail;
                        entry.VHSOfferEntryDetails[0].detail.forEach(function (det) {
                            det.TotalPriceInUSD = det.TotalPrice * entry.ExchangeRateToUSD;
                            det.TotalPriceInUSD = det.TotalPriceInUSD.toFixed(2);
                        });
                    }, function (error) {
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OFFERS');
                    });
                });
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_ITEM_PRS');
            });
        }

        vm.setItemPRAward = setItemPRAward;
        function setItemPRAward(itemPRIndex) {
            var itemPR = vm.itemPRs[itemPRIndex];

            var item = {
                rfqvhsID: vm.tenderRefID,
                itemPR: itemPR,
                itemPRIndex: itemPRIndex,
                offerEntries: vm.offerEntries,
                criterias: vm.criterias,
                evaluationVendors: vm.evaluationVendors,
                isProcess: vm.isProcess,
                isAllowedEdit: vm.isAllowedEdit
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-vhs/setItemPRAward.modal.html?v=1.000006',
                controller: 'setVHSItemPRAwardCtrl',
                controllerAs: 'prAwardCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                loadPagedOEDetail();
            });
        }

        function loadEvaluationData() {
            UIControlService.loadLoading(loadmsg);
            EvaluasiPenawaranVHSService.getByTenderStepData({
                ID: vm.stepID,
                TenderID: vm.tenderStepData.TenderID
            }, function (reply) {
                vm.evaluation = reply.data;
                vm.evaluationVendors = vm.evaluation.VHSOfferEvaluationVendors;
                vm.epvOtherCosts = vm.evaluation.VHSOEvaluationEPVOtherCosts;
                if (vm.evaluation.ID > 0) { //Jika sudah pernah dilakukan evaluasi
                    EvaluasiPenawaranVHSService.getOtherCosts({
                        ID: vm.evaluation.ID
                    }, function (reply) {
                        vm.otherCosts = reply.data;
                        EvaluasiPenawaranVHSService.getCriterias({
                            ID: vm.evaluation.ID
                        }, function (reply) {
                            vm.criterias = reply.data;
                            sumCriteriaWeights();
                            vm.costChangeAll();
                            //setFreightDeliveryCostAndTimes();
                            calculateLeadTimeScores();
                            calculatePriceScores();
                            vm.evaluationVendors.forEach(function (ev) {
                                var scoreDetIndex = 0;
                                ev.VHSOEvaluationVendorScoreDetails.forEach(function (scoreDet) {
                                    var weight = vm.criterias[scoreDetIndex++].Weight;
                                    scoreDet.CalculatedScore = scoreDet.Score * weight / 100;
                                    scoreDet.CalculatedScore = scoreDet.CalculatedScore.toFixed(2);
                                })
                            })
                            calculateTotalScores();
                            UIControlService.unloadLoading();
                        }, function (error) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_CRITERIA');
                        });
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OTHER_COST');
                    });
                } else { //Jika belum
                    vm.evaluation.TenderStepDataID = vm.stepID;
                    vm.evaluationVendors = [];
                    vm.epvOtherCosts = [];
                    EvaluasiPenawaranVHSService.getOtherCostsbyDefault({
                        ID: vm.tenderStepData.TenderID
                    }, function (reply) {
                        vm.otherCosts = reply.data;
                        vm.otherCosts.forEach(function (otherCost) {
                            var epvOtherCost = {
                                OtherCostID: otherCost.OtherCostID,
                                OtherCost: 0,
                                OtherCostPercent: 0
                            }
                            vm.epvOtherCosts.push(epvOtherCost);
                        });
                        EvaluasiPenawaranVHSService.getCriteriasFromEMDC({
                            ID: vm.tenderStepData.TenderID
                        }, function (reply) {
                            vm.criterias = reply.data;
                            sumCriteriaWeights();
                            EvaluasiPenawaranVHSService.getVerifiedDocScore(vm.offerEntries, function (reply) {
                                var verifiedDocDetails = reply.data;
                                var vendorIndex = 0;
                                vm.offerEntries.forEach(function (entry) {
                                    var evaluationVendor = {
                                        VHSOfferEntryID: entry.ID,
                                        Score: 0,
                                        VHSOEvaluationVendorOtherCostDetails: [],
                                        VHSOEvaluationVendorScoreDetails: []
                                    };
                                    vm.evaluationVendors.push(evaluationVendor);
                                    vm.otherCosts.forEach(function (otherCost) {
                                        var otherCostDet = {
                                            OtherCostID: otherCost.OtherCostID,
                                            OtherCost: 0,
                                            OtherCostPercent: 0
                                        }
                                        evaluationVendor.VHSOEvaluationVendorOtherCostDetails.push(otherCostDet);
                                    });
                                    var scoreDetIndex = 0;
                                    vm.criterias.forEach(function (criteria) {
                                        var scoreDet = {
                                            CriteriaID: criteria.CriteriaID,
                                            Score: 0,
                                            SelectedECOptionID: null,
                                            SelectedECOptionName: null,
                                        };
                                        verifiedDocDetails.forEach(function (docDet) { //Set skor default dari tahapan verifikasi dokumen
                                            if (docDet.CriteriaId == criteria.CriteriaID && docDet.VHSOfferEntry.VHSOEid == entry.ID) {
                                                scoreDet.Score = docDet.Score;
                                                scoreDet.SelectedECOptionID = docDet.SelectedECOptionID;
                                            }
                                        });
                                        evaluationVendor.VHSOEvaluationVendorScoreDetails.push(scoreDet);
                                        optionChange(vendorIndex, scoreDetIndex);
                                        scoreDetIndex++;
                                    });
                                    vendorIndex++;
                                });
                                autoSetTechnicalAndVPScore();
                                assignLBIPNIScores();
                                //setFreightDeliveryCostAndTimes();
                                calculateLeadTimeScores();
                                calculatePriceScores();
                                calculateTotalScores();
                                UIControlService.unloadLoading();
                            }, function (error) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_VERIFICATION_SCORE');
                            });
                        }, function (error) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_CRITERIA');
                        });
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OTHER_COST');
                    });
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        }

        vm.costChangeAll = costChangeAll;
        function costChangeAll() {
            calculateAllTotalPurchaseValue();
            calculatePriceScores();
            calculateTotalScores();

            historicalCostChange();
            epvCostChange();
        }

        vm.costChange = costChange;
        function costChange(index) {
            calculateTotalPurchaseValue(index);
            calculatePriceScores();
            calculateTotalScores();
        }

        vm.historicalCostChange = historicalCostChange;
        function historicalCostChange() {
            vm.evaluation.TotalHistoricalValue = vm.totalHistoricalValue;
            vm.evaluation.TotalHistoricalValueInUSD = vm.totalHistoricalValueInUSD;
            var rate = vm.totalHistoricalValue > 0 ? vm.totalHistoricalValueInUSD / vm.totalHistoricalValue : 0;

            if (vm.evaluation.FreightCostUnitName === 'COST_UNIT_PERCENT') {

                vm.evaluation.FreightCostPercentHistoricalValue = vm.evaluation.FreightCostPercentHistorical ?
                    vm.totalHistoricalValue * vm.evaluation.FreightCostPercentHistorical / 100 : 0;
                vm.evaluation.TotalHistoricalValue += vm.evaluation.FreightCostPercentHistoricalValue;

                vm.evaluation.FreightCostPercentHistoricalValueInUSD = vm.evaluation.FreightCostPercentHistorical ?
                   vm.totalHistoricalValueInUSD * vm.evaluation.FreightCostPercentHistorical / 100 : 0;
                vm.evaluation.TotalHistoricalValueInUSD += vm.evaluation.FreightCostPercentHistoricalValueInUSD;

            } else if (vm.evaluation.FreightCostUnitName === 'COST_UNIT_CURRENCY') {
                vm.evaluation.TotalHistoricalValue += Number(vm.evaluation.FreightCostHistorical);

                vm.evaluation.FreightCostHistoricalInUSD = Number(vm.evaluation.FreightCostHistorical) * rate;
                vm.evaluation.TotalHistoricalValueInUSD += vm.evaluation.FreightCostHistoricalInUSD;
                vm.evaluation.FreightCostHistoricalInUSD = vm.evaluation.FreightCostHistoricalInUSD.toFixed(2);
            }

            vm.evaluation.CustomsDutyCostPercentHistoricalValue = vm.evaluation.CustomsDutyCostPercentHistorical ?
                    vm.totalHistoricalValue * vm.evaluation.CustomsDutyCostPercentHistorical / 100 : 0;
            vm.evaluation.TotalHistoricalValue += vm.evaluation.CustomsDutyCostPercentHistoricalValue;

            vm.evaluation.CustomsDutyCostPercentHistoricalValueInUSD = vm.evaluation.CustomsDutyCostPercentHistorical ?
                    vm.totalHistoricalValueInUSD * vm.evaluation.CustomsDutyCostPercentHistorical / 100 : 0;
            vm.evaluation.TotalHistoricalValueInUSD += vm.evaluation.CustomsDutyCostPercentHistoricalValueInUSD;
        }

        vm.epvCostChange = epvCostChange;
        function epvCostChange() {
            vm.evaluation.TotalEPVValue = vm.totalEPValue;
            vm.evaluation.TotalEPVValueInUSD = vm.totalEPValueInUSD;
            var rate = vm.totalEPValue > 0 ? vm.totalEPValueInUSD / vm.totalEPValue : 0;

            if (vm.evaluation.FreightCostUnitName === 'COST_UNIT_PERCENT') {

                vm.evaluation.FreightCostPercentEPVValue = vm.evaluation.FreightCostPercentEPV ?
                    vm.totalEPValue * vm.evaluation.FreightCostPercentEPV / 100 : 0;
                vm.evaluation.TotalEPVValue += vm.evaluation.FreightCostPercentEPVValue;

                vm.evaluation.FreightCostPercentEPVValueInUSD = vm.evaluation.FreightCostPercentEPV ?
                   vm.totalEPValueInUSD * vm.evaluation.FreightCostPercentEPV / 100 : 0;
                vm.evaluation.TotalEPVValueInUSD += vm.evaluation.FreightCostPercentEPVValueInUSD;

            } else if (vm.evaluation.FreightCostUnitName === 'COST_UNIT_CURRENCY') {
                vm.evaluation.TotalEPVValue += Number(vm.evaluation.FreightCostEPV);
                
                vm.evaluation.FreightCostEPVInUSD = Number(vm.evaluation.FreightCostEPV) * rate;
                vm.evaluation.TotalEPVValueInUSD += vm.evaluation.FreightCostEPVInUSD;
                vm.evaluation.FreightCostEPVInUSD = vm.evaluation.FreightCostEPVInUSD.toFixed(2);
            }

            vm.evaluation.CustomsDutyCostPercentEPVValue = vm.evaluation.CustomsDutyCostPercentEPV ?
                    vm.totalEPValue * vm.evaluation.CustomsDutyCostPercentEPV / 100 : 0;
            vm.evaluation.TotalEPVValue += vm.evaluation.CustomsDutyCostPercentEPVValue;

            vm.evaluation.CustomsDutyCostPercentEPVValueInUSD = vm.evaluation.CustomsDutyCostPercentEPV ?
                    vm.totalEPValueInUSD * vm.evaluation.CustomsDutyCostPercentEPV / 100 : 0;
            vm.evaluation.TotalEPVValueInUSD += vm.evaluation.CustomsDutyCostPercentEPVValueInUSD;

            var otherCostDetIndex = 0;
            vm.otherCosts.forEach(function (cost) {
                var otherCostDet = vm.epvOtherCosts[otherCostDetIndex];
                if (cost.OtherCostUnitName === 'COST_UNIT_PERCENT') {

                    otherCostDet.OtherCostPercentValue = otherCostDet.OtherCostPercent ?
                        vm.totalEPValue * otherCostDet.OtherCostPercent / 100 : 0;
                    vm.evaluation.TotalEPVValue += otherCostDet.OtherCostPercentValue;

                    otherCostDet.OtherCostPercentValueInUSD = otherCostDet.OtherCostPercent ?
                        vm.totalEPValueInUSD * otherCostDet.OtherCostPercent / 100 : 0;
                    vm.evaluation.TotalEPVValueInUSD += otherCostDet.OtherCostPercentValueInUSD;

                } else if (cost.OtherCostUnitName === 'COST_UNIT_CURRENCY') {
                    vm.evaluation.TotalEPVValue += Number(otherCostDet.OtherCost);

                    otherCostDet.OtherCostInUSD = Number(otherCostDet.OtherCost) * rate;
                    vm.evaluation.TotalEPVValueInUSD += otherCostDet.OtherCostInUSD;
                    otherCostDet.OtherCostInUSD = otherCostDet.OtherCostInUSD.toFixed(2);
                }
                otherCostDetIndex++;
            });
        }

        function calculateAllTotalPurchaseValue() {
            for (var i = 0; i < vm.offerEntries.length;i++) {
                calculateTotalPurchaseValue(i);
            }
        }

        function calculateTotalPurchaseValue(index) {
            vm.offerEntries[index].TotalPurchaseValue = vm.offerEntries[index].OfferTotalCost;

            vm.offerEntries[index].OfferTotalCostPrevPercentDiff = vm.evaluation.PreviousOfferTotal > 0 ?
                (100 * vm.offerEntries[index].OfferTotalCost * vm.offerEntries[index].ExchangeRateToUSD / vm.evaluation.PreviousOfferTotal) - 100 : 0;

            if (vm.evaluation.FreightCostUnitName === 'COST_UNIT_PERCENT') {
                vm.offerEntries[index].FreightCostPercentValue = vm.offerEntries[index].FreightCostPercent ?
                    vm.offerEntries[index].OfferTotalCost * vm.offerEntries[index].FreightCostPercent / 100 : 0;
                vm.offerEntries[index].TotalPurchaseValue += vm.offerEntries[index].FreightCostPercentValue;

                vm.offerEntries[index].FreightCostPercentValueInUSD = vm.offerEntries[index].FreightCostPercentValue * vm.offerEntries[index].ExchangeRateToUSD;
                vm.offerEntries[index].FreightCostPercentValueInUSD = vm.offerEntries[index].FreightCostPercentValueInUSD.toFixed(2);

            } else if (vm.evaluation.FreightCostUnitName === 'COST_UNIT_CURRENCY'){
                vm.offerEntries[index].TotalPurchaseValue += Number(vm.offerEntries[index].FreightCost);

                vm.offerEntries[index].FreightCostInUSD = Number(vm.offerEntries[index].FreightCost) * vm.offerEntries[index].ExchangeRateToUSD;
                vm.offerEntries[index].FreightCostInUSD = vm.offerEntries[index].FreightCostInUSD.toFixed(2);
            }

            vm.offerEntries[index].CustomsDutyCostPercentValue = vm.offerEntries[index].CustomsDutyCostPercent ?
                    vm.offerEntries[index].OfferTotalCost * vm.offerEntries[index].CustomsDutyCostPercent / 100 : 0;
            vm.offerEntries[index].TotalPurchaseValue += vm.offerEntries[index].CustomsDutyCostPercentValue;

            vm.offerEntries[index].CustomsDutyCostPercentValueInUSD = vm.offerEntries[index].CustomsDutyCostPercentValue * vm.offerEntries[index].ExchangeRateToUSD;
            vm.offerEntries[index].CustomsDutyCostPercentValueInUSD = vm.offerEntries[index].CustomsDutyCostPercentValueInUSD.toFixed(2);

            /*
            if (vm.evaluation.OtherCostUnitName === 'COST_UNIT_PERCENT') {
                vm.offerEntries[index].OtherCostPercentValue = vm.offerEntries[index].OtherCostPercent ?
                    vm.offerEntries[index].OfferTotalCost * vm.offerEntries[index].OtherCostPercent / 100 : 0;
                vm.offerEntries[index].TotalPurchaseValue += vm.offerEntries[index].OtherCostPercentValue;

                vm.offerEntries[index].OtherCostPercentValueInUSD = vm.offerEntries[index].OtherCostPercentValue * vm.offerEntries[index].ExchangeRateToUSD;
                vm.offerEntries[index].OtherCostPercentValueInUSD = vm.offerEntries[index].OtherCostPercentValueInUSD.toFixed(2);
            } else if (vm.evaluation.OtherCostUnitName === 'COST_UNIT_CURRENCY') {
                vm.offerEntries[index].TotalPurchaseValue += Number(vm.offerEntries[index].OtherCost);

                vm.offerEntries[index].OtherCostInUSD = Number(vm.offerEntries[index].OtherCost) * vm.offerEntries[index].ExchangeRateToUSD;
                vm.offerEntries[index].OtherCostInUSD = vm.offerEntries[index].OtherCostInUSD.toFixed(2);
            }
            */

            var otherCostDetIndex = 0;
            vm.otherCosts.forEach(function (cost) {
                var otherCostDet = vm.evaluationVendors[index].VHSOEvaluationVendorOtherCostDetails[otherCostDetIndex];
                if (cost.OtherCostUnitName === 'COST_UNIT_PERCENT') {
                    otherCostDet.OtherCostPercentValue = otherCostDet.OtherCostPercent ?
                        vm.offerEntries[index].OfferTotalCost * otherCostDet.OtherCostPercent / 100 : 0;
                    vm.offerEntries[index].TotalPurchaseValue += otherCostDet.OtherCostPercentValue;

                    otherCostDet.OtherCostPercentValueInUSD = otherCostDet.OtherCostPercentValue * vm.offerEntries[index].ExchangeRateToUSD;
                    otherCostDet.OtherCostPercentValueInUSD = otherCostDet.OtherCostPercentValueInUSD.toFixed(2);
                } else if (cost.OtherCostUnitName === 'COST_UNIT_CURRENCY') {
                    vm.offerEntries[index].TotalPurchaseValue += Number(otherCostDet.OtherCost);

                    otherCostDet.OtherCostInUSD = Number(otherCostDet.OtherCost) * vm.offerEntries[index].ExchangeRateToUSD;
                    otherCostDet.OtherCostInUSD = otherCostDet.OtherCostInUSD.toFixed(2);
                }
                otherCostDetIndex++;
            });

            vm.offerEntries[index].TotalPurchaseValueInUSD = vm.offerEntries[index].TotalPurchaseValue * vm.offerEntries[index].ExchangeRateToUSD;
            vm.offerEntries[index].TotalPurchaseValueInUSD = vm.offerEntries[index].TotalPurchaseValueInUSD.toFixed(2);
        }

        function autoSetTechnicalAndVPScore() {
            var techScoreCriteriaIndex = -1;
            var vendorPerformanceCriteriaIndex = -1;
            for (var i = 0; i < vm.criterias.length; i++) {
                if (vm.criterias[i].CriteriaName) {
                    if (vm.criterias[i].CriteriaName.toLowerCase().indexOf("teknis") >= 0 ||
                            vm.criterias[i].CriteriaName.toLowerCase().indexOf("technical") >= 0) {
                        techScoreCriteriaIndex = i;
                    } else if (vm.criterias[i].CriteriaName.toLowerCase().indexOf("vendor performance") >= 0) {
                        vendorPerformanceCriteriaIndex = i;
                    }
                }
            }
            if (techScoreCriteriaIndex !== -1) {
                for (var i = 0; i < vm.offerEntries.length; i++) {

                    var techScore = vm.offerEntries[i].TechnicalEvaluationScore;
                    vm.evaluationVendors[i].VHSOEvaluationVendorScoreDetails[techScoreCriteriaIndex].Score = techScore;

                    if (vm.criterias[techScoreCriteriaIndex].CriteriaOptions.length > 0) {
                        var options = vm.criterias[techScoreCriteriaIndex].CriteriaOptions;
                        for (var j = 0; j < options.length; j++) {
                            if (techScore >= options[j].MinScore && techScore <= options[j].MaxScore) {
                                vm.evaluationVendors[i].VHSOEvaluationVendorScoreDetails[techScoreCriteriaIndex].SelectedECOptionID = options[j].ID;
                                optionChange(i, techScoreCriteriaIndex);
                                break;
                            }
                        }
                    }
                    scoreChange(i, techScoreCriteriaIndex);
                }
            }
            if (vendorPerformanceCriteriaIndex !== -1) {
                for (var i = 0; i < vm.offerEntries.length; i++) {

                    var vpScore = vm.offerEntries[i].VendorPerformanceScore;
                    vm.evaluationVendors[i].VHSOEvaluationVendorScoreDetails[vendorPerformanceCriteriaIndex].Score = vpScore;

                    scoreChange(i, vendorPerformanceCriteriaIndex);
                }
            }
        }

        function assignLBIPNIScores() {
            var lbipniCriteriaIndex = -1;
            for (var i = 0; i < vm.criterias.length; i++) {
                if (vm.criterias[i].CriteriaName === "LBI-PNI" || vm.criterias[i].CriteriaName === "Area Category Index") {
                    lbipniCriteriaIndex = i;
                    break;
                }
            }
            if (lbipniCriteriaIndex !== -1) {
                var lbipniCriteriaOptions = vm.criterias[lbipniCriteriaIndex].CriteriaOptions;
                var lbipniCriteriaOptionInt = lbipniCriteriaOptions[0];
                var lbipniCriteriaOptionNat = lbipniCriteriaOptions[1];
                var lbipniCriteriaOptionLoc = lbipniCriteriaOptions[2];
                var lbipniAssignedOption = null;
                for (var i = 0; i < vm.offerEntries.length; i++) {
                    switch (vm.offerEntries[i].VendorAreaType) {
                        case "AREA_TYPE_INTERNATIONAL":
                            lbipniAssignedOption = lbipniCriteriaOptionInt;
                            break;
                        case "AREA_TYPE_NATIONAL":
                            lbipniAssignedOption = lbipniCriteriaOptionNat;
                            break;
                        case "AREA_TYPE_LOCAL":
                            lbipniAssignedOption = lbipniCriteriaOptionLoc;
                            break;
                    }
                    if (lbipniAssignedOption) {
                        vm.evaluationVendors[i].VHSOEvaluationVendorScoreDetails[lbipniCriteriaIndex].SelectedECOptionID = lbipniAssignedOption.ID;
                        optionChange(i, lbipniCriteriaIndex);
                    }
                }
            }
        }

        vm.optionChange = optionChange;
        function optionChange(vendorIndex, scoreDetIndex) {
            var scoreDet = vm.evaluationVendors[vendorIndex].VHSOEvaluationVendorScoreDetails[scoreDetIndex];
            var optionID = scoreDet.SelectedECOptionID;
            var optionData = getOptionById(vm.criterias[scoreDetIndex].CriteriaOptions, optionID);
            scoreDet.MaxScore = optionData.MaxScore;
            scoreDet.MinScore = optionData.MinScore;

            //Langsung masukkan nilai skor Max apabila opsi kriteria berupa nilai fix, bukan range
            if (vm.criterias[scoreDetIndex].IsOptionScoreFixed && optionID > 0) {
                scoreDet.Score = scoreDet.MaxScore;
            };

            //Memastikan skor berada dalam range yang benar
            scoreChange(vendorIndex, scoreDetIndex);
        };

        vm.scoreChange = scoreChange;
        function scoreChange(vendorIndex, scoreDetIndex) {
            var scoreDet = vm.evaluationVendors[vendorIndex].VHSOEvaluationVendorScoreDetails[scoreDetIndex];
            //Memastikan skor berada dalam range yang benar
            if (scoreDet.Score > scoreDet.MaxScore){
                scoreDet.Score = scoreDet.MaxScore
            }
            if (scoreDet.Score < scoreDet.MinScore){
                scoreDet.Score = scoreDet.MinScore
            }
            //Kalkulasi skor + bobot
            var weight = vm.criterias[scoreDetIndex].Weight;
            scoreDet.CalculatedScore = scoreDet.Score * weight / 100;
            scoreDet.CalculatedScore = scoreDet.CalculatedScore.toFixed(2);
            calculateTotalScores();
        };

        function getOptionById(options, id) { //untuk mengambil besar min/max score
            if (options && id) {
                for (var i = 0; i < options.length; i++) {
                    if (options[i].ID === id) {
                        return options[i];
                    }
                }
            }
            else {
                return {
                    ID: null,
                    MinScore: 0,
                    MaxScore: 100
                };
            }
        };

        vm.freightTypeChange = freightTypeChange;
        function freightTypeChange() {
            vm.offerEntries.forEach(function (entry) {
                entry.FreightTypeName = vm.evaluation.FreightTypeName;
                setFreightDeliveryCostAndTimes(entry);
            })

            calculateAllTotalPurchaseValue();
            calculateLeadTimeScores();
            calculatePriceScores();
            calculateTotalScores();
        }

        vm.vendorFreightTypeChange = vendorFreightTypeChange;
        function vendorFreightTypeChange(vendorIndex) {
            vm.evaluation.FreightTypeName = null;
            var entry = vm.offerEntries[vendorIndex];
            setFreightDeliveryCostAndTimes(entry);

            calculateAllTotalPurchaseValue();
            calculateLeadTimeScores();
            calculatePriceScores();
            calculateTotalScores();
        }

        function setFreightDeliveryCostAndTimes(entry) {
            switch (entry.FreightTypeName) {
                case "Air":
                    entry.FreightDeliveryTime = entry.FreightDeliveryTimeAir;
                    entry.FreightCostPercent = entry.FreightDeliveryCostAir;
                    break;
                case "Land":
                    entry.FreightDeliveryTime = entry.FreightDeliveryTimeLand;
                    entry.FreightCostPercent = entry.FreightDeliveryCostLand;
                    break;
                case "Sea":
                    entry.FreightDeliveryTime = entry.FreightDeliveryTimeSea;
                    entry.FreightCostPercent = entry.FreightDeliveryCostSea;
                    break;
                default:
                    entry.FreightDeliveryTime = 0;
                    entry.FreightCostPercent = 0;
                    break;
            }
        }

        function calculateLeadTimeScores() {
            var minLeadTime;
            vm.offerEntries.forEach(function (entry) {
                var totalLeadTime = entry.SupplierQLTime + entry.FreightDeliveryTime;
                if (totalLeadTime > 0 && (minLeadTime === undefined || totalLeadTime < minLeadTime)) {
                    minLeadTime = totalLeadTime;
                }
            });
            var vendorIndex = 0;
            vm.evaluationVendors.forEach(function (ev) {
                var offerEntry = vm.offerEntries[vendorIndex++];
                var totalLeadTime = offerEntry.SupplierQLTime + offerEntry.FreightDeliveryTime;
                ev.VHSOEvaluationVendorScoreDetails[1].Score = totalLeadTime > 0 ? minLeadTime * 100 / totalLeadTime : 0;
                var leadTimeWeight = vm.criterias[1].Weight;
                ev.VHSOEvaluationVendorScoreDetails[1].CalculatedScore =
                    ev.VHSOEvaluationVendorScoreDetails[1].Score * leadTimeWeight / 100;
                ev.VHSOEvaluationVendorScoreDetails[1].Score =
                    ev.VHSOEvaluationVendorScoreDetails[1].Score.toFixed(2);
                ev.VHSOEvaluationVendorScoreDetails[1].CalculatedScore =
                    ev.VHSOEvaluationVendorScoreDetails[1].CalculatedScore.toFixed(2);
            });
        };
        

        function calculatePriceScores() {
            var minPrice;
            vm.offerEntries.forEach(function (entry) {
                var totalPrice = Number(entry.TotalPurchaseValueInUSD);
                if (totalPrice > 0 && (minPrice === undefined || totalPrice < minPrice)) {
                    minPrice = totalPrice;
                }
            });
            var vendorIndex = 0;
            vm.evaluationVendors.forEach(function (ev) {
                var offerEntry = vm.offerEntries[vendorIndex++];
                var totalPrice = Number(offerEntry.TotalPurchaseValueInUSD);
                ev.VHSOEvaluationVendorScoreDetails[0].Score = totalPrice > 0 ? minPrice * 100 / totalPrice : 0;
                var priceWeight = vm.criterias[0].Weight;
                ev.VHSOEvaluationVendorScoreDetails[0].CalculatedScore =
                    ev.VHSOEvaluationVendorScoreDetails[0].Score * priceWeight / 100;
                ev.VHSOEvaluationVendorScoreDetails[0].Score =
                    ev.VHSOEvaluationVendorScoreDetails[0].Score.toFixed(2);
                ev.VHSOEvaluationVendorScoreDetails[0].CalculatedScore =
                    ev.VHSOEvaluationVendorScoreDetails[0].CalculatedScore.toFixed(2);
            });
        };

        function calculateTotalScores() {
            vm.evaluationVendors.forEach(function (ev) {
                ev.Score = 0;
                ev.VHSOEvaluationVendorScoreDetails.forEach(function (det) {
                    ev.Score += Number(det.CalculatedScore);
                });
                ev.Score = ev.Score.toFixed(2);
            });
        };

        function sumCriteriaWeights() {
            vm.totalWeight = 0;
            vm.criterias.forEach(function (crit) {
                vm.totalWeight += crit.Weight;
            })
        };

        vm.totalOfferTooltip = totalOfferTooltip;
        function totalOfferTooltip(offerEntryIndex, costType, typeIndex) {
            var vendorName = vm.offerEntries[offerEntryIndex].VendorName;
            var currency = vm.offerEntries[offerEntryIndex].CurrencySymbol;
            var typeText = "";
            switch (typeIndex) {
                case 0: return "";
                case 1: typeText = "(" + currency + ")"; break;
                case 2: typeText = "(USD)"; break;
            }
            return "Vendor : " + vendorName + "\n" + costType + " " + typeText;
        }

        vm.leadTimeTooltip = leadTimeTooltip;
        function leadTimeTooltip(offerEntryIndex, leadTimeType, typeIndex) {
            if (typeIndex !== 0) {
                return "";
            }
            var vendorName = vm.offerEntries[offerEntryIndex].VendorName;
            return "Vendor : " + vendorName + "\n" + leadTimeType;
        }

        vm.criteriaEvaluationTooltip = criteriaEvaluationTooltip;
        function criteriaEvaluationTooltip(offerEntryIndex, criteriaIndex, typeIndex) {
            var vendorName = vm.offerEntries[offerEntryIndex].VendorName;
            var criteriaName = "";
            switch(criteriaIndex){
                case 0: criteriaName = "Price"; break;
                case 1: criteriaName = "Leadtime"; break;
                default: criteriaName = vm.criterias[criteriaIndex].CriteriaName; break;
            }
            var typeText = "";
            switch (typeIndex) {
                case 1: typeText = "(Score)"; break;
                case 2: typeText = "(Weighted Result)"; break;
            }
            return "Vendor : " + vendorName + "\n" + criteriaName + " " + typeText;
        }

        vm.sortByScore = sortByScore;
        function sortByScore() {

            //Urutkan evaluation vendor
            vm.evaluationVendors.sort(function (a, b) {
                return b.Score - a.Score;
            });

            //Urutkan offer entry berdasarkan urutan evaluation vendor
            var orderedOfferEntries = [];
            vm.evaluationVendors.forEach(function (ev) {
                var offerEntryId = ev.VHSOfferEntryID;
                for (var i = 0; i < vm.offerEntries.length; i++) {
                    if (vm.offerEntries[i].ID === offerEntryId) {
                        orderedOfferEntries.push(vm.offerEntries[i]);
                        vm.offerEntries.splice(i, 0);
                        break;
                    }
                }
            });
            vm.offerEntries = orderedOfferEntries;
        };

        vm.save = save;
        function save() {
            vm.evaluation.FreightCostUnitRef = {
                Name: vm.evaluation.FreightCostUnitName
            };
            //vm.evaluation.OtherCostUnitRef = {
            //    Name: vm.evaluation.OtherCostUnitName
            //};
            vm.evaluation.FreightTypeRef = {
                Name: vm.evaluation.FreightTypeName
            };
            vm.evaluationVendors.forEach(function (ev) {

                ev.VHSOEvaluationVendorOtherCostDetails.forEach(function (det) {
                    det.VHSOfferEvaluationOtherCost = {
                        OtherCostID: det.OtherCostID
                    };
                });

                ev.VHSOEvaluationVendorScoreDetails.forEach(function (det) {
                    if (det.Score > 100 || det.Score < 0) {
                        return;
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_SCORE');
                    }
                    det.VHSOfferEvaluationCriteria = {
                        CriteriaID: det.CriteriaID
                    };
                });
            });
            vm.offerEntries.forEach(function (entry) {
                entry.FreightTypeRef = {
                    Name: entry.FreightTypeName
                };
            })
            vm.otherCosts.forEach(function (otherCost) {
                otherCost.OtherCostUnitReference = {
                    Name: otherCost.OtherCostUnitName
                };
            });
            vm.epvOtherCosts.forEach(function (epvOtherCost) {
                epvOtherCost.VHSOfferEvaluationOtherCost = {
                    OtherCostID: epvOtherCost.OtherCostID
                };
            });
            vm.evaluation.VHSOfferEvaluationOtherCosts = vm.otherCosts;
            vm.evaluation.VHSOfferEvaluationCriterias = vm.criterias;
            vm.evaluation.VHSOfferEvaluationVendors = vm.evaluationVendors;
            vm.evaluation.VHSOEvaluationEPVOtherCosts = vm.epvOtherCosts;

            UIControlService.loadLoading(loadmsg);
            EvaluasiPenawaranVHSService.saveScoring({
                VHSOfferEntries: vm.offerEntries,
                VHSOfferEvaluation: vm.evaluation
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", 'MESSAGE.SUCC_SAVE_SCORE');
                    //loadData();
                    $state.transitionTo("evaluasi-penawaran-vhs", { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE_SCORE');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE_SCORE');
            });
        }

        vm.printExcel = printExcel;
        function printExcel() {

            var item = {
                tenderStepData: vm.tenderStepData,
                offerEntries: vm.offerEntries,
                evaluation: vm.evaluation,
                evaluationVendors: vm.evaluationVendors,
                scoreDetails: vm.scoreDetails,
                //repeater: vm.repeater,
                otherCosts: vm.otherCosts,
                criterias: vm.criterias,
                epvOtherCosts: vm.epvOtherCosts,
                isFPA: vm.isFPA,
                paymentTermsOptions: vm.paymentTermsOptions,
                totalHistoricalValue: vm.totalHistoricalValue,
                totalHistoricalValueInUSD: vm.totalHistoricalValueInUSD,
                totalEPValue: vm.totalEPValue,
                totalEPValueInUSD: vm.totalEPValueInUSD,
                epvCurrencySymbol: vm.epvCurrencySymbol,
                epvRateToUSD: vm.epvRateToUSD
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-vhs/detailEvaluasiVHSPrint.html?v=1.000009',
                controller: 'detailEvaluasiPenawaranVHSPrintController',
                controllerAs: 'depvpCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
            });
        }

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('evaluasi-penawaran-vhs', { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
        }
    }
})();