﻿(function () {
    'use strict';

    angular.module("app").controller('DetAuctionController', ctrl);

    ctrl.$inject = ['UIControlService', '$translatePartialLoader', 'detAuctionService', '$uibModal'];

    function ctrl(UIControlService, $translatePartialLoader, detAuctionService, $uibModal) {
        var vm = this;

        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;

        vm.keyword = '';
        vm.totalItems = 0;
        vm.maxSize = 10;
        vm.currentPage = 1;

        vm.dataWbsOac = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-wbs-oac');
            loadDetAuction(1);
        }

        vm.loadDetAuction = loadDetAuction;
        function loadDetAuction(current) {
            console.log('tes')
            //detAuctionService.
        }

        vm.inputSummaryAuction = inputSummaryAuction;
        function inputSummaryAuction() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/detailauction/modalInputSummaryAuction.html',
                controller: 'modalInputSummaryAuctionController',
                controllerAs: 'modalInputSummaryAuctionCtrl'
            });

            modalInstance.result.then(function () {
                init();
            });
        }

        vm.modalPenawaranTerbaik = modalPenawaranTerbaik;
        function modalPenawaranTerbaik() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/detailauction/penawaranTerbaik.modal.html',
                controller: 'penawaranTerbaikModalController',
                controllerAs: 'penawaranTerbaikModalCtrl'
            });

            modalInstance.result.then(function () {
                init();
            });
        }



    }

})();