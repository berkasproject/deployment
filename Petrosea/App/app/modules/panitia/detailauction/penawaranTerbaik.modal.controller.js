﻿(function () {
    'use strict';

    angular.module("app").controller("penawaranTerbaikModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'detAuctionService', 'UIControlService', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, detAuctionService, UIControlService, $uibModalInstance) {
        var vm = this;

        vm.init = init;
        function init() {

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss')
        }

    }
})();