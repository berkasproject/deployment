﻿(function () {
	angular.module("app").controller("fpaTechEntryCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'vhsTechEntryService', '$stateParams', '$uibModal', '$state', '$translate', '$translatePartialLoader'];

	function ctrl(UIControlService, vhsTechEntryService, $stateParams, $uibModal, $state, $translate, $translatePartialLoader) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.IDDoc = Number($stateParams.DocID);

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('pemasukkan-penawaran-vhs');
			loadDataPenawaran();
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.IDStepTender;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/fpa-technical-entry/detailApproval.html',
				controller: "VHSFPAApprvCtrl",
				controllerAs: "VHSFPAApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.simpan = simpan;
		function simpan() {

		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', {
				TenderRefID: vm.IDTender,
				ProcPackType: vm.ProcPackType,
				TenderID: vm.tenderID
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.SENDING');
					vhsTechEntryService.sendToApproval({ ID: vm.IDStepTender }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SEND_SUCCESS', "MESSAGE.SEND_SUCCESS_TITLE");
							init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		function loadDataPenawaran() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vhsTechEntryService.getAllTechnical({ column: vm.IDStepTender, }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var jumlah = reply.data;
					vm.detail = reply.data;
					vm.totalItems = vm.detail.length;
					vm.GOEVendor = vm.detail;
					if (vm.detail.length === 0) {
						vhsTechEntryService.GetStepTechnical({ Status: vm.IDTender, FilterType: vm.ProcPackType }, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								//console.info("re:" + JSON.stringify(reply));
								var data = reply.data;
								vm.StartDate = data.StartDate;
								vm.EndDate = data.EndDate;
								vm.TenderName = data.tender.TenderName;
								vm.isCancelled = data.tender.IsCancelled;
								vm.tenderID = data.tender.ID;
							}
						}, function (err) {
							UIControlService.msg_growl("error", "MESSAGE.API");
							UIControlService.unloadLoading();
						});
					} else {
						console.info("Masuk else");
						vm.StartDate = vm.detail[0].tender.StartDate;
						vm.EndDate = vm.detail[0].tender.EndDate;
						vm.TenderName = vm.detail[0].tender.TenderName;
					}

					vhsTechEntryService.isNeedTenderStepApprovalTechnical({ TenderStepID: vm.IDStepTender }, function (result) {
						vm.isNeedApproval = result.data;
						if (!vm.isNeedApproval) {
							vhsTechEntryService.isApprovalSent({ TenderStepID: vm.IDStepTender }, function (result2) {
								vm.isApprovalSent = result2.data;
							});
						}
						//console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
	}
})();