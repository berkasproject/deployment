﻿(function () {
    'use strict';

    angular.module("app")
    .controller("AddKriteriaCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModal', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCriteriaSettings', 'item', 'UIControlService', 'VPCPRDataService'];
    /* @ngInject */
    function ctrl($http, $uibModal, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, VPCriteriaSettings, item, UIControlService, VPCPRDataService) {
        var vm = this;
        vm.level = item.Level;
        vm.CriId = item.CriteriaId;
        vm.IsVhsCpr = item.IsVhsCpr;
        vm.placeHolder = item.Level === 1 ? 'MODAL.MASTER_KRITERIA' : 'MODAL.SUB_KRITERIA';
        vm.Label_Name = item.Level === 1 ? 'NAMA_KRITERIA' : 'NAMA_SUBKRITERIA';
        vm.isEdit = false;
        vm.title = "MODAL.TAMBAH";
        vm.nama = "";
        vm.subcriteria = [];
        vm.standardCriteria = [];
        vm.isOptionScoreFixed = false;
        var loadingMessage = '';
        vm.isOptionSub = false;
        vm.isOptionStandard = false;
        vm.isOptionScoreFixed = false;
        vm.isprakual = false;

        vm.vendorTypeOptions = [];
        vm.selectedVendorTypeId;
        vm.vendorSpecOptions = [
            "ALL_VENDOR",
            "BY_TYPE", // barang / jasa
            "BY_BF",   // bidang usaha
            "BY_COMM"  // komoditas
        ];
        vm.selectedVendorSpec = "ALL_VENDOR";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('vpsettings-criteria');
            $translate.refresh().then(function () {
                loadingMessage = $filter('translate')('MESSAGE.LOADING');
            });
            getType();
            getRole();
            getBusinessCommodity();
            if (item.CriteriaId) {
                //console.info("Update");
                vm.title = "MODAL.UPDATE";
                vm.isEdit = true;
                vm.nama = item.CriteriaName;
                vm.isOptionScoreFixed = item.isOptionScoreFixed;
                VPCriteriaSettings.getstandards({
                    criteriaId: item.CriteriaId
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        vm.standardCriteria = reply.data;
                        if (vm.standardCriteria.length)
                            vm.isOptionStandard = true;
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                    UIControlService.unloadLoadingModal();
                });
            }
            else {
                //console.info("Insert");
                vm.isEdit = false;
                vm.title = "MODAL.TAMBAH";
            }

        };
        function getRole() {
           VPCPRDataService.cekRole(
           function (reply) {
               if (reply.status === 200) {
                   vm.flagrole = reply.data;
                   vm.FlagRole = vm.flagrole.flagRole;
                   //console.info("role: " + vm.FlagRole);
               } else {
                   UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
               }
           }, function (err) {
               UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
           });
        }

        function getBusinessCommodity() {
            VPCriteriaSettings.getallbfcomm(function (reply) {
                vm.businessFields = reply.data.BusinessFields;
                vm.commodities = reply.data.Commodities;
                if (item.CriteriaId) {
                    VPCriteriaSettings.selectedbfcomm({
                        CriteriaId: item.CriteriaId
                    }, function (reply) {
                        if (reply.data.VendorType) {
                            vm.selectedVendorSpec = "BY_TYPE";
                            vm.selectedVendorTypeId = reply.data.VendorType;
                        }
                        else if (reply.data.BusinessFieldIds.length) {
                            vm.selectedVendorSpec = "BY_BF";
                            var selectedBusinessFieldIds = reply.data.BusinessFieldIds;
                            for (var i = 0; i < vm.businessFields.length; i++) {
                                for (var j = 0; j < selectedBusinessFieldIds.length; j++) {
                                    if (vm.businessFields[i].ID === selectedBusinessFieldIds[j]) {
                                        vm.businessFields[i].selected = true;
                                        selectedBusinessFieldIds.splice(j, 1);
                                        break;
                                    }
                                }
                            }
                        }
                        else if (reply.data.CommodityIds.length) {
                            vm.selectedVendorSpec = "BY_COMM";
                            var selectedCommoditieIds = reply.data.CommodityIds;
                            for (var i = 0; i < vm.commodities.length; i++) {
                                for (var j = 0; j < selectedCommoditieIds.length; j++) {
                                    if (vm.commodities[i].ID === selectedCommoditieIds[j]) {
                                        vm.commodities[i].selected = true;
                                        selectedCommoditieIds.splice(j, 1);
                                        break;
                                    }
                                }
                            }
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_BUSINESS_FLDS_COMMODITIES'));
                    });
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_BUSINESS_FLDS_COMMODITIES'));
            });
        }

        function getType() {
            VPCriteriaSettings.getType(
                function (reply) {
                    if (reply.status === 200) {
                        vm.type = reply.data;
                        if (item.CriteriaId) {
                            for (var i = 0; i < vm.type.length; i++) {
                                if (vm.type[i].ID == item.IsVhsCpr) {
                                    vm.Type = vm.type[i];
                                    /*
                                    if (vm.Type.Name == "TYPE_CPR") {
                                        vm.disFlag = true;
                                    } else if (vm.Type.Name == "TYPE_VHS") {
                                        vm.disFlag = false;
                                    } else if (vm.Type.Name == "TYPE_PRAKUAL") {
                                        vm.disFlag = true;
                                        vm.isprakual = true;
                                        vm.isOptionScoreFixed = true;
                                    }
                                    */
                                    adjustOption();
                                    break;
                                }
                            }
                        }
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            );

            VPCriteriaSettings.getVendorTypes(
                function (reply) {
                    vm.vendorTypeOptions = reply.data;
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            );
        }

        function loadCriteria() {
            vm.kriteria = [];
            VPCriteriaSettings.select({
                keyword: "",
                level: 1,
                parentId: 0,
                offset: (vm.currentPage - 1) * vm.maxSize,
                limit: vm.maxSize
            }, function (reply) {
                if (reply.status === 200) {
                    vm.criteria = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        vm.deleteStandard = deleteStandard;
        function deleteStandard(index, data) {
            vm.standardCriteria.splice(index, 1);
        }

        vm.showOption = showOption;
        function showOption() {
            vm.standardCriteria = [];
            adjustOption();
        }

        function adjustOption() {
            if (vm.Type.Name == "TYPE_CPR") {
                vm.isOptionScoreFixed = true;
                vm.disFlag = true;
            } else if (vm.Type.Name == "TYPE_VHS") {
                vm.isOptionScoreFixed = false;
                vm.disFlag = false;
            } else if (vm.Type.Name == "TYPE_PRAKUAL") {
                vm.isOptionScoreFixed = true;
                vm.disFlag = true;
                vm.isprakual = true;
            } else if (vm.Type.Name == "TYPE_SUPPLIERPERFORMANCE") {
                vm.isOptionScoreFixed = true;
                vm.disFlag = true;
            }
        }

        vm.addStandard = addStandard;
        function addStandard() {
            if (vm.level == 1) {
                //console.info("Lv1");
                if (vm.Type.Name == "TYPE_CPR") {
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 3
                    });
                } else if (vm.Type.Name == "TYPE_VHS") {
                    var prevMaxScore = 0;
                    if (vm.standardCriteria.length > 0) {
                        prevMaxScore = vm.standardCriteria[vm.standardCriteria.length - 1].MaxScore;
                    }
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 100
                    });
                } else if (vm.Type.Name == "TYPE_SUPPLIERPERFORMANCE") {
                    vm.isOptionScoreFixed = true;
                    vm.disFlag = true;
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 100
                    });
                }
            } else if (vm.level == 2) {
                //console.info("Lv2");
                if (item.IsVhsCpr == 1) {
                    vm.isOptionScoreFixed = true;
                    vm.disFlag = true;
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 3
                    });
                } else if (item.IsVhsCpr == 2) {
                    var prevMaxScore = 0;
                    if (vm.standardCriteria.length > 0) {
                        prevMaxScore = vm.standardCriteria[vm.standardCriteria.length - 1].MaxScore;
                    }
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 100
                    });
                } else if (item.IsVhsCpr == 4) {
                    vm.isOptionScoreFixed = true;
                    vm.disFlag = true;
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 100
                    });
                }
            } else if (vm.level == 3) {
                if (item.IsVhsCpr == 3) {
                    vm.isOptionScoreFixed = true;
                    vm.disFlag = true;
                    vm.isprakual = true;
                    vm.standardCriteria.push({
                        MinScore: prevMaxScore,
                        MaxScore: 20
                    });
                }
            }
        }

        vm.maxScoreChange = maxScoreChange;
        function maxScoreChange(index) {
            if (!vm.standardCriteria[index].MaxScore) {
                vm.standardCriteria[index].MaxScore = 0;
            }
            var prevMaxScore = 0; //memastikan nilai maksimum tidak kurang dari nilai maksimum opsi sebelumnya
            if (index > 0) {
                prevMaxScore = vm.standardCriteria[index - 1].MaxScore;
            }
            if (vm.standardCriteria[index].MaxScore < prevMaxScore) {
                vm.standardCriteria[index].MaxScore = prevMaxScore;
            }

            var nextMaxScore = 100; //memastikan nilai maksimum tidak melebihi nilai maksimum opsi selanjutnya
            if (index != vm.standardCriteria.length - 1) {
                nextMaxScore = vm.standardCriteria[index + 1].MaxScore;
            }
            if (vm.standardCriteria[index].MaxScore > nextMaxScore) {
                vm.standardCriteria[index].MaxScore = nextMaxScore;
            }

            if (index != vm.standardCriteria.length - 1) { //menjadikan nilai maksimum sebagai nilai minimum opsi selanjutnya
                vm.standardCriteria[index + 1].MinScore = vm.standardCriteria[index].MaxScore;
            }
        }

        vm.selectBFComm = selectBFComm;
        function selectBFComm() {
            var item = {
                items: vm.selectedVendorSpec === 'BY_BF' ? vm.businessFields : vm.commodities,
                selectedVendorSpec: vm.selectedVendorSpec,
                flagRole: vm.FlagRole
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpcriteria-settings/pilihBFComm.html',
                controller: 'selectBusinessFieldCommodityController',
                controllerAs: 'selBFCommCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                
            });
        }

        vm.deleteItem = deleteItem;
        function deleteItem(item) {
            item.selected = false;
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.save = save
        function save() {
            if (!vm.nama) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NO_NAME");
                return;
            }

            if (vm.level < 2) {
                if (!vm.Type) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NO_TYPE");
                    return;
                }
            }

            var typeFlag = 0;
            if (vm.level == 1) {
                typeFlag = vm.Type.ID;
            } else {
                typeFlag = item.IsVhsCpr;
            }

            if (vm.standardCriteria.length > 0) {
                for (var i = 0; i < vm.standardCriteria.length; i++) {
                    /*
                    if (item.IsVhsCpr === 3 && !vm.standardCriteria[i].MaxScore) {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_NO_SCORE");
                        return;
                    }
                    */
                    if (!vm.standardCriteria[i].DetailScore) {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_NO_DESC");
                        return;
                    }
                }
            }

            if (vm.standardCriteria.length > 0) {
                for (var i = 0; i < vm.standardCriteria.length - 1; i++) {
                    if (vm.standardCriteria[i].MaxScore >= vm.standardCriteria[i + 1].MaxScore) {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_OPTION_SCORE");
                        return;
                    }
                }
            }
            
            var vendorType = null;
            var businessFieldIds = null;
            var commodityIds = null;

            switch (vm.selectedVendorSpec) {
                case "BY_TYPE":
                    vendorType = vm.selectedVendorTypeId;
                    break;
                case "BY_BF":
                    businessFieldIds = [];
                    for (var i = 0; i < vm.businessFields.length; i++) {
                        if (vm.businessFields[i].selected) {
                            businessFieldIds.push(vm.businessFields[i].ID)
                        }
                    }
                    break;
                case "BY_COMM":
                    commodityIds = [];
                    for (var i = 0; i < vm.commodities.length; i++) {
                        if (vm.commodities[i].selected) {
                            commodityIds.push(vm.commodities[i].ID)
                        }
                    }
                    break;
            }

            if (vm.isEdit) {
                VPCriteriaSettings.update({
                    CriteriaId: item.CriteriaId,
                    CriteriaName: vm.nama,
                    IsVhsCpr: typeFlag,
                    isOptionScoreFixed: vm.isOptionScoreFixed,
                    VPCriteriaDescriptionStandards: vm.standardCriteria,
                    Level: item.Level,
                    ParentId: item.ParentId,
                    VendorType: vendorType,
                    BusinessFieldIds: businessFieldIds,
                    CommodityIds: commodityIds
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    UIControlService.unloadLoadingModal();
                });
            } else {
                VPCriteriaSettings.insert({
                    CriteriaName: vm.nama,
                    IsVhsCpr: typeFlag,
                    isOptionScoreFixed: vm.isOptionScoreFixed,
                    VPCriteriaDescriptionStandards: vm.standardCriteria,
                    Level: item.Level,
                    ParentId: item.ParentId,
                    VendorType: vendorType,
                    BusinessFieldIds: businessFieldIds,
                    CommodityIds: commodityIds
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    UIControlService.unloadLoadingModal();
                });
            }
        };

    }
})();