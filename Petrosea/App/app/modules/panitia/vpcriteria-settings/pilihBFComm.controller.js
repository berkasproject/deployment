﻿(function () {
    'use strict';

    angular.module('app').controller('selectBusinessFieldCommodityController', ctrl);

    ctrl.$inject = ['item', 'UIControlService', '$uibModalInstance'];

    function ctrl(item, UIControlService, $uibModalInstance) {

        var vm = this;

        vm.items = item.items;
        vm.selectedVendorSpec = item.selectedVendorSpec;
        vm.flagRole = item.flagRole;

        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.pagedItems = {};
        
        vm.init = init;
        function init() {
            loadPage(1);
        };

        vm.loadPage = loadPage;
        function loadPage(page) {
            vm.currentPage = page;
            var offset = vm.maxSize * (vm.currentPage - 1);
            vm.pagedItems = vm.items.slice(offset, offset + vm.maxSize);
        };

        vm.close = close;
        function close() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();