(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalDeliveryDateModalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'DeliveryDateApprovalService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, DeliveryDateApprovalService) {

        var vm = this;
        var Id = item.Id;
        vm.tenderCode = item.tenderCode;
        vm.tenderName = item.tenderName;
        vm.remark = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-penawaran-barang');
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reject = reject;
        function reject() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REJECT_DD'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    DeliveryDateApprovalService.reject({
                        Id: Id,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REJECT_DD'));
                        $uibModalInstance.close();
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_DD'));
                    });
                }
            });
        }
    }
})();