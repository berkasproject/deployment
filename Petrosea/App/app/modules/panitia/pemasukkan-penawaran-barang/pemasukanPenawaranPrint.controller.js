﻿(function () {
	'use strict';

	angular.module("app").controller("PemasukkanPenawaranBarangCtrlPrint", ctrl)

	ctrl.$inject = ['$stateParams', 'UIControlService', 'GoodOfferEntryService', '$translatePartialLoader']
	function ctrl($stateParams, UIControlService, GOEService, $translatePartialLoader) {
		var vm = this
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.TenderID = Number($stateParams.TenderID);
		vm.jumlahlembar = 1

		vm.init = init;
		function init() {
			loadDataPenawaran();
			$translatePartialLoader.addPart('pemasukkan-penawaran-barang');
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		function loadDataPenawaran() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			GOEService.getAll({ column: vm.IDStepTender, }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var jumlah = reply.data;
					vm.detail = reply.data;
					vm.totalItems = vm.detail.length;
					vm.GOEVendor = vm.detail;
					if (vm.detail.length === 0) {
						GOEService.GetStep({
							Status: vm.IDTender,
							FilterType: vm.ProcPackType,
							IntParam1: vm.TenderID
						}, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								var data = reply.data;
								vm.StartDate = data.StartDate;
								vm.EndDate = data.EndDate;
								vm.TenderName = data.tender.TenderName;
								vm.isCancelled = data.tender.IsCancelled;
							}
						}, function (err) {
							UIControlService.msg_growl("error", "MESSAGE.API");
							UIControlService.unloadLoading();
						});
					} else {
						vm.StartDate = vm.detail[0].StartDateTen;
						vm.EndDate = vm.detail[0].EndDateTen;
						vm.TenderName = vm.detail[0].TenderName;
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
	}
})()