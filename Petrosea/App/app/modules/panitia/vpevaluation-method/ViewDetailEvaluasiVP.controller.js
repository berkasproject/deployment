﻿(function () {
    'use strict';

    angular.module("app")
    .controller("veiwDetailEVP", ctrl);

    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPEvaluationMthodService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, VPEvaluationMthodService, UIControlService, item) {

        var vm = this;
        var metode_evaluasi_id = item.metode_evaluasi_id;

        var VHS = [];
        var VHSLevel1 = [];
        var VHSLevel2 = [];
        var VHSLevel3 = [];

        var kriteria = [];
        var kriteriaLv1 = [];
        var kriteriaLv2 = [];
        var kriteriaLv3 = [];

        vm.kriteria;

        var loadingMessage = "";
        vm.Administrasi;
        vm.bobotAdministrasi = 0;
        vm.Teknis;
        vm.bobotTeknis = 0;
        vm.Harga;
        vm.bobotHarga = 0;
        vm.Barang;
        vm.bobotBarang = 0;
        vm.VHS;
        vm.bobotVHS = 0;
        vm.kategori;
        vm.nama;

        vm.init = init;
        function init() {
            $translate.refresh().then(function () {
                loadingMessage = $filter('translate')('MESSAGE.LOADING');
            });
            vm.loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            VPEvaluationMthodService.selectById({
                VPEvaluationMethodId: metode_evaluasi_id
            }, function (reply) {
                if (reply.status === 200) {
                    vm.kategori = reply.data.VPEvaluationMethodDetails;
                    for (var i = 0; i < vm.kategori.length; i++) {
                        if (vm.kategori[i].DetailType === 'VHS') {
                            vm.flagVHS = true;
                            vm.flagCPR = false;
                            vm.bobotVhs = vm.kategori[i].Weight;
                        }
                        else if (vm.kategori[i].DetailType === 'CPR' || vm.kategori[i].DetailType === 'K3L') {
                            vm.flagVHS = false;
                            vm.flagCPR = true;
                        } else {
                            vm.flagPrakual = true;
                        }
                    }
                    vm.nama = reply.data.VPEvaluationMethodName;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
                }
            }, function (err) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });

            VPEvaluationMthodService.selectDCbyID({
                VPEvaluationMethodId: metode_evaluasi_id
            }, function (reply) {
                if (reply.status === 200) {
                    var hasil = reply.data;
                    for (var i = 0; i < hasil.length; i++) {
                        kriteria.push(hasil[i]);
                    }

                    for (var i = 0; i < kriteria.length; i++) {
                        if (kriteria[i].Level === 1) {
                            kriteriaLv1.push(kriteria[i]);
                        }
                        else if (kriteria[i].Level === 2) {
                            kriteriaLv2.push(kriteria[i]);
                        }
                        else if (kriteria[i].Level === 3) {
                            kriteriaLv3.push(kriteria[i]);
                        }
                    }

                    for (var i = 0; i < kriteriaLv2.length; i++) {
                        kriteriaLv2[i].sub = [];
                        for (var j = 0; j < kriteriaLv3.length; j++) {
                            if (kriteriaLv3[j].Parent === kriteriaLv2[i].CriteriaId) {
                                kriteriaLv2[i].sub.push(kriteriaLv3[j]);
                            }
                        }
                    }
                    for (var i = 0; i < kriteriaLv1.length; i++) {
                        kriteriaLv1[i].sub = [];
                        for (var j = 0; j < kriteriaLv2.length; j++) {
                            if (kriteriaLv2[j].Parent === kriteriaLv1[i].CriteriaId) {
                                kriteriaLv1[i].sub.push(kriteriaLv2[j]);
                            }
                        }

                    }

                    vm.kriteria = kriteriaLv1;
                    //console.info(vm.kriteria);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
                }
            }, function (err) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.keluar = keluar;
        function keluar() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();