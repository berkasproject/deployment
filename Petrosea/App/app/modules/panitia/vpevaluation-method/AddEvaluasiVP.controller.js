﻿(function () {
    'use strict';

    angular.module("app").controller("AddEvalVPCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$scope', '$http', '$uibModal', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCriteriaSettings', 'VPEvaluationMthodService', 'UIControlService', 'VPCPRDataService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $scope, $http, $uibModal, $filter, $translate, $translatePartialLoader, $location, SocketService, VPCriteriaSettings, VPEvaluationMthodService, UIControlService, VPCPRDataService) {
        var vm = this;
        var lang;
        var VPEvaluationMthodId = Number($stateParams.id);
        var vhsorcpr = Number($stateParams.type);

        vm.isVhsCpr = vhsorcpr;
        vm.isEdit = VPEvaluationMthodId > 0;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.menuhome = 0;
        $scope.my_tree = {};
        vm.page_id = 135;
        vm.level = 1;
        vm.srcText = "";
        vm.Type = '';
        vm.evalById = [];
        vm.allChecked = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('vpevaluation-method');
            getRole();
            if (vm.isEdit) {
                vm.kriteriaList = [];
                getType();
                VPEvaluationMthodService.selectById({
                    VPEvaluationMethodId: Number($stateParams.id)
                },
                function (reply) {
                    if (reply.status === 200) {
                        vm.evalById = reply.data;
                        vm.namaMetode = vm.evalById.VPEvaluationMethodName;
                        vm.ambiltipe = String(vm.evalById.Type.Name);
                        if (vm.evalById.VPEvaluationMethodDetails.length > 0) {
                            vm.allChecked = true;
                            for (var i = 0; i < vm.evalById.VPEvaluationMethodDetails.length; i++) {
                                vm.kriteriaList.push({
                                    id: vm.evalById.VPEvaluationMethodDetails[i].VPEMDId,
                                    kriteria_nama: vm.evalById.VPEvaluationMethodDetails[i].DetailType,
                                    checked: vm.evalById.VPEvaluationMethodDetails[i].IsActive,
                                    bobot: vm.evalById.VPEvaluationMethodDetails[i].Weight
                                });
                                if (!vm.evalById.VPEvaluationMethodDetails[i].IsActive) {
                                    vm.allChecked = false;
                                };
                            }
                            filterKriteriaList();
                        }
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
            }
            else {
                getType();
            }
        };

        function getRole() {
            VPCPRDataService.cekRole(
            function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info("role: " + vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
        }

        function loadType(tipe) {
            vm.srcText = "";
            vm.currentPage = 1;
            
            if (tipe === 'TYPE_CPR') {
                vm.kriteriaList = [
                    { kriteria_nama: "CPR", checked: true, bobot: 0 },
                ];
                filterKriteriaList();
            } else if (tipe === 'TYPE_VHS') {
                vm.kriteriaList = [
                    { kriteria_nama: "VHS", checked: true, bobot: 100 },
                ];
                filterKriteriaList();
            }
            else if (tipe === 'TYPE_PRAKUAL') {
                vm.kriteriaList = [];
                VPCriteriaSettings.select({
                    keyword: "",
                    isVhsCpr: 3,
                    level: 1,
                    parentId: null,
                    offset: 0,
                    limit: 0,
                }, function (reply) {
                    if (reply.status === 200) {
                        var allKriteria = reply.data;
                        for (var i = 0; i < allKriteria.length; i++) {
                            vm.kriteriaList.push({
                                kriteria_nama: allKriteria[i].CriteriaName,
                                checked: false,
                                bobot: 0
                            });
                        }
                        filterKriteriaList();
                    }
                })
            }
        }

        vm.onTypeChange = onTypeChange;
        function onTypeChange() {
            loadType(vm.Type.Name);
            if (vm.Type.Name == "TYPE_VHS") {
                vm.flagShow = true;
            } else if (vm.Type.Name == "TYPE_CPR") {
                vm.flagShow = false;
            } else if (vm.Type.Name == "TYPE_PRAKUAL") {
                vm.flagShow = false;
            }
        };

        function getType() {
            VPCriteriaSettings.getType(
                function (reply) {
                    if (reply.status === 200) {
                        vm.type = reply.data;
                        if (vm.isEdit == true) {
                            for (var i = 0; i < vm.type.length; i++) {
                                if (vm.type[i].ID == vm.isVhsCpr) {
                                    vm.Type = vm.type[i];
                                    if (vm.Type.Name == "TYPE_VHS") {
                                        vm.flagShow = true;
                                    } else if (vm.Type.Name == "TYPE_CPR") {
                                        vm.flagShow = false;
                                    } else if (vm.Type.Name == "TYPE_PRAKUAL") {
                                        vm.flagShow = false;
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText.toLowerCase();
            vm.currentPage = 1;

            filterKriteriaList();
        };

        function filterKriteriaList() {
            vm.kriteriaListPaged = [];
            for (var i = 0; i < vm.kriteriaList.length; i++) {
                if (vm.kriteriaList[i].kriteria_nama.toLowerCase().includes(vm.srcText)) {
                    vm.kriteriaListPaged.push(vm.kriteriaList[i]);
                }
            }
            onCheck();
        }

        vm.back = back;
        function back() {
            $state.transitionTo('evaluasi-vp');
        };

        vm.onCheck = onCheck;
        function onCheck() {
            vm.allChecked = true;
            for (var i = 0; i < vm.kriteriaListPaged.length; i++) {
                if (!vm.kriteriaListPaged[i].checked) {
                    vm.allChecked = false;
                    break;
                }
            }
        }

        vm.onCheckAll = onCheckAll;
        function onCheckAll() {
            for (var i = 0; i < vm.kriteriaListPaged.length; i++) {
                vm.kriteriaListPaged[i].checked = vm.allChecked;
            }
        }

        vm.detail = detail;
        function detail(id, namaKriteria) {
            if (vm.isVhsCpr == 1 || vm.isVhsCpr == 2) {
                $state.transitionTo('detail-evaluasi-vp', {
                    id: id,
                    type: vm.isVhsCpr
                });
            } else if (vm.isVhsCpr == 3) {
                    $state.transitionTo('detail-evaluasi-prakual', {
                        id: id,
                        name: namaKriteria
                    });
            }
        };

        vm.save = save;
        function save() {

            if (!vm.namaMetode) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_NAME'));
                return;
            }

            var noCheckedCrit = true;
            for (var i = 0; i < vm.kriteriaList.length; i++) {
                if(vm.kriteriaList[i].checked === true){
                    noCheckedCrit = false;
                    break
                }
            }
            if (noCheckedCrit) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_SELECTED_CRIT'));
                return;
            }

            var VPMetodeEvaluasi = {
                VPEvaluationMethodName: vm.namaMetode,
                IsVhsOrCpr: vm.Type.ID,
                VPEvaluationMethodDetails: []
            }

            if (vm.isEdit) {
                VPMetodeEvaluasi.VPEvaluationMethodId = vm.evalById.VPEvaluationMethodId;
            }

            var details = [];
            vm.kriteriaList.forEach(function (k) {
                var detail = {
                    DetailType: k.kriteria_nama,
                    Weight: k.bobot,
                    IsActive: k.checked
                }
                if (k.id > 0) {
                    detail.VPEMDId = k.id;
                }
                details.push(detail);
            });

            VPMetodeEvaluasi.VPEvaluationMethodDetails = details;

            //console.info(VPMetodeEvaluasi);
            if (vm.isEdit) {
                //update
                VPEvaluationMthodService.updateEval(VPMetodeEvaluasi,
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "MESSAGE.SUCC_UPDATE");
                            $state.transitionTo('evaluasi-vp');
                        }
                        else {
                            UIControlService.msg_growl("error", $filter('translate')("MESSAGE.ERR_UPDATE"));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_UPDATE'));
                    }
                );
            } else {
                //insert
                VPEvaluationMthodService.insertEval(VPMetodeEvaluasi,
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "MESSAGE.SUCC_INSERT");
                            $state.transitionTo('evaluasi-vp');
                        }
                        else {
                            UIControlService.msg_growl("error", $filter('translate')("MESSAGE.ERR_INSERT"));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT'));
                    }
                );
            }
        };
    }
})();

