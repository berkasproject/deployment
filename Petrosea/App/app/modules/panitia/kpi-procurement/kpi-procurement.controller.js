(function () {
	'use strict';

	angular.module("app").controller("KPIProcCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'KPIProcurementService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, KPIProcurementService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.arrKPIProc = [];
		vm.arrOntimePO = [];
		vm.arrSavingCost = [];
		vm.procTypes = [];
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.isCalendarOpened = [false, false, false, false];

		vm.isMgr = true;

		vm.totalFinalValue = 0;
		vm.totalCostSaving = 0;
		vm.totalAvoidance = 0;
		vm.totalProposedValue = 0;
		vm.totalBudgetedValue = 0;

		function init() {
		    $translatePartialLoader.addPart('kpi-procurement');
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    procurementType();
		    //konfigurasiWaktu();
		}

		function employeePositionName() {
            /*
		    KPIProcurementService.employeePositionName({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.employeePositionName = reply.data;
		            console.info("emposname:" + JSON.stringify(vm.employeePositionName));
		            vm.procType = vm.procTypes[0];
                    /*
		            if (vm.employeePositionName == "L1 Stock Purchasing") {
		                vm.procType = vm.procTypes[1];
		                vm.isMgr = false;
		            }
		            else if (vm.employeePositionName == "L1 Direct Purchasing") {
		                vm.procType = vm.procTypes[0];
		                vm.isMgr = false;
		            }
		            else if (vm.employeePositionName == "MGR Procurement") {
		                vm.procType = vm.procTypes[0];
		            }
		            else if (vm.employeePositionName == "MGR Supply Chain Management") {
		                vm.procType = vm.procTypes[0];
		            }
		            konfigurasiWaktu();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETPOST");
		        UIControlService.unloadLoading();
		    });*/
		    vm.procType = vm.procTypes[0];
		    /*
            if (vm.employeePositionName == "L1 Stock Purchasing") {
                vm.procType = vm.procTypes[1];
                vm.isMgr = false;
            }
            else if (vm.employeePositionName == "L1 Direct Purchasing") {
                vm.procType = vm.procTypes[0];
                vm.isMgr = false;
            }
            else if (vm.employeePositionName == "MGR Procurement") {
                vm.procType = vm.procTypes[0];
            }
            else if (vm.employeePositionName == "MGR Supply Chain Management") {
                vm.procType = vm.procTypes[0];
            }*/
		    konfigurasiWaktu();
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		vm.konfigurasiWaktu = konfigurasiWaktu;
		function konfigurasiWaktu() {
		    vm.datenow = new Date();
		    vm.getYearNow = vm.datenow.getFullYear();
		    var mindateNow = new Date("January 01, " + vm.getYearNow + " 00:00:00");
		    var maxdateNow = new Date("December 31, " + vm.getYearNow + " 23:59:59");
		    //mindateNow.setYear(vm.getYearNow - 1);
		    vm.datepickeroptions = {
		        minMode: 'month',
		        maxDate: maxdateNow,
		        minDate: mindateNow
		    }
		    vm.datepickeroptionsTahun = {
		        minMode: 'year'
		    }
		    if (localStorage.getItem("currLang") === 'id' || localStorage.getItem("currLang") === 'ID') {
			    vm.ddDurasi =
	            [
	                { Value: 0, Name: "Minggu" },
	                { Value: 1, Name: "Bulan" }

	            ]
	        }
		    else if (localStorage.getItem("currLang") === 'en' || localStorage.getItem("currLang") === 'EN') {
	        	vm.ddDurasi =
	            [
	                { Value: 0, Name: "Week" },
	                { Value: 1, Name: "Month" }

	            ]
	        }
		    //vm.jenistender = vm.ddTender[0];
		    vm.durasi = vm.ddDurasi[0];
		    vm.filterTahun = vm.datenow;
		    vm.tahun = vm.getYearNow;
		    weekByYear();
		}

		function weekByYear() {
		    KPIProcurementService.weekByYear({
		        column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.weeks = reply.data;
		            //console.info("weeks" + JSON.stringify(vm.weeks));
		            for (var i = 0; i <= vm.weeks.length - 1; i++) {
		                vm.weeks[i].label = "Week #" + vm.weeks[i].NumberOfWeek + " (" + UIControlService.convertDate(vm.weeks[i].StartDate) + " s/d " + UIControlService.convertDate(vm.weeks[i].EndDate) + ")";
		            }
		            vm.ma = vm.weeks[0];
		            vm.mb = vm.weeks[vm.weeks.length - 1];
		            vm.batasAtas = vm.ma.NumberOfWeek;
		            vm.batasBawah = vm.mb.NumberOfWeek;
		            if (vm.arrKPI.length == 0) {
		                dataKPIProc(1);
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETPOST");
		        UIControlService.unloadLoading();
		    });
		}

		function procurementType() {
		    KPIProcurementService.procurementType({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.procTypes = reply.data;
		            //console.info("proctypes:" + JSON.stringify(vm.procTypes));
		            employeePositionName();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETPOST");
		        UIControlService.unloadLoading();
		    });
		}

		vm.cekFilterTahun = cekFilterTahun;
		function cekFilterTahun() {
		    vm.tahun = vm.filterTahun.getFullYear();
		    if (vm.durasi.Value == 0) {
		        weekByYear();
		    }
		    else if (vm.durasi.Value == 1) {
		        var mindateNow = new Date("January 01, " + vm.tahun + " 00:00:00");
		        var maxdateNow = new Date("December 31, " + vm.tahun + " 23:59:59");
		        vm.datepickeroptions = {
		            minMode: 'month',
		            maxDate: maxdateNow,
		            minDate: mindateNow
		        }
		        vm.ba = mindateNow;
		        vm.bb = maxdateNow;
		    }
		}

		vm.filter = filter;
		function filter() {
		    var invalidFilter = false;
		    if (vm.durasi == null) {
		        invalidFilter = true;
		    }
		    else {
		        if (vm.durasi.Value == 0) { //minggu
		            if (vm.ma == null || vm.mb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ma.NumberOfWeek > vm.mb.NumberOfWeek) {
		                    invalidFilter = true;
		                }
		                else {
		                    vm.batasAtas = vm.ma.NumberOfWeek;
		                    vm.batasBawah = vm.mb.NumberOfWeek;
		                }
		            }
		        }
		        else if (vm.durasi.Value == 1) { //bulan         
		            if (vm.ba == null || vm.bb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ba > vm.bb) {
		                    invalidFilter = true;
		                }
		                else {
		                    var intBatasAtas = vm.ba.getMonth();
		                    var intBatasBawah = vm.bb.getMonth();
		                    vm.batasAtas = intBatasAtas + 1;
		                    vm.batasBawah = intBatasBawah + 1;
		                }
		            }

		        }
		    }
		    if (invalidFilter == true) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_FILTER");
		        return;
		    }
		    else if (invalidFilter == false) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        dataKPIProc(1);
		    }
		}

		function getDaysInMonth(m, y) {
		    return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
		}
		vm.arrKPI = [];
		vm.dataKPIProc = dataKPIProc;
		function dataKPIProc(current) {
		    //console.info("proctype:" + vm.procType.RefID);
		    vm.currentPage = current;
		    var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    KPIProcurementService.dataKPIProc({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Offset: offset,
		        Limit: vm.maxSize,
		        Status: vm.durasi.Value,
                FilterType:vm.procType.RefID
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            vm.arrKPI = reply.data.List;
		            vm.totalFinalValue = vm.arrKPI[0].TotalFinalPOUSD;
		            vm.totalAvoidance = vm.arrKPI[0].Avoidance;
		            vm.totalCostSaving = vm.arrKPI[0].TotalCostSaving;
		            vm.totalProposedValue = vm.arrKPI[0].TotalOETotalPriceUSD;
		            vm.totalBudgetedValue = vm.arrKPI[0].TotalBudgetedPOUSD;
		            //console.info("arr" + JSON.stringify(vm.arrKPI));
		            vm.totalItems = Number(reply.data.Count);
		            dataGrafik(vm.batasAtas, vm.batasBawah, vm.totalItems, vm.durasi.Value);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.dataGrafik = dataGrafik;
		function dataGrafik(atas, bawah, totalItems, tipeDurasi) {
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    KPIProcurementService.dataKPIProc({
		        column: vm.tahun,
		        IntParam1: atas,
		        IntParam2: bawah,
		        Offset: 0,
		        Limit: totalItems,
		        Status: vm.durasi.Value,
		        FilterType: vm.procType.RefID
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            vm.dataGrafikKPI = reply.data.List;
		            console.info("arr grafik" + JSON.stringify(vm.dataGrafikKPI));
		            vm.totalItems = Number(reply.data.Count);
		            vm.arrOntime = [];
		            vm.arrCostSaving = [];
		            vm.label = [];
		            for (var i = 0; i <= vm.dataGrafikKPI.length - 1; i++) {
		                vm.arrOntime[i] = vm.dataGrafikKPI[i].OntimePO;
		                vm.arrCostSaving[i] = vm.dataGrafikKPI[i].CostSaving;
		                vm.label[i] = atas + i;
		            }
		            if (tipeDurasi == 0) {
		                var durasi = 'Week';
		            }
		            else {
		                var durasi = 'Month';
		            }
		            //console.info("arr" + JSON.stringify(vm.arrCostSaving));
		            grafikOntime(durasi, vm.label, vm.arrOntime);
		            grafikCostSaving(durasi, vm.label, vm.arrCostSaving);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		function grafikOntime(durasi, label, arr) {
		    vm.dataOntime = [arr];
		    //console.info("data ontime" + JSON.stringify(vm.dataOntime));
		    var maxvalue = Math.max.apply(Math, arr);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'ID') {
		        vm.labelOntime = label;
		        vm.datasetOntime = [{
		            label: "Ontime PO"
		        }];

		        vm.optionOntime = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah PO'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelOntime = label;
		        vm.datasetOntime = [{
		            label: "Ontime PO"
		        }];

		        vm.optionOntime = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'PO Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    }
		}

		function grafikCostSaving(durasi, label, arr) {
		    //console.info("cs"+JSON.stringify(arr));
		    var maxvalue = Math.max.apply(Math, arr);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    vm.dataSC = [arr];
		    if (localStorage.getItem("currLang") === 'ID') {
		        vm.labelSC = label;
		        vm.datasetSC = [{
		            label: "Cost Saving"
		        }];

		        vm.optionSC = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Cost Saving (%)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelSC = label;
		        vm.datasetSC = [{
		            label: "Cost Saving"
		        }];

		        vm.optionSC = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Cost Saving (%)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: durasi
		                    }
		                }]
		            }
		        };
		    }
		}

        
		vm.exportKPI = exportKPI;
		function exportKPI() {
		    //console.info("proctype:" + vm.procType.RefID);
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    KPIProcurementService.dataKPIProc({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Offset: 0,
		        Limit: vm.totalItems,
		        Status: vm.durasi.Value,
		        FilterType: vm.procType.RefID
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            var data = reply.data;
		            vm.alldata = data.List;
		            //console.info("datasampel:" + JSON.stringify(vm.alldata[1]));
		            vm.totalItemAlldata = Number(data.Count);
		            vm.filterdata = [];
		            //console.info("total semua data:" + JSON.stringify(vm.alldata.length));
		            for (var i = 0; i < vm.totalItemAlldata; i++) {
		                UIControlService.loadLoading("MESSAGE.LOADING");
		                var forExcel = {
		                    Year: vm.alldata[i].Year,
		                    Durration: vm.alldata[i].Durration,
		                    ApprovedPR: vm.alldata[i].ApprovedPR,
		                    CreatedPO: vm.alldata[i].CreatedPO,
		                    OntimePO: vm.alldata[i].OntimePO,
                            Currency: "USD",
                            FinalPOValue: vm.alldata[i].FinalPOToUSD,
                            BudgetedValue: vm.alldata[i].BudgetedPOToUSD,
                            CostSaving: vm.alldata[i].CostSaving,
                            OEValue: vm.alldata[i].OETotalPriceToUSD,
                            POLocal: vm.alldata[i].POLocal,
		                    PONational: vm.alldata[i].PONational,
		                    POInternational: vm.alldata[i].POInternational
		                }
		                vm.filterdata.push(forExcel);
		            }
		            //console.info("dataForExcel:" + JSON.stringify(vm.filterdata));
		            JSONToCSVConvertor(vm.filterdata, true);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });


		}
        


		vm.exportExcel = exportExcel;
		function exportExcel() {
		    //console.info("proctype:" + vm.procType.RefID);
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    KPIProcurementService.exportKPIItemPr({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Status: vm.durasi.Value,
		        FilterType: vm.procType.RefID
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            var list = reply.data;
		            console.info("list" + JSON.stringify(list));
		            vm.filterdata = [];
		            //console.info("total semua data:" + JSON.stringify(vm.alldata.length));
                    
		            for (var i = 0; i <= list.length-1; i++) {
		                UIControlService.loadLoading("MESSAGE.LOADING");
		                //if (i <= list.length - 1) {
		                var posentdate = '';
		                var approvalPRdate = '';
		                var budgetedv = '';
		                var finalv = '';
		                var proposedv = '';
		                var avoid = '';
		                var cs = '';
		                var cs2 = '';
		                var rfqcode = '';
		                var um = '';
		                if (list[i].POSentDate != null) {
		                    posentdate = UIControlService.getStrDate(list[i].POSentDate);
		                }
		                if (list[i].POApprovedDate != null) {
		                    approvalPRdate = UIControlService.getStrDate(list[i].POApprovedDate);
		                }
		                if (list[i].BudgetedValue != null) {
		                    budgetedv = list[i].BudgetedValue;
		                }
		                if (list[i].FinalValue != null) {
		                    finalv = list[i].FinalValue;
		                }
		                if (list[i].PurposedValue != null) {
		                    proposedv = list[i].PurposedValue;
		                }
		                if (list[i].Avoidance != null) {
		                    avoid = list[i].Avoidance;
		                }
		                if (list[i].CostSaving != null) {
		                    cs = list[i].CostSaving;
		                }
		                if (list[i].CostSavingValue != null) {
		                    cs2 = list[i].CostSavingValue;
		                }
		                if (list[i].RFQCode != null) {
		                    rfqcode = list[i].RFQCode;
		                }
		                if (list[i].UnitMeasure != null) {
		                    um = list[i].UnitMeasure.replace(/"/g, "''");
		                }
		                    var forExcel = {
		                        PurchaseReq: list[i].PurchaseReq,
		                        ReqItem: list[i].RequisnItem,
		                        Quantity: list[i].Quantity,
		                        Unit: um,
		                        Description: list[i].Description,
		                        BudgetedValue: budgetedv,
		                        POSentDate: posentdate,
		                        ApprovalPRDate: approvalPRdate,
		                        OntimePO:list[i].DiffDays,
		                        FinalValue: finalv,
		                        ProposedValue: proposedv,
		                        Avoidance: avoid,
		                        CostSavingProcentage: cs,
		                        CostSaving: cs2,
		                        RFQCode: rfqcode
		                    }
		                //}
                            /*
		                else if (i ==  list.length) {
		                    var forExcel = {
		                        PurchaseReq: "",
		                        ReqItem: "",
		                        Quantity: "",
		                        Unit: "",
		                        Description: "",
		                        BudgetedValue: vm.totalBudgetedValue,
		                        POSentDate: "",
		                        ApprovalPRDate: "",
		                        FinalValue: vm.totalFinalValue,
		                        ProposedValue: vm.totalProposedValue,
		                        Avoidance: vm.totalAvoidance,
		                        CostSaving: vm.totalCostSaving
		                    }
		                }*/
		                vm.filterdata.push(forExcel);
		            }
		            JSONToCSVConvertor(vm.filterdata, true);
                    
                    
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });


		}


		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ShowLabel) {
		    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		    var arrData = JSONData;
		    //console.info(arrData[0]);
		    var CSV = '';

		    //This condition will generate the Label/Header
		    if (ShowLabel) {
		        var row = "sep=," + '\n';

		        //This loop will extract the label from 1st index of on array
		        for (var index in arrData[0]) {

		            //Now convert each value to string and comma-seprated
		            row += index + ',';
		        }

		        row = row.slice(0, -1);
		        //console.info(row);
		        //append Label row with line break
		        CSV += row + '\r\n';
		        //console.info(CSV);
		    }

		    //1st loop is to extract each row
		    for (var i = 0; i < arrData.length; i++) {
		        var row = "";

		        //2nd loop will extract each column and convert it in string comma-seprated
		        for (var index in arrData[i]) {
		            row += '"' + arrData[i][index] + '",';
		        }

		        row.slice(0, row.length - 1);

		        //add a line break after each row
		        CSV += row + '\r\n';
		    }

		    if (CSV == '') {
		        alert("Invalid data");
		        return;
		    }

		    //Generate a file name
		    var fileName = "KPI Procurement";

		    //Initialize file format you want csv or xls
		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

		    // Now the little tricky part.
		    // you can use either>> window.open(uri);
		    // but this will not work in some browsers
		    // or you will not get the correct file extension    

		    //this trick will generate a temp <a /> tag
		    var link = document.createElement("a");
		    link.href = uri;

		    //set the visibility hidden so it will not effect on your web-layout
		    link.style = "visibility:hidden";
		    link.download = fileName + ".csv";

		    //this part will append the anchor tag and remove it after automatic click
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
		}

		function getUsername() {
		    DashboardAdminService.getUsername({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.username = reply.data;
		            if (vm.username != 'admin') {
		                getEmpPos();
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETPOST");
		        UIControlService.unloadLoading();
		    });
		}
	}
})();
