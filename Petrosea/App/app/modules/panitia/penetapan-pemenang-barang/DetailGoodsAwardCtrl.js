(function () {
	'use strict';

	angular.module("app").controller("DetailGoodsAwardCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'GoodsAwardService', '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, GoodsAwardService, $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService) {
	    var vm = this;
	    vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.detail = item.item;
		vm.url = item.item.DocumentURL;
		vm.isCalendarOpened = [false, false, false, false];
		vm.init = init;
		function init() {
		    console.log(vm.detail);
			$translatePartialLoader.addPart('goods-award');
			UploadFileConfigService.getByPageName("PAGE.ADMIN.VHSAWARD", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});

			if (vm.isAdd === true) {
				vm.action = "Tambah";

			} else {
				vm.action = "Ubah ";

			}
			//if (vm.detail.NoPO !== null) {
			//	vm.NoPO = vm.detail.NoPO;
			//	vm.PODate = vm.detail.PODate;
			//}
			//convertToDate();
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.PODate) {
				vm.PODate = UIControlService.getStrDate(vm.PODate);
			}
		};

		//Supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.PODate) {
				vm.PODate = new Date(Date.parse(vm.PODate));
			}
		}

		/*start upload */
		vm.uploadFile = uploadFile;
		function uploadFile() {
			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.detail.TenderStepID.toString() + "_" + vm.detail.VendorID.toString());
			}
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}

			var selectedFileType = file[0].type;
			selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);

			if (selectedFileType === "vnd.openxmlformats-officedocument.wordprocessingml.document") {
				selectedFileType = "docx";
			} else if (selectedFileType == "msword") {
				selectedFileType = "doc";
			} else {
				selectedFileType = selectedFileType;
			}
			vm.type = selectedFileType;
			console.info("filenew:" + selectedFileType);

			//jika excel
			if (selectedFileType === "vnd.ms-excel") var allowed = false;

			for (var i = 0; i < allowedFileTypes.length; i++) {
				if (allowedFileTypes[i].Name == selectedFileType) {
					allowed = true;
					return allowed;
				}
			}

			if (!allowed) {
				UIControlService.msg_growl("warning", "MESSAGE.ERR_INVALID_FILETYPE");
				return false;
			}
		}

		vm.upload = upload;
		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}

			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileGoodsAward(dates, file, size, filters, function (response) {
				UIControlService.unloadLoading();
				console.info("response:" + JSON.stringify(response));
				if (response.status == 200) {
					var url = response.data.Url;
					vm.pathFile = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;

					//if (vm.flag == 0) {
					//	vm.size = Math.floor(s);
					//} else if (vm.flag == 1) {
					//	vm.size = Math.floor(s / (1024));
					//} if (vm.detail.ID == 0) {
					//	var dataSimpan = {
					//		ID: vm.detail.ID,
					//		DocumentUrl: vm.pathFile,
					//		TenderStepID: vm.Step,
					//		VendorID: vm.detail.VendorID,
					//		StartContractDate: vm.detail.StartContractDate,
					//		Duration: vm.detail.Duration,
					//		FinishContractDate: vm.detail.FinishContractDate,
					//		SAPContractNo: vm.detail.SAPContractNo,
					//		FinishContractPVIDate: vm.detail.FinishContractPVIDate,
					//		RFQVHSId: vm.reff,
					//		RFQType: vm.detail.RFQType
					//	};
					//} else {
					//	var dataSimpan = {
					//		ID: vm.detail.ID,
					//		DocumentUrl: vm.pathFile
					//	};
					//}

					GoodsAwardService.uploadDoc({ ID: vm.detail.ID, DocumentURL: vm.pathFile }, function (reply) {
						UIControlService.unloadLoadingModal();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_UPLOAD");
							$uibModalInstance.close();

						} else {
							UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
							return;
						}
					}, function (err) {
					    if (err.data == "ERRORS.FILE_SIZE_EXCEEDED") {
					        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					    } else {
					        UIControlService.msg_growl("error", "MESSAGE.API");
					    }
						UIControlService.unloadLoadingModal();
					});

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
			    if (response.data == "ERRORS.FILE_SIZE_EXCEEDED") {
			        UIControlService.msg_growl("error", "MESSAGE.FILE_SIZE_EXCEEDED");
			    } else {
			        UIControlService.msg_growl("error", "MESSAGE.API")
			    }
				UIControlService.unloadLoading();
			});
		}

		vm.simpan = simpan;
		function simpan() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			convertAllDateToString();
			GoodsAwardService.update({
				ID: vm.detail.ID,
				TenderStepID: vm.detail.TenderStepID,
				VendorID: vm.detail.VendorID,
				TotalNego: vm.detail.TotalNego,
				NoPO: vm.NoPO,
				PODate: vm.PODate
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_DATA_PO");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		};
	}
})();;