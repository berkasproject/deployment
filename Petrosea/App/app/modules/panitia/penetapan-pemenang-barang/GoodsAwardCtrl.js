(function () {
	'use strict';

	angular.module("app").controller("GoodsAwardCtrl", ctrl);

	ctrl.$inject = ['ApprovalNoSAPService', '$filter', 'Excel', '$timeout', '$translatePartialLoader', 'GoodsAwardService', 'UIControlService', '$uibModal', '$stateParams', 'DataPengadaanService', '$state'];
	function ctrl(ApprovalNoSAPService, $filter, Excel, $timeout, $translatePartialLoader, GoodsAwardService, UIControlService, $uibModal, $stateParams, DataPengadaanService, $state) {

		var vm = this;
		var page_id = 141;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.TenderID = Number($stateParams.TenderID);
		vm.evalsafety = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.userBisaMengatur = false;
		vm.allowAdd = true;
		vm.allowEdit = true;
		vm.allowDelete = true;
		vm.kata = new Kata("");
		vm.init = init;
		vm.exportHref;
		vm.detail = [];
		vm.jLoad = jLoad;
		vm.isCalendarOpened = [false, false, false, false];
		vm.isCancelled;
		vm.flagApprove = false;
		vm.Remark = "";
		function init() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			$translatePartialLoader.addPart('goods-award');

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			jLoad(1);
			getUserLogin();
		}
		vm.jLoad = jLoad;
		function jLoad(current) {
			var tender = {
				column: vm.StepID,
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}
			GoodsAwardService.select(tender, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.detail = reply.data.List;
					for (var i = 0; i < vm.detail.length; i++) {
						loadTaxCode(vm.detail[i]);
						loadStorageLocation(vm.detail[i]);
						if (vm.detail[i].PODate != null)
							vm.detail[i].StartDate = UIControlService.convertDateTime(vm.detail[i].StartDate);
						vm.detail[i].EndDate = UIControlService.convertDateTime(vm.detail[i].EndDate);

						//vm.detail[i].PODate = new Date(Date.parse(vm.detail[i].PODate));
						if (vm.detail[i].ApprovalStatusReff == null) {
							vm.detail[i].Status = 'Draft';
							vm.detail[i].flagTemp = 0;
						} else if (vm.detail[i].ApprovalStatusReff != null) {
							cekEmployee(vm.detail[i].ID, vm.detail[i]);
							vm.detail[i].Status = vm.detail[i].ApprovalStatusReff.Value;
							vm.detail[i].flagTemp = 1;
						}

						if (vm.detail[i].ApprovalStatusReff == null || vm.detail[i].Status === 'Draft' || vm.detail[i].Status === 'REJECT') {
							vm.flagApprove = true;
							vm.detail[i].flagApprove = true;
						} else {
							vm.detail[i].flagApprove = false;
						}
						vm.detail[i].StartContractDate = new Date(Date.parse(vm.detail[i].StartContractDate));
					}
					vm.count = reply.data.Count;
					if (vm.count !== 0) {
						loadExcelVendor();
					}
					console.info("data:" + JSON.stringify(reply.data));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Penetapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal mendapatkan data Penetapan Pemenang" });
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadTaxCode = loadTaxCode;
		function loadTaxCode(data) {
			GoodsAwardService.selectTaxCode(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listTaxCode = reply.data;
					for (var x = 0; x < reply.data.length; x++) {
						if (data.TaxCode == reply.data[x].ID) {
							data.selectTaxCode = reply.data[x];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadStorageLocation = loadStorageLocation;
		function loadStorageLocation(data) {
			GoodsAwardService.selectStorageLocation(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listStorageLocation = reply.data;
					for (var y = 0; y < reply.data.length; y++) {
						if (data.StorageLocation == reply.data[y].ID) {
							data.selectStorageLocation = reply.data[y];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.cekEmployee = cekEmployee;
		function cekEmployee(Id, reff) {
			GoodsAwardService.CekEmployee({
				ID: Id
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					reff.flagEmp = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString(data) { // TIMEZONE (-)
			if (data) {
				data = UIControlService.getStrDate(data);
			}
		};

		//supaya muncul di date picker saat awal load
		function convertToDate(data) {
			if (data) {
				data = new Date(Date.parse(data));
			}
		}

		vm.loadExcelVendor = loadExcelVendor;
		function loadExcelVendor() {
			vm.vendorExcelAll = [];
			vm.vendorExcels = [];
			var tender = {
				column: vm.StepID,
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}
			GoodsAwardService.selectExcelVendor(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.vendorExcelAll = reply.data;
					vm.detail.forEach(function (det) {
						var list = vm.vendorExcelAll.filter(function (exc) {
							return exc.VendorCode === det.VendorSAPCode;
						});
						vm.vendorExcels.push({
							list: list
						});
					});
					console.info("data:" + JSON.stringify(vm.vendorExcels));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.save = save;
		function save(data) {
			console.info(data);
			if (data.selectStorageLocation == null) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_STORAGE");
			} else if (data.selectTaxCode == null) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_TAXCODE");
			} else if (data.selectStorageLocation == null) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_STORAGE");
			} else {
				UIControlService.loadLoadingModal("MESSAGE.LOADING");
				GoodsAwardService.updateApproval(data, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "SUCC_APPRV");
					} else {
						UIControlService.msg_growl("error", "ERR_SAVE_DATA");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "ERR_SAVE_DATA");
					UIControlService.unloadLoadingModal();
				});

			}
		}

		vm.simpan = simpan;
		vm.List = [];
		function simpan() {
			for (var i = 0; i < vm.detail.length; i++) {
				if (vm.detail[i].Status === 'Draft' || vm.detail[i].Status === 'REJECT') {
					if (vm.detail[i].selectTaxCode === undefined) {
						UIControlService.msg_growl("warning", "{{'MESSAGE.TAXCODE_FILL'}}");
						return;
					} else if (vm.detail[i].selectStorageLocation === undefined) {
						UIControlService.msg_growl("warning", "MESSAGE.STOR_LOC_FILL");
						return;
					} /*else if (vm.detail[i].PODate === null) {
						UIControlService.msg_growl("warning", "MESSAGE.DELIV_DATE_FILL");
						return;
					}*/ else {
						UIControlService.loadLoading("MESSAGE.LOADING");
						var dataGoods = {
							ID: vm.detail[i].ID,
							TenderStepID: vm.detail[i].TenderStepID,
							VendorID: vm.detail[i].VendorID,
							TotalNego: vm.detail[i].TotalNego,
							TaxCode: vm.detail[i].selectTaxCode.ID,
							Summary: vm.detail[i].Summary,
							StorageLocation: vm.detail[i].selectStorageLocation.ID
							//PODate: UIControlService.getStrDate(vm.detail[i].PODate)
						}
						vm.List.push(dataGoods);
						if (i == vm.detail.length - 1) {
							GoodsAwardService.update(vm.List, function (reply) {
								UIControlService.unloadLoading();
								if (reply.status === 200) {
									vm.ListDeal = [];
									vm.flag = false;
									UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_DATA_PO");
									window.location.reload();

								} else {
									UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
									return;
								}
							}, function (err) {
								UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
								UIControlService.unloadLoadingModal();
							});
						}
					}
				}
			}
		}

		vm.edit = edit;
		function edit(dataTabel) {
			var data = {
				item: dataTabel
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/Detail.html',
				controller: 'DetailGoodsAwardCtrl',
				controllerAs: 'DetailGoodsAwardCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.Export = Export;
		function Export(index) {
			vm.exportHref = Excel.tableToExcel('#tableToExport' + index, 'sheet name');
			$timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
		}

		vm.getUserLogin = getUserLogin;
		function getUserLogin() {
			GoodsAwardService.CekRequestor({
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.flagSave = reply.data;
					console.info(vm.flagSave);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.sendToApproveL1L2 = sendToApproveL1L2
		function sendToApproveL1L2(data) {
			vm.ID = data.ID;
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV_L1_L2'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					var dt = {
						TenderStepDataId: vm.StepID,
						VendorID: data.VendorID
					};
					ApprovalNoSAPService.sendToApproveL1L2(dt, function (reply) {
						if (reply.status === 200) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
					});
				}
			});
		}

		vm.sendToApprove = sendToApprove;
		function sendToApprove(data) {
			vm.ID = data.ID;
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					var dt = {
						ID: data.ID,
						TenderStepID: vm.StepID,
						flagEmp: 1
					};
					GoodsAwardService.SendApproval(dt, function (reply) {
						if (reply.status === 200) {
							sendMailRFQ(0);
							UIControlService.unloadLoading();
							UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
					});
				}
			});
		}

		vm.sendMailRFQ = sendMailRFQ;
		function sendMailRFQ(flag) {
			GoodsAwardService.sendMailRFQGoods({
				Status: vm.ID, FilterType: flag, Remark: vm.Remark
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.EMAIL");
				} else {
					// UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval(dt, data) {
			var item = {
				ID: data.ID,
				flag: data.flagEmp,
				Status: dt
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/detailApproval.modal.html',
				controller: 'detailApprovalSignOffCtrl',
				controllerAs: 'detailApprovalSignOffCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function (ID, filter, flag) {
				init();
			});
		};

		vm.detailApprovalL1L2 = detailApprovalL1L2;
		function detailApprovalL1L2(dt) {
			var item = {
				TenderStepDataId: vm.StepID,
				VendorID: dt.VendorID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-no-SAPcode/approval-no-SAPcode.modal.html',
				controller: 'detailApprovalNoSAPCtrl',
				controllerAs: 'detailApprovalNoSAPCtrl',
				resolve: { item: function () { return item } }
			})
			modalInstance.result.then(function (ID, filter, flag) {
				init()
			})
		}

		vm.createExcel = createExcel;
		function createExcel() {
			var data = {
				StepID: vm.StepID,
				TenderRefID: vm.TenderRefID,
				ProcPackType: vm.ProcPackType
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/SaveExcelNotif.html',
				controller: "SaveNotifExcel",
				controllerAs: "SaveNotifExcel",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.openSummary = openSummary
		function openSummary(data) {
			var item = {
				Summary: data.Summary,
				VendorName: data.VendorName,
				isReadOnly: !vm.isAllowedEdit || !(data.Status === 'Draft' || data.Status === 'REJECT'),
				ID: data.ID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/goodsSummary.modal.html',
				controller: 'goodsSummaryController',
				controllerAs: 'goodsSummaryController',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function (summary) {
				data.Summary = summary;
			});
		}

		vm.print = print;
		function print() {
			$state.transitionTo('penetapan-pemenang-barang-print', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
		}

		vm.awardReport = awardReport;
		function awardReport(vendorId) {
			$state.transitionTo('award-report', { TenderStepDataID: vm.StepID, ProcPackType: vm.ProcPackType, VendorID: vendorId });
		}

		vm.cancelVendor = cancelVendor;
		function cancelVendor(data) {
			vm.ID = data.ID;
			if (localStorage.getItem("currLang") === 'id') {
				var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Alasan membatalkan vendor : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*jika klik OK, maka secara otomatis kirim approval untuk membatalkan vendor</div></div></form>');
			} else if (localStorage.getItem("currLang") === 'en') {
				var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Reasons to cancel the Vendor : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*If click OK, then it will be automatically sent for approval process</div></div></form>');
			}
			bootbox.confirm(form, function (yes) {
				if (yes) {
					vm.Remark = form.find('textarea[name=usernameInput]').val();
					if (vm.Remark == "") {
						UIControlService.msg_growl('error', "MESSAGE.NO_REASON");
						return false;
					} else {
						UIControlService.loadLoadingModal("MESSAGE.LOADING");
						GoodsAwardService.cancelVendor({
							ID: data.ID,
							RemarkCancel: vm.Remark
						}, function (reply) {
							if (reply.status === 200) {
								UIControlService.unloadLoading();
								sendMailRFQ(0);
								init();
							}
						}, function (err) {
							// UIControlService.msg_growl("error", "Gagal Akses Api!!");
						});
					}
				}
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
		    $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.TenderID });
		}

	}
})();
//TODO

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}

