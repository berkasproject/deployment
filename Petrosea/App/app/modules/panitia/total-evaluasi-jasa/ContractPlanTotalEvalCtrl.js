﻿(function () {
    'use strict';

    angular.module("app").controller("tenderVerTotEvalCtrl", ctrl);

    ctrl.$inject = ['$filter', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'TotalEvaluasiJasaService', '$state', 'UIControlService', 'UploadFileConfigService', 'UploaderService',
        'GlobalConstantService', '$uibModal', '$stateParams'];
    function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService,
        TEJService, $state, UIControlService, UploadFileConfigService, UploaderService,
        GlobalConstantService, $uibModal, $stateParams) {
        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.TenderName = '';
        vm.ProsentaseTechnical;
        vm.ProsentasePricing;
        vm.TenderID;
        vm.pageSize = 10;
        vm.Keyword = '';
        vm.init = init;
        function init() {
            getLoginCP();
            $translatePartialLoader.addPart('verifikasi-tender');
            $translatePartialLoader.addPart('total-evaluasi-jasa');
            loadMethodEval(1);
        }

        vm.getLoginCP = getLoginCP;
        function getLoginCP() {
            TEJService.getLoginCP(function (reply) {
                if (reply.status === 200) {
                    vm.flagCheckList = reply.data;
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        /*
        function loadDataTender() {
            TEJService.getDataStepTender({
                ID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.TenderName = data.tender.TenderName;
                    vm.StartDate = UIControlService.getStrDate(data.StartDate);
                    vm.EndDate = UIControlService.getStrDate(data.EndDate);
                    vm.nama_tahapan = data.step.TenderStepName;
                    vm.TenderID = data.TenderID;

                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }
        */

        vm.loadMethodEval = loadMethodEval;
        function loadMethodEval(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.currentPage = current;
			var offset = (current * 10) -10;
            TEJService.getDataVerification({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.Keyword,
                Status: 1

            }, function (reply) {
                if (reply.status === 200) {
                    vm.contracttotal = reply.data.List;
                    for (var i = 0; i < vm.contracttotal.length; i++) {
                        vm.contracttotal[i].ReviewDate = UIControlService.convertDateTime(vm.contracttotal[i].ReviewDate);
                        if (vm.contracttotal[i].ApprovalStatusReff.Name == "CR_APPROVED" || vm.contracttotal[i].ApprovalStatusReff.Name == "APPROVAL_VENDOR_REJECTED") {
                            vm.contracttotal[i].flagButton = false;

                        }
                        else vm.contracttotal[i].flagButton = true;
                    }
                    vm.totalItems = Number(reply.data.Count);
                    if (reply.data.Count != 0)
                        vm.IDTender = vm.contracttotal[0].tender.TenderRefID;
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.onSearchSubmit = function (searchText) {
            vm.Keyword = searchText;
            loadMethodEval(1);
        };

        vm.detailtotal = detailtotal;
        function detailtotal(detail) {
            var item = {
                TenderStepID: detail.TenderStepID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/total-evaluasi-jasa/detailTotalEvaluasi.html',
                controller: 'detTotalEvaluasiCtrl',
                controllerAs: 'detTotalEvaluasiCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        /*
        vm.backDetailTahapan = backDetailTahapan;
        function backDetailTahapan() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType });
        }
        */

        vm.aturApproval = aturApproval;
        function aturApproval(dt) {
           // $state.transitionTo('verifikasi-totalEval-atur-app', { contractRequisitionId: dt.tender.TenderRefID});
            vm.ID = dt.TotalEvaluationId;
            $state.transitionTo('committee-totalEval', { contractRequisitionId: dt.tender.TenderRefID, totalEval: vm.ID, TenderStepID: dt.TenderStepID });

        };

        

        vm.detailApproval = detailApproval;
        function detailApproval(dt) {
            var item = {
                TenderId: dt.tender.TenderRefID,
                Status: 1
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/total-evaluasi-jasa/detailApproval.modal.html?v=1.000002',
                controller: 'detailApprovalEvalCtrl',
                controllerAs: 'detailApprovalEvalCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                init();
            });
        };

       
    }
})();