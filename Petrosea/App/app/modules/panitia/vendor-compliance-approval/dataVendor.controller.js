﻿(function () {
    'use strict';

    angular.module("app").controller("VendorComplianceApprovalDataVendorCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VendorComplianceApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, VendorComplianceApprovalService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
            //$translatePartialLoader.addPart('contract-variation');
            $translatePartialLoader.addPart('akta-pendirian');
            vm.data = item.data;
            aktaPendirian();

        }


        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }

        vm.files1 = [];
        vm.files2 = [];
        vm.files3 = [];
        function aktaPendirian() {
            VendorComplianceApprovalService.aktaPendirian({ VendorID: vm.data.Vendor.VendorID }, function (reply) {
                if (reply.status == 200) {
                    vm.dataAkta = reply.data;
                    if (vm.dataAkta.length > 0) {
                        for (var i = 0; i < vm.dataAkta.length; i++) {
                            if (vm.dataAkta[i].DocumentType === 'LEGAL_DOC_PENDIRIAN') {
                                vm.files1.push(vm.dataAkta[i]);
                            } else if (vm.dataAkta[i].DocumentType === 'LEGAL_DOC_PERUBAHAN') {
                                vm.files2.push(vm.dataAkta[i]);
                            } else if (vm.dataAkta[i].DocumentType === 'LEGAL_DOC_PENGESAHAN') {
                                vm.files3.push(vm.dataAkta[i]);
                            }
                            vm.dataAkta[i].DocumentDateConverted = UIControlService.convertDate(vm.dataAkta[i].DocumentDate);
                            vm.dataAkta[i].FilesizeKB = vm.dataAkta[i].Filesize / 1024;
                            vm.dataAkta[i].FilesizeKB = vm.dataAkta[i].FilesizeKB.toFixed(1);
                        }
                    }
                    //console.info("files1" + JSON.stringify(vm.files1));
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
                UIControlService.unloadLoading();
            });
        }


    }
})();
//TODO


