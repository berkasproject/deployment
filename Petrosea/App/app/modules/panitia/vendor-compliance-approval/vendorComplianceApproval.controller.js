(function () {
	'use strict';

	angular.module("app").controller("VendorComplianceApprovalCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'VendorComplianceApprovalService', 'UIControlService', '$filter', '$uibModal'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, VendorComplianceApprovalService, UIControlService, $filter, $uibModal) {

		var vm = this;
		vm.init = init;

		vm.maxSize = 10;
		vm.currentPage = 1;


		vm.listDropdown =
        [
            { Value: 0, Name: "SELECT.ALL" },
            { Value: 1, Name: "SELECT.APPROVED" },
            { Value: 2, Name: "SELECT.REJECTED" },
            { Value: 3, Name: "SELECT.ON_PROCESS" }
        ]

		vm.filter = vm.listDropdown[3];

		vm.data = [];
		function init() {
		    $translatePartialLoader.addPart('verifikasi-data');
		    //dataWorkloadProc();
		    //grafikNewVendor();
		    dataApproval(1);
		}

		vm.data = [];
		vm.totalItems = 0;
		vm.dataApproval = dataApproval;
		function dataApproval(current) {
		    vm.currentPage = current;
		    var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    VendorComplianceApprovalService.dataApproval({
		        Offset: offset,
		        Limit: vm.maxSize,
		        FilterType: vm.filter.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.data = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("approval:" + JSON.stringify(vm.data));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.approve = approve;
		function approve(data, isApprove) {
		    var item = {
		        data: data,
                isApprove:isApprove
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/vendor-compliance-approval/detailApproval.modal.html',
		        controller: 'DetailApprovalComplianceCtrl',
		        controllerAs: 'DetailApprovalComplianceCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
            /*
		    VendorComplianceApprovalService.approve({
		        ID: data.VendorComplianceApprovalDetail1.ID,
		        VendorComplianceApprovalID: data.ID,
                IsApprove:isApprove
		    }, function (reply) {
		        if (reply.status === 200) {
		            UIControlService.msg_growl("success", "Berhasil memproses data");
		            init();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal memproses data");
		        UIControlService.unloadLoading();
		    });
            */
		}

		vm.lihat = lihat;
		function lihat(data) {
		    var item = {
		        data:data
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/vendor-compliance-approval/lihat-kuesioner.html',
		        controller: 'LihatKuesionerReviewerCtrl',
		        controllerAs: 'LihatKuesionerReviewerCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}

		vm.datavendor = datavendor;
		function datavendor(data) {
		    var item = {
		        data: data
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/vendor-compliance-approval/dataVendor.html',
		        controller: 'VendorComplianceApprovalDataVendorCtrl',
		        controllerAs: 'VendorComplianceApprovalDataVendorCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}

		vm.tambah = tambah;
		function tambah() {
		    $state.transitionTo('questionnaire-template-form', { questId: 0 });
		}
		vm.ubah = ubah;
		function ubah(id) {
		    $state.transitionTo('questionnaire-template-form', { questId: id });
		}

	}
})();
