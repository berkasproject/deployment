(function () {
	'use strict';
	angular.module("app").controller("DataPerlengkapanController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'AssessmentPrequalService', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'DataPerlengkapanService', 'UIControlService', '$filter'];
	/* @ngInject */
	function ctrl($state, $stateParams, AssessmentPrequalService, $http, $uibModal, $translate, $translatePartialLoader, $location, DataPerlengkapanService, UIControlService, $filter) {
		var vm = this;
		vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
		vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
		vm.VEPID = Number($stateParams.VEPID);
		vm.maxSize = 10;
		vm.msgLoading = "MSG.LOADING";
		vm.currentPage = 1;
		vm.isApprovedCR = false;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("data-perlengkapan");
			loadSetupStep();
		}

		vm.loadSetupStep = loadSetupStep;
		function loadSetupStep() {
			AssessmentPrequalService.SelectSetupStep({
			    Status: vm.PrequalStepAssID,
			    column: vm.VendorPrequalID,
			    Offset: vm.VEPID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.isEntry = reply.data.IsNotStarted;
					vm.PrequalStepID = reply.data.PrequalStepID;
					vm.PrequalSetupID = reply.data.PrequalSetupID;
					vm.IsNeedRevision = reply.data.IsNeedRevision;
					vm.remark = reply.data.Remark;
					loadBuilding(1);
					loadBuildingPrequal(1);
					loadEquipmentVehicle(1);
					loadEquipmentVehiclePrequal(1);
					loadEquipmentTools(1);
					loadEquipmentToolsPrequal(1);
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.listBuilding = [];
		function loadBuilding(current) {
			UIControlService.loadLoading(vm.msgLoading);
			AssessmentPrequalService.selectBuilding({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listBuilding = reply.data;
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.listBuildingPrequal = [];
		function loadBuildingPrequal(current) {
			UIControlService.loadLoading(vm.msgLoading);
			AssessmentPrequalService.selectBuildingPrequal({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listBuildingPrequal = reply.data;
				console.info("listbuilding" + JSON.stringify(vm.listBuildingPrequal));
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.listVehicle = [];
		function loadEquipmentVehicle(current) {
			UIControlService.loadLoading(vm.msgLoading);
			AssessmentPrequalService.selectEquipment({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID,
				flagEquip: 1
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listVehicle = reply.data;
				vm.totalItems = Number(reply.data.Count);
				for (var i = 0; i < vm.listVehicle.length; i++) {
					vm.listVehicle[i].MfgDate = UIControlService.getStrDate(vm.listVehicle[i].MfgDate);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.listVehiclePrequal = [];
		function loadEquipmentVehiclePrequal(current) {
			UIControlService.loadLoading(vm.msgLoading);
			AssessmentPrequalService.selectEquipmentPrequal({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID,
				flagEquip: 1
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listVehiclePrequal = reply.data;
				vm.totalItems = Number(reply.data.Count);
				for (var i = 0; i < vm.listVehiclePrequal.length; i++) {
					vm.listVehiclePrequal[i].MfgDate = UIControlService.getStrDate(vm.listVehiclePrequal[i].MfgDate);
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.listEquipmentTools = [];
		function loadEquipmentTools(current) {
			UIControlService.loadLoading(vm.msgLoading);
			AssessmentPrequalService.selectEquipmentTools({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID,
				flagEquip: 2
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listEquipmentTools = reply.data;
				for (var i = 0; i < vm.listEquipmentTools.length; i++) {
					vm.listEquipmentTools[i].MfgDate = UIControlService.getStrDate(vm.listEquipmentTools[i].MfgDate);
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.listEquipmentToolsPrequal = [];
		function loadEquipmentToolsPrequal(current) {
			UIControlService.loadLoading(vm.msgLoading);
			AssessmentPrequalService.selectEquipmentToolsPrequal({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID,
				flagEquip: 2
			},
            function (reply) {
            	UIControlService.unloadLoading();
            	vm.listEquipmentToolsPrequal = reply.data;
            	//console.info("alat:" + JSON.stringify(vm.listEquipmentToolsPrequal));
            	for (var i = 0; i < vm.listEquipmentToolsPrequal.length; i++) {
            		vm.listEquipmentToolsPrequal[i].MfgDate = UIControlService.getStrDate(vm.listEquipmentToolsPrequal[i].MfgDate);
            	}
            }, function (err) {
            	UIControlService.unloadLoading();
            });
		}

		vm.deleteData = deleteData;
		function deleteData(type, data) {
			bootbox.confirm($filter('translate')('MESSAGE.DELETECONFIRM'), function (res) {
				if (res) {
					if (type === "building") {
						UIControlService.loadLoading('MSG.DELETING');
						AssessmentPrequalService.deleteBuilding({
							ID: data.ID
						}, function () {
							UIControlService.unloadLoading();
							window.location.reload();
						}, function () {
							UIControlService.unloadLoading();
						});
					} else {
						UIControlService.loadLoading('MSG.DELETING');
						AssessmentPrequalService.deleteNonBuilding({
							ID: data.ID
						}, function () {
							UIControlService.unloadLoading();
							window.location.reload();
						}, function () {
							UIControlService.unloadLoading();
						});
					}
				}
			});
		}

		vm.openForm = openForm;
		function openForm(type, data, isAdd, IsCR) {
			// console.info("cr:" + IsCR);
			var data = {
				type: type,
				data: data,
				isForm: isAdd,
				VendorEntryPrequalID: vm.VEPID
				//IsCR: IsCR
			}
			var temp;
			var ctrl;
			var ctrlAs;
			if (type === "building") {
				temp = "app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/data-perlengkapan/formBuilding.html";
				ctrl = "FormBuildingController";
				ctrlAs = "FormBuildingCtrl";
			} else {
				temp = "app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/data-perlengkapan/formNonBuilding.html";
				ctrl = "FormNonBuildingController";
				ctrlAs = "FormNonBuildingCtrl";
			}
			var modalInstance = $uibModal.open({
				templateUrl: temp,
				controller: ctrl,
				controllerAs: ctrlAs,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.detailForm = detailForm;
		function detailForm(type, data, isAdd) {
			var data = {
				type: type,
				data: data,
				isForm: isAdd
			}
			var ctrl;
			var ctrlAs;
			if (type === "building") {
				ctrl = "FormBuildingController";
				ctrlAs = "FormBuildingCtrl";
			} else {
				ctrl = "FormNonBuildingController";
				ctrlAs = "FormNonBuildingCtrl";
			}

			var modalInstance = $uibModal.open({
				templateUrl: "app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/data-perlengkapan/detailData.html",
				controller: ctrl,
				controllerAs: ctrlAs,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.editActiveBuilding = editActiveBuilding;
		function editActiveBuilding(data, active) {
			var act = null; var isTemp = null;
			if (vm.isApprovedCR == true) {
				act = 2; isTemp = true;
			}
			UIControlService.loadLoading("Silahkan Tunggu");
			DataPerlengkapanService.editActiveBulding({
				ID: data.ID,
				IsActive: active,
				//Action: act,
				//IsTemporary: isTemp
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var msg = "";
					if (active === false) msg = " NonAktifkan ";
					if (active === true) msg = "Aktifkan ";
					UIControlService.msg_growl("success", "Data Berhasil di " + msg);
					vm.init();
				}
				else {
					UIControlService.msg_growl("error", "Gagal menonaktifkan data ");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "Gagal Akses API ");
				UIControlService.unloadLoading();
			});

		}

		vm.editActiveNonBuilding = editActiveNonBuilding;
		function editActiveNonBuilding(data, active) {
			var act = null; var isTemp = null;
			//if (vm.isApprovedCR == true) {
			//	act = 2;
			//	isTemp = true;
			//}
			UIControlService.loadLoading("Silahkan Tunggu");
			DataPerlengkapanService.editActiveNonBulding({
				ID: data.ID,
				IsActive: active,
				//Action: act,
				//IsTemporary: isTemp
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var msg = "";
					if (active === false) msg = " NonAktifkan ";
					if (active === true) msg = "Aktifkan ";
					UIControlService.msg_growl("success", "Data Berhasil di " + msg);
					vm.init();
				} else {
					UIControlService.msg_growl("error", "Gagal menonaktifkan data ");
					return;
				}
			}, function (err) {

				UIControlService.msg_growl("error", "Gagal Akses API ");
				UIControlService.unloadLoading();
			});
		}

		vm.valid = true;
		vm.notvalid = function notvalid() {
			vm.valid = false;
		}

		vm.Submit = Submit;
		function Submit(flag) {
			vm.valid = flag;
			if (vm.remark == undefined) {
				vm.remark = "";
			}
			AssessmentPrequalService.InsertSubmit({
				PrequalStepID: vm.PrequalStepAssID,
				VendorPrequalID: vm.VendorPrequalID,
				IsAgree: flag,
				VEPID: vm.VEPID,
				Remark: vm.remark
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					$state.go('assessment-prakualifikasi-detail', {
						SetupStepID: vm.PrequalSetupID,
						//PrequalStepID: vm.PrequalStepID,
						PrequalStepID: vm.PrequalStepAssID,
						VendorPrequalID: vm.VendorPrequalID
					});
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

	}
})();// baru controller pertama