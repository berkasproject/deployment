﻿(function () {
    'use strict';

    angular.module("app")
    .controller("LicenseAssPrequalCtrl", ctrl);

    ctrl.$inject = ['$filter', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'AssessmentPrequalService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService, AssessmentPrequalService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams) {
        var vm = this;
        vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
        vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
        vm.VEPID = Number($stateParams.VEPID);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.textSearch = '';
        vm.listPengumuman = [];
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.step = [];
        vm.init = init;
        vm.oriDate = new Date();
        vm.listPersonalPrequal = [];
        vm.listPersonal = [];
        vm.addressFlagPrequal = 0;
        function init() {
            $translatePartialLoader.addPart('assessment-prakualifikasi');
            $translatePartialLoader.addPart('data-izinusaha');
            $translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
            $translatePartialLoader.addPart("permintaan-ubah-data");
            loadStep();
            loadSetupStep();
        }


        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID,
                Offset: vm.VEPID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isEntry = reply.data.IsNotStarted;
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID;
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    vm.remark = reply.data.Remark;
                    loadVendorLicensePrequal();
                    loadVendorLicense();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadStep = loadStep;
        function loadStep() {
            AssessmentPrequalService.selectStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                    vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorLicensePrequal = loadVendorLicensePrequal;
        function loadVendorLicensePrequal() {
            vm.iplusPrequal = 0;
            AssessmentPrequalService.loadLicensePrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    var list = reply.data;
                    for (var i = 0; i < list.length; i++) {
                        if (!(list[i].IssuedDate === null)) {
                            list[i].IssuedDate = UIControlService.getStrDate(list[i].IssuedDate);
                        }
                        if (!(list[i].ExpiredDate === null)) {
                            list[i].ExpiredDate = UIControlService.getStrDate(list[i].ExpiredDate);
                        }
                    }
                    vm.listLicensiPrequal = list;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendorLicense = loadVendorLicense;
        function loadVendorLicense() {
            AssessmentPrequalService.loadLicense({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    var list = reply.data;
                    for (var i = 0; i < list.length; i++) {
                        if (!(list[i].IssuedDate === null)) {
                            list[i].IssuedDate = UIControlService.getStrDate(list[i].IssuedDate);
                        }
                        if (!(list[i].ExpiredDate === null)) {
                            list[i].ExpiredDate = UIControlService.getStrDate(list[i].ExpiredDate);
                        }
                    }
                    vm.listLicensi = list;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.view = view;
        function view(data) {
            $state.transitionTo('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepID, VendorPrequalID: data.VendorPrequalID });
        }

        vm.valid = true;
        vm.notvalid = function notvalid() {
            vm.valid = false;
        }


        vm.Submit = Submit;
        function Submit(flag) {
            vm.valid = flag;
            if (vm.remark == undefined) {
                vm.remark = "";
            }
            AssessmentPrequalService.InsertSubmit({
                PrequalStepID: vm.PrequalStepAssID,
                VendorPrequalID: vm.VendorPrequalID,
                IsAgree: flag,
                VEPID: vm.VEPID,
                Remark:vm.remark
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $state.go('assessment-prakualifikasi-detail', {SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.openForm = openForm;
        function openForm(data, isForm) {
            vm.dataUpdate = {
                item: data,
                isForm: isForm ,
                //cityID: vm.cityID,
                VendorEntryPrequal: {
                    PrequalSetupStepID: vm.PrequalStepID
                },
                VendorEntryPrequalID:vm.VEPID
            }
            var temp;
            if (isForm === true) {
                temp = "app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/izin-usaha/form-izin-usaha.html";
            } else {
                temp = "app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/izin-usaha/detail-izin-usaha.html";
            }
            var modalInstance = $uibModal.open({
                templateUrl: temp,
                controller: 'FormIzinPrequalCtrl',
                controllerAs: 'FormIzinPrequalCtrl',
                resolve: {
                    item: function () {
                        return vm.dataUpdate;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.deleteLic = deleteLic;
        function deleteLic(lic) {
            vm.lic = lic;
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
                if (reply) {
                    UIControlService.loadLoading("DELETING");
                    AssessmentPrequalService.deleteLicensePrequal({ ID: lic.ID }, function (reply) {
                        if (reply.status == 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("success", "MESSAGE.DELETE_SUCCESS");
                            init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", "MESSAGE.DELETE_FAILED");
                            return;
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", "MESSAGE.DELETE.FAILED");
                        return;
                    });
                }
            });
        }

    }
})();;