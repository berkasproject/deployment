﻿
(function () {
	'use strict';

	angular.module("app").controller("formPengurusPerusahaanCtrl", ctrl);

	ctrl.$inject = ['AssessmentPrequalService', '$uibModalInstance', 'item', '$location','PengurusPerusahaanPrequalService', 'PengurusPerusahaanService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'ProvinsiService'];
	/* @ngInject */
	function ctrl(AssessmentPrequalService, $uibModalInstance, item, $location, PengurusPerusahaanPrequalService, PengurusPerusahaanService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, ProvinsiService) {
		var loadmsg = 'MESSAGE.LOADING';
		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.compPerson = item.compPerson;
		vm.file;
		vm.isCalendarOpened = [false, false, false];
		vm.positionTypes = [];
		//vm.IsCR = item.compPerson.IsCR;
		vm.vendorLocation = item.compPerson.Location;
		vm.action = item.action;

		//console.info("cr:" + JSON.stringify(item.compPerson.Address.StateID));

		if (vm.action=="add") {
			vm.StateID = 0;
		} else {
			vm.StateID = item.compPerson.Address.StateID;
		}

		vm.init = init;
		function init() {
		    //console.info("item.action" + vm.action);
		    //console.info("comperss:" + JSON.stringify(vm.compPerson));
			UIControlService.loadLoadingModal(loadmsg);
			//Konfigurasi upload disamakan dengan yang ada di halaman pendaftaran

			UploadFileConfigService.getByPageName("PAGE.VENDOR.COMPANYPERSON", function (response) {
			    if (response.status == 200) {
			        //console.info("aa");
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
					AssessmentPrequalService.GetPositionTypes(function (response) {
						UIControlService.unloadLoadingModal();
						if (response.status == 200) {
						    vm.positionTypes = response.data;
						    console.info("poss:" + JSON.stringify(vm.positionTypes));
							if (vm.action === 'add') {
								loadCountries();
							} else if (vm.action === 'edit') {
							    console.info("edit");
								loadCountries(vm.compPerson.Address.State);
							}
							//loadCountries();getProvinsi();
						}
					}, function (err) {
						UIControlService.unloadLoadingModal();
					});
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
			});
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		/* begin provinsi, kabupaten, kecamatan */

		function loadCountries(data) {
		    //UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
		    console.info("ctry");
		    AssessmentPrequalService.getallnationalities(
                function (response) {
                	vm.countryList = response.data;
                	//console.info("negara"+JSON.stringify(vm.countryList));
                	if (vm.action === 'edit') {
                		for (var i = 0; i < vm.countryList.length; i++) {
                			if (vm.countryList[i].CountryID === data.CountryID) {
                				vm.selectedCountry = vm.countryList[i];
                				vm.countryCode = data.Country.Code;
                				//console.info("masuk");
                				changeCountry(false, data);
                			}
                		}
                	}
                }, function (response) {
                	UIControlService.unloadLoadingModal();
                	UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_STATES");;
                });
		}

		function getProvinsi() {
		    console.info("countryID" + vm.CountryID);
			UIControlService.loadLoadingModal(loadmsg);
			ProvinsiService.getStates(vm.CountryID, function (response) {
				UIControlService.unloadLoadingModal();
				vm.listProvinsi = response.data;
				getCities();
			}, function (response) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_STATES");;
			});
		}

		vm.changeProvince = changeProvince;
		function changeProvince() {
			vm.compPerson.Address.CityID = null;
			vm.listKabupaten = [];
			vm.compPerson.Address.DistrictID = null;
			vm.listKecamatan = [];
			getCities();
		}

		vm.changeCountry = changeCountry;
		function changeCountry(flag, data) {
			if (flag === true) {
				vm.countryCode = data.Code;
			}
			vm.CountryID = data.CountryID;
			getProvinsi();
		}

		function getCities() {
			if (vm.compPerson.Address.StateID) {
				UIControlService.loadLoadingModal(loadmsg);
				ProvinsiService.getCities(vm.compPerson.Address.StateID, function (response) {
					UIControlService.unloadLoadingModal();
					vm.listKabupaten = response.data;
					getDistrict();
				}, function (response) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_CITIES");
				});
			}
		}

		vm.validateAge = validateAge;
		function validateAge(inputDate) {
			//var convertedDate = moment(inputDate).format("DD-MM-YYYY");
			var validatedAge = false;
			var birthDate = moment(inputDate).format("DD-MM");;
			var dateNow = moment().format("DD-MM");
			var birthYear = moment(inputDate).format("YYYY");;
			var yearNow = moment().format("YYYY");
			var yearAge = yearNow - birthYear;
			if (yearAge > 17) {
				var validatedAge = true;
			} else if (yearAge === 17) {
				if (birthDate < dateNow || birthDate === dateNow) {
					var validatedAge = true;
				}
			}

			if (validatedAge === false) {
				UIControlService.msg_growl('error', "ERRORS.AGE_UNDER17");
				vm.compPerson.DateOfBirth = null;
			}

			console.info("umur" + JSON.stringify(yearAge));
			console.info("validasi" + JSON.stringify(validatedAge));
		}

		vm.changeCities = changeCities;
		function changeCities() {
			vm.compPerson.Address.DistrictID = null;
			vm.listKecamatan = [];
			getDistrict();
		}

		function getDistrict() {
			if (vm.compPerson.Address.CityID) {
				UIControlService.loadLoadingModal(loadmsg);
				ProvinsiService.getDistrict(vm.compPerson.Address.CityID, function (response) {
					UIControlService.unloadLoadingModal();
					vm.listKecamatan = response.data;
				}, function (response) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DISTRICT");
				});
			}
		}
		/* end provinsi, kabupaten, kecamatan */

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		}

		vm.fileSelect = fileSelect;
		function fileSelect(file) {
			vm.file = file;
		}

		vm.save = save;
		function save() {
			if (validateField() === false) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_INCOMPLETE_FIELD");
				return;
			}

			if (!vm.file && !vm.compPerson.IDUrl) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return;
			}
			if (vm.file) {
				uploadFile(vm.file);
			} else {
				saveCompPerson(vm.compPerson.IDUrl);
			}
		};

		function validateField() {
			console.info("validate" + JSON.stringify(vm.vendorLocation));
			if (vm.vendorLocation === 'ID') {
			    if (!vm.compPerson.PersonName || !vm.compPerson.DateOfBirth || !vm.compPerson.NoID || !vm.compPerson.ServiceStartDate || !vm.compPerson.Address.AddressInfo
                    || vm.compPerson.PersonName == "" || vm.compPerson.NoID == "" || vm.compPerson.Address.AddressInfo == "") {
					console.info("vendor lokal");
					return false;
				}
			} else if (vm.vendorLocation !== 'ID') {
			    if (!vm.compPerson.PersonName || !vm.compPerson.DateOfBirth || !vm.compPerson.ServiceStartDate || !vm.compPerson.Address.AddressInfo
                    || vm.compPerson.PersonName == "" || vm.compPerson.NoID == "" || vm.compPerson.Address.AddressInfo == "") {
					console.info("vendor internasional");
					return false;
				}
			}
			return true;
		};

		function uploadFile(file) {
			if (validateFileType(file, vm.idUploadConfigs)) {
				upload(file, vm.idFileSize, vm.idFileTypes);
			}
		}

		function validateFileType(file, idUploadConfigs) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		function upload(file, config, types) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}
			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UIControlService.loadLoadingModal(loadmsg);
			vm.IDUpload = vm.compPerson.ID + '_' + vm.compPerson.VendorEntryPrequalID;
			UploaderService.uploadCompanyPersonID(vm.IDUpload, file, size, types,
            function (reply) {
            	if (reply.status == 200) {
            		UIControlService.unloadLoadingModal();
            		var url = reply.data.Url;
            		saveCompPerson(url);
            	} else {
            		UIControlService.unloadLoadingModal();
            		UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
            	}
            }, function (err) {
            	UIControlService.unloadLoadingModal();
            	UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
            });
		};

		function saveCompPerson(url) {
			vm.compPerson.IDUrl = url;
			vm.compPerson.CompanyPosition;
			vm.compPerson.DateOfBirth = UIControlService.getStrDate(vm.compPerson.DateOfBirth);
			vm.compPerson.ServiceStartDate = UIControlService.getStrDate(vm.compPerson.ServiceStartDate);
			vm.compPerson.ServiceEndDate = UIControlService.getStrDate(vm.compPerson.ServiceEndDate);
			vm.compPerson.VendorEntryPrequalID = item.VendorEntryPrequalID;
			
			UIControlService.loadLoadingModal(loadmsg);

			if (vm.action === 'add') {
				AssessmentPrequalService.AddCompanyPersonPrequal(vm.compPerson, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					}
				}, function (error) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
				});
			} else {
				AssessmentPrequalService.UpdateCompanyPersonPrequal(vm.compPerson, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					}
				}, function (error) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
				});
			}
		};

		vm.close = close;
		function close() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();