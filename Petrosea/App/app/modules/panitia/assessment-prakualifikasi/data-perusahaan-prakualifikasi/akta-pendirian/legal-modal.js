﻿(function () {
    'use strict';

    angular.module("app").controller("formLegalCtrl", ctrl);

    ctrl.$inject = ['VerifikasiDataService', 'AssessmentPrequalService', '$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'AktaPendirianService', 'AktaPendirianPrequalService', 'CommonEngineService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'VendorRegistrationService'];
    /* @ngInject */
    function ctrl(VerifikasiDataService, AssessmentPrequalService, $http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, AktaPendirianService, AktaPendirianPrequalService, CommonEngineService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService) {

        var loadmsg = 'MESSAGE.LOADING';
        var vm = this;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.data = item.data;
        vm.currencyList = [];
        vm.stockUnits = [];
        vm.stockOption = [];
        vm.stock = {};
        vm.file;
        vm.isDobCalendarOpened = false;
        vm.nama = "NM_KOSONG";
        vm.tgl = "TGL_KOSONG";
        vm.identitas = "NO_KOSONG";

        vm.init = init;
        function init() {
            //console.info("data" + JSON.stringify(item));
            vm.data.DocumentDate = new Date(Date.parse(vm.data.DocumentDate));
            loadVendorContactPrequal();
        }

        vm.loadVendorContactPrequal = loadVendorContactPrequal;
        function loadVendorContactPrequal() {
            vm.iplusPrequal = 0;
            AssessmentPrequalService.loadContactPrequal({
                PrequalSetupStepID: vm.data.VendorEntryPrequal.PrequalSetupStepID,
                VendorPrequalID: vm.data.VendorEntryPrequal.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listContactPrequal = reply.data;
                    vm.listContactPrequal.forEach(function (contact) {
                        if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.VendorNamePrequal = contact.Vendor.VendorName;
                            vm.PemasokPrequal = contact.Vendor.SysReference1.Value;
                            vm.TipePrequal = contact.Vendor.SysReference.Value;
                            vm.CountryPrequal = contact.Contact.Address.State.Country.Code;
                            vm.businessName = contact.Vendor.Vendor.business.Name;
                            //vm.Associate = contact.Vendor.MstAssociation.AssosiasionName;
                            vm.PKPNumberPrequal = contact.Vendor.PKPNumber;
                            vm.PKPUrlPrequal = contact.Vendor.PKPUrl;
                            vm.PhonePrequal = contact.Contact.Phone;
                            vm.FaxPrequal = contact.Contact.Fax;
                            vm.EmailPrequal = contact.Contact.Email;
                            vm.WebsitePrequal = contact.Contact.Website;
                            vm.vendorAddress = contact.Contact.Address.AddressInfo + " , " + contact.Contact.Address.AddressDetail;
                        }
                    });
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });

            VerifikasiDataService.GetCities(function (reply) {
                if (reply.status === 200) {
                    vm.listKotaKab = reply.data;
                    for (var i = 0; i < vm.listKotaKab.length; i++) {
                        if (vm.data.NotaryLocation === vm.listKotaKab[i].CityID) {
                            vm.selectedNotaryLocation = vm.listKotaKab[i];
                        }
                    }
                } else {
                    UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD_CITIES');
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD_CITIES');
            });
            loadConfigAkta();
        };

        vm.loadConfigAkta = loadConfigAkta;
        function loadConfigAkta() {
            UploadFileConfigService.getByPageName("PAGE.VENDOR.LEGALDOCS", function (response) {
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
            });
        }


        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.openDobCalendar = openDobCalendar;
        function openDobCalendar() {
            vm.isDobCalendarOpened = true;
        }

        vm.onUnitChange = onUnitChange;
        function onUnitChange() {
            vm.showCurrencyField = false;
            for (var i = 0; i < vm.stockUnits.length; i++) {
                if (vm.stockUnits[i].RefID === vm.stock.UnitID) {
                    vm.showCurrencyField = vm.stockUnits[i].Name === 'STOCK_UNIT_CURRENCY';
                    break;
                }
            }
        }

        vm.labelChange = labelChange;
        function labelChange() {
            for (var i = 0; i < vm.stockOption.length; i++) {
                if (vm.stockOption[i].RefID === vm.stock.stockTypeID) {
                    vm.tes = vm.stockOption[i].Value;
                    break;
                }
            }
            if (vm.tes == 'STOCK_PERSONAL') {
                //PERSONAL
                vm.nama = "NM_PERSONAL";
                vm.tgl = "TGL_PERSONAL";
                vm.identitas = "NO_PERSONAL";
            } else if (vm.tes == 'STOCK_COMPANY') {
                //COMPANY
                vm.nama = "NM_COMPANY";
                vm.tgl = "TGL_COMPANY";
                vm.identitas = "NO_COMPANY";
            }
        }

        vm.validateAge = validateAge;
        function validateAge(inputDate) {
            if (vm.stock.stockTypeID == 4309) {
                var validatedAge = false;
                var birthDate = moment(inputDate).format("DD-MM");;
                var dateNow = moment().format("DD-MM");
                var birthYear = moment(inputDate).format("YYYY");;
                var yearNow = moment().format("YYYY");
                var yearAge = yearNow - birthYear;
                if (yearAge > 17) {
                    var validatedAge = true;
                } else if (yearAge === 17) {
                    if (birthDate < dateNow || birthDate === dateNow) {
                        var validatedAge = true;
                    }
                }

                if (validatedAge === false) {
                    UIControlService.msg_growl('error', "ERRORS.AGE_UNDER17");
                    vm.stock.OwnerDOBDate = null;
                }
            }
        }


        vm.save = save;
        function save() {
            UIControlService.loadLoadingModal(loadmsg);
            console.info(vm.stock);
            AssessmentPrequalService.isAnotherStockHolder({
                VendorEntryPrequalID: vm.stock.VendorEntryPrequalID,
                OwnerID: vm.stock.OwnerID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200 && reply.data === true) {
                    UIControlService.msg_growl('error', "ERRORS.IS_ANOTHER_STOCKHOLDER");
                } else {
                    uploadAndSave();
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_VERIFY_STOCKHOLDER");
                uploadAndSave();
            });
        };

        function uploadAndSave() {
            if (!vm.file && !vm.stock.OwnerURL) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return;
            }
            if (vm.file) {
                uploadFile(vm.file);
            } else {
                saveVendorStock(vm.stock.OwnerURL);
            }
        }


        vm.savedata = savedata;
        function savedata() {
            if (!(vm.fileUpload === undefined)) {
                    uploadFileLegal(vm.fileUpload);
                } else {
                    saveprocess();
                }
        }

        function uploadFileLegal(file) {
            if (validateFileType(file, vm.idUploadConfigs)) {
                upload(file, vm.idFileSize, vm.idFileTypes);
            }
        }
        function validateFileType(file, idUploadConfigs) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload(file, config, types) {

            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            UIControlService.loadLoadingModal(loadmsg);
            UploaderService.uploadSingleFileLegalDocuments(vm.data.VendorEntryPrequalID, file, size, filters, function (response) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    var url = reply.data.Url;
                    vm.data.DocumentURL = url;
                    save(url);
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
            });
        };

        function save(url) {
            UIControlService.loadLoadingModal(loadmsg);
            AssessmentPrequalService.UpdateLegal(vm.data, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE_VSTOCK");
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_VSTOCK");
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_VSTOCK");
            });
        };

        vm.close = close;
        function close() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();