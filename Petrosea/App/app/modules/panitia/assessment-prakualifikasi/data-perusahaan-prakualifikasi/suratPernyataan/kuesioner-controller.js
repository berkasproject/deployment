(function () {
  'use strict';

  angular.module("app").controller("KuesionerCtrl", ctrl);

  ctrl.$inject = ['$window', '$filter', '$stateParams', '$timeout', '$uibModal', '$http', '$translate', '$translatePartialLoader', '$location', '$state', 'VerifiedSendService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'VendorRegistrationService', 'AssessmentPrequalService'];
  /* @ngInject */
  function ctrl($window, $filter, $stateParams, $timeout, $uibModal, $http, $translate, $translatePartialLoader, $location, $state, VerifiedSendService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService, AssessmentPrequalService) {
      var endpoint = GlobalConstantService.getConstant("admin_endpoint");
      var vm = this;
      vm.PrequalStepID = Number($stateParams.PrequalStepID);
      vm.PrequalStepAssID = Number($stateParams.PrequalStepAssID);
      vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
      vm.VEPID = Number($stateParams.VEPID);
      vm.tglSekarang = new Date();
      vm.isApprovedCR = false;
      vm.CR = 0;
      vm.initialize = initialize;
      function initialize(printableArea, button, divInfo, divInfo1) {
          console.info("prequalstepid" + vm.PrequalStepID);
          console.info("vpid" + vm.VendorPrequalID);
          localStorage.removeItem('vendor_reg_id');
          vm.CR = localStorage.getItem('InfoKuesioner');
          localStorage.removeItem('InfoKuesioner');
          vm.printableArea = printableArea;
          vm.button = button;
          vm.divInfo = divInfo;
          vm.divInfo1 = divInfo1;
          vm.currentLang = $translate.use();
          $translatePartialLoader.addPart('daftar');
          loadVendor();
          loadCek(printableArea, button, divInfo, divInfo1);
          localStorage.setItem('vendor_reg_id', vm.VendorPrequalID);
          loadPrequalStep();
      }

      vm.loadPrequalStep = loadPrequalStep;
      function loadPrequalStep() {
          AssessmentPrequalService.SelectSetupStep({
              Status: vm.PrequalStepAssID,
              column: vm.VendorPrequalID
          }, function (reply) {
              UIControlService.unloadLoading();
              if (reply.status === 200) {
                  vm.isEntry = reply.data.IsNotStarted;
                  vm.IsNeedRevision = reply.data.IsNeedRevision;
              }
          }, function (err) {
              UIControlService.unloadLoading();
          });
      }


      vm.loadVendor = loadVendor();
      function loadVendor() {
          AssessmentPrequalService.loadContactPrequal({
              PrequalSetupStepID: vm.PrequalStepID,
              VendorPrequalID: vm.VendorPrequalID
          }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        vm.vendor = reply.data;
                        for (var i = 0; i < vm.vendor.length; i++) {
                            if (vm.vendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN') {
                                vm.address = vm.vendor[i].Contact.Address.AddressInfo + ' '+ vm.vendor[i].Contact.Address.AddressDetail;
                                vm.VendorName = vm.vendor[i].Vendor.VendorName;
                                vm.npwp = vm.vendor[i].Vendor.Npwp;
                            }
                            else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                                vm.email = vm.vendor[i].Contact.Email;
                                vm.username = vm.vendor[i].Vendor.user.Username;
                                vm.VendorName = vm.vendor[i].Vendor.VendorName;
                            }
                            else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {

                                if (vm.telp == undefined) {
                                    vm.name = vm.vendor[i].Contact.Name;
                                    vm.telp = vm.vendor[i].Contact.Phone;
                                    vm.email = vm.vendor[i].Contact.Email;
                                }
                            }
                        }
                    } else {
                        $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    console.info("error:" + JSON.stringify(err));
                    //$.growl.error({ message: "Gagal Akses API >" + err });
                    UIControlService.unloadLoading();
                });
      }

      vm.loadCek = loadCek;
      function loadCek(printableArea, button, divInfo, divInfo1) {
          AssessmentPrequalService.CekVendorPrequal({ Status: vm.PrequalStepID, FilterType: vm.VendorPrequalID }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        vm.vendorQues = reply.data;
                        vm.jload(true, printableArea, button, divInfo, divInfo1);
                    } else {
                        $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    console.info("error:" + JSON.stringify(err));
                    //$.growl.error({ message: "Gagal Akses API >" + err });
                    UIControlService.unloadLoading();
                });
      }

      vm.jload = jload;
      function jload(dataflag, printableArea, button, divInfo, divInfo1) {
          vm.button = button;
          vm.divInfo = divInfo;
          vm.divInfo1 = divInfo1;

          UIControlService.loadLoading("LOADERS.LOADING");
          AssessmentPrequalService.selectQuestionnairePrequal({ FilterType: vm.VendorPrequalID, Status: vm.PrequalStepID, Keyword: vm.currentLang},
               function (reply) {
                   vm.data = [];
                   vm.type = [];
                   UIControlService.unloadLoading();
                   if (reply.status === 200) {
                       vm.list = reply.data;
                       if (vm.vendorQues.length !== 0) {
                           vm.flag = 1;
                           for (var i = 0; i < vm.vendorQues.length; i++) {
                               for (var j = 0; j < vm.list[i].type.length; j++) {
                                   var calldateType = {
                                       ID: vm.list[i].type[j].ID,
                                       question: vm.list[i].type[j].question,
                                       DetailAnswer: vm.list[i].type[j].DetailAnswer
                                   }
                                   vm.type.push(calldateType);
                               }
                               vm.calldata = {
                                   ID: vm.list[i].ID,
                                   question: vm.list[i].question,
                                   DetailId: vm.list[i].DetailId,
                                   AnswerName: vm.list[i].AnswerName,
                                   Value: vm.vendorQues[i].VendQuesDetailId,
                                   Description: vm.vendorQues[i].Description,
                                   type: vm.type
                               }
                               vm.data.push(vm.calldata);
                               vm.type = []; 
                               if (i == vm.vendorQues.length - 1) {
                                   var reenterButton = document.getElementById(vm.button);
                                   var divInfo = document.getElementById(vm.divInfo);
                                   var divInfo1 = document.getElementById(vm.divInfo1);
                                   //divInfo1.style.visibility = 'visible';
                                   // reenterButton.style.visibility = 'hidden';
                                   //divInfo.style.visibility = 'visible';
                                   $timeout(function () {
                                       if(dataflag == false) window.print();
                                       vm.flag = 2;
                                   }, 3000);

                                   reenterButton.style.visibility = 'visible';
                                   divInfo.style.visibility = 'hidden';
                                   divInfo1.style.visibility = 'hidden';
                               }
                           }
                       }
                       else {
                           var divInfo = document.getElementById(vm.divInfo);
                           divInfo.style.visibility = 'hidden';
                           var divInfo1 = document.getElementById(vm.divInfo1);
                           divInfo1.style.visibility = 'hidden';

                           vm.data = vm.list;
                       }
                   } else {
                       $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                       UIControlService.unloadLoading();
                   }
               }, function (err) {
                   console.info("error:" + JSON.stringify(err));
                   //$.growl.error({ message: "Gagal Akses API >" + err });
                   UIControlService.unloadLoading();
               });
      }

      vm.downloadQuestionnaire = downloadQuestionnaire;
      function downloadQuestionnaire() {
          var questionaire = [];
          for (var i = 0; i < vm.data.length; i++) {
              var data = {
                  Value: vm.data[i].Value,
                  VendorPrequalID: vm.VendorPrequalID,
                  Description: vm.data[i].Description,
                  PrequalStepID: vm.PrequalStepID
              }
              questionaire.push(data);
          }
          var headers = {};
          headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

          $http({
              method: 'POST',
              url: endpoint + '/AssessmentPrequal/generateQuestionnaire',
              headers: headers,
              data: questionaire,
              responseType: 'arraybuffer'
          }).success(function (data, status, headers) {
              var linkElement = document.createElement('a');
              var fileName = "Initial Risk Assessment Questionnaire " + vm.VendorName + ".pdf";

              try {
                  var blob = new Blob([data], { type: headers('content-type') });
                  var url = window.URL.createObjectURL(blob);
                  linkElement.setAttribute('href', url);
                  linkElement.setAttribute('download', fileName);

                  var clickEvent = new MouseEvent("click", {
                      "view": window,
                      "bubbles": true,
                      "cancelable": false
                  });

                  linkElement.dispatchEvent(clickEvent);
              } catch (e) {
                  console.log(e);
              }
          });
          //var a = document.createElement("a");
          //document.body.appendChild(a);

          //PrequalCertificateService.GenerateCertificate({
          //	PrequalSetupStepID: vm.stepId
          //}, function (reply) {
          //	if (reply.status === 200) {

          //		var octetStreamMime = 'application/octet-stream';
          //		var success = false;
          //		var filename = 'download.docx';
          //		var contentType = octetStreamMime;
          //		var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;

          //		if (urlCreator) {
          //			var link = document.createElement('a');
          //			if ('download' in link) {
          //				try {
          //					// Prepare a blob URL
          //					console.log("Trying download link method with simulated click ...");
          //					var blob = new Blob([reply.data], { type: contentType });
          //					var url = urlCreator.createObjectURL(blob);
          //					link.setAttribute('href', url);

          //					// Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
          //					link.setAttribute("download", filename);

          //					// Simulate clicking the download link
          //					var event = document.createEvent('MouseEvents');
          //					event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
          //					link.dispatchEvent(event);
          //					console.log("Download link method with simulated click succeeded");
          //					success = true;

          //				} catch (ex) {
          //					console.log("Download link method with simulated click failed with the following exception:");
          //					console.log(ex);
          //				}
          //			}
          //		}
          //		//var file = new Blob([reply.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
          //		//var file = new File([reply.data], fileName, { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
          //		//var fileURL = window.URL.createObjectURL(file);
          //		////window.open(fileURL);
          //		//a.href = fileURL;
          //		//a.download = fileName;
          //		//a.click();
          //	}
          //}, function (error) {
          //	UIControlService.unloadLoading();
          //});

          //var innerContents = document.getElementById(formCertificate).innerHTML;
          //var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
          //popupWindow.document.open();
          //popupWindow.document.write('<html><head><title>Prequal Cerificate</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
          //popupWindow.document.close();
      }

      vm.printDiv = printDiv;
      vm.FlagValue = 0;
      function printDiv(areaID, button, divInfo, divInfo1) {
          for (var i = 0; i < vm.data.length; i++) {
              if (vm.data[i].Value == 0) {
                  vm.FlagValue = 1;
                  UIControlService.msg_growl('error', "ERRORS.VALUE_NOT_FOUND");

              }
              if (vm.FlagValue === 0 && i === (vm.data.length - 1)) {
                  bootbox.confirm($filter('translate')('SURE_QUEST'), function (yes) {
                      if (yes) {
                          UIControlService.loadLoading("Loading. . .");
                          vm.save();
                      }
                  });

              }
          }
          vm.FlagValue = 0;
      }

      vm.save = save;
      function save() {
          UIControlService.loadLoading("Loading . . .");
          var questionaire = [];
          for (var i = 0; i < vm.data.length; i++) {
              var data = {
                  Value: vm.data[i].Value,
                  VendorPrequalID: vm.VendorPrequalID,
                  Description: vm.data[i].Description,
                  PrequalStepID: vm.PrequalStepID
              }
              questionaire.push(data);
          }
         // UIControlService.loadLoading("LOADERS.LOADING_SAVE_QUESTIONAIRE");
          AssessmentPrequalService.saveQuestionairePrequal(questionaire,
                function (response) {
                    UIControlService.unloadLoading();
                    localStorage.setItem('InfoKuesioner', 1);
                    window.location.reload();
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                    //$state.go('login-rekanan');
                });
      }

      vm.back = back;
      function back() {
          localStorage.setItem('currLang', vm.currentLang);
          $state.go('surat-pernyataan-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID, VEPID: vm.VEPID });
      }

      vm.printForm = printForm;
      function printForm(form) {
          var innerContents = document.getElementById(form).innerHTML;
          var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
          popupWindow.document.open();
          popupWindow.document.write('<html><head><title>Kuesioner-'+ vm.VendorName+ '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
          popupWindow.document.close();

          //$uibModalInstance.close();
      }


  }
})();

