﻿(function () {
    'use strict';

    angular.module("app").controller("SuratPernyataanPrequalAssController", ctrl);

    ctrl.$inject = ['$http', '$window', '$translate', '$stateParams', '$state', '$uibModal', '$translatePartialLoader', 'VerifiedSendService', 'SrtPernyataanPrequalService', 'UIControlService', 'GlobalConstantService', 'VendorRegistrationService', 'AssessmentPrequalService'];
    function ctrl($http, $window, $translate, $stateParams, $state, $uibModal, $translatePartialLoader, VerifiedSendService, SrtPernyataanPrequalService, UIControlService, GlobalConstantService, VendorRegistrationService, AssessmentPrequalService) {
        var vm = this;
        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        vm.currentLang = $translate.use();
        $translatePartialLoader.addPart('daftar');
        vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
        vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
        vm.VEPID = Number($stateParams.VEPID);
        vm.tglSekarang = new Date();
        vm.data = [];
        vm.pos;
        vm.cek;
        vm.DocUrlAgreement;
        vm.DocUrlBConduct;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.DocType = null;
        vm.isApprovedCR = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('surat-pernyataan');
            $translatePartialLoader.addPart('assessment-prakualifikasi');
            loadDDquest();
            if (localStorage.getItem("currLang") === 'id') {
                vm.DocType = 4225;
            } else {
                vm.DocType = 4232;
            }
                UIControlService.loadLoading("MESSAGE.LOADING");
                AssessmentPrequalService.SelectSetupStep({
                    Status: vm.PrequalStepAssID,
                    column: vm.VendorPrequalID,
                    Offset: vm.VEPID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        vm.isEntry = reply.data.IsNotStarted;
                        vm.PrequalStepID = reply.data.PrequalStepID;
                        vm.PrequalSetupID = reply.data.PrequalSetupID;
                        vm.IsNeedRevision = reply.data.IsNeedRevision;
                        vm.remark = reply.data.Remark;
                        loadUrlLibrary();
                        loadUrlLibraryAgree(1);
                        loadUrlKuesioner();
                        loadData();
                        //forKuesioner
                        loadVendor();
                        loadCek();
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                });

           
            
        }

        vm.loadData = loadData;
        function loadData() {
            AssessmentPrequalService.loadST({ Status: vm.PrequalStepID, FilterType : vm.VendorPrequalID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.data = reply.data;
                    loadUrlAgreement(1);
                    loadVendorContact();
                    loadDataVendorAgreement();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.loadDDquest= loadDDquest;
        function loadDDquest() {
            AssessmentPrequalService.loadDDQuest({ column: vm.VendorPrequalID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.ddquest = reply.data;
                    //console.info("ddquest" + JSON.stringify(vm.ddquest));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }


        vm.ddkuesioner = ddkuesioner;
        function ddkuesioner() {
            var item = {
                data: vm.ddquest
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/lihat-kuesioner.html',
                controller: 'LihatKuesionerAssPrequalCtrl',
                controllerAs: 'LihatKuesionerAssPrequalCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                init();
            });
        }


        vm.loadVendor = loadVendor;
        function loadVendor() {
            AssessmentPrequalService.loadContactPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendor = reply.data;
                    for (var i = 0; i < vm.vendor.length; i++) {
                        if (vm.vendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN') {
                            if (vm.vendor[i].Contact.Address.StateID != null) {
                                if (vm.vendor[i].Contact.Address.CityID != null) {
                                    if (vm.vendor[i].Contact.Address.DistrictID != null) vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.Distric.Name + ", " + vm.vendor[i].Contact.Address.City.Name + ", " + vm.vendor[i].Contact.Address.State.Name;
                                    else vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.City.Name + ", " + vm.vendor[i].Contact.Address.State.Name;
                                }
                                else vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.State.Name;
                            }
                            else vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.State.Name;
                            vm.VendorName = vm.vendor[i].Vendor.VendorName;
                            vm.npwp = vm.vendor[i].Vendor.Npwp;
                        }
                        else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            vm.email = vm.vendor[i].Contact.Email;
                            vm.username = vm.vendor[i].Vendor.user.Username;
                            vm.VendorName = vm.vendor[i].Vendor.VendorName;
                        }
                        else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {

                            if (vm.telp == undefined) {
                                vm.name = vm.vendor[i].Contact.Name;
                                vm.telp = vm.vendor[i].Contact.Phone;
                            }
                        }
                    }
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCek = loadCek;
        function loadCek() {
            AssessmentPrequalService.CekVendorPrequal({
                Status: vm.PrequalStepID,
                FilterType: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorQues = reply.data;
                    jloadKuesioner();
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.jloadKuesioner = jloadKuesioner;
        function jloadKuesioner() {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectQuestionnairePrequal({ 
                Status: vm.PrequalStepID,
                FilterType: vm.VendorPrequalID, 
                Keyword: vm.currentLang
            },
               function (reply) {
                   vm.data = [];
                   vm.type = [];
                   UIControlService.unloadLoading();
                   if (reply.status === 200) {
                       vm.list = reply.data;
                       if (vm.vendorQues.length !== 0) {
                           vm.flag = 1;
                           for (var i = 0; i < vm.vendorQues.length; i++) {
                               for (var j = 0; j < vm.list[i].type.length; j++) {
                                   var calldateType = {
                                       ID: vm.list[i].type[j].ID,
                                       question: vm.list[i].type[j].question,
                                       DetailAnswer: vm.list[i].type[j].DetailAnswer
                                   }
                                   vm.type.push(calldateType);
                               }
                               vm.calldata = {
                                   ID: vm.list[i].ID,
                                   question: vm.list[i].question,
                                   DetailId: vm.list[i].DetailId,
                                   AnswerName: vm.list[i].AnswerName,
                                   Value: vm.vendorQues[i].VendQuesDetailId,
                                   Description: vm.vendorQues[i].Description,
                                   type: vm.type
                               }
                               vm.data.push(vm.calldata);
                               vm.type = [];
                           }
                       }
                       else {
                           vm.data = vm.list;
                       }
                   }
               }, function (err) {
               });
        }

        vm.downloadQuestionnaire = downloadQuestionnaire;
        function downloadQuestionnaire() {
            var questionaire = [];
            for (var i = 0; i < vm.data.length; i++) {
                var data = {
                    Value: vm.data[i].Value,
                    VendorPrequalID: vm.VendorPrequalID,
                    Description: vm.data[i].Description,
                    PrequalStepID: vm.PrequalStepID
                }
                questionaire.push(data);
            }
            var headers = {};
            headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

            $http({
                method: 'POST',
                url: endpoint + '/AssessmentPrequal/generateQuestionnaire',
                headers: headers,
                data: questionaire,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                var linkElement = document.createElement('a');
                var fileName = "Initial Risk Assessment Questionnaire " + vm.VendorName + ".pdf";

                try {
                    var blob = new Blob([data], { type: headers('content-type') });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute('download', fileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);
                } catch (e) {
                    console.log(e);
                }
            });
            //var a = document.createElement("a");
            //document.body.appendChild(a);

            //PrequalCertificateService.GenerateCertificate({
            //	PrequalSetupStepID: vm.stepId
            //}, function (reply) {
            //	if (reply.status === 200) {

            //		var octetStreamMime = 'application/octet-stream';
            //		var success = false;
            //		var filename = 'download.docx';
            //		var contentType = octetStreamMime;
            //		var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;

            //		if (urlCreator) {
            //			var link = document.createElement('a');
            //			if ('download' in link) {
            //				try {
            //					// Prepare a blob URL
            //					console.log("Trying download link method with simulated click ...");
            //					var blob = new Blob([reply.data], { type: contentType });
            //					var url = urlCreator.createObjectURL(blob);
            //					link.setAttribute('href', url);

            //					// Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
            //					link.setAttribute("download", filename);

            //					// Simulate clicking the download link
            //					var event = document.createEvent('MouseEvents');
            //					event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
            //					link.dispatchEvent(event);
            //					console.log("Download link method with simulated click succeeded");
            //					success = true;

            //				} catch (ex) {
            //					console.log("Download link method with simulated click failed with the following exception:");
            //					console.log(ex);
            //				}
            //			}
            //		}
            //		//var file = new Blob([reply.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
            //		//var file = new File([reply.data], fileName, { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
            //		//var fileURL = window.URL.createObjectURL(file);
            //		////window.open(fileURL);
            //		//a.href = fileURL;
            //		//a.download = fileName;
            //		//a.click();
            //	}
            //}, function (error) {
            //	UIControlService.unloadLoading();
            //});

            //var innerContents = document.getElementById(formCertificate).innerHTML;
            //var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            //popupWindow.document.open();
            //popupWindow.document.write('<html><head><title>Prequal Cerificate</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            //popupWindow.document.close();
        }

        //ambil VendorID
        vm.loadVerifiedVendor = loadVerifiedVendor;
        function loadVerifiedVendor() {
            VerifiedSendService.selectVerifikasi(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.verified = reply.data;
                    vm.cekTemporary = vm.verified.IsTemporary;
                    vm.VendorID = vm.verified.VendorID;
                    vm.VendorName = vm.verified.VendorName;
                    jLoad(1);
                    loadUrlAgreement(1);
                    loadVendorContact();
                    loadDataVendorAgreement();
                    loadVendor();
                    //loadCek();
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.printForm = printForm;
        function printForm(form) {
            var innerContents = document.getElementById(form).innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Kuesioner-' + vm.VendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();
        }

        vm.downloadQuestionnaire = downloadQuestionnaire;
        function downloadQuestionnaire() {
            console.info("download");
            var questionaire = [];
            for (var i = 0; i < vm.data.length; i++) {
                var data = {
                    Value: vm.data[i].Value,
                    VendorPrequalID: vm.VendorPrequalID,
                    Description: vm.data[i].Description,
                    PrequalStepID: vm.PrequalStepID
                }
                questionaire.push(data);
            }
            var headers = {};
            headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

            $http({
                method: 'POST',
                url: endpoint + '/AssessmentPrequal/generateQuestionnaire',
                headers: headers,
                data: questionaire,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                var linkElement = document.createElement('a');
                var fileName = "Initial Risk Assessment Questionnaire " + vm.VendorName + ".pdf";

                try {
                    var blob = new Blob([data], { type: headers('content-type') });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute('download', fileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);
                } catch (e) {
                    console.log(e);
                }
            });
            //var a = document.createElement("a");
            //document.body.appendChild(a);

            //PrequalCertificateService.GenerateCertificate({
            //	PrequalSetupStepID: vm.stepId
            //}, function (reply) {
            //	if (reply.status === 200) {

            //		var octetStreamMime = 'application/octet-stream';
            //		var success = false;
            //		var filename = 'download.docx';
            //		var contentType = octetStreamMime;
            //		var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;

            //		if (urlCreator) {
            //			var link = document.createElement('a');
            //			if ('download' in link) {
            //				try {
            //					// Prepare a blob URL
            //					console.log("Trying download link method with simulated click ...");
            //					var blob = new Blob([reply.data], { type: contentType });
            //					var url = urlCreator.createObjectURL(blob);
            //					link.setAttribute('href', url);

            //					// Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
            //					link.setAttribute("download", filename);

            //					// Simulate clicking the download link
            //					var event = document.createEvent('MouseEvents');
            //					event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
            //					link.dispatchEvent(event);
            //					console.log("Download link method with simulated click succeeded");
            //					success = true;

            //				} catch (ex) {
            //					console.log("Download link method with simulated click failed with the following exception:");
            //					console.log(ex);
            //				}
            //			}
            //		}
            //		//var file = new Blob([reply.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
            //		//var file = new File([reply.data], fileName, { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
            //		//var fileURL = window.URL.createObjectURL(file);
            //		////window.open(fileURL);
            //		//a.href = fileURL;
            //		//a.download = fileName;
            //		//a.click();
            //	}
            //}, function (error) {
            //	UIControlService.unloadLoading();
            //});

            //var innerContents = document.getElementById(formCertificate).innerHTML;
            //var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            //popupWindow.document.open();
            //popupWindow.document.write('<html><head><title>Prequal Cerificate</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            //popupWindow.document.close();
        }

        vm.loadDataVendorAgreement = loadDataVendorAgreement;
        function loadDataVendorAgreement() {
            AssessmentPrequalService.selectData({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID:vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listData = reply.data;
                    vm.IsBConductUploaded = false; vm.IsAgreementLetterUploaded = false;
                    for (var i = 0; i < vm.listData.length; i++) {
                        if (vm.listData[i].DocType === 4232 || vm.listData[i].DocType === 4225) {
                            if (vm.listData[i].DocumentUrl != null) {
                                if (vm.listData[i].DocumentUrl != "") {
                                    vm.IsBConductUploaded = true;
                                    vm.urlBConduct = vm.listData[i].DocumentUrl;
                                }
                            }
                        }
                        if (vm.listData[i].DocType === 4226) {
                            if (vm.listData[i].DocumentUrl != null) {
                                if (vm.listData[i].DocumentUrl != "") {
                                    vm.IsAgreementLetterUploaded = true;
                                    vm.urlAgreementLetter = vm.listData[i].DocumentUrl;
                                }
                            }
                        }
                    }
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorContact = loadVendorContact;
        function loadVendorContact(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.loadContactPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID:vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.ContactID = data[0].ContactID;
                    getAddressID(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.getAddressID = getAddressID;
        function getAddressID(current) {
            AssessmentPrequalService.selectAddressID({
                ContactID: vm.ContactID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.AddressID = data[0].AddressID;
                    loadAddress(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadAddress = loadAddress;
        function loadAddress(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectAddress({
                AddressID: vm.AddressID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Address = data[0].AddressDetail;
                    vm.StateID = data[0].StateID;
                    loadCountryID(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountryID = loadCountryID;
        function loadCountryID(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectCountryID({
                StateID: vm.StateID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.CountryID = data[0].CountryID;
                    vm.StateName = data[0].Name;
                    loadCountry(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountry = loadCountry;
        function loadCountry(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectCountry({
                CountryID: vm.CountryID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Country = data[0].Name;
                    if (vm.Country === 'Indonesia') {
                        vm.cek = true;
                    } else {
                        vm.cek = false;
                    }

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.All({
                VendorId: vm.VendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.data = data;

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlLibrary = loadUrlLibrary;
        function loadUrlLibrary() {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectDocLibrary({
                DocType: vm.DocType
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.DocUrlBConduct = data[0].DocUrl;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlLibraryAgree = loadUrlLibraryAgree;
        function loadUrlLibraryAgree(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectDocLibrary({
                DocType: 4226
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.DocUrlAgreement = data[0].DocUrl;

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlAgreement = loadUrlAgreement;
        function loadUrlAgreement(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectUrlAgree({
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlBConduct = loadUrlBConduct;
        function loadUrlBConduct(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectUrlBConduct({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.DOcUrlBConduct = data[0].AddressID;

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlBConductEN = loadUrlBConductEN;
        function loadUrlBConductEN(current) {
            UIControlService.loadLoading("LOADERS.LOADING");
            AssessmentPrequalService.selectBConductEN({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {;
                UIControlService.unloadLoading();
            });
        }

        vm.baca = baca;
        function baca() {
            var data = {
                DocType: vm.DocType,
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID,
                IsNeedRevision:vm.IsNeedRevision
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/suratPernyataan.modal.html',
                controller: 'SuratPernyataanModalAssCtrl',
                controllerAs: 'SrtPernyataanModalAssCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
        };

        vm.baca2 = baca2;
        function baca2() {
            var data = {
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID,
                IsNeedRevision: vm.IsNeedRevision
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/suratPernyataan.modalBaca.html',
                controller: 'SuratPernyataanModalBacaAssCtrl',
                controllerAs: 'SrtPernyataanModalBacaAssCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
        };

        vm.upload = upload;
        function upload(DocType) {
            var data = {
                DocType: DocType,
                act: 1,
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/suratPernyataan.modalUpload.html',
                controller: 'SuratPernyataanModalUploadAssCtrl',
                controllerAs: 'SrtPernyataanModalUpAssCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        };

        vm.upload2 = upload2;
        function upload2(data) {
            var data = {
                DocType: data,
                act: 0,
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID

            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/suratPernyataan.modalUpload.html',
                controller: 'SuratPernyataanModalUploadAssCtrl',
                controllerAs: 'SrtPernyataanModalUpAssCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        };

        vm.editKuesioner = editKuesioner;
        function editKuesioner() {
            $state.go('kuesioner-vendor-prequal-assessment', { PrequalStepAssID : vm.PrequalStepAssID, PrequalStepID: vm.PrequalStepID, VendorPrequalID : vm.VendorPrequalID, VEPID : vm.VEPID });
        }

        vm.back = back;
        function back() {
            $state.go('assessment-prakualifikasi-detail', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
        }

        vm.Submit = Submit;
        function Submit() {
            if (vm.DocUrlBConduct == null) {
                UIControlService.msg_growl("error", 'ERR_BCONDUCT');
                return;
            }
            if (vm.cek == true) {
                if(vm.DocUrlAgreement == null){
                    UIControlService.msg_growl("error", 'ERR_SURPEN');
                    return;
                }
            }
            if (vm.UrlKuesioner == null) {
                UIControlService.msg_growl("error", 'ERR_QUESTIONNAIRE');
                return;
            }
            if (vm.DocUrlBConduct !== null || (vm.cek == true && vm.DocUrlAgreement !== null) || vm.UrlKuesioner !== null) {
                AssessmentPrequalService.Submit({ Status: vm.PrequalStepID,  }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "Success Submit");
                        $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
            
        }

        vm.uploadKuesioner = uploadKuesioner;
        function uploadKuesioner() {
            var data = {
                PrequalStepID: vm.PrequalSetupID,
                VendorPrequalID: vm.VendorPrequalID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/formUploadQuestionnaire.html',
                controller: 'FormQuestionnaireAssCtrl',
                controllerAs: 'FormQuestionnaireAssCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        };

        vm.loadUrlKuesioner = loadUrlKuesioner;
        function loadUrlKuesioner() {
            AssessmentPrequalService.selectUrlKuesioner({VendorPrequalID:vm.VendorPrequalID}, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    //console.info("quest:" + JSON.stringify(data));
                    vm.UrlKuesioner = data.QuestionnaireUrl;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.valid = true;
        vm.notvalid = function notvalid() {
            vm.valid = false;
        }


        vm.Submit = Submit;
        function Submit(flag) {
            vm.valid = flag;
            if (vm.remark == undefined) {
                vm.remark = "";
            }
            AssessmentPrequalService.InsertSubmit({
                PrequalStepID: vm.PrequalStepAssID,
                VendorPrequalID: vm.VendorPrequalID,
                IsAgree: flag,
                VEPID: vm.VEPID,
                Remark: vm.remark
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

    }
})();