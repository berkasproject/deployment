﻿(function () {
	'use strict';

	angular.module("app").controller("SuratPernyataanModalUploadAssCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$stateParams', '$location', 'SocketService', 'VerifiedSendService', 'SrtPernyataanPrequalService',
        '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService', 'AssessmentPrequalService'];
	function ctrl($http, $translate, $translatePartialLoader, $stateParams, $location, SocketService, VerifiedSendService, SrtPernyataanPrequalService,
        $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService, AssessmentPrequalService) {
		var vm = this;

		vm.cek2 = item.act;
		vm.VendorID;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.action = "";
		vm.pathFile;
		vm.Description;
		vm.fileUpload;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;
		vm.idFileTypes;
		vm.idFileSize;
		vm.idUploadConfigs;
		vm.DocUrl;
		vm.tglSekarang = UIControlService.getDateNow2("-");
		vm.DocType = item.DocType;
		vm.ID;
		vm.PrequalStepID = item.PrequalStepID;
		vm.VendorPrequalID = item.VendorPrequalID;
		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('surat-pernyataan');
			UploadFileConfigService.getByPageName("PAGE.VENDOR.SURATPERNYATAAN", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", "ERRORS.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "NOTIF.API");
				UIControlService.unloadLoading();
				return;
			});
		};

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.selectUpload = selectUpload;
		vm.fileUpload;
		function selectUpload() {
			//console.info(vm.fileUpload);
		}

		/*start upload */
		vm.uploadFile = uploadFile;
		function uploadFile() {
			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.VendorPrequalID, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
			}
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "ERRORS.NO_FILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(id, file, config, filters, callback) {

		    //console.info(id);
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }
		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    if (vm.cek2 === 1) {
		        UIControlService.loadLoading("NOTIF.LOADING_UPLOAD_FILE");
		        UploaderService.uploadSingleFileBusinessConduct(id, file, size, filters, function (response) {
		            UIControlService.unloadLoading();
		            //console.info("response:" + JSON.stringify(response));
		            if (response.status == 200) {
		                //console.info(response);
		                var url = response.data.Url;
		                vm.pathFile = url;
		                vm.name = response.data.FileName;
		                var s = response.data.FileLength;
		                vm.DocUrl = vm.pathFile;
		                //console.info(vm.DocUrl);
		                if (vm.flag == 0) {
		                    vm.size = Math.floor(s);
		                    //console.info(vm.size);
		                }
		                if (vm.flag == 1) {
		                    vm.size = Math.floor(s / (1024));
		                }
		                AssessmentPrequalService.insertDocST({
		                    VendorPrequalID: vm.VendorPrequalID,
		                    DocType: vm.DocType,
		                    DocumentUrl: vm.DocUrl,
		                    UploadDate: vm.tglSekarang,
		                    IsActive: 1,
		                    PrequalStepID: vm.PrequalStepID

		                }, function (reply) {
		                    //console.info("reply" + JSON.stringify(reply))
		                    UIControlService.unloadLoadingModal();
		                    if (reply.status === 200) {
		                        UIControlService.msg_growl("success", "NOTIF.SUC_UPLOAD");
		                        $uibModalInstance.close();
		                    } else {
		                        UIControlService.msg_growl("error", "NOTIF.FAIL_UPLOAD");
		                        return;
		                    }
		                }, function (err) {
		                    console.info(err);
		                    UIControlService.msg_growl("error", "NOTIF.API");
		                    UIControlService.unloadLoadingModal();
		                });
		            } else {
		                UIControlService.msg_growl("error", "error");
		                return;
		            }
		        }, function (response) {
		            console.info(response);
		            UIControlService.msg_growl("error", "NOTIF.API")
		            UIControlService.unloadLoading();
		        });
		    } else {
		        UIControlService.loadLoading("NOTIF.LOADING_UPLOAD_FILE");
		        UploaderService.uploadSingleFileAgreement(id, file, size, filters, function (response) {
		            UIControlService.unloadLoading();
		            //console.info("response:" + JSON.stringify(response));
		            if (response.status == 200) {
		                //console.info(response);
		                var url = response.data.Url;
		                vm.pathFile = url;
		                vm.name = response.data.FileName;
		                var s = response.data.FileLength;
		                vm.DocUrl = vm.pathFile;
		                //console.info(vm.DocUrl);
		                if (vm.flag == 0) {
		                    vm.size = Math.floor(s);
		                    //console.info(vm.size);
		                }
		                if (vm.flag == 1) {
		                    vm.size = Math.floor(s / (1024));
		                }

		                AssessmentPrequalService.insertDocST({
		                    VendorPrequalID: vm.VendorPrequalID,
		                    DocType: vm.DocType,
		                    DocumentUrl: vm.DocUrl,
		                    UploadDate: vm.tglSekarang,
		                    IsActive: 1,
		                    PrequalStepID: vm.PrequalStepID

		                }, function (reply) {
		                    //console.info("reply" + JSON.stringify(reply))
		                    UIControlService.unloadLoadingModal();
		                    if (reply.status === 200) {
		                        UIControlService.msg_growl("success", "NOTIF.SUC_UPLOAD");
		                        $uibModalInstance.close();

		                    } else {
		                        UIControlService.msg_growl("error", "NOTIF.FAIL_UPLOAD");
		                        return;
		                    }
		                }, function (err) {
		                    console.info(err);
		                    UIControlService.msg_growl("error", "NOTIF.API");
		                    UIControlService.unloadLoadingModal();
		                });
		            } else {
		                UIControlService.msg_growl("error", "error");
		                return;
		            }
		        }, function (response) {
		            console.info(response);
		            UIControlService.msg_growl("error", "NOTIF.API")
		            UIControlService.unloadLoading();
		        });
		    }
		}


		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

	}
})();