﻿(function () {
    'use strict';

    angular.module("app").controller("LihatKuesionerAssPrequalCtrl", ctrl);


    ctrl.$inject = ['item', '$http', '$window', '$translate', '$stateParams', '$state', '$uibModal', '$translatePartialLoader', 'VerifiedSendService', 'SrtPernyataanPrequalService', 'UIControlService', 'GlobalConstantService', 'VendorRegistrationService', 'AssessmentPrequalService', '$uibModalInstance'];
    function ctrl(item, $http, $window, $translate, $stateParams, $state, $uibModal, $translatePartialLoader, VerifiedSendService, SrtPernyataanPrequalService, UIControlService, GlobalConstantService, VendorRegistrationService, AssessmentPrequalService, $uibModalInstance) {

        var vm = this;
        vm.init = init;

        function init() {
            //$translatePartialLoader.addPart('contract-variation');
            vm.data = item.data;


        }


        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }


    }
})();
//TODO


