(function () {
	'use strict';

	angular.module("app").controller("DataPengalamanController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'AssessmentPrequalService', '$timeout', '$uibModal', '$translatePartialLoader', 'UIControlService', 'VendorExperienceService', '$filter'];
	/* @ngInject */
	function ctrl($state, $stateParams, AssessmentPrequalService, $timeout, $uibModal, $translatePartialLoader, UIControlService, VendorExperienceService, $filter) {
		var vm = this;
		vm.totalItems = 0;
		vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
		vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
		vm.VEPID = Number($stateParams.VEPID);
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.vendorID = [];
		vm.comodity = [];
		vm.isApprovedCR = false;

		vm.initialize = initialize;
		function initialize() {
			$translatePartialLoader.addPart('data-pengalaman');
			loadSetupStep();
		};

		vm.loadSetupStep = loadSetupStep;
		function loadSetupStep() {
			AssessmentPrequalService.SelectSetupStep({
			    Status: vm.PrequalStepAssID,
			    column: vm.VendorPrequalID,
			    Offset: vm.VEPID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.isEntry = reply.data.IsNotStarted;
					vm.PrequalStepID = reply.data.PrequalStepID;
					vm.PrequalSetupID = reply.data.PrequalSetupID;
					vm.IsNeedRevision = reply.data.IsNeedRevision;
					vm.remark = reply.data.Remark;
					loadExp();
					loadExpPrequal();

					//loadVendorCommodity(vm.Vendor.VendorID);
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadExp = loadExp;
		function loadExp() {
			vm.listExp = [];
			vm.listExpFinish = [];
			AssessmentPrequalService.SelectExp({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    console.info("data:"+JSON.stringify(reply.data));
					for (var i = 0; i < reply.data.length; i++) {
						reply.data[i].StartDate = UIControlService.getStrDate(reply.data[i].StartDate);

						if (reply.data[i].CityLocation == null) {
						    reply.data[i].AddressInfo = reply.data[i].Address;
						} else {
						    reply.data[i].AddressInfo = reply.data[i].Address + ", " + reply.data[i].CityLocation.Name + ", " + reply.data[i].CityLocation.State.Country.Name;
						}

						if (reply.data[i].ExperienceRef.Name == "FINISH_PROJECT") vm.listExp.push(reply.data[i]);
						else vm.listExpFinish.push(reply.data[i]);
					}
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadExpPrequal = loadExpPrequal;
		function loadExpPrequal() {
			vm.listExpPrequal = [];
			vm.listExpFinishPrequal = [];
			AssessmentPrequalService.SelectExpPrequal({
				PrequalSetupStepID: vm.PrequalStepID,
				VendorPrequalID: vm.VendorPrequalID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					for (var i = 0; i < reply.data.length; i++) {
						reply.data[i].StartDate = UIControlService.getStrDate(reply.data[i].StartDate);

						if (reply.data[i].CityLocation == null) {
						    reply.data[i].AddressInfo = reply.data[i].Address;
						} else {
						    reply.data[i].AddressInfo = reply.data[i].Address + ", " + reply.data[i].CityLocation.Name + ", " + reply.data[i].CityLocation.State.Country.Name;
						}

						if (reply.data[i].ExperienceRef.Name == "FINISH_PROJECT") vm.listExpPrequal.push(reply.data[i]);
						else vm.listExpFinishPrequal.push(reply.data[i]);
					}
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadVendor = loadVendor;
		function loadVendor() {
			VendorExperienceService.selectVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.Vendor = reply.data;
					loadVendorCommodity(vm.Vendor.VendorID);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data pengalaman perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadVendorCommodity = loadVendorCommodity;
		function loadVendorCommodity(data) {
			VendorExperienceService.SelectVendorCommodity({
				VendorID: data
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					console.info(reply.status);
					vm.comodity = reply.data;
					loadawal(vm.comodity.BusinessFieldID);
					console.info("comodity:" + JSON.stringify(vm.comodity));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data pengalaman perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		function loadCheckCR() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			console.info("CR:");
			VendorExperienceService.getCRbyVendor({ CRName: 'OC_VENDOREXPERIENCE' }, function (reply) {
				UIControlService.unloadLoading();
				console.info("CR:" + JSON.stringify(reply.status));
				if (reply.status === 200) {
					console.info("CR:" + JSON.stringify(reply.data));
					vm.CR = reply.data;
					if (reply.data === true) {
						vm.isApprovedCR = true;
					} else {
						vm.isApprovedCR = false;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.deleteData = deleteData;
		function deleteData(data) {
			bootbox.confirm($filter('translate')('MESSAGE.DELETECONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.DELETING');
					AssessmentPrequalService.deleteExp({
						ID: data.ID
					}, function () {
						UIControlService.unloadLoading();
						window.location.reload();
					}, function () {
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.addExp = addExp;
		function addExp(data, isAdd, isCR) {
			//console.info(isCR);
			var sendData = {
				item: data,
				isAdd: isAdd,
				isCR: isCR,
				VendorEntryPrequalID: vm.VEPID
			}
			//console.info(JSON.stringify(sendData));
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/data-pengalaman/form-pengalaman.html',
				controller: 'formExpCtrl',
				controllerAs: 'formExpCtrl',
				resolve: {
					item: function () {
						return sendData;
					}
				}
			});

			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.detailExp = detailExp;
		function detailExp(data) {
			var sendData = {
				item: data,
				isAdd: false
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/data-pengalaman/detail-pengalaman.html',
				controller: 'formExpCtrl',
				controllerAs: 'formExpCtrl',
				resolve: {
					item: function () {
						return sendData;
					}
				}
			});

			modalInstance.result.then(function () {
				loadawal();
			});
		}

		vm.loadAwal = loadawal;
		function loadawal(data) {
			//limit sementara dibuat 100
			UIControlService.loadLoading('MESSAGE.LOADING_VENDOREXPERIENCE');
			VendorExperienceService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: 100,
				Keyword: vm.keyword,
				FilterType: data,
				column: 1
			}, function (reply) {
				console.info("lrjlbij" + vm.comodity.BusinessFieldID);
				if (reply.status === 200) {
					reply.data = reply.data.List;
					console.info("listFinishExp:" + JSON.stringify(reply.data));
					for (var i = 0; i < reply.data.length; i++) {
						reply.data[i].StartDate = UIControlService.getStrDate(reply.data[i].StartDate);
						reply.data[i].AddressInfo = reply.data[i].Address + ", " + reply.data[i].CityLocation.Name + ", " + reply.data[i].CityLocation.State.Country.Name;
					}
					vm.totalItems = reply.data.Count;
					console.info("finish:" + JSON.stringify(reply.data));
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
			});

			UIControlService.loadLoading('MESSAGE.LOADING_VENDOREXPERIENCE');
			VendorExperienceService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: 100,
				Keyword: vm.keyword,
				column: 2
			}, function (reply) {
				//console.info("current?:"+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listCurrentExp = reply.data.List;
					for (var i = 0; i < vm.listCurrentExp.length; i++) {
						vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
						vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].CityLocation.Name + ", " + vm.listCurrentExp[i].CityLocation.State.Country.Name;
					}
					console.info("current exp:" + JSON.stringify(vm.listCurrentExp));
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
			});

		}

		vm.editActive = editActive;
		function editActive(data, active) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			VendorExperienceService.Delete({ ID: data.ID, IsActive: active }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var msg = "";
					if (active === false) msg = " NonAktifkan ";
					if (active === true) msg = "Aktifkan ";
					UIControlService.msg_growl("success", "{{'MESSAGE.SUCC_DATA' | translate}}" + msg);
					$timeout(function () {
						window.location.reload();
					}, 1000);
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_DEACTIVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.valid = true;
		vm.notvalid = function notvalid() {
			vm.valid = false;
		}


		vm.Submit = Submit;
		function Submit(flag) {
			vm.valid = flag;
			if (vm.remark == undefined) {
				vm.remark = "";
			}
			AssessmentPrequalService.InsertSubmit({
				PrequalStepID: vm.PrequalStepAssID,
				VendorPrequalID: vm.VendorPrequalID,
				IsAgree: flag,
				VEPID: vm.VEPID,
				Remark: vm.remark
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					//$state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepID, VendorPrequalID: vm.VendorPrequalID });
					$state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}
	}


})();// baru controller pertama
