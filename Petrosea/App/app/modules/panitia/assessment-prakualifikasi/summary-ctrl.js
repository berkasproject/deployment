﻿(function () {
    'use strict';

    angular.module("app").controller("SummaryPQVerificationCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'AssessmentPrequalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, AssessmentPrequalService,
         UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.StepID = item.StepID;
        vm.PrequalStepID = item.PrequalStepID;
        vm.PrequalSetupID = item.PrequalSetupID;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.tender = {};
        vm.nb = item.item;
       // vm.jLoad = jLoad;

        function init() {
            //console.info(item);
            UIControlService.loadLoading("Silahkan Tunggu...");
            jLoad();

        }
        
        vm.jLoad = jLoad;
        function jLoad() {
            AssessmentPrequalService.listSummary({
                Status:vm.PrequalSetupID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listSummary = reply.data;
                    console.info("data:" + JSON.stringify(vm.listSummary));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Penilai" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        
        vm.simpan = simpan;
        function simpan() {
            vm.list = [];
            for (var i = 0; i < vm.listSummary.length; i++) {
                if (vm.listSummary[i].IsPassed === null) {
                    vm.listSummary[i].IsPassed = false;
                }
                if (vm.listSummary[i].ID !== 0) {
                    var dta = {
                        ID: vm.listSummary[i].ID,
                        IsPassed: vm.listSummary[i].IsPassed
                    };
                }
                else {
                    var dta = {
                        ID: vm.listSummary[i].ID,
                        IsPassed: vm.listSummary[i].IsPassed
                    };
                }
                vm.list.push(dta);

                AssessmentPrequalService.updateSummary(dta,
                    function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "Berhasil Simpan data");
                            $uibModalInstance.close();
                        }
                        else {
                            UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                            return;
                        }
                    },
                    function (err) {
                        UIControlService.msg_growl("error", "Gagal Akses Api!!");
                        // UIControlService.unloadLoadingModal();
                    }
                );
            }
            console.info("summary" + JSON.stringify(vm.list));
        }

        vm.backpengadaan = backpengadaan;
        function backpengadaan() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType });
        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        }

    }
})();
//TODO


