(function () {
	'use strict';

	angular.module("app").controller("PendaftaranLelangCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService', '$filter', 'PendaftaranLelangService', 'RoleService', 'UIControlService', '$uibModal', '$stateParams', 'GlobalConstantService', '$state'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService, $filter, PendaftaranLelangService, RoleService, UIControlService, $uibModal, $stateParams, GlobalConstantService, $state) {
		var vm = this;
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.StepID = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.pendaftaran = [];
		vm.userBisaNgatur = false;
		vm.page_id = 103;
		vm.nama_paket = "";
		vm.nama_tahapan = "";
		vm.is_created = false;
		vm.status = -1;
		vm.peserta = [];
		vm.menuhome = 0;
		vm.labelcurr;
		vm.isCancelled;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('pendaftaran-lelang');
			// UIControlService.loadLoading("Silahkan Tunggu...");
			//  getBlacklist();
			//console.info(vm.StepID + ">>");

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			jLoad(1);
			loadDataTender();
			loadTenderInterest();
			loadTenderInterestReview();
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('pendaftaran-lelang-print', {
				TenderRefID: vm.TenderRefID,
				StepID: vm.StepID,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SENDAPPROVAL'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.LOADING');
					PendaftaranLelangService.sendToApproval({ ID: vm.StepID }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
							init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		function loadDataTender() {
			PendaftaranLelangService.getDataStepTender({
				ID: vm.StepID
			}, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.TenderName = data.tender.TenderName;
					vm.isCancelled = data.tender.IsCancelled;
					vm.StartDate = UIControlService.getStrDate(data.StartDate);
					vm.EndDate = UIControlService.getStrDate(data.EndDate);
					vm.nama_tahapan = data.step.TenderStepName;
					vm.TenderID = data.TenderID;
					vm.DocumentUrl = data.DocumentUrl;
					vm.IsOvertime = data.IsOvertime;
					console.info("tender::" + JSON.stringify(data));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				//UIControlService.unloadLoading();
			});
		}


		vm.tenderInterestReview = [];
		function loadTenderInterestReview() {
			PendaftaranLelangService.SelectTenderInterestReview({
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.tenderInterestReview = reply.data;
					console.info("tender::" + JSON.stringify(vm.tenderInterestReview));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				//UIControlService.unloadLoading();
			});
		}

		vm.tenderInterest = [];
		function loadTenderInterest() {
			PendaftaranLelangService.SelectTenderInterest({
				StepID: vm.StepID,
				ProcPackageType: vm.ProcPackType,
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.tenderInterest = reply.data;
					//console.info("tender::" + JSON.stringify(vm.tenderInterest));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				//UIControlService.unloadLoading();
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.register = [];
			UIControlService.loadLoading("");
			PendaftaranLelangService.SelectTender({
				StepID: vm.StepID,
				ProcPackageType: vm.ProcPackType,
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.register = reply.data;
					//console.info(JSON.stringify(vm.register));

					PendaftaranLelangService.isNeedTenderStepApproval({
						TenderRefID: vm.TenderRefID,
						TenderStepDataID: vm.StepID,
						ProcPackageType: vm.ProcPackType
					}, function (result) {
						UIControlService.unloadLoading();
						vm.isNeedApproval = result.data;
						if (!vm.isNeedApproval) {
							PendaftaranLelangService.isApprovalSent({ TenderStepID: vm.StepID }, function (result2) {
								vm.isApprovalSent = result2.data;
							});
						}
						//console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Pendaftaran Lelang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.approve = approve;
		function approve(data, isApprove, msg) {
			//console.info("data" + JSON.stringify(data));


			bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')(msg) + data.Vendor.VendorName + '? </h4>', function (res) {
				if (res) {

					PendaftaranLelangService.approvePendaftaran({
						ID: data.ID,
						IsApprove: isApprove,
						VendorID: data.VendorID,
						TenderStepDataID: data.TenderStepDataID,
						TenderID: data.TenderID,
						CreatedDate: data.CreatedDate
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_PROCESS");
							//$uibModalInstance.close();
							init();
						}
						UIControlService.unloadLoading();
					}, function (error) {
						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoading();
					});

					//SocketService.emit("daftarRekanan");
				}
			});


		}




		vm.sendApprovalTenderInterest = sendApprovalTenderInterest;
		function sendApprovalTenderInterest() {


			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SENDAPPROVAL'), function (res) {
				if (res) {



					PendaftaranLelangService.sendApprovalTenderInterest({
						ID: vm.tenderInterestReview.ID,
						TenderStepDataID: vm.tenderInterestReview.TenderStepDataID
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_PROCESS");
							//$uibModalInstance.close();
							init();
						}
						UIControlService.unloadLoading();
					}, function (error) {
						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoading();
					});

					//SocketService.emit("daftarRekanan");
				}
			});



		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.StepID;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pendaftaran/detailApproval.html',
				controller: "tenderRegisCtrl",
				controllerAs: "tenderRegisCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', {
				TenderRefID: vm.TenderRefID,
				ProcPackType: vm.ProcPackType,
				TenderID: vm.TenderID
			});
		}



		vm.summaryTenderInterest = summaryTenderInterest;
		function summaryTenderInterest() {
			var data = {
				data: vm.tenderInterest,
				TenderRefID: vm.TenderRefID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pendaftaran/summary.html',
				controller: 'SummaryTenderInterestCtrl',
				controllerAs: 'SummaryTenderInterestCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.detailApprovalTenderInterest = detailApprovalTenderInterest;
		function detailApprovalTenderInterest() {
			var data = {
				data: vm.tenderInterestReview
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pendaftaran/detailApproval-tenderInterest.html',
				controller: 'DetailApprovalTenderInterestCtrl',
				controllerAs: 'DetailApprovalTenderInterestCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}


		vm.lihatDokumenVendor = lihatDokumenVendor;
		function lihatDokumenVendor(data) {
			var data = {
				isTenderInterestDoc: true,
				data: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pendaftaran/DetailPendaftaranLelang.html',
				controller: 'DetailPendaftaranLelangCtrl',
				controllerAs: 'DetailPendaftaranLelangCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.lihatPendaftaranLelang = lihatPendaftaranLelang;
		function lihatPendaftaranLelang() {
			var data = {
				isTenderInterestDoc: false,
				TenderName: vm.TenderName,
				DocumentUrl: vm.DocumentUrl
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pendaftaran/DetailPendaftaranLelang.html',
				controller: 'DetailPendaftaranLelangCtrl',
				controllerAs: 'DetailPendaftaranLelangCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.uploadDokumen = uploadDokumen;
		function uploadDokumen(tenderID, tenderStepID) {
			var data = {
				TenderID: tenderID, TenderStepID: tenderStepID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pendaftaran/uploadDokumen.html',
				controller: 'FormUploadPendaftaranLelangCtrl',
				controllerAs: 'FormUploadPendaftaranLelangCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}
	}
})();