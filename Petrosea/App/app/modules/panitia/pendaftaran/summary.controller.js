﻿(function () {
    'use strict';

    angular.module("app").controller("SummaryTenderInterestCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PendaftaranLelangService', 'RoleService', 'UIControlService', '$uibModal', 'GlobalConstantService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PendaftaranLelangService,
        RoleService, UIControlService, $uibModal, GlobalConstantService, item, $uibModalInstance) {
        var vm = this;
        vm.data = item.data;
        vm.TenderRefID=item.TenderRefID;
        vm.pendaftaran = [];
        vm.userBisaNgatur = false;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.page_id = 103;
        vm.nama_paket = "";
        vm.nama_tahapan = "";
        vm.is_created = false;
        vm.status = -1;
        vm.peserta = [];
        vm.menuhome = 0;
        vm.labelcurr;

        vm.init = init;
        function init() {
            //console.info(JSON.stringify(vm.data));
            $translatePartialLoader.addPart('pendaftaran-lelang');
            loadTenderInterestReview();
            // UIControlService.loadLoading("Silahkan Tunggu...");
            //  getBlacklist();

        }


        vm.tenderInterestReview = [];
        function loadTenderInterestReview() {
            PendaftaranLelangService.SelectTenderInterestReview({
                TenderRefID: vm.TenderRefID
            }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.tenderInterestReview = reply.data;
                    console.info("tender::" + JSON.stringify(vm.tenderInterestReview));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                //UIControlService.unloadLoading();
            });
        }

        vm.simpan = simpan;
        function simpan() {
            vm.tenderInterestReview.TenderStepDataID = vm.tenderInterestReview.TenderInterestVendor[0].TenderStepDataID;
            vm.tenderInterestReview.TenderID = vm.tenderInterestReview.TenderInterestVendor[0].TenderID;
            PendaftaranLelangService.reviewTenderInterest(vm.tenderInterestReview, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_PROCESS");
                    $uibModalInstance.close();
                    //init();
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }


        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();