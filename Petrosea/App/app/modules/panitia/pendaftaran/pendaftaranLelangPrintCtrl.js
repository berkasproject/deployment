﻿(function () {
	'use strict';

	angular.module("app").controller("PendaftaranLelangPrintCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', '$stateParams', 'PendaftaranLelangService', 'UIControlService', '$state'];
	function ctrl($translatePartialLoader, $stateParams, PendaftaranLelangService, UIControlService, $state) {
		var vm = this
		vm.TenderRefID = Number($stateParams.TenderRefID)
		vm.StepID = Number($stateParams.StepID)
		vm.ProcPackType = Number($stateParams.ProcPackType)
		vm.maxRow = 20
		vm.jumlahlembar = 1
		vm.pagedData = []

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('pendaftaran-lelang')
			jLoad(1)
			loadDataTender()
		}

		function loadDataTender() {
			PendaftaranLelangService.getDataStepTender({
				ID: vm.StepID
			}, function (reply) {
				if (reply.status === 200) {
					var data = reply.data;
					vm.TenderName = data.tender.TenderName;
					vm.isCancelled = data.tender.IsCancelled;
					vm.StartDate = UIControlService.getStrDate(data.StartDate);
					vm.EndDate = UIControlService.getStrDate(data.EndDate);
					vm.nama_tahapan = data.step.TenderStepName;
					vm.TenderID = data.TenderID;
					vm.DocumentUrl = data.DocumentUrl;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('pendaftaran-lelang', {
				TenderRefID: vm.TenderRefID,
				ProcPackType: vm.ProcPackType,
				StepID: vm.StepID
			});
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download('Tender Registration - ' + vm.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.register = [];
			UIControlService.loadLoading("");
			PendaftaranLelangService.SelectTender({
				ProcPackageType: vm.ProcPackType,
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					vm.register = reply.data;
					vm.jumlahlembar = Math.ceil(reply.data.length / vm.maxRow);
					for (var i = 0; i < vm.jumlahlembar; i++) {
						vm.pagedData[i] = vm.register.slice(i * vm.maxRow, vm.maxRow * (i + 1));
					}

				} else {
					$.growl.error({ message: "Gagal mendapatkan data Pendaftaran Lelang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
	}
})()