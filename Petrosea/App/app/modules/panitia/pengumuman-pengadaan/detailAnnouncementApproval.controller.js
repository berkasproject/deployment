﻿(function () {
	'use strict';

	angular.module("app").controller("detailAnnouncementApprovalController", ctrlApproval);

	ctrlApproval.$inject = ['item', 'UIControlService', 'PengumumanPengadaanService', '$uibModalInstance', '$translatePartialLoader'];
	function ctrlApproval(item, UIControlService, PengumumanPengadaanService, $uibModalInstance, $translatePartialLoader) {

	    var vm = this;
	    var loadmsg = "";
	    var ID = item.ID;
	    
	    vm.init = init;
	    function init() {
	        loadData();
	    };

	    vm.loadData = loadData;
	    function loadData() {
	        UIControlService.loadLoading(loadmsg);
	        PengumumanPengadaanService.currentAnnouncementApproval({
	            ID: ID
	        }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                vm.list = reply.data;
	            } else {
	                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
	            }
	        }, function (error) {
	            UIControlService.unloadLoading();
	            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
	        });
	    }

	    vm.cancel = cancel;
	    function cancel() {
	        $uibModalInstance.dismiss('cancel');
	    };
	}
})();