﻿(function () {
	'use strict';

	angular.module("app")
    .controller("formPPController", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PengumumanPengadaanService', '$state', 'UIControlService', 'item', '$uibModalInstance',
        'UploadFileConfigService', 'UploaderService', '$uibModal', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        PengumumanPengadaanService, $state, UIControlService, item, $uibModalInstance, UploadFileConfigService,
        UploaderService, $uibModal, GlobalConstantService) {
		console.info(item);
		var vm = this;
		vm.DataTender = item.DataTender;
		vm.ContractRequisitionID = item.ContractRequisitionID;
		vm.Description = '';
		vm.fileUpload;
		vm.ProcPackType = vm.DataTender.IDProcPackType;
		vm.TypeTender = item.TypeTender;
		vm.TenderID = item.IDRefTender;
		vm.TenderStepDataID = item.IDStepTender;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.Vendors = [];
		vm.isAdd = item.isAdd;
		vm.viewvendor = [];
		//ng-model
		vm.TenderCode = ''; vm.TenderName = ''; vm.IsLocal = false; vm.IsNational = false; vm.IsInternational = false;
		vm.IsVendorEmails = false; vm.IsOpen = false; vm.Emails = ''; vm.CompScale = null; vm.CommodityID = null; vm.DocUrl = '';
		vm.vendorByAreaNotSaved = false;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('pengumuman-pengadaaan-tender');
			loadCR();
			loadTypeSizeFile();
			loadKlasifikasi();
			loadKomoditi();
			loadTechnical();
			loadEmail();
			vm.TenderCode = vm.DataTender.TenderCode;
			vm.TenderName = vm.DataTender.TenderName;
			if (vm.DataTender.IsLocal === true) {
				vm.IsLocal = vm.DataTender.IsLocal;
			}
			if (vm.DataTender.IsNational === true) {
				vm.IsNational = vm.DataTender.IsNational;
			}
			if (vm.DataTender.IsInternational === true) {
				vm.IsInternational = vm.DataTender.IsInternational;
			}
			if (vm.DataTender.IsOpen === true) {
				vm.IsOpen = vm.DataTender.IsOpen;
			}
			if (vm.DataTender.IsVendorEmails === true) {
				vm.IsVendorEmails = vm.DataTender.IsVendorEmails;
				//vm.Emails = vm.DataTender.Emails;
			}

			vm.CompScale = vm.DataTender.CompScale;
			vm.CommodityID = vm.DataTender.CommodityID;
			vm.Vendors = vm.DataTender.Vendors;
			if (!vm.Vendors)
			{
			    vm.Vendors = [];
			}
			if (vm.isAdd === false) {
				vm.Description = vm.DataTender.Description;
				vm.TechnicalID = vm.DataTender.TechnicalID;
				vm.DocUrl = vm.DataTender.DocUrl;
			}
			if (vm.DataTender.Emails != undefined) {
				if (vm.DataTender.Emails != null) {
					vm.EmailsView = "";
					vm.EmailSplit = vm.DataTender.Emails.split(',');
					for (var i = 0; i < vm.EmailSplit.length; i++) {
						if (vm.EmailsView == "") vm.EmailsView = vm.EmailSplit[i];
						else vm.EmailsView += ', ' + vm.EmailSplit[i];
					}
				}
			}
			vm.Emails = vm.DataTender.Emails;
			vm.sendEmail = [];
			var batas = vm.Vendors.length - 1;
			if (vm.Emails === '' || vm.Emails === null) {
				//for (var i = 0; i < vm.Vendors.length; i++) {
				//	if (i === batas) {
				//		vm.Emails += vm.Vendors[i].Email;
				//	} else {
				//		vm.Emails += vm.Vendors[i].Email + " , ";
				//	}
				//}
			}
			if (!(vm.Emails === '' || vm.Emails === null)) {
				vm.IsVendorEmails = true;
			}
			if (item.DetailVendorComm != undefined) {
				if (item.DetailVendorComm != null) {

					vm.viewvendor = item.DetailVendorComm;
				}
			}
		}

		vm.loadCR = loadCR;
		function loadCR() {
			PengumumanPengadaanService.getCR({ TenderRefID: vm.TenderID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.flagDirectAward = reply.data;
				}
			}, function (err) {
			});
		}

		vm.loadEmail = loadEmail;
		function loadEmail() {
			PengumumanPengadaanService.getEmailContent({
				Keyword: "Pengumuman Tender Jasa",
				Offset: 0,
				Limit: 10
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					for (var i = 0; i < reply.data.List.length; i++) {
						if (reply.data.List[i].Name == "Pengumuman Tender Jasa") {
							vm.EmailContent = reply.data.List[i].EmailContent1;
							vm.Description = vm.EmailContent;
							break;

						}
					}

				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		/*proses upload file*/
		function loadTypeSizeFile() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.ADMIN.TENDER.ANNOUNCEMENT", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}

		vm.uploadFile = uploadFile;
		function uploadFile() {
			var folder = "TENDER" + vm.TypeTender + vm.TenderID;
			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder);
			}

		}
		vm.hapusVendor = hapusVendor;
		function hapusVendor(index, data) {
			vm.Vendors.splice(index, 1);
		}

		function upload(file, config, filters, folder, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, folder,
                function (response) {
                	UIControlService.unloadLoading();
                	if (response.status == 200) {
                		var url = response.data.Url;
                		UIControlService.msg_growl("success", "MESSAGE.SUCC_UPLOAD");
                		processSave(url);

                	} else {
                		UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                    if (response.data == "ERRORS.FILE_SIZE_EXCEEDED") {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_FILE_EXCEEDED")
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.API")
                        UIControlService.unloadLoading();
                    }
                });

		}

		function validateFileType(file, allowedFileTypes) {
			//console.info(JSON.stringify(allowedFileTypes));
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}

			/* Berbeda antara Chrome dan Mozilla FF
            var selectedFileType = file[0].type;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
            //console.info("tipefile: " + selectedFileType);
            if (selectedFileType === "vnd.ms-excel") {
                selectedFileType = "xls";
            }
            else if (selectedFileType === "vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                selectedFileType = "xlsx";
            } else if (selectedFileType === "application/msword") {
                selectedFileType = "doc";
            }
            else if (selectedFileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || selectedFileType == "vnd.openxmlformats-officedocument.wordprocessingml.document") {
                selectedFileType = "docx";
            }
            else {
                selectedFileType = selectedFileType;
            }
            //jika excel
            var allowed = false;
            for (var i = 0; i < allowedFileTypes.length; i++) {
                if (allowedFileTypes[i].Name == selectedFileType) {
                    allowed = true;
                    return allowed;
                }
            }

            if (!allowed) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_INVALID_FILETYPE");
                return false;
            }
            */
			return true;
		}
		/* end proses upload*/

		//list combo komoditas
		vm.selectedComodity;
		vm.listComodity = [];
		function loadKomoditi() {
			PengumumanPengadaanService.getCommodity({ type: 0 }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listComodity = reply.data;
					if ((vm.CommodityID > 0 || vm.CommodityID != null)) {
						console.info(vm.CommodityID + "...");
						for (var i = 0; i < vm.listComodity.length; i++) {
							if (vm.CommodityID === vm.listComodity[i].ID) {
								vm.selectedComodity = vm.listComodity[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeComodity = changeComodity;
		function changeComodity() {
		    vm.CommodityID = vm.selectedComodity.ID;
		    vm.viewvendor = [];
		    vm.vendorByAreaNotSaved = true;
		}

		//list combo klasifikasi
		vm.selectedClasification;
		vm.listClasification = [];
		function loadKlasifikasi() {
			PengumumanPengadaanService.getClasification(function (reply) {
				UIControlService.unloadLoading();
				vm.listClasification = reply.data.List;
				if ((vm.CompScale > 0 || vm.CompScale != null)) {
					for (var i = 0; i < vm.listClasification.length; i++) {
						if (vm.CompScale === vm.listClasification[i].RefID) {
							vm.selectedClasification = vm.listClasification[i];
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeClasification = changeClasification;
		function changeClasification() {
			vm.CompScale = vm.selectedClasification.RefID;
			vm.viewvendor = [];
			vm.vendorByAreaNotSaved = true;
        }

		//list combo technical
		vm.selectedTechnical;
		vm.listTechnical = [];
		function loadTechnical() {
			PengumumanPengadaanService.getTechnical(function (reply) {
				UIControlService.unloadLoading();
				vm.listTechnical = reply.data.List;
				if ((vm.TechnicalID > 0 || vm.TechnicalID != null)) {
					for (var i = 0; i < vm.listTechnical.length; i++) {
						if (vm.TechnicalID === vm.listTechnical[i].RefID) {
							vm.selectedTechnical = vm.listTechnical[i]
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeTechnical = changeTechnical;
		function changeTechnical() {
			vm.TechnicalID = vm.selectedTechnical.RefID;
			vm.viewvendor = [];
			vm.vendorByAreaNotSaved = true;
        }

		vm.changeArea = changeArea;
		function changeArea() {
		    vm.viewvendor = [];
		    vm.vendorByAreaNotSaved = true;
		}

		/* tambah vendor */
		vm.openDataVendor = openDataVendor;
		function openDataVendor() {
			var data = {};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pengumuman-pengadaan/DataVendor.html',
				controller: 'DataVendorCtrl',
				controllerAs: 'DataVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (dataVendor) {
				//console.info("get:"+JSON.stringify(dataVendor));
				//cek vendor sudah ada belum
				var foundby = $.map(vm.Vendors, function (val) {
					return val.VendorID === dataVendor.VendorID ? val : null;
				});
				if (foundby.length > 0) {
					UIControlService.msg_growl("error", "Vendor " + dataVendor.VendorName + " sudah ditambahkan!!");
					return;
				} else {
					vm.Vendors.push(dataVendor);
				}
			});
		}

		vm.openDataCommVendor = openDataCommVendor;
		function openDataCommVendor() {
		    vm.flag = true;

			if (vm.IsLocal == true) {
				if (vm.CommodityID == null) {
					vm.flag = false;
					UIControlService.msg_growl("error", "MESSAGE.NO_FIELD");
					return;
				}
				if (vm.CompScale == null) {
					vm.flag = false;
					UIControlService.msg_growl("error", "MESSAGE.NO_CLASS");
					return;
				} else if (vm.selectedTechnical == undefined) {
					vm.flag = false;
					UIControlService.msg_growl("error", "MESSAGE.NO_TECHNIC");
					return;
				}
			} else if (vm.IsLocal == false && vm.IsNational == false && vm.IsInternational == false) {
				vm.flag = false;
				UIControlService.msg_growl("error", "MESSAGE.NO_AREA");
				return;
			}
			if (vm.flag == true) {
				if (vm.IsLocal == false) {
					var data = {
						CommodityID: vm.CommodityID,
						IsLocal: vm.IsLocal,
						IsNational: vm.IsNational,
						IsInternational: vm.IsInternational,
						viewvendor: vm.viewvendor,
						ContractRequisitionID : vm.ContractRequisitionID
					};
				} else {
					var data = {
						CommodityID: vm.CommodityID,
						CompScale: vm.CompScale,
						IsLocal: vm.IsLocal,
						IsNational: vm.IsNational,
						IsInternational: vm.IsInternational,
						TechnicalID: vm.selectedTechnical.RefID,
						viewvendor: vm.viewvendor,
						ContractRequisitionID: vm.ContractRequisitionID
					};
				}

				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/panitia/pengumuman-pengadaan/viewVendorRFQ.html?v=1.000003',
					controller: 'DataVendorCtrl',
					controllerAs: 'DataVendorCtrl',
					resolve: {
						item: function () {
							return data;
						}
					}
				});
				modalInstance.result.then(function (dataVendor) {
				    vm.viewvendor = dataVendor;
				    vm.vendorByAreaNotSaved = false;
					//console.info(vm.viewvendor);
				});
			}

		}

		//proses simpan
		vm.saveData = saveData;
		function saveData() {
			if (vm.flagDirectAward == false) {
				if (vm.IsLocal == false && vm.IsNational == false && vm.IsInternational == false) {
					UIControlService.msg_growl("error", "MESSAGE.NO_AREA");
					return;
				} else if (vm.Vendors.length == 0 && vm.viewvendor.length == 0) {
				    UIControlService.msg_growl("error", "MESSAGE.NO_VENDOR");
				    return;
				}
			} else {
			    if (vm.Vendors.length == 0 && vm.viewvendor.length == 0) {
					UIControlService.msg_growl("error", "MESSAGE.NO_VENDOR");
					return;
				}
			}
			if (vm.Emails) {
				PengumumanPengadaanService.cekEmails({ Keyword: vm.Emails }, function (reply) {
					UIControlService.unloadLoading();
					//console.info("prq:: " + JSON.stringify(reply));
					if (reply.status === 200) {
						if (reply.data.Email == null) saveAll();
						else UIControlService.msg_growl("error", "MESSAGE.NOTIF1" + reply.data.Email + " MESSAGE.NOTIF2 " + reply.data.Name);
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoadingModal();
				});
			} else saveAll();

		}

		vm.saveAll = saveAll;
		function saveAll() {
			if (vm.selectedTechnical === undefined) {
				vm.TechnicalID = null;
			}
			if (vm.fileUpload === undefined) {
				console.info("nofile");
				processSave('')
			} else {
				uploadFile();
			}
		}

		function processSave(urlDoc) {
			if (vm.flagDirectAward == true) {
				vm.CommodityID = null;
				vm.IsLocal = null;
				vm.IsNational = null;
				vm.IsInternational = null;
				vm.TechnicalID = null;
				vm.CompScale = null;
			}
			//if (vm.viewvendor.length == 0 && !(vm.flagDirectAward == true) && vm.IsLocal == true) {
			//    PengumumanPengadaanService.viewVendor({
			//        CommodityID: vm.CommodityID,
			//        IsLokal: true,
			//        IsNational: false,
			//        IsInternational: false,
			//        CompanyScaleID: vm.CompScale,
			//        TechnicalID: vm.TechnicalID
			//    }, function (reply) {
			//        if (reply.status === 200) {
			//            vm.list = reply.data;
			//            for (var i = 0; i < vm.list.length; i++) {
			//                var data = { VendorID: vm.list[i].VendorID, IsByArea: true }
			//                detailVendor.push(data);
			//            }
			//        }
			//    }, function (err) {
			//    });
			//}
			//var detailVendor = [];
			//for (var i = 0; i < vm.Vendors.length; i++) {
			//	var data = { VendorID: vm.Vendors[i].VendorID, IsByArea:false }
			//	detailVendor.push(data);
			//}
			//for (var i = 0; i < vm.viewvendor.length; i++) {
			//    var data = { VendorID: vm.viewvendor[i].VendorID, IsByArea: true }
			//    detailVendor.push(data);
			//}
			//set send email
			if (vm.Emails) {
				if (vm.Emails != "") vm.sendEmail = vm.Emails.split(',');
			}
			//console.info(JSON.stringify(vm.sendEmail));

			var dataAnnouncement = {
				CommodityID: vm.CommodityID,
				CompanyScaleID: vm.CompScale,
				Description: vm.Description,
				DocUrl: urlDoc,
				Emails: vm.Emails,
				IsInternational: vm.IsInternational,
				IsLokal: vm.IsLocal,
				IsNational: vm.IsNational,
				IsOpen: vm.IsOpen,
				IsVendorEmail: vm.IsVendorEmails,
				TechnicalID: vm.TechnicalID,
				TenderStepDataID: vm.TenderStepDataID
			}
			UIControlService.unloadLoadingModal("MESSAGE.LOADING");
			PengumumanPengadaanService.insertAnnouncement({
				TenderAnnouncement: dataAnnouncement, DetailVendor: vm.Vendors, DetailVendorByArea: vm.viewvendor, flagDirectAward: vm.flagDirectAward
			}, function (reply) {
				//console.info("insert"+JSON.stringify(reply));
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
					$uibModalInstance.close();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});

		}

		function sendEmail() {
			var email = {
				subject: 'Pengumuman ' + vm.TenderName,
				mailContent: vm.Description + '<br />' + '<label>Tender code : ' + vm.DataTender.TenderCode + '</label>',
				isHtml: true,
				addresses: vm.sendEmail
			};

			UIControlService.loadLoadingModal("Loading");
			PengumumanPengadaanService.sendEmailToVendor(email, function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					UIControlService.msg_growl("notice", "MESSAGE.SUCC_EMAIL");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_EMAIL");
					return;
				}
				//$state.go('daftar_kuesioner');
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				//$state.go('daftar_kuesioner');
			});
		}

		function sendEmailVendors() {
			var email = {
				subject: 'Pengumuman ' + vm.TenderName,
				mailContent: vm.Description,
				isHtml: true,
				Addresses: vm.DataTender.filteredEmails
			};

			UIControlService.loadLoadingModal("Loading");
			PengumumanPengadaanService.sendEmailToVendors(email, function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					//UIControlService.msg_growl("notice", "Berhasil Kirim Email");
					$uibModalInstance.close();
				} else {
					//UIControlService.msg_growl("error", "Gagal Kirim Email, Ada email tidak sesuai format");
					return;
				}
				//$state.go('daftar_kuesioner');
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				//$state.go('daftar_kuesioner');
			});
		}

		vm.batal = batal;
		function batal() {
			console.info("batal");
			$uibModalInstance.dismiss('cancel');
		};
	}
})();