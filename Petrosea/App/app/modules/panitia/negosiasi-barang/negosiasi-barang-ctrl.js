﻿(function () {
	'use strict';

	angular.module("app").controller("NegosiasiBarangCtrl", ctrl);

	ctrl.$inject = ['$filter', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService', 'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService, NegosiasiBarangService, RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.jLoad = jLoad;
		vm.nb = [];
		vm.GoodsNegoId = 0;
		vm.generate = 7;

		function init() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			$translatePartialLoader.addPart('negosiasi-barang');

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			loadOvertime();
			loadStepNego();
			jLoad(1);
		}

		vm.loadOvertime = loadOvertime;
		function loadOvertime() {
			NegosiasiBarangService.isOvertime({ ID: vm.StepID }, function (reply) {
				if (reply.status === 200) {
					vm.overtime = reply.data;
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadStepNego = loadStepNego;
		function loadStepNego() {
			NegosiasiBarangService.loadStepNego({ ID: vm.StepID }, function (reply) {
				if (reply.status === 200) {
					vm.listTenderStep = reply.data;
					vm.listTenderStep.StartDate = UIControlService.convertDateTime(vm.listTenderStep.StartDate);
					vm.listTenderStep.EndDate = UIControlService.convertDateTime(vm.listTenderStep.EndDate);
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		//tampil data forum
		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.list = [];
			//console.info("curr "+current)
			vm.nb = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			NegosiasiBarangService.select({
				column: vm.StepID,
				status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}, function (reply) {
				if (reply.status === 200) {
					vm.nb = reply.data;
					vm.flagSum = true;
					for (var i = 0; i < vm.nb.length; i++) {
						if (vm.nb[i].Curr == null) vm.nb[i].Curr = "";
						if (vm.nb[i].ID !== 0) {
							if (vm.nb[i].IsDeal !== null) {
								console.info(vm.nb[i].IsDeal);
								vm.flagSum = false;
							} else {
								console.info(vm.nb[i].IsDeal);
								vm.flagSum = true;
							}
							vm.list.push(vm.nb[i]);
						}
					}
					vm.TenderStepDataID = vm.StepID;
					cek();
					for (var i = 0; i < vm.nb.length; i++) {
						if (vm.nb[i].ID === 0) {
							vm.cek_summary = true;
							i = vm.nb.length;
						}
					}
					UIControlService.unloadLoading();
				}
			}, function (err) {
				// console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//btn kembali
		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.listTenderStep.TenderID });
		}

		vm.cek = cek;
		function cek() {
			NegosiasiBarangService.cek({
				TenderStepDataID: vm.TenderStepDataID
			}, function (reply) {
				console.info("data:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.hasil_cek = reply.data;
					//console.info("data:" + JSON.stringify(vm.hasil_cek));
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				// console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//modal summary
		vm.do_summary = do_summary;
		function do_summary() {
			var data = {
				item: vm.list
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/negosiasi-barang/summary.html?v=1.000002',
				controller: 'SummaryBarangCtrl',
				controllerAs: 'SummaryBarangCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		//detail chat
		vm.viewDetail = viewDetail;
		function viewDetail(index, data, flag, flagNego) {
			if (flag == true) {
				if (vm.overtime == false) {
					UIControlService.msg_growl("error", "MESSAGE.NEGO_CANT_BEGIN");
					return;
				} else {
					vm.VendorID = data.VendorID;
					if (flag === true) {
						if (flagNego === true) {
							if (index != 0 && data.TenderItemize == false) {
								//if (localStorage.getItem("currLang") === 'id') {
								//	var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Alasan negosiasi selain best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*jika klik OK, maka secara otomatis kirim approval ke L1 untuk negosiasi selain best bidder</div></div></form>');
								//} else if (localStorage.getItem("currLang") === 'en') {
								//	var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Reasons for Negotiation with non-best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*If click OK, then it will automatically send approval to L1 for negotiation with non-best bidder</div></div></form>');
								//}
								//bootbox.confirm(form, function (yes) {
									//if (yes) {
										//vm.Remark = form.find('textarea[name=usernameInput]').val();
										//if (vm.Remark == "") {
										//	UIControlService.msg_growl('error', "MESSAGE.REASON_CANT_EMPTY");
										//	return false;
										//} else {
											UIControlService.loadLoadingModal("MESSAGE.LOADING");
											var datainsert = {
												VendorID: data.VendorID,
												TenderStepDataID: vm.StepID,
												IsNego: true,
												IsNeedApproval: false,
												Remark: ""
											}
											NegosiasiBarangService.insert(datainsert, function (reply) {
												if (reply.status === 200) {
													UIControlService.unloadLoading();
													vm.GoodsNegoId = reply.data.ID;
													init();
													location.reload();
													//sendApprovalNego();
												}
											}, function (err) {
												// UIControlService.msg_growl("error", "Gagal Akses Api!!");
											});
										//}
									//}
								//});
							} else {
								//bootbox.confirm($filter('translate')('MESSAGE.SURE_TO_AGREE'), function (yes) {
									//if (yes) {
										UIControlService.loadLoadingModal("MESSAGE.LOADING");
										var datainsert = {
											VendorID: data.VendorID,
											TenderStepDataID: vm.StepID,
											IsNego: true,
											IsNeedApproval: false
										}
										NegosiasiBarangService.insert(datainsert, function (reply) {
											if (reply.status === 200) {
												UIControlService.unloadLoading();
												init();
												location.reload();
												// $state.transitionTo('detail-penawaran', { VendorID: vm.VendorID, StepID: vm.StepID });
											}
										}, function (err) {
											// UIControlService.msg_growl("error", "Gagal Akses Api!!");
										});
									//}
								//});
							}
						} else {
							if (index != 0 && data.TenderItemize == false) {
								//if (localStorage.getItem("currLang") === 'id') {
								//	var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Alasan negosiasi selain best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*jika klik OK, maka secara otomatis kirim approval ke L1 untuk negosiasi selain best bidder</div></div></form>');
								//} else if (localStorage.getItem("currLang") === 'en') {
								//	var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Reasons for Negotiation with non-best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*If click OK, then it will automatically send approval to L1 for negotiation with non-best bidder</div></div></form>');
								//}
								//bootbox.confirm(form, function (yes) {
									//if (yes) {
										//vm.Remark = form.find('textarea[name=usernameInput]').val();
										//if (vm.Remark == "") {
										//	UIControlService.msg_growl('error', "MESSAGE.REASON_CANT_EMPTY");
										//	return false;
										//} else {
											UIControlService.loadLoadingModal("MESSAGE.LOADING");
											var datainsert = {
												VendorID: data.VendorID,
												TenderStepDataID: vm.StepID,
												IsNego: false,
												IsNeedApproval: true,
												Remark: ""
											}
											NegosiasiBarangService.insert(datainsert, function (reply) {
												if (reply.status === 200) {
													UIControlService.unloadLoading();
													vm.GoodsNegoId = reply.data.ID;
													init();
													location.reload();
													//sendApprovalNego();
												}
											}, function (err) {
												// UIControlService.msg_growl("error", "Gagal Akses Api!!");
											});
										//}
									//}
								//});
							} else {
								//bootbox.confirm($filter('translate')('MESSAGE.SURE_TO_NEGO'), function (yes) {
									//if (yes) {
										UIControlService.loadLoadingModal("MESSAGE.LOADING");
										var datainsert = {
											VendorID: data.VendorID,
											TenderStepDataID: vm.StepID,
											IsNego: false
										}
										NegosiasiBarangService.insert(datainsert, function (reply) {
											if (reply.status === 200) {
												UIControlService.unloadLoading();
												vm.GoodsNegoId = reply.data.ID;
												sendMailNego(reply.data.ID);
												$state.transitionTo('negosiasi-barang-chat', { StepID: data.TenderStepDataID, VendorID: data.VendorID });
											} else {
												// UIControlService.msg_growl("error", "Gagal menyimpan data!!");
												return;
											}
										}, function (err) {
											// UIControlService.msg_growl("error", "Gagal Akses Api!!");
										});
									//}
								//});
							}
						}
					} else {
						if (data.IsNego == false) $state.transitionTo('detail-penawaran', { VendorID: vm.VendorID, StepID: vm.StepID });
						else $state.transitionTo('negosiasi-barang-chat', { StepID: data.TenderStepDataID, VendorID: data.VendorID, ProcPackType: vm.ProcPackType, TenderRefID: vm.TenderRefID });
					}

				}
			} else {
				if (data.IsNego == true) $state.transitionTo('detail-penawaran', { VendorID: data.VendorID, StepID: data.TenderStepDataID });
				else $state.transitionTo('negosiasi-barang-chat', { StepID: data.TenderStepDataID, VendorID: data.VendorID, ProcPackType: vm.ProcPackType, TenderRefID: vm.TenderRefID });
			}
		}

		//vm.sendApprovalNego = sendApprovalNego;
		//function sendApprovalNego() {
		//	var datainsert = {
		//		GoodsNegoId: vm.GoodsNegoId
		//	}
		//	NegosiasiBarangService.InsertApproval(datainsert, function (reply) {
		//		if (reply.status === 200) {
		//			UIControlService.msg_growl("success", "MESSAGE.SEND_APPROVAL");
		//			sendMailToApprover();
		//			init();
		//		} else {
		//			//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
		//			return;
		//		}
		//	}, function (err) {
		//		//UIControlService.msg_growl("error", "Gagal Akses Api!!");
		//	});

		//}

		vm.sendMailToApprover = sendMailToApprover;
		function sendMailToApprover() {
			var datainsert = {
				ID: vm.GoodsNegoId
			}
			NegosiasiBarangService.sendMailToApprover(datainsert, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SEND_EMAIL");
				} else {
					//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "Gagal Akses Api!!");
			});
		}

		vm.sendMailNego = sendMailNego;
		function sendMailNego(negoId) {
			var datainsert = {
				ID: negoId
			}
			NegosiasiBarangService.sendMail(datainsert, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SEND_EMAIL");
				} else {
					//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "Gagal Akses Api!!");
			});

		}

		vm.detailApproval = detailApproval;
		function detailApproval(dt) {
			var item = {
				ID: dt.ID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/negosiasi-barang/detailApproval.modal.html',
				controller: 'detailApprovalGoodsNegoCtrl',
				controllerAs: 'detailApprovalGoodsNegoCtrl',
				resolve: { item: function () { return item; } }
			});
		};


		vm.print = print;
		function print() {
			$state.transitionTo('negosiasi-barang-print', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
		}

		vm.endnego = endnego;
		function endnego(data) {
			bootbox.confirm($filter('translate')('MESSAGE.END_NEGO'), function (yes) {
				if (yes) {
					NegosiasiBarangService.editactive({
						ID: data.ID,
						IsActive: false
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
							init();
						} else {
							//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
							return;
						}
					}, function (err) {
						//UIControlService.msg_growl("error", "Gagal Akses Api!!");
					});
				}
			});
		}

		vm.reOpen = reOpen;
		function reOpen(data) {
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REOPEN_NEGO'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("");
					NegosiasiBarangService.CanReOpenNego({
						ID: data.ID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.data === true) {
							UIControlService.loadLoading("");
							NegosiasiBarangService.ReOpenNegotiation({
								ID: data.ID
							}, function (reply) {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REOPEN_NEGO'));
								sendMailNego(data.ID);
								init();
							}, function (error) {
								UIControlService.unloadLoading();
								UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
							});
						} else {
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_MAX_NEGO'));
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
					});
				}
			});
		}

		vm.history = history;
		function history(data) {
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/negosiasi-barang/negosiasi-barang-history.html',
		        controller: 'HistoryBarangCtrl',
		        controllerAs: 'HistoryBarangCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}
	}
})();
//TODO


