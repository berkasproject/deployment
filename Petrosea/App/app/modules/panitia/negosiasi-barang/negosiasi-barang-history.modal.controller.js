﻿(function () {
    'use strict';

    angular.module("app").controller("HistoryBarangCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiBarangService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.TenderStepID = item.TenderStepDataID;
        vm.VendorID = item.VendorID;
        vm.ID = item.ID;
        vm.VendorName = item.VendorName;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        console.log(item)
        function init() {
            $translatePartialLoader.addPart('negosiasi');
            //console.info(item);
            jLoad(1);
        }
        
        
        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            NegosiasiBarangService.selectHistory({
                VendorID: vm.VendorID,
                TenderStepDataID: vm.TenderStepID,
                ID: vm.ID,
                Offset: offset,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataHistory = reply.data;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                // console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        
        
        /*
        vm.backpengadaan = backpengadaan;
        function backpengadaan() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType });
        }
        */

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();
//TODO


