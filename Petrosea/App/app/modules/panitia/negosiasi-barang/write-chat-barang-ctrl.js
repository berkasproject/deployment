﻿(function () {
	'use strict';

	angular.module("app").controller("WriteChatBarangCtrl", ctrl);

	ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService'];
	function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiBarangService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance, UploadFileConfigService, UploaderService) {

		var vm = this;
		vm.vendor = item.VendorID;
		vm.step = item.StepID;
		vm.NegoId = item.NegoId;
		vm.judule = item.Judul;
		vm.gni = item.GoodsNegoId;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.tender = {};
		vm.fileUpload = null;

		// vm.jLoad = jLoad;

		function init() {
			vm.isInformByEmail = true
			UIControlService.loadLoading("MESSAGE.LOADING");
			$translatePartialLoader.addPart("negosiasi-barang");
			getTypeSizeFile()
			jLoad(1);
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.ADMIN.GOODSNEGOTIATION", function (response) {
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
				return;
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			console.info("curr " + current)
			vm.nego = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			var tend = {
				VendorID: item.VendorID,
				TenderStepDataID: item.StepID
			}
			NegosiasiBarangService.bychat(tend, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.nego = reply.data;
					//vm.judule = vm.nego[0].TenderName;
					//vm.idtender = vm.nego[0].TenderStepID;
					// console.info("judul:" + JSON.stringify(vm.idtender));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Chatting" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.selectUpload = selectUpload;
		function selectUpload(fileUpload) {
			vm.fileUpload = fileUpload;
		}

		function uploadFile() {
			var folder = 'NEGOTIATION_GOODS';
			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder);
			}
		}

		function upload(file, config, filters, folder) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoadingModal("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, folder, function (response) {
				console.info()
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					var url = response.data.Url;
					UIControlService.msg_growl("success", "MESSAGE.SUCC_UPLOAD");
					processsave(url);

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD")
				UIControlService.unloadLoadingModal();
			});

		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.simpan = simpan;
		function simpan() {
			if (!vm.fileUpload) {
				processsave(null);
			} else {
				uploadFile();
			}
		}

		function processsave(url) {
			var data = {
				GoodsNegoId: vm.gni,
				VendorID: vm.vendor,
				Description: vm.isi,
				UrlDoc: url,
				IsInformByEmail: vm.isInformByEmail,
				TenderStepID: item.StepID
			};
			NegosiasiBarangService.insertchat(data, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
					$uibModalInstance.close();

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoadingModal();
			});
		}

		/*
        vm.backpengadaan = backpengadaan;
        function backpengadaan() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType });
        }
        */

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}

	}
})();
//TODO


