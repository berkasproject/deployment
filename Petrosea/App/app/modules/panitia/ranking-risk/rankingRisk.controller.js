(function () {
	'use strict';

	angular.module("app").controller("RankingRiskCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'RankingRiskService', 'UIControlService','$filter'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, RankingRiskService, UIControlService,$filter) {

		var vm = this;
		vm.init = init;

		vm.procTypes = [];
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.isCalendarOpened = [false, false, false, false];

		vm.data = [];
		vm.dataVendor = [];
		vm.dataMain = [];
		function init() {
		    $translatePartialLoader.addPart('ranking-risk');
		    konfigurasiWaktu();
		    //dataWorkloadProc();
		    //grafikNewVendor();
		}


		vm.filter = filter;
		function filter() {
		    var invalidFilter = false;
		    if (vm.durasi == null) {
		        invalidFilter = true;
		    }
		    else {
		        if (vm.durasi.Value == 0) { //minggu
		            if (vm.ma == null || vm.mb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ma.NumberOfWeek > vm.mb.NumberOfWeek) {
		                    invalidFilter = true;
		                }
		                else {
		                    vm.batasAtas = vm.ma.NumberOfWeek;
		                    vm.batasBawah = vm.mb.NumberOfWeek;
		                }
		            }
		        }
		        else if (vm.durasi.Value == 1) { //bulan         
		            if (vm.ba == null || vm.bb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ba > vm.bb) {
		                    invalidFilter = true;
		                }
		                else {
		                    var intBatasAtas = vm.ba.getMonth();
		                    var intBatasBawah = vm.bb.getMonth();
		                    vm.batasAtas = intBatasAtas + 1;
		                    vm.batasBawah = intBatasBawah + 1;
		                }
		            }

		        }
		    }
		    if (invalidFilter == true) {
		        UIControlService.msg_growl("warning", "MESSAGE.INVALID_FILTER");
		        return;
		    }
		    else if (invalidFilter == false) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        dataReport();
		        dataReportVendor(1);
		    }
		}

		vm.cekFilterTahun = cekFilterTahun;
		function cekFilterTahun() {
		    vm.tahun = vm.filterTahun.getFullYear();
		    if (vm.durasi.Value == 0) {
		        weekByYear();
		    }
		    else if (vm.durasi.Value == 1) {
		        var mindateNow = new Date("January 01, " + vm.tahun + " 00:00:00");
		        var maxdateNow = new Date("December 31, " + vm.tahun + " 23:59:59");
		        vm.datepickeroptions = {
		            minMode: 'month',
		            maxDate: maxdateNow,
		            minDate: mindateNow
		        }
		        vm.ba = mindateNow;
		        vm.bb = maxdateNow;
		    }
		}


		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		vm.konfigurasiWaktu = konfigurasiWaktu;
		function konfigurasiWaktu() {
		    vm.datenow = new Date();
		    vm.getYearNow = vm.datenow.getFullYear();
		    var mindateNow = new Date("January 01, " + vm.getYearNow + " 00:00:00");
		    var maxdateNow = new Date("December 31, " + vm.getYearNow + " 23:59:59");
		    //mindateNow.setYear(vm.getYearNow - 1);
		    vm.datepickeroptions = {
		        minMode: 'month',
		        maxDate: maxdateNow,
		        minDate: mindateNow
		    }
		    vm.datepickeroptionsTahun = {
		        minMode: 'year'
		    }
		    if (localStorage.getItem("currLang") === 'id' || localStorage.getItem("currLang") === 'ID') {
			    vm.ddDurasi =
	            [
	                { Value: 0, Name: "Minggu" },
	                { Value: 1, Name: "Bulan" }

	            ]
	        }
		    else if (localStorage.getItem("currLang") === 'en' || localStorage.getItem("currLang") === 'EN') {
	        	vm.ddDurasi =
	            [
	                { Value: 0, Name: "Week" },
	                { Value: 1, Name: "Month" }

	            ]
	        }
		    //vm.jenistender = vm.ddTender[0];
		    vm.durasi = vm.ddDurasi[0];
		    vm.filterTahun = vm.datenow;
		    vm.tahun = vm.getYearNow;
		    weekByYear();
		}

		function weekByYear() {
		    RankingRiskService.weekByYear({
		        column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.weeks = reply.data;
		            //console.info("weeks" + JSON.stringify(vm.weeks));
		            for (var i = 0; i <= vm.weeks.length - 1; i++) {
		                vm.weeks[i].label = "Week #" + vm.weeks[i].NumberOfWeek + " (" + UIControlService.convertDate(vm.weeks[i].StartDate) + " s/d " + UIControlService.convertDate(vm.weeks[i].EndDate) + ")";
		            }
		            vm.ma = vm.weeks[0];
		            vm.mb = vm.weeks[vm.weeks.length - 1];
		            vm.batasAtas = vm.ma.NumberOfWeek;
		            vm.batasBawah = vm.mb.NumberOfWeek;
		            if (vm.dataMain.length == 0) {
		                dataReportVendor(1);
		                dataReport();
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
		        UIControlService.unloadLoading();
		    });
		}
		vm.dataReportVendor = dataReportVendor;
		function dataReportVendor(current) {
		    vm.currentPage = current;
		    var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    RankingRiskService.datavendor({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Status: vm.durasi.Value,
		        Offset: offset,
		        Limit: vm.maxSize
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            vm.dataVendor = reply.data;
		            vm.listData = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("data" + JSON.stringify(vm.listData));
		            //grafik(vm.data);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		function dataReport() {
		    //vm.currentPage = current;
		    //var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
		    RankingRiskService.rankingRisk({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.dataMain = reply.data;
		            vm.totalvendor = 0;
		            vm.totalLocal = 0;
		            vm.totalNational = 0;
		            vm.totalInternational = 0;
		            console.info("data main:" + JSON.stringify(vm.dataMain));
		            for (var i = 0; i <= vm.dataMain.length - 1; i++) {
		                vm.totalvendor = vm.dataMain[i].Total + vm.totalvendor;
		                vm.totalLocal = vm.dataMain[i].Local + vm.totalLocal;
		                vm.totalNational = vm.dataMain[i].National + vm.totalNational;
		                vm.totalInternational = vm.dataMain[i].International + vm.totalInternational;
		            }
		            grafikRisk(vm.dataMain);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		function grafikRisk(param) {
		    vm.lokal = []; vm.nasional = []; vm.internasional = [];
		    vm.label = []; vm.total = [];
		    for (var i = 0; i <= param.length-1; i++) {
		        //console.info("Goods ke-" + i + ": " + param[i].Goods);
		        vm.total[i] = param[i].Total;
		        vm.lokal[i] = param[i].Local;
		        vm.nasional[i] = param[i].National;
		        vm.internasional[i] = param[i].International;
		        vm.label[i] = $filter('translate')(param[i].RiskType.Name);
		    }
		    //console.info("total" + JSON.stringify(vm.total));
		    var maxvalue = Math.max.apply(Math, vm.total);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(0);
		    }
		    else {
		        maxvalue = 10;
		    }
		    vm.colours = ['#8FBC8F', '#DC143C', '#3498DB'];
		    vm.dataRisk = [
                vm.lokal, vm.nasional, vm.internasional
		    ]
		    if (localStorage.getItem("currLang") === 'id' || localStorage.getItem("currLang") === 'ID') {
		        vm.dataset = [{
		            label: "Lokal"
		        }, { label: "Nasional" }, { label: "Internasional" }];
		        vm.series = ['Lokal', 'Nasional', 'Internasional'];

		        vm.option = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Kategori'
		                    }
		                }]
		            }
		        };
		    }
		    else {
		        vm.dataset = [{
		            label: "Local"
		        }, { label: "National" }, { label: "International" }];
		        vm.series = ['Local', 'National', 'International'];

		        vm.option = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Vendor Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Category'
		                    }
		                }]
		            }
		        };
		    }
		}

		vm.exportPdf = exportPdf;
		function exportPdf() {
		    $state.transitionTo('print-preview-ranking-risk', { tahun: vm.tahun, batasAtas:vm.batasAtas,batasBawah:vm.batasBawah,durasi:vm.durasi.Value, totalItems: vm.totalItems });
		}

		vm.exportExcel = exportExcel;
		function exportExcel() {
		    RankingRiskService.datavendor({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Status: vm.durasi.Value,
		        Offset: 0,
		        Limit: vm.totalItems
		    }, function (reply) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        if (reply.status === 200) {
		            vm.dataAllVendor = reply.data;
		            vm.listAllData = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            vm.filterdata = [];
		            //console.info("total semua data:" + JSON.stringify(vm.alldata.length));
		            for (var i = 0; i < vm.listAllData.length; i++) {
		                //UIControlService.loadLoading("MESSAGE.LOADING");
		                var complianceDate = '';
		                if ($filter('translate')(vm.listAllData[i].CategoryVendorData.Name) == 'Low Risk' || $filter('translate')(vm.listAllData[i].CategoryVendorData.Name) == 'Medium Risk') {
		                    complianceDate = 'N/A';
		                }
		                if (vm.listAllData[i].ApprovalComplianceDate != null) {
		                    complianceDate = UIControlService.getStrDate(vm.listAllData[i].ApprovalComplianceDate);
		                }
		                var forExcel = {
                            No:i+1,
		                    Nama: vm.listAllData[i].VendorName,
		                    RankingRisk: $filter('translate')(vm.listAllData[i].CategoryVendorData.Name),
		                    TanggalAktivasi:UIControlService.getStrDate(vm.listAllData[i].ActivedDate),
		                    ApprovalComplianceDate: complianceDate,
		                    Area: $filter('translate')(vm.listAllData[i].Area)

		                }
		                vm.filterdata.push(forExcel);
		            }
		            //console.info("dataForExcel:" + JSON.stringify(vm.filterdata));
		            JSONToCSVConvertor(vm.filterdata, true);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });

		}

		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ShowLabel) {
		    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		    var arrData = JSONData;
		    //console.info(arrData[0]);
		    var CSV = '';

		    //This condition will generate the Label/Header
		    if (ShowLabel) {
		        var row = "sep=," + '\n';

		        //This loop will extract the label from 1st index of on array
		        for (var index in arrData[0]) {

		            //Now convert each value to string and comma-seprated
		            row += index + ',';
		        }

		        row = row.slice(0, -1);
		        //console.info(row);
		        //append Label row with line break
		        CSV += row + '\r\n';
		        //console.info(CSV);
		    }

		    //1st loop is to extract each row
		    for (var i = 0; i < arrData.length; i++) {
		        var row = "";

		        //2nd loop will extract each column and convert it in string comma-seprated
		        for (var index in arrData[i]) {
		            row += '"' + arrData[i][index] + '",';
		        }

		        row.slice(0, row.length - 1);

		        //add a line break after each row
		        CSV += row + '\r\n';
		    }

		    if (CSV == '') {
		        alert("Invalid data");
		        return;
		    }

		    //Generate a file name
		    var fileName = "Data Ranking Risk_" + UIControlService.getStrDate(new Date());

		    //Initialize file format you want csv or xls
		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

		    // Now the little tricky part.
		    // you can use either>> window.open(uri);
		    // but this will not work in some browsers
		    // or you will not get the correct file extension    

		    //this trick will generate a temp <a /> tag
		    var link = document.createElement("a");
		    link.href = uri;

		    //set the visibility hidden so it will not effect on your web-layout
		    link.style = "visibility:hidden";
		    link.download = fileName + ".csv";

		    //this part will append the anchor tag and remove it after automatic click
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
		}

	}
})();
