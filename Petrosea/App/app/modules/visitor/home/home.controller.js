﻿(function () {
	'use strict';

	angular.module("app").controller("HomeController", ctrl);

	ctrl.$inject = ['$scope', '$http', '$translate', '$translatePartialLoader', '$location', 'NewsService', '$state'];
	/* @ngInject */
	function ctrl($scope, $http, $translate, $translatePartialLoader, $location, NewsService, $state) {

		/* jshint validthis: true */
		var vm = this;

		// bindable variables
		vm.panels = [];
		vm.activeSlide = 0;
		vm.news = [];

		// functions
		vm.initialize = initialize;
		vm.navigate = goToSlide;

		// function declarations
		function initialize() {

			// Load partial traslastion
			$translatePartialLoader.addPart('home');
			vm.panels = [1, 2, 3];
			loadNews();
		}

		function loadNews() {
		    NewsService.getFrontNews(function (response) {
		        //console.info("news" + JSON.stringify(response));
				vm.news = response.data;
				//response = response.data;
				//if (response.status == 200) {
				//    vm.news = response.result.data;
				//} else {

				//}
			},
			function (error) {

			});
		}

		function goToSlide(index) {
			vm.activeSlide = index;
		}
		vm.gotofrontnews = gotofrontnews;
		function gotofrontnews(newsid) {
		    console.info("newsid" + newsid);
		    $state.transitionTo('frontnews', { id:newsid });
		}

		// services and events



	}
})();