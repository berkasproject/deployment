﻿(function () {
	'use strict';

	angular.module("app").controller("prequalAnnouncementVisitorCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'UIControlService'];
	function ctrl($translatePartialLoader, UIControlService) {
		var vm = this;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('pengumuman-prakual-client');
			vm.loadData(1);
		}

		vm.loadData = loadData;
		function loadData(current) {
			UIControlService.loadLoading("Silahkan Tunggu...");
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			PrequalAnnounceVendorService.getByOpenPrequal({
				Offset: offset,
				Limit: vm.maxSize,
				Keyword: vm.txtSearch
			}, function (reply) {
				//console.info("datane:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.endDateHours = [];
					vm.endDateMinutes = [];
					var data = reply.data;
					vm.listPengumuman = data.List;
					var tglhariini = new Date();
					tglhariini.setHours(23);
					tglhariini.setMinutes(59);
					tglhariini.setSeconds(0);
					for (var i = 0; i < vm.listPengumuman.length; i++) {
						vm.listPengumuman[i]['expired'] = false;
						vm.listPengumuman[i].TenderStepData.ConvertedStartDate = UIControlService.getStrDate(vm.listPengumuman[i].TenderStepData.StartDate);
						vm.listPengumuman[i].TenderStepData.ConvertedEndDate = UIControlService.getStrDate(vm.listPengumuman[i].TenderStepData.EndDate);
						vm.listPengumuman[i]['masa_berlaku'] = Math.floor(hitungSelisihHari(tglhariini, vm.listPengumuman[i].TenderStepData.ConvertedEndDate));

						if (vm.listPengumuman[i].masa_berlaku < 0) {
							vm.endDateHours[i] = new Date(vm.listPengumuman[i].TenderStepData.EndDate).getHours();
							vm.endDateMinutes[i] = new Date(vm.listPengumuman[i].TenderStepData.EndDate).getMinutes();
							if (vm.endDateHours[i] === 23 && vm.endDateMinutes[i] === 59) {
								vm.listPengumuman[i].expired = true;
							}
						}
					}
					vm.totalItems = Number(data.Count);
				} else {
					UIControlService.msg_growl("error", "Gagal mendapatkan data Master Badan Usaha");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "Gagal akses API");
				UIControlService.unloadLoading();
			});
		}
	}
})();