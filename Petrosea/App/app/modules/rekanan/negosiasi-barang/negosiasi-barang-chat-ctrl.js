﻿(function () {
    'use strict';

    angular.module("app").controller("NegosiasiBarangChatVCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiBarangService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.StepID = Number($stateParams.StepID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.maxSize = 10;
        vm.init = init;
        vm.ProcPackType = 0;
        vm.jLoad = jLoad;
        vm.nb = [];

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
            //UIControlService.loadLoading("Silahkan Tunggu...");
            //loadTender();
            jLoad(1);
            loadSteps();
            $translatePartialLoader.addPart("negosiasi");

        }
        vm.loadSteps = loadSteps;
        function loadSteps() {
            NegosiasiBarangService.GetSteps({
                ID: vm.StepID
            }, function (reply) {
                vm.step = reply.data;
                for (var i = 0; i < vm.step.length; i++) {
                    if (vm.step[i].step.FormTypeURL == "negosiasi-barang") {
                        vm.IsOvertime = vm.step[i].IsOvertime;
                    }
                }
                UIControlService.unloadLoading();
            }, function (error) {
            });
        };

        //tampil isi chat
       vm.jLoad = jLoad;
        function jLoad(current) {
            vm.nego = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            var tender = {
                TenderStepDataID: vm.StepID
            }
            NegosiasiBarangService.bychatv(tender, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.nego = reply.data;
                    vm.nego.forEach(function (data) {
                        data.ChatDate = UIControlService.convertDateTime(data.ChatDate);
                    });
                    vm.VendorName = reply.data[0].VendorName;
                    vm.judul = reply.data[0].TenderName;
                    vm.idnb = vm.nego[0].GoodsNegoId
                    vm.TenderID = vm.nego[0].TenderID;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        //ke halaman itemall
        vm.detailpenawaran = detailpenawaran;
        function detailpenawaran(flag) {
            $state.transitionTo('detail-penawaran-vendor', { StepID: vm.StepID });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('detail-tahapan-vendor', { TenderID: vm.TenderID });
        }
        //tulis, insertchat
        vm.tulis = tulis;
        function tulis() {
            var data = {
                StepID: vm.StepID,
                Judul: vm.judul,
                GoodsNegoId: vm.idnb
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/negosiasi-barang/write-chat-barang.html?v=1.000002',
                controller: 'WriteChatBarangVCtrl',
                controllerAs: 'WriteChatBarangVCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

    }
})();
//TODO


