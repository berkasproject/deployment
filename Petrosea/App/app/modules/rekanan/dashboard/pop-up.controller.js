(function () {
	'use strict';

	angular.module("app").controller("PopUpAcknowledgementCtrl", ctrl);

	ctrl.$inject = ['$filter', 'item', '$state', '$uibModal', '$translatePartialLoader', 'DashboardVendorService', 'PengumumanPengadaanService', 'UIControlService', '$window', '$stateParams', '$uibModalInstance'];

	/* @ngInject */
	function ctrl($filter, item, $state, $uibModal, $translatePartialLoader, DashboardVendorService, PengumumanPengadaanService, UIControlService, $window, $stateParams, $uibModalInstance) {
		var vm = this;
		vm.paketCSO = {}
		var loadmsg = "MESSAGE.LOADING";
		var loadingCount;

		vm.monpoid = item.monitoringPOID;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("dashboard-vendor");
			$translatePartialLoader.addPart("permintaan-ubah-data");
			UIControlService.loadLoading(loadmsg);
			//cekAcknowledgement();
		}

		vm.initContract = initContract;
		function initContract() {
			$translatePartialLoader.addPart("dashboard-vendor");
			$translatePartialLoader.addPart("permintaan-ubah-data");
			//cekAcknowledgement();
		}

		vm.paketCSO.list = item.list.filter(pendingAck)

		function pendingAck(dataPending) {
			return dataPending.StatusName === "REVIEW_VENDOR" || dataPending.SysReference.Name === 'REVIEW_VENDOR'
		}

		function cekAcknowledgement() {
			DashboardVendorService.cekAcknowledgement({
			}, function (reply) {
				unloadLoading();
				vm.monitoringPOID = reply.data;
				if (vm.monitoringPOID != 0) {
					popUP(vm.monitoringPOID);
				}
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		function popUP(id) {
			//$window.onbeforeunload = vm.onExit;
			var data = {
				monitoringPOID: id
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'popup.html',
				controller: 'DashboardVendorCtrl',
				controllerAs: 'DVCtrl',
				resolve: {
					item: function () { return data; }
				},
				backdrop: 'static',
				keyboard: false
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.gotoAcknowledgement = gotoAcknowledgement;
		function gotoAcknowledgement() {
			//console.info("monitoringPOID" + vm.monpoid);
			$state.transitionTo('detail-monitoring-po', { ID: vm.monpoid });
			$uibModalInstance.close();
		}

		vm.closePopUp = closePopUp
		function closePopUp() {
			$uibModalInstance.close();
		}

		vm.acknowledgeFPA = acknowledgeFPA
		function acknowledgeFPA(data) {
			bootbox.confirm($filter('translate')('CONFIRM_ACKNOWLEDGE'), function (res) {
				if (res) {
					DashboardVendorService.acknowledgeFPA({
						ID: data.ID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_ACKNOWLEDGE");

							var paketData = vm.paketCSO.list.indexOf(data)

							if (vm.paketCSO.list[paketData].StatusName == null)
								vm.paketCSO.list[paketData].SysReference.Name = ''
							else
								vm.paketCSO.list[paketData].StatusName = ''

							//loadMonitoring(vm.paketFPA.currentPage);
							//loadMonitoringVHS(vm.paketMRKO.currentPage);
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.FAIL_ACKNOWLEDGE");
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.acknowledgeContract = acknowledgeContract
		function acknowledgeContract(data) {
			bootbox.confirm($filter('translate')('CONFIRM_ACKNOWLEDGE'), function (res) {
				if (res) {
					DashboardVendorService.acknowledgeContract({
						ID: data.ID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCC_ACKNOWLEDGE");
							var paketData = vm.paketCSO.list.indexOf(data)
							vm.paketCSO.list[paketData].StatusName = ''
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.FAIL_ACKNOWLEDGE");
						UIControlService.unloadLoading();
					});
				}
			});
		}

		function loadCSO(current) {
			vm.currentPageCSO = current;
			DashboardVendorService.getDataCSO({
				Offset: vm.paketCSO.pageSize * (vm.currentPageCSO - 1),
				Limit: vm.paketCSO.pageSize,
				Keyword: vm.paketCSO.keyword
			}, function (reply) {
				unloadLoading();
				vm.paketCSO.list = reply.data.List;
				vm.paketCSO.totalItems = reply.data.Count;
				calculateTotalTenderAward();
				//console.log("warning: " + JSON.stringify(vm.warning.list));
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_WARNING');
			});
		}

		vm.gotoAcknowledgementFPA = gotoAcknowledgementFPA
		function gotoAcknowledgementFPA() {
			$uibModalInstance.close();
		}
		/*
		vm.onExit = function () {
		    return ('bye bye');
		    event.preventDefault();
		    return;
		};

		$window.onbeforeunload = vm.onExit;
        */
	}

})();

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}