﻿(function () {
    'use strict';

    angular.module("app")
    .controller("approveVendorCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$state', 'item', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'DashboardVendorService', 'UIControlService', '$window', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    /* @ngInject */
    function ctrl($http, $filter, $state, item, $uibModal, $translate, $translatePartialLoader, $location, DashboardVendorService, UIControlService, $window, $stateParams, GlobalConstantService, $uibModalInstance) {
        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var loadingCount;
        vm.totalItems = 0;
        vm.currentPage = 0;
        vm.pageSize = 5;
        vm.penerimaChat = item.penerimaChat;
        

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("dashboard-vendor");
            $translatePartialLoader.addPart("permintaan-ubah-data");
            //UIControlService.loadLoading(loadmsg);
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal(loadmsg);

            if (vm.review == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.REVIEW');
                return;
            }
            DashboardVendorService.insertApproveVendor({
                Review: vm.review,
                ForumID: Number(item.ForumID)
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();
                }
            }, function (error) {
                unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();