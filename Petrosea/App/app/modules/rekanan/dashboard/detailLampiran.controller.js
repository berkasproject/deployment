﻿(function () {
    'use strict';

    angular.module("app")
    .controller("LampiranCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$state', 'item','$uibModal', '$translate', '$translatePartialLoader', '$location', 'DashboardVendorService', 'UIControlService', '$window', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    /* @ngInject */
    function ctrl($http, $filter, $state, item, $uibModal, $translate, $translatePartialLoader, $location, DashboardVendorService, UIControlService, $window, $stateParams, GlobalConstantService, $uibModalInstance) {
        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var loadingCount;
        vm.totalItems = 0;
        vm.currentPage = 0;
        vm.pageSize = 5;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("dashboard-vendor");
            //UIControlService.loadLoading(loadmsg);
            loadLampiran(1);
        }

        function loadLampiran(current) {
            vm.currentPage = current;
            var offset = (current * 5) - 5;
            DashboardVendorService.getAttachment({
                IntParam1: Number(item.CMID),
                Offset: offset,
                Limit: vm.pageSize
            }, function (reply) {
                vm.lampiran = reply.data.List;
                vm.totalItems = reply.data.Count;
            }, function (error) {
                unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
            });
        }

        vm.view = view;
        function view(data) {
            var data = {
                data: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/dashboard/viewLampiran.html',
                controller: 'viewLampiranCtrl',
                controllerAs: 'viewLampiranCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                },
                backdrop: 'static'
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}