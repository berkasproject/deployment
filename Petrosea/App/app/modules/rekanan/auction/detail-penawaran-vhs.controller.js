﻿(function () {
    'use strict';
    
    angular.module("app").controller("detPenawaranVhsCtrl", ctrl);

    ctrl.$inject = ['UIControlService', '$uibModalInstance', 'item', '$translatePartialLoader', 'AuctionService', 'NegosiasiService'];

    function ctrl(UIControlService, $uibModalInstance, item, $translatePartialLoader, AuctionService, NegosiasiService) {
        var vm = this;
        vm.init = init;

        vm.tenderID = item.TenderID;
        vm.tenderName = item.TenderName;
        vm.vendorID = item.VendorID;
        vm.procPackType = item.ProcPackType;
        vm.tenderRefID = item.TenderRefID;

        vm.totalHargaRekanan = 0;

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;

        function init() {
            //UIControlService.loadLoadingModal('MESSAGE.LOADING');
            $translatePartialLoader.addPart('auction');
            itemAll();
            //jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            AuctionService.getVOEDetails({
                VendorID: vm.vendorID,
                status: vm.procPackType,
                TenderID: vm.tenderID,
                Offset: offset,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.offerEntry = reply.data.List;
                    var jum = reply.data;
                    vm.totalItems = Number(jum.Count);
                    vm.maxSize = vm.totalItems;
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
            });
        }

        function itemAll() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            
            AuctionService.getVOEDetails({
                VendorID: vm.vendorID,
                status: vm.procPackType,
                TenderID: vm.tenderID,
                Offset: 0,
                Limit: 0
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.offerEntry = reply.data.List;
                    vm.totalItems = reply.data.Count;
                    vm.offerEntry.forEach(function (sub) {
                        vm.totalHargaRekanan += sub.TotalPriceVOE;
                    });
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
            });
        }

        vm.generate = generate;
        function generate() {
            if (vm.isGenerate == true) {
                if (vm.hargaPenawaran == undefined || vm.hargaPenawaran == "" || vm.hargaPenawaran == 0) {
                    vm.isGenerate = false;
                    UIControlService.msg_growl("error", 'Masukkan harga penawaran terlebih dahulu');
                    return;
                }
                calculatePriceAuction();
            } else {
                vm.offerEntry.forEach(function (sub) {
                    sub.PrecentageAuction = null;
                });
            }
        }

        function calculatePriceAuction() {
            vm.totalHargaAuction = 0;
            for (var i = 0; i < vm.offerEntry.length; i++) {
                //persen unit auction
                vm.percent = (vm.offerEntry[i].TotalPriceVOE / vm.totalHargaRekanan) * 100;
                vm.percent = vm.percent.toFixed(2)
                console.info(vm.percent)

                //harga total unit auction
                vm.totalPriceAuction = (vm.percent / 100) * vm.hargaPenawaran;
                vm.totalPriceAuction = Math.round(vm.totalPriceAuction)

                //harga unit auction
                vm.unitPriceAuction = vm.totalPriceAuction / vm.offerEntry[i].Quantity;
                vm.unitPriceAuction = Math.round(vm.unitPriceAuction)

                vm.offerEntry[i].PrecentageAuction = vm.percent;
                vm.offerEntry[i].UnitPriceAuction = vm.unitPriceAuction;
                vm.offerEntry[i].TotalPriceAuction = vm.totalPriceAuction;

                vm.totalHargaAuction += vm.offerEntry[i].TotalPriceAuction;
            }
            console.info(vm.offerEntry);
        }

        vm.manualInput = manualInput;
        function manualInput(data) {
            data.TotalPriceAuction = data.UnitPriceAuction * data.Quantity;

            //vm.totalHargaAuction += data.TotalPriceAuction;
            vm.offerEntry.forEach(function (sub) {
                vm.totalHargaAuction += sub.TotalPriceAuction;
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()