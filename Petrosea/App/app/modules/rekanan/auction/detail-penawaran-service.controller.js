﻿(function () {
    'use strict';
    
    angular.module("app").controller("detPenawaranServiceCtrl", ctrl);

    ctrl.$inject = ['UIControlService', '$uibModalInstance', 'item', '$translatePartialLoader', 'AuctionService', 'NegosiasiService'];

    function ctrl(UIControlService, $uibModalInstance, item, $translatePartialLoader, AuctionService, NegosiasiService) {
        var vm = this;
        vm.init = init;

        vm.tenderID = item.TenderID;
        vm.tenderName = item.TenderName;
        vm.vendorID = item.VendorID;
        vm.procPackType = item.ProcPackType;
        vm.tenderRefID = item.TenderRefID;

        vm.totalHargaRekanan = 0;
        vm.totalHargaAuction = 0;
        vm.isGenerate = false;

        vm.currentPage = 1;
        vm.pageSize = 10;

        function init() {
            //UIControlService.loadLoadingModal('MESSAGE.LOADING');
            $translatePartialLoader.addPart('auction');
            jLoad();
        }

        vm.jLoad = jLoad;
        function jLoad() {
            loadSOEDetails();
        }

        function loadSOEDetails() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            NegosiasiService.selectVendor({
                VendorID: vm.vendorID,
                TenderRefID: vm.tenderRefID,
                ProcPackType: vm.procPackType
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.detail = reply.data[0];
                    
                    NegosiasiService.selectLineCost({
                        VendorID: vm.vendorID,
                        TenderRefID: vm.tenderRefID,
                        ProcPackType: vm.procPackType,
                        CRCESubId: vm.detail.ContractRequisitionCESubID
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            vm.offerEntry = reply.data;
                            console.info(vm.offerEntry);
                            vm.offerEntry.forEach(function (sub) {
                                vm.totalHargaRekanan += sub.LineOfferCost;
                            });
                            //for (var i = 0; i < vm.offerEntry.List.length; i++) {
                            //    vm.totalHargaRekanan += vm.offerEntry.List[i].TotalPrice;
                            //    //console.info("Harga ke -" + i + " : " + vm.offerEntry.List[i].TotalPrice);
                            //}
                            //console.info("Total: "+vm.totalHargaRekanan);
                        } else {
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
                    });
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_API');
            });
        }

        vm.generate = generate;
        function generate() {
            if (vm.isGenerate == true) {
                if (vm.hargaPenawaran == undefined || vm.hargaPenawaran == "" || vm.hargaPenawaran == 0) {
                    vm.isGenerate = false;
                    UIControlService.msg_growl("error", 'Masukkan harga penawaran terlebih dahulu');
                    return;
                }
                calculatePriceAuction();
            } else {
                vm.offerEntry.forEach(function (sub) {
                    sub.PrecentageAuction = null;
                });
            }
        }

        function calculatePriceAuction() {
            vm.totalHargaAuction = 0;
            for (var i = 0; i < vm.offerEntry.length; i++) {
                //persen unit auction
                vm.percent = (vm.offerEntry[i].LineOfferCost / vm.totalHargaRekanan) * 100;
                vm.percent = vm.percent.toFixed(2)

                //harga total unit auction
                vm.totalPriceAuction = (vm.percent / 100) * vm.hargaPenawaran;
                vm.totalPriceAuction = Math.round(vm.totalPriceAuction)

                //harga unit auction
                vm.unitPriceAuction = vm.totalPriceAuction / vm.offerEntry[i].Quantity;
                vm.unitPriceAuction = Math.round(vm.unitPriceAuction)

                vm.offerEntry[i].PrecentageAuction = vm.percent;
                vm.offerEntry[i].UnitPriceAuction = vm.unitPriceAuction;
                vm.offerEntry[i].TotalPriceAuction = vm.totalPriceAuction;

                vm.totalHargaAuction += vm.offerEntry[i].TotalPriceAuction;
            }
        }

        vm.manualInput = manualInput;
        function manualInput(data) {
            data.TotalPriceAuction = data.UnitPriceAuction * data.item.Quantity;

            //vm.totalHargaAuction += data.TotalPriceAuction;
            vm.offerEntry.forEach(function (sub) {
                vm.totalHargaAuction += sub.TotalPriceAuction;
            });
        }


        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()