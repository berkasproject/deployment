(function () {
	'use strict';

	angular.module("app").controller("AuctionVendorController", ctrl);

	ctrl.$inject = ['UIControlService', 'AuctionService', '$stateParams', '$filter', '$translatePartialLoader', '$timeout', '$uibModal'];
	function ctrl(UIControlService, AuctionService, $stateParams, $filter, $translatePartialLoader, $timeout, $uibModal) {
	    var vm = this;

	    vm.tenderRefID = Number($stateParams.TenderRefID);
	    vm.stepID = Number($stateParams.StepID);
	    vm.procPackType = Number($stateParams.ProcPackType);

	    vm.isJoin = false;
	    vm.isOverTime = false;
	    vm.rangkingNow = 0;
	    vm.distance = 0;

	    console.info(vm.stepID);

	    vm.init = init;
	    function init() {
	        $translatePartialLoader.addPart("auction");
	        loadDataTender();
	    }

	    function loadDataTender() {
	        UIControlService.loadLoading("MESSAGE.LOADING");
	        AuctionService.getDataStepTender({ ID: vm.stepID }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                var data = reply.data;
	                console.info("data: " + JSON.stringify(data));
	                vm.tenderStepID = data.ID;
	                vm.tenderName = data.tender.TenderName;
	                vm.isCancelled = data.tender.IsCancelled;
	                vm.startDate = data.StartDate;
	                vm.endDate = data.EndDate;
	                vm.namaTahapan = data.step.TenderStepName;
	                vm.tenderID = data.TenderID;
	                vm.tenderType = data.tender.ProcPackTypeName;

	                vm.dateEnd = new Date(vm.endDate);
	                vm.dateToday = new Date();
	                if (vm.dateToday > vm.dateEnd) {
	                    vm.isOverTime = true;
	                }

	                console.info("vm.isOverTime: " + vm.isOverTime);
	                getAuction();
	            } else {
	                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD");
	                UIControlService.unloadLoading();
	            }
	        }, function (err) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
	            UIControlService.unloadLoading();
	        });
	    }

	    vm.getCountDown = getCountDown;
	    function getCountDown() {
	        vm.endCount = new Date(vm.endDate).getTime();
	        vm.date = new Date(vm.endDate);

	        vm.now = new Date().getTime();
	        vm.distance = vm.endCount - vm.now;

	        //vm.countDown = vm.distance;
	        vm.days = Math.floor(vm.distance / (1000 * 60 * 60 * 24));
	        vm.hour = Math.floor((vm.distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	        vm.minutes = Math.floor((vm.distance % (1000 * 60 * 60)) / (1000 * 60));
	        vm.seconds = Math.floor((vm.distance % (1000 * 60)) / 1000);
	        
	        vm.distance--;
	        vm.destroyCountDown = $timeout(vm.getCountDown, 1000);
	        if (vm.distance < 0) {
	            vm.isOverTime = true;
	        }
	    }

	    function getAuction() {
	        UIControlService.loadLoading("MESSAGE.LOADING");
	        AuctionService.getAuction({ TenderStepID: vm.tenderStepID }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                if (reply.data.length > 0) {
	                    var data = reply.data[0];
	                    vm.minimumPrice = $filter('currency')(data.MinimumPrice, "Rp. ");
	                    vm.batasBawah = data.MinimumPrice;
	                    vm.biddingInformation = data.BiddingInformation;
	                    vm.biddingInformationName = data.BiddingInformationName;
	                    vm.totalPeserta = data.AuctionDetails.length;
	                    getAuctionID();
	                } else {
	                    vm.batasBawah == null;
	                }
	            } else {
	                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD_AUCTION");
	                UIControlService.unloadLoading();
	            }
	        }, function (err) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	            UIControlService.unloadLoading();
	        });
	    }

	    function getAuctionID() {
	        UIControlService.loadLoading("MESSAGE.LOADING");
	        AuctionService.getAuctionID({
	            TenderID: vm.tenderID
	        }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                var data = reply.data;
	                console.info("data: " + JSON.stringify(data));
	                vm.auctionID = data.ID;
	                getAuctionVendorDetail();
	            } else {
	                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD_AUCTION");
	                UIControlService.unloadLoading();
	            }
	        }, function (err) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	            UIControlService.unloadLoading();
	        });
	    }

	    function getAuctionVendorDetail() {
	        UIControlService.loadLoading("MESSAGE.LOADING");
	        AuctionService.getAuctionVendorDetail({
	            AuctionID: vm.auctionID
	        }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                if (reply.data.length > 0) {
	                    var data = reply.data[0];
	                    vm.isJoin = data.IsJoin;
	                    vm.bidPriceBefore = $filter('currency')(data.BidPrice, "Rp. ");
	                    if (vm.bidPriceBefore == null) {
	                        vm.bidPriceBefore = $filter('currency')(0, "Rp. ");;
	                    }
	                    if (vm.isJoin == null || vm.isJoin == undefined) {
	                        vm.isJoin = false;
	                    }
	                }
	                console.info("bidPriceBefore: " + vm.bidPriceBefore);
	                getVendor();
	            } else {
	                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD_VENDOR_AUCTION");
	                UIControlService.unloadLoading();
	            }
	        }, function (err) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	            UIControlService.unloadLoading();
	        });
	    }

	    function getVendor()
	    {
	        UIControlService.loadLoading('MESSAGE.LOADING');
	        AuctionService.selectByUsername(function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                vm.vendorID = reply.data.VendorID;
	                getBestBid();
	            } else {
	                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_VENDOR");
	            }
	        }, function (error) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	            UIControlService.unloadLoading();
	        });
	    }

	    function getBestBid() {
	        UIControlService.loadLoading('MESSAGE.LOADING');
	        console.info("vm.batasBawah: " + vm.batasBawah);
	        AuctionService.getBestBid({
	            AuctionID: vm.auctionID,
                BatasBawah: vm.batasBawah
	        }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                if (reply.data.length) {
	                    var allData = reply.data;
	                    var data = reply.data[0];
	                    vm.bestVendorName = data.VendorName;
	                    vm.bestBidPrice = $filter('currency')(data.BidPrice, "Rp. ");
	                    console.info("vm.bestBidPrice: " + vm.bestBidPrice);
	                    console.info("vm.allData: " + JSON.stringify(allData));
	                    for (var i = 0; i < allData.length; i++) {
	                        if (allData[i].VendorID == vm.vendorID) {
	                            vm.rangkingNow = i + 1;
	                            break;
	                        }
	                    }
	                    console.info("vm.rangkingNow: " + vm.rangkingNow);
	                }
	                
	                if (vm.isOverTime == false) {
	                    getCountDown();
	                }
	            } else {
	                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_BEST_BID");
	            }
	        }, function (error) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	            UIControlService.unloadLoading();
	        });
	    }

	    vm.joinAuction = joinAuction;
	    function joinAuction() {
	        UIControlService.loadLoading('MESSAGE.LOADING');
	        AuctionService.joinAuction({
	            AuctionID: vm.auctionID,
	            VendorID: vm.vendorID
	        }, function (reply) {
	            if (reply.status === 200) {
	                UIControlService.unloadLoading();
	                loadDataTender();
	                UIControlService.msg_growl("success", "MESSAGE.SUCC_JOIN");
	            } else {
	                UIControlService.msg_growl("error", "MESSAGE.ERR_JOIN");
	                UIControlService.unloadLoading();
	            }
	        }, function (error) {
	            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	            UIControlService.unloadLoading();
	        });
	    }

	    vm.saveBidPrice = saveBidPrice;
	    function saveBidPrice() {
	        //if (vm.bidPrice < vm.batasAtas) {
	        //    UIControlService.msg_growl("warning", "MESSAGE.WAR_BID " + vm.minimumPrice);
	        //    return;
	        //}
	        UIControlService.loadLoading("MESSAGE.LOADING");
	        
	        AuctionService.saveBidPrice({
	            AuctionID: vm.auctionID,
                VendorID: vm.vendorID,
                BidPrice: vm.bidPrice,
                EndDate: vm.endDate
	        }, function (reply) {
	            UIControlService.unloadLoading();
	            if (reply.status === 200) {
	                vm.bidPrice = "";
	                loadDataTender();
	                UIControlService.msg_growl('success', "MESSAGE.SUCC_SUBMIT");
	            } else {
	                UIControlService.msg_growl('error', "MESSAGE.ERR_SUBMIT");
	            }
	        }, function (err) {
	            console.info("err: " + JSON.stringify(err));
	            if (err == 'ERR_ENDDATE_AUCTION') {
	                UIControlService.msg_growl("error", "MESSAGE.ERR_ENDDATE_AUCTION");
	                UIControlService.unloadLoading();
	            } else {
	                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
	                UIControlService.unloadLoading();
	            }
	        });
	    }

	    vm.detailPenawaran = detailPenawaran;
	    function detailPenawaran() {
	        if (vm.tenderType == "TYPE_RFQGOODS") {
	            var post = {
	                TenderName: vm.tenderName,
	                TenderID: vm.tenderID,
	                VendorID: vm.vendorID,
	                ProcPackType: vm.procPackType,
	                TenderRefID: vm.tenderRefID
	            };

	            var modalInstance = $uibModal.open({
	                templateUrl: 'app/modules/rekanan/auction/detail-penawaran-goods.html',
	                controller: 'detPenawaranGoodsCtrl',
	                controllerAs: 'detPenawaranGoodsCtrl',
	                resolve: { item: function () { return post; } }
	            });

	            modalInstance.result.then(function () {
	                vm.init();
	            })
	        } else if (vm.tenderType == "TYPE_RFQVHS") {
	            var post = {
	                TenderName: vm.tenderName,
	                TenderID: vm.tenderID,
	                VendorID: vm.vendorID,
	                ProcPackType: vm.procPackType,
	                TenderRefID: vm.tenderRefID
	            };

	            var modalInstance = $uibModal.open({
	                templateUrl: 'app/modules/rekanan/auction/detail-penawaran-vhs.html',
	                controller: 'detPenawaranVhsCtrl',
	                controllerAs: 'detPenawaranVhsCtrl',
	                resolve: { item: function () { return post; } }
	            });

	            modalInstance.result.then(function () {
	                vm.init();
	            })
	        } else {
	            var post = {
	                TenderName: vm.tenderName,
	                TenderID: vm.tenderID,
	                VendorID: vm.vendorID,
	                ProcPackType: vm.procPackType,
	                TenderRefID: vm.tenderRefID
	            };

	            var modalInstance = $uibModal.open({
	                templateUrl: 'app/modules/rekanan/auction/detail-penawaran-service.html',
	                controller: 'detPenawaranServiceCtrl',
	                controllerAs: 'detPenawaranServiceCtrl',
	                resolve: { item: function () { return post; } }
	            });

	            modalInstance.result.then(function () {
	                vm.init();
	            })
	        }
	    }
	}
})();