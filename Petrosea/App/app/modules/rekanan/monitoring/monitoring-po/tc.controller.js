﻿(function () {
	'use strict';

	angular.module("app").controller("TCVendorCtrl", ctrl);

	ctrl.$inject = ['item','GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService', '$uibModalInstance'];
	function ctrl(item, GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService, $uibModalInstance) {

		var vm = this;
		vm.init = init;
		vm.item = item.item;
		vm.reff = item.reff;

		function init() {
		    console.info(item);
		    $translatePartialLoader.addPart('detail-po');
		}

		vm.valid = true;
		vm.notvalid = function notvalid() {
		    vm.valid = false;
		}

		vm.setuju = setuju;
		function setuju() {
		    if (vm.VendorRemark == undefined) {
		        vm.VendorRemark = "";
		    }
		    DataMonitoringService.AgreePO({
		        ID: vm.item,
		        IsCheck: true,
		        VendorRemark: vm.VendorRemark
		    },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $uibModalInstance.close();
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });

		}

		vm.reject = reject;
		function reject() {
		    if (vm.VendorRemark == undefined) {
		        vm.VendorRemark = "";
		    }
		    DataMonitoringService.AgreePO({
		        ID: vm.item,
		        IsCheck: false,
                VendorRemark:vm.VendorRemark
		    },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $uibModalInstance.close();
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });

		}



		//VendorBalance
		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}
	}
})();
//TODO


