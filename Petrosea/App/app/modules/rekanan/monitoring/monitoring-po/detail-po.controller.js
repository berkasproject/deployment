﻿(function () {
	'use strict';

	angular.module("app").controller("DetailmonitoringPOCtrl", ctrl);

	ctrl.$inject = ['GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
	/* @ngInject */
	function ctrl(GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.vendor_name = "";
		vm.list_detail = "";
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.dataPOId = String($stateParams.ID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.paket = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('detail-po');
			$translatePartialLoader.addPart('acknowledgementsheet');
			UIControlService.loadLoading("MESSAGE.LOADING");
			loadPaket();
		};

		vm.loadPaket = loadPaket;
		function loadPaket() {
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.DetailVendorMonitoring({
				Status: vm.dataPOId,
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.paket = reply.data;
					for (var i = 0; i < vm.paket.length; i++) {
						if (vm.paket[i].SysReference.Name == "Detail_SI") vm.paket[i].IsCheck == true;
					}
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();

				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.cariPaket = cariPaket;
		function cariPaket(keyword) {
			vm.keyword = keyword;
			vm.currentPage = 1;
			loadPaket();
		};

		vm.open = open;
		function open(param, data) {
			vm.reff = data.VendorMonitoringPO.SysReference.Name;
			console.info("param" + param);
			if (param === "DETAIL_PURCHASE") {
				PO(data.VendorMonitoringPO.MonitoringPOId);
			}
			else if (param === "DETAIL_TOC") {
				for (var i = 0; i < vm.paket.length; i++) {
					if (vm.paket[i].SysReference.Name === "DETAIL_TOC") {
						var parameter = vm.paket[i].ID;
						i = vm.paket.length;
					}
				}
				console.info("parameter" + parameter);
				tc(parameter);
			}
			else if (param === "DETAIL_ACKN") {
				Aknowsheet(data.VendorMonitoringPO.MonitoringPOId);
			}
			//$uibModalInstance.close();
		}

		vm.tc = tc;
		function tc(dt) {
			var data = {
				item: dt,
				reff: vm.reff
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/monitoring/monitoring-po/toc.html',
				controller: 'TCVendorCtrl',
				controllerAs: 'TCVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.PO = PO;
		function PO(id) {
			$state.transitionTo('export-po-vendor', { id: id });
		};
		vm.Aknowsheet = Aknowsheet;
		function Aknowsheet(id) {
			$state.transitionTo('acknowledgement-vendor', { id: id });
		};
		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}
		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.setuju = setuju;
		function setuju() {
			for (var i = 0; i < vm.paket.length; i++) {
				if (vm.paket[i].SysReference.Name === "DETAIL_TOC") {
					DataMonitoringService.AgreePO({
						Status: vm.paket[i].ID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							$uibModalInstance.close();
						}
						else {
							$.growl.error({ message: "MESSAGE.ERR_LOAD" });
							UIControlService.unloadLoading();
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
					});
				}
			}

		}

		vm.setujuAll = setujuAll;
		function setujuAll() {
			vm.cekAll = true;
			for (var i = 0; i < vm.paket.length; i++) {
				if (vm.paket[i].SysReference.Name !== "DETAIL_OTHERDOC" && vm.paket[i].SysReference.Name !== "DETAIL_SI") {
					if (vm.paket[i].CheckDate == null) {
						UIControlService.msg_growl("error", $filter('translate')(vm.paket[i].SysReference.Value) + ' ' + $filter('translate')('LABEL_CEKDETAIL'));
						return;
					}
				}
				if (vm.paket[i].SysReference.Name !== "DETAIL_OTHERDOC") {
					if (vm.paket[i].IsCheck == false) vm.cekAll = false;
				}
			}
			if (vm.cekAll == false) {
				bootbox.confirm($filter('translate')('LABEL_SUREREVISI'), function (yes) {
					if (yes) {
						UIControlService.loadLoadingModal("MESSAGE.LOADING");
						DataMonitoringService.AgreePOAll(vm.paket, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								window.location.reload();
							} else {
								$.growl.error({ message: "MESSAGE.ERR_LOAD" });
								UIControlService.unloadLoading();
							}
						}, function (error) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
						});
					}
				});
			} else {
				bootbox.confirm($filter('translate')('LABEL_SUREALL'), function (yes) {
					if (yes) {
						UIControlService.loadLoadingModal("MESSAGE.LOADING");
						DataMonitoringService.AgreePOAll(vm.paket, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								window.location.reload();
							}
							else {
								$.growl.error({ message: "MESSAGE.ERR_LOAD" });
								UIControlService.unloadLoading();
							}
						}, function (error) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
						});
					}
				});
			}
		}
	}
})();
