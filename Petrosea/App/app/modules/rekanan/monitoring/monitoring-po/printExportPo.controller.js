﻿(function () {
    'use strict';

    angular.module("app").controller("PrintExportPoVendorCtrl", ctrl);

    ctrl.$inject = ['GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
    function ctrl(GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.pageSize = 10;
        vm.init = init;
        vm.totalItems = String($stateParams.totalItems);
        vm.dataPOId = String($stateParams.dataPOId);
        vm.contactCP = [];
        vm.contactAddress = [];
        vm.paket = [];
        vm.potype = "";

        vm.hasilParsing = Math.ceil(vm.totalItems / 5);
        vm.alldata_ = [];

        function init() {
            //console.info("vm.coba" + JSON.stringify(vm.coba));
            //filter();
            $translatePartialLoader.addPart('detail-po');
            loadPaket();
        }

        function filter() {
            for (var j = 0; j < vm.export.detailVendor.length; j++) {
                if (vm.export.detailVendor[j].SysReference.Name == "DETAIL_PURCHASE") {
                    vm.IdAgree = vm.export.detailVendor[j].ID;
                }
            }
            vm.nasional = true;
            vm.hascity = true;
            vm.hasdistrict = true;
            vm.internasional = false;
            for (var i = 0; i < vm.export.vendorcontact.length; i++) {
                if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                    vm.contactCompany = vm.export.vendorcontact[i];
                    if (vm.export.vendorcontact[i].Contact.Address.State.Country.Code == "ID") vm.flagVAT = true;
                    else vm.flagVAT = false;
                }
                else if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
                    vm.contactCP.push(vm.export.vendorcontact[i]);
                }
                else if (vm.export.vendorcontact[i].VendorContactType.Type == "VENDOR_OFFICE_TYPE" && vm.export.vendorcontact[i].IsPrimary == null) {
                    vm.contactAddress.push(vm.export.vendorcontact[i]);
                    if (vm.contactAddress.length == 1) {
                        if (vm.contactAddress.length == 1) {
                            if (vm.export.vendorcontact[i].Contact.Address.State.Country != null) {
                                if (vm.export.vendorcontact[i].Contact.Address.State.Country.Name === "Indonesia") {
                                    if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
                                        if (vm.export.vendorcontact[i].Contact.Address.DistrictID != null) {
                                            vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.Distric.Name + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                            vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                            vm.District = vm.export.vendorcontact[i].Contact.Address.Distric.Name;
                                            vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
                                            vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                            vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        }
                                        else {
                                            vm.hasdistrict = false;
                                            vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                            vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                            vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
                                            vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                            vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        }
                                    }
                                    else {
                                        vm.hascity = false;
                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                        vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                        vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                    }
                                }
                                else {
                                    vm.internasional = true;
                                    vm.nasional = false;
                                    if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                        vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
                                        vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                        vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                    }
                                    else {
                                        vm.hascity = false;
                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                        vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                        vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataMonitoringService.ExportPurchase({
                Status: vm.dataPOId
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.export = reply.data;
                    for (var j = 0; j < vm.export.detailVendor.length; j++) {
                        if (vm.export.detailVendor[j].SysReference.Name == "DETAIL_PURCHASE") {
                            vm.IdAgree = vm.export.detailVendor[j].ID;
                        }
                    }
                    vm.nasional = true;
                    vm.hascity = true;
                    vm.hasdistrict = true;
                    vm.internasional = false;
                    for (var i = 0; i < vm.export.vendorcontact.length; i++) {
                        if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.contactCompany = vm.export.vendorcontact[i];
                            if (vm.export.vendorcontact[i].Contact.Address.State.Country.Code == "ID") vm.flagVAT = true;
                            else vm.flagVAT = false;
                        }
                        else if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
                            vm.contactCP.push(vm.export.vendorcontact[i]);
                        }
                        else if (vm.export.vendorcontact[i].VendorContactType.Type == "VENDOR_OFFICE_TYPE" && vm.export.vendorcontact[i].IsPrimary == null) {
                            vm.contactAddress.push(vm.export.vendorcontact[i]);
                            if (vm.contactAddress.length == 1) {
                                if (vm.export.vendorcontact[i].Contact.Address.State.Country.Name == "Indonesia") {
                                    if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
                                        if (vm.export.vendorcontact[i].Contact.Address.DistrictID != null) {
                                            vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.Distric.Name + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                            vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                            vm.District = vm.export.vendorcontact[i].Contact.Address.Distric.Name;
                                            vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
                                            vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                            vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        }
                                        else {
                                            vm.hasdistrict = false;
                                            vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                            vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                            vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
                                            vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                            vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        }
                                    }
                                    else {
                                        vm.hascity = false;
                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                        vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                        vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                    }
                                }
                                else {
                                    vm.internasional = true;
                                    vm.nasional = false;
                                    if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                        vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
                                        vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                        vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                    }
                                    else {
                                        vm.hascity = false;
                                        vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                        vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
                                        vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
                                        vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
                                    }
                                }
                            }
                        }
                    }
                    console.info("alldata_" + JSON.stringify(vm.export));
                    showPO(1);
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };
        vm.Remark = "";
        vm.showPO = showPO;
        function showPO(current) {
            vm.currentPage = current;
            DataMonitoringService.loadItem({
                Keyword: "",
                Offset: 0,
                Limit: vm.totalItems,
                Status: vm.dataPOId,
                FilterType: vm.export.VendorId
            },
            function (reply) {
                if (reply.status === 200) {
                    vm.alldata = reply.data.List;
                    vm.totalItems = Number(reply.data.Count);
                    vm.potype = vm.alldata[0].POType;
                    vm.jumlahkarakter = [];
                    for (var i = 0; i < vm.alldata.length; i++) {
                        vm.jumlahkarakter[i] = (vm.alldata[i].ShortText + " " + vm.alldata[i].Mfr_No + " " + vm.alldata[i].Mfr_Name + " " + vm.alldata[i].Manu_Mat + " " + vm.alldata[i].Purch_OrderText + " " + vm.alldata[i].PO_ItemText + " TARIFF CODE:" + vm.alldata[i].TariffCode + "CONC. CAT:" + vm.alldata[i].ConcessCat).length;
                        //console.info("jmlk" + vm.jumlahkarakter[i]);
                        if (vm.alldata[i].BuyerRemark != null) {
                            if (vm.Remark == "") vm.Remark = vm.alldata[i].BuyerRemark;
                            else vm.Remark = vm.Remark + ', ' + vm.alldata[i].BuyerRemark;
                        }
                    }

                    var batasandata = 0;
                    var maxvalue = Math.max.apply(Math, vm.jumlahkarakter);
                    console.info("maxv:" + maxvalue);
                    if (maxvalue > 50 && maxvalue <= 350) {
                        batasandata = 5;
                    }
                    else if (maxvalue > 350 && maxvalue <= 500) {
                        batasandata = 3;
                    }
                    else if (maxvalue > 500) {
                        batasandata = 1;
                    }
                    else if (maxvalue <= 50) {
                        batasandata = 10;
                    }
                    //if (vm.totalItems <= batasandata) {
                    //  jumlahlembar = 1;
                    //}
                    //else {
                    var jumlahlembar = Math.ceil(vm.totalItems / batasandata);
                    //}
                    if (vm.totalItems > batasandata) {
                        for (var i = 1; i <= jumlahlembar; i++) {
                            vm.alldata_[i] = [];
                            var batasAtas = (i * batasandata) - batasandata;
                            var batasBawah = 0;
                            if (i == jumlahlembar) {
                                batasBawah = vm.totalItems - 1;
                            }
                            else {
                                batasBawah = (i * batasandata) - 1;
                            }
                            for (var k = batasAtas; k <= batasBawah; k++) {
                                vm.alldata_[i].push(vm.alldata[k]);
                            }
                        }
                    }
                    else {
                        vm.alldata_[1] = [];
                        //for (var k = 0; k <= vm.totalItems; k++) {
                        if (vm.alldata != null) {
                            vm.alldata_[1] = vm.alldata;
                        }
                        else {
                            vm.alldata_[1] = vm.export.POmodel;
                        }
                        //}
                    }
                    vm.jumlahlembar = jumlahlembar;
                    console.info("alldata_" + JSON.stringify(vm.alldata));
                }
                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };


        //print
        vm.exportpdf = exportpdf;
        var pdf, page_section, HTML_Width, HTML_Height, top_left_margin, PDF_Width, PDF_Height, canvas_image_width, canvas_image_height;

        function exportpdf() {

            pdf = "";
            $("#downloadbtn").hide();
            $("#genmsg").show();
            html2canvas($(".print-wrap:eq(0)")[0], { allowTaint: true }).then(function (canvas) {

                calculatePDF_height_width(".print-wrap", 0);
                var imgData = canvas.toDataURL("image/png", 1.0);
                pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);

            });

            html2canvas($(".print-wrap:eq(1)")[0], { allowTaint: true }).then(function (canvas) {

                calculatePDF_height_width(".print-wrap", 1);

                var imgData = canvas.toDataURL("image/png", 1.0);
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);

            });

            html2canvas($(".print-wrap:eq(2)")[0], { allowTaint: true }).then(function (canvas) {

                calculatePDF_height_width(".print-wrap", 2);

                var imgData = canvas.toDataURL("image/png", 1.0);
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);

            });

            html2canvas($(".print-wrap:eq(3)")[0], { allowTaint: true }).then(function (canvas) {

                calculatePDF_height_width(".print-wrap", 2);

                var imgData = canvas.toDataURL("image/png", 1.0);
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);



                //console.log((page_section.length-1)+"==="+index);
                setTimeout(function () {

                    //Save PDF Doc  
                    pdf.save("HTML-Document.pdf");

                    //Generate BLOB object
                    var blob = pdf.output("blob");

                    //Getting URL of blob object
                    var blobURL = URL.createObjectURL(blob);

                    //Showing PDF generated in iFrame element
                    var iframe = document.getElementById('sample-pdf');
                    //iframe.src = blobURL;

                    //Setting download link
                    var downloadLink = document.getElementById('pdf-download-link');
                    //downloadLink.href = blobURL;

                    $("#sample-pdf").slideDown();


                    $("#downloadbtn").show();
                    $("#genmsg").hide();
                }, 0);
            });
            /*
            html2canvas(document.getElementById('print'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 3000,
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("example.pdf");
                }
            });

            html2canvas(document.getElementById('print2'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500,
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("example2.pdf");
                }
            });*/

        }
        vm.generateKriteria = [];
        vm.generateKriteria2 = [];
        vm.makePDF = makePDF;
        function makePDF() {

            var fax = '';
            if (vm.export.buyer.Fax != null) {
                fax = vm.export.buyer.Fax;
            }
            var tel = '';
            if (vm.contactCompany.Contact.Phone != null) {
                tel = vm.contactCompany.Contact.Phone;
            }
            var faxVendor = '';
            if (vm.contactCompany.Contact.Fax != null) {
                faxVendor = vm.contactCompany.Contact.Fax;
            }
            var businessName = '';
            if (vm.export.vendorcontact[0].Vendor.business != null) {
                businessName = vm.export.vendorcontact[0].Vendor.business.Name + '.';
            }

            //header Kriteria
            var newmap1 = {
                text: 'ITEM', bold: true, alignment: 'center'
            };
            var newmap2 = {
                text: 'SCD/PR/FPA', bold: true, alignment: 'center'

            };
            var newmap3 = {
                text: 'QTY', bold: true, alignment: 'center'

            };
            var newmap4 = {
                text: 'UM', bold: true, alignment: 'center'

            };
            var newmap5 = {
                text: 'DESCRIPTION', bold: true, alignment: 'center'

            };
            var newmap6 = {
                text: 'DUE DATE', bold: true, alignment: 'center'

            };
            var newmap7 = {
                text: 'NET PRICE', bold: true, alignment: 'center'

            };
            var newmap8 = {
                text: 'TOTAL PRICE', bold: true, alignment: 'center'

            };
            vm.generateKriteria.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8);
            vm.generateKriteria2[0] = vm.generateKriteria;
            vm.generateKriteria = [];
            var indeks = 0;
            for (var i = 0; i <= vm.alldata.length - 1; i++) {
                indeks = indeks + 1;

                var newmap1 = {
                    text: vm.alldata[i].POItem
                };
                if (vm.POType == 'Amanded') {
                    var newmap1 = {
                        text: vm.alldata[i].POItem + '*', alignment: 'center'
                    };
                }
                var newmap2 = {
                    text: vm.alldata[i].ScdPrFpa, alignment: 'center'
                };
                var newmap3 = {
                    text: vm.alldata[i].Quantity, alignment: 'center'
                };
                var newmap4 = {
                    text: vm.alldata[i].UnitMeasure, alignment: 'center'
                };
                var newmap5 = {
                    text: vm.alldata[i].ShortText + vm.alldata[i].Mfr_No + vm.alldata[i].Mfr_Name + vm.alldata[i].Manu_Mat + '\n' + vm.alldata[i].Purch_OrderText + vm.alldata[i].PO_ItemText + 'TARIFF CODE:' + vm.alldata[i].TariffCode + '/CONC. CAT:' + vm.alldata[i].ConcessCat
                };
                var newmap6 = {
                    text: vm.alldata[i].DelivDateOri, alignment: 'center'
                };
                var newmap7 = {
                    text: vm.alldata[i].NettPricePO.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'), alignment: 'right'
                };
                var newmap8 = {
                    text: vm.alldata[i].TotalPricePO.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'), alignment: 'right'
                };
                vm.generateKriteria.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8);
                vm.generateKriteria2[indeks] = vm.generateKriteria;
                vm.generateKriteria = [];
            }
            var newmap1 = {
                text: '',
                colSpan: 5
            };
            var newmap2 = {

            };
            var newmap3 = {

            };
            var newmap4 = {

            };
            var newmap5 = {

            };
            var newmap6 = {
                text: 'TOTAL',
                bold: true,
                alignment: 'left'
            };
            var newmap7 = {
                text: vm.alldata[0].Currency,
                bold: true,
                alignment: 'left'
            };
            var newmap8 = {
                text: vm.alldata[0].Totalvalue.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'),
                bold: true,
                alignment: 'right'
            };
            vm.generateKriteria.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8);
            vm.generateKriteria2[indeks + 1] = vm.generateKriteria;
            vm.generateKriteria = [];

            if (vm.flagVAT == true) {
                var newmap1 = {
                    text: '',
                    colSpan: 5
                };
                var newmap2 = {

                };
                var newmap3 = {

                };
                var newmap4 = {

                };
                var newmap5 = {

                };
                var newmap6 = {
                    text: 'VAT 10%',
                    bold: true,
                    alignment: 'left',
                    colSpan: 2
                };
                var newmap7 = {
                };
                var newmap8 = {
                    text: vm.alldata[0].vatvalue.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'),
                    bold: true,
                    alignment: 'right'
                };
                vm.generateKriteria.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8);
                vm.generateKriteria2[indeks + 2] = vm.generateKriteria;
                vm.generateKriteria = [];


                var newmap1 = {
                    text: '',
                    colSpan: 5
                };
                var newmap2 = {

                };
                var newmap3 = {

                };
                var newmap4 = {

                };
                var newmap5 = {

                };
                var newmap6 = {
                    text: 'GRAND TOTAL',
                    bold: true,
                    alignment: 'left',
                    colSpan: 2
                };
                var newmap7 = {
                };
                var newmap8 = {
                    text: vm.alldata[0].grandvalue.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'),
                    bold: true,
                    alignment: 'right'
                };
                vm.generateKriteria.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8);
                vm.generateKriteria2[indeks + 3] = vm.generateKriteria;
                vm.generateKriteria = [];
            }
            console.info("generateKriteria" + JSON.stringify(vm.generateKriteria2));


            var address1 = ' ';
            var address2 = ' ';
            var address3 = ' ';
            if ((vm.internasional == true && vm.hascity == true) || (vm.nasional == true && vm.hascity == true && vm.hasdistrict == false && vm.internasional == false)) {
                address1 = vm.AddressInfo;
                address2 = vm.City;
                address3 = vm.State + ', ' + vm.Country;
            }
            else if (vm.internasional == true && vm.hascity == false) {
                address1 = vm.address;
            }
            else if (vm.nasional == true && vm.hascity == false && vm.hasdistrict == false && vm.internasional == false) {
                address1 = vm.AddressInfo;
                if (vm.State != undefined && vm.Country != undefined) {
                    address2 = vm.State + ', ' + vm.Country;
                }
            }
            else if (vm.nasional == true && vm.hascity == true && vm.hasdistrict == true && vm.internasional == false) {
                address1 = vm.AddressInfo;
                if (vm.District != undefined && vm.City!=undefined) {
                    address2 = vm.District + ', ' + vm.City;
                }
                address3 = vm.State + ', ' + vm.Country;
            }
            else if (vm.nasional == true && vm.hascity == false && vm.hasdistrict == true && vm.internasional == false) {
                address1 = vm.AddressInfo;
                if (vm.District != undefined) {
                    address2 = vm.District;
                }
                address3 = vm.State + ', ' + vm.Country;
            }
            console.info("address3" + address3);

            var freight = '';
            if (vm.export.freightMethod != null) {
                freight = vm.export.freightMethod.Name + ' ';
            }

            var remark = '';
            if (vm.export.POmodel.BuyerRemark != null) {
                remark = vm.export.POmodel.BuyerRemark;
            }


            var documentDefinition = {
                pageMargins: [40, 40, 40, 80],
                footer: {
                    style: "tableStyleFooter",
                    fontSize: 6,
                    text: [
                       { text: 'PT. PETROSEA TBK\n', bold: true },
                       "Jakarta# :The Energy Building 31st Fl. SCBD Lot 11 A, Jl. Jend. Sudirman# Kav. 52-53, Jakarta 12190, Indonesia. T.(62) 21 524 9000 F.(62) 21 524 9020 \n",
                       "Makassar :Jl.Somba Opu 281, Po.Box 1143, Makassar 90001, Sulawesi Selatan, Indonesia. T.(62) 411 366 9000 F. (62) 411 366 9020 \n",
                       "Sorowako :Jl. Ternate 44, Sorowako, Nuha - Luwu Timur 92984, Sulawesi Selatan, Indonesia. T.(62) 475 332 9100 F.(62) 475 332 9557 \n\n",
                       "www.vale.com/indonesia"
                    ]
                },
                content:
                [

                    {
                        style: 'marginAtas',
                        columns: [
                            {},
                            {
                                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAyCAYAAACQyQOIAAANoklEQVR4nO1de5AcRRn/dmfvwsMcyWWXA8UUAgEVLVFBSIDyeJSISKkl6xPKKNYKyuZub3u2HzO70xcKpaBACUgBicgjDzhCILvJlbzEKrUUTSWlsXgEBBF5JbcbAwnJZe+u/WO6Z3pf98ru3uHdV9WV25nunn785vf9uuebCYBu0agB0ajh/U4kDoc0/XyI29xw7M0hbu8wHHuP4djDIW4Lw7GLBrd2hhzrTyFu3xzi9oUQj8+RpQPAO0PAIQhTZJxD8GkOoac5hAAgoJ0K1Dg+w41DsAQAafJpg7MVBrdeDnFbTDDtCGaYAzz1wap1N8H6+sDo64OKa265E1q23Akt5cdnAQHgsoC6a7m1xODWk/rEGtwaMbg1JNOIwa2RivOOPVx+3uDW3hC3bwBC5ldcp0HGOQR1ABRy8xcWcuGr8rnwukI2vLWQjbxSyIX/VchFthVy4dUD2QVXvLrmqPmqLMx4MPBk2ODWam9yHXvYcOyicgETSeVlDW69ZWSs77rXaRg7BCQAAgAA+Vz74nw28nA+F9lfyEVE9RQeKeQiopCNDOSz4d5dG8NzVV0NaN/0NyNjXWo49qty0oYMbg1Nwh1UAsJliqKmJx4BSiMAAMA7Q/Vqf18fGPJuhoENCz5WyIazpZMdLhZy4WIhGx4uSe6xIQWIfC78n0Ju/kIAjx1mlkmhJwzHHqwHAGoxhATHG6EMOw8AFBgmfffpbuCt++DIgWzk+kI2POje5XKi1V0/agoflEB4QnMrM5AVbPtEg1tvqLu4EWDQVhgjBrdGghmWBIDJ6oaALu4Kj0a+VMhGXpYAGHLv8rEmX6ZseFi6h1fe2vCBo1X99Rvc95kZGevryjU0CghV2OFujxXGpxsCT3MIKdrOP9x+XD4X6StzAeNggBK3MVLIhod2PRLuBHDdTONGebqb9Nchx/q5unMbCgaXFQ7K378DShfo7ahontwLAHmnvtYHhxeyC3A+F96jscDw+AHgAaFYyEVEPhtGAN4SckZbAAACEIu1hBzrj81ghhLAOdYLwOnJAODpBjX5umh7cQXMyW+M/EBzA8MTcgPVQJCLPAQw05lANzXgduoUw7F3N1ovlIPB4NYApOm5JW1RS8FH2z9cyC7AHgAknU/MDVTqgnwuvOO1vrZ2/VqzBuBRs5Gxvt0sVtCvYzj2oMHT3xICQv9cf+zpO7OReD4beSyfjbynTeIk3UCFLhjM59oXA8yyQXVTYODstmboBY0Zhlu5JSCdOeDccfHbQ/1tYncuInZlO3wqPyQAuCkvXcJAdkEcYFYXjGYuRcbjc0Lc/kuTmWGkxbGHIcPFlbdeNrJnU3tx36b5QzuzHZNzASUAiIh87ujink0LxK6NR68GmGWCsU356DQ+1eDWO83SCyq1cDYC9rViyY0/Es9u+IgY7p8rdmU7xECuY1Ig2JXtEIVsePjA5nniz32nPHv9nRceJXs6qwvGNF8vfK+ZrKDSYb1MQLpXzF9OxH33LhbF/jaxd3O72Jk9ZlyAyEsA7MweI97bPG/kYH+buGXVeQeO4YnPAQBEo7NsMH6TmzyGY93ZTL2g0pxeSwSdjID0chH9xeVi+8MnipH+uWLv5nYxkHMneVe2oySpY7tzEXFg8zwx1N8mtq0/qXjJzUsFsOt/AgDQWcfnHDPFXOrksSNC3N46FczQyi0xp9cSkF4ujuxlInH7V8XW9YvEe5vni+H+uaLY3yYG++eJwf55otjf5h3ble0Qj607TXx/RbTYyi0B7Lo1AADNjov4/zE1cGn8KRlf0FS9oLODwdMC7GtFK7fEhTddKfhdF4u1958pNq39jMiu+ay4/97F4mcrvyAuX/EdccrPlgnIOMOQXi5C3H72BByb1QWHbP6S8odTwQo+O7jaIcRtAeleAfa17r8Z7qb0cnlsuYB0ZqSVW6KVW4MtaXomAMyyQV3M1wu/ngq9UA6IOb2WOKyXiTm9lpATXnKsxbGKIW6LILeuAYC6xj/MdHMpNZWaG3Ks7RIME45eakbytq0de1YXNMTUgHLrdMOx90s30XS9MAYIXHA61nPAu+fJls/qgrqbpNhgxr56ql1EBQhU0KxjDwK3zgKACbNBNBo1Ojs7Q52dnaHoOMtOpMxk6geAgCrT2TkxF6dfb6w0kXpV7a5ekMGu0wUMqh3BjB0HgHroggCMzSbjyVMr70TKTktzG4/xUSHHeq6EkqcYBAa31gLApHUBShGKML3DTLGVyRS5qqS/lRYEAEim6BUI01UI07tQilyjnyvPaxKy1EyxlQjTVT0m+WaNvMoCAADxOI0kMV2BML0dYXpTPB5vG6Nd3jkTszjCdFUS0xVJTG9DhN6qkvqdxHQFIvTWUeoaxXy9cJYKep0qvaCB8PnJ6gJFjYiw603ChJtoobu7+9ga9QUAAJYuXXoYwuwlVSZJWEKvT897NSHzTUxf9+rH7PloNNo6SnuDAADLTHMRwnTIJEwgwt41TfOYcfQxCACAMH3KJEyo8lXSsNsW+vg4h6qKeXqBdU2Vi1DBsAa3DkKaLgaAybKBe8ea5okI070mYYMmYQKl6DKAiokF5d8Rxpf4A0rfisdluL42SV7eFL1S5t2PMD3gThC+VM9TrU2JBDnJz892d3WxjvJr1CqLCH1MAuGABMOwSvL3foTpmqvVS0iTNm9/wX5wKsDg6QJuLQOAQ9IF/uSy9dpd+9fR8pqErlZ5EaZ36ec0C8h6fy/zFmUSCLP1NcoA1IERTEyf9PtCe7swXmia5mkIsU8kMP7kNSn5WmIdtIp8HpFoNxzrxWbqBW2/YB24o3lI+wXld7k/+KRTP6/63NXFOkxCB9RAJ0w31K5sUoMAAD0YL/EAQ9i7inFMwvYlCDlBz1tetl5ASBISA6hkN5m3DqLVex5Bz631fmTDdIFjvQA+rR1qZwIA7kAhQrf7E0fvcbvp9tPTEz7VCxOzZ6oPjWSOFFupAWG5xg4CpQjV69WsvkBIkR8DAMRisSOi0WhrNBptjcViLVBbrE7ClF7gFmq0i6ijLqgwXzQS7FM+261RaBCqDLJaLVQTid3d3ccizPKKZbq6WIeJaUoD2natnD65zWKEOpuMbDK4taGRYPD3C1iXe926diwIANCF8UIT03cQpiMmYSKJ3WsplZ9M0o/79E4Hqk2OxhzdGqh+AwCQwPhUhOlBVb9pki/K+ivcSv00AnsGYXq3Sehak9DViND7TEI3Ioy/cqiDVm5SL6CjDcd6pRF6QdsveADckav7c4SqQpDQLQD65JK0JhLv1suVjAVAEGG2zadneoXKgAh9Qluqrq1SR92AUGP5WJRsZk16sGqa7EiI2+fXWys0SBdU6YLbh54Uu6CaaIzFYi2I0L/5IKkQk97fSYwv0tjg5VgsdgT4q4hvaIr+nS6MF8riQf3fOgFhxMT0vyamryNM30SYvmli+ppJ2KBiu/qbrxdYvVxEiS7g1hIAaMZTxQAidIs2kfcCACBCzh/v8hJh+qA22X9PEpZEKeKYmBKUYjeYhBW9uxVTBFDiw+urEeSEJxKJ9hjGR8Xj8bbu7u55cf9TRw0wXy9skhN5SMEs2n5Bt1t/YwWP5t+XaWp/z9KlSw9DhN5SPrhlAszbnDIJ26dYxdMDZTt73iRjtg18NgjA+1Ys6qZC4hk71uDWvw9FL2j7BQ8CQLPiCyoUvzvxlPlLS1pIJBIf0vMD+AOdxDSjfHENECiNMSJ3+0RPil3gdjFqQA0gqBWMlkdPnjYBqFg+XgUAEI/H50SjUUNP0AAX65vSCxl20WSBoJXZATzR1PcUy/cAEKZDiLB3te3ee/V8etsSicThJmY7tOVlOpkkZ/Sk2AWIkM6eFDuvJ8XOS2J8jonpP8rdz2hAmMgW89QzgjL/eYQzUb3g6QLHLkKang0ATY028oBg0rMVjWtJIELO1/PpfydT9DLNpbxxdRVxW2vPQrGMqqsECJjuRYgu7ibk+J4eevIy01y0zDQXJRLkpK5U6hTlNry2l2gEasfjNJJM0o/q5Xp66Mmau2mgKb3gWI9NRC9o+wUJt54piTt0FT6hf5AT8V4Nf65MPvVjmzQg/BKgkpLlrh4kCDkBYboXYXpQ35xSexaJBDnJJGxQ6oyDcv+iPO2Ty9z79LImpo/Ldh+Q+xYH9HKqPyahqxs/lN6n+1LHeZ/oGcNNaPsFfeD2bKriDvVnBfsUGySJ++mfaiIxmSRnaL5fICR3P6ts5Wqri4f11QVo/n6ZaS7yWKi21pAPseg6WW+rrHfUx9D+cfpAY4avsscyqol+eSwg+F9hs170vqYytZE87uNgjE81CX0AYbpVW/NXPm7G+FKE6YOI0HsQYT8dIyTNrduk5yJM15kpthIRek88lTpOZejqYh0y8OVuE7NfVU1usMu6ch2ACDFNQtciTFeNUm6NFoTTBPM/0XNdLb1Qogu4dQ4ATJco5PIndBMB5njyvq9D1iZjAQAIGI71VDW9oO0X9ADAdHsfIagttUYNE5tEgGpwlEDSkuDVMQJQS9zPeINXy8s13jy9QI43HPttxQJl+wUPyV5MByaYtYaZ/8r915Qm0D7X+9I00QWz1hTz9cKNig0Mbg15H9SaZYMZY66fjUYNg1u/lfsFjYgvmLVpb/5/DXC69z7CTPwY9jSx/wHaStM+GgZv+AAAAABJRU5ErkJggg==',
                                width: 70,
                                height: 28,
                                alignment: 'right'
                            }
                        ]
                    },
                    {
                        style: 'marginAtas',
                        text: "PURCHASE ORDER",
                        fontSize: 12,
                        bold: true,
                        alignment: 'center'
                    },
                    {
                        style: 'marginAtas',
                        columns: [
                            {},
                            {
                                width: 'auto',
                                table:
                                    {
                                        widths: [70, 60, 100],
                                        body: [
                                           [{ text: 'SUPPLIER CODE', bold: true, alignment: 'left', fontSize: 8 }, { text: 'PO DATE', bold: true, alignment: 'left', fontSize: 8 }, { text: 'PURCHASE ORDER NBR', bold: true, alignment: 'left', fontSize: 8 }],
                                           [{ text: vm.export.vendorcontact[0].Vendor.SAPCode, alignment: 'left', fontSize: 8 }, { text: UIControlService.convertDate(vm.export.POmodel.CreatedDate), alignment: 'left', fontSize: 8 }, { text: vm.export.POmodel.PONumber, alignment: 'left', fontSize: 8 }],
                                        ]
                                    }
                            }
                        ]
                    },
                    {
                        style: 'marginAtas',
                        columns: [
                            {
                                width: 250,
                                table:
                                    {
                                        widths: [50, 10, '*'],
                                        body: [
                                           [{ text: 'SUPPLIER', bold: true, alignment: 'left', fontSize: 8, rowSpan: 4 }, { rowSpan: 4, text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: businessName + vm.export.vendorcontact[0].Vendor.VendorName, alignment: 'left', fontSize: 8 }],
                                           [{}, {}, { text: address1, alignment: 'left', fontSize: 8 }],
                                           [{}, {}, { text: address2, alignment: 'left', fontSize: 8 }],
                                           [{}, {}, { text: address3, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'ATTENTION', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.contactCP[0].Contact.Name, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'FAX', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: faxVendor, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'EMAIL', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.contactCompany.Contact.Email, alignment: 'left', fontSize: 8 }],
                                        ]
                                    },
                                layout: 'noBorders'
                            },
                            {
                                width: 350,
                                table:
                                    {
                                        widths: [100, 10, '*'],
                                        body: [
                                           [{ text: 'FREIGHT METHOD', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: freight + 'Freight', alignment: 'left', fontSize: 8 }],
                                           [{ text: 'DELIVERY POINT', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.export.DeliverPoint, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'PAYMENT TERM', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.export.POmodel.PaymentTerms, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'TEL', bold: true, alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: tel, alignment: 'left', fontSize: 8 }],
                                        ]
                                    },
                                layout: 'noBorders'
                            }
                        ]
                    },
                    {
                        style: 'marginAtas',
                        alignment: 'left',
                        fontSize: 8,
                        text: ['In accepting this order, seller agrees to furnish the material specified in full accordance with all conditions agreed to on our contract terms and all conditions attached to this document. \n',
                                { text: 'Please indicate your acknowledgement/acceptance immadiately through E-Procurement.', bold: true }
                        ]
                    },
                    {
                        style: "tableStyle",
                        table:
                        {
                            widths: ['auto', 'auto', 'auto', 'auto', 150, 'auto', '*', '*'],
                            body: vm.generateKriteria2
                        }
                    },
                    {
                        style: 'marginAtas30',
                        columns: [
                            {
                                width: 270,
                                table:
                                    {
                                        widths: [70, 10, '*'],
                                        body: [
                                           [{ text: 'BUYER REMARK', alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.Remark, alignment: 'left', fontSize: 8 }],
                                        ]
                                    },
                                layout: 'noBorders'
                            },
                            {
                                width: 350,
                                table:
                                    {
                                        widths: [70, 10, '*'],
                                        body: [
                                           [{ text: 'BUYER', alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.export.buyer.FullName + ' ' + vm.export.buyer.SurName, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'PHONE', alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.export.buyer.WorkPhone, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'FAX', alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: fax, alignment: 'left', fontSize: 8 }],
                                           [{ text: 'EMAIL', alignment: 'left', fontSize: 8 }, { text: ':', bold: true, alignment: 'right', fontSize: 8 }, { text: vm.export.buyer.Email, alignment: 'left', fontSize: 8 }],
                                        ]
                                    },
                                layout: 'noBorders'
                            }
                        ]
                    },
                    /*,
                    {
                        style: "tableStyleHeader",
                        table:
                        {
                            widths: [150, 240, '*'],
                            body: [
                               [{ text: 'Assessment Date :', color: 'white', fillColor: '#007E7A' }, { text: UIControlService.convertDate(vm.vpdata.CPRDate), colSpan: 2 }, {}],
					           [{ text: 'Contract No :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.ContractNo + ' - ' + vm.vpdata.TanderName, colSpan: 2 }, {}],
                               [{ text: 'Contract Date :', color: 'white', fillColor: '#007E7A' }, { text: UIControlService.convertDate(vm.vpdata.ContractStartDate) + ' - ' + UIControlService.convertDate(vm.vpdata.ContractEndDate), colSpan: 2 }, {}],
                               [{ text: 'Contractor :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.VendorName + businessName, colSpan: 2 }, {}],
                               [{ text: 'Department Holder :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.Department, colSpan: 2 }, {}],
                               [{ text: 'Sponsor/Project Manager :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.Sponsor + '/' + vm.vpdata.ProjectManagerId + ' - ' + vm.vpdata.ProjManager }, { text: 'Score: ' + vm.vpdata.Score + ' %', fontSize: 12, bold: true, rowSpan: 2, margin: [0, 7, 0, 0] }],
                               [{ text: 'Contract Engineer :', color: 'white', fillColor: '#007E7A' }, vm.vpdata.ContractEngineerId + ' - ' + vm.vpdata.ContractEngineer, {}],
                            ]
                        }
                    },
                    {
                        style: "tableStyle",
                        table:
                        {
                            widths: [250, 25, '*'],
                            body: vm.generateKriteria2
                        }
                    },
                    {
                        text: $filter('translate')('FORM.NOTE'),
                        bold: true,
                        style: "marginAtas",
                        fontSize: 7
                    },
                    {
                        style: "tableStyleMargin2",
                        table:
                        {
                            widths: ['*'],
                            body: [
                               [{ text: vm.vpdata.Note }]
                            ]
                        }
                    },
                    {
                        text: $filter('translate')('FORM.DOC'),
                        bold: true,
                        style: "marginAtas",
                        fontSize: 7
                    },
                    {
                        style: "tableStyleMargin2",
                        table:
                        {
                            widths: [15,250],
                            body: vm.generateDoc2
                        }
                    }*/
                ],



                styles:
                {
                    tableHeader: {
                        bold: true,
                        alignment: 'center',
                        fillColor: '#EAC2C2'
                    },
                    tFoot: {
                        bold: true,
                        alignment: 'right',
                        fillColor: '#EAC2C2'
                    },
                    textRight: {
                        alignment: 'right'
                    },
                    marginAtas: {
                        margin: [0, 10, 0, 0],
                    },
                    marginAtas30: {
                        margin: [0, 30, 0, 0],
                    },
                    tableStyleHeader: {
                        fontSize: 10,
                        margin: [0, 5, 0, 0],
                        alignment: 'left'
                    },
                    tableStyle: {
                        fontSize: 7,
                        margin: [0, 10, 0, 0],
                        alignment: 'left'
                    },
                    tableHeader2: {
                        bold: true,
                        alignment: 'center'
                    },
                    tableStyle2: {
                        fontSize: 7,
                        margin: [0, 10, 0, 0],
                        alignment: 'left'
                    },
                    tableStyleMargin2: {
                        fontSize: 7,
                        margin: [0, 2, 0, 0],
                        alignment: 'left'
                    },
                    tableStyleMargin100: {
                        fontSize: 10,
                        margin: [0, 100, 0, 0],

                    },
                    tableStyleFooter: {
                        margin: [40, 10, 40, 'auto'],
                        fontSize: 8,
                        alignment: 'left'
                    }
                }
            };



            pdfMake.createPdf(documentDefinition).download('PO ' + vm.export.POmodel.PONumber + '.pdf');





            //console.info("height:" + document.getElementById('print1').clientHeight);
            /*
            for (var i = 1; i <= vm.jumlahlembar; i++) {
                indeks = i.toString();
                html2canvas(document.getElementById('print'+i), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("PO#"+vm.export.POmodel.PONumber+" -"+indeks+".pdf");
                    }
                });
                indeks = "";
            }*/
        }

        function calculatePDF_height_width(selector, index) {
            page_section = $(selector).eq(index);
            HTML_Width = page_section.width();
            HTML_Height = page_section.height();
            top_left_margin = 15;
            PDF_Width = HTML_Width + (top_left_margin * 2);
            PDF_Height = (PDF_Width * 1.2) + (top_left_margin * 2);
            canvas_image_width = HTML_Width;
            canvas_image_height = HTML_Height;
        }




        //VendorBalance
        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        }
    }
})();
//TODO