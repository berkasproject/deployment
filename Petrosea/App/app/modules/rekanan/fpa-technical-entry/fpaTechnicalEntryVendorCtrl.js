﻿(function () {
	'use strict';

	angular.module("app").controller("fpaTechnicalEntryVendorCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'vhsTechEntryVendorService', '$stateParams', '$uibModal', '$translatePartialLoader', '$state', 'GlobalConstantService', '$filter'];

	function ctrl(UIControlService, vhsTechEntryVendorService, $stateParams, $uibModal, $translatePartialLoader, $state, GlobalConstantService, $filter) {
		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.IsSubmit = null;
		vm.SubmitDate = null;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
			GetRFQ();
			loadTender();
			loadSteps();
		}

		function checkStepDate(startDate, endDate) {
			var dateNow = new Date();
			var startDate = new Date(startDate);
			var endDate = new Date(endDate);
			vm.inProcess = false;
			if (dateNow >= startDate && dateNow <= endDate) {
				vm.inProcess = true;
			}
		}

		vm.loadSteps = loadSteps;
		function loadSteps() {
			vhsTechEntryVendorService.GetSteps({
				ID: vm.StepID
			}, function (reply) {
				vm.stepOvertime = reply.data;
				for (var i = 0; i < vm.stepOvertime.length; i++) {
					if (vm.stepOvertime[i].step.FormTypeURL == "fpa-technical-entry") {
						vm.accessPermission = vm.stepOvertime[i].IsOvertime;
					}
				}
				UIControlService.unloadLoading();
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
			});
		};

		vm.uploadDet = uploadDet;
		function uploadDet(data, flag) {
			var data = {
				TenderStepID: vm.StepID,
				act: flag,
				item: data,
				inProcess: vm.inProcess
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formDoc.html',
				controller: "frmDocCtrl",
				controllerAs: "frmDocCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.Insert = Insert;
		function Insert() {
			vm.cekDoc = true;
			if (vm.QuotationNo == null) {
				vm.cekDoc = false;
				UIControlService.msg_growl("error", 'ERR.QUOTATION');
				return;
			} else if (vm.selectedDeliveryTender.Name == "RFQ_CUSTOM") {
				if (vm.selectedIncoTerms == null) {
					vm.cekDoc = false;
					UIControlService.msg_growl("error", 'ERR.INCO');
					return;
				} else if (vm.selectedState == null) {
					vm.cekDoc = false;
					UIControlService.msg_growl("error", 'ERR.FREIGHT');
					return;
				}

			}
			if (vm.cekDoc == true) {
				var data = {
					QuotationNo: vm.QuotationNo,
					TenderStepID: vm.StepID,
					SupplierLeadTime: vm.RFQId.LeadTime,
					ExpiredDay: vm.RFQId.LeadTime,
					VHSOfferEntryDetails: vm.Template,
					PaymentTerms: vm.selectedPaymentTerm.Id
				}
				if (vm.selectedIncoTerms == null) data.IncoId = null;
				else data.IncoId = vm.selectedIncoTerms.ID;
				if (vm.selectedState == null) data.FreightCostDetailId = null;
				else data.FreightCostDetailId = vm.selectedState.ID;
				vhsTechEntryVendorService.InsertAll(data, function (reply) {
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
						init();
						//window.location.reload();
						// $state.transitionTo('detail-tahapan-vendor', { TenderID: vm.TenderID });
					} else {
						UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
				});
			}
		}

		function GetRFQ() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vhsTechEntryVendorService.selectRFQId({ Status: vm.TenderRefID }, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.RFQId = reply.data;
					loadDeliveryTerms(vm.RFQId);

					loadTemplate();
					loadReference();
					loadOptionsTender();
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Evaluasi Teknis" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.selectedPaymentTerm;
		vm.listPaymentTerm = [];
		function loadPaymentTerm(data) {
			vhsTechEntryVendorService.getPaymentTerm(function (reply) {
				UIControlService.unloadLoading();
				vm.listPaymentTerm = reply.data;
				for (var i = 0; i < vm.listPaymentTerm.length; i++) {
					if (data === vm.listPaymentTerm[i].Id) {
						vm.selectedPaymentTerm = vm.listPaymentTerm[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('detail-tahapan-vendor', { TenderID: vm.TenderID });
		}

		vm.cekLimit = cekLimit;
		function cekLimit() {
			vm.cekDoc = true;
			if (vm.QuotationNo == null) {
				vm.cekDoc = false;
				UIControlService.msg_growl("error", 'ERR.QUOTATION');
				return;
			} else if (vm.selectedDeliveryTender.Name == "RFQ_CUSTOM") {
				if (vm.selectedIncoTerms == null) {
					vm.cekDoc = false;
					UIControlService.msg_growl("error", 'ERR.INCO');
					return;
				} else if (vm.selectedState == null) {
					vm.cekDoc = false;
					UIControlService.msg_growl("error", 'ERR.FREIGHT');
					return;
				}
			}
			if (vm.IsFlagToSubmit == false) {
				UIControlService.msg_growl("error", 'MESSAGE.NOT_COMPLETE');
				return;
			}
			if (vm.cekDoc == true) {
				for (var i = 0; i < vm.Template.length; i++) {
					if (vm.Template[i].DocName == "Dokumen Penawaran" && vm.Template[i].DocumentUrl == null) {
						vm.cekDoc = false;
						UIControlService.msg_growl("error", 'ERR.DOCOFFERENTRY');
						return;
					}
				}
				if (vm.cekDoc == true) {
					if (vm.selectedOptionsTender.Name === "TENDER_OPTIONS_ITEMIZE") {
						SaveSubmit();
					} else {
						vhsTechEntryVendorService.cekLimit({
							Status: vm.TenderRefID,
							FilterType: vm.StepID
						}, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								if (reply.data == false) {
									UIControlService.msg_growl("error", "MESSAGE.MIN_OFFER");
									return;
								} else {
									UIControlService.loadLoading("MESSAGE.LOADING");
									SaveSubmit();
								}
							} else {
								$.growl.error({ message: "Gagal mendapatkan data" });
								UIControlService.unloadLoading();
							}
						}, function (err) {
							//$.growl.error({ message: "Gagal Akses API >" + err });
							UIControlService.unloadLoading();
						});
					}
				}
			}
		}

		vm.SaveSubmit = SaveSubmit;
		function SaveSubmit() {
			vhsTechEntryVendorService.InsertSubmit({ TenderStepID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUC_SUBMIT");
					init();
				} else {
					$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.deleteData = deleteData;
		function deleteData(data) {
			bootbox.confirm('<h3 class="afta-font">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '</h3>', function (res) {
				if (res) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					vhsTechEntryVendorService.deleteData({
						ID: data.ID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUC_DEL");
							init();
						}
						else {
							UIControlService.msg_growl("error", "MESSAGE.FAIL_DEL");
							return;
						}
					}, function (err) {

						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoading();
					});
				} else {
					console.info("sorry");
				}
			});

		}

		vm.viewDetail = viewDetail;
		function viewDetail() {
			var data = {
				StepID: vm.StepID,
				DocTypeID: 21
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/DetailPenawaranTechnical.html',
				controller: "DetailPenawaranCtrl",
				controllerAs: "DetailPenawaranCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.upload = upload;
		function upload(data) {
			$state.transitionTo('upload-file-vhs-technical', {
				VendorID: vm.Template[0].VendorID, DocTypeID: data.TenderDocTypeID, StepID: vm.StepID, TenderRefID: vm.TenderRefID
			});
		}

		vm.downloadExcel = downloadExcel;
		function downloadExcel() {
			var data = {
				TenderRefID: vm.TenderRefID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/fpa-technical-entry/TechnicalSaveExcelNotif.html',
				controller: "TechnicalSaveNotifExcel",
				controllerAs: "TechnicalSaveNotifExcel",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.selectedFreight;
		vm.listFreight = [];
		vm.changeFreightDetai = changeFreightDetai;
		function changeFreightDetai(data, flag) {
			if (data) vm.Id = data.IncoTerm;
			else vm.Id = vm.selectedIncoTerms.ID;
			vhsTechEntryVendorService.selectFreight({
				Status: vm.Id
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listFreight = reply.data;
				console.info(vm.flagFreight);
				if (data) {
					for (var i = 0; i < vm.listFreight.length; i++) {
						if (vm.flagFreight == 0) {
							if (data.FreightCostID === vm.listFreight[i].FreightCostId) {
								vm.selectedState = vm.listFreight[i];
								console.info(vm.selectedState);
								break;
							}
						} else {
							if (data.FreightCostID === vm.listFreight[i].ID) {
								vm.selectedState = vm.listFreight[i];
								console.info(vm.selectedState);
								break;
							}
						}

					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedIncoTerms;
		vm.listIncoTerms = [];
		vm.loadIncoTerms = loadIncoTerms;
		function loadIncoTerms(data, flag) {
			vm.flagFreight = flag;
			vhsTechEntryVendorService.getIncoTerms({
				BidderSelMethod: data.BidderSelMethod,
				DeliveryTerms: data.DeliveryTerms
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listIncoTerms = reply.data;
				for (var i = 0; i < vm.listIncoTerms.length; i++) {
					if (data.IncoTerm === vm.listIncoTerms[i].ID) {
						vm.selectedIncoTerms = vm.listIncoTerms[i];
						vm.changeFreightDetai(data, flag);
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedOptionsTender;
		vm.loadOptionsTender = loadOptionsTender;
		function loadOptionsTender() {
			vhsTechEntryVendorService.getOptionsTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listOptionsTender = reply.data.List;
				for (var i = 0; i < vm.listOptionsTender.length; i++) {
					if (vm.RFQId.TenderOption === vm.listOptionsTender[i].RefID) {
						vm.selectedOptionsTender = vm.listOptionsTender[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadReference = loadReference;
		function loadReference() {
			vhsTechEntryVendorService.DeliveryTerm({}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.DeliveryTerm = reply.data.List;
					for (var i = 0; i < vm.DeliveryTerm.length; i++) {
						if (vm.RFQId.TenderType == vm.DeliveryTerm[i].RefID) vm.selectedDeliveryTerms = vm.DeliveryTerm[i];
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.uploadDetBC = uploadDetBC;
		function uploadDetBC(data, flag) {
			var data = {
				TenderStepID: vm.StepID,
				act: flag,
				item: data,
				Limit: vm.RFQId.Limit,
				inProcess: vm.inProcess
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-vhs/formBC.html',
				controller: "frmBCCtrl",
				controllerAs: "frmBCCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.loadTemplate = loadTemplate;
		function loadTemplate() {
			vm.IsFlagToSubmit = true;
			vm.IsFlagQuotation = true;
			vm.IsFlagInco = true;
			vm.IsFlagDeliv = true;
			vm.j = 0;
			var tender = {
				Status: vm.TenderRefID,
				column: vm.StepID
			}
			vhsTechEntryVendorService.select(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.Template = reply.data;
					loadPaymentTerm(vm.Template[0].vhs.PaymentTerms);
					vm.QuotationNo = vm.Template[0].vhs.QuotationNo;

					if (vm.Template[0].vhs.QuotationNo == null)
						vm.IsFlagQuotation = false;

					if (vm.RFQId.DeliveryTerms !== 3087) {
						if (vm.Template[0].vhs.IncoId == null) vm.IsFlagInco = false;
						if (vm.Template[0].vhs.FreightCostDetailId == null) vm.IsFlagDeliv = false;
					}

					for (var i = 0; i < vm.Template.length; i++) {
						if (vm.Template[i].IsPublishTechnical == true) {
							vm.IsSubmit = vm.Template[i].vhs.IsSubmitTechnical;
							vm.SubmitDate = UIControlService.convertDateTime(vm.Template[i].vhs.SubmitDateTechnical);
						}
						if (vm.Template[i].IsPublishTechnical == false) {
							vm.IsFlagToSubmit = false;
						}
					}

					if (vm.Template[0].vhs.IncoId !== null) {
						vm.VHSData = {
							IncoTerm: vm.Template[0].vhs.IncoId,
							FreightCostID: vm.Template[0].vhs.FreightCostDetailId,
							BidderSelMethod: vm.RFQId.BidderSelMethod,
							DeliveryTerms: vm.RFQId.DeliveryTerms
						}
						loadIncoTerms(vm.VHSData, 1);
					} else {
						loadIncoTerms(vm.RFQId, 0);
					}

					for (var i = 0; i < vm.Template.length; i++) {
						if (vm.j == 0 && vm.Template[i].ID !== 0) {
							vm.QuotationNo = vm.Template[i].vhs.QuotationNo;
							vm.j = 1;
						}
						if (i === vm.Template.length - 1 && vm.j == 0) { }
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.selectedDeliveryTender;
		vm.listDeliveryTerms = [];
		vm.loadDeliveryTerms = loadDeliveryTerms;
		function loadDeliveryTerms(data) {
			vhsTechEntryVendorService.getDeliveryTerms(function (reply) {
				UIControlService.unloadLoading();
				vm.listDeliveryTerms = reply.data.List;

				for (var i = 0; i < vm.listDeliveryTerms.length; i++) {
					if (data.DeliveryTerms === vm.listDeliveryTerms[i].RefID) {
						vm.selectedDeliveryTender = vm.listDeliveryTerms[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadTender = loadTender;
		function loadTender() {
			vhsTechEntryVendorService.selectStep({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.step = reply.data;
					var startDate = vm.step.StartDate;
					var endDate = vm.step.EndDate;
					checkStepDate(startDate, endDate);
					vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
					vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
					vm.TenderRefID = vm.step.tender.TenderRefID;
					vm.ProcPackType = vm.step.tender.ProcPackageType;
					vm.TenderID = vm.step.TenderID;
					vm.StartDateFull = new Date(vm.step.StartDate);
					vm.EndDateFull = new Date(vm.step.EndDate);
					vm.today = new Date();
					//console.info("tender::" + JSON.stringify(vm.dataTenderReal));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Chatting" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
	}
})();