﻿(function () {
    'use strict';

    angular.module("app").controller("cprvendorCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'CprVendorService',
        '$state', 'UIControlService', 'GlobalConstantService',
        '$uibModal', '$stateParams'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, CprVendorService,
        $state, UIControlService, GlobalConstantService,
        $uibModal, $stateParams) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.CPRID = Number($stateParams.CPRID);
        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");
            //console.info("cprID:"+vm.CPRID);
            /*
            CprVendorService.cekRole(
            function (reply) {
               if (reply.status === 200) {
                   vm.flagrole = reply.data;
                   vm.FlagRole = vm.flagrole.flagRole;
                   console.info(vm.FlagRole);
               } else {
                   UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
               }
            }, function (err) {
               UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
            */
            loadAwal();
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            loadAwal(1);
        };

        vm.loadAwal = loadAwal;
        function loadAwal(current) {
            vm.currentPage = current;
            UIControlService.loadLoading("");
            CprVendorService.CPRbyContract({
                Status:vm.CPRID,
                Limit: vm.pageSize,
                Offset: vm.pageSize * (vm.currentPage - 1),
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.cpr = reply.data.List;
                    console.info("dataCPR:" + JSON.stringify(vm.cpr));
                    vm.count = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });
        };

        vm.detail = detail;
        function detail(wl) {
            var lempar = {
                //flagRole: vm.FlagRole,
                VPCPRDataId: wl.VPCPRDataId,
                data:wl
                //IsVhsCpr: wl.IsVhsCpr
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/cpr-vendor/cpr-vendor.modal.html?v=1.000002',
                controller: 'cprVendorModal',
                controllerAs: 'cprVendorModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };
    }
})();