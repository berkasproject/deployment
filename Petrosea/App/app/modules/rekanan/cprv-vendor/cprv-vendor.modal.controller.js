﻿(function () {
    'use strict';

    angular.module("app")
    .controller("vhsVendorModal", ctrl);

    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'GlobalConstantService', 'CprvVendorService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, GlobalConstantService, CprvVendorService, UIControlService, item) {

        var vm = this;
        vm.VPVHSDataId = item.VPVHSDataId;
        vm.type = item.IsVhsCpr;
        vm.flagRole = item.flagRole;
        vm.data = item.data;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        var kr = [];
        var krLv1 = [];
        var krLv2 = [];
        var krLv3 = [];
        var vendorName = "";

        vm.init = init;
        function init() {

            CprvVendorService.selectbyid({
                VPVHSDataId: vm.VPVHSDataId
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vpdata = reply.data[0];
                    vendorName = vm.vpdata.VendorName;
                    vm.approvalstatus = vm.vpdata.StatusDescription;
                    loadDocs();

                    for (var i = 0; i < vm.vpdata.VPVHSDataDetails.length; i++) {
                        kr.push(vm.vpdata.VPVHSDataDetails[i]);
                    }

                    for (var i = 0; i < kr.length; i++) {
                        if (kr[i].Level === 1) {
                            krLv1.push(kr[i]);
                        }
                        else if (kr[i].Level === 2) {
                            krLv2.push(kr[i]);
                        }
                        else if (kr[i].Level === 3) {
                            krLv3.push(kr[i]);
                        }
                    }

                    for (var i = 0; i < krLv2.length; i++) {
                        krLv2[i].sub = [];
                        for (var j = 0; j < krLv3.length; j++) {
                            if (krLv3[j].Parent === krLv2[i].CriteriaId) {
                                krLv2[i].sub.push(krLv3[j]);
                            }
                        }
                    }

                    for (var i = 0; i < krLv1.length; i++) {
                        krLv1[i].sub = [];
                        for (var j = 0; j < krLv2.length; j++) {
                            if (krLv2[j].Parent === krLv1[i].CriteriaId) {
                                krLv1[i].sub.push(krLv2[j]);
                            }
                        }
                    }

                    vm.kriteria = krLv1;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
            });
        };

        vm.docs = [];
        vm.loadDataDocPostCPR = loadDataDocPostCPR;
        function loadDataDocPostCPR() {
            CprvVendorService.selectDocsCPRId({
                VPVHSDataId: vm.VPVHSDataId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoading();
            });
        }

        function loadDocs() {
            UIControlService.loadLoadingModal("");
            CprvVendorService.getDocs({
                VPVHSDataId: vm.VPVHSDataId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoadingModal();
            });
        };


        vm.valid = true;
        vm.NoteFromVendor = "";
        vm.notvalid = function notvalid() {
            vm.valid = false;
        }

        vm.submitByVendor = function submitByVendor(param) {
            vm.valid = param;
            //console.info("param:" + param);
            if (param == true) {
                //vm.status = 4330;
                vm.status = "STS_ACCVENDOR";
            }
            else if (param == false) {
                //vm.status = 4421;
                vm.status = "STS_REVISIONBYVENDOR";
            }
            CprvVendorService.submitbyvendor({
                VPVHSDataId: vm.vpdata.VPVHSDataId,
                //StatusDescription: vm.status,
                SysReference: { Name: vm.status },
                NoteFromVendor: vm.NoteFromVendor
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCCESS_SUBMITDATA'));
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
                    //UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();