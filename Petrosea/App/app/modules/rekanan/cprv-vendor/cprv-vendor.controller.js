﻿(function () {
    'use strict';

    angular.module("app").controller("vhsvendorCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'CprvVendorService',
        '$state', 'UIControlService', 'GlobalConstantService',
        '$uibModal', '$stateParams'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, CprvVendorService,
        $state, UIControlService, GlobalConstantService,
        $uibModal, $stateParams) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.VPVHSID = Number($stateParams.VPVHSID);

        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            /*
            CprvVendorService.cekRole(
            function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info(vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
            */
            loadAwal();
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.pageNumber = 1;
            loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            UIControlService.loadLoading("");
            CprvVendorService.CPRbyContract({
                Status: vm.VPVHSID,
                Limit: vm.pageSize,
                Offset: vm.pageSize * (vm.pageNumber - 1),
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vpvhs = reply.data.List;
                    //console.info("dataCPR:" + JSON.stringify(vm.vpvhs));
                    vm.count = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });
        };

        vm.detail = detail;
        function detail(wl) {
            var lempar = {
                //flagRole: vm.FlagRole,
                VPVHSDataId: wl.VPVHSDataId,
                data: wl
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/cprv-vendor/cprv-vendor.modal.html',
                controller: 'vhsVendorModal',
                controllerAs: 'vhsVendorModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };
    }
})();