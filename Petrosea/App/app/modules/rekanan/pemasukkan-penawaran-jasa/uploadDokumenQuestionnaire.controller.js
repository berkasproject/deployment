﻿(function () {
	'use strict';

	angular.module("app").controller("uploadDokumenQuestionnaireCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'OfferEntryService',
        '$state', 'UploadFileConfigService', 'UIControlService', 'UploaderService', 'item', '$uibModalInstance',
        'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, OEService, $state, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {
	    var vm = this;
	    vm.item = item;
		vm.docurl = null;
		vm.DocumentName = item.DocName;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.OfferEntryVendorID = item.OfferEntryVendorID;
		vm.OfferEntryQuestionaireID = item.OfferEntryQuestionaireID;
		vm.fileUpload = null;

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('pemasukkan-penawaran-jasa');
			loadTypeSizeFile();
			//console.info("ap"+JSON.stringify(vm.accessPermission));
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		function loadTypeSizeFile() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.ADMIN.TENDER.QUESTIONNAIRESERVICEOFFER", function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				return;
			});
		}

		vm.uploadFile = uploadFile;
		function uploadFile(data) {
		    console.log(vm.fileUpload)
		    console.log(data)
		    if (vm.fileUpload != null) {
		        var folder = "QUESTIONNAIRE_TENDER_JASA";
		        var tipefileupload = typefile(vm.fileUpload, vm.idUploadConfigs);
		        if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder, tipefileupload, data);
		        }
		    } else {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		        return;
		    }
		}

		function typefile(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}

			var selectedFileType = file[0].type;
			selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
			//console.info("tipefile: " + selectedFileType);
			if (selectedFileType === "vnd.ms-excel") {
				selectedFileType = "xls";
			} else if (selectedFileType === "vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
				selectedFileType = "xlsx";
			} else if (selectedFileType === "application/msword") {
				selectedFileType = "doc";
			} else if (selectedFileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || selectedFileType == "vnd.openxmlformats-officedocument.wordprocessingml.document") {
				selectedFileType = "docx";
			} else {
				selectedFileType = selectedFileType;
			}
			return selectedFileType;
			//console.info("file:" + selectedFileType);
		}
        
		function upload(file, config, filters, folder, tipefile, data) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			//console.info(file[0].size + ":" + file[0].type);
			var tipesize_file = tipefile + " / " + Math.floor(file[0].size / 1024) + " KB";
			console.info(tipesize_file);

			UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, folder, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.docurl = url;
					//vm.dataExp.DocumentURL = url;
					//vm.pathFile = vm.folderFile + url;
					UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
					//saveProcess(url, tipesize_file);
					saveQuest(url);
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});
		}

		vm.saveQuest = saveQuest;
		function saveQuest(url) {
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    vm.data = [];
		    vm.data.push(vm.OfferEntryQuestionaireID);
		    vm.data.push(vm.docurl);
		    $uibModalInstance.close(vm.data);
		    UIControlService.unloadLoading();
		    //OEService.insertDocUrl({
		    //    UploadURL: url,
		    //    OfferEntryVendorID: vm.OfferEntryVendorID,
		    //    OfferEntryQuestionaireID: vm.OfferEntryQuestionaireID
		    //}, function (reply) {
		    //    UIControlService.unloadLoading();
		    //    if (reply.status === 200) {
		    //        $uibModalInstance.close();
		    //        vm.init();
		    //    }
		    //}, function (err) {
		    //    UIControlService.msg_growl("error", "MESSAGE.API");
		    //    UIControlService.unloadLoading();
		    //});
		}
	}
})();
