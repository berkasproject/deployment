﻿(function () {
  'use strict';

  angular.module('app').factory('prequalResultAnnounceService', serviceMethod);

  serviceMethod.$inject = ['GlobalConstantService'];

  /* @ngInject */
  function serviceMethod(GlobalConstantService) {
    var endpoint = GlobalConstantService.getConstant("api_endpoint");
    var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
    var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

    // interfaces
    var service = {
      getAnnouncement: getAnnouncement,
      insertRegistration: insertRegistration,
      getByOpenPrequal: getByOpenPrequal,
      isRegistrationOpened: isRegistrationOpened
    };
    return service;

    function getAnnouncement(param, successCallback, errorCallback) {
      GlobalConstantService.post(vendorpoint + "/prequalResult/getAnnouncement", param).then(successCallback, errorCallback);
    }

    function insertRegistration(param, successCallback, errorCallback) {
      GlobalConstantService.post(vendorpoint + "/prequal/insertRegistration", param).then(successCallback, errorCallback);
    }

    function getByOpenPrequal(param, successCallback, errorCallback) {
      GlobalConstantService.post(vendorpoint + "/prequal/getByOpenPrequal", param).then(successCallback, errorCallback);
    }

    function isRegistrationOpened(param, successCallback, errorCallback) {
      GlobalConstantService.post(vendorpoint + "/prequal/isRegistrationOpened", param).then(successCallback, errorCallback);
    }
  }
})();