﻿(function () {
	'use strict';

	angular.module("app").controller('prequalAnnounceVendorCtrl', ctrl);

	ctrl.$inject = ['PrequalAnnounceVendorService', '$translatePartialLoader', 'UIControlService', '$uibModal'];
	function ctrl(PrequalAnnounceVendorService, $translatePartialLoader, UIControlService, $uibModal) {
		var vm = this;
		vm.keyword = '';
		vm.pageSize = 10;
		vm.currentPage = 1;

		vm.IsRegistered = true;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('prequal-announcement-vendor');
			getPrequalAnnounceVendor(vm.currentPage);
			cekPrakualifikasiVendor();
		}

		vm.getPrequalAnnounceVendor = getPrequalAnnounceVendor;
		function getPrequalAnnounceVendor(currPage) {
			vm.currentPage = currPage;
			PrequalAnnounceVendorService.getPrequalAnnounce({
				Keyword: vm.keyword,
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize
			}, function (reply) {
				UIControlService.loadLoadingModal('LOADING.PREQUALANNOUNCE');
				if (reply.status === 200) {
				    vm.prequalAnnounce = reply.data;
				    for (var i = 0; i <= vm.prequalAnnounce.List.length - 1; i++) {
				        if (vm.prequalAnnounce.List[i].IsRegistered == false) {
				            vm.IsRegistered = false;
				        }
				    }
				    console.info("isregistered" + vm.IsRegistered);
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
			});
		}

		vm.announcementDetail = announcementDetail;
		function announcementDetail(data) {
			var modalInstance = $uibModal.open({
				templateUrl: 'prequal-announcement-vendor-detail.html',
				controller: "prequalAnnounceVendorDtlController",
				controllerAs: "prequalAnnounceVendorDtlController",
				resolve: { item: function () { return data; } }
			});

			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
		function cekPrakualifikasiVendor() {
		    PrequalAnnounceVendorService.cekPrakualifikasiVendor(function (reply) {
		        UIControlService.loadLoadingModal('LOADING.PREQUALANNOUNCE');
		        if (reply.status === 200) {
		            vm.isregistered = reply.data;
		            console.info("isregistered" + vm.isregistered);
		            UIControlService.unloadLoadingModal();
		        } else {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		    });
		}

		vm.seeRegistration = seeRegistration;
		function seeRegistration(data) {
			//data.DocUrl = encodeURI(data.DocUrl);

			var data = {
				item: data
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/prequalification/prequalAnnouncement/prequalRegistration.html',
				controller: "prequalRegistrationCtrl",
				controllerAs: "prequalRegistrationCtrl",
				resolve: { item: function () { return data; } }
			});

			modalInstance.result.then(function () {
				vm.init(1);
			});
		}
	}
})();