﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailPersyaratanVendorCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PengumumanPengadaanService', '$state', 'UIControlService', 'item', '$uibModalInstance',
         'UploaderService', '$uibModal', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PengumumanPengadaanService,
        $state, UIControlService, item, $uibModalInstance, UploaderService, $uibModal, GlobalConstantService) {
        console.info(">>" + JSON.stringify(item));
        var vm = this;
        vm.TenderRefID = item.TenderRefID;
        
        vm.init = init;
        function init() {
            console.info("tendRef:" + vm.TenderRefID);
            loadDataOfferEntry();
        }
        function loadDataOfferEntry() {
            PengumumanPengadaanService.loadTemplateOfferEntry({
                Status: 25,
                column: 191
            }, function (reply) {
                console.info("offEntry" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.data = reply.data;
                    for (var i = 0; i < vm.data.length; i++) {
                        if (vm.data[i].DocName === "Surat Penawaran") {
                            vm.data[i].DocName += ' ' + vm.RFQId.Limit + '%';
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.close = close;
        function close() {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();