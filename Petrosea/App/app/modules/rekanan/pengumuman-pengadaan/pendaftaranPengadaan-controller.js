(function () {
	'use strict';

	angular.module("app").controller("PendaftaranPengadaanCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PengumumanPengadaanService', '$filter', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploaderService', 'UploadFileConfigService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PengumumanPengadaanService,
        $filter, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploaderService, UploadFileConfigService) {
		var vm = this;
		vm.data = item.item;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.TanggalHariIni = new Date();
		vm.textSearch = '';
		vm.listPengumuman = [];
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.statusMinat = false;


		vm.tenderInterest = {
			TenderStepDataID: null,
			TenderID: null,
			TenderInterestVendorDoc: null
		}

		vm.tenderInterestDoc = [];

		vm.tenderInterestDocData = null;

		vm.init = init;
		function init() {
			console.info(JSON.stringify(vm.data));
			fileConfig();
			$translatePartialLoader.addPart("pengumuman-pengadaan-client");
			//loadDataVendor(1);
		}



		vm.tglSekarang = UIControlService.getDateNow("");
		vm.urlTenderDoc = null;
		vm.addTenderDoc = addTenderDoc;
		function addTenderDoc(url) {
			if (vm.tenderInterestDocData != null) {
				if (url != '') {
					vm.tenderInterestDocData.DocumentUrl = url;
					vm.tenderInterestDoc.push(vm.tenderInterestDocData);
					vm.tenderInterestDocData = null;
				} else {
					if (vm.docTender == undefined || vm.tenderInterestDocData.DocumentName == undefined) {
						UIControlService.msg_growl("err", "Tidak ada file yang dipilih");
						return
					} else {
						if (validateFileType(vm.docTender, vm.idUploadConfigs)) {
							uploadSingleDoc(vm.docTender, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
						}
					}
				}
			}
			else {
				UIControlService.msg_growl('warning', "Dokumen tender harus lengkap");
				return;
			}
			console.info("temp Doc:" + JSON.stringify(vm.tenderInterestDoc));
		}

		vm.removeTenderDoc = removeTenderDoc;
		function removeTenderDoc(obj) {
			vm.tenderInterestDoc = remove(vm.tenderInterestDoc, obj);
		}

		function remove(list, obj) {
			var index = list.indexOf(obj);
			if (index >= 0) {
				list.splice(index, 1);
			} else {
				UIControlService.msg_growl('error', "ERRORS.OBJECT_NOT_FOUND");
			}
			return list;
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.handleRequestError("ERRORS.INVALID_FILETYPE!");
				return false;
			}

			var selectedFileType = file[0].type;
			selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
			var allowed = false;
			for (var i = 0; i < allowedFileTypes.length; i++) {
				if (allowedFileTypes[i].Name == selectedFileType) {
					allowed = true;
					return allowed;
				}
			}

			if (!allowed) {
				UIControlService.handleRequestError("ERRORS.INVALID_FILETYPE!");
				return false;
			}
		}


		function uploadSingleDoc(file, config, filters, dates) {
			//console.info("doctype:" + doctype);
			//console.info("file"+JSON.stringify(file));
			var size = config.Size;

			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoading("LOADING");
			UploaderService.uploadSingleFileLibrary(file, size, filters, function (response) {
				//console.info("upload:" + JSON.stringify(response.data));
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					var fileName = response.data.FileName;
					vm.pathFile = url;
					addTenderDoc(vm.pathFile);
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});

		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
			//$uibModalInstance.close();
		};

		function fileConfig() {
			UploadFileConfigService.getByPageName('PAGE.VENDOR.REGISTRATION.ID', function (response) {
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		/*
        function loadDataVendor(current) {
            var offset = (current * 10) - 10;
            PengumumanPengadaanService.selectDataVendor({
                Keyword: '', Offset: offset, Limit: 10
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.vendorID = reply.data[0].VendorID;
                    //console.info("vendor ID" + JSON.stringify(vm.vendorID));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }
        */

		vm.changeStatusMinat = changeStatusMinat;
		function changeStatusMinat() {
			vm.statusMinat = true;
		}


		vm.minat = minat;
		function minat() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			PengumumanPengadaanService.insertRegistration({
				//TenderAnnounID: vm.data.TenderAnnounID,
				//VendorID: vm.vendorID,
				//IsSurvive: 1, 
				//IsResign: 0, 
				LastTenderStepID: vm.data.TenderStepData.ID,
				TenderID: vm.data.TenderStepData.TenderID
			}, function (reply) {
				console.info(JSON.stringify(reply.status));
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", "MESSAGE.SUC_REG_TENDER");
				$uibModalInstance.close();
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_REG_TENDER");
				if (err.Message.substr(0, 4) === "ERR_") {
					UIControlService.msg_growl("error", $filter('translate')('MESSAGE.' + err.Message));
				}
				UIControlService.unloadLoadingModal();
			});
		}

		vm.minatROI = minatROI;
		function minatROI() {
			if (vm.tenderInterestDoc.length != 0) {
				vm.tenderInterest.TenderStepDataID = vm.data.TenderStepData.ID;
				vm.tenderInterest.TenderID = vm.data.TenderStepData.TenderID;
				vm.tenderInterest.TenderInterestVendorDoc = vm.tenderInterestDoc;
				//console.info("temp:" + JSON.stringify(vm.tenderInterest));
				UIControlService.loadLoadingModal("MESSAGE.LOADING");
				PengumumanPengadaanService.insertRegistrationROI(vm.tenderInterest, function (reply) {
					//console.info(JSON.stringify(reply.status));
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("success", "MESSAGE.SUC_REG_TENDER");
					$uibModalInstance.close();
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.ERR_REG_TENDER");
					if (err.Message.substr(0, 4) === "ERR_") {
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.' + err.Message));
					}
					UIControlService.unloadLoadingModal();
				});
			} else {
				UIControlService.msg_growl("warning", "Dokumen pendaftaran tender harus diisi");
				return;
			}
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}
	}
})();
