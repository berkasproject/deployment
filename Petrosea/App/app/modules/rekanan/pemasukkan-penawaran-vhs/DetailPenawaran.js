﻿(function () {
	'use strict';

	angular.module("app").controller("DetailPenawaranCtrl", ctrl);

	ctrl.$inject = ['$timeout', 'Excel', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PPVHSService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService'];
	function ctrl($timeout, Excel, $http, $translate, $translatePartialLoader, $location, SocketService, PPVHSService, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.DocTypeID = item.DocTypeID;
		vm.StepID = item.StepID;
		vm.TenderType = item.TenderType;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.maxSize = 10;
		function init() {
			$translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
			loadAwal(1);

		}

		vm.loadAwal = loadAwal;
		function loadAwal(current) {
			vm.currentPage = current;
			var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
			PPVHSService.selectDetail({
				TenderDocTypeID: vm.DocTypeID,
				vhs: { TenderStepID: vm.StepID },
				Offset: offset,
				Limit: vm.pageSize
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.newExcel = [];
				if (reply.status === 200) {
					vm.det = reply.data.List;
					vm.totalItems = Number(reply.data.Count);
					for (var i = 0; i < vm.det.length; i++) {

						var objExcel = {
							MaterialCode: vm.det[i].MaterialCode,
							Estimate: vm.det[i].Estimate,
							Unit: vm.det[i].Unit,
							ItemDescrip: vm.det[i].ItemDescrip,
							Manufacture: vm.det[i].Manufacture,
							PartNo: vm.det[i].PartNo,
							Currency: vm.det[i].Currency,
							PriceIDR: vm.det[i].PriceIDR,
							LeadTime: vm.det[i].LeadTime,
							CountryOfOrigin: vm.det[i].CountryOfOrigin,
							Remark: vm.det[i].Remark,
                            Alternatif: vm.det[i].Alternatif
						}

						vm.newExcel.push(objExcel);
						if (i == vm.det.length - 1) {
							console.info(vm.newExcel);
						}

					}
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_TECHNICAL" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};


	}
})();