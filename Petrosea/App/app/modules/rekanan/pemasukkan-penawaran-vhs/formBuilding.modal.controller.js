(function () {
	'use strict';

	angular.module("app").controller("FormBuildingCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'PPVHSService', '$uibModalInstance', 'UploadFileConfigService', 'DataPerlengkapanService', 'VendorExperienceService', 'item', 'ProvinsiService', 'UIControlService', 'UploaderService', 'AuthService'];
	/* @ngInject */
	function ctrl($translatePartialLoader, PPVHSService, $uibModalInstance, UploadFileConfigService, DataPerlengkapanService, VendorExperienceService, item, ProvinsiService, UIControlService, UploaderService, AuthService) {
		var vm = this;
		vm.dataBuilding = {};
		vm.isAdd = item.isForm;
		vm.tipeform = item.type;
		vm.title = "FORM.BUILDINGTITLE"; //Data Bangunan dan Infrastruktur
		vm.FullAddress;
		vm.isCategory = item.category;
		var categoryID;
		vm.item = item.item;
		vm.TenderStepID = item.TenderStepID;
		vm.isCalendarOpened = [false, false, false, false];
		vm.minDate = new Date();

		vm.init = init;
		function init() {
		    console.info(vm.minDate);
			$translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
			if (item.isAdd === false && item.isAdd !== undefined) {
				if (vm.tipeform == 18) {
					loadTypeExp();
					vm.dataExp = item.item;
					loadCountries();
					loadTypeTender();
					getTypeSizeFile();
					loadCurrency();
					vm.dataExp.StartDate = new Date(vm.dataExp.StartDate);
					vm.dataExp.EndDate = new Date(vm.dataExp.EndDate);
				}
			}
			console.info("editExp" + JSON.stringify(vm.dataExp));
			if (vm.isAdd === true) {
				vm.dataBuilding = new BuildingField(0, 0, 0, "", 0);
			}
			if (vm.tipeform == 17 || vm.tipeform == 19) {
				if (vm.item !== undefined) {
					vm.ID = vm.item.ID;
					vm.dataBuilding = new BuildingField(vm.item.VendorID, vm.item.category, vm.item.BuildingArea,
                        vm.item.address.AddressInfo, vm.item.ownershipStatus);
					vm.FullAddress = vm.item.address.AddressInfo + " , " + vm.item.address.AddressDetail
					getProvinsi();
					loadCategory();
					loadKepemilikan(vm.item.OwnershipStatus);
				} else {
					getProvinsi();
					loadCategory();
					loadKepemilikan();
					vm.dataBuilding = new BuildingField(0, 0, 0, "", 0);
				}
			}
			if (vm.tipeform == 18 && item.isAdd === undefined) {
				loadTypeExp();
				vm.dataExp = new dataField('', null, '', '', '', '', 0, null, null, new Date(), 0, 0, '', 0, '', null, 0);
				loadCountries();
				loadTypeTender();
				getTypeSizeFile();
				loadCurrency();
			}

			if (vm.tipeform == 20) {
				vm.datenow = new Date();
				vm.datepickeroptions = {
					minMode: 'year',
					maxDate: vm.datenow
				}
				if (vm.isCategory === "vehicle") {
					vm.title = "Vehicle Data"; //Data Kendaraan
					categoryID = 3136;
				}
				if (vm.isCategory === "equipment") {
					vm.title = "Equipment Data"; //Data Peralatan
					categoryID = 3137;
				}
				if (vm.item !== undefined) {
					vm.dataForm = new Field(vm.item.Brand, vm.item.Capacity, categoryID, vm.item.Condition,
                    new Date(Date.parse(vm.item.MfgDate)), vm.item.Name, vm.item.Ownership, vm.item.SerialNo, vm.item.Type);
					vm.ID = vm.item.ID;
					vm.dateconvert = UIControlService.getStrDate(vm.dataForm.MfgDate);
					loadConditional(vm.item.Condition);
					loadKepemilikan(vm.item.Ownership);
				} else {
					vm.dataForm = new Field("", "", categoryID, 0, null, "", 0, "", "");
					vm.ID = 0;
					loadConditional();
					loadKepemilikan();
				}
			}

		}

		vm.changeCurr = changeCurr;
		function changeCurr(data) {
			console.info(data);
			vm.dataExp.ExpCurrID = data.CurrencyID;
		}

		vm.loadCurrency = loadCurrency;
		vm.selectedCurrencies = [];
		function loadCurrency(data) {
			VendorExperienceService.getCurrencies(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listCurrencies = reply.data;
					if (vm.isAdd === false) {
						console.info(data.ExpCurrID);
						for (var i = 0; i < vm.listCurrencies.length; i++) {
							if (data.ExpCurrID === vm.listCurrencies[i].CurrencyID) {
								vm.selectedCurrencies = vm.listCurrencies[i];
								break;
							}
						}
					}
					if (item.isAdd === false) {
						for (var i = 0; i < vm.listCurrencies.length; i++) {
							if (vm.dataExp.ExpCurrID === vm.listCurrencies[i].CurrencyID) {
								vm.selectedCurrencies = vm.listCurrencies[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.listBussinesDetailField = []
		vm.changeTypeTender = changeTypeTender;
		function changeTypeTender() {
			if (vm.selectedTypeTender === undefined) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEVENDOR");
				return;
			}
			vm.dataExp.FieldType = vm.selectedTypeTender.RefID;
			loadBusinessField();
			vm.listBussinesDetailField = [];
		}

		vm.loadBusinessField = loadBusinessField;
		vm.selectedBusinessField;
		vm.listBusinessField = [];
		function loadBusinessField() {
			console.info(vm.dataExp.Field);
			VendorExperienceService.SelectBusinessField({
				GoodsOrService: vm.dataExp.FieldType
			}, function (response) {
				if (response.status === 200) {
					vm.listBusinessField = response.data;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listBusinessField.length; i++) {
							if (vm.listBusinessField[i].ID === vm.dataExp.Field) {
								vm.selectedBusinessField = vm.listBusinessField[i];
								break;
							}
						}
					}
					if (item.isAdd === false) {
						for (var i = 0; i < vm.listBusinessField.length; i++) {
							if (vm.listBusinessField[i].ID === vm.dataExp.Field) {
								vm.selectedBusinessField = vm.listBusinessField[i];
								break;
							}
						}
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_BUSINESS_FIELD"); //Gagal mendapatkan list bidang usaha
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");//Gagal Akses API
				return;
			});
		}

		vm.changeBusinessField = changeBusinessField;
		function changeBusinessField() {
			//console.info("field:" + JSON.stringify(vm.selectedBusinessField));
			vm.dataExp.Field = vm.selectedBusinessField.ID;
		}

		vm.loadStates = loadStates;
		function loadStates(country) {
			if (!country) {
				country = vm.selectedCountry;
				vm.selectedState = "";
			}
			loadRegions(country.CountryID);
			UIControlService.loadLoading("LOADERS.LOADING_STATE");
			VendorExperienceService.getStates(country.CountryID, function (response) {
				vm.stateList = response.data;
				for (var i = 0; i < vm.stateList.length; i++) {
					if (country.StateID === vm.stateList[i].StateID) {
						vm.selectedState = vm.stateList[i];
						console.info("...." + JSON.stringify(vm.selectedState.Country.Code));
						if (vm.selectedState.Country.Code === 'ID') {
							changeState();
							break;
						}
					}
				}
				if (item.isAdd === false) {
					console.info("masukk");
					for (var i = 0; i < vm.stateList.length; i++) {
						if (vm.dataExp.StateLocation === vm.stateList[i].StateID) {
							console.info("msk");
							vm.selectedState = vm.stateList[i];
							changeState();
							break;
						}
					}
				}
				UIControlService.unloadLoading();
			});
		}

		vm.loadCountries = loadCountries;
		function loadCountries(data) {
			UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
			//console.info("loadCountries");
			VendorExperienceService.getCountries(function (response) {
				vm.countryList = response.data;
				for (var i = 0; i < vm.countryList.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.countryList[i].CountryID) {
							vm.selectedCountry = vm.countryList[i];
							loadStates(data);
							break;
						}
					}
				}
				UIControlService.unloadLoading();
			});
		}

		vm.loadRegions = loadRegions;
		function loadRegions(data) {
			UIControlService.loadLoading("LOADERS.LOADING_REGION");
			VendorExperienceService.getRegions({ CountryID: data }, function (response) {
				vm.selectedRegion = response.data;
				UIControlService.unloadLoading();
			});
		}

		vm.changeState = changeState;
		vm.listCities = [];
		vm.selectedCities;
		function changeState() {
			console.info(vm.dataExp.Location);
			if (!(vm.selectedState.CountryID === vm.IDN_id)) {
				vm.dataExp.Location = null;
				vm.dataExp.StateLocation = vm.selectedState.StateID;
			}
			ProvinsiService.getCities(vm.selectedState.StateID, function (response) {
				vm.listCities = response.data;
				console.info(vm.dataExp.Location);
				if (vm.isAdd === false) {
					for (var i = 0; i < vm.listCities.length; i++) {
						if (vm.dataExp.Location === vm.listCities[i].CityID) {
							vm.selectedCities = vm.listCities[i];
							changeCities();
							break;
						}
					}
				}
				if (item.isAdd === false) {
					for (var i = 0; i < vm.listCities.length; i++) {
						if (vm.dataExp.CityLocation.Name === vm.listCities[i].Name) {
							vm.selectedCities = vm.listCities[i];
							changeCities();
							break;
						}
					}
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API"); //Gagal Akses API
				return;
			});
		}

		vm.changeCitiesExp = changeCitiesExp;
		function changeCitiesExp() {
			console.info(JSON.stringify(vm.selectedCities));
			vm.dataExp.StateLocation = vm.selectedState.StateID;
			vm.dataExp.Location = vm.selectedCities.CityID;

		}


		vm.selectedTypeTender;
		vm.listTypeTender;
		function loadTypeTender() {
			VendorExperienceService.getTypeTender(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTypeTender = reply.data.List;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listTypeTender.length; i++) {
							if (vm.listTypeTender[i].RefID === vm.dataExp.FieldType) {
								vm.selectedTypeTender = vm.listTypeTender[i];
								loadBusinessField();
								break;
							}
						}
					}
					if (item.isAdd === false) {
						for (var i = 0; i < vm.listTypeTender.length; i++) {
							if (vm.listTypeTender[i].RefID === vm.dataExp.FieldType) {
								vm.selectedTypeTender = vm.listTypeTender[i];
								loadBusinessField();
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadCountries = loadCountries;
		function loadCountries(data) {
			UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
			//console.info("loadCountries");
			VendorExperienceService.getCountries(function (response) {
				vm.countryList = response.data;
				for (var i = 0; i < vm.countryList.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.countryList[i].CountryID) {
							vm.selectedCountry = vm.countryList[i];
							loadStates(data);
							break;
						}
					}
				}
				if (item.isAdd === false) {
					console.info("masukk");
					for (var i = 0; i < vm.countryList.length; i++) {
						console.info("countryedit" + JSON.stringify(vm.dataExp.StateLocationRef));
						if (vm.dataExp.StateLocationRef.CountryID === vm.countryList[i].CountryID) {
							vm.selectedCountry = vm.countryList[i];
							loadStates(data);
							break;
						}
					}
				}
				UIControlService.unloadLoading();
			});
		}

		vm.listTypeExp = [];
		vm.cekTypeExp = 3154;
		function loadTypeExp() {
			UIControlService.loadLoading("MESSAGE.LOAD_TYPE_EXP"); //Loading Tipe Pengalaman
			VendorExperienceService.typeExperience(function (reply) {
				UIControlService.unloadLoading();
				vm.listTypeExp = reply.data.List;

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API_TYPE_EXP");
				UIControlService.unloadLoading();
			});
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.VENDOR.EXPERIENCE", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}


		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function getNumbMonth(input) {
		    var numbMonth = 0;
		    if (input == "Jan") numbMonth = 1;
		    if (input == "Feb") numbMonth = 2;
		    if (input == "Mar") numbMonth = 3;
		    if (input == "Apr") numbMonth = 4;
		    if (input == "May") numbMonth = 5;
		    if (input == "Jun") numbMonth = 6;
		    if (input == "Jul") numbMonth = 7;
		    if (input == "Aug") numbMonth = 8;
		    if (input == "Sep") numbMonth = 9;
		    if (input == "Oct") numbMonth = 10;
		    if (input == "Nov") numbMonth = 11;
		    if (input == "Dec") numbMonth = 12;

		    return numbMonth;
		}

		vm.checkStartDate = checkStartDate;
		function checkStartDate(selectedStartDate) {
		    console.info(new Date(selectedStartDate));

            //Validasi Tanggal < 5 Tahun yg lalu
		    var inputDate = {};
		    var todays = {};
		    var today = new Date();
		    todays.yearToday = today.toString().substr(11, 4);
		    todays.stringMonthToday = today.toString().substr(4, 3);
		    todays.numbMonthToday = getNumbMonth(todays.stringMonthToday);
		    todays.dateToday = today.toString().substr(8, 2);

		    inputDate.year = selectedStartDate.toString().substr(11, 4);
		    inputDate.stringMonth = selectedStartDate.toString().substr(4, 3);
		    inputDate.numbMonth = getNumbMonth(inputDate.stringMonth);
		    inputDate.date = selectedStartDate.toString().substr(8, 2);

		    var validyear = Number(todays.yearToday) - 5;
		    console.info(Number(inputDate.year));
		    console.info(todays);
		    if (Number(inputDate.year) <= validyear && Number(inputDate.numbMonth) <= Number(todays.numbMonthToday) && Number(inputDate.date) < Number(todays.dateToday)) {
		        UIControlService.msg_growl("warning", "ERROR.START_DATE_1");
		        vm.dataExp.StartDate = "";
		        return;
		    }

			if (today < selectedStartDate) {
				UIControlService.msg_growl("warning", "ERROR.START_DATE");
				vm.dataExp.StartDate = " ";
			}
		}

		vm.checkEndDate = checkEndDate;
		function checkEndDate(selectedEndDate, selectedStartDate, selectedExpType) {
			console.info(selectedExpType);
			if (selectedEndDate < selectedStartDate) {
				UIControlService.msg_growl("warning", "ERROR.END_DATE");
				vm.dataExp.EndDate = " ";
			} else if (selectedExpType == 3153) {
				var convertedEndDate = moment(selectedEndDate).format('YYYY-MM-DD');

				console.info(convertedEndDate);
				var datenow = new Date();
				var convertedDateNow = moment(datenow).format('YYYY-MM-DD');
				console.info(convertedDateNow);
				if (datenow < selectedEndDate) {
					UIControlService.msg_growl("warning", "ERROR.END_DATE_FINISH_CONTRACT");
				} else {
					console.info("masuk");
				}
			}
		}

		vm.listConditional = [];
		vm.selectedConditional;
		function loadConditional(ID) {
			UIControlService.loadLoading("MESSAGE.LOAD_CONDITION"); //Loading Data Kondisi
			DataPerlengkapanService.getConditionEq(function (reply) {
				UIControlService.unloadLoading();
				vm.listConditional = reply.data.List;
				if (ID) {
					for (var i = 0; i < vm.listConditional.length; i++) {
						if (ID == vm.listConditional[i].RefID) {
							vm.selectedConditional = vm.listConditional[i];
							break;
						}
					}
				}

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.listCategory = [];
		vm.selectedCategory;
		function loadCategory() {
			UIControlService.loadLoading("MESSAGE.LOAD_CATEGORY"); //Loading Data Kategori
			DataPerlengkapanService.getCategoryBuilding(
			function (reply) {
				UIControlService.unloadLoading();
				vm.listCategory = reply.data.List;
				console.info(vm.tipeform);
				for (var i = 0; i < vm.listCategory.length; i++) {
					if (vm.tipeform == 19) {
						if (vm.listCategory[i].Name === "BUILDING_CATEGORY_WORKSHOP") {
							vm.selectedCategory = vm.listCategory[i];
							break;
						}
					} else if (vm.tipeform == 17) {
						if (vm.listCategory[i].Name === "BUILDING_CATEGORY_WAREHOUSE") {
							vm.selectedCategory = vm.listCategory[i];
							break;
						}
					}

				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.listKepemilikan = [];
		vm.selectedKepemilikan;
		function loadKepemilikan(data) {
			UIControlService.loadLoading("MESSAGE.LOAD_OWNERSHIP"); //Loading Data Kepemilikan
			DataPerlengkapanService.getOwnership(function (reply) {
				UIControlService.unloadLoading();
				vm.listKepemilikan = reply.data.List;
				if (data) {
					for (var i = 0; i < vm.listKepemilikan.length; i++) {
						if (data === vm.listKepemilikan[i].RefID) {
							vm.selectedKepemilikan = vm.listKepemilikan[i];
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		/* begin provinsi, kabupaten, kecamatan */
		vm.getProvinsi = getProvinsi;
		vm.selectedProvince;
		function getProvinsi() {
			ProvinsiService.getStates(360, function (response) {
				vm.listProvinsi = response.data;
				if (vm.item !== undefined) {
					for (var i = 0; i < vm.listProvinsi.length; i++) {
						if (vm.item.address.StateID === vm.listProvinsi[i].StateID) {
							vm.selectedProvince = vm.listProvinsi[i];
							vm.changeProvince();
							break;
						}
					}
				}


			}, function (response) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.API");;
			});
		}

		vm.changeProvince = changeProvince;
		vm.selectedKabupaten;
		function changeProvince() {
			ProvinsiService.getCities(vm.selectedProvince.StateID, function (response) {
				vm.listKabupaten = response.data;
				if (vm.item !== undefined) {
					for (var i = 0; i < vm.listKabupaten.length; i++) {
						if (vm.item.address.CityID === vm.listKabupaten[i].CityID) {
							vm.selectedKabupaten = vm.listKabupaten[i];
							vm.changeCities();
							break;
						}
					}
				}/*
               	if (item.isAdd === false) {
               	    console.info("masukk");
               	    for (var i = 0; i < vm.listProvinsi.length; i++) {
               	        console.info("countryedit" + JSON.stringify(vm.dataExp.StateLocationRef));
               	        if (vm.dataExp.StateLocation === vm.listProvinsi[i].StateID) {
               	            vm.selectedProvince = vm.listProvinsi[i];
               	            vm.changeProvince();
               	            break;
               	        }
               	    }
               	}*/

			}, function (response) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.API");
			});

		}

		vm.changeCities = changeCities;
		vm.selectedKecamatan;
		function changeCities() {
			ProvinsiService.getDistrict(vm.selectedKabupaten.CityID, function (response) {
				vm.listKecamatan = response.data;
				if (vm.item !== undefined) {
					for (var i = 0; i < vm.listKecamatan.length; i++) {
						if (vm.item.address.DistrictID === vm.listKecamatan[i].DistrictID) {
							vm.selectedKecamatan = vm.listKecamatan[i];
							break;
						}
					}
				}
			}, function (response) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.API");
			});

		}
		/* end provinsi, kabupaten, kecamatan */

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}

		vm.saveData = saveData;
		function saveData() {
			if (vm.tipeform == 17 || vm.tipeform == 19) {
				if (vm.selectedCategory === undefined) {
					UIControlService.msg_growl("warning", "MESSAGE.NO_CATEGORY");
					return;
				}
				if (vm.selectedKepemilikan === undefined) {
					UIControlService.msg_growl("error", "MESSAGE.NO_OWNERSHIP");
					return;
				}
				if (vm.selectedKabupaten === undefined) {
					UIControlService.msg_growl("error", "MESSAGE.NO_CITY");
					return;
				}
				if (vm.selectedProvince === undefined) {
					UIControlService.msg_growl("error", "MESSAGE.NO_PROVINCE");
					return;
				}
				if (vm.selectedKecamatan === undefined) {
					UIControlService.msg_growl("error", "MESSAGE.NO_DISTRICT");
					return;
				}

				var addr_arr = [];
				var addr = {
					AddressDetail: vm.selectedKecamatan.Name + " , " + vm.selectedKabupaten.Name + " , " + vm.selectedProvince.Name,
					AddressInfo: vm.dataBuilding.Address,
					StateID: vm.selectedProvince.StateID,
					CityID: vm.selectedKabupaten.CityID,
					DistrictID: vm.selectedKecamatan.DistrictID
				}
				addr_arr.push(addr);
				console.info(vm.item);
				if (vm.item === undefined) {
					PPVHSService.InsertBuildingTender({
						addresses: addr_arr,
						BuildingArea: vm.dataBuilding.BuildingArea,
						Category: vm.selectedCategory.RefID,
						TenderStepId: vm.TenderStepID,
						OwnershipStatus: vm.selectedKepemilikan.RefID
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_BUILDING");
							$uibModalInstance.close();
							//}
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.API");
					});
				} else {
					PPVHSService.UpdateBuildingTender({
						ID: vm.ID,
						addresses: addr_arr,
						BuildingArea: vm.dataBuilding.BuildingArea,
						Category: vm.selectedCategory.RefID,
						TenderStepId: vm.TenderStepID,
						OwnershipStatus: vm.selectedKepemilikan.RefID
					}, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_BUILDING");
							$uibModalInstance.close();
							//}
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.API");
					});
				}
			} else if (vm.tipeform === 20) {
				if (vm.selectedConditional === undefined) {
					UIControlService.msg_growl("warning", "MESSAGE.NO_CONDITIONAL");
					return;
				}
				if (vm.selectedKepemilikan === undefined) {
					UIControlService.msg_growl("error", "MESSAGE.NO_OWNERSHIP");
					return;
				}
				var senddata = {
					ID: vm.ID,
					Brand: vm.dataForm.Brand,
					Capacity: vm.dataForm.Capacity,
					Category: categoryID,
					Condition: vm.selectedConditional.RefID,
					MfgDate: vm.dataForm.MfgDate,
					Name: vm.dataForm.Name,
					Ownership: vm.selectedKepemilikan.RefID,
					SerialNo: vm.dataForm.SerialNo,
					Type: vm.dataForm.Brand,
					TenderStepId: vm.TenderStepID
				}
				var checkCapacity = angular.isString(senddata.Capacity);
				if (vm.item == undefined) {
					PPVHSService.InsertNonBuildingTender(senddata, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_NONBUILDING");
							$uibModalInstance.close();
							//}
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.API");
					});
				} else {
					PPVHSService.UpdateNonBuildingTender(senddata, function (reply) {
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_NONBUILDING");
							$uibModalInstance.close();
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.API");
					});
				}
			} else if (vm.tipeform == 18) {
				if (vm.fileUpload === undefined) {
					vm.dataExp.DocumentURL = '';
					if (item.isAdd === false) {
						updateExp();
					} else if (item.isAdd === undefined) {
						saveProcess();
					}
				} else if (!(vm.fileUpload === undefined)) {
					uploadFile();
				}
				vm.dataExp.ExperienceType = Number(vm.dataExp.ExperienceType);
			}
		}

		function uploadFile() {
			AuthService.getUserLogin(function (reply) {
				console.info(JSON.stringify(reply));
				vm.dataExpLogin = reply.data.CurrentUsername + "_expe";
				if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.dataExpLogin);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});

		}

		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, dates, function (response) {
				console.info()
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.dataExp.DocumentURL = url;
					vm.pathFile = vm.folderFile + url;
					UIControlService.msg_growl("success", "FORM.MSG_SUC_UPLOAD");
					saveProcess();

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});

		}

		function validateFileType(file, allowedFileTypes) {
			//console.info(JSON.stringify(allowedFileTypes));
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		function saveProcess() {
			vm.dataExp['ExpCurrID'] = vm.dataExp.ExpCurrID;
			vm.dataExp['StateLocation'] = vm.selectedState.StateID;
			vm.dataExp['TenderStepID'] = vm.TenderStepID;
			PPVHSService.InsertExperienceTender(vm.dataExp, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "FORM.MSG_SUC_SAVE");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		function updateExp() {
			vm.dataExp['ExpCurrID'] = vm.dataExp.ExpCurrID;
			vm.dataExp['StateLocation'] = vm.selectedState.StateID;
			vm.dataExp['TenderStepID'] = vm.TenderStepID;
			PPVHSService.updateExp(vm.dataExp, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "FORM.MSG_SUC_SAVE");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		function editActiveBuilding(id, active, action) {
			UIControlService.loadLoading("MESSAGE.LOADING"); //Silahkan Tunggu
			DataPerlengkapanService.editActiveBulding({
				ID: id,
				IsActive: active,
				//Action: action
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_BUILDING");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "ERR.NONACTIVE"); //Gagal menonaktifkan data 
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API"); //Gagal Akses API 
				UIControlService.unloadLoading();
			});
		}

		function dataField(ContractName, Location, Address, Agency, AgencyTelpNo, ContractNo, ContractValue, StartDate,
			EndDate, UploadDate, Field, FieldType, Remark, ExperienceType, DocumentURL, StateLocation, ExpCurrID) {
			var self = this;
			self.ContractName = ContractName;
			self.Location = Location;
			self.Address = Address;
			self.Agency = Agency;
			self.AgencyTelpNo = AgencyTelpNo;
			self.ContractNo = ContractNo;
			self.ContractValue = ContractValue;
			self.StartDate = StartDate;
			self.EndDate = EndDate;
			self.UploadDate = UploadDate;
			self.Field = Field;
			self.FieldType = FieldType;
			self.Remark = Remark;
			self.ExperienceType = ExperienceType;
			self.DocumentURL = DocumentURL;
			self.StateLocation = StateLocation;
			self.ExpCurrID = ExpCurrID;
			self.TenderStepId = vm.TenderStepID;
		}

		function BuildingField(VendorID, Category, BuildingArea, Address, OwnershipStatus) {
			var self = this;
			self.VendorID = VendorID;
			self.Category = Category;
			self.BuildingArea = BuildingArea;
			self.Address = Address;
			self.OwnershipStatus = OwnershipStatus;
			self.TenderStepId = vm.TenderStepID;
		}

		function Field(Brand, Capacity, Category, Condition, MfgDate, Name, Ownership, SerialNo, Type) {
			var self = this;
			self.Brand = Brand;
			self.Capacity = Capacity;
			self.Category = Category;
			self.Condition = Condition;
			self.MfgDate = MfgDate;
			self.Name = Name;
			self.Ownership = Ownership;
			self.SerialNo = SerialNo;
			self.Type = Type;
			self.TenderStepId = vm.TenderStepID;
		}
	};
})();