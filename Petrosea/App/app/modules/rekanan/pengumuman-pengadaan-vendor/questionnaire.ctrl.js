﻿(function () {
    'use strict';

    angular.module("app").controller("QuestionnaireTenderAnnouncementCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'OfferEntryService',
        '$state', 'UIControlService', 'UploaderService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, OEService, $state,
        UIControlService, UploaderService, $uibModal, GlobalConstantService, $stateParams) {
        var vm = this;
        vm.listQuest = [];
        vm.IDTender = Number($stateParams.TenderRefID);
        vm.IDStepTender = Number($stateParams.StepID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.IDDoc = Number($stateParams.DocID);

        vm.back = back;
        function back() {
            $state.transitionTo('kelengkapan-tender-jasa-vendor', {
                TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: vm.IDDoc
            });
        }

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("pengumuman-pengadaan-client");
            UIControlService.loadLoading("MESSAGE.LOADING");
            OEService.getQuestionaireByVendor({
                TenderStepID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listQuest = data.VendorQuestionaires;
                    console.info(JSON.stringify(vm.listQuest));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.saveQuest = saveQuest;
        function saveQuest() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            OEService.approveQuestionaire(vm.listQuest, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE_QUEST");
                    vm.init();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

    }
})();
