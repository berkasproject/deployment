﻿(function () {
	'use strict';

	angular.module("app").controller("VendorPrequalEntryCtrl", ctrl);

	ctrl.$inject = ['$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VendorPrequalEntryService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl( $timeout, $http, $translate, $translatePartialLoader, $location, SocketService, VendorPrequalEntryService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.StepID = Number($stateParams.SetupStepID);
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.NegoId = 0;
		vm.IsSubmit = null;
		vm.PrequalSetupID
		vm.TipeVendor = "";
		function init() {
		    vm.Submit = true;
			$translatePartialLoader.addPart("pemasukkan-prakualifikasi");
			$translatePartialLoader.addPart("permintaan-ubah-data");
			UIControlService.loadLoading("Silahkan Tunggu...");
			loadDataVendorPrequal();
			loadPrequalStep();

		}

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    VendorPrequalEntryService.loadPrequalStep({
		        Status: vm.StepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadDataVendorPrequal = loadDataVendorPrequal;
		function loadDataVendorPrequal() {
		    VendorPrequalEntryService.loadDataVendorPrequal({
		        Status: vm.StepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.TipeVendor = reply.data == "" ? "" : reply.data.SysReference.Name;
		            loadPrequalEntry();
		            UIControlService.unloadLoading();
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.loadPrequalEntry = loadPrequalEntry;
		function loadPrequalEntry() {
			VendorPrequalEntryService.select({
				Status: vm.StepID
			}, function (reply) {
			    if (reply.status == 200) {
			        vm.list = reply.data;
			        vm.dataAssessment = vm.list[0].PrequalAssessment;
			        vm.list.forEach(function (list) {
			            if (list.VendorPrequal !== null) {
			                list.VendorPrequal.SubmitDate = UIControlService.convertDateTime(list.VendorPrequal.SubmitDate);
			                list.PrequalSetupStep.StartDate = UIControlService.convertDateTime(list.PrequalSetupStep.StartDate);
			                list.PrequalSetupStep.EndDate = UIControlService.convertDateTime(list.PrequalSetupStep.EndDate);
			            }
			            vm.PrequalSetupID = list.PrequalSetupStep.PrequalSetupID;
			            if (vm.TipeVendor != "") {
			                if (list.IsSubmit !== true) {
			                    if (list.SysReference.Name == "OC_VENDOREXPERTS" && vm.TipeVendor !== "VENDOR_TYPE_GOODS") {
			                        vm.Submit = false;
			                    }
			                    else if (!(list.SysReference.Name === "OC_VENDOREXPERIENCE" ||list.SysReference.Name === "OC_VENDOREXPERTS" )) {
			                        vm.Submit = false;
			                    }
			                }
			            }
			            else {
			                vm.Submit = false;
                        }
			            
			            //if (list.IsSubmit !== true) vm.Submit = false;
			        });
			        UIControlService.unloadLoading();
			    }
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
			});
		};

		vm.DetailPrequal = DetailPrequal;
		function DetailPrequal(data, nameModule) {
		    if (nameModule == "OC_ADM_LEGAL") $state.go('data-administrasi-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDORLICENSI") $state.go('izin-usaha-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDORSTOCK") $state.go('akta-pendirian-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_COMPANYPERSON") $state.go('pengurus-perusahaan-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDORBALANCE") $state.go('neraca-perusahaan-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDOREXPERTS") $state.go('tenaga-ahli-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDOREQUIPMENT") $state.go('data-perlengkapan-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDOREXPERIENCE") $state.go('data-pengalaman-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDORBANKDETAIL") $state.go('bank-detail-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_VENDORBUSINESSFIELD") $state.go('bidang-usaha-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "K3L_POINT") { gotoUploadCSMS(data); }
		    else if (nameModule == "VENDOR_OTHERDOCUMENT") $state.go('dokumen-lainlain-prakualifikasi', { PrequalStepID: vm.StepID });
		    else if (nameModule == "OC_STATEMENTLETTER") $state.go('surat-pernyataan-prakualifikasi', { PrequalStepID: vm.StepID });
		}

		vm.gotoUploadCSMS = gotoUploadCSMS;
		function gotoUploadCSMS(dt) {
		    var data = {
		        PrequalStepID: vm.StepID,
                DocumentUrl: dt.DocumentUrl == null ? "": dt.DocumentUrl
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/K3L/k3l-point-upload.html',
		        controller: 'K3LPointUploadCtrl',
		        controllerAs: 'K3LPointUploadCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}

		vm.cekSubmit = cekSubmit;
		function cekSubmit() {
		    if (vm.Submit == false) {
		        UIControlService.msg_growl("error", "ALERTERRORSUBMIT");
		        return;
		    }
		    else {
		        if (vm.dataAssessment == null) {
		            VendorPrequalEntryService.UpdateSubmit({
		                Status: vm.StepID
		            }, function (reply) {
		                if (reply.status == 200) {
		                    UIControlService.msg_growl("success", "Success Submit !!");
		                    init();
		                }
		            }, function (error) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		            });
		        }
		        else {
		            VendorPrequalEntryService.updateAssessmentSubmitDate({
		                Status: vm.StepID
		            }, function (reply) {
		                if (reply.status == 200) {
		                    UIControlService.msg_growl("success", "Success Submit !!");
		                    init();
		                }
		            }, function (error) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		            });
		        }
		    }
		}


	}
})();
//TODO


