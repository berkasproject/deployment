
(function () {
	'use strict';

	angular.module("app").controller("FormNonBuildingPrequalController", ctrl);

	ctrl.$inject = ['$scope', '$http', '$uibModalInstance', '$cookieStore', '$state', '$stateParams', '$rootScope',
        '$uibModal', '$translate', '$translatePartialLoader', '$location', 'DataPerlengkapanPrequalService', 'item',
        'ProvinsiService', 'UIControlService'];
	/* @ngInject */
	function ctrl($scope, $http, $uibModalInstance, $cookieStore, $state, $stateParams, $rootScope,
        $uibModal, $translate, $translatePartialLoader, $location, DataPerlengkapanPrequalService, item,
        ProvinsiService, UIControlService) {
		var vm = this;

		vm.dataForm = {};
		vm.isAdd = item.isForm;
		vm.isCategory = item.type;
		vm.title = "";
		vm.ID;
		vm.tipeform = item.type;
		var categoryID; //sesuai id ref
		vm.isCalendarOpened = [false, false, false, false];
		vm.tipeform = item.type;
		vm.dateconvert;

		vm.init = init;
		function init() {
			//cek vehicle atau equipment
			vm.datenow = new Date();
			vm.datepickeroptions = {
				minMode: 'year',
				maxDate: vm.datenow
			}
			if (vm.isCategory === "vehicle") {
				vm.title = "Data Kendaraan";
				categoryID = 3136;
			}
			if (vm.isCategory === "equipment") {
				vm.title = "Data Peralatan";
				categoryID = 3137;
			}

			if (vm.isAdd === true) {
				vm.dataForm = new Field("", "", categoryID, 0, null, "", 0, "", "");
				vm.ID = 0;
			}
			else {
				vm.dataForm = new Field(item.data.Brand, item.data.Capacity, categoryID, item.data.Condition,
                    new Date(Date.parse(item.data.MfgDate)), item.data.Name, item.data.Ownership, item.data.SerialNo, item.data.Type);
				vm.ID = item.data.ID;
				vm.dateconvert = UIControlService.getStrDate(vm.dataForm.MfgDate);
			}
			loadConditional();
			//loadKepemilikan();
		}

		vm.listConditional = [];
		vm.selectedConditional;
		function loadConditional() {
			UIControlService.loadLoadingModal("");
			DataPerlengkapanPrequalService.getConditionEq(
            function (reply) {
            	UIControlService.unloadLoadingModal();
            	vm.listConditional = reply.data.List;
            	if (vm.isAdd === false) {
            		for (var i = 0; i < vm.listConditional.length; i++) {
            			if (item.data.Condition === vm.listConditional[i].RefID) {
            				vm.selectedConditional = vm.listConditional[i];
            				break;
            			}
            		}
            	}
            	loadKepemilikan();
            }, function (err) {
            	UIControlService.msg_growl("error", "Gagal mendapat daftar opsi kondisi");
            	UIControlService.unloadLoadingModal();
            });
		}

		vm.listKepemilikan = [];
		vm.selectedKepemilikan;
		function loadKepemilikan() {
			UIControlService.loadLoadingModal("");
			DataPerlengkapanPrequalService.getOwnership(
            function (reply) {
            	UIControlService.unloadLoadingModal();
            	vm.listKepemilikan = reply.data.List;
            	if (vm.isAdd === false) {
            		for (var i = 0; i < vm.listKepemilikan.length; i++) {
            			if (item.data.Ownership === vm.listKepemilikan[i].RefID) {
            				vm.selectedKepemilikan = vm.listKepemilikan[i];
            				break;
            			}
            		}
            	}
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal mendapat daftar opsi kepemilikan");
            	UIControlService.unloadLoadingModal();
            });
		}

		/*open form date*/
		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		vm.saveData = saveData;
		function saveData() {
			if (vm.selectedConditional === undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.NO_CONDITIONAL");
				return;
			}
			if (vm.selectedKepemilikan === undefined) {
				UIControlService.msg_growl("error", "MESSAGE.NO_OWNERSHIP");
				return;
			}

			var senddata = {
				ID: vm.ID,
				Brand: vm.dataForm.Brand,
				Capacity: vm.dataForm.Capacity,
				Category: categoryID,
				Condition: vm.selectedConditional.RefID,
				MfgDate: vm.dataForm.MfgDate,
				Name: vm.dataForm.Name,
				Ownership: vm.selectedKepemilikan.RefID,
				SerialNo: vm.dataForm.SerialNo,
				Type: vm.dataForm.Brand,
			    PrequalStepID: item.PrequalStepID
			}
			DataPerlengkapanPrequalService.saveEquipment(senddata, function (reply) {
				UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_NONBUILDING");
				$uibModalInstance.close();
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
			});
		}
        
		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}

		function Field(Brand, Capacity, Category, Condition, MfgDate, Name, Ownership, SerialNo, Type) {
			var self = this;
			self.Brand = Brand;
			self.Capacity = Capacity;
			self.Category = Category;
			self.Condition = Condition;
			self.MfgDate = MfgDate;
			self.Name = Name;
			self.Ownership = Ownership;
			self.SerialNo = SerialNo;
			self.Type = Type;
		}
	}
})();