﻿(function () {
    'use strict';

    angular.module("app").controller("UploadDokModalPrequalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$stateParams', '$location', 'UploadDokumenLainlainPrequalService',
        '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService'];
    function ctrl($http, $translate, $translatePartialLoader, $stateParams, $location, UploadDokumenLainlainPrequalService,
        $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService) {
        var vm = this;
        //console.info("console modal upload");
        vm.detail = item.item;
        vm.VendorID;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.isAdd = item.act;
        vm.action = "";
        vm.pathFile;
        vm.Description;
        vm.fileUpload;
        vm.size;
        vm.name;
        vm.type;
        vm.flag;
        vm.selectedForm;
        vm.isCalendarOpened = [false, false, false];
        vm.Nama;
        vm.No;
        vm.ID;
        vm.idFileTypes;
        vm.idFileSize;
        vm.idUploadConfigs;
        vm.tgl = {};
        vm.DocUrl;
        vm.tglSekarang = UIControlService.getDateNow("");

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('other-docs');
            UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.VENDOR.UPLOADDL", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", ".NOTIF.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "NOTIF.API");
                UIControlService.unloadLoading();
                return;
            });
            if (vm.isAdd == 1) {
                vm.action = "Tambah";
            } else {
                vm.action = "Ubah";
                vm.DocUrl = item.item.DocumentUrl;
                vm.ID = item.item.ID;
                vm.Nama = item.item.DocumentName;
                vm.No = item.item.DocumentNo;
                vm.tgl.StartDate = item.item.ValidDate;
                vm.VendorEntryPrequalID = item.item.VendorEntryPrequalID;
                console.info("item" + JSON.stringify(item));
                //console.info(vm.tgl.StartDate);
            }


        }

        vm.simpan = simpan;
        function simpan() {
            console.info(vm.Nama);
            console.info(vm.No);
            if (vm.Nama === undefined || vm.Nama === "" || vm.Nama == null) {
                UIControlService.msg_growl("error", "Document Name is still empty");
                return;
            } else if (vm.No === undefined || vm.No === "" || vm.No == null) {
                UIControlService.msg_growl("error", "Document Number is still empty");
                return;
            }
            else if (vm.fileUpload == null && vm.DocUrl != null) {
                saveData();
            }
            else if (vm.fileUpload == null && vm.DocUrl == null) {
                UIControlService.msg_growl("error", "ERROR.NO_FILE");
                return;
            } else {
                uploadFile();
            }
        }

        //get tipe dan max.size file - 2
        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }
        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }

        vm.selectUpload = selectUpload;
        //vm.fileUpload;
        function selectUpload() {
            //console.info(vm.fileUpload);
        }
        /*start upload */
        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                if (vm.isAdd == 1) {
                    var id = item.PrequalStepID;
                }
                else {
                    var id = vm.VendorEntryPrequalID;
                }
                upload(id, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
            }
        }

        function validateFileType(file, allowedFileTypes) {

            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "ERROR.NO_FILE");
                return false;
            }
            return true;
        }

        vm.upload = upload;
        function upload(id, file, config, filters, callback) {
            convertToDate();

            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoading("NOTIF.LOADING_UPLOAD");
            
            UploaderService.uploadSingleFileUploadDocument(id, file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    //console.info("response:" + JSON.stringify(response));
                    if (response.status == 200) {
                        //console.info(response);
                        var url = response.data.Url;
                        vm.pathFile = url;
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        vm.DocUrl = vm.pathFile;
                        if (vm.flag == 0) {
                            vm.size = Math.floor(s);

                        }
                        if (vm.flag == 1) {
                            vm.size = Math.floor(s / (1024));
                        }
                        saveData();

                    } else {
                        UIControlService.msg_growl("error", "ERROR.FAIL_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    //console.info(response);
                    UIControlService.msg_growl("error", "NOTIF.NOT_COMPLETE")
                    UIControlService.unloadLoading();
                });

                

        }

        vm.saveData = saveData;
        function saveData() {
            if (vm.isAdd == 1) {
                UploadDokumenLainlainPrequalService.insert({
                    DocumentName: vm.Nama,
                    DocumentNo: vm.No,
                    DocumentUrl: vm.DocUrl,
                    ValidDate: vm.tgl.StartDate,
                    PrequalStepID: item.PrequalStepID
                }, function (reply) {
                    //console.info("reply" + JSON.stringify(reply))
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        //console.info(vm.tgl.StartDate);
                        UIControlService.msg_growl("success", "NOTIF.SUCCESS_UPLOAD");
                        $uibModalInstance.close();

                    } else {
                        UIControlService.msg_growl("error", "ERROR.FAIL_ADD_DOC");
                        return;
                    }
                }, function (err) {
                    console.info(err);
                    UIControlService.msg_growl("error", "NOTIF.NOT_COMPLETE");
                    UIControlService.unloadLoadingModal();
                }
                    );
            }
            else {
                UploadDokumenLainlainPrequalService.Update({
                    DocumentName: vm.Nama, DocumentUrl: vm.DocUrl, DocumentNo: vm.No, VendorEntryPrequalID: vm.VendorEntryPrequalID, ValidDate: vm.tgl.StartDate, ID: vm.ID
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "NOTIF.SUCCESS_UPDATE");
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", "ERROR.FAIL_UPDATE");
                        return;
                    }
                }, function (err) {
                    //console.info(vm.tgl.StartDate);
                    UIControlService.msg_growl("error", "NOTIF.NOT_COMPLETE");
                    UIControlService.unloadLoadingModal();
                });
            }
        }

        function convertAllDateToString() { // TIMEZONE (-)
            vm.tgl = UIControlService.getStrDate(vm.tgl);
        };

        function convertToDate() {
            vm.tgl.StartDate = UIControlService.getStrDate(vm.tgl.StartDate);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
