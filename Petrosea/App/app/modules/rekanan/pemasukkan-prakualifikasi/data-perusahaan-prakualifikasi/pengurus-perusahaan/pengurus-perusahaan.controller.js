(function () {
	'use strict';

	angular.module("app").controller("PengurusPerusahaanPrequalController", ctrl);

	ctrl.$inject = ['$state', '$stateParams','$uibModal', '$filter', '$translatePartialLoader', 'PengurusPerusahaanPrequalService', 'CommonEngineService', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, $uibModal, $filter, $translatePartialLoader, PengurusPerusahaanPrequalService, CommonEngineService, UIControlService) {
		var vm = this;
		var loadmsg = 'MESSAGE.LOADING';
		vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.compPersons = [];
		vm.vendorName;
		vm.vendorID;
		vm.bisaMengubahData;
		vm.isChangeData = false;
		vm.isApprovedCR = false;
		vm.finalApproveBy = null;
		vm.isEditedByVendor = false;
		vm.pageSize = 10;
		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
		    $translatePartialLoader.addPart('pengurus-perusahaan');
			UIControlService.loadLoading(loadmsg);
			loadPrequalStep();
			loadContactCompany();
			loadData(1);
		};

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    PengurusPerusahaanPrequalService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadContactCompany = loadContactCompany;
		function loadContactCompany() {
		    PengurusPerusahaanPrequalService.selectContact({ Status: vm.PrequalStepID }, function (reply) {
				if (reply.status == 200) {
					vm.contact = reply.data;
					vm.vendorLocation = [];
					for (var i = 0; i < vm.contact.length; i++) {
						if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
							vm.vendorLocation = vm.contact[i].Contact.Address.State.Country.Code;
							break;
						}
					}
				} else {
					return;
				}
			}, function (err) {
				UIControlService.unloadLoading();
				return;
			});
		}

		vm.loadData = loadData;
		function loadData(current) {
		    vm.currentPage = current;
		    var offset = (current * vm.pageSize) - vm.pageSize;
		    PengurusPerusahaanPrequalService.GetByVendor({
		        Status: vm.PrequalStepID,
		        Offset: offset,
                Limit: vm.pageSize
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.compPersons = reply.data.List;
		            vm.compPersons.forEach(function (cp) {
		                cp.DateOfBirthConverted = UIControlService.convertDate(cp.DateOfBirth);
		            });
		            checkMandatoryPositions();
		        } else {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD');
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD');
		        UIControlService.unloadLoading();
		    });
		}

		vm.submitCP = submitCP;
		function submitCP() {
			bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_SUBMIT') + '<h3>', function (reply) {
				if (reply) {
					UIControlService.loadLoading(loadmsg);
					PengurusPerusahaanPrequalService.Submit({ OpsiCode: 'OC_COMPANYPERSON' }, function (reply2) {
						UIControlService.unloadLoading();
						if (reply2.status === 200) {
							UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SUBMIT');
							loadData();
						} else
							UIControlService.msg_growl('error', 'MESSAGE.ERR_SUBMIT');
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl('error', 'MESSAGE.ERR_SUBMIT');
					});
				}
			});
		}

		function loadCheckCR() {
			UIControlService.loadLoading("Silahkan Tunggu");
			PengurusPerusahaanPrequalService.getCRbyVendor({ CRName: 'OC_COMPANYPERSON' }, function (reply) {
				//PermintaanUbahDataService.getCRbyVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    console.info("CR:" + JSON.stringify(reply));
				    vm.CR = reply.data;

					if (reply.data === true) { //has data
						vm.isApprovedCR = true;
						//reply.data[0].ChangeRequestDataDetails[0].IsEditedByVendor == null ? vm.isEditedByVendor = false : vm.isEditedByVendor = reply.data[0].ChangeRequestDataDetails[0].IsEditedByVendor;
						//vm.finalApproveBy = reply.data[0].ChangeRequestDataDetails[0].FinalApproveBy;
					} else {
						vm.isApprovedCR = false;
					}

					//if (!(reply.data === null) && reply.data.ApproveBy === 1) {
					//	vm.isApprovedCR = true;
					//}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function chekcIsVerified() {
			PengurusPerusahaanPrequalService.isVerified(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					console.info(JSON.stringify(reply));
					var data = reply.data;
					vm.vendorID = data.VendorID;
					if (!(data.Isverified === null)) {
						vm.isChangeData = true;
					}
					loadContactCompany();
					loadData();
					loadCheckCR();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		

		function checkMandatoryPositions() {
			vm.noPresDir = true;
			vm.noFinDir = true;
			vm.noOperDir = true;

			vm.compPersons.forEach(function (cp) {
				if (cp.PositionRef === 'PRESIDENT_DIRECTOR') {
					vm.noPresDir = false;
				} else if (cp.PositionRef === 'DIRECTOR_OF_FINANCE') {
					vm.noFinDir = false;
				} else if (cp.PositionRef === 'DIRECTOR_OF_OPERATIONS') {
					vm.noOperDir = false;
				}
			});
		}

		vm.addCP = addCP;
		function addCP() {
			var lempar = {
				compPerson: {
					VendorID: vm.vendorID,
					PositionRef: 'OTHERS',
					Address: {},
					Location: vm.vendorLocation,
					PrequalStepID: vm.PrequalStepID
				},
                action: 'add'
			};
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/pengurus-perusahaan/pengurus-perusahaan.formModal.html',
				controller: 'formPengurusPerusahaanCtrl',
				controllerAs: 'formPPCtrl',
				resolve: {
					item: function () {
						return lempar;
					}
				}
			});
			modalInstance.result.then(function () {
			    window.location.reload();
			});
		};

		vm.editCP = editCP;
		function editCP(cp) {
			var lempar = {
				compPerson: {
					ID: cp.ID,
					VendorEntryPrequalID: cp.VendorEntryPrequalID,
					PersonName: cp.PersonName,
					DateOfBirth: new Date(Date.parse(cp.DateOfBirth)),
					NoID: cp.NoID,
					IDUrl: cp.IDUrl,
					PersonAddress: cp.PersonAddress,
					PositionRef: cp.PositionRef,
					ServiceStartDate: new Date(Date.parse(cp.ServiceStartDate)),
					ServiceEndDate: new Date(Date.parse(cp.ServiceEndDate)),
					Address: cp.Address,
                    CompanyPosition: cp.CompanyPosition
				},
                action:'edit'
			};
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/pengurus-perusahaan/pengurus-perusahaan.formModal.html',
				controller: 'formPengurusPerusahaanCtrl',
				controllerAs: 'formPPCtrl',
				resolve: {
					item: function () {
						return lempar;
					}
				}
			});
			modalInstance.result.then(function () {
				loadData();
			});
		};

		vm.viewCP = viewCP;
		function viewCP(cp) {
			var lempar = {
				compPerson: {
					ID: cp.ID,
					VendorID: vm.vendorID,
					PersonName: cp.PersonName,
					DateOfBirth: UIControlService.convertDate(cp.DateOfBirth),
					NoID: cp.NoID,
					IDUrl: cp.IDUrl,
					PersonAddress: cp.PersonAddress,
					PositionRef: cp.PositionRef,
					ServiceStartDate: UIControlService.convertDate(cp.ServiceStartDate),
					ServiceEndDate: UIControlService.convertDate(cp.ServiceEndDate),
					Address: cp.Address,
					Country: cp.Address.State.Country.Name,
                    CompanyPosition: cp.CompanyPosition
				}
			};
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/pengurus-perusahaan/pengurus-perusahaan.viewModal.html',
				controller: 'viewPengurusPerusahaanCtrl',
				controllerAs: 'viewPPCtrl',
				resolve: {
					item: function () {
						return lempar;
					}
				}
			});
		};

		vm.deleteCP = deleteCP;
		function deleteCP(cp) {
			bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
				if (reply) {
					UIControlService.loadLoading(loadmsg);
					PengurusPerusahaanPrequalService.Delete({
						ID: cp.ID
					}, function (reply2) {
						UIControlService.unloadLoading();
						if (reply2.status === 200) {
							UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
							window.location.reload();
						} else
							UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
					});
				}
			});
		};
        
		vm.Submit = Submit;
		function Submit() {
		    if (vm.noPresDir == false && vm.noFinDir == false && vm.noOperDir == false) {
		        PengurusPerusahaanPrequalService.Submit({
		            Status: vm.PrequalStepID
		        }, function (reply2) {
		            UIControlService.unloadLoading();
		            if (reply2.status === 200) {
		                UIControlService.msg_growl('notice', 'Success Submit');
		                $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		            }
		        }, function (error) {
		            UIControlService.unloadLoading();
		        });
		    }
		    else {
		        if (vm.noPresDir == true) {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_NO_PRESIDENT_DIRECTOR');
		            return;
		        }
		        else if (vm.noFinDir == true) {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_NO_DIRECTOR_OF_FINANCE');
		            return;
		        }
		        else if (vm.noOperDir == true) {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_NO_DIRECTOR_OF_OPERATIONS');
		            return;
		        }
		    }
		}
	}
})();