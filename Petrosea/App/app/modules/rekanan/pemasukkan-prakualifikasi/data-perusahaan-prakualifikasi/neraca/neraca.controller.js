(function () {
    'use strict';

    angular.module("app").controller("BalanceVendorPrequalCtrl", ctrl);

    ctrl.$inject = ['$stateParams', 'AuthService', '$scope', '$state', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BalanceVendorPrequalService', 'BalanceVendorService', 'RoleService', 'UIControlService', 'GlobalConstantService', '$uibModal'];
    function ctrl($stateParams, AuthService, $scope, $state, $http, $translate, $translatePartialLoader, $location, SocketService, BalanceVendorPrequalService, BalanceVendorService,
        RoleService, UIControlService, GlobalConstantService, $uibModal) {

        var vm = this;
	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
        var page_id = 141;
        vm.departemen = [];
        var asset = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        //vm.isApprovedCR;
        vm.balanceDocUrl = "";

        vm.IsApprovedCR = false;
        vm.initialize = initialize;
        vm.changeDataPermission = false;
        function initialize() {
            $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
            $translatePartialLoader.addPart('vendor-balance');
            jLoad(1);
            loadPrequalStep();
        }

        vm.loadPrequalStep = loadPrequalStep;
        function loadPrequalStep(){
            BalanceVendorPrequalService.loadPrequalStep({ Status: vm.PrequalStepID }, function (reply) {
                if (reply.status === 200) {
                    vm.isEntry = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }
        
        vm.loadBalanceDocUrl = loadBalanceDocUrl;
        function loadBalanceDocUrl() {
            BalanceVendorPrequalService.balanceDocUrl({ Status: vm.PrequalStepID }, function (reply) {
                if (reply.status === 200) {
                    vm.balanceDocUrl = reply.data.DocUrl;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.asset = 0;
            vm.hutang = 0;
            vm.modal = 0;
            vm.vendorbalance = [];
            vm.currentPage = current;
            BalanceVendorPrequalService.select({ Status: vm.PrequalStepID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorbalance = reply.data;
                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        if (vm.vendorbalance[i].WealthType.Name == "WEALTH_TYPE_ASSET") {
                            vm.listAsset = vm.vendorbalance[i];
                        }
                        if (vm.vendorbalance[i].WealthType.Name == "WEALTH_TYPE_DEBTH") {
                            vm.listDebth = vm.vendorbalance[i];
                        }
                    }
                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        for (var j = 0; j < vm.vendorbalance[i].subWealth.length; j++) {
                            if (vm.vendorbalance[i].subWealth[j].subCategory.length === 0) {
                                if (vm.vendorbalance[i].WealthType.RefID === 3097 && vm.vendorbalance[i].subWealth[j].IsActive === true) {
                                    if (vm.asset === 0) {
                                        vm.asset = vm.vendorbalance[i].subWealth[j].nominal;
                                    }
                                    else
                                        vm.asset = +vm.asset + +vm.vendorbalance[i].subWealth[j].nominal;

                                }
                                else if (vm.vendorbalance[i].WealthType.RefID === 3099 && vm.vendorbalance[i].subWealth[j].IsActive === true) {
                                    if (vm.hutang === 0) {
                                        vm.hutang = vm.vendorbalance[i].subWealth[j].nominal;
                                    }
                                    else {
                                        vm.hutang = +vm.hutang + +vm.vendorbalance[i].subWealth[j].nominal;
                                    }


                                }
                            }
                            else {
                                for (var k = 0; k < vm.vendorbalance[i].subWealth[j].subCategory.length; k++) {
                                    if (vm.vendorbalance[i].subWealth[j].subCategory[k].Wealth.RefID === 3097 && vm.vendorbalance[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.asset === 0) {
                                            vm.asset = vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else
                                            vm.asset = +vm.asset + +vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;

                                    }
                                    else if (vm.vendorbalance[i].subWealth[j].subCategory[k].WealthType === 3099 && vm.vendorbalance[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.hutang === 0) {
                                            vm.hutang = vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else {
                                            vm.hutang = +vm.hutang + +vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                        }


                                    }
                                }
                            }
                            
                        }
                    }
                    vm.modal = +vm.asset - +vm.hutang;
                    loadBalanceDocUrl();
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            UIControlService.loadLoading("Silahkan Tunggu");
            BalanceVendorPrequalService.dlete({
                ID: data.ID,
                IsActive: active
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var msg = "";
                    if (active === false) msg = "Non Aktifkan ";
                    if (active === true) msg = "Aktifkan ";
                    UIControlService.msg_growl("success", "SUC_DELETE");
                    initialize();
                }
                else {
                    UIControlService.msg_growl("error", "Gagal menonaktifkan data ");
                    return;
                }
            }, function (err) {

                UIControlService.msg_growl("error", "Gagal Akses API ");
                UIControlService.unloadLoading();
            });

        }

        vm.tambah = tambah;
        function tambah() {
            var data = {
                act: true,
                PrequalStepID: vm.PrequalStepID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/neraca/formNeraca.html',
                controller: 'formNeracaCtrl',
                controllerAs: 'formNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        }

        vm.edit = edit;
        function edit(data, flag) {
            console.info("masuk form add/edit");
            if (flag == 1) {
                var data = {
                    act: false,
                    item: data,
                    PrequalStepID: vm.PrequalStepID
                }
            }
            if (flag != 1) {

                var data = {
                    act: false,
                    item:
                    {
                        ID: flag.ID,
                        Wealth: data,
                        COA: flag.COAType,
                        Unit: flag.Unit,
                        Amount: flag.Amount,
                        DocUrl: flag.DocUrl,
                        Nominal: flag.nominal
                    },
                    PrequalStepID: vm.PrequalStepID
                }
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/neraca/formNeraca.html',
                controller: 'formNeracaCtrl',
                controllerAs: 'formNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.upload = upload;
        function upload() {
            console.info("masuk form add/edit");
            var data = {
                act: true,
                PrequalStepID: vm.PrequalStepID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/neraca/formUploadNeraca.html',
                controller: 'frmUploadNeracaCtrl',
                controllerAs: 'frmUploadNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
                //$scope.main.loadMenus();
                //AuthService.getUserLogin(function (reply) {
                //    if (reply.status === 200) {
                //        $scope.main.getCurrUser();
                //        //console.info(JSON.stringify(reply));
                //        AuthService.getRoleUserLogin({ Keyword: reply.data.CurrentUsername }, function (reply1) {
                //            if (reply1.status === 200 && reply1.data.List.length > 0) {
                //                var role = reply1.data.List[0].RoleName;
                //                UIControlService.msg_growl("notice", 'NOTIFICATION.LOGIN.SUCCESS.MESSAGE', "NOTIFICATION.LOGIN.SUCCESS.TITLE");
                //                if (role === 'APPLICATION.ROLE_VENDOR') {
                //                    $state.go('vendor-balance');
                //                } else if (role === 'APPLICATION.ROLE_VENDOR_INTERNATIONAL') {
                //                    $state.go('vendor-balance');
                //                } else {
                //                    $state.go('homeadmin');
                //                }
                //            } else {
                //                UIControlService.msg_growl("error", "User Tidak Berhak Login");
                //                $state.go('home');
                //            }
                //        }, function (err1) {
                //            UIControlService.msg_growl("error", "MESSAGE.API");
                //        });
                //    }
                //}, function (err) {
                //    UIControlService.msg_growl("error", "MESSAGE.API");
                //});
            });
        }

        vm.Submit = Submit;
        function Submit() {
            if (vm.vendorbalance.length === 0) {
                UIControlService.msg_growl("error", "Data Belum lengkap");
                return;
            }
            else if (vm.hutang == 0) {
                UIControlService.msg_growl("error", "Data hutang perusahaan belum diisi");
                return;
            }
            else {
                BalanceVendorPrequalService.Submit({ Status: vm.PrequalStepID }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                });

            }
        }
    }
})();
