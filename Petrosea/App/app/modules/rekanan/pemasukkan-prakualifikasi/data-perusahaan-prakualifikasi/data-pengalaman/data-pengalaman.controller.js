(function () {
	'use strict';

	angular.module("app").controller("DataPengalamanController", ctrl);

	ctrl.$inject = ['$filter', '$state', '$stateParams', '$timeout','$uibModal', '$translatePartialLoader', 'UIControlService', 'VendorExperienceService'];
	/* @ngInject */
	function ctrl($filter, $state, $stateParams, $timeout, $uibModal, $translatePartialLoader, UIControlService, VendorExperienceService) {
		var vm = this;
		vm.totalItems = 0;
		vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.Offset = 0;
		vm.keyword = '';
		vm.vendorID = [];
		vm.comodity = [];
		vm.isApprovedCR = false;

		vm.initialize = initialize;
		function initialize() {
		    vm.listCurrentExp = [];
		    vm.listFinishExp = [];
		    $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
			$translatePartialLoader.addPart('data-pengalaman');
			loadCurrentExperience(1, 0);
			loadPrequalStep();

		};

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    VendorExperienceService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadCurrentExperience = loadCurrentExperience;
		function loadCurrentExperience(current, flag) {
		    vm.flag = flag;
		    vm.currentPageCurrent = current;
		    var offset = vm.maxSize * current - vm.maxSize;
		    VendorExperienceService.loadAllExperience({
		        Status: vm.PrequalStepID,
		        FilterType: 0,
		        Offset: offset,
                Limit: vm.maxSize
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.listCurrentExp = reply.data.List;
		            for (var i = 0; i < vm.listCurrentExp.length; i++) {
		                vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
		                if (vm.listCurrentExp[i].CityLocation != null) vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].CityLocation.Name + ", " + vm.listCurrentExp[i].CityLocation.State.Country.Name;
		                else vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].StateLocationRef.Name + ", " + vm.listCurrentExp[i].StateLocationRef.Country.Name;
		            }

		            vm.totalItemsCurrent = Number(reply.data.Count);
                    if(vm.flag == 0) loadFinishExperience(1);
                    
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadFinishExperience = loadFinishExperience;
		function loadFinishExperience(current) {
		    vm.currentPageFinish = current;
		    var offset = vm.maxSize * current - vm.maxSize;
		    VendorExperienceService.loadAllExperience({
		        Status: vm.PrequalStepID,
		        FilterType: 1,
		        Offset: offset,
		        Limit: vm.maxSize
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.listFinishExp = reply.data.List;
		            for (var i = 0; i < vm.listFinishExp.length; i++) {
		                vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);
		                if (vm.listFinishExp[i].CityLocation != null) vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address + ", " + vm.listFinishExp[i].CityLocation.Name + ", " + vm.listFinishExp[i].CityLocation.State.Country.Name;
		                else vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address + ", " + vm.listFinishExp[i].StateLocationRef.Name + ", " + vm.listFinishExp[i].StateLocationRef.Country.Name;
		            }
		            vm.totalItemsFinish = Number(reply.data.Count);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.addExp = addExp;
		function addExp(data, isAdd) {
			var sendData = {
			    item: data,
			    isAdd: isAdd,
                PrequalStepID: vm.PrequalStepID
			}
			//console.info(JSON.stringify(sendData));
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-pengalaman/form-pengalaman.html',
				controller: 'formExpCtrl',
				controllerAs: 'formExpCtrl',
				resolve: {
					item: function () {
						return sendData;
					}
				}
			});

			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.detailExp = detailExp;
		function detailExp(data) {
			var sendData = {
				item: data,
				isAdd: false
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-pengalaman/detail-pengalaman.html',
				controller: 'formExpCtrl',
				controllerAs: 'formExpCtrl',
				resolve: {
					item: function () {
						return sendData;
					}
				}
			});

			modalInstance.result.then(function () {
				loadawal();
			});
		}

		vm.loadAwal = loadawal;
		function loadawal(data) {
            //limit sementara dibuat 100
			UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
			VendorExperienceService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: 100,
				Keyword: vm.keyword,
				FilterType: data,
				column: 1
			}, function (reply) {
				console.info("lrjlbij" + vm.comodity.BusinessFieldID);
				if (reply.status === 200) {
					vm.listFinishExp = reply.data.List;
					console.info("listFinishExp:" + JSON.stringify(vm.listFinishExp));
					for (var i = 0; i < vm.listFinishExp.length; i++) {
						vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);
						vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address + ", " + vm.listFinishExp[i].CityLocation.Name + ", " + vm.listFinishExp[i].CityLocation.State.Country.Name;
					}
					vm.totalItems = reply.data.Count;
					console.info("finish:" + JSON.stringify(vm.listFinishExp));
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
			});

			UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
			VendorExperienceService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: 100,
				Keyword: vm.keyword,
				column: 2
			}, function (reply) {
				//console.info("current?:"+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listCurrentExp = reply.data.List;
					for (var i = 0; i < vm.listCurrentExp.length; i++) {
						vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
						vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].CityLocation.Name + ", " + vm.listCurrentExp[i].CityLocation.State.Country.Name;
					}
					console.info("current exp:" + JSON.stringify(vm.listCurrentExp));
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
			});

		}

		vm.editActive = editActive;
		function editActive(data, active) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			VendorExperienceService.Delete({ ID: data.ID, IsActive: active }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", $filter('translate')('SUCCESS_DELETE'));
					initialize();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.submit = submit;
		function submit() {
		    VendorExperienceService.Submit({ Status: vm.PrequalStepID }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status == 200) {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("success", "Success Submit");
		            $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		        } else {
		            UIControlService.unloadLoading();
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "Gagal Akses API ");
		        UIControlService.unloadLoading();
		    });
		}
	}


})();// baru controller pertama
