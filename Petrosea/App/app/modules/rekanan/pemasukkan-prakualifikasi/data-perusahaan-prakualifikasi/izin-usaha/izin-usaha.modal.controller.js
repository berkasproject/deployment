﻿(function () {
    'use strict';

    angular.module("app").controller("FormIzinPrequalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'item', '$uibModal', 'IzinUsahaPrequalService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService', 'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService'];

    function ctrl($http, $translate, $translatePartialLoader, $location, UIControlService, item, $uibModal, IzinUsahaPrequalService, $uibModalInstance, UploadFileConfigService, UploaderService, AuthService, $filter, ProvinsiService, GlobalConstantService) {
        var vm = this;
        vm.dataLicensi = item.item;
        vm.dataLicensi['VendorEntryPrequal'] = item.VendorEntryPrequal;
        vm.licensiname = vm.dataLicensi.LicenseName;
        vm.IsBPJS = false;
        if (vm.licensiname.match(/BPJS.*/)) {
            console.info("bpjs");
            vm.IsBPJS = true;
        }
        vm.isCalendarOpened = [false, false, false, false];
        vm.pathFile;
        vm.getIDstate;
        vm.VendorLogin;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.mindate = new Date();
        vm.cityID = item.cityID;
        vm.isNominal = vm.dataLicensi.IsNominal;
        vm.tglSekarang = new Date();

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("data-izinusaha");
            vm.LicenseNo = vm.dataLicensi.LicenseNo;
            vm.licensiname = vm.dataLicensi.LicenseName;
            vm.IssuedDate = new Date(Date.parse(vm.dataLicensi.IssuedDate));
            vm.ExpiredDate = new Date(Date.parse(vm.dataLicensi.ExpiredDate));
            vm.IssuedBy = vm.dataLicensi.IssuedBy;
            vm.CapitalAmount = vm.dataLicensi.CapitalAmount;
            vm.Remark = vm.dataLicensi.Remark;
            vm.DocumentURL = vm.dataLicensi.DocumentURL;

            loadKlasifikasi();
            getTypeSizeFile();
            if (vm.dataLicensi.IssuedLocation !== null) {
                getCityByID(vm.dataLicensi.IssuedLocation);
            } else {
                changeCountry(vm.cityID);
            }
        }

        /*get username*/
        function getUsLogin() {
            AuthService.getUserLogin(function (reply) {
                vm.VendorLogin = reply.data.CurrentUsername;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        vm.cekDate = cekDate;
        function cekDate(StartDate, EndDate, flag) {
            vm.flag = flag;
            vm.IssuedDate = StartDate;
            vm.ExpiredDate = EndDate;
            IzinUsahaPrequalService.cekDate(
                {
                    Status: flag,
                    Date1: UIControlService.getStrDate(StartDate),
                    Date2 : UIControlService.getStrDate(EndDate)
                },
               function (response) {
                   if (flag == 1 && response.data == 0) {
                       UIControlService.msg_growl("error", 'MESSAGE.STARTDATE_VALID');
                       vm.IssuedDate = undefined;
                       return;
                   }
                   else if (flag == 2 && response.data == 0) {
                       UIControlService.msg_growl("error", 'MESSAGE.DATE_VALID');
                       vm.ExpiredDate = undefined;
                       return;
                   }
                   else if (flag == 1 && response.data == 1) {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_ISSUEDDATE');
                       vm.IssuedDate = undefined;
                       return;
                   }
                   else if (flag == 2 && response.data == 1) {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_DATE');
                       vm.ExpiredDate = undefined;
                       return;
                   }
               },
           function (response) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
        }

        /*get city by id*/
        function getCityByID(id) {
            ProvinsiService.getCityByID({ column: id }, function (reply) {
                UIControlService.unloadLoading();
                var data = reply.data.List[0];
                console.info(data);
                vm.selectedCities = data;
                vm.selectedState = data.State;
                changeState(data.State.StateID);
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        /*open form date*/
        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        /*get combo klasifikasi*/
        vm.listClasification = [];
        vm.selectedClasification;
        function loadKlasifikasi() {
            //alert("load");
            IzinUsahaPrequalService.getClasification(function (reply) {
                UIControlService.unloadLoading();
                vm.dataClass = reply.data.List;
                for (var i = 0; i < vm.dataClass.length; i++) {
                    if (vm.dataClass[i].Name != "ALL_COMPANY") {
                        vm.listClasification.push(vm.dataClass[i]);

                        if (!(vm.dataLicensi.LicenseNo === null)) {
                            if (vm.dataClass[i].RefID === vm.dataLicensi.CompanyScale) {
                                vm.selectedClasification = vm.dataClass[i];
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        /* combo country, state, city */
        vm.changeCountry = changeCountry;
        vm.listState = [];
        vm.selectedState;
        function changeCountry(idstate) {
            ProvinsiService.getStates(idstate,
               function (response) {
                   vm.listState = response.data;
                   if (vm.dataLicensi.IssuedLocation !== null) {
                       for (var i = 0; i < vm.listState.length; i++) {
                           if (vm.selectedState.StateID === vm.listState[i].StateID) {
                               vm.selectedState = vm.listState[i];
                               break;
                           }
                       }
                   }
               },
           function (response) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
        }

        vm.changeState = changeState;
        vm.listCities = [];
        vm.selectedCities;
        function changeState() {
            ProvinsiService.getCities(vm.selectedState.StateID, function (response) {
                vm.listCities = response.data;
                if (!(vm.dataLicensi.LicenseNo === null)) {
                    for (var i = 0; i < vm.listCities.length; i++) {
                        if (vm.dataLicensi.IssuedLocation === vm.listCities[i].CityID) {
                            vm.selectedCities = vm.listCities[i];
                            changeCountry(vm.selectedState.CountryID);
                            break;
                        }
                    }
                }
            }, function (response) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }


        vm.changeCities = changeCities;
        function changeCities() {
            vm.IssuedLocation = vm.selectedCities.CityID;
        }
        /* end combo country, state, city*/

        /*get type n size file upload*/
        vm.selectUpload = selectUpload;
        vm.fileUpload;
        function selectUpload() {
            console.info(">" + vm.fileUpload);
            //vm.fileUpload = vm.fileUpload;
        }

        function getTypeSizeFile() {
            UploadFileConfigService.getByPageName("PAGE.VENDOR.LICENSI", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                    //console.info("file:" + JSON.stringify(response));

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }

        //simpan
        vm.savedata = savedata;
        function savedata() {
            if (vm.licensiname === 'SIUP') {
                if (vm.selectedClasification === undefined && vm.dataLicensi.CapitalAmount === " ") {
                    UIControlService.msg_growl("warning", "MESSAGE.ERR_NOMINAL");
                    return;
                }
            }

            if (vm.IssuedDate === null || vm.IssuedDate === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.ISSUED_DATE");
                return;
            } else if (vm.IsBPJS === false && (vm.ExpiredDate === null || vm.ExpiredDate === undefined )) {
                UIControlService.msg_growl("warning", "MESSAGE.EXPIRED_DATE");
                return;
            }
            if (vm.LicenseNo == null || vm.IssuedBy == null || vm.selectedState == null ||
                vm.selectedCities == null || (vm.fileUpload == null && vm.dataLicensi.DocumentURL === null)) {
                UIControlService.msg_growl("warning", "MESSAGE.NOT_COMPLETE");
                return;
            } else if (!(vm.LicenseNo == null || vm.IssuedBy == null || vm.selectedState == null ||
                vm.selectedCities == null || (vm.fileUpload == null && vm.DocumentURL === null))) {
                if (vm.selectedClasification === undefined) {
                    vm.cscale = null;
                } else {
                    vm.cscale = vm.selectedClasification.RefID;
                }
                if (!(vm.fileUpload === undefined)) {
                    uploadFile();
                } else {
                    saveprocess();
                }
            }



        }

        /*proses upload file*/
        function uploadFile() {
            AuthService.getUserLogin(function (reply) {
                console.info(JSON.stringify(reply));
                vm.VendorLogin = reply.data.CurrentUsername;
                if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.VendorLogin);
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });

        }

        function upload(file, config, filters, dates, callback) {
            console.info(dates);
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, dates, function (response) {
                console.info()
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.DocumentURL = response.data.Url;
                    UIControlService.msg_growl("success", "FORM.MSG_SUC_UPLOAD");
                    saveprocess();

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (response) {
                UIControlService.msg_growl("error", "MESSAGE.API")
                UIControlService.unloadLoading();
            });

        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        /* end proses upload*/

        function saveprocess() {
            IzinUsahaPrequalService.updateLicensi({
                LicenseID: vm.dataLicensi.LicenseID,
                VendorEntryPrequal: vm.dataLicensi.VendorEntryPrequal,
                LicenseNo: vm.LicenseNo,
                CapitalAmount: vm.CapitalAmount,
                DocumentURL: vm.DocumentURL,
                ExpiredDate: vm.IsBPJS === true ? null : UIControlService.getStrDate(vm.ExpiredDate),
                IssuedBy: vm.IssuedBy,
                IssuedDate: UIControlService.getStrDate(vm.IssuedDate),
                IssuedLocation: vm.selectedCities.CityID,
                Remark: vm.Remark,
                CompanyScale: vm.cscale
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "FORM.MSG_SUC_SAVE");
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();