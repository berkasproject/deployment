﻿(function () {
	'use strict';

	angular.module("app").controller("goodsTechEvalResulDetermineVendorCtrl", ctrl);

	ctrl.$inject = ['goodsTechEvalResulDetermineVendorService', 'UIControlService', '$stateParams', '$translatePartialLoader', '$state'];

	function ctrl(goodsTechEvalResulDetermineVendorService, UIControlService, $stateParams, $translatePartialLoader, $state) {
		var vm = this;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("goods-technical-eval-result");
			GetStep();
		}

		function GetStep() {
			goodsTechEvalResulDetermineVendorService.GetStep({
				TenderStepDataID: vm.StepID
			}, function (reply) {
				var result = reply.data;
				vm.StartDate = UIControlService.convertDateTime(result.StartDate);
				vm.EndDate = UIControlService.convertDateTime(result.EndDate);
				vm.TenderName = result.TenderName;

				vm.Summary = result.Summary;
				vm.TenderID = result.TenderID;
				vm.VendorName = result.VendorName;
				vm.IsPassed = result.IsPassed;
				vm.IsInformedToVendor = result.IsInformedToVendor;
				UIControlService.unloadLoading();
			}, function (error) {
				UIControlService.unloadLoading();
				// UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('detail-tahapan-vendor', { TenderID: vm.TenderID });
		}
	}
})();