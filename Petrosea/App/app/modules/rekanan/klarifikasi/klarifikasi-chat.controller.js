﻿(function () {
    'use strict';

    angular.module("app").controller("ClarificationChatVCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'ClarificationChatVendorService',
        'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, ClarificationChatVendorService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.StepID = Number($stateParams.StepID);
        vm.TenderID = Number($stateParams.TenderID);        
        
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;

        vm.stepInfo = {};
        vm.chats = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("klarifikasi");

            UIControlService.loadLoading("");
            ClarificationChatVendorService.GetStepInfo({
                ID: vm.StepID
            }, function (reply) {
                vm.stepInfo = reply.data;
                ClarificationChatVendorService.IsAllowed({
                    ID: vm.StepID
                }, function (reply) {
                    vm.isAllowed = reply.data;
                    loadChats(1);
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
            });
        }

        //tampil isi chat
        vm.loadChats = loadChats;
        function loadChats(current) {
            vm.currentPage = current;
            UIControlService.loadLoading("");
            ClarificationChatVendorService.GetChats({
                column: vm.StepID, //tenderStepDataId
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.chats = reply.data.List;
                vm.totalItems = reply.data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "ERR_LOAD_CHATS");
                UIControlService.unloadLoading();
            });
        }

        vm.tulis = tulis;
        function tulis() {
            var data = {
                StepID: vm.StepID,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/klarifikasi/write-chat.modal.html?v=1.000002',
                controller: 'WriteClarificationChatVCtrl',
                controllerAs: 'wccvCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadChats(vm.currentPage);
            });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('detail-tahapan-vendor', { TenderID: vm.TenderID });
        }
    }
})();


