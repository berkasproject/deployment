(function () {
	'use strict';

	angular.module("app").controller("PemasukkanPenawaranBarangVendorCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'GoodOfferEntryService', '$state', 'UIControlService', 'UploaderService', '$uibModal', 'GlobalConstantService', '$stateParams', 'PurchaseRequisitionService', 'VerifiedSendService', 'UploadFileConfigService'];
	function ctrl($translatePartialLoader, GoodOfferEntryService, $state, UIControlService, UploaderService, $uibModal, GlobalConstantService, $stateParams, PurchReqService, VerifiedSendService, UploadFileConfigService) {
		var vm = this;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.maxSize = 10;
		vm.dataUpload = {
			FileName: "",
			DocUrl: "",
			TenderStepId: 0
		}
		vm.IsSubmitTechnical = false;
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.StepID = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.IDDoc = Number($stateParams.DocID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.habisTanggal = false;
		vm.showterendah = true;
		vm.selectedCurrencies = null;
		vm.goods = [];
		vm.selectedFreightCostTime = {};
		vm.init = init;
		vm.Date;
		vm.listUploadDocSupport = [];
		vm.status = 2;
		vm.listUpload = [];
		vm.listyyy = [];
		vm.checked = [];
		vm.checkedAll = false;
		vm.listOtherDoc = [];
		vm.listOther = [];
		vm.listDelete = [];
		vm.offer = 0;
		vm.fileUpload1 = undefined;
		vm.Remark = undefined;
		vm.IsFlagQuotation = true;
		vm.IsFlagInco = true;
		vm.IsFlagState = true;
		vm.IsFlagCurr = true;
		function init() {

			vm.flagDoc = 0;
			vm.flagUploadDoc = 1;
			$translatePartialLoader.addPart('pemasukkan-penawaran-barang');
			UIControlService.loadLoading("MESSAGE.LOADING");

			loadVerifiedVendor();
			UploadFileConfigService.getByPageName("PAGE.VENDOR.GOODSOFFERENTRY", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", "ERR.TYPEFILE");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			loadSteps();
			loadRFQItem();
			//loadVendorCurrency();
		}

		vm.loadSteps = loadSteps;
		function loadSteps() {
			GoodOfferEntryService.GetSteps({
				ID: vm.StepID
			}, function (reply) {
				vm.step = reply.data;
				for (var i = 0; i < vm.step.length; i++) {
					if (vm.step[i].step.FormTypeURL == "pemasukan-penawaran-barang") {
						vm.IsOvertime = vm.step[i].IsOvertime;
					}
				}
				UIControlService.unloadLoading();
			}, function (error) {
				UIControlService.unloadLoading();
				// UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
			});
		};

		vm.loadVendorCurrency = loadVendorCurrency;
		function loadVendorCurrency() {
			GoodOfferEntryService.loadVendorCurrency({}, function (reply) {
				vm.listCurrencies = reply.data;
				for (var i = 0; i < vm.listCurrencies.length; i++) {
					if (vm.goods[0].goods.RateID === vm.listCurrencies[i].CurrencyID) {
						vm.selectedCurrencies = vm.listCurrencies[i];
						changeCurrent();
						break;
					}
				}
				//loadCurrencies(vm.vendorcurr);
			}, function (error) {
				UIControlService.unloadLoading();
				// UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
			});
		};

		function convertTanggal(input) {
			return UIControlService.convertDateTime(input);
		};

		vm.selectedPaymentTerm;
		vm.listPaymentTerm = [];
		function loadPaymentTerm(data) {
			GoodOfferEntryService.getPaymentTerm(function (reply) {
				UIControlService.unloadLoading();
				vm.listPaymentTerm = reply.data;
				for (var i = 0; i < vm.listPaymentTerm.length; i++) {
					if (data === vm.listPaymentTerm[i].Id) {
						vm.selectedPaymentTerm = vm.listPaymentTerm[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadReference = loadReference;
		function loadReference(data) {
			GoodOfferEntryService.DeliveryTerm({}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.DeliveryTerm = reply.data.List;
					for (var i = 0; i < vm.DeliveryTerm.length; i++) {
						if (data == vm.DeliveryTerm[i].RefID) vm.selectedDeliveryTerms = vm.DeliveryTerm[i];
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.checkAll = checkAll
		function checkAll() {
			if (vm.checkedAll) {
				for (var i = 0; i < vm.goods.length; i++) {
					if (!vm.goods[i].Checklist)
						vm.goods[i].Checklist = true
				}
			} else {
				for (var i = 0; i < vm.goods.length; i++) {
					if (vm.goods[i].Checklist)
						vm.goods[i].Checklist = false
				}
			}

			checkItem()
		}

		vm.checkItem = checkItem
		function checkItem() {
			vm.checked = [];

			for (var i = 0; i < vm.goods.length; i++) {
				if (vm.goods[i].Checklist) {
					vm.checked.push(i);
				}
			}

			if (vm.checked.length == vm.goods.length) {
				vm.checkedAll = true
			} else {
				vm.checkedAll = false
			}
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.IsFlagQuotation = true;
			vm.IsFlagInco = true;
			vm.IsFlagState = true;
			vm.IsFlagCurr = true;
			vm.currentPage = current;
			var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
			var tender = {
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType,
				Offset: offset,
				Limit: vm.pageSize,
				column: vm.StepID
			}

			GoodOfferEntryService.select(tender, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.goods = reply.data.List;
				    console.log(vm.goods);
					vm.IsSubmitTechnical = vm.goods[0].goods.IsSubmitTechnical;
					if (vm.goods[0].goods.QuotationNo == null) vm.IsFlagQuotation = false;
					if (vm.goods[0].goods.DeliveryTerms !== 3087) {
						if (vm.goods[0].goods.IncoId == null) vm.IsFlagInco = false;
						if (vm.goods[0].goods.FreighCostId == null) vm.IsFlagState = false;
						if (vm.goods[0].goods.RateID == null) vm.IsFlagCurr = false;
					}
					vm.goods[0].goods.StartDateTen = UIControlService.convertDateTime(vm.goods[0].goods.StartDateTen);
					vm.goods[0].goods.EndDateTen = UIControlService.convertDateTime(vm.goods[0].goods.EndDateTen);
					vm.goods[0].goods.SubmitDate = UIControlService.convertDateTime(vm.goods[0].goods.SubmitDate);
					vm.totalItems = reply.data.Count;
					for (var i = 0; i < vm.goods.length; i++) {
						if (i == vm.goods.length - 1) {
							vm.offer = vm.goods[i].goods.OfferTotalCost;
							vm.Date = vm.goods[i].goods.CurrencyDate;
						}
					}

					if (vm.goods[0].goods.ID !== 0) {
						loadOtherDoc(vm.goods[0].goods.ID);
						vm.QLTime = vm.goods[0].goods.SupplierQLTime;
						loadFreightCost2(vm.goods[0].goods.FreighCostId);
					} else {
					    loadFreightCost2(vm.goods[0].goods.FreighCostId);
					}

					if (vm.goods[0].goods.IncoId !== null) {
						vm.GoodsData = {
							IncoTerm: vm.goods[0].goods.IncoId,
							FreightCostID: vm.goods[0].goods.FreighCostId,
							BidderSelMethod: vm.RfqID.BidderSelMethod,
							DeliveryTerms: vm.RfqID.DeliveryTerms
						}
						loadIncoTerms(vm.GoodsData, 1);
					} else {

						loadIncoTerms(vm.RfqID, 0);
					}

					//loadIncoTerms();
					loadStateDelivery();
					loadVendorCurrency();
					loadDeliveryTender();
					loadDeliveryTerms();
					loadFreightCost();
					loadRFQGoods();
					console.log(vm.goods[0])

					loadPaymentTerm(vm.goods[0].goods.PaymentTerms);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Penawaran Barang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadOtherDoc = loadOtherDoc;
		function loadOtherDoc(data) {
			vm.listOtherDoc = [];
			GoodOfferEntryService.getOtherDoc({ Status: data }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.data = reply.data;
					for (var i = 0; i < vm.data.length; i++) {
						vm.listOtherDoc.push({
							ID: vm.data[i].ID,
							fileUpload: [{
								name: vm.data[i].Filename
							}],
							Remark: vm.data[i].Remark,
							DocUrl: vm.data[i].DocUrl
						});
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.batal = batal;
		function batal() {
			$state.transitionTo('detail-tahapan-vendor', { TenderID: vm.goods[0].goods.TenderID });
		}

		vm.changePrice = changePrice;
		function changePrice(data) {
			data.TotalPrice = data.UnitPrice * data.item.Quantity;

		}

		vm.listIncoTerms = [];
		vm.loadIncoTerms = loadIncoTerms;
		function loadIncoTerms(data, flag) {
			vm.flagFreight = flag;
			GoodOfferEntryService.getIncoTerms({
				BidderSelMethod: data.BidderSelMethod,
				DeliveryTerms: data.DeliveryTerms
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.listIncoTerms = reply.data;
				for (var i = 0; i < vm.listIncoTerms.length; i++) {
					if (data.IncoTerm === vm.listIncoTerms[i].ID) {
						vm.selectedIncoTerms = vm.listIncoTerms[i];
						vm.changeFreightDetai(data, flag);
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//vm.selectedFreight;
		//vm.listFreight = [];
		//vm.changeFreightDetai = changeFreightDetai;
		//function changeFreightDetai(data, flag) {
		//	if (data) vm.Id = data.IncoTerm;
		//	else vm.Id = vm.selectedIncoTerms.ID;
		//	GoodOfferEntryService.selectFreight({
		//		Status: vm.Id
		//	}, function (reply) {
		//		UIControlService.unloadLoading();
		//		vm.listFreight = reply.data;
		//		if (data) {
		//		    for (var i = 0; i < vm.listFreight.length; i++) {
		//		        if (vm.goods[0].goods.FreighCostId == vm.RfqGoods[0].DeliveryLocationState) {
		//		            if (vm.RfqGoods[0].DeliveryLocationState === vm.listFreight[i].FreightCostID) {
		//		                vm.selectedState = vm.listFreight[i];
		//		                console.info(vm.selectedState);
		//		                break;
		//		            }
		//		        }
		//			}
		//		}
		//	}, function (err) {
		//		//UIControlService.msg_growl("error", "MESSAGE.API");
		//		UIControlService.unloadLoading();
		//	});
		//}

		vm.selectedState;
		vm.listState = [];
		function loadStateDelivery() {
			PurchReqService.getStateDelivery(function (reply) {
				UIControlService.unloadLoading();
				vm.listState = reply.data;
				//for (var i = 0; i < vm.listState.length; i++) {
				//    if (vm.RfqGoods[0].DeliveryLocationState === vm.listState[i].FreightCostID) {
				//        vm.selectedState = vm.listState[i];
				//        loadCityDelivery(vm.selectedState);
				//        //changeState();
				//        break;
				//    }
				//}
			}, function (err) {
				// UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedState;
		vm.selectedFreightCost2;
		vm.listFreightCost2 = [];
		vm.listFreight = [];
		function loadFreightCost2(data) {
			GoodOfferEntryService.GetFreightCost(function (reply) {
				UIControlService.unloadLoading();
				vm.listFreightCost2 = reply.data;
				vm.listFreight = reply.data;
				if (data !== null) {
				    for (var i = 0; i < vm.listFreightCost2.length; i++) {
						if (vm.listFreightCost2[i].FreightCostID == data) {
						    vm.selectedState = vm.listFreight[i];
						}
					}
				}

			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeState = changeState;
		function changeState(data) {
			loadCityDelivery(data.StateID);
		}

		//list City
		vm.selectedCity;
		vm.listCity = [];
		function loadCityDelivery(data) {
			PurchReqService.getCityDelivery({ StateID: data.StateID }, function (reply) {
				UIControlService.unloadLoading();
				vm.listCity = reply.data;
				for (var i = 0; i < vm.listCity.length; i++) {
					if (vm.goods[0].goods.CityID === vm.listCity[i].CityID) {
						vm.selectedCity = vm.listCity[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadCurrencies = loadCurrencies;
		vm.selectedCurrencies;
		vm.listCurrencies = [];
		var CurrencyID;
		function loadCurrencies(dt) {
			GoodOfferEntryService.getCurrencies(function (response) {
				if (response.status === 200) {
					vm.listCurrencies = response.data;
					for (var i = 0; i < vm.listCurrencies.length; i++) {
						if (dt.CurrencyID === vm.listCurrencies[i].CurrencyID) {
							vm.selectedCurrencies = vm.listCurrencies[i];
							changeCurrent();
							break;
						}
					}
				} else {
					//UIControlService.msg_growl("error", "Gagal mendapatkan list Currency");
					return;
				}
			}, function (err) {
				// UIControlService.msg_growl("error", "Gagal Akses API");
				return;
			});
		}

		vm.selectedDeliveryTerms;
		vm.listDeliveryTerms = [];
		function loadDeliveryTerms() {
			PurchReqService.getDeliveryTerms(function (reply) {
				UIControlService.unloadLoading();
				vm.listDeliveryTerms = reply.data.List;
				for (var i = 0; i < vm.listDeliveryTerms.length; i++) {
					if (vm.goods[0].goods.DeliveryTerms === vm.listDeliveryTerms[i].RefID) {
						vm.selectedDeliveryTerms = vm.listDeliveryTerms[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadRFQItem = loadRFQItem;
		function loadRFQItem() {
			var status = {
				Status: vm.TenderRefID
			}
			GoodOfferEntryService.GetRFQ(status, function (reply) {
				UIControlService.unloadLoading();
				vm.RfqID = reply.data;
				jLoad(vm.currentPage);
			}, function (err) {
				// UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedTypeTender;
		vm.loadTypeTender = loadTypeTender;
		function loadTypeTender() {
			GoodOfferEntryService.getTypeTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listTypeTender = reply.data.List;
				for (var i = 0; i < vm.listTypeTender.length; i++) {
					if (vm.RfqGoods[0].TenderType === vm.listTypeTender[i].RefID) {
						vm.selectedTypeTender = vm.listTypeTender[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//list combo tipe penawaran (options tender)
		vm.selectedOptionsTender;
		vm.loadOptionsTender = loadOptionsTender;
		function loadOptionsTender() {
			GoodOfferEntryService.getOptionsTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listOptionsTender = reply.data.List;
				for (var i = 0; i < vm.listOptionsTender.length; i++) {
					if (vm.RfqGoods[0].TenderOption === vm.listOptionsTender[i].RefID) {
						vm.selectedOptionsTender = vm.listOptionsTender[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedDeliveryTender;
		vm.loadDeliveryTender = loadDeliveryTender;
		function loadDeliveryTender() {
			GoodOfferEntryService.getDeliveryTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listDeliveryTender = reply.data.List;
				for (var i = 0; i < vm.listDeliveryTender.length; i++) {
					if (vm.goods[0].goods.DeliveryTerms === vm.listDeliveryTender[i].RefID) {
						vm.selectedDeliveryTender = vm.listDeliveryTender[i];
						break;
					}
				}
			}, function (err) {
				// UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

        vm.deleteRow = deleteRow;
        function deleteRow(index, data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_DELETE_DOC'), function (res) {
                if (res) {
                    var idx = index - 1;
                    var _length = vm.listOtherDoc.length; // panjangSemula
                    vm.listOtherDoc.splice(idx, 1);
                    if (data.ID != undefined) {
                        data.TenderStepId = vm.StepID;
                        vm.listDelete.push(data);
                    }
                }
            });
        };

		vm.loadRFQGoods = loadRFQGoods;
		function loadRFQGoods() {
			var status = {
				Status: vm.TenderRefID
			}
			GoodOfferEntryService.GetRFQGoods(status, function (reply) {
				UIControlService.unloadLoading();
				vm.RfqGoods = reply.data;
				vm.TenderType = reply.data[0].TenderType;
				loadReference(reply.data[0].DeliveryTerms);
				loadTypeTender();
				loadOptionsTender();
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedFreightCost;
		vm.listFreightCost = [];
		function loadFreightCost() {
			GoodOfferEntryService.getTypeFreightCost(function (reply) {
				UIControlService.unloadLoading();
				vm.listFreightCost = reply.data.List;
				for (var i = 0; i < vm.listFreightCost.length; i++) {
					if (vm.goods[0].goods.FreightCostType === vm.listFreightCost[i].RefID) {
						vm.selectedFreightCost = vm.listFreightCost[i];
						changeFreight(vm.selectedFreightCost.RefID);
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeFreight = changeFreight;
		vm.freight = [];
		vm.selectedFreightCostTime;
		function changeFreight(data) {
			GoodOfferEntryService.GetFreightTime({ Status: data }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.freight = reply.data;
					for (var i = 0; i < vm.freight.length; i++) {
						if (vm.goods[0].goods.FreightLeadTime === vm.freight[i].DeliveryTime) {
							vm.selectedFreightCostTime = vm.freight[i];
						}
					}

				} else {
					$.growl.error({ message: "Gagal mendapatkan data Penawaran Barang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.changeCurrent = changeCurrent;
		function changeCurrent() {
			if (vm.selectedCurrencies.MstCurrency.Symbol == 'IDR') {
				return;
			}
			var data = {
				Keyword: vm.selectedCurrencies.MstCurrency.Symbol
			}
			GoodOfferEntryService.GetExchangeRate(data, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.rate = reply.data;
					vm.ExchangeRate = vm.rate.ExchangeRate;
					vm.Date = vm.rate.ValidFrom;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Penawaran Barang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.deleteRow = deleteRow;
		function deleteRow(index, data) {
			var idx = index - 1;
			var _length = vm.listOtherDoc.length; // panjangSemula
			vm.listOtherDoc.splice(idx, 1);
			if (data.ID != undefined) {
				data.TenderStepId = vm.StepID;
				vm.listDelete.push(data);
			}
		};

		vm.uploadFile = uploadFile;
		function uploadFile(flag, iplus, data) {

			if (flag === 0) {
				if (validateFileType(data, vm.idUploadConfigs)) {
					upload(iplus, data, vm.idFileSize, vm.idFileTypes, "");
				}
			} else {
				if (validateFileType1(data, vm.idUploadConfigs)) {
					upload1(iplus, data, vm.idFileSize, vm.idFileTypes, "");
				}
			}
		}

		function validateFileType(data, allowedFileTypes) {
			var allowed = false;
			if (!data.fileUpload || data.fileUpload.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		function validateFileType1(data, allowedFileTypes) {
			var allowed = false;
			if (!data || data.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload1 = upload1;
		function upload1(iplus, data, config, filters, callback) {
			vm.flagS = 0;
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}
			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
			vm.dataUpload = {
				CurrencyDate: UIControlService.getStrDate(vm.Date),
				FreightLeadTime: 1, //Belum di null
				SupplierQLTime: vm.QLTime,
				StartDate: vm.goods[0].goods.StartDate
			}
			if (vm.selectedCurrencies != null)
				vm.dataUpload.RateID = vm.selectedCurrencies.MstCurrency.CurrencyID;
			var id = data[0].name;
			vm.dataUpload.FileName = data[0].name;
			UploaderService.uploadSingleFileGoodsOfferEntry(id, data, size, filters, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.flagS = 1;
					var url = response.data.Url;
					vm.pathFile = url;
					vm.dataUpload.Remark = vm.Remark;
					vm.dataUpload.DocUrl = vm.pathFile;
					vm.name = response.data.FileName;
					vm.dataUpload.TenderStepId = vm.StepID;
					if (vm.selectedIncoTerms == null) vm.dataUpload.IncoId = null;
					else vm.dataUpload.IncoId = vm.selectedIncoTerms.ID;
					if (vm.selectedState == null) vm.dataUpload.FreighCostId = null;
					else vm.dataUpload.FreighCostId = vm.selectedState.FreightCostId;
					vm.listUploadDocSupport.push(vm.dataUpload);
					var s = response.data.FileLength;
					GoodOfferEntryService.UploadOther(vm.listUploadDocSupport, function (reply) {
						if (reply.status === 200) {
							vm.fileUpload1 = undefined;
							vm.Remark = undefined;
							vm.listUploadDocSupport = [];
							loadOtherDoc(reply.data);
						} else {
							//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
							return;
						}
					}, function (err) {
						//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
					});
				} else {
					//UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				//UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD")
				UIControlService.unloadLoading();
			});


		}

		vm.upload = upload;
		function upload(iplus, data, config, filters, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}
			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			if (iplus == 0)
				UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
			var id = data.ShortText + '_' + data.VendorName;
			UploaderService.uploadSingleFileGoodsOfferEntry(id, data.fileUpload, size, filters, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.pathFile = url;
					data.DocUrl = vm.pathFile;
					vm.name = response.data.FileName;
					vm.listyyy.push(data);
					var s = response.data.FileLength;
					simpanitemPR(data);
				} else {
					//UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				//UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});
		}

		vm.selectUpload = selectUpload;
		function selectUpload(data) {
			vm.listUpload.push({
				ItemPRId: data.ItemPRId,
				ShortText: data.item.ShortText,
				VendorName: data.goods.VendorName,
				fileUpload: data.fileUpload,
				TenderStepID: vm.StepID
			});
		}

		vm.Simpan = Simpan;
		function Simpan() {
			vm.cek = 0;
			if (vm.goods[0].goods.QuotationNo == null || vm.goods[0].goods.QuotationNo == '' || vm.goods[0].goods.QuotationNo == undefined) {
				vm.cek = 1;
				UIControlService.msg_growl("error", "ERR.QUOTATION")
				return;
			}

			if (vm.selectedCurrencies == null || vm.selectedCurrencies == '' || vm.selectedCurrencies == undefined) {
				vm.cek = 1;
				UIControlService.msg_growl("error", "ERR.CURRENCY")
				return;
			}

			if (vm.selectedDeliveryTender.Name == "RFQ_CUSTOM") {
				if (vm.selectedIncoTerms == null) {
					vm.cek = 1;
					UIControlService.msg_growl("error", "ERR.INCOTERMS")
					return;
				} else if (vm.selectedState == null) {
					vm.cek = 1;
					UIControlService.msg_growl("error", "ERR.FREIGHCOST")
					return;
				}
			}
			if (vm.cek == 0) {
				vm.list = [];
				vm.up = 0;
				vm.flagList = false;
				GoodOfferEntryService.UploadOther(vm.listDelete, function (reply) {
					if (reply.status === 200) { }
					else {
						//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
						return;
					}
				}, function (err) {
					// UIControlService.msg_growl("error", "Gagal menyimpan data!!");
				});
				var data = {
					goods: {
						TenderStepID: vm.goods[0].goods.TenderStepID,
						VendorID: vm.goods[0].goods.VendorID,
						RateID: vm.selectedCurrencies.MstCurrency.CurrencyID,
						CurrencyDate: UIControlService.getStrDate(vm.Date),
						FreightLeadTime: 1, //Belum di null
						SupplierQLTime: vm.QLTime,
						StartDate: vm.goods[0].goods.StartDate,
						QuotationNo: vm.goods[0].goods.QuotationNo,
						PaymentTerms: vm.selectedPaymentTerm.Id
					}
				}
				if (vm.selectedIncoTerms == null) data.goods.IncoId = null;
				else data.goods.IncoId = vm.selectedIncoTerms.ID;
				if (vm.selectedState == null) data.goods.FreighCostId = null;
				else if (vm.selectedState.FreightCostId != null) data.goods.FreighCostId = vm.selectedState.FreightCostId;
				else data.goods.FreighCostId = vm.selectedState.FreightCostID;

				console.log(data);
				vm.list.push(data);
				GoodOfferEntryService.Insert(vm.list, function (reply) {
					var x = 0, k = 0;
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "SUCCESS.SAVE");
						init();
					}
					else {
						//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
						return;
					}
				}, function (err) {
					//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
				});
			}
		}

		vm.SimpanItem = SimpanItem;
		function SimpanItem(data) {
			if (data.LeadTime == 0) {
				UIControlService.msg_growl("error", "ERR.LEADTIME_NULL");
				return;
			} else if (data.UnitPrice == 0) {
				UIControlService.msg_growl("error", "ERR.UNITPRICE_NULL");
				return;
			} else {
				UIControlService.loadLoading("Loading");
				vm.list = [];
				vm.up = 0;
				vm.flagList = false;
				console.info(data.fileUpload);

				if (data.fileUpload != undefined) {
					uploadFile(0, 0, data);
				} else
					simpanitemPR(data);
			}
		}

		vm.saveAllChecked = saveAllChecked;
		function saveAllChecked() {
			for (var i = 0; i < vm.checked.length; i++) {
				var data = vm.goods[vm.checked[i]]

				if (data.LeadTime == 0) {
					UIControlService.msg_growl("error", "ERR.LEADTIME_NULL");
					return;
				} else if (data.UnitPrice == 0) {
					UIControlService.msg_growl("error", "ERR.UNITPRICE_NULL");
					return;
				}
			}


			for (var i = 0; i < vm.checked.length; i++) {
				var data = vm.goods[vm.checked[i]]

				if (data.LeadTime == 0) {
					UIControlService.msg_growl("error", "ERR.LEADTIME_NULL");
					return;
				} else if (data.UnitPrice == 0) {
					UIControlService.msg_growl("error", "ERR.UNITPRICE_NULL");
					return;
				} else {
					UIControlService.loadLoading("Loading");
					vm.list = [];
					vm.up = 0;
					vm.flagList = false;
					console.info(data.fileUpload);

					if (data.fileUpload != undefined) {
						uploadFile(0, 0, data);
					} else
						simpanitemPR(data);
				}
			}
		}

		vm.simpanitemPR = simpanitemPR;
		function simpanitemPR(dataPR) {
			dataPR.IsSave = true;
			var data = {
				ID: dataPR.ID,
				GoodsOEId: dataPR.GoodsOEId,
				ItemPRId: dataPR.ItemPRId,
				UnitPrice: dataPR.UnitPrice,
				LeadTime: dataPR.LeadTime,
				DocUrl: dataPR.DocUrl,
				Alternative: dataPR.Alternative,
				Remark: dataPR.Remark,
				goods: {
					TenderStepID: vm.goods[0].goods.TenderStepID,
					VendorID: vm.goods[0].goods.VendorID,
					RateID: vm.selectedCurrencies == null ? null : vm.selectedCurrencies.MstCurrency.CurrencyID,
					CurrencyDate: UIControlService.getStrDate(vm.Date),
					FreightLeadTime: 1, //Belum di null
					SupplierQLTime: vm.QLTime,
					StartDate: vm.goods[0].goods.StartDate,
					QuotationNo: vm.goods[0].goods.QuotationNo
				},
				item: {
					Quantity: dataPR.item.Quantity
				}
			}
			if (vm.selectedIncoTerms == null) data.goods.IncoId = null;
			else data.goods.IncoId = vm.selectedIncoTerms.ID;
			if (vm.selectedState == null) data.goods.FreighCostId = null;
			else data.goods.FreighCostId = vm.selectedState.FreightCostId;

			vm.list.push(data);
			GoodOfferEntryService.InsertDetail(vm.list, function (reply) {
				var x = 0, k = 0;
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "SUCCESS.SAVE");
					UIControlService.unloadLoading();
					jLoad(vm.currentPage);
				}
				else {
					//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
			});
		}

		vm.SimpanUpload = SimpanUpload;
		function SimpanUpload() {
			for (var i = 0; i < vm.listOtherDoc.length; i++) { //1
				vm.listOtherDoc[i].TenderStepId = vm.StepID;
				if (vm.listOtherDoc[i].ID == undefined) {
					uploadFile(1, vm.flagUploadDoc, vm.listOtherDoc[i]);
				}
			}
		}

		//ambil VendorID
		vm.loadVerifiedVendor = loadVerifiedVendor;
		function loadVerifiedVendor() {
			VerifiedSendService.selectVerifikasi(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data;
					vm.cekTemporary = vm.verified.IsTemporary;
					vm.VendorID = vm.verified.VendorID;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.selectUpload1 = selectUpload1;
		function selectUpload1(data) {
			console.info(data);
		}

		vm.addOtherDoc = addOtherDoc;
		function addOtherDoc() {
			uploadFile(1, 0, vm.fileUpload1);
		}

		vm.Submit = Submit;
		function Submit() {
			if (vm.offer == undefined || vm.offer == 0) {
				vm.cek = 1;
				UIControlService.msg_growl("error", "ERR.PRICE0")
				return;
			}
			if (vm.IsFlagQuotation == false) {
				vm.cek = 1;
				UIControlService.msg_growl("error", "ERR.QUOTATION")
				return;
			}
			else if (vm.IsFlagCurr == false) {
				vm.cek = 1;
				UIControlService.msg_growl("error", "ERR.CURRENCY")
				return;
			}
			else if (vm.selectedDeliveryTender.Name == "RFQ_CUSTOM") {
				if (vm.IsFlagInco == false) {
					vm.cek = 1;
					UIControlService.msg_growl("error", "ERR.INCOTERMS")
					return;
				}
				else if (vm.IsFlagState == false) {
					vm.cek = 1;
					UIControlService.msg_growl("error", "ERR.FREIGHCOST")
					return;
				}
			}

			GoodOfferEntryService.cekSubmit({ Status: vm.TenderRefID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.flag = reply.data;
					if (vm.flag == true) {
						SaveSubmit();
					}
					else {
						if (vm.selectedOptionsTender.Name === "TENDER_OPTIONS_ITEMIZE") {
							SaveSubmit();
						}
						else {
							UIControlService.msg_growl("error", "ERR.MAX_DATA");
							return;
						}
					}
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.SaveSubmit = SaveSubmit;
		function SaveSubmit() {
			GoodOfferEntryService.InsertSubmit({ TenderStepID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "SUCCESS.SUBMIT");
					init();
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.DeleteItem = DeleteItem;
		function DeleteItem(data) {
			if (vm.IsSubmitTechnical) {
				data.LeadTime = 0;
				data.UnitPrice = 0;
				data.Alternative = '';
				return;
			}

			GoodOfferEntryService.DeleteItem({ ID: data.ID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "SUCCESS.DELETE");
					jLoad(vm.currentPage);
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
	}
})();
