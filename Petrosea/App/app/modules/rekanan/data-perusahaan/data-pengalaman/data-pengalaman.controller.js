(function () {
	'use strict';

	angular.module("app").controller("DataPengalamanController", ctrl);

	ctrl.$inject = ['$timeout', '$filter', '$uibModal', '$translatePartialLoader', 'UIControlService', 'VendorExperienceService'];
	/* @ngInject */
	function ctrl($timeout, $filter, $uibModal, $translatePartialLoader, UIControlService, VendorExperienceService) {
		var vm = this;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.vendorID = [];
		vm.comodity = [];
		vm.isApprovedCR = false;

		vm.initialize = initialize;
		function initialize() {
			$translatePartialLoader.addPart('data-pengalaman');
			loadVendor();
			loadCheckCR();
			cekPrakualifikasiVendor();
		};

		vm.loadVendor = loadVendor;
		function loadVendor() {
			VendorExperienceService.selectVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.Vendor = reply.data;
				    vm.IsVerified = vm.Vendor.Isverified;
				    console.log(vm.IsVerified);
					loadVendorCommodity(vm.Vendor.VendorID);
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
		function cekPrakualifikasiVendor() {
			VendorExperienceService.cekPrakualifikasiVendor(function (reply) {
				UIControlService.loadLoadingModal();
				if (reply.status === 200) {
					vm.pqWarning = reply.data;
					console.info("isregistered" + vm.pqWarning);
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
			});
		}

		vm.loadVendorCommodity = loadVendorCommodity;
		function loadVendorCommodity(data) {
			VendorExperienceService.SelectVendorCommodity({
				VendorID: data
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					console.info(reply.status);
					vm.comodity = reply.data;
					loadawal(vm.comodity.BusinessFieldID);
					console.info("comodity:" + JSON.stringify(vm.comodity));
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		function loadCheckCR() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			console.info("CR:");
			VendorExperienceService.getCRbyVendor({ CRName: 'OC_VENDOREXPERIENCE' }, function (reply) {
				UIControlService.unloadLoading();
				console.info("CR:" + JSON.stringify(reply.status));
				if (reply.status === 200) {
					console.info("CR:" + JSON.stringify(reply.data));
					vm.CR = reply.data;
					if (reply.data === true) {
						vm.isApprovedCR = true;
					} else {
						vm.isApprovedCR = false;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.addExp = addExp;
		function addExp(data, isAdd, isCR) {
			console.info(isCR);
			var sendData = {
				item: data,
				isAdd: isAdd,
				isCR: isCR
			}
			//console.info(JSON.stringify(sendData));
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/data-pengalaman/form-pengalaman.html',
				controller: 'formExpCtrl',
				controllerAs: 'formExpCtrl',
                backdrop:'static',
				resolve: {
					item: function () {
						return sendData;
					}
				}
			});

			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.detailExp = detailExp;
		function detailExp(data) {
			var sendData = {
				item: data,
				isAdd: false
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/data-pengalaman/detail-pengalaman.html',
				controller: 'formExpCtrl',
				controllerAs: 'formExpCtrl',
				resolve: {
					item: function () {
						return sendData;
					}
				}
			});

			modalInstance.result.then(function () {
				loadawal();
			});
		}

		vm.loadAwal = loadawal;
		function loadawal(data) {
			//limit sementara dibuat 100
			UIControlService.loadLoading('MESSAGE.LOADING');
			VendorExperienceService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: 100,
				Keyword: vm.keyword,
				FilterType: data,
				column: 1
			}, function (reply) {
				console.info("lrjlbij" + vm.comodity.BusinessFieldID);
				if (reply.status === 200) {
					vm.listFinishExp = reply.data.List;
					console.info("listFinishExp:" + JSON.stringify(vm.listFinishExp));
					for (var i = 0; i < vm.listFinishExp.length; i++) {
						vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);

						if (vm.listFinishExp[i].CityLocation == null) {
							vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address;
						} else {
							vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address + ", " + vm.listFinishExp[i].CityLocation.Name + ", " + vm.listFinishExp[i].CityLocation.State.Country.Name;
						}
					}
					vm.totalItems = reply.data.Count;
					console.info("finish:" + JSON.stringify(vm.listFinishExp));
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
			});

			UIControlService.loadLoading('MESSAGE.LOADING');
			VendorExperienceService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: 100,
				Keyword: vm.keyword,
				column: 2
			}, function (reply) {
				//console.info("current?:"+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listCurrentExp = reply.data.List;
					for (var i = 0; i < vm.listCurrentExp.length; i++) {
						vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
						if (vm.listCurrentExp[i].CityLocation == null) {
							vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address;
						} else {
							vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].CityLocation.Name + ", " + vm.listCurrentExp[i].CityLocation.State.Country.Name;
						}
					}
					console.info("current exp:" + JSON.stringify(vm.listCurrentExp));
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
			});

		}

		vm.editActive = editActive;
		function editActive(data, active) {
			swal({				
				icon: "warning",
				text: $filter('translate')('MESSAGE.DELETECONFIRM'),		
				buttons: {
					cancel: {
						text: "Batal",
						value: null,
						visible: true,
						className: "",
						closeModal: true,
						className: "red-bg",
					},
					confirm: {
						text: "Hapus Data",
						value: true,
						visible: true,
						className: "",
						closeModal: true
					},
				},
				closeOnClickOutside: false
			}).then((value) => {
				if (value) {
					UIControlService.loadLoading("MESSAGE.LOADING");
		            VendorExperienceService.Delete({ ID: data.ID, IsActive: active }, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.status === 200) {
		                    var msg = "";
		                    if (active === false) msg = "Hapus";
		                    if (active === true) msg = "Aktifkan ";
		                    UIControlService.msg_growl("success", "Data Berhasil di " + msg);
		                    $timeout(function () {
		                        window.location.reload();
		                    }, 1000);
		                } else {
		                    UIControlService.msg_growl("error", "MESSAGE.FAIL_NON_AKTIF");
		                    return;
		                }
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.FAIL_DELETE");
		                UIControlService.unloadLoading();
		            });
                }
			});
		    // bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.DELETECONFIRM') + '<h3>', function (reply) {
		    //     if (reply) {
		    //         UIControlService.loadLoading("MESSAGE.LOADING");
		    //         VendorExperienceService.Delete({ ID: data.ID, IsActive: active }, function (reply) {
		    //             UIControlService.unloadLoading();
		    //             if (reply.status === 200) {
		    //                 var msg = "";
		    //                 if (active === false) msg = "Hapus";
		    //                 if (active === true) msg = "Aktifkan ";
		    //                 UIControlService.msg_growl("success", "Data Berhasil di " + msg);
		    //                 $timeout(function () {
		    //                     window.location.reload();
		    //                 }, 1000);
		    //             } else {
		    //                 UIControlService.msg_growl("error", "MESSAGE.FAIL_NON_AKTIF");
		    //                 return;
		    //             }
		    //         }, function (err) {
		    //             UIControlService.msg_growl("error", "MESSAGE.FAIL_DELETE");
		    //             UIControlService.unloadLoading();
		    //         });
		    //     }
		    // });
		}
	}


})();// baru controller pertama
