﻿(function () {
	'use strict';

	angular.module("app").controller("SuratPernyataanCtrl", ctrl);

	ctrl.$inject = ['$http', '$window', '$translate', '$state', '$uibModal', '$translatePartialLoader', 'VerifiedSendService', 'SrtPernyataanService', 'UIControlService', 'GlobalConstantService', 'VendorRegistrationService'];
	/* @ngInject */
	function ctrl($http, $window, $translate, $state, $uibModal, $translatePartialLoader, VerifiedSendService, SrtPernyataanService, UIControlService, GlobalConstantService, VendorRegistrationService) {
		var vm = this;
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		vm.currentLang = $translate.use();
		$translatePartialLoader.addPart('daftar');
		vm.data = [];
		vm.pos;
		vm.cek;
		vm.DocUrlAgreement;
		vm.DocUrlBConduct;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.DocType = null;
		vm.isApprovedCR = false;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('surat-pernyataan');
			loadVerifiedVendor();
			if (localStorage.getItem("currLang") === 'id') {
				vm.DocType = 4225;
			} else {
				vm.DocType = 4232;
			}
			loadUrlLibrary(1);
			loadUrlLibraryAgree(1);
			loadUrlKuesioner();
			SrtPernyataanService.isUploadAllowed({ CRName: 'OC_STATEMENTLETTER' }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					if (reply.data === true) {
						vm.IsApprovedCR = true;
					} else {
						vm.IsApprovedCR = false;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});

		}

		vm.loadVendor = loadVendor();
		function loadVendor() {
			VendorRegistrationService.selectVendor({ VendorID: vm.VendorID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.vendor = reply.data;
					for (var i = 0; i < vm.vendor.length; i++) {
						if (vm.vendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN') {
							vm.address = vm.vendor[i].Contact.Address.AddressInfo + ' ' + vm.vendor[i].Contact.Address.AddressDetail;
							vm.VendorName = vm.vendor[i].Vendor.VendorName;
							vm.npwp = vm.vendor[i].Vendor.Npwp;
						}
						else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
							vm.email = vm.vendor[i].Contact.Email;
							vm.username = vm.vendor[i].Vendor.user.Username;
							vm.VendorName = vm.vendor[i].Vendor.VendorName;
						}
						else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {

							if (vm.telp == undefined) {
								vm.name = vm.vendor[i].Contact.Name;
								vm.telp = vm.vendor[i].Contact.Phone;
							}
						}
					}
				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DATA_DEPT" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadKuesionerDD = loadKuesionerDD;
		function loadKuesionerDD() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			SrtPernyataanService.loadDDQuest({
				VendorID: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.loadDDQuest = reply.data;
					console.info("loadDDquest:" + JSON.stringify(vm.loadDDQuest));
				} else {
					$.growl.error({ message: "Gagal mendapatkan dokumen" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadCek = loadCek;
		function loadCek() {
			VendorRegistrationService.CekVendor({ VendorID: vm.VendorID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.vendorQues = reply.data;
					vm.jloadKuesioner();
				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DATA_DEPT" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		//vm.jloadKuesioner = jloadKuesioner;
		//function jloadKuesioner() {
		//    UIControlService.loadLoading("LOADERS.LOADING");
		//    VendorRegistrationService.selectQuestionnaire({ Keyword: vm.currentLang },
		//       function (reply) {
		//           vm.data = [];
		//           vm.type = [];
		//           UIControlService.unloadLoading();
		//           if (reply.status === 200) {
		//               vm.list = reply.data;
		//               if (vm.vendorQues.length !== 0) {
		//                   vm.flag = 1;
		//                   for (var i = 0; i < vm.vendorQues.length; i++) {
		//                       for (var j = 0; j < vm.list[i].type.length; j++) {
		//                           var calldateType = {
		//                               ID: vm.list[i].type[j].ID,
		//                               question: vm.list[i].type[j].question,
		//                               DetailAnswer: vm.list[i].type[j].DetailAnswer
		//                           }
		//                           vm.type.push(calldateType);
		//                       }
		//                       vm.calldata = {
		//                           ID: vm.list[i].ID,
		//                           question: vm.list[i].question,
		//                           DetailId: vm.list[i].DetailId,
		//                           AnswerName: vm.list[i].AnswerName,
		//                           Value: vm.vendorQues[i].VendQuesDetailId,
		//                           Description: vm.vendorQues[i].Description,
		//                           type: vm.type
		//                       }
		//                       vm.data.push(vm.calldata);
		//                       vm.type = [];
		//                   }
		//               }
		//               else {
		//                   vm.data = vm.list;
		//               }
		//           }
		//       }, function (err) {
		//       });
		//}

		vm.jloadKuesioner = jloadKuesioner;
		function jloadKuesioner() {
			UIControlService.loadLoading("LOADERS.LOADING");
			VendorRegistrationService.selectQuestionnaire({ Keyword: vm.currentLang },
               function (reply) {
               	vm.data = [];
               	vm.type = [];
               	UIControlService.unloadLoading();
               	if (reply.status === 200) {
               		vm.list = reply.data;
               		//if (vm.vendorQues.length !== 0) {
               		//    vm.flag = 1;
               		//    for (var i = 0; i < vm.vendorQues.length; i++) {
               		//        for (var j = 0; j < vm.list[i].type.length; j++) {
               		//            var calldateType = {
               		//                ID: vm.list[i].type[j].ID,
               		//                question: vm.list[i].type[j].question,
               		//                DetailAnswer: vm.list[i].type[j].DetailAnswer
               		//            }
               		//            vm.type.push(calldateType);
               		//        }
               		//        vm.calldata = {
               		//            ID: vm.list[i].ID,
               		//            DDQuestName: vm.list[i].DDQuestName,
               		//            Value: vm.vendorQues[i].VendQuesDetailId,
               		//            Description: vm.vendorQues[i].Description,
               		//            type: vm.type
               		//        }
               		//        vm.data.push(vm.calldata);
               		//        vm.type = [];
               		//    }
               		//}
               		//else {
               		vm.DataDueDilligenceQuestionnaire = vm.list;
               		console.log(vm.DataDueDilligenceQuestionnaire);
               		//}
               	}
               }, function (err) {
               });
		}



		vm.downloadQuestionnaire = downloadQuestionnaire;
		function downloadQuestionnaire() {
			var questionaire = [];
			for (var i = 0; i < vm.data.length; i++) {
				var data = {
					Value: vm.data[i].Value,
					VendorID: vm.VendorID,
					Description: vm.data[i].Description,
					PrequalStepID: vm.PrequalStepID
				}
				questionaire.push(data);
			}
			var headers = {};
			headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

			$http({
				method: 'POST',
				url: endpoint + '/vendor/registration/generateQuestionnaire',
				headers: headers,
				data: questionaire,
				responseType: 'arraybuffer'
			}).success(function (data, status, headers) {
				var linkElement = document.createElement('a');
				var fileName = "Initial Risk Assessment Questionnaire " + vm.VendorName + ".pdf";

				try {
					var blob = new Blob([data], { type: headers('content-type') });
					var url = window.URL.createObjectURL(blob);
					linkElement.setAttribute('href', url);
					linkElement.setAttribute('download', fileName);

					var clickEvent = new MouseEvent("click", {
						"view": window,
						"bubbles": true,
						"cancelable": false
					});

					linkElement.dispatchEvent(clickEvent);
				} catch (e) {
					console.log(e);
				}
			});
			//var a = document.createElement("a");
			//document.body.appendChild(a);

			//PrequalCertificateService.GenerateCertificate({
			//	PrequalSetupStepID: vm.stepId
			//}, function (reply) {
			//	if (reply.status === 200) {

			//		var octetStreamMime = 'application/octet-stream';
			//		var success = false;
			//		var filename = 'download.docx';
			//		var contentType = octetStreamMime;
			//		var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;

			//		if (urlCreator) {
			//			var link = document.createElement('a');
			//			if ('download' in link) {
			//				try {
			//					// Prepare a blob URL
			//					console.log("Trying download link method with simulated click ...");
			//					var blob = new Blob([reply.data], { type: contentType });
			//					var url = urlCreator.createObjectURL(blob);
			//					link.setAttribute('href', url);

			//					// Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
			//					link.setAttribute("download", filename);

			//					// Simulate clicking the download link
			//					var event = document.createEvent('MouseEvents');
			//					event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
			//					link.dispatchEvent(event);
			//					console.log("Download link method with simulated click succeeded");
			//					success = true;

			//				} catch (ex) {
			//					console.log("Download link method with simulated click failed with the following exception:");
			//					console.log(ex);
			//				}
			//			}
			//		}
			//		//var file = new Blob([reply.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
			//		//var file = new File([reply.data], fileName, { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
			//		//var fileURL = window.URL.createObjectURL(file);
			//		////window.open(fileURL);
			//		//a.href = fileURL;
			//		//a.download = fileName;
			//		//a.click();
			//	}
			//}, function (error) {
			//	UIControlService.unloadLoading();
			//});

			//var innerContents = document.getElementById(formCertificate).innerHTML;
			//var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
			//popupWindow.document.open();
			//popupWindow.document.write('<html><head><title>Prequal Cerificate</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
			//popupWindow.document.close();
		}

		//ambil VendorID
		vm.loadVerifiedVendor = loadVerifiedVendor;
		function loadVerifiedVendor() {
			VerifiedSendService.selectVerifikasi(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data;
					vm.cekTemporary = vm.verified.IsTemporary;
					vm.VendorID = vm.verified.VendorID;
					vm.VendorName = vm.verified.VendorName;
					jLoad(1);
					loadUrlAgreement(1);
					loadVendorContact();
					loadDataVendorAgreement();
					//forKuesioner
					loadVendor();
					loadCek();
					loadKuesionerDD();
				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DATA_COMP" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.printForm = printForm;
		function printForm(form) {
			var innerContents = document.getElementById(form).innerHTML;
			var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
			popupWindow.document.open();
			popupWindow.document.write('<html><head><title>Kuesioner-' + vm.VendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
			popupWindow.document.close();

			//$uibModalInstance.close();
		}

		vm.loadDataVendorAgreement = loadDataVendorAgreement;
		function loadDataVendorAgreement() {
			SrtPernyataanService.selectData({
				VendorId: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status == 200) {
					vm.listData = reply.data;
					vm.IsBConductUploaded = false; vm.IsAgreementLetterUploaded = false;
					for (var i = 0; i <= vm.listData.length - 1; i++) {
						if (vm.listData[i].DocType == 4232 || vm.listData[i].DocType == 4225) {
							if (vm.listData[i].DocumentUrl != null) {
								if (vm.listData[i].DocumentUrl != "") {
									vm.IsBConductUploaded = true;
									vm.urlBConduct = vm.listData[i].DocumentUrl;
								}
							}
						}
						if (vm.listData[i].DocType == 4226) {
							if (vm.listData[i].DocumentUrl != null) {
								if (vm.listData[i].DocumentUrl != "") {
									vm.IsAgreementLetterUploaded = true;
									vm.urlAgreementLetter = vm.listData[i].DocumentUrl;
								}
							}
						}
					}
					console.info("url bconduct" + JSON.stringify(vm.listData));
				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DATA_DEPT" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadVendorContact = loadVendorContact;
		function loadVendorContact(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectContactID({
				VendorID: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.ContactID = data[0].ContactID;
					getAddressID(1);

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.getAddressID = getAddressID;
		function getAddressID(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectAddressID({
				ContactID: vm.ContactID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.AddressID = data[0].AddressID;
					loadAddress(1);

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadAddress = loadAddress;
		function loadAddress(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectAddress({
				AddressID: vm.AddressID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.Address = data[0].AddressDetail;
					vm.StateID = data[0].StateID;
					loadCountryID(1);

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadCountryID = loadCountryID;
		function loadCountryID(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectCountryID({
				StateID: vm.StateID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.CountryID = data[0].CountryID;
					vm.StateName = data[0].Name;
					loadCountry(1);

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadCountry = loadCountry;
		function loadCountry(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectCountry({
				CountryID: vm.CountryID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.Country = data[0].Name;
					if (vm.Country === 'Indonesia') {
						vm.cek = true;
					} else {
						vm.cek = false;
					}

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.All({
				VendorId: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.data = data;

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadUrlLibrary = loadUrlLibrary;
		function loadUrlLibrary(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectDocLibrary({
				DocType: vm.DocType
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.DocUrlBConduct = data[0].DocUrl;
				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadUrlLibraryAgree = loadUrlLibraryAgree;
		function loadUrlLibraryAgree(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectDocLibrary({
				DocType: 4226
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.DocUrlAgreement = data[0].DocUrl;

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadUrlAgreement = loadUrlAgreement;
		function loadUrlAgreement(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectUrlAgree({
				VendorId: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadUrlBConduct = loadUrlBConduct;
		function loadUrlBConduct(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectUrlBConduct({
				VendorId: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.DocUrlBConduct = data[0].AddressID;

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadUrlBConductEN = loadUrlBConductEN;
		function loadUrlBConductEN(current) {
			UIControlService.loadLoading("LOADERS.LOADING");
			SrtPernyataanService.selectBConductEN({
				VendorId: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.baca = baca;
		function baca() {
			var data = {
				DocType: vm.DocType
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modal.html',
				controller: 'SuratPernyataanModalCtrl',
				controllerAs: 'SrtPernyataanModalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
		};

		vm.baca2 = baca2;
		function baca2() {
			var data = {
				item: data
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modalBaca.html',
				controller: 'SuratPernyataanModalBacaCtrl',
				controllerAs: 'SrtPernyataanModalBacaCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
		};

		vm.upload = upload;
		function upload(data) {
			var data = {
				DocType: data,
				act: 1
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modalUpload.html',
				controller: 'SuratPernyataanModalUploadCtrl',
				controllerAs: 'SrtPernyataanModalUpCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		};

		vm.upload2 = upload2;
		function upload2(data) {
			var data = {
				DocType: data,
				act: 0
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modalUpload.html',
				controller: 'SuratPernyataanModalUploadCtrl',
				controllerAs: 'SrtPernyataanModalUpCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		};

		vm.editKuesioner = editKuesioner;
		function editKuesioner() {
			$state.go('kuesioner-vendor', { VendorID: vm.VendorID });
		}

		vm.ddkuesioner = ddkuesioner;
		function ddkuesioner() {
			$state.go('vendor-duedilligence-questionnaire', { VendorID: vm.VendorID });
		}

		vm.uploadKuesioner = uploadKuesioner;
		function uploadKuesioner() {
			var data = {
				VendorID: vm.VendorID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/formUploadQuestionnaire.html',
				controller: 'FormQuestionnaireCtrl',
				controllerAs: 'FormQuestionnaireCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		};

		vm.loadUrlKuesioner = loadUrlKuesioner;
		function loadUrlKuesioner() {
			SrtPernyataanService.selectUrlKuesioner({}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.UrlKuesioner = data.QuestionnaireUrl;

				} else {
					$.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}
	}
})();