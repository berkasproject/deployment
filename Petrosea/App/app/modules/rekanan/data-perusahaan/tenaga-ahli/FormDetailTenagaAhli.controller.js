﻿(function () {
	'use strict';

	angular.module("app")
            .controller("formDetailTenagaAhliCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'TenagaAhliService',
        'UIControlService', 'item', 'UploaderService', '$uibModalInstance', 'GlobalConstantService', 'UploadFileConfigService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, TenagaAhliService,
        UIControlService, item, UploaderService, $uibModalInstance, GlobalConstantService, UploadFileConfigService) {

		var vm = this;
		var type = item.type;
		vm.act = item.act;
		vm.item = item.item;
		vm.SysReference = { Value: "" };
		vm.Gender = 0;
		vm.tenagaahli = {};
		vm.fileUpload = "";
		vm.isCalendarOpened = [false, false, false, false];
		console.log(item);
		vm.addresses = {
			AddressInfo: ""
		};
		vm.countrys = {
			Name: ""
		};
		vm.statuss = {
			Name: ""
		};
		vm.vendor = {};
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.getDate = new Date();
		vm.ExpMaxDate = {
		    maxDate: Date.now()
		};
		vm.ExpMinDate = {
		    minDate: item.birthDate
		};

		vm.init = init;
		function init() {
			if (type == 1) {
				vm.type = "Job Experience";
			} else if (type === 2) {
				vm.type = "DIPLOMA";
			} else if (type === 3) {
				vm.type = "Sertifikat";
			}
			if (vm.act == true) vm.action = "Tambah";
			else vm.action = "Ubah";

			$translatePartialLoader.addPart('tenaga-ahli');
			console.info(vm.item);
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.EXPERTSCERTIFICATE", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			if (vm.act === false) {
				vm.tenagaahli.StartDate = vm.item.StartDate;
				vm.tenagaahli.EndDate = vm.item.EndDate;
				vm.DocUrl = vm.item.DocUrl;
				vm.Description = vm.item.Description;
				convertToDate();
			}
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.tenagaahli.StartDate) {
				vm.tenagaahli.StartDate = UIControlService.getStrDate(vm.tenagaahli.StartDate);
			}
			if (vm.tenagaahli.EndDate) {
				vm.tenagaahli.EndDate = UIControlService.getStrDate(vm.tenagaahli.EndDate);
			}
		};

		//Supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.tenagaahli.StartDate) {
				vm.tenagaahli.StartDate = new Date(Date.parse(vm.tenagaahli.StartDate));
			}
			if (vm.tenagaahli.EndDate) {
				vm.tenagaahli.EndDate = new Date(Date.parse(vm.tenagaahli.EndDate));
			}
		}

		function generateFilterStrings(allowedTypes) {
			console.info(allowedTypes);
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.uploadFile = uploadFile;
		function uploadFile() {
		    //if (vm.fileUpload == undefined && vm.act == true) {
		    //    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		    //    return;
		    //}
		    if (vm.tenagaahli.StartDate == null) {
		        UIControlService.msg_growl("Silahkan isi tanggal mulai!");
		    }
		    if (vm.tenagaahli.EndDate == null) {
		        UIControlService.msg_growl("Silahkan isi tanggal selesai!");
		    }
		    if (vm.act == true && (vm.fileUpload == undefined || vm.fileUpload == "")) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		    } else if (vm.act == false && (vm.fileUpload == undefined || vm.fileUpload == "")) {
		        addToSave();
		    }

		    if ((vm.fileUpload !== undefined || vm.fileUpload != "") && vm.act === false) {
			    if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
			        upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			    } else {
			        vm.fileUpload = "";
			    }
			}
			if (vm.act === true) {
				if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
				} else {
				    vm.fileUpload = "";
				}
			}
		}

		function validateFileType(file, allowedFileTypes) {
			//console.info(JSON.stringify(allowedFileTypes));
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {

			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}


			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                	UIControlService.unloadLoading();
                	console.info("response:" + JSON.stringify(response));
                	if (response.status == 200) {
                		var url = response.data.Url;
                		vm.DocUrl = url;
                		vm.name = response.data.FileName;
                		var s = response.data.FileLength;
                		if (vm.flag == 0) {

                			vm.size = Math.floor(s)
                		}

                		if (vm.flag == 1) {
                			vm.size = Math.floor(s / (1024));
                		}
                		addToSave();
                	} else {
                		UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	UIControlService.msg_growl("error", "MESSAGE.API")
                	UIControlService.unloadLoading();
                });



		}

		vm.addToSave = addToSave;
		vm.vendor = {};
		function addToSave() {
            
			if (type == 1) {
				vm.SysReference.Value = "JOB_EXPERIENCE";
			}
			else if (type === 2) {
				vm.SysReference.Value = "DIPLOMA";
			}
			else if (type === 3) {
				vm.SysReference.Value = "CERTIFICATE";
			}
			if (vm.act === true) {
				vm.vendor = {
				    VendorExpertsID: vm.item.VendorExpertsID,
					StartDate: UIControlService.getStrDate(vm.tenagaahli.StartDate),
					EndDate: UIControlService.getStrDate(vm.tenagaahli.EndDate),
					Description: vm.Description,
					DocUrl: vm.DocUrl,
					SysReference: vm.SysReference
				};
			}
			if (vm.act === false) {
				vm.vendor = {
					ID: vm.item.ID,
					VendorExpertsID: vm.item.VendorExpertsID,
				    StartDate: UIControlService.getStrDate(vm.tenagaahli.StartDate),
					EndDate: UIControlService.getStrDate(vm.tenagaahli.EndDate),
					Description: vm.Description,
					DocUrl: vm.DocUrl,
					SysReference: vm.SysReference
				};
			}
			if (vm.act === true) {
				TenagaAhliService.insertExpertCertificate(vm.vendor,
                    function (reply) {
                    	console.info("reply" + JSON.stringify(reply))
                    	UIControlService.unloadLoadingModal();
                    	if (reply.status === 200) {
                    		if (type === 1) {
                    			UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD_WORK");

                    		} else if (type === 2) {
                    			UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD_EDUCATION");

                    		} else if (type === 3) {
                    			UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD_CERTIFICATE");

                    		}
                    		$uibModalInstance.close();

                    	} else {
                    		UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
                    		return;
                    	}
                    },
                    function (err) {
                    	UIControlService.msg_growl("error", "MESSAGE.API");
                    	UIControlService.unloadLoadingModal();
                    }
                );
			}
			if (vm.act === false) {
				TenagaAhliService.updateExpertCertificate(vm.vendor,
				   function (reply) {
				   	UIControlService.unloadLoadingModal();
				   	if (reply.status === 200) {
				   		if (type === 1) {
				   			UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE_WORK");

				   		} else if (type === 2) {
				   			UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE_EDUCATION");

				   		} else if (type === 3) {
				   			UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE_CERTIFICATE");

				   		}
				   		$uibModalInstance.close();

				   	} else {
				   		UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
				   		return;
				   	}
				   },
				   function (err) {
				   	UIControlService.msg_growl("error", "MESSAGE.API");
				   	UIControlService.unloadLoadingModal();
				   }
			   );
			}
		}

		vm.update = update;
		vm.vendor = {};
		function update() {
			convertAllDateToString();
			UIControlService.loadLoading("MESSAGE.LOADING");
			if (vm.act === false) {
				vm.addresses.AddressInfo = vm.address;
				vm.countrys.Name = vm.Nationality;
				vm.statuss.Name = vm.Status;
				vm.vendor = {
					ID: vm.item.ID,
					Name: vm.Name,
					DateOfBirth: vm.tenagaahli.BirthDate,
					Gender: vm.Gender,
					address: vm.addresses,
					Education: vm.Education,
					country: vm.countrys,
					Position: vm.Position,
					YearOfExperience: vm.YearOfExperience,
					Email: vm.Email,
					Statusperson: vm.statuss,
					Expertise: vm.Expertise
				};
				TenagaAhliService.update(vm.vendor, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE_EXPERTS");
						jLoad(1);
					} else {
						UIControlService.msg_growl("error", "MESSAGE.FAILED_NON_AKTIF");
						return;
					}
				}, function (err) {

					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			}
			else if (vm.act === true) {
				vm.addresses.AddressInfo = vm.address;
				vm.countrys.Name = vm.Nationality;
				vm.statuss.Name = vm.Status;
				vm.vendor = {
					Name: vm.Name,
					DateOfBirth: vm.tenagaahli.BirthDate,
					Gender: vm.Gender,
					address: vm.addresses,
					Education: vm.Education,
					country: vm.countrys,
					Position: vm.Position,
					Email: vm.Email,
					Statusperson: vm.statuss,
					Expertise: vm.Expertise
				};
				TenagaAhliService.insert(vm.vendor, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE_EXPERTS");
						jLoad(1);
					} else {
						UIControlService.msg_growl("error", "MESSAGE.FAILED_NON_AKTIF");
						return;
					}
				}, function (err) {

					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			}


		}

		vm.addToList = addToList;
		function addToList() {
			vm.addresses.AddressInfo = vm.address;
			vm.countrys.Name = vm.Nationality;
			vm.statuss.Name = vm.Status;
			vm.vendor = {
				Name: vm.Name,
				DateOfBirth: vm.tenagaahli.BirthDate,
				Gender: vm.Gender,
				address: vm.addresses,
				Education: vm.Education,
				country: vm.countrys,
				Position: vm.Position,
				YearOfExperience: vm.YearOfExperience,
				Email: vm.Email,
				Statusperson: vm.statuss,
				Expertise: vm.Expertise
			};

			console.info(JSON.stringify(vm.vendor));
		}

		vm.add = add;
		function add(data) {
			console.info(JSON.stringify(data));
		}

		vm.cekStartDate = cekStartDate;
		function cekStartDate(date) {
		    if (UIControlService.getStrDate(vm.getDate) < UIControlService.getStrDate(date)) {
		        UIControlService.msg_growl('error', 'MESSAGE.ERR_STARTDATE_BF_NOW');
		        vm.tenagaahli.StartDate = undefined;
		        return;
		    }
		    else {
		        if (vm.tenagaahli.EndDate) {
		            if (UIControlService.getStrDate(date) > UIControlService.getStrDate(vm.tenagaahli.EndDate)) {
		                UIControlService.msg_growl('error', 'MESSAGE.ERR_STARTDATE_AF_NEXT_STARTDATE');
		                vm.tenagaahli.StartDate = undefined;
		                return;
		            }
		        }
		    }
		}

		vm.cekEndDate = cekEndDate;
		function cekEndDate(date) {
		    if (type == 1 || type == 2) {
                // gak dipake
		        //if (UIControlService.getStrDate(vm.getDate) < UIControlService.getStrDate(date)) {
		        //    console.log(vm.getDate);
		        //    console.log(date);
		        //    console.log('masuk sini');
		        //    UIControlService.msg_growl('error', 'MESSAGE.ERR_ENDDATE_BF_NOW');
		        //    vm.tenagaahli.EndDate = undefined;
		        //    return;
		        //}
		    }
		    if (vm.tenagaahli.EndDate) {
		        if (UIControlService.getStrDate(date) < UIControlService.getStrDate(vm.tenagaahli.StartDate)) {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_ENDDATE_BF_PREV_ENDDATE');
		            vm.tenagaahli.EndDate = undefined;
		            return;
		        }
		    }
		}
	}
})();