﻿(function () {
	'use strict';

	angular.module("app").controller("DetailTenagaAhliCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'TenagaAhliService',
        'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, TenagaAhliService,
        UIControlService, item, $uibModalInstance, GlobalConstantService) {
	    var vm = this;
	    vm.fullSize = 10;
	    var offset = 0;
	    vm.item = item.item;
		console.log(vm.item);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('tenaga-ahli');
		    loadCertificate();
			console.info(JSON.stringify(item.item));
		}

		vm.loadCertificate = loadCertificate;
		function loadCertificate() {
		    vm.listJob = [];
		    vm.listEducation = [];
		    vm.listCertificate = [];
		    //console.info("curr "+current)
		    TenagaAhliService.selectCertificate({
		        Offset: offset,
		        Limit: vm.fullSize,
		        Status: vm.item.ID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.vendorexpertsCertificate = reply.data;
		            for (var i = 0; vm.vendorexpertsCertificate.length; i++) {
		                vm.vendorexpertsCertificate[i].StartDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].StartDate));
		                vm.vendorexpertsCertificate[i].EndDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].EndDate));
		                if (vm.vendorexpertsCertificate[i].SysReference.Name == "JOB_EXPERIENCE" && vm.vendorexpertsCertificate[i].IsActive == true)
		                    vm.listJob.push(vm.vendorexpertsCertificate[i]);
		                else if (vm.vendorexpertsCertificate[i].SysReference.Name == "DIPLOMA" && vm.vendorexpertsCertificate[i].IsActive == true)
		                    vm.listEducation.push(vm.vendorexpertsCertificate[i]);
		                else if (vm.vendorexpertsCertificate[i].SysReference.Name == "CERTIFICATE" && vm.vendorexpertsCertificate[i].IsActive == true)
		                    vm.listCertificate.push(vm.vendorexpertsCertificate[i]);

		            }
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};


	}
})();