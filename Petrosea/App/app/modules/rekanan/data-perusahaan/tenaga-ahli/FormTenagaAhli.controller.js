﻿(function () {
	'use strict';

	angular.module("app").controller("formTenagaAhliCtrl", ctrl);

	ctrl.$inject = ['TenagaAhliService', '$uibModal', 'UIControlService', 'item', '$location', '$uibModalInstance', '$translatePartialLoader', 'GlobalConstantService'];

	function ctrl(TenagaAhliService, $uibModal, UIControlService, item, $location, $uibModalInstance, $translatePartialLoader, GlobalConstantService) {
		var vm = this;
		vm.act = item.act;
		vm.item = item.item;
		vm.fullSize = 10;
		var offset = 0;
		vm.Gender = "";
		vm.tenagaahli = { BirthDate: "" };
		vm.tenagaahli2 = {};
		vm.isCalendarOpened = [false, false, false, false];
		vm.addresses = { AddressInfo: "" };
		vm.countrys = { Name: "" };
		vm.statuss = { Name: "" };
		vm.radio = {
			tipeM: "M",
			tipeF: "F",
			StatusK: "CONTRACT",
			StatusI: "INTERNSHIP",
			StatusP: "PERMANENT",
		}
		vm.nationalities = ["Indonesia"];
		vm.isApprovedCR = item.isApprovedCR;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
	    //tambahan fungsi data pengalaman kerja, pendidikan, sertifikat keahlian
		vm.disabledTab = disabledTab;
		function disabledTab(index) {
		    if (vm.fillDetail == false) {
		        var active = $('#uibTab' + index).attr('class');

		        var activeArray = active.split(' ');

		        var act = $.grep(activeArray, function (n) { return n == 'active-add'; });
		        var act2 = $.grep(activeArray, function (n) { return n == 'active'; });
		    }
		}




		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('tenaga-ahli');
			var tomorrow = new Date();
			console.info("Act:" + vm.act);
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date(tomorrow);
			afterTomorrow.setDate(tomorrow.getDate() + 30);
			console.info(afterTomorrow);
			UIControlService.loadLoadingModal("MESSAGE.LOADING");

			TenagaAhliService.GetAllNationalities(function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.nationalities = reply.data;
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_LIST_COUNTRY");
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_LIST_COUNTRY");
				UIControlService.unloadLoadingModal();
			});

			if (vm.act === false) {
			    vm.VendorExpertsID = item.item.ID;
			    vm.Name = vm.item.Name;
			    vm.namaVendor = vm.item.Name;
				vm.tenagaahli.BirthDate = vm.item.DateOfBirth;
				if (vm.item.Gender === "M") {
					vm.Gender = "M";
				} else if (vm.item.Gender === "F") {
					vm.Gender = "F";
				}
				vm.address = vm.item.address.AddressInfo;
				vm.Education = vm.item.Education;
				vm.Nationality = vm.item.country.Name;
				vm.Position = vm.item.Position;
				vm.YearOfExperience = vm.item.YearOfExperience;
				vm.Email = vm.item.Email;

				if (vm.item.Statusperson.Name === "CONTRACT") {
					vm.Status = "CONTRACT";
				} else if (vm.item.Statusperson.Name === "INTERNSHIP") {
					vm.Status = "INTERNSHIP";
				} else if (vm.item.Statusperson.Name === "PERMANENT") {
					vm.Status = "PERMANENT";
				}

				vm.Expertise = vm.item.Expertise;
				loadCertificate();
			} else {
			    vm.formData = false;
			    vm.fillDetail = true;
			}

			convertToDate();
		}

		vm.hapusdetail = hapusdetail;
		function hapusdetail(data, act) {
		    TenagaAhliService.editCertificateActive({
		        ID: data.ID,
		        IsActive: act
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            if (act === false)
		                UIControlService.msg_growl("success", "MESSAGE.SUCCESS_DELETE");

		            if (act === true)
		                UIControlService.msg_growl("success", "MESSAGE.SUCCESS_AKTIF");
		            loadCertificate(vm.dataExpert);
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.FAILED_NON_AKTIF");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		vm.DetailCertificate = DetailCertificate;
		function DetailCertificate(data1, act, type) {
		    console.info("masuk form add/edit");
		    console.log(data1);
		    if (data1 == 0) {
		        vm.cc = {
		            VendorExpertsID: vm.VendorExpertsID
		        }
		    } else {
		        vm.cc = data1
		    }

		    var data = {
		        type: type,
		        act: act,
		        item: vm.cc,
		        birthDate: vm.tenagaahli.BirthDate
		    }

		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/rekanan/data-perusahaan/tenaga-ahli/FormDetailTenagaAhli.html',
		        controller: 'formDetailTenagaAhliCtrl',
		        controllerAs: 'formDetailTenagaAhliCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        loadCertificate();
		    });
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.tenagaahli.BirthDate) {
				vm.tenagaahli.BirthDate = UIControlService.getStrDate(vm.tenagaahli.BirthDate);
			}
		};

		//Supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.tenagaahli.BirthDate) {
				vm.tenagaahli.BirthDate = new Date(Date.parse(vm.tenagaahli.BirthDate));
			}
		}

		vm.calculateAge = calculateAge;
		function calculateAge(birthday) {
		    if (birthday !== '') {
		        var ageDifMs = Date.now() - birthday.getTime();
		        var ageDate = new Date(ageDifMs); // miliseconds from epoch
		        var age = Math.abs(ageDate.getUTCFullYear() - 1970);
		        if (age < 17) {
		            vm.tenagaahli.BirthDate = "";
		            UIControlService.msg_growl("error", "FORM.AGE");
		            return;
		        }
		    }
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		};

		vm.update = update;
		vm.vendor = {};
		function update() {
		    if (vm.tenagaahli.BirthDate == "") {
		        UIControlService.msg_growl("error", "FORM.AGE_NULL");
		        return;
		    }
			convertAllDateToString();
            
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			if (vm.act === false) {
				vm.addresses.AddressInfo = vm.address;
				vm.countrys.Name = vm.Nationality;
				vm.statuss.Value = vm.Status;
				vm.vendor = {
					ID: vm.item.ID,
					Name: vm.Name,
					DateOfBirth: vm.tenagaahli.BirthDate,
					Gender: vm.Gender,
					address: vm.addresses,
					Education: vm.Education,
					country: vm.countrys,
					Position: vm.Position,
					YearOfExperience: vm.YearOfExperience,
					Email: '',
					Statusperson: vm.statuss,
					Expertise: vm.Expertise
				};
				TenagaAhliService.update(vm.vendor, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
					UIControlService.unloadLoadingModal();
				});
			} else if (vm.act === true) {
			}
		}

		vm.addToList = addToList;
		function addToList() {
			vm.addresses.AddressInfo = vm.address;
			vm.countrys.Name = vm.Nationality;
			vm.statuss.Name = vm.Status;
			vm.vendor = {
				Name: vm.Name,
				DateOfBirth: vm.tenagaahli.BirthDate,
				Gender: vm.Gender,
				address: vm.addresses,
				Education: vm.Education,
				country: vm.countrys,
				Position: vm.Position,
				YearOfExperience: vm.YearOfExperience,
				Email: '',
				Statusperson: vm.statuss,
				Expertise: vm.Expertise
			};
		}

		vm.add = add;
		function add(data) {
			console.info(JSON.stringify(data));
		}

		vm.loadCertificate = loadCertificate;
		function loadCertificate() {
		    vm.listJob = [];
		    vm.listEducation = [];
		    vm.listCertificate = [];
		    //console.info("curr "+current)
		    TenagaAhliService.selectCertificate({
		        Offset: offset,
		        Limit: vm.fullSize,
		        Status: vm.VendorExpertsID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.vendorexpertsCertificate = reply.data;
		            for (var i = 0; vm.vendorexpertsCertificate.length; i++) {
		                vm.vendorexpertsCertificate[i].StartDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].StartDate));
		                vm.vendorexpertsCertificate[i].EndDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].EndDate));
		                if (vm.vendorexpertsCertificate[i].SysReference.Name == "JOB_EXPERIENCE" && vm.vendorexpertsCertificate[i].IsActive == true)
		                    vm.listJob.push(vm.vendorexpertsCertificate[i]);
		                else if (vm.vendorexpertsCertificate[i].SysReference.Name == "DIPLOMA" && vm.vendorexpertsCertificate[i].IsActive == true)
		                    vm.listEducation.push(vm.vendorexpertsCertificate[i]);
		                else if (vm.vendorexpertsCertificate[i].SysReference.Name == "CERTIFICATE" && vm.vendorexpertsCertificate[i].IsActive == true)
		                    vm.listCertificate.push(vm.vendorexpertsCertificate[i]);

		            }
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}
		vm.klik = 0;
		vm.nextPage = nextPage;
		function nextPage() {
		    if (vm.klik > 0) {
		        return;
		    }
		    vm.klik++;

		    if (vm.address == undefined || vm.address == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Alamat!");
		        return;
		    }
		    if (vm.Nationality == undefined || vm.Nationality == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Kewarganegaraan!");
		        return;
		    }
		    if (vm.Status == undefined || vm.Status == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Status Kepegawaian!");
		        return;
		    }
		    if (vm.Name == undefined || vm.Name == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Nama !");
		        return;
		    }
		    if (vm.tenagaahli.BirthDate == undefined || vm.tenagaahli.BirthDate == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Tanggal Lahir!");
		        return;
		    }
		    if (vm.Gender == undefined || vm.Gender == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Jenis Kelamin!");
		        return;
		    }
		    if (vm.addresses == undefined || vm.addresses == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Alamat!");
		        return;
		    }
		    if (vm.Education == undefined || vm.Education == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Pendidikan Terakhir!");
		        return;
		    }
		    if (vm.Position == undefined || vm.Position == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Jabatan !");
		        return;
		    }
		    if (vm.Expertise == undefined || vm.Expertise == null) {
		        UIControlService.msg_growl("error", "Lengkapi Data Profesi/Keahlian!");
		        return;
		    }

		    if (vm.act == false) {
		        vm.addresses.AddressInfo = vm.address;
		        vm.countrys.Name = vm.Nationality;
		        vm.statuss.Value = vm.Status;
		        vm.vendor = {
		            ID: vm.item.ID,
		            Name: vm.Name,
		            DateOfBirth: vm.tenagaahli.BirthDate,
		            Gender: vm.Gender,
		            address: vm.addresses,
		            Education: vm.Education,
		            country: vm.countrys,
		            Position: vm.Position,
		            YearOfExperience: vm.YearOfExperience,
		            Email: '',
		            Statusperson: vm.statuss,
		            Expertise: vm.Expertise
		        };
		        TenagaAhliService.update(vm.vendor, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                vm.vendorExpertID = reply.data;
		                UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE");
		                vm.indextab = 1;
		                //var data = {
		                //    item: vm.vendor,
                        //    act:false
		                //}
		                //var modalInstance = $uibModal.open({
		                //    templateUrl: 'app/modules/rekanan/data-perusahaan/tenaga-ahli/FormDetailCVTenagaAhli.html',
		                //    controller: 'TenagaAhliCVController',
		                //    controllerAs: 'TenagaAhliCVController',
		                //    resolve: {
		                //        item: function () {
		                //            return data;
		                //        }
		                //    }
		                //});
		                //modalInstance.result.then(function () {
		                //    initialize();
		                //    //viewdetail(vm.dataExpert);
		                //    window.location.reload();
		                //});
		                //$uibModalInstance.close();
		            } else {
		                UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
		            UIControlService.unloadLoadingModal();
		        });
		    } else {
		        vm.addresses.AddressInfo = vm.address;
		        vm.countrys.Name = vm.Nationality;
		        vm.statuss.Value = vm.Status;
		        vm.vendor = {
		            Name: vm.Name,
		            DateOfBirth: vm.tenagaahli.BirthDate,
		            Gender: vm.Gender,
		            address: vm.addresses,
		            Education: vm.Education,
		            country: vm.countrys,
		            Position: vm.Position,
		            Email: '',
		            Statusperson: vm.statuss,
		            Expertise: vm.Expertise
		        };

		        TenagaAhliService.insert(vm.vendor, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200 && reply.data != 0) {
		                vm.vendorExpertID = reply.data;
		                vm.vendor.vendorExpID = vm.vendorExpertID;
		                UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");

		                vm.fillDetail = false;
		                vm.formData = true;
		                vm.indextab = 1;
		                vm.namaVendor = vm.vendor.Name;
		                vm.VendorExpertsID = vm.vendor.vendorExpID;
		                loadCertificate();
		                //var data = {
		                //    item: vm.vendor,
                        //    act: true
		                //}
		                //var modalInstance = $uibModal.open({
		                //    templateUrl: 'app/modules/rekanan/data-perusahaan/tenaga-ahli/FormDetailCVTenagaAhli.html',
		                //    controller: 'TenagaAhliCVController',
		                //    controllerAs: 'TenagaAhliCVController',
		                //    resolve: {
		                //        item: function () {
		                //            return data;
		                //        }
		                //    }
		                //});
		                //modalInstance.result.then(function () {
		                //    initialize();
		                //    //viewdetail(vm.dataExpert);
		                //    window.location.reload();
		                //});
		            } else if (reply.status === 200 && reply.data == 0) {
		                UIControlService.msg_growl("error", "MESSAGE.DUPLICATE_EXPERT");
		                return;
		            }else {
		                UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
		            UIControlService.unloadLoadingModal();
		        });
		    }
		}
	}
})();