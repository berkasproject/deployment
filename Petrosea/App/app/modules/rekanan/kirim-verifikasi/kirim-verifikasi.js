﻿(function () {
	'use strict';

	angular.module("app").controller("VerifiedSendCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', '$filter', 'SocketService', 'VerifiedSendService', 'UIControlService', 'VendorRegistrationService'];
	function ctrl($translatePartialLoader, $filter, SocketService, VerifiedSendService, UIControlService, VendorRegistrationService) {
		var vm = this;
		vm.verified = [];
		vm.init = init;


		function init() {
			$translatePartialLoader.addPart('kirim-verifikasi');
			jLoad(1);
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			VerifiedSendService.selectVerifikasi(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data;
					vm.vendorID = vm.verified.VendorID;
					vm.VendorName = reply.data.VendorName
					role(1);
				} else {
					$.growl.error({ message: "FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
		vm.data = [];
		vm.role = role;
		function role(current) {
			VerifiedSendService.role(function (reply) {
				if (reply.status === 200) {
					var data = reply.data[0].Children;
					vm.data = data;
					check();
					console.info(vm.data);
				} else {
					$.growl.error({ message: "FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadContact = loadContact;
		function loadContact() {
			VerifiedSendService.selectcontact({ VendorID: vm.vendorID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.contact = reply.data;
					vm.listEmail = [];
					for (var i = 0; i < vm.contact.length; i++) {
						if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
							vm.listEmail.push(vm.contact[i].Contact.Email);
						}
					}
					//console.info("list email" + JSON.stringify(vm.listEmail));
					sendMail(vm.listEmail);
					//console.info(JSON.stringify(reply.data));
				} else {
					$.growl.error({ message: "FAIL_GET_DATA_COMP" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.sendMail = sendMail;
		function sendMail(listEmail) {
			var email = {
				subject: 'Verifikasi Akun',
				mailContent: 'Permintaan verifikasi akun Anda telah terkirim. Mohon tunggu sampai akun Anda diverifikasi oleh pihak administrator.<br>Terima kasih.',
				isHtml: true,
				addresses: listEmail
			};

			// UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
			VerifiedSendService.sendMail(email, function (response) {
				// UIControlService.unloadLoading();
				if (response.status == 200) {
					UIControlService.msg_growl("notice", "SENT_EMAIL")
				}
			}, function (response) {
				UIControlService.unloadLoading();
			});
		}

		vm.isChecked;
		vm.check = check;
		vm.flag = 1;
		function check() {
			if (vm.verified.VendorTypeID !== null) {
				console.info(vm.verified);
				if (vm.verified.VendorType.Name === "VENDOR_TYPE_GOODS") {
					for (var i = 0; i < vm.data.length - 1; i++) {
						if (vm.data[i].Label !== "MENUS.COMPANYDATA.SENDVERIFICATION" && vm.data[i].Label !== "MENUS.COMPANYDATA.OTHERDOCUMENT" && vm.data[i].Label !== "MENUS.COMPANYDATA.EXPERTS" && vm.data[i].Label !== "MENUS.COMPANYDATA.EXPERIENCE") {
							if (vm.data[i].IsChecked === null) {
								vm.flag = 0;
								break;
							}
						}
					}
				} else {
					for (var i = 0; i < vm.data.length - 1; i++) {
						if (vm.data[i].Label !== "MENUS.COMPANYDATA.SENDVERIFICATION" && vm.data[i].Label !== "MENUS.COMPANYDATA.OTHERDOCUMENT" && vm.data[i].Label !== "MENUS.COMPANYDATA.EXPERIENCE") {
							if (vm.data[i].IsChecked === null) {
								console.info(vm.data[i]);
								vm.flag = 0;
								break;
							}
						}
					}
				}
			}
			else vm.flag = 0;
		}


		vm.add = add;
		function add() {
			swal({
				icon: "success",
				text: $filter('translate')('VERIFCONFIRM'),
				buttons: {
					cancel: {
						text: "Batal",
						value: null,
						visible: true,
						className: "",
						closeModal: true,
						className: "red-bg",
					},
					confirm: {
						text: "Kirim Data Verifikasi",
						value: true,
						visible: true,
						className: "",
						closeModal: true
					},
				},
				closeOnClickOutside: false
			}).then((value) => {
				if (value) {
					UIControlService.loadLoading("LOADING");
					//console.info("ada:"+JSON.stringify(data))
					VerifiedSendService.update(function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							if (reply.data == true) {
								UIControlService.msg_growl("success", "SUC_SEND");
								loadContact();
								VendorRegistrationService.sendEmailPegawai({ Name: vm.VendorName, IsActived: true }, function (response1) {
									if (response1.status == 200) {
										UIControlService.unloadLoading();
										UIControlService.msg_growl("notice", "LOADERS.SUCC_EMAIL_PEGAWAI");
									} else {
										UIControlService.msg_growl('error', 'LOADERS.FAIL_EMAIL_PEGAWAI', 'LOADERS.FAIL_EMAIL_PEGAWAI_TITLE');
										UIControlService.unloadLoading();
									}
								}, function (response1) {
									UIControlService.handleRequestError(response1.data);
									UIControlService.unloadLoading();
								});
								SocketService.emit("daftarRekanan");
								jLoad(1);
							} else {
								UIControlService.msg_growl("error", 'INCOMPLETE');
								return;
							}
						} else {
							UIControlService.msg_growl("error", "ERR_VER");
							return;
						}
					}, function (err) {
						UIControlService.msg_growl("error", "API");
						UIControlService.unloadLoading();
					});
				}
			});
		    //bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('VERIFCONFIRM') + '<h3>', function (reply) {
		    //    if (reply) {
		    //        UIControlService.loadLoading("LOADING");
		    //        //console.info("ada:"+JSON.stringify(data))
		    //        VerifiedSendService.update(function (reply) {
		    //            UIControlService.unloadLoading();
		    //            if (reply.status === 200) {
		    //                if (reply.data == true) {
		    //                    UIControlService.msg_growl("success", "SUC_SEND");
		    //                    loadContact();
		    //                    VendorRegistrationService.sendEmailPegawai({ Name: vm.VendorName, IsActived: true }, function (response1) {
		    //                        if (response1.status == 200) {
		    //                            UIControlService.unloadLoading();
		    //                            UIControlService.msg_growl("notice", "LOADERS.SUCC_EMAIL_PEGAWAI");
		    //                        } else {
		    //                            UIControlService.msg_growl('error', 'LOADERS.FAIL_EMAIL_PEGAWAI', 'LOADERS.FAIL_EMAIL_PEGAWAI_TITLE');
		    //                            UIControlService.unloadLoading();
		    //                        }
		    //                    }, function (response1) {
		    //                        UIControlService.handleRequestError(response1.data);
		    //                        UIControlService.unloadLoading();
		    //                    });
		    //                    SocketService.emit("daftarRekanan");
		    //                    jLoad(1);
		    //                } else {
		    //                    UIControlService.msg_growl("error", 'INCOMPLETE');
		    //                    return;
		    //                }
		    //            } else {
		    //                UIControlService.msg_growl("error", "ERR_VER");
		    //                return;
		    //            }
		    //        }, function (err) {
		    //            UIControlService.msg_growl("error", "API");
		    //            UIControlService.unloadLoading();
		    //        });
		    //    }
		    //});
		}
	}
})();
