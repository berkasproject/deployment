﻿(function () {
	'use strict';

	angular.module("app").factory("PPVHSService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var Vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		var Publicendpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			select: select,
			Insert: Insert,
			selectDetail: selectDetail,
			InsertOpen: InsertOpen,
			selectTemplate: selectTemplate,
			selectStep: selectStep,
			selectRFQ: selectRFQ,
			getDeliveryTerms: getDeliveryTerms,
			getIncoTerms: getIncoTerms,
			selectRFQId: selectRFQId,
			selectFreight: selectFreight,
			changeState: changeState,
			selectTransport: selectTransport,
			selectVendor: selectVendor,
			selectCompanyPerson: selectCompanyPerson,
			InsertDetail: InsertDetail,
			selectVendorExperience: selectVendorExperience,
			SelectVendorCommodity: SelectVendorCommodity,
			selectEquipment: selectEquipment,
			selectVehicle: selectVehicle,
			InsertAll: InsertAll,
			cekCurr: cekCurr,
			selectBuilding: selectBuilding,
			DeliveryTerm: DeliveryTerm,
			InsertBuildingTender: InsertBuildingTender,
			SelectBuildingTender: SelectBuildingTender,
			selectVehicleTender: selectVehicleTender,
			GetSteps: GetSteps,
			getOptionsTender: getOptionsTender,
			deleteData: deleteData,
			InsertNonBuildingTender: InsertNonBuildingTender,
			InsertExperienceTender: InsertExperienceTender,
			cekLimit: cekLimit,
			InsertSubmit: InsertSubmit,
			UpdateBuildingTender: UpdateBuildingTender,
			editActiveBulding: editActiveBulding,
			UpdateNonBuildingTender: UpdateNonBuildingTender,
			editActiveNonBulding: editActiveNonBulding,
			editActiveExp: editActiveExp,
			updateExp: updateExp,
			getDeliveryTender: getDeliveryTender,
			getPaymentTerm: getPaymentTerm,
			loadRFQ: loadRFQ,
			deleteItem: deleteItem
		};

		return service;

		function loadRFQ(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + '/vhsOfferEntry/GetRFQId', param).then(successCallback, errorCallback);
		}
		function getPaymentTerm(successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + '/GoodsOfferEntry/getPymentTerm').then(successCallback, errorCallback);
		}
		function GetSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/VOE/tahapan/getsteps", param).then(successCallback, errorCallback);
		}
		function updateExp(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vendorExperienceTender/update", param).then(successCallback, errorCallback);
		}
		function editActiveExp(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vendorExperienceTender/editActive", param).then(successCallback, errorCallback);
		}
		function editActiveNonBulding(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/equipmentTender/editActive", param).then(successCallback, errorCallback);
		}
		function UpdateNonBuildingTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/equipmentTender/update", param).then(successCallback, errorCallback);
		}
		function editActiveBulding(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/buildingTender/editActive", param).then(successCallback, errorCallback);
		}
		function UpdateBuildingTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/buildingTender/update", param).then(successCallback, errorCallback);
		}
		function InsertSubmit(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + '/vhsOfferEntry/InsertIsSubmit', param).then(successCallback, errorCallback);
		}
		function cekLimit(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/cekLimit", param).then(successCallback, errorCallback);
		}
		function InsertExperienceTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vendorExperienceTender/insert", param).then(successCallback, errorCallback);
		}
		function InsertNonBuildingTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/vehicleTender/Insert", param).then(successCallback, errorCallback);
		}

		function deleteItem(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/deleteItem", param).then(successCallback, errorCallback);
		}

		function deleteData(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/DeleteData", param).then(successCallback, errorCallback);
		}

		function getOptionsTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_OPTIONS_RFQ" };
			GlobalConstantService.post(Publicendpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function selectVehicleTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/BuildingTender/select", param).then(successCallback, errorCallback);
		}
		function SelectBuildingTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/BuildingTender/select", param).then(successCallback, errorCallback);
		}
		function InsertBuildingTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/buildingTender/insert", param).then(successCallback, errorCallback);
		}
		function DeliveryTerm(param, successCallback, errorCallback) {
			var param = { Keyword: "TENDER_TYPE_RFQ" };
			GlobalConstantService.post(Publicendpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getDeliveryTender(successCallback, errorCallback) {
			var param = { Keyword: "RFQ_DELIVERY_TERMS" };
			GlobalConstantService.post(Publicendpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function selectBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/Building/select", param).then(successCallback, errorCallback);
		}
		function cekCurr(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/cekCurr", param).then(successCallback, errorCallback);
		}
		function InsertAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/InsertAll", param).then(successCallback, errorCallback);
		}
		function selectVehicle(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/vehicleTender/select", param).then(successCallback, errorCallback);
		}

		function selectEquipment(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/EquipmentTender/select", param).then(successCallback, errorCallback);
		}
		function SelectVendorCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(Publicendpoint + "/vendor/commodityall", param).then(successCallback, errorCallback);
		}
		function selectVendorExperience(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vendorExperienceTender/select", param).then(successCallback, errorCallback);
		}
		function selectCompanyPerson(successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/GetCompPersVendor").then(successCallback, errorCallback);
		}
		function selectVendor(successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/GetVendor").then(successCallback, errorCallback);
		}
		function selectTransport(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/GetFreighCostTransport", param).then(successCallback, errorCallback);
		}
		function changeState(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/ChangeState", param).then(successCallback, errorCallback);
		}
		function selectFreight(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/cekIncoFreight", param).then(successCallback, errorCallback);
		}
		function selectRFQId(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/GetRFQId", param).then(successCallback, errorCallback);
		}
		function getIncoTerms(param, successCallback, errorCallback) {
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/cekInco", param).then(successCallback, errorCallback);
		}
		function getDeliveryTerms(successCallback, errorCallback) {
			var param = { Keyword: "RFQ_DELIVERY_TERMS" };
			GlobalConstantService.post(Publicendpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function selectRFQ(param, successCallback, errorCallback) {
			console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/GetRFQ", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry", param).then(successCallback, errorCallback);
		}
		function InsertDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/InsertDetail", param).then(successCallback, errorCallback);
		}
		function Insert(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/Insert", param).then(successCallback, errorCallback);
		}
		function InsertOpen(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/Open", param).then(successCallback, errorCallback);
		}
		function selectDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/selectDetail", param).then(successCallback, errorCallback);
		}
		function selectTemplate(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/GetVHS", param).then(successCallback, errorCallback);
		}
		function selectStep(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/vhsOfferEntry/Step", param).then(successCallback, errorCallback);
		}

	}
})();