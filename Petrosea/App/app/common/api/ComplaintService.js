﻿(function () {
    'use strict';

    angular.module("app").factory("ComplaintService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            SelectVendorName: SelectVendorName,
            SelectWarningType: SelectWarningType,
            insert: insert,
            update: update,
            editActive: editActive,
            SelectTemplate: SelectTemplate,
            SelectEmployeeName: SelectEmployeeName,
            UpdateTemplate: UpdateTemplate,
            getCurrentUser: getCurrentUser,
            checkExistingRecord: checkExistingRecord,
            SelectVendor: SelectVendor
        };

        return service;


        function select(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/complaint/select", param).then(successCallback, errorCallback);
        }

        function SelectVendorName(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/warningLetter/listVendorName").then(successCallback, errorCallback);

        }

        function SelectWarningType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/reference/warning-type").then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/warningLetter/insert", param).then(successCallback, errorCallback);
        }
        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/complaint/update", param).then(successCallback, errorCallback);
        }

        function editActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/complaint/updateactive", param).then(successCallback, errorCallback);
        }

        function SelectTemplate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/warningLetter/selectTemplate", param).then(successCallback, errorCallback);
        }

        function SelectEmployeeName(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/pegawai").then(successCallback, errorCallback);
        }

        function UpdateTemplate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/warningLetter/updateTemplate", param).then(successCallback, errorCallback);
        }
        
        function getCurrentUser(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/complaint/getCurrentUser").then(successCallback, errorCallback);
        }

        function checkExistingRecord(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/warningLetter/checkExistingRecord", param).then(successCallback, errorCallback);
        }

        function SelectVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/complaint/selectvendor", param).then(successCallback, errorCallback);
        }
    }
})();