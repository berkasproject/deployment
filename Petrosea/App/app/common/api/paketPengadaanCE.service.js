﻿(function () {
    'use strict';

    angular.module('app').factory('PaketPengadaanCEService', dataService);

    dataService.$inject = ['GlobalConstantService'];

    function dataService(GlobalConstantService) {
        var endpoint = GlobalConstantService.getConstant('admin_endpoint');

        var service = {
            IsAllowEdit: IsAllowEdit,
            SelectCR: SelectCR,
            //SelectTPByCRID: SelectTPByCRID,
            SelectByID: SelectByID,
            SaveTP: SaveTP,
            ConfirmTP: ConfirmTP,
            GetTenderTypes: GetTenderTypes,
            GetAllOptions: GetAllOptions,
            GetStepsByMethod: GetStepsByMethod,
            CancelVariation: CancelVariation,
            ReviseOE: ReviseOE,
            IsOERevision: IsOERevision,
            ReCreatePackage: ReCreatePackage,
            ReTenderPackage: ReTenderPackage,
            GetRegisteredVendors: GetRegisteredVendors,
            GetRetenderVendors: GetRetenderVendors
        };

        return service;

        function IsAllowEdit(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/isAllowEdit', param).then(successCallback, errorCallback);
        }

        function SelectCR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/selectContractRequisition', param).then(successCallback, errorCallback);
        }

        //function SelectTPByCRID(param, successCallback, errorCallback) {
        //    GlobalConstantService.post(endpoint + '/tenderPackage/getByCRID', param).then(successCallback, errorCallback);
        //}

        function SelectByID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/getByID', param).then(successCallback, errorCallback);
        }

        function SaveTP(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/create', param).then(successCallback, errorCallback);
        }

        function ConfirmTP(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/confirm', param).then(successCallback, errorCallback);
        }

        function GetTenderTypes(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/getTenderPackageType').then(successCallback, errorCallback);
        }

        function GetAllOptions(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/getAllOptions', param).then(successCallback, errorCallback);
        }

        function GetStepsByMethod(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/getStepsByMethod', param).then(successCallback, errorCallback);
        }

        function CancelVariation(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/cancelVariation', param).then(successCallback, errorCallback);
        }

        function ReviseOE(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/reviseOE', param).then(successCallback, errorCallback);
        }

        function IsOERevision(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/isOERevision', param).then(successCallback, errorCallback);
        }

        function ReCreatePackage(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/reCreatePackage', param).then(successCallback, errorCallback);
        }

        function ReTenderPackage(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/reTenderPackage', param).then(successCallback, errorCallback);
        }

        function GetRegisteredVendors(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/registeredVendors', param).then(successCallback, errorCallback);
        }

        function GetRetenderVendors(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tenderPackage/retenderVendors', param).then(successCallback, errorCallback);
        }
    }
})();