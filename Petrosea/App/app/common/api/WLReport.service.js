(function () {
    'use strict';

    angular.module("app")
        .factory("WLReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            warningLetterReport: warningLetterReport,
            reportbyWLtype:reportbyWLtype
        };
        return service;

        // implementation

        function warningLetterReport(param, successCallback, errorCallback) {
            //console.info("param" + JSON.stringify(param));
            GlobalConstantService.post(adminpoint + "/warningletterreport/report", param).then(successCallback, errorCallback);
        }
        function reportbyWLtype(param, successCallback, errorCallback) {
            //console.info("param" + JSON.stringify(param));
            GlobalConstantService.post(adminpoint + "/warningletterreport/reportbyWLtype", param).then(successCallback, errorCallback);
        }
    }
})();