﻿(function () {
    'use strict';

    angular.module("app").factory("VPCPRDataService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            getStatusOptions: getStatusOptions,
            selectbyid: selectbyid,
            getByType: getByType,
            getContract: getContract,
            noDraft: noDraft,
            getPreviousCPR: getPreviousCPR,
            insert: insert,
            insertDefault: insertDefault,
            update: update,
            inserturl: inserturl,
            publish: publish,
            reject: reject,
            publishskipvendor: publishskipvendor,
            getapprovalhistories: getapprovalhistories,
            //insertWL: insertWL,
            switchActive: switchActive,
            cekRole: cekRole,
            sendToVendor: sendToVendor,
            selectDocsNonCPRId: selectDocsNonCPRId,
            selectDocsCPRId: selectDocsCPRId,
            saveDocNonCPRId: saveDocNonCPRId,
            saveDocPostCPRId:saveDocPostCPRId,
            deleteDoc: deleteDoc,
            getReminder: getReminder
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/select", param).then(successCallback, errorCallback);
        }

        function getStatusOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/getStatusOptions").then(successCallback, errorCallback);
        }

        function selectbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/selsectbyId", param).then(successCallback, errorCallback);
        }

        function getByType(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/selectEvalByType", param).then(successCallback, errorCallback);
        }

        function getContract(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/getdatacontract").then(successCallback, errorCallback);
        }

        function noDraft(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/noDraft", param).then(successCallback, errorCallback);
        }

        function getPreviousCPR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/getpreviouscpr", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/insert", param).then(successCallback, errorCallback);
        }

        function insertDefault(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/insertDefault", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/update", param).then(successCallback, errorCallback);
        }

        function inserturl(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/insertUrl", param).then(successCallback, errorCallback);
        }

        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/publish", param).then(successCallback, errorCallback);
        }

        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/reject", param).then(successCallback, errorCallback);
        }

        function publishskipvendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/publishskipvendoracc", param).then(successCallback, errorCallback);
        }

        function getapprovalhistories(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/getapprovalhistories", param).then(successCallback, errorCallback);
        }

        //function insertWL(param, successCallback, errorCallback) {
        //    GlobalConstantService.post(endpoint + "/vpdata/insertWL", param).then(successCallback, errorCallback);
        //}

        function switchActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/switchactive", param).then(successCallback, errorCallback);
        }

        function cekRole(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/cekRole").then(successCallback, errorCallback);
        }

        function sendToVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/sendToVendor", param).then(successCallback, errorCallback);
        }
        
        function selectDocsNonCPRId(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vpdata/selectDocsNonCPRId").then(successCallback, errorCallback);
        }

        function selectDocsCPRId(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/selectDocsCPRId", param).then(successCallback, errorCallback);
        }

        function saveDocNonCPRId(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/saveDocNonCPRId", param).then(successCallback, errorCallback);
        }

        function saveDocPostCPRId(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/saveDocPostCPRId", param).then(successCallback, errorCallback)
        }

        function deleteDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/deleteDoc", param).then(successCallback, errorCallback);
        }

        function getReminder(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpdata/getreminder", param).then(successCallback, errorCallback);
        }
    }
})();