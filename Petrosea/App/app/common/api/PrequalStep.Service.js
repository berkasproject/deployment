﻿(function () {
	'use strict';

	angular.module('app').factory('PrequalStepService', prequalStepService);

	prequalStepService.$inject = ['GlobalConstantService'];

	//@ngInject
	function prequalStepService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('admin_endpoint');

		//interfaces
		var service = {
		    getformtypes: getformtypes,
		    getbyid: getbyid,
			select: select,
			insert: insert,
			update: update,
			remove: remove,
		};

		return service;

		function getformtypes(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint+ '/prequalstep/getformtypes').then(successCallback, errorCallback);
		}
		function getbyid(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/prequalstep/getbyid', param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/prequalstep/select', param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/prequalstep/insert', param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/prequalstep/update', param).then(successCallback, errorCallback);
		}
		function remove(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/prequalstep/remove', param).then(successCallback, errorCallback);
		}

	}
})();