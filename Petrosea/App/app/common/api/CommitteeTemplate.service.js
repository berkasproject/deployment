﻿(function () {
	'use strict';

	angular.module('app').factory('CommitteeTemplateService', contractSignOffService);

	contractSignOffService.$inject = ['GlobalConstantService'];

	function contractSignOffService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('admin_endpoint');

		var service = {
		    Select: Select,
		    GetAll: GetAll,
		    GetById: GetById,
		    Save: Save,
		    Delete: Delete,
		    SelectEmployee: SelectEmployee,
		    GetPositions: GetPositions
		};

		return service;

		function Select(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/select', model).then(successCallback, errorCallback);
		}
		function GetAll(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/getall').then(successCallback, errorCallback);
		}
		function GetById(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/getbyid', model).then(successCallback, errorCallback);
		}
		function Save(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/save', model).then(successCallback, errorCallback);
		}
		function Delete(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/delete', model).then(successCallback, errorCallback);
		}
		function SelectEmployee(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/selectemployee', model).then(successCallback, errorCallback);
		}
		function GetPositions(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/committeetemplate/getpositions').then(successCallback, errorCallback);
		}
	}
})();