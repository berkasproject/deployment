(function () {
    'use strict';

    angular.module("app")
        .factory("KPIVendorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            getNewVendorCount: getNewVendorCount,
            getModifyVendorCount: getModifyVendorCount
        };
        return service;

        // implementation

        function getNewVendorCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiVendor/getNewVendorCount", param).then(successCallback, errorCallback);
        }
        function getModifyVendorCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiVendor/getModifyVendorCount", param).then(successCallback, errorCallback);
        }
    }
})();