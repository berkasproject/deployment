﻿(function () {
	'use strict';

	angular.module("app").factory("GoodsAwardService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		var Publicendpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			select: select,
			update: update,
			selectExcelVendor: selectExcelVendor,
			updateApproval: updateApproval,
			sendMail: sendEmail,
			selectVendorNotDeal: selectVendorNotDeal,
			SendApproval: SendApproval,
			GetApproval: GetApproval,
			CekEmployee: CekEmployee,
			CekRequestor: CekRequestor,
			selectTaxCode: selectTaxCode,
			selectStorageLocation: selectStorageLocation,
			selectApproval: selectApproval,
			sendMailRFQGoods: sendMailRFQGoods,
			cancelVendor: cancelVendor,
			sendEmailApprovalCancel: sendEmailApprovalCancel,
			saveGoodsSummary: saveGoodsSummary,
			approve: approve,
			checkAuth: checkAuth,
			downloadDoc: downloadDoc,
			uploadDoc: uploadDoc
		};

		return service;
		function sendEmailApprovalCancel(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/sendEmailApprovalCancel', model).then(successCallback, errorCallback);
		}
		function cancelVendor(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/cancelVendor', model).then(successCallback, errorCallback);
		}
		function downloadDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/downloadSAPDoc', model).then(successCallback, errorCallback);
		}
		function uploadDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/UploadDoc', model).then(successCallback, errorCallback);
		}
		function checkAuth(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/checkAuth').then(successCallback, errorCallback);
		}
		function sendMailRFQGoods(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/SendMail', model).then(successCallback, errorCallback);
		}
		function selectApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAwardApproval/getGoodsAward', model).then(successCallback, errorCallback);
		}
		function selectStorageLocation(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + '/StorageLocation').then(successCallback, errorCallback);
		}
		function selectTaxCode(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + '/taxcode').then(successCallback, errorCallback);
		}
		function saveGoodsSummary(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/saveGoodsSummary', model).then(successCallback, errorCallback);
		}
		function CekRequestor(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/CekRequestor', model).then(successCallback, errorCallback);
		}
		function CekEmployee(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/GoodsAward/CekEmployee', model).then(successCallback, errorCallback);
		}
		function GetApproval(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAward/GetApproval", param).then(successCallback, errorCallback);
		}
		function SendApproval(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAward/SendApproval", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAward", param).then(successCallback, errorCallback);
		}
		function selectVendorNotDeal(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAward/NotDeal", param).then(successCallback, errorCallback);
		}

		function sendEmail(mail, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/GoodsAward/SendEmail", mail).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/GoodsAward/Update", param).then(successCallback, errorCallback);
		}

		function selectExcelVendor(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAward/ExcelVendor", param).then(successCallback, errorCallback);
		}
		function updateApproval(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAward/UpdateApproval", param).then(successCallback, errorCallback);
		}
		function approve(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/GoodsAwardApproval/approve", param).then(successCallback, errorCallback);
		}
	}
})();