﻿(function () {
    'use strict';

    angular.module("app").factory("CommodityService", dataService);

    dataService.$inject = ['GlobalConstantService'];
    /* @ngInject */
    function dataService(GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            create: create,
            update: update,
            inactivate: inactivate,
            activate: activate,
            getByID: getByID
        };

        return service;

        //implementaion
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/commodity/select", param).then(successCallback, errorCallback);
        }

        function create(param, successCallback, errorCallback) {
            //console.info("test" + endpoint);
            GlobalConstantService.post(endpoint + '/commodity/create', param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/commodity/update', param).then(successCallback, errorCallback);
        }

        function inactivate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/commodity/inactivate', param).then(successCallback, errorCallback);
        }

        function activate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/commodity/activate', param).then(successCallback, errorCallback);
        }

        function getByID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/commodity/getByID', param).then(successCallback, errorCallback);
        }
    }
})()