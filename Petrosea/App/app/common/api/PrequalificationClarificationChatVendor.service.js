﻿(function () {
  'use strict';

  angular.module("app").factory("PrequalificationClarificationChatVendorService", dataService);

  dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
  /* @ngInject */
  function dataService($http, $q, GlobalConstantService) {

    var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
    // interfaces
    var service = {
      GetStepInfo: GetStepInfo,
      IsAllowed: IsAllowed,
      GetChats: GetChats,
      AddChat: AddChat,
      GetIsPeriodOpen: GetIsPeriodOpen,
      SendMailToCommitee: SendMailToCommitee
    };

    return service;
    
    function SendMailToCommitee(param, successCallback, errorCallback) {
      GlobalConstantService.post(endpoint + "/prequalClarification/SendMailToCommitee", param).then(successCallback, errorCallback);
    }
    
    function GetIsPeriodOpen(param, successCallback, errorCallback) {
      GlobalConstantService.post(endpoint + "/prequalClarification/isPeriodOpen", param).then(successCallback, errorCallback);
    }
    function GetStepInfo(param, successCallback, errorCallback) {
      GlobalConstantService.post(endpoint + "/prequalClarification/getstepinfo", param).then(successCallback, errorCallback);
    }
    function IsAllowed(param, successCallback, errorCallback) {
      GlobalConstantService.post(endpoint + "/prequalClarification/isallowed", param).then(successCallback, errorCallback);
    }
    function GetChats(param, successCallback, errorCallback) {
      GlobalConstantService.post(endpoint + "/prequalClarification/getchats", param).then(successCallback, errorCallback);
    }
    function AddChat(param, successCallback, errorCallback) {
      GlobalConstantService.post(endpoint + "/prequalClarification/addchat", param).then(successCallback, errorCallback);
    }
    
  }
})();