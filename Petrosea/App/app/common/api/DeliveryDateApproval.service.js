﻿(function () {
    'use strict';

    angular.module("app").factory("DeliveryDateApprovalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            //getResults: getResults,
            getAwardedItempr: getAwardedItempr,
            approve: approve,
            reject: reject
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/delidateappr/select", param).then(successCallback, errorCallback);
        }
        //function getResults(param, successCallback, errorCallback) {
        //    GlobalConstantService.post(endpoint + "/delidateappr/getresults", param).then(successCallback, errorCallback);
        //}
        function getAwardedItempr(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/delidateappr/getawardeditempr", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/delidateappr/approve", param).then(successCallback, errorCallback);
        }
        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/delidateappr/reject", param).then(successCallback, errorCallback);
        }

    }
})();