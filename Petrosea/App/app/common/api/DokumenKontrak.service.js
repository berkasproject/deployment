﻿(function () {
    'use strict';

    angular.module("app").factory("DokumenKontrakService", dataService);

    dataService.$inject = ['GlobalConstantService'];
    /* @ngInject */
    function dataService(GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            selectCM: selectCM,
            getTenderType: getTenderType,
            getBusinessField: getBusinessField,
            getJenisTender: getJenisTender,
            save: save,
            getData: getData,
            aktif: aktif,
            nonAktif: nonAktif
        };

        return service;

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen-kontrak/select", param).then(successCallback, errorCallback);
        }

        function selectCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen-kontrak/selectCM", param).then(successCallback, errorCallback);
        }

        function getTenderType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/dokumen-kontrak/getTenderType").then(successCallback, errorCallback);
        }

        function getBusinessField(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/dokumen-kontrak/getBusinessField").then(successCallback, errorCallback);
        }

        function getJenisTender(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/dokumen-kontrak/getJenisTender").then(successCallback, errorCallback);
        }

        function save(param, successCallback, errorCallback) {
            if (param.create == 1) {
                GlobalConstantService.post(endpoint + "/dokumen-kontrak/insert", param).then(successCallback, errorCallback);
            } else if (param.create == 0) {
                GlobalConstantService.post(endpoint + "/dokumen-kontrak/update", param).then(successCallback, errorCallback);
            }
        }

        function getData(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen-kontrak/getData", param).then(successCallback, errorCallback);
        }

        function aktif(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen-kontrak/aktif", param).then(successCallback, errorCallback);
        }

        function nonAktif(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen-kontrak/nonAktif", param).then(successCallback, errorCallback);
        }
    }
})()