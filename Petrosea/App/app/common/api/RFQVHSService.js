﻿(function () {
	'use strict';

	angular.module('app').factory('RFQVHSService', rfqvhsService);

	rfqvhsService.$inject = ['GlobalConstantService'];

	function rfqvhsService(GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant('admin_endpoint');
		var endpoint2 = GlobalConstantService.getConstant("api_endpoint");

		var service = {
			detailApproval: detailApproval,
			getEvalMethod: getEvalMethod,
			getIncoTerms: getIncoTerms,
			getLocation: getLocation,
			getAllVendors: getAllVendors,
			getVendorByID: getVendorByID,
			create: create,
			update: update,
			selectRFQ: selectRFQ,
			getProcMethods: getProcMethods,
			getSteps: getSteps,
			loadRFQVHS: loadRFQVHS,
			getRFQVHSItems: getRFQVHSItems,
			getCommodities: getCommodities,
			getCommoditiesByItem: getCommoditiesByItem,
			viewVendor: viewVendor,
			getCompScale: getCompScale,
			getFixCustom: getFixCustom,
			sendForApproval: sendForApproval,
			approve: approve,
			getPlants: getPlants,
			reject: reject,
			publish: publish,
			generateCode: generateCode,
			getDocTypes: getDocTypes,
			selectDoc: selectDoc,
			createDoc: createDoc,
			updateDoc: updateDoc,
			deleteDoc: deleteDoc,
			getTypeTender: getTypeTender,
			getOptionsTender: getOptionsTender,
			getBidderMethod: getBidderMethod,
			getStateDelivery: getStateDelivery,
			getCityDelivery: getCityDelivery,
			getApprovalData: getApprovalData,
			getDefaultTemplate: getDefaultTemplate,
			getPymentTerm: getPymentTerm,
			selectcommite: selectcommite,
			selectposition: selectposition,
			selectemployee: selectemployee,
			getVHSItemTemplate: getVHSItemTemplate,
			chkBidder: chkBidder,
			getUserLogin: getUserLogin,
			getProvince: getProvince,
			getCity: getCity,
			getDepartment: getDepartment,
			checkMaterialItem: checkMaterialItem
			//insertCommitee: insertCommitee
		};

		return service;

		function getDefaultTemplate(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getDefaultTemplate').then(successCallback, errorCallback);
		}

		function selectposition(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/selectposition', param).then(successCallback, errorCallback);
		}

		//function insertCommitee(param, successCallback, errorCallback) {
		//	GlobalConstantService.post(endpoint + '/RFQVHS/insertCommitee', param).then(successCallback, errorCallback);
		//}

		function getUserLogin(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getUserLogin', param).then(successCallback, errorCallback);
		}

		function selectemployee(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/selectemployee', param).then(successCallback, errorCallback);
		}

		function getApprovalData(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getApprovalData', model).then(successCallback, errorCallback);
		}

		function getIncoTerms(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getIncoTerms', model).then(successCallback, errorCallback);
		}

		function getEvalMethod(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getEvalMethod', model).then(successCallback, errorCallback);
		}

		function getLocation(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getLocation').then(successCallback, errorCallback);
		}

		function selectcommite(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/selectcommite', param).then(successCallback, errorCallback);
		}

		function getAllVendors(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getAllVendor', model).then(successCallback, errorCallback);
		}

		function getVendorByID(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getVendorByID', model).then(successCallback, errorCallback);
		}

		function create(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/create', model).then(successCallback, errorCallback);
		}

		function update(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/update', model).then(successCallback, errorCallback);
		}

		function getProcMethods(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getProcMethods', model).then(successCallback, errorCallback);
		}

		function selectRFQ(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/selectRFQ', model).then(successCallback, errorCallback);
		}

		function getSteps(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getStepsByMethod', model).then(successCallback, errorCallback);
		}

		function loadRFQVHS(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/loadRFQVHS', model).then(successCallback, errorCallback);
		}

		function getRFQVHSItems(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getRFQVHSItems', model).then(successCallback, errorCallback);
		}

		function getCommodities(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getCommodities').then(successCallback, errorCallback);
		}

		function getPymentTerm(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getPymentTerm').then(successCallback, errorCallback);
		}

		function getCommoditiesByItem(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getCommoditiesbyitem', param).then(successCallback, errorCallback);
		}

		function viewVendor(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/viewVendor', model).then(successCallback, errorCallback);
		}

		function getCompScale(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getCompScale').then(successCallback, errorCallback);
		}

		function getPlants(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getPlants').then(successCallback, errorCallback);
		}

		function getFixCustom(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getFixCustom').then(successCallback, errorCallback);
		}

		function sendForApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/sendforapproval', model).then(successCallback, errorCallback);
		}

		function approve(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/approve', model).then(successCallback, errorCallback);
		}

		function reject(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/reject', model).then(successCallback, errorCallback);
		}

		function publish(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/publish', model).then(successCallback, errorCallback);
		}

		function detailApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/detailApproval', model).then(successCallback, errorCallback);
		}

		function generateCode(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/generatecode', model).then(successCallback, errorCallback);
		}

		function getDocTypes(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/doctypes').then(successCallback, errorCallback);
		}

		function selectDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/selectdoc', model).then(successCallback, errorCallback);
		}

		function createDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/createdoc', model).then(successCallback, errorCallback);
		}

		function updateDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/updatedoc', model).then(successCallback, errorCallback);
		}

		function deleteDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/deletedoc', model).then(successCallback, errorCallback);
		}

		function getVHSItemTemplate(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/getvhsitemtemplate').then(successCallback, errorCallback);
		}

		function checkMaterialItem(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/RFQVHS/checkMaterialItem', param).then(successCallback, errorCallback);
		}

		function getTypeTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_TYPE_RFQ" };
			GlobalConstantService.post(endpoint2 + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getOptionsTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_OPTIONS_RFQ" };
			GlobalConstantService.post(endpoint2 + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getBidderMethod(successCallback, errorCallback) {
			var param = { Keyword: "BIDDER_METHOD" };
			GlobalConstantService.post(endpoint2 + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getStateDelivery(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/rfqgoods/getFreightCosts').then(successCallback, errorCallback);
			//GlobalConstantService.post(endpoint + '/rfqgoods/getstates').then(successCallback, errorCallback);
		}

		function getCityDelivery(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/rfqgoods/getcitiesbystate', param).then(successCallback, errorCallback);
		}

		function chkBidder(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/RFQVHS/chkBidder', param).then(successCallback, errorCallback);
		}

		function getProvince(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + '/RFQVHS/getProvince').then(successCallback, errorCallback);
		}

		function getCity(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/RFQVHS/getCity', param).then(successCallback, errorCallback);
		}

		function getDepartment(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + '/RFQVHS/getDepartment').then(successCallback, errorCallback);
		}
	}
})();