﻿(function () {
	'use strict';

	angular.module('app').factory('TenderStepService', tenderStepService);

	tenderStepService.$inject = ['GlobalConstantService'];

	//@ngInject
	function tenderStepService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('admin_endpoint');

		//interfaces
		var service = {
			getFormTypes: getFormTypes,
			create: create,
			select: select,
			getByID: getByID,
			isInUse: isInUse,
			update: update,
			inactivate: inactivate,
			activate: activate
		};

		return service;

		function getFormTypes(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/formTypes', model).then(successCallback, errorCallback);
		}

		function isInUse(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/isInUse', model).then(successCallback, errorCallback);
		}

		function create(tenderStepModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/create', tenderStepModel).then(successCallback, errorCallback);
		}

		function select(selectModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/select', selectModel).then(successCallback, errorCallback);
		}

		function getByID(selectModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/getByID', selectModel).then(successCallback, errorCallback);
		}

		function update(selectModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/update', selectModel).then(successCallback, errorCallback);
		}

		function inactivate(selectModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/inactivate', selectModel).then(successCallback, errorCallback);
		}

		function activate(selectModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderStep/activate', selectModel).then(successCallback, errorCallback);
		}
	}
})();