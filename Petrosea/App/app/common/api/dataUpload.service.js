﻿(function () {
	'use strict';

	angular.module('app').factory('dataUploadService', svc);

	svc.$inject = ['$upload', '$http', '$q', 'GlobalConstantService'];

	//@ngInject
	function svc($upload, $http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		//interfaces
		var service = {
			Select: Select,
			Insert: Insert,
			Detail: Detail,
			Compare: Compare,
			InsertFile: InsertFile,
			DeleteFile: DeleteFile,
			readExcelAPI: readExcelAPI
		};

		return service;


		function DeleteFile(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDataupload/deleteFile', model).then(successCallback, errorCallback);
		}
		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDataUpload', param).then(successCallback, errorCallback);
		}
		function InsertFile(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/selectDataupload/insertFile", model).then(successCallback, errorCallback);
		}
		function Insert(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/selectDataUpload/insert", model).then(successCallback, errorCallback);
		}
		function Detail(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/detailDataUpload", model).then(successCallback, errorCallback);
		}
		function Compare(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/compareDataUpload", model).then(successCallback, errorCallback);
		}

		function readExcelAPI(file, successCallback, errorCallback) {
			var param = {
				url: adminpoint + '/dataUpload/readExcelAPI',
				file: file.Data1,
				fields: { idSAP: file.Data2 }
			};
			$upload.upload(param).then(successCallback, errorCallback);
		}
	}
})();