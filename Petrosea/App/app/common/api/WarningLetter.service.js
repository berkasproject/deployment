﻿(function () {
	'use strict';

	angular.module("app").factory("WarningLetterService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
		    select: select,
		    SelectContractByVendor_Jasa: SelectContractByVendor_Jasa,
		    SelectContractByVendor_Barang: SelectContractByVendor_Barang,
		    SelectContractByVendor_VHS: SelectContractByVendor_VHS,
		    SelectVendorName: SelectVendorName,
		    SelectWarningType: SelectWarningType,
		    SelectWarning: SelectWarning,
		    insert: insert,
		    update: update,
		    editActive: editActive,
		    SelectTemplate: SelectTemplate,
		    SelectDepartment: SelectDepartment,
		    UpdateTemplate: UpdateTemplate,
		    getFooter: getFooter,
		    getAccumulationPar: getAccumulationPar,
		    getContact: getContact,
		    getAddress: getAddress,
		    getCompanyPerson: getCompanyPerson,
		    cekRole: cekRole,
		    changeState: changeState,
		    stateSent: stateSent,
		    stateIncomplete: stateIncomplete,
		    stateReview: stateReview,
		    stateCancel: stateCancel,
		    stateOnProcess: stateOnProcess,
		    stateRejected: stateRejected,
		    stateApproved: stateApproved,
		    stateSendVendor: stateSendVendor,
		    stateRevise: stateRevise,
		    stateClosed: stateClosed,
		    stateComplete: stateComplete,
		    stateCompleteSkipVendor: stateCompleteSkipVendor,
		    getNumberSequence: getNumberSequence,
		    getApprover: getApprover,
		    updateNumberSequence: updateNumberSequence,
		    getPositionDetail: getPositionDetail,
		    getEmployeeDetail: getEmployeeDetail,
		    SelectWarningTypeEn: SelectWarningTypeEn,
		    checkContract: checkContract,
		    getAdditionalInfoJasa: getAdditionalInfoJasa,
		    getAdditionalInfoVHS: getAdditionalInfoVHS,
		    getAdditionalInfoBarang: getAdditionalInfoBarang,
		    getComplainType: getComplainType,
		    SelectContractByVendor_BarangPO: SelectContractByVendor_BarangPO,
		    //insertWarningLetterByScoreCPR: insertWarningLetterByScoreCPR,
		    //insertWarningLetterByScoreVHS: insertWarningLetterByScoreVHS,
		    stateApprovedByL1: stateApprovedByL1,
		    stateApprovedByVendor: stateApprovedByVendor,
		    stateApprovedByL3: stateApprovedByL3,
		    stateApprovedByL1Support: stateApprovedByL1Support,
		    selectWLApproval: selectWLApproval,
		    getApproval: getApproval,
		    getCurrentUserDepartment: getCurrentUserDepartment,
		    getCurrentEmpID: getCurrentEmpID,
		    getDepartmentDetail: getDepartmentDetail,
		    checkExistingRecord: checkExistingRecord,
		    sendMailToVendor: sendMailToVendor,
		    stateCancelPost: stateCancelPost,
		    GetRemarks: GetRemarks
		};

		return service;


		function select(param, successCallback, errorCallback) {			
			GlobalConstantService.post(endpoint + "/warningLetter/select", param).then(successCallback, errorCallback);
		}

		function SelectContractByVendor_Jasa(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/selectContractByVendor_Jasa", param).then(successCallback, errorCallback);
		}

		function SelectContractByVendor_Barang(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/selectContractByVendor_Barang", param).then(successCallback, errorCallback);
		}

		function SelectContractByVendor_VHS(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/selectContractByVendor_VHS", param).then(successCallback, errorCallback);
		}

		function SelectVendorName(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/warningLetter/listVendorName").then(successCallback, errorCallback);
		}

		function SelectWarningType(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/reference/warning-type").then(successCallback, errorCallback);
        }

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/insert", param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/complaint/update", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/editActive", param).then(successCallback, errorCallback);
		}

		function SelectTemplate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/selectTemplate", param).then(successCallback, errorCallback);
		}

		function SelectDepartment(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/departmen").then(successCallback, errorCallback);
		}

		function UpdateTemplate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/updateTemplate", param).then(successCallback, errorCallback);
		}

		function SelectWarning(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/reference/warning").then(successCallback, errorCallback);
		}

		function getFooter(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getFooter").then(successCallback, errorCallback);
		}

		function getAccumulationPar(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getAccumulationPar", param).then(successCallback, errorCallback);
		}

		function getContact(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getContact", param).then(successCallback, errorCallback);
		}

		function getAddress(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getAddress", param).then(successCallback, errorCallback);
		}

		function getCompanyPerson(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getCompanyPerson", param).then(successCallback, errorCallback);
		}

		function cekRole(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/cekRole").then(successCallback, errorCallback);
		}

		function changeState(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/changeState", param).then(successCallback, errorCallback);
		}

		function stateSent(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateSent", param).then(successCallback, errorCallback);
		}

		function stateIncomplete(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateIncomplete", param).then(successCallback, errorCallback);
		}

		function stateReview(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateReview", param).then(successCallback, errorCallback);
		}

		function stateCancel(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateCancel", param).then(successCallback, errorCallback);
		}

		function stateOnProcess(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateOnProcess", param).then(successCallback, errorCallback);
		}

		function stateRejected(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateRejected", param).then(successCallback, errorCallback);
		}

		function stateApproved(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateApproved", param).then(successCallback, errorCallback);
		}

		function stateSendVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateSendVendor", param).then(successCallback, errorCallback);
		}

		function stateRevise(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateRevise", param).then(successCallback, errorCallback);
		}

		function stateClosed(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateClosed", param).then(successCallback, errorCallback);
		}

		function stateComplete(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateComplete", param).then(successCallback, errorCallback);
		}

		function stateCompleteSkipVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateCompleteSkipVendor", param).then(successCallback, errorCallback);
		}

		function getNumberSequence(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getNumberSequence", param).then(successCallback, errorCallback);
		}
		
		function getApprover(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getApprover", param).then(successCallback, errorCallback);
		}
		
		function updateNumberSequence(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/updateNumberSequence", param).then(successCallback, errorCallback);
		}

		function getPositionDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getPositionDetail", param).then(successCallback, errorCallback);
		} getEmployeeDetail

		function getEmployeeDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getEmployeeDetail", param).then(successCallback, errorCallback);
		}

		function SelectWarningTypeEn(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/reference/warning-type-en").then(successCallback, errorCallback);
		}

		function checkContract(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/checkContract", param).then(successCallback, errorCallback);
		}
		
		function getAdditionalInfoJasa(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getAdditionalInfoJasa", param).then(successCallback, errorCallback);
		}

		function getAdditionalInfoVHS(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getAdditionalInfoVHS", param).then(successCallback, errorCallback);
		}

		function getAdditionalInfoBarang(param, successCallback, errorCallback){
		    GlobalConstantService.post(endpoint + "/warningLetter/getAdditionalInfoBarang", param).then(successCallback, errorCallback);
		}

		function getComplainType(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/warningLetter/getComplainType").then(successCallback, errorCallback);
		}
		
		function SelectContractByVendor_BarangPO(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/selectContractByVendor_BarangPO", param).then(successCallback, errorCallback);
		}

		//function insertWarningLetterByScoreCPR(param, successCallback, errorCallback) {
		//    GlobalConstantService.post(endpoint + "/warningLetter/insertWarningLetterByScoreCPR", param).then(successCallback, errorCallback);
		//} 

		//function insertWarningLetterByScoreVHS(param, successCallback, errorCallback) {
		//    GlobalConstantService.post(endpoint + "/warningLetter/insertWarningLetterByScoreVHS", param).then(successCallback, errorCallback);
		//}

		function stateApprovedByL1(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateApprovedByL1", param).then(successCallback, errorCallback);
		}

		function stateApprovedByVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateApprovedByVendor", param).then(successCallback, errorCallback);
		}

		function stateApprovedByL3(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateApprovedByL3", param).then(successCallback, errorCallback);
		}

		function stateApprovedByL1Support(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateApprovedByL1Support", param).then(successCallback, errorCallback);
		}

		function selectWLApproval(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/selectWLApproval", param).then(successCallback, errorCallback);
		}

		function getApproval(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getApproval", param).then(successCallback, errorCallback);
		}
		

		function getCurrentUserDepartment(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/warningLetter/getCurrentUserDepartment").then(successCallback, errorCallback);
		} 

		function getCurrentEmpID(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/warningLetter/getCurrentEmpID").then(successCallback, errorCallback);
		}

		function getDepartmentDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getDepartmentDetail", param).then(successCallback, errorCallback);
		}

		function checkExistingRecord(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/checkExistingRecord", param).then(successCallback, errorCallback);
		}

		function sendMailToVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/sendMailToVendor", param).then(successCallback, errorCallback);
		}
		
		function stateCancelPost(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/stateCancelPost", param).then(successCallback, errorCallback);
		}

		function GetRemarks(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/warningLetter/getRemarks", param).then(successCallback, errorCallback);
		}
	}
})();