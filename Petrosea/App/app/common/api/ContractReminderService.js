﻿(function () {
	'use strict'

	angular.module("app").factory("CtrReminderService", dataService)

	dataService.$inject = ['$http', '$q', 'GlobalConstantService']

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint")
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint")
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint")

		// interfaces
		var service = {
		    getReminder: getReminder,
		    getReminderVHS: getReminderVHS,
            saveContract: saveContract
		}

		return service

		function getReminder(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/contractReminder/getReminder", param).then(successCallback, errorCallback)
		}

		function getReminderVHS(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/contractReminder/getReminderVHS", param).then(successCallback, errorCallback)
		}

		function saveContract(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/contractReminder/saveContract", param).then(successCallback, errorCallback)
		}
	}
})()