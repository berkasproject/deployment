﻿(function () {
	'use strict';

	angular.module("app").factory("OfferEntryService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
		    getAllDataOffer: getAllDataOffer,
		    GetStep: GetStep,
			getDataTender: getDataTender,
			getDataStepTender: getDataStepTender,
			getDataVendor: getDataVendor,
			getKelengkapanTender: getKelengkapanTender,
			saveKelengkapanTender: saveKelengkapanTender,
			savePricelist: savePricelist,
			updateDocs: updateDocs,
			updateChecklist: updateChecklist,
			getDocs: getDocs,
			getKelengkapanDocVendor: getKelengkapanDocVendor,
			approveDocByVendor: approveDocByVendor,
			getDataChecklistVendor: getDataChecklistVendor,
			approveChecklistByVendor: approveChecklistByVendor,
			getQuestionaireByVendor: getQuestionaireByVendor,
			approveQuestionaire: approveQuestionaire,
			getPricelist: getPricelist,
			complete: complete,
			sendToApproval: sendToApproval,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			GetSteps: GetSteps,
			isApprovalSent: isApprovalSent,
			detailApproval: detailApproval,
			insertDocUrl: insertDocUrl,
			deleteDocument: deleteDocument
		}
		return service;

		// implementation
		function GetSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/VOE/tahapan/getsteps", param).then(successCallback, errorCallback);
		}
		function complete(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/setComplete", param).then(successCallback, errorCallback);
		}

		function getPricelist(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getPricelist", param).then(successCallback, errorCallback);
		}
		function approveQuestionaire(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/approveQuestionaire", param).then(successCallback, errorCallback);
		}
		function getQuestionaireByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getQuestionaireByVendorID", param).then(successCallback, errorCallback);
		}
		function approveChecklistByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/approveChecklist", param).then(successCallback, errorCallback);
		}
		function getDataChecklistVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getChecklistByVendorID", param).then(successCallback, errorCallback);
		}
		function approveDocByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/approveDocument", param).then(successCallback, errorCallback);
		}
		function getKelengkapanDocVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getByVendorID", param).then(successCallback, errorCallback);
		}
		function getDocs(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/ServiceOfferEntry/getDoc", param).then(successCallback, errorCallback);
		}
		function updateChecklist(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/ServiceOfferEntry/updateChecklist", param).then(successCallback, errorCallback);
		}
		function updateDocs(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/ServiceOfferEntry/updateDoc", param).then(successCallback, errorCallback);
		}
		function saveKelengkapanTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/ServiceOfferEntry/save", param).then(successCallback, errorCallback);
		}
		function savePricelist(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/approvePricelist", param).then(successCallback, errorCallback);
		}
		function getKelengkapanTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/ServiceOfferEntry/getReqDocs", param).then(successCallback, errorCallback);
		}
		function getAllDataOffer(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/ServiceOfferEntry/getByTenderStepID", param).then(successCallback, errorCallback);
		}
		function GetStep(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + '/ServiceOfferEntry/SelectStep', param).then(successCallback, errorCallback);
		}
		function getDataTender(param, successCallback, errorCallback) {
			//{ProcPackageType: vm.ProcPackType, TenderRefID: vm.IDTender}
			GlobalConstantService.post(adminpoint + "/tender/getinfo", param).then(successCallback, errorCallback);
		}
		function getDataStepTender(param, successCallback, errorCallback) {
			//{ID: vm.IDStepTender}
			GlobalConstantService.post(adminpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
		}
		function getDataVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/getByTenderStepID", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ServiceOfferEntry/sendToApproval', param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ServiceOfferEntry/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ServiceOfferEntry/isApprovalSent', param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ServiceOfferEntry/detailApproval', param).then(successCallback, errorCallback);
		}
		function insertDocUrl(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/insertDocUrl", param).then(successCallback, errorCallback);
		}
		function deleteDocument(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/deleteDocument", param).then(successCallback, errorCallback);
		}
	}
})();