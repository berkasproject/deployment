﻿(function () {
	'use strict';

	angular.module("app").factory("ItemPRViewService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
		    select: select,
            detailItemPr:detailItemPr
		};

		return service;

		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/itemPRView/select", param).then(successCallback, errorCallback);
		}
		function detailItemPr(param, successCallback, errorCallback) {
		    //console.info("masuk service!! " + JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/itemPRView/detailItemPr", param).then(successCallback, errorCallback);
		}
	}
})()