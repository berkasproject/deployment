﻿(function () {
	'use strict';

	angular.module("app").factory("SrtPernyataanPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			All: All,
			AllMain: AllMain,
			selectDocLibrary: selectDocLibrary,
			selectUrlAgree: selectUrlAgree,
			selectUrlBConduct: selectUrlBConduct,
			selectUrlBConductEN: selectUrlBConductEN,
			selectUrlKuesioner: selectUrlKuesioner,
			selectNamaDir: selectNamaDir,
			selectLegalDoc: selectLegalDoc,
			selectContactID: selectContactID,
			selectAddressID: selectAddressID,
			selectAddress: selectAddress,
			selectCountryID: selectCountryID,
			selectCountry: selectCountry,
			insertAgree: insertAgree,
			updateAgree: updateAgree,
			insertDoc: insertDoc,
			updateDoc: updateDoc,
			insertDoc2: insertDoc2,
			selectCek: selectCek,
			selectCek2: selectCek2,
			CekAgree: CekAgree,
			selectData: selectData,
			isUploadAllowed: isUploadAllowed,
			datavendor: datavendor,
			load: load,
			Submit: Submit,
			loadPrequalStep: loadPrequalStep,
			cekSubmit: cekSubmit,
            loadContact: loadContact
		};

		return service;

	    // implementation
		function loadContact(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadVendorContact", param).then(successCallback, errorCallback);
		}
		function cekSubmit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreementpq/cekSubmit", param).then(successCallback, errorCallback);
		}
		function loadPrequalStep(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
		}
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreementpq/Submit", param).then(successCallback, errorCallback);
		}
		function CekAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/CekAgree", param).then(successCallback, errorCallback);
		}
		function load(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreementpq/load", param).then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function All(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/GetAllByVendorID", param).then(successCallback, errorCallback);
		}
		function AllMain(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/SelectAgree", param).then(successCallback, errorCallback);
		}
		function insertAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/insertAgree", param).then(successCallback, errorCallback);
		}
		function selectCek(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/Cek", param).then(successCallback, errorCallback);
		}
		function selectCek2(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/Cek2", param).then(successCallback, errorCallback);
		}
		function selectData(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/SelectData", param).then(successCallback, errorCallback);
		}
		function selectNamaDir(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getDirNameByVendorID", param).then(successCallback, errorCallback);
		}
		function selectDocLibrary(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getDocUrlLibrary", param).then(successCallback, errorCallback);
		}
		function selectUrlAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getDocUrlAgreement", param).then(successCallback, errorCallback);
		}
		function selectUrlBConduct(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getDocUrlBConduct", param).then(successCallback, errorCallback);
		}
		function selectUrlBConductEN(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getDocUrlBConductEN", param).then(successCallback, errorCallback);
		}
		function selectUrlKuesioner(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreementpq/getDocUrlKuesioner", param).then(successCallback, errorCallback);
		}
		function selectLegalDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getLegalDocByVendorID", param).then(successCallback, errorCallback);
		}
		function selectContactID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getContactID", param).then(successCallback, errorCallback);
		}
		function selectAddressID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getAddressID", param).then(successCallback, errorCallback);
		}
		function selectAddress(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getAddress", param).then(successCallback, errorCallback);
		}
		function selectCountryID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getCountryID", param).then(successCallback, errorCallback);
		}
		function selectCountry(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/getCountry", param).then(successCallback, errorCallback);
		}
		function updateAgree(param, successCallback, errorCallback) {
			//console.info("wdqqwweeqrggg");
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/UpdateAgree", param).then(successCallback, errorCallback);
		}
		function insertDoc(param, successCallback, errorCallback) {
			//console.info("awdawdawd");
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/insertDoc", param).then(successCallback, errorCallback);
		}
		function updateDoc(param, successCallback, errorCallback) {
			//console.info("awdawdawdawdawd");
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/UpdateDoc", param).then(successCallback, errorCallback);
		}
		function insertDoc2(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreementpq/insertDoc2", param).then(successCallback, errorCallback);
		}
		function isUploadAllowed(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
		function datavendor(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreementpq/datavendor").then(successCallback, errorCallback);
		}
	}
})();