﻿(function () {
    'use strict';

    angular.module("app").factory("NegosiasiService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        var Vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            selectFirst: selectFirst,
            selectChat: selectChat,
            selectChatVendor: selectChatVendor,
            selectTender: selectTender,
            InsertChat: InsertChat,
            InsertChatVendor: InsertChatVendor,
            Insert: Insert,
            selectVendor: selectVendor,
            selectByVendor: selectByVendor,
            selectVendorName: selectVendorName,
            selectStep: selectStep,
            selectStepVendor: selectStepVendor,
            selectLine: selectLine,
            InsertDetail: InsertDetail,
            InsertDetailVendor: InsertDetailVendor,
            InsertOpen: InsertOpen,
            EditQuantityNego: EditQuantityNego,
            InsertByPersen: InsertByPersen,
            InsertByPersenVendor: InsertByPersenVendor,
            InsertActive: InsertActive,
            updatedeal: updatedeal,
            cek: cek,
            updateitem: updateitem,
            CekDetail: CekDetail,
            sendMail: sendMail,
            isOvertime: isOvertime,
            InsertApproval: InsertApproval,
            InsertFinanceApproval: InsertFinanceApproval,
            CurrentFinanceApproval: CurrentFinanceApproval,
            CanReOpenNego: CanReOpenNego,
            ReOpenNegotiation: ReOpenNegotiation,
            sendMailToApprover: sendMailToApprover,
            GetApproval: GetApproval,
            GetNegoToApproval: GetNegoToApproval,
            updateApproval: updateApproval,
            sendMailToCE: sendMailToCE,
            loadStep: loadStep,
            selectLineCost: selectLineCost,
            selectHistory: selectHistory
        };

        return service;
        function loadStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/getTenderStep", param).then(successCallback, errorCallback);
        }
        function sendMailToCE(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/sendMailToCE", param).then(successCallback, errorCallback);
        }
        function updateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/updateApproval", param).then(successCallback, errorCallback);
        }
        function GetNegoToApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/GetNegoToApproval", param).then(successCallback, errorCallback);
        }
        function GetApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/GetApproval", param).then(successCallback, errorCallback);
        }
        function sendMailToApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/sendMailToApprover", param).then(successCallback, errorCallback);
        }
        function InsertApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/InsertApproval", param).then(successCallback, errorCallback);
        }
        function InsertFinanceApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/InsertFinanceApproval", param).then(successCallback, errorCallback);
        }
        function CurrentFinanceApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/CurrentFinanceApproval", param).then(successCallback, errorCallback);
        }
        function CanReOpenNego(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/CanReOpenNego", param).then(successCallback, errorCallback);
        }
        function ReOpenNegotiation(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/ReOpenNegotiation", param).then(successCallback, errorCallback);
        }
        function isOvertime(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negosiasi/getsteps", param).then(successCallback, errorCallback);
        }
        function sendMail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/sendMail", param).then(successCallback, errorCallback);
        }
        function CekDetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi/CekDetail", param).then(successCallback, errorCallback);
        }
        function selectFirst(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi", param).then(successCallback, errorCallback);
        }
        function selectChat(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/ByChatID", param).then(successCallback, errorCallback);
        }
        function selectChatVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi-vendor/ByChatID", param).then(successCallback, errorCallback);
        }
        function selectTender(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/step", param).then(successCallback, errorCallback);
        }
        function InsertChat(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/InsertChat", param).then(successCallback, errorCallback);
        }
        function InsertChatVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi-vendor/InsertChat", param).then(successCallback, errorCallback);
        }
        function Insert(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/Insert", param).then(successCallback, errorCallback);
        }
        function InsertActive(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/InsertByActive", param).then(successCallback, errorCallback);
        }
        function InsertByPersen(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/InsertByPersen", param).then(successCallback, errorCallback);
        }
        function InsertByPersenVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi-vendor/InsertByPersen", param).then(successCallback, errorCallback);
        }
        function InsertOpen(param, successCallback, errorCallback) {
            //console.info("masuk service!! " + JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/InsertOpen", param).then(successCallback, errorCallback);
        }
        function EditQuantityNego(param, successCallback, errorCallback) {
            //console.info("masuk service!! " + JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/EditQuantityNego", param).then(successCallback, errorCallback);
        }
        function selectVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/getcesuboffer", param).then(successCallback, errorCallback);
        }
        function selectLineCost(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/getcelineoffer", param).then(successCallback, errorCallback);
        }
        function selectByVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi-vendor/BySubParent", param).then(successCallback, errorCallback);
        }
        function selectVendorName(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/SelectVendor", param).then(successCallback, errorCallback);
        }
        function selectStep(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
        }
        function selectStepVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi/step", param).then(successCallback, errorCallback);
        }
        function selectLine(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/BySubID", param).then(successCallback, errorCallback);
        }
        function InsertDetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/InsertDetail", param).then(successCallback, errorCallback);
        }
        function InsertDetailVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi-vendor/InsertDetail", param).then(successCallback, errorCallback);
        }
        function updatedeal(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/UpdateDeal", param).then(successCallback, errorCallback);
        }
        function cek(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/Cek", param).then(successCallback, errorCallback);
        }
        function updateitem(param, successCallback, errorCallback) {
            console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(Vendorendpoint + "/negosiasi/UpdatePerItem", param).then(successCallback, errorCallback);
        }
        function selectHistory(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/negosiasi/SelectHistory", param).then(successCallback, errorCallback);
        }

    }
})();