(function () {
    'use strict';

    angular.module("app")
        .factory("InactiveVendorReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            inactiveVendorCount:inactiveVendorCount
            //getModifyVendorCount: getModifyVendorCount
        };
        return service;

        // implementation

        function inactiveVendorCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/inactiveVendorReport/inactiveVendorCount", param).then(successCallback, errorCallback);
        }
    }
})();