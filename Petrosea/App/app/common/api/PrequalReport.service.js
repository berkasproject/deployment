(function () {
    'use strict';

    angular.module("app")
        .factory("PrequalReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            prequalCreated: prequalCreated,
            prequalReport: prequalReport,
            prequalReportByVendor:prequalReportByVendor
        };
        return service;

        // implementation

        function prequalReport(param, successCallback, errorCallback) {
            //console.info("param" + JSON.stringify(param));
            GlobalConstantService.post(adminpoint + "/prequalReport", param).then(successCallback, errorCallback);
        }
        function prequalCreated(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalReport/prequalCreated").then(successCallback, errorCallback);
        }
        function prequalReportByVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalReport/prequalReportByVendor", param).then(successCallback, errorCallback);
        }
    }
})();