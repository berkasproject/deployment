﻿(function () {
    'use strict';

    angular.module("app").factory("RequisitionListService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            /*
            GetStatusOptions: GetStatusOptions,
            SelectCR: SelectCR,
            IsApprover: IsApprover,
            SetApprovalStatus: SetApprovalStatus
            */
        };

        return service;

        // implementation
        /*
        function GetStatusOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/requisitionlist/statusoptions").then(successCallback, errorCallback);
        }

        function SelectCR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/requisitionlist/selectcr", param).then(successCallback, errorCallback);
        }

        function IsApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/requisitionlist/isapprover", param).then(successCallback, errorCallback);
        }

        function SetApprovalStatus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/requisitionlist/setapprovalstatus", param).then(successCallback, errorCallback);
        }
        */
    }
})();