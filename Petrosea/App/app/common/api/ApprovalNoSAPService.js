﻿(function () {
	'use strict';

	angular.module("app").factory("ApprovalNoSAPService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			select: select,
			approve: approve,
			reject: reject,
			viewVendor: viewVendor,
			sendToApproveL1L2: sendToApproveL1L2,
			detailApproval: detailApproval
		};

		return service;

		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/noSAPApproval/select", param).then(successCallback, errorCallback);
		}
		function approve(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/noSAPApproval/approve", param).then(successCallback, errorCallback);
		}
		function reject(param, successCallback, errorCallback) {
			//console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/noSAPApproval/reject", param).then(successCallback, errorCallback);
		}
		function viewVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/noSAPApproval/viewVendor", param).then(successCallback, errorCallback);
		}
		function sendToApproveL1L2(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/noSAPApproval/SendToApproveL1L2", param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/noSAPApproval/detailApproval", param).then(successCallback, errorCallback);
		}
	}
})()