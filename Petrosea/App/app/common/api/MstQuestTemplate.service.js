﻿(function () {
    'use strict';

    angular.module("app")
        .factory("MstQuestTemplateService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            riskCriteria: riskCriteria,
            questAnswerType: questAnswerType,
            insertQuest: insertQuest,
            dataQuest: dataQuest,
            dataQuestById: dataQuestById,
            editQuest: editQuest,
            publishQuest: publishQuest,
            questAnswerTypeByAnswer:questAnswerTypeByAnswer
            //weekByYear: weekByYear
        };
        return service;

        // implementation

        function riskCriteria(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/riskCriteria").then(successCallback, errorCallback);
        }
        function questAnswerType(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/questAnswerType").then(successCallback, errorCallback);
        }
        function insertQuest(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/insertQuest", param).then(successCallback, errorCallback);
        }
        function dataQuest(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/dataQuest", param).then(successCallback, errorCallback);
        }
        function dataQuestById(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/dataQuestById", param).then(successCallback, errorCallback);
        }
        function editQuest(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/editQuest", param).then(successCallback, errorCallback);
        }
        function publishQuest(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/publishQuest", param).then(successCallback, errorCallback);
        }
        function questAnswerTypeByAnswer(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstDDQuest/questAnswerTypeByAnswer",param).then(successCallback, errorCallback);
        }
    }
})();