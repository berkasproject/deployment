(function () {
	'use strict';

	angular.module("app").factory("DataPerlengkapanPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

	    var endpoint = GlobalConstantService.getConstant("api_endpoint");
	    var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
	    var service = {

	        selectBuilding: selectBuilding,
		    selectEquipment: selectEquipment,

		    getCategoryBuilding: getCategoryBuilding,
		    getOwnership: getOwnership,
		    getConditionEq: getConditionEq,

		    saveBuilding: saveBuilding,
		    saveEquipment: saveEquipment,
		    deleteBuilding: deleteBuilding,
		    deleteEquipment: deleteEquipment,

		    loadPrequalStep: loadPrequalStep,
		    Submit: Submit
		};

		return service;

		function selectBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/building", param).then(successCallback, errorCallback);
		}
		function selectEquipment(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/equipment", param).then(successCallback, errorCallback);
		}

		function getCategoryBuilding(successCallback, errorCallback) {
		    var param = { Keyword: "BUILDING_CATEGORY" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getOwnership(successCallback, errorCallback) {
		    var param = { Keyword: "OWNERSHIP_STATUS" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getConditionEq(successCallback, errorCallback) {
		    var param = { Keyword: "CONDITION_TYPE" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function saveBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/building/save", param).then(successCallback, errorCallback);
		}
		function saveEquipment(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/equipment/save", param).then(successCallback, errorCallback);
		}
		function deleteBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/building/delete", param).then(successCallback, errorCallback);
		}
		function deleteEquipment(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/equipment/delete", param).then(successCallback, errorCallback);
		}

		function loadPrequalStep(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
		}
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBuildingPrequal/Submit", param).then(successCallback, errorCallback);
		}
	}
})();