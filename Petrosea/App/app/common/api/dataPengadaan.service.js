﻿(function () {
	'use strict';

	angular.module('app').factory('DataPengadaanService', dataService);

	dataService.$inject = ['GlobalConstantService'];

	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('admin_endpoint');

		var service = {
			Select: Select,
			SelectByID: SelectByID,
			GetStep: GetStep,
			GetStepByID: GetStepByID,
			GetStatusOption: GetStatusOption,
			AturStep: AturStep,
			AturStepAll: AturStepAll,
			CurrentStepDateApproval: CurrentStepDateApproval,
			SendStepDateApproval: SendStepDateApproval,
			IsAllowed: IsAllowed,
			IsAllowedByStep: IsAllowedByStep,
			IsAllowedEdit: IsAllowedEdit,
			CancelTender: CancelTender,
			ReviseOE: ReviseOE,
			IsOERevision: IsOERevision,
			StepIsNotStarted: StepIsNotStarted,
			StepHasEnded: StepHasEnded,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			sendToApproval: sendToApproval,
			isApproved: isApproved,
			detailApproval: detailApproval,
			getEvaluator: getEvaluator
		};

		return service;

		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/detailApproval', param).then(successCallback, errorCallback);
		}

		function isApproved(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/isApproved', param).then(successCallback, errorCallback);
		}

		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/sendToApproval', param).then(successCallback, errorCallback);
		}

		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/isApprovalSent', param).then(successCallback, errorCallback);
		}

		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}

		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/select', param).then(successCallback, errorCallback);
		}

		function SelectByID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/selectbyid', param).then(successCallback, errorCallback);
		}

		function GetStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/getstep', param).then(successCallback, errorCallback);
		}

		function GetStepByID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/stepbyid', param).then(successCallback, errorCallback);
		}

		function GetStatusOption(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/getStatusOption').then(successCallback, errorCallback);
		}

		function AturStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/aturstep', param).then(successCallback, errorCallback);
		}

		function AturStepAll(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/tender/aturstepall', param).then(successCallback, errorCallback);
		}

		function CurrentStepDateApproval(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/tender/currentstepdateapproval', param).then(successCallback, errorCallback);
		}

		function SendStepDateApproval(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/tender/sendstepdateapproval', param).then(successCallback, errorCallback);
		}

		function IsAllowed(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/isallowed', param).then(successCallback, errorCallback);
		}

		function IsAllowedByStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/isallowedbystep', param).then(successCallback, errorCallback);
		}

		function IsAllowedEdit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/isallowededit', param).then(successCallback, errorCallback);
		}

		function CancelTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/canceltender', param).then(successCallback, errorCallback);
		}

		function ReviseOE(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/tender/reviseOE', param).then(successCallback, errorCallback);
		}

		function IsOERevision(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/tender/isOERevision', param).then(successCallback, errorCallback);
		}

		function StepIsNotStarted(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/stepisnotstarted', param).then(successCallback, errorCallback);
		}

		function StepHasEnded(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tender/stephasended', param).then(successCallback, errorCallback);
		}

		function getEvaluator(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/tender/getEvaluator", param).then(successCallback, errorCallback);
		}
	}
})();