﻿(function () {
    'use strict';

    angular.module("app").factory("PPPrequalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            GetAll: GetAll,
            selectPrequalStep: selectPrequalStep,
            prequalAssessmentData: prequalAssessmentData,
            insertAssessmentDate: insertAssessmentDate,
            updateAssessmentDate: updateAssessmentDate,
            uploadAssessmentFile: uploadAssessmentFile,
            assessmentReport:assessmentReport
        }
        return service;

        // implementation
        function selectPrequalStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/GetStepEntryPrequal", param).then(successCallback, errorCallback);
        }
        function GetAll(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vendorEntryPrequal", param).then(successCallback, errorCallback);
        }
        function prequalAssessmentData(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalAssessment/prequalAssessmentData", param).then(successCallback, errorCallback);
        }
        function insertAssessmentDate(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalAssessment/insertAssessmentDate", param).then(successCallback, errorCallback);
        }
        function updateAssessmentDate(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalAssessment/updateAssessmentDate", param).then(successCallback, errorCallback);
        }
        function uploadAssessmentFile(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalAssessment/uploadAssessmentFile", param).then(successCallback, errorCallback);
        }
        function assessmentReport(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vendorEntryPrequalAdmin/assessmentReport", param).then(successCallback, errorCallback);
        }
    }
})();