﻿(function () {
    'use strict';

    angular.module("app")
        .factory("RankingRiskService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            rankingRisk: rankingRisk,
            datavendor:datavendor,
            weekByYear: weekByYear
        };
        return service;

        // implementation

        function rankingRisk(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/rankingRisk", param).then(successCallback, errorCallback);
        }
        function datavendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/rankingRisk/datavendor", param).then(successCallback, errorCallback);
        }
        function weekByYear(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstWeek/weekByYear", param).then(successCallback, errorCallback);
        }
    }
})();