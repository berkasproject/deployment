(function () {
	'use strict';

	angular.module("app").factory("PendaftaranLelangService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
			SelectTender: SelectTender,
			UpdateUrl: UpdateUrl,
			getDataStepTender: getDataStepTender,
			sendToApproval: sendToApproval,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			detailApproval: detailApproval,
			SelectTenderInterest: SelectTenderInterest,
			approvePendaftaran: approvePendaftaran,
			SelectTenderInterestReview: SelectTenderInterestReview,
			reviewTenderInterest: reviewTenderInterest,
			sendApprovalTenderInterest: sendApprovalTenderInterest
		};

		return service;

		// implementation
		function SelectTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderRegistrationDetail/selectAll", param).then(successCallback, errorCallback);
		}
		function SelectTenderInterest(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderRegistrationDetail/selectInterest", param).then(successCallback, errorCallback);
		}
		function UpdateUrl(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderregister/UpdateUrl", param).then(successCallback, errorCallback);
		}
		function getDataStepTender(param, successCallback, errorCallback) {
			//{ID: vm.IDStepTender}
			GlobalConstantService.post(endpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderRegistrationDetail/sendToApproval', param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderRegistrationDetail/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderRegistrationDetail/isApprovalSent', param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tenderRegistrationDetail/detailApproval', param).then(successCallback, errorCallback);
		}
		function approvePendaftaran(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderRegistrationDetail/approvePendaftaran", param).then(successCallback, errorCallback);
		}
		function SelectTenderInterestReview(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderRegistrationDetail/selectInterestReview", param).then(successCallback, errorCallback);
		}
		function reviewTenderInterest(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderRegistrationDetail/reviewTenderInterest", param).then(successCallback, errorCallback);
		}
		function sendApprovalTenderInterest(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tenderRegistrationDetail/sendApprovalTenderInterest", param).then(successCallback, errorCallback);
		}
	}
})();