﻿(function () {
    'use strict';

    angular.module("app").factory("ClarificationChatAdminService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            GetStepInfo: GetStepInfo,
            GetVendorInfo: GetVendorInfo,
            GetVendors: GetVendors,
            GetChats: GetChats,
            AddChat: AddChat,
            IsAllowed: IsAllowed,
            SendMailToVendor: SendMailToVendor
        };

        return service;

        function GetStepInfo(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/getstepinfo", param).then(successCallback, errorCallback);
        }
        function GetVendorInfo(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/getvendorinfo", param).then(successCallback, errorCallback);
        }
        function GetVendors(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/getvendors", param).then(successCallback, errorCallback);
        }
        function GetChats(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/getchats", param).then(successCallback, errorCallback);
        }
        function AddChat(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/addchat", param).then(successCallback, errorCallback);
        }
        function IsAllowed(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/isallowed", param).then(successCallback, errorCallback);
        }
        function SendMailToVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/sendmailtovendor", param).then(successCallback, errorCallback);
        }
    }
})();