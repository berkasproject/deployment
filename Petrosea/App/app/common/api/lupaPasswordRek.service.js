﻿(function () {
    'use strict';

    angular.module("app")
    .factory("LupaPasswordService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            save: save
        };

        return service;

        // implementation
        function save(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/lupaPassword", param).then(successCallback, errorCallback);
        }
    }
})();