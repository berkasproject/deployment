﻿(function () {
	'use strict';

	angular.module("app").factory("AsosiasiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
		    select: select,
		    editActive: editActive,
		    cekData: cekData,
		    insert: insert,
		    update: update,
		    getProvince: getProvince,
		    getCities: getCities,
            getDistrict: getDistrict
		};

		return service;

		// implementation
		function select(param, successCallback, errorCallback) {
		    console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/association/search", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
		    //console.info("serv:" + JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/association/remove", param).then(successCallback, errorCallback);
		}

		function cekData(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/association/cek", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/association/create", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/association/update", param).then(successCallback, errorCallback);
		}

		function getProvince(country, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/states/" + country).then(successCallback, errorCallback);
		}

		function getCities(state, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/cities/" + state).then(successCallback, errorCallback);
		}

		function getDistrict(city, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/districts/" + city).then(successCallback, errorCallback);
		}
	}
})();