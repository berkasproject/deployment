(function () {
	'use strict';

	angular.module("app").factory("PengurusPerusahaanPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var apipoint = GlobalConstantService.getConstant("api_endpoint");

		// interfaces
		var service = {
		    selectContact: selectContact,
		    loadPrequalStep: loadPrequalStep,
		    GetByVendor: GetByVendor,
		    EditSingle: EditSingle,
		    CreateSingle: CreateSingle,
		    Delete: Delete,
            Submit: Submit
		};

		return service;

	    // implementation
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/companyPersonPrequal/Submit", param).then(successCallback, errorCallback);
		}
		function Delete(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/companyPersonPrequal/delete", param).then(successCallback, errorCallback);
		}
		function CreateSingle(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/companyPersonPrequal/createsingle", param).then(successCallback, errorCallback);
		}
		function EditSingle(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/companyPersonPrequal/EditCompanyPersonPrequal", param).then(successCallback, errorCallback);
		}
		function selectContact(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorPrequalEntry/loadVendorContact", param).then(successCallback, errorCallback);
		}
		function loadPrequalStep(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
		}

		function GetByVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/companyPersonPrequal", param).then(successCallback, errorCallback);
		}
		
	}
})();