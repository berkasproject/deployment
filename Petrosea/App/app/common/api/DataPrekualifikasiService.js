﻿(function () {
    'use strict';

    angular.module("app").factory("DataPraqualService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            selectbyid: selectbyid,
            getstepbyid: getstepbyid,
            getstatus: getstatus,
            configstep: configstep
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/prequalData/select", param).then(successCallback, errorCallback);
        }

        function selectbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/prequalData/getById", param).then(successCallback, errorCallback);
        }

        function getstepbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/prequalData/getStepById", param).then(successCallback, errorCallback);
        }

        function getstatus(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/prequalData/getStatusOption").then(successCallback, errorCallback);
        }

        function configstep(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/prequalData/configStep", param).then(successCallback, errorCallback);
        }
    }
})();