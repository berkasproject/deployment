﻿(function () {
	'use strict';

	angular.module("app").factory("AssessmentPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			select: select,
			selectStep: selectStep,
			selectDetailEntry: selectDetailEntry,
			loadContact: loadVendorContact,
			loadCurrency: loadVendorCurrency,
			loadCommodity: loadVendorCommodity,
			loadContactPrequal: loadVendorContactPrequal,
			loadCurrencyPrequal: loadVendorCurrencyPrequal,
			loadCommodityPrequal: loadVendorCommodityPrequal,
			InsertSubmit: InsertSubmit,
			loadLicensePrequal: loadLicensePrequal,
			loadLicense: loadLicense,
			loadStockPrequal: loadStockPrequal,
			loadStock: loadStock,
			loadLegalPrequal: loadLegalPrequal,
			loadLegal: loadLegal,
			loadCompPers: loadCompPers,
			loadCompPersPrequal: loadCompPersPrequal,
			loadBalance: loadBalance,
			loadBalancePrequal: loadBalancePrequal,
			loadBankDetail: loadBankDetail,
			loadBankDetailPrequal: loadBankDetailPrequal,
			balanceDocUrl: balanceDocUrl,
			balanceDocUrlPrequal: balanceDocUrlPrequal,
			EditAdministrasi: EditAdministrasi,
			UpdateLicensi: UpdateLicensi,
			UpdateStock: UpdateStock,
			isAnotherStockHolder: isAnotherStockHolder,
			UpdateLegal: UpdateLegal,
			GetPositionTypes: GetPositionTypes,
			UpdateCompanyPersonPrequal: UpdateCompanyPersonPrequal,
			SelectSetupStep: SelectSetupStep,
			SaveAss: SaveAss,
			selectBuilding: selectBuilding,
			selectBuildingPrequal: selectBuildingPrequal,
			SelectExp: SelectExp,
			SelectExpPrequal: SelectExpPrequal,
			UpdateExp: UpdateExp,
			SelectExpert: SelectExpert,
			SelectExpertPrequal: SelectExpertPrequal,
			updateExpert: updateExpert,
			selectCertificate: selectCertificate,
			selectCertificatePrequal: selectCertificatePrequal,
			selectEquipmentPrequal: selectEquipmentPrequal,
			selectEquipment: selectEquipment,
			selectEquipmentToolsPrequal: selectEquipmentToolsPrequal,
			selectEquipmentTools: selectEquipmentTools,
			updateBuilding: updateBuilding,
			updateNonBuilding: updateNonBuilding,
			listSummary: listSummary,
			updateSummary: updateSummary,
			getallnationalities: getallnationalities,
			updateVendorExpertsCertificatePrequal: updateVendorExpertsCertificatePrequal,
			getCurrencies: getCurrencies,
			getstates: getstates,
			loadDokumenCSMS: loadDokumenCSMS,
			loadDokumenLainLain: loadDokumenLainLain,
			loadCommodityPrequal: loadCommodityPrequal,
			insertDocCSMS: insertDocCSMS,
			updateOtherDoc: updateOtherDoc,
			CekAgree: CekAgree,
			load: load,
			all: all,
			All: All,
			AllMain: AllMain,
			insertAgree: insertAgree,
			selectCek: selectCek,
			selectCek2: selectCek2,
			selectData: selectData,
			selectNamaDir: selectNamaDir,
			selectDocLibrary: selectDocLibrary,
			selectUrlAgree: selectUrlAgree,
			selectUrlBConduct: selectUrlBConduct,
			selectUrlBConductEN: selectUrlBConductEN,
			selectUrlKuesioner: selectUrlKuesioner,
			selectContactID: selectContactID,
			selectAddressID: selectAddressID,
			selectAddress: selectAddress,
			selectCountryID: selectCountryID,
			selectCountry: selectCountry,
			updateAgree: updateAgree,
			insertDoc: insertDoc,
			updateDoc: updateDoc,
			insertDoc2: insertDoc2,
			loadCatalogueEquipment: loadCatalogueEquipment,
			insertBalance: insertBalance,
			updateBalance: updateBalance,
			deleteBalance: deleteBalance,
			balanceDocUrl: balanceDocUrl,
			SelectBusinessFieldPrequal: SelectBusinessFieldPrequal,
			InsertBusinessField: InsertBusinessField,
			deleteBusinessField: deleteBusinessField,
			loadST: loadST,
			CekVendorPrequal: CekVendorPrequal,
			selectQuestionnairePrequal: selectQuestionnairePrequal,
			insertDocST: insertDocST,
			saveQuestionairePrequal: saveQuestionairePrequal,
			saveQuestionaireUrl: saveQuestionaireUrl,
			insertOtherDoc: insertOtherDoc,
			removeOtherDoc: removeOtherDoc,
			insertExpert: insertExpert,
			editActiveExpert: editActiveExpert,
			insertExpertCertificate: insertExpertCertificate,
			deleteLicensePrequal: deleteLicensePrequal,
			editCertificateActive: editCertificateActive,
			DeleteVendorStock: DeleteVendorStock,
			CreateLegalDocPrequal: CreateLegalDocPrequal,
			DeleteLegalDocPrequal: DeleteLegalDocPrequal,
			insertBankDetailPrequal: insertBankDetailPrequal,
			updateBankDetailPrequal: updateBankDetailPrequal,
			removeBankDetailPrequal: removeBankDetailPrequal,
			insertBuilding: insertBuilding,
			insertNonBuilding: insertNonBuilding,
			deleteBuilding: deleteBuilding,
			deleteNonBuilding: deleteNonBuilding,
			InsertExp: InsertExp,
			deleteExp: deleteExp,
			AddCompanyPersonPrequal: AddCompanyPersonPrequal,
			deletePerson: deletePerson,
			LoadAssessmentReport: LoadAssessmentReport,
			GetAddressID: GetAddressID,
			getBusinessField: getBusinessField,
			checkIsNotPassed: checkIsNotPassed,
			verifyBusinessField: verifyBusinessField,
            insertSingleCommodity:insertSingleCommodity,
            loadDDQuest:loadDDQuest

		}
		return service;

	    // implementation
		function LoadAssessmentReport(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/assessmentReport", param).then(successCallback, errorCallback);
		}
		function deletePerson(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/deletePerson", param).then(successCallback, errorCallback);
		}
		function AddCompanyPersonPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/AddCompanyPersonPrequal", param).then(successCallback, errorCallback);
		}
		function deleteExp(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/deleteExp", param).then(successCallback, errorCallback);
		}
		function deleteNonBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/deleteNonBuilding", param).then(successCallback, errorCallback);
		}
		function deleteBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/deleteBuilding", param).then(successCallback, errorCallback);
		}
		function insertNonBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertNonBuilding", param).then(successCallback, errorCallback);
		}
		function insertBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertBuilding", param).then(successCallback, errorCallback);
		}
		function removeBankDetailPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/removeBankDetailPrequal", param).then(successCallback, errorCallback);
		}
		function updateBankDetailPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateBankDetailPrequal", param).then(successCallback, errorCallback);
		}
		function insertBankDetailPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertBankDetailPrequal", param).then(successCallback, errorCallback);
		}
		function DeleteLegalDocPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/DeleteVendorLegalDocumentPrequal", param).then(successCallback, errorCallback);
		}
		function CreateLegalDocPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/CreatevendorlegalDocumentPrequal", param).then(successCallback, errorCallback);
		}
		function DeleteVendorStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/DeleteVendorStock", param).then(successCallback, errorCallback);
		}
		function editCertificateActive(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/vendorExpertsCertificatePrequal/updateActive", param).then(successCallback, errorCallback);
		}
		function deleteLicensePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/deleteLicensePrequal", param).then(successCallback, errorCallback);
		}
		function insertExpertCertificate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/vendorExpertsCertificatePrequal/create", param).then(successCallback, errorCallback);
		}
		function editActiveExpert(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateActiveExpert", param).then(successCallback, errorCallback);
		}
		function insertExpert(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertExpert", param).then(successCallback, errorCallback);
		}
		function removeOtherDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/RemoveOtherDoc", param).then(successCallback, errorCallback);
		}
		function insertOtherDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertOtherDoc", param).then(successCallback, errorCallback);
		}
		function saveQuestionaireUrl(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UploadQuestionnaire", param).then(successCallback, errorCallback);
		}
		function saveQuestionairePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/SaveQuestionairePrequal", param).then(successCallback, errorCallback);
		}
		function insertDocST(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/vendorAgreementpq/insertDoc", param).then(successCallback, errorCallback);
		}
		function selectQuestionnairePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/mstquestionairePrequal", param).then(successCallback, errorCallback);
		}
		function CekVendorPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/check-vendor-prequal", param).then(successCallback, errorCallback);
		}
		function loadST(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/vendorAgreementpq/load", param).then(successCallback, errorCallback);
		}
		function getBusinessField(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getBusinessField", param).then(successCallback, errorCallback);
		}
		function checkIsNotPassed(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/checkIsNotPassed", param).then(successCallback, errorCallback);
		}
		function deleteBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/deleteBusinessField", param).then(successCallback, errorCallback);
		}
		function verifyBusinessField(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/verifyBusinessField", param).then(successCallback, errorCallback);
		}
		function InsertBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/InsertBusinessFieldPrequal", param).then(successCallback, errorCallback);
		}
		function SelectBusinessFieldPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/businessField/goodsorservice", param).then(successCallback, errorCallback);
		}
		function updateNonBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateNonBuilding", param).then(successCallback, errorCallback);
		}
		function updateBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateBuilding", param).then(successCallback, errorCallback);
		}
		function selectEquipmentTools(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectVendorEquipmentTools", param).then(successCallback, errorCallback);
		}
		function selectEquipmentToolsPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectVendorEquipmentToolsPrequal", param).then(successCallback, errorCallback);
		}
		function selectEquipment(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectVendorEquipment", param).then(successCallback, errorCallback);
		}
		function selectEquipmentPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectVendorEquipmentPrequal", param).then(successCallback, errorCallback);
		}
		function selectCertificatePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectExpertCertificatePrequal", param).then(successCallback, errorCallback);
		}
		function selectCertificate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectExpertCertificate", param).then(successCallback, errorCallback);
		}
		function updateExpert(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateExpert", param).then(successCallback, errorCallback);
		}
		function SelectExpert(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectExpert", param).then(successCallback, errorCallback);
		}
		function SelectExpertPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectExpertPrequal", param).then(successCallback, errorCallback);
		}
		function InsertExp(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/InsertExp", param).then(successCallback, errorCallback);
		}
		function UpdateExp(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateExp", param).then(successCallback, errorCallback);
		}
		function SelectExp(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectExperience", param).then(successCallback, errorCallback);
		}
		function SelectExpPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectExperiencePrequal", param).then(successCallback, errorCallback);
		}
		function selectBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectVendorBuilding", param).then(successCallback, errorCallback);
		}
		function selectBuildingPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectVendorBuildingPrequal", param).then(successCallback, errorCallback);
		}
		function SaveAss(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/SaveAss", param).then(successCallback, errorCallback);
		}
		function SelectSetupStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/SelectSetupStep", param).then(successCallback, errorCallback);
		}
		function UpdateCompanyPersonPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateCompanyPersonPrequal", param).then(successCallback, errorCallback);
		}
		function GetPositionTypes(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/CompPers/getPositionTypes").then(successCallback, errorCallback);
		}
		function UpdateLegal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateVendorlegalDocumentPrequal", param).then(successCallback, errorCallback);
		}
		function isAnotherStockHolder(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/isAnotherStockHolder", param).then(successCallback, errorCallback);
		}
		function UpdateStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdatetVendorStock", param).then(successCallback, errorCallback);
		}
		function UpdateLicensi(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateLicense", param).then(successCallback, errorCallback);
		}
		function EditAdministrasi(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateAdministrasiPrequal", param).then(successCallback, errorCallback);
		}
		function loadBankDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadBankDetail", param).then(successCallback, errorCallback);
		}
		function loadBankDetailPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadBankDetailPrequal", param).then(successCallback, errorCallback);
		}
		function balanceDocUrl(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/balanceDocUrl", param).then(successCallback, errorCallback);
		}
		function balanceDocUrlPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/balanceDocUrlPrequal", param).then(successCallback, errorCallback);
		}
		function loadBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadBalance", param).then(successCallback, errorCallback);
		}
		function loadBalancePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadBalancePrequal", param).then(successCallback, errorCallback);
		}
		function loadCompPers(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadCompPers", param).then(successCallback, errorCallback);
		}
		function loadCompPersPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadCompPersPrequal", param).then(successCallback, errorCallback);
		}
		function loadLegal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadLegal", param).then(successCallback, errorCallback);
		}
		function loadLegalPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadLegalPrequal", param).then(successCallback, errorCallback);
		}
		function loadStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadStock", param).then(successCallback, errorCallback);
		}
		function loadStockPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadStockPrequal", param).then(successCallback, errorCallback);
		}
		function loadLicense(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadLicense", param).then(successCallback, errorCallback);
		}
		function loadLicensePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadLicensePrequal", param).then(successCallback, errorCallback);
		}
		function InsertSubmit(param, successCallback, errorCallback) {
			//console.info("param" + JSON.stringify(param));
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/InsertSubmit", param).then(successCallback, errorCallback);
		}
		function loadVendorCommodityPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadVendorCommodityPrequal", param).then(successCallback, errorCallback);
		}
		function loadVendorCurrencyPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadVendorCurrencyPrequal", param).then(successCallback, errorCallback);
		}
		function loadVendorContactPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadVendorContactPrequal", param).then(successCallback, errorCallback);
		}
		function loadVendorCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadVendorCommodity", param).then(successCallback, errorCallback);
		}
		function loadVendorCurrency(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadVendorCurrency", param).then(successCallback, errorCallback);
		}
		function loadVendorContact(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadVendorContact", param).then(successCallback, errorCallback);
		}
		function selectDetailEntry(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectDetail", param).then(successCallback, errorCallback);
		}
		function selectStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/selectStep", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal", param).then(successCallback, errorCallback);
		}
		function listSummary(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/listSummary", param).then(successCallback, errorCallback);
		}
		function updateSummary(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateSummary", param).then(successCallback, errorCallback);
		}
		function getallnationalities(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getallnationalities").then(successCallback, errorCallback);
		}
		function loadCatalogueEquipment(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadCatalogueEquipment").then(successCallback, errorCallback);
		}
		function updateVendorExpertsCertificatePrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateVendorExpertsCertificatePrequal", param).then(successCallback, errorCallback);
		}
		function getCurrencies(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getCurrency", param).then(successCallback, errorCallback);
		}
		function getstates(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getstates").then(successCallback, errorCallback);
		}
		function loadDokumenCSMS(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadDokumenCSMS", param).then(successCallback, errorCallback);
		}
		function loadDokumenLainLain(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadDokumenLainLain", param).then(successCallback, errorCallback);
		}
		function loadCommodityPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadCommodity", param).then(successCallback, errorCallback);
		}
		function insertDocCSMS(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertDocCSMS", param).then(successCallback, errorCallback);
		}
		function updateOtherDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/updateOtherDoc", param).then(successCallback, errorCallback);
		}

		function CekAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/CekAgree", param).then(successCallback, errorCallback);
		}
		function load(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/load", param).then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(adminpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function All(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/GetAllByVendorID", param).then(successCallback, errorCallback);
		}
		function AllMain(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/SelectAgree", param).then(successCallback, errorCallback);
		}
		function insertAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertAgree", param).then(successCallback, errorCallback);
		}
		function selectCek(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/Cek", param).then(successCallback, errorCallback);
		}
		function selectCek2(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/Cek2", param).then(successCallback, errorCallback);
		}
		function selectData(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/SelectData", param).then(successCallback, errorCallback);
		}
		function selectNamaDir(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getDirNameByVendorID", param).then(successCallback, errorCallback);
		}
		function selectDocLibrary(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getDocUrlLibrary", param).then(successCallback, errorCallback);
		}
		function selectUrlAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getDocUrlAgreement", param).then(successCallback, errorCallback);
		}
		function selectUrlBConduct(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getDocUrlBConduct", param).then(successCallback, errorCallback);
		}
		function selectUrlBConductEN(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getDocUrlBConductEN", param).then(successCallback, errorCallback);
		}
		function selectUrlKuesioner(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getDocUrlKuesioner", param).then(successCallback, errorCallback);
		}
		function selectLegalDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getLegalDocByVendorID", param).then(successCallback, errorCallback);
		}
		function selectContactID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getContactID", param).then(successCallback, errorCallback);
		}
		function selectAddressID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getAddressID", param).then(successCallback, errorCallback);
		}
		function selectAddress(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getAddress", param).then(successCallback, errorCallback);
		}
		function selectCountryID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getCountryID", param).then(successCallback, errorCallback);
		}
		function selectCountry(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getCountry", param).then(successCallback, errorCallback);
		}
		function updateAgree(param, successCallback, errorCallback) {
			//console.info("wdqqwweeqrggg");
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateAgree", param).then(successCallback, errorCallback);
		}
		function insertDoc(param, successCallback, errorCallback) {
			//console.info("awdawdawd");
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertDoc", param).then(successCallback, errorCallback);
		}
		function updateDoc(param, successCallback, errorCallback) {
			//console.info("awdawdawdawdawd");
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateDoc", param).then(successCallback, errorCallback);
		}
		function insertDoc2(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertDoc2", param).then(successCallback, errorCallback);
		}
		function insertBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/InsertBalance", param).then(successCallback, errorCallback);
		}
		function updateBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/UpdateBalance", param).then(successCallback, errorCallback);
		}
		function deleteBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/DeleteBalance", param).then(successCallback, errorCallback);
		}
		function balanceDocUrl(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/AssessmentPrequal/balanceDocUrl", param).then(successCallback, errorCallback);
		}
		function GetAddressID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/getAddressID", param).then(successCallback, errorCallback);
		}
		function insertSingleCommodity(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/insertSingleCommodity", param).then(successCallback, errorCallback);
		}
		function loadDDQuest(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/AssessmentPrequal/loadDDQuest", param).then(successCallback, errorCallback);
		}
	}
})();