﻿(function () {
	'use strict'

	angular.module("app").factory("mstLicenseService", dataService)

	dataService.$inject = ['$http', '$q', 'GlobalConstantService']

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint")
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint")
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint")

		// interfaces
		var service = {
			getLicense: getLicense,
			inactivate: inactivate,
			saveLicense: saveLicense,
			editLicense: editLicense
		}

		return service

		function getLicense(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/license/getLicense", param).then(successCallback, errorCallback)
		}

		function inactivate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/license/inactivate", param).then(successCallback, errorCallback)
		}

		function saveLicense(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/license/saveLicense", param).then(successCallback, errorCallback)
		}

		function editLicense(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/license/editLicense", param).then(successCallback, errorCallback)
		}
	}
})()