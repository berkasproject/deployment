﻿(function () {
	'use strict';

	angular.module("app").factory("AuctionService", dataService);

	dataService.$inject = ['GlobalConstantService'];
	/* @ngInject */
	function dataService(GlobalConstantService) {
	    var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
	    var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
		    getAuction: getAuction,
		    getAuctionID: getAuctionID,
		    getAuctionDetail: getAuctionDetail,
		    getAuctionVendorDetail:getAuctionVendorDetail,
		    selectByUsername: selectByUsername,
		    getEvaluationID: getEvaluationID,
		    getHPS: getHPS,
		    saveMinimumPrice: saveMinimumPrice,
		    saveBidPrice:saveBidPrice,
		    getDataStepTender: getDataStepTender,
		    joinAuction: joinAuction,
		    inputSummary: inputSummary,
		    publishSummary: publishSummary,
		    getBiddingInformation: getBiddingInformation,
		    getBestBid: getBestBid,
		    getGOEDetails: getGOEDetails,
		    getVOEDetails: getVOEDetails
		}
		return service;

		// implementation
		function getEvaluationID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/getTahapanEvaluasiPenawaranID", param).then(successCallback, errorCallback);
		}
		function getHPS(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/getHPS", param).then(successCallback, errorCallback);
		} 
		function saveMinimumPrice(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/saveMinimumPrice", param).then(successCallback, errorCallback);
		}
		function saveBidPrice(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/saveBidPrice", param).then(successCallback, errorCallback);
		}
		function getAuction(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/selectAuction", param).then(successCallback, errorCallback);
		}
		function getBestBid(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/selectBestBid", param).then(successCallback, errorCallback);
		}
		function getAuctionDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/selectAuctionDetail", param).then(successCallback, errorCallback);
		}
		function getAuctionVendorDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/selectAuctionVendorDetail", param).then(successCallback, errorCallback);
		}
		function getAuctionID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/getAuctionID", param).then(successCallback, errorCallback);
		}
		function selectByUsername(successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/selectByUsername").then(successCallback, errorCallback);
		}
		function getDataStepTender(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
		} 
		function joinAuction(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/joinAuction", param).then(successCallback, errorCallback);
		}
		function inputSummary(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/inputSummary", param).then(successCallback, errorCallback);
		}
		function publishSummary(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/publishSummary", param).then(successCallback, errorCallback);
		}
		function getBiddingInformation(successCallback, errorCallback) {
		    var param = { Keyword: "BID_AUCTION" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getGOEDetails(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/getGOEDetails", param).then(successCallback, errorCallback);
		}
		function getVOEDetails(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/auction/getVOEDetails", param).then(successCallback, errorCallback);
		}
	}
})();