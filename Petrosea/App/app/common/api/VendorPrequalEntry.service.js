﻿(function () {
    'use strict';

    angular.module("app").factory("VendorPrequalEntryService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        var Vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var Publicendpoint = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            select: select,
            loadPrequalStep: loadPrequalStep,
            UpdateSubmit: UpdateSubmit,
            loadDataVendorPrequal: loadDataVendorPrequal,
            insertDocCSMS: insertDocCSMS,
            CekVendorEntryPrequal: CekVendorEntryPrequal,
            updateAssessmentSubmitDate:updateAssessmentSubmitDate
        };

        return service;
        function CekVendorEntryPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(Vendorendpoint + "/vendorPrequalEntry/CekVendorEntry", param).then(successCallback, errorCallback);
        }
        function insertDocCSMS(param, successCallback, errorCallback) {
            GlobalConstantService.post(Vendorendpoint + "/vendorPrequalEntry/insertDocCSMS", param).then(successCallback, errorCallback);
        }
        function loadDataVendorPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(Vendorendpoint + "/vendorPrequalEntry/loadDataVendorPrequal", param).then(successCallback, errorCallback);
        }
        function UpdateSubmit(param, successCallback, errorCallback) {
            GlobalConstantService.post(Vendorendpoint + "/vendorPrequalEntry/UpdateSubmit", param).then(successCallback, errorCallback);
        }
        function loadPrequalStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(Vendorendpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
        	GlobalConstantService.post(Vendorendpoint + '/vendorPrequalEntry', param).then(successCallback, errorCallback);
        }
        function updateAssessmentSubmitDate(param, successCallback, errorCallback) {
            GlobalConstantService.post(Vendorendpoint + "/vendorPrequalEntry/updateAssessmentSubmitDate", param).then(successCallback, errorCallback);
        }
    }
})();