(function () {
    'use strict';

    angular.module("app").factory("PlantService", dataService);

    dataService.$inject = ['GlobalConstantService'];
    /* @ngInject */
    function dataService(GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            save: save,
            saveAktif: saveAktif
        };

        return service;

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/plant/select", param).then(successCallback, errorCallback);
        }

        function save(param, successCallback, errorCallback) {
            if(param.create == 1) {
                GlobalConstantService.post(endpoint + "/plant/insert", param).then(successCallback, errorCallback); 
            } else if(param.create == 0) {
                GlobalConstantService.post(endpoint + "/plant/update", param).then(successCallback, errorCallback);
            }
        }

        function saveAktif(param, successCallback, errorCallback) {
            if(param.nonAktif == 1) {
                GlobalConstantService.post(endpoint + "/plant/nonAktif", param).then(successCallback, errorCallback); 
            } else if(param.nonAktif == 0) {
                GlobalConstantService.post(endpoint + "/plant/aktif", param).then(successCallback, errorCallback);
            }
        }
    }
})()