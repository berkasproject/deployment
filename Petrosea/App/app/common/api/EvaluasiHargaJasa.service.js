﻿(function () {
	'use strict';

	angular.module("app").factory("EvaluasiHargaJasaService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			getByTenderStepData: getByTenderStepData,
			getOfferPriceLimit: getOfferPriceLimit,
			getCurrency: getCurrency,
			getTotalCostEstimate: getTotalCostEstimate,
			saveEvaluation: saveEvaluation,
			getOfferEntryVendor: getOfferEntryVendor,
			getQuestionnaires: getQuestionnaires,
			isVariation: isVariation,
			isNoLimit: isNoLimit,
			getOEEVInfo: getOEEVInfo,
			getCESubOffer: getCESubOffer,
			getCELineOffer: getCELineOffer,
			checkTime: checkTime,
			checkTechnicalEvaluation: checkTechnicalEvaluation,
			isLess3Approved: isLess3Approved,
			isTechEvalApproved: isTechEvalApproved,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			sendToApproval: sendToApproval
		};

		return service;

		function getByTenderStepData(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getbytenderstepdata", param).then(successCallback, errorCallback);
		}

		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/sendToApproval", param).then(successCallback, errorCallback);
		}

		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/isNeedTenderStepApproval", param).then(successCallback, errorCallback);
		}

		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/isApprovalSent", param).then(successCallback, errorCallback);
		}

		function getOfferPriceLimit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getofferpricelimit", param).then(successCallback, errorCallback);
		}

		function getCurrency(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getcurrency", param).then(successCallback, errorCallback);
		}

		function getTotalCostEstimate(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/gettotalce", param).then(successCallback, errorCallback);
		}

		function saveEvaluation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/saveevaluation", param).then(successCallback, errorCallback);
		}

		function getOfferEntryVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getofferentryvendor", param).then(successCallback, errorCallback);
		}

		function getQuestionnaires(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getquestionnaires", param).then(successCallback, errorCallback);
		}

		function isVariation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/isvariation", param).then(successCallback, errorCallback);
		}

		function isNoLimit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/isnolimit", param).then(successCallback, errorCallback);
		}

		function getOEEVInfo(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getoevinfo", param).then(successCallback, errorCallback);
		}

		function getCESubOffer(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getcesuboffer", param).then(successCallback, errorCallback);
		}

		function getCELineOffer(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/getcelineoffer", param).then(successCallback, errorCallback);
		}

		function checkTime(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/checktime", param).then(successCallback, errorCallback);
		}

		function checkTechnicalEvaluation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/checktechnicalevaluation", param).then(successCallback, errorCallback);
		}

		function isLess3Approved(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/isless3approved", param).then(successCallback, errorCallback);
		}

		function isTechEvalApproved(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/soevaluation/istechevalapproved", param).then(successCallback, errorCallback);
		}
	}
})();