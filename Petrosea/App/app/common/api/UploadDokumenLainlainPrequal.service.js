(function () {
    'use strict';

    angular.module("app").factory("UploadDokumenLainlainPrequalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            load: load,
            Select: Select,
            insert: insert,
            Update: Update,
            remove: remove,
            SelectbyVEPID: SelectbyVEPID,
            loadPrequalStep: loadPrequalStep,
            submit: submit,
            isUploaded:isUploaded
        };

        return service;

        // implementation
        function load(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumenpq/load", param).then(successCallback, errorCallback);
        }
        function Select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/select", param).then(successCallback, errorCallback);
        }
        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumenpq/insert", param).then(successCallback, errorCallback);
        }

        function Update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumenpq/update", param).then(successCallback, errorCallback);
        }

        function remove(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumenpq/remove", param).then(successCallback, errorCallback);
        }
        function SelectbyVEPID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/getByVEPID", param).then(successCallback, errorCallback);
        }
        function loadPrequalStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
        }
        function submit(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumenpq/submit", param).then(successCallback, errorCallback);
        }
        function isUploaded(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumenpq/isUploaded", param).then(successCallback, errorCallback);
        }


    }
})();