(function () {
	'use strict';

	angular.module("app").factory("VerifikasiDataService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			all: all,
			editActive: editActive,
			InsertActivedRejected: InsertActivedRejected,
			allVendorContact: allVendorContact,
			selectlicensi: selectlicensi,
			allcontact: allcontact,
			sendMail: sendMail,
			selectApproval: selectApproval,
			selectApprovalByVendor: selectApprovalByVendor,
			editApproval: editApproval,
			GetByVendor: GetByVendor,
			GetByVendorComPer: GetByVendorComPer,
			allTenagaahli: allTenagaahli,
			selectCertificate: selectCertificate,
			selectBuilding: selectBuilding,
			selectVehicle: selectVehicle,
			selectEquipment: selectEquipment,
			selectExperience: selectExperience,
			selectSaham: selectSaham,
			selectNeraca: selectNeraca,
			select: select,
			getClasification: getClasification,
			SelectVend: SelectVend,
			updateLicensi: updateLicensi,
			DeleteVendorStock: DeleteVendorStock,
			getStockTypes: getStockTypes,
			updateStock: updateStock,
			GetDetailVendor: GetDetailVendor,
			GetCities: GetCities,
			updateLegal: updateLegal,
			DeleteVendorLegal: DeleteVendorLegal,
			DeleteVendorBalance: DeleteVendorBalance,
			getUnit: getUnit,
			getAsset: getAsset,
			getCOA: getCOA,
			sendToSAP: sendToSAP,
			getSubCOA: getSubCOA,
			updateBalance: updateBalance,
			DeleteOtherDoc: DeleteOtherDoc,
			updateDoc: updateDoc,
			selectcontact: selectcontact,
			getCurrencies: getCurrencies,
			GetAllNationalities: GetAllNationalities,
			updateExperts: updateExperts,
			DeleteVendorExperts: DeleteVendorExperts,
			updateExpertCertificate: updateExpertCertificate,
			DeleteVendorExpertCertificate: DeleteVendorExpertCertificate,
			updateBuilding: updateBuilding,
			getCategoryBuilding: getCategoryBuilding,
			getOwnership: getOwnership,
			getConditionEq: getConditionEq,
			updateNonBuilding: updateNonBuilding,
			DeleteVendorBuilding: DeleteVendorBuilding,
			DeleteVendorEquipment: DeleteVendorEquipment,
			getCompanyScale: getCompanyScale,
			getTechnical: getTechnical,
			UpdateScaleBalance: UpdateScaleBalance,
			DeleteVendorLicensi: DeleteVendorLicensi,
			GetCurrencies: GetCurrencies,
			getUploadPrefix: getUploadPrefix,
			GetPositionTypes: GetPositionTypes,
			updateCompany: updateCompany,
			DeletePengurus: DeletePengurus,
			typeExperience: typeExperience,
			getTypeTender: getTypeTender,
			SelectBusinessField: SelectBusinessField,
			insert: insert,
			VendorCommodity: VendorCommodity,
			getUserLogin: getUserLogin,
			selectBankDetail: selectBankDetail,
			insertBankDetail: insertBankDetail,
			updateBankDetail: updateBankDetail,
			deleteBankDetail: deleteBankDetail,
			saveSAPCode: saveSAPCode,
			getCountries: getCountries,
			getRegions: getRegions,
			updateExperience: updateExperience,
			selectUrlStatementLetter: selectUrlStatementLetter,
			cekNpwp: cekNpwp,
			balanceDocUrl: balanceDocUrl,
			DocConduct: DocConduct,
			loadQuestionnaire: loadQuestionnaire,
			sendMailActived: sendMailActived,
			getActMail: getActMail,
			getAllCurrencies: getAllCurrencies,
			checkEmail: checkEmail,
			sendRegistrationApproval: sendRegistrationApproval,
			sendMailActiveOrNot: sendMailActiveOrNot,
			sendMailApprove: sendMailApprove,
			exportDataVendor: exportDataVendor,
			businessfieldValidation: businessfieldValidation,
			updateSAPCode: updateSAPCode,
			exportDataPerusahaan: exportDataPerusahaan,
			deleteInactiveVendor: deleteInactiveVendor,
			sendQuestionnaire: sendQuestionnaire,
			loadDDQuest: loadDDQuest,
			sendApprovalVendorCompliance: sendApprovalVendorCompliance,
			exportDataVendor2: exportDataVendor2,
			registerVendorManual: registerVendorManual,
			cekUsername: cekUsername,
			detailRegistrationApproval: detailRegistrationApproval,
			selectQuestionnaire: selectQuestionnaire,
			submitReject: submitReject,
			loadDDQuest: loadDDQuest,
			answerQuest: answerQuest
		};

		return service;
		function sendMailApprove(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/sendMailApprove", param).then(successCallback, errorCallback);
		}
		function sendMailActiveOrNot(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/sendMailActiveOrNot", param).then(successCallback, errorCallback);
		}
		function checkEmail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/administrasi-check-email", param).then(successCallback, errorCallback);
		}
		function getUploadPrefix(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/reference/upload-prefix").then(successCallback, errorCallback);
		}
		function getAllCurrencies(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/currencies").then(successCallback, errorCallback);
		}
		function getActMail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/getActivationMail", param).then(successCallback, errorCallback);
		}
		function sendMailActived(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/aktivasi/send-email", param).then(successCallback, errorCallback);
		}
		function loadQuestionnaire(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/loadQuestionnaire", param).then(successCallback, errorCallback);
		}
		function DocConduct(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/DocConduct", param).then(successCallback, errorCallback);
		}
		function balanceDocUrl(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/balanceDocUrl", param).then(successCallback, errorCallback);
		}
		function cekNpwp(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/check-npwp", param).then(successCallback, errorCallback);
		}
		function updateExperience(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/update", param).then(successCallback, errorCallback);
		}
		function getRegions(param, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/continents", param).then(successCallback, errorCallback);
		}
		function getCountries(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/countries").then(successCallback, errorCallback);

		}
		function saveSAPCode(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/verifiedvendor/saveSAP", param).then(successCallback, errorCallback);
		}

		function sendRegistrationApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/sendRegistrationApproval", param).then(successCallback, errorCallback);
		}

		function detailRegistrationApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/detailRegistrationApproval", param).then(successCallback, errorCallback);
		}

		function sendToSAP(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/sendToSAP", param).then(successCallback, errorCallback);
		}

		function insertBankDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/bankdetail/insert", param).then(successCallback, errorCallback);
		}
		function selectBankDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/bankdetail/select", param).then(successCallback, errorCallback);
		}
		function updateBankDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/bankdetail/update", param).then(successCallback, errorCallback);
		}
		function deleteBankDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/bankdetail/remove", param).then(successCallback, errorCallback);
		}


		function getUserLogin(successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/verifiedvendor/GetUserLogin").then(successCallback, errorCallback);
		}
		function SelectBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/businessField/goodsorservice", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/update-admin", param).then(successCallback, errorCallback);
		}
		function getTypeTender(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function typeExperience(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_EXPERIENCE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function DeletePengurus(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/DeletePengurus", param).then(successCallback, errorCallback);
		}
		function updateCompany(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/UpdateCompany", param).then(successCallback, errorCallback);
		}
		function GetPositionTypes(successCallback, errorCallback) {
			var param = { Keyword: "POSITION_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getUploadPrefix(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/reference/upload-prefix")
            .then(successCallback, errorCallback);
		}
		function GetCurrencies(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/vendor/registration/currency/list").then(successCallback, errorCallback);
		}
		function DeleteVendorLicensi(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verified-vendor/License/delete", param).then(successCallback, errorCallback);
		}
		function UpdateScaleBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/UpdateScaleBalance", param).then(successCallback, errorCallback);
		}
		function getCompanyScale(successCallback, errorCallback) {
			var param = { Keyword: "COMPANY_SCALE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getTechnical(successCallback, errorCallback) {
			var param = { Keyword: "TYPE_TECHNICAL" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function DeleteVendorEquipment(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/DeleteVendorEquipment", param).then(successCallback, errorCallback);
		}
		function DeleteVendorBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/DeleteVendorBuilding", param).then(successCallback, errorCallback);
		}
		function updateNonBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/equipment/update", param).then(successCallback, errorCallback);
		}
		function getConditionEq(successCallback, errorCallback) {
			var param = { Keyword: "CONDITION_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getOwnership(successCallback, errorCallback) {
			var param = { Keyword: "OWNERSHIP_STATUS" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getCategoryBuilding(successCallback, errorCallback) {
			var param = { Keyword: "BUILDING_CATEGORY" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function updateBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/building/update", param).then(successCallback, errorCallback);
		}
		function DeleteVendorExpertCertificate(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorExpertsCertificate/delete", param).then(successCallback, errorCallback);
		}
		function updateExpertCertificate(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorExpertsCertificate/update", param).then(successCallback, errorCallback);
		}
		function DeleteVendorExperts(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/Experts/Delete", param).then(successCallback, errorCallback);
		}
		function updateExperts(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorExperts/update", param).then(successCallback, errorCallback);
		}
		function GetAllNationalities(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/getallnationalities").then(successCallback, errorCallback);
		}
		function getCurrencies(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/getCurrencies", param).then(successCallback, errorCallback);
		}
		function selectcontact(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
		}
		function updateDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/update-Doc", param).then(successCallback, errorCallback);
		}
		function updateBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/update-Balance", param).then(successCallback, errorCallback);
		}
		function getSubCOA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getCOA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getAsset(successCallback, errorCallback) {
			var param = { Keyword: "WEALTH_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getUnit(successCallback, errorCallback) {
			var param = { Keyword: "UNIT_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function GetCities(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/legalDocument/getCities").then(successCallback, errorCallback);
		}
		function sendMail(mail, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/registration/send-email", mail)
            .then(successCallback, errorCallback);
		}
		function all(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/select", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/updateApproval", param).then(successCallback, errorCallback);
		}
		function editApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/updateActive", param).then(successCallback, errorCallback);
		}

		function InsertActivedRejected(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/insertdetail", param).then(successCallback, errorCallback);
		}

		function allVendorContact(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/selectcontact", param).then(successCallback, errorCallback);
		}

		function selectlicensi(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/license", param).then(successCallback, errorCallback);
		}
		function GetByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/legalDocument/getLegalDocumentByVendor", param).then(successCallback, errorCallback);
		}
		function GetByVendorComPer(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/companyPerson/getByVendorID", param).then(successCallback, errorCallback);
		}
		function allTenagaahli(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorExperts/selectByID", param).then(successCallback, errorCallback);
		}
		function selectCertificate(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorExpertsCertificate/select", param).then(successCallback, errorCallback);
		}
		function selectBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/Building/select", param).then(successCallback, errorCallback);
		}
		function selectVehicle(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vehicle/select", param).then(successCallback, errorCallback);
		}
		function selectEquipment(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/Equipment/select", param).then(successCallback, errorCallback);
		}
		function selectExperience(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorExperience/select", param).then(successCallback, errorCallback);
		}
		function selectSaham(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorStock/getByVendor", param).then(successCallback, errorCallback);
		}
		function selectNeraca(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/balance", param).then(successCallback, errorCallback);
		}
		function allcontact(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
		}
		function selectApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/selectApproval", param).then(successCallback, errorCallback);
		}
		function selectApprovalByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/selectApprovalByVendor", param).then(successCallback, errorCallback);
		}

		function SelectVend(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/dokumen/getByVendorID", param).then(successCallback, errorCallback);
		}
		function getClasification(successCallback, errorCallback) {
			var param = { Keyword: "COMPANY_SCALE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function updateLicensi(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/update-license", param).then(successCallback, errorCallback);
		}
		function DeleteVendorStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verified-vendor/vendorStock/delete", param).then(successCallback, errorCallback);
		}
		function getStockTypes(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/reference/stock-unit")
                .then(successCallback, errorCallback);
		}
		function updateStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/update-Stock", param).then(successCallback, errorCallback);
		}
		function GetDetailVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/legalDocument/getDetailVendor", param).then(successCallback, errorCallback);
		}
		function updateLegal(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/update-Legal", param).then(successCallback, errorCallback);
		}
		function DeleteVendorLegal(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/legalDocument/delete", param).then(successCallback, errorCallback);
		}
		function DeleteVendorBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/Balance/delete", param).then(successCallback, errorCallback);
		}
		function DeleteOtherDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/OtherDoc/delete", param).then(successCallback, errorCallback);
		}
		function VendorCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/vendorcommodity", param).then(successCallback, errorCallback);
		}
		function selectUrlStatementLetter(param, successCallback, errorCallback) {
			console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectUrlStatementLetter", param).then(successCallback, errorCallback);
		}
		function exportDataVendor(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/exportDataVendor").then(successCallback, errorCallback);
		}
		function businessfieldValidation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/businessfieldValidation", param).then(successCallback, errorCallback);
		}
		function updateSAPCode(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/updateSAPCode", param).then(successCallback, errorCallback);
		}
		function exportDataPerusahaan(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/exportDataPerusahaan").then(successCallback, errorCallback);
		}
		function deleteInactiveVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/deleteInactiveVendor", param).then(successCallback, errorCallback);
		}
		function sendQuestionnaire(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/sendQuestionnaire", param).then(successCallback, errorCallback);
		}
		function loadDDQuest(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/loadDDQuest", param).then(successCallback, errorCallback);
		}
		function selectQuestionnaire(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectQuestionnaire", param).then(successCallback, errorCallback);
		}
		function sendApprovalVendorCompliance(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/sendApprovalVendorCompliance", param).then(successCallback, errorCallback);
		}
		function exportDataVendor2(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/exportDataVendor2", param).then(successCallback, errorCallback);
		}
		function registerVendorManual(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/registerVendorManual", param).then(successCallback, errorCallback);
		}
		function cekUsername(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/cekUsername", param).then(successCallback, errorCallback);
		}
		function submitReject(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/submitReject", param).then(successCallback, errorCallback);
		}
		function loadDDQuest(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/loadDDQuest2", param).then(successCallback, errorCallback);
		}
		function answerQuest(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/answerQuest", param).then(successCallback, errorCallback);
		}
	}
})();