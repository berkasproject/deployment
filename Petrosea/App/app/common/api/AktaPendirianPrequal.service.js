﻿(function () {
    'use strict';

    angular.module("app").factory("AktaPendirianPrequalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            loadContact: loadContact,
            loadPrequalStep: loadPrequalStep,
            GetVendorStocks: GetVendorStocks,
            CreateVendorStock: CreateVendorStock,
            EditVendorStock: EditVendorStock,
            DeleteVendorStock: DeleteVendorStock,
            GetLegal: GetLegal,
            CreateLegalDoc: CreateLegalDoc,
            EditLegal: EditLegal,
            DeleteLegal: DeleteLegal,
            Submit: Submit,
            CekSubmit: CekSubmit,
            CekVendorEntryPrequal: CekVendorEntryPrequal,
            isAnotherStockHolder: isAnotherStockHolder
        }
        return service;

        // implementation
        function CekVendorEntryPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorLegalDocumentPrequal/CekVendorEntry", param).then(successCallback, errorCallback);
        }
        function CekSubmit(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorLegalDocumentPrequal/CekSubmit", param).then(successCallback, errorCallback);
        }
        function Submit(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorLegalDocumentPrequal/Submit", param).then(successCallback, errorCallback);
        }
        function DeleteLegal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorLegalDocumentPrequal/DeleteVendorLegalDocumentPrequal", param).then(successCallback, errorCallback);
        }
        function EditLegal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorlegalDocumentPrequal/EditVendorlegalDocumentPrequal", param).then(successCallback, errorCallback);
        }
        function GetLegal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorlegalDocumentPrequal", param).then(successCallback, errorCallback);
        }
        function CreateLegalDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorlegalDocumentPrequal/CreatevendorlegalDocumentPrequal", param).then(successCallback, errorCallback);
        }
        function DeleteVendorStock(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorStockPrequal/DeleteVendorStock", param).then(successCallback, errorCallback);
        }
        function EditVendorStock(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorStockPrequal/EditVendorStock", param).then(successCallback, errorCallback);
        }
        function CreateVendorStock(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorStockPrequal/CreateVendorStock", param).then(successCallback, errorCallback);
        }
        function GetVendorStocks(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorStockPrequal", param).then(successCallback, errorCallback);
        }
        function loadPrequalStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
        }
        function loadContact(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/loadVendorContact", param).then(successCallback, errorCallback);
        }
        function isAnotherStockHolder(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorLegalDocumentPrequal/isAnotherStockHolder", param).then(successCallback, errorCallback);
        }
    }
})();