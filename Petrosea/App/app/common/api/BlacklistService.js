﻿(function () {
    'use strict';

    angular.module("app").factory("BlacklistService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            GetBlacklist: GetBlacklist,
            editBlacklist: editBlacklist,
            SelectEmployeeVendor: SelectEmployeeVendor,
            SelectVendorStock: SelectVendorStock,
            SelectBlacklistByID: SelectBlacklistByID,
            insert: insert,
            insertDetail: insertDetail,
            InsertBlacklist: InsertBlacklist,
            SelectType: SelectType,
            selectblacklist: selectblacklist,
            selecthistoryblacklist: selecthistoryblacklist,
            selectvendor: selectvendor,
            cek: cek,
            selectApproval: selectApproval,
            editApproval: editApproval,
            selectdetailStock: selectdetailStock,
            selectdetailCompPers: selectdetailCompPers,
            selectdetailPerson: selectdetailPerson,
            getUserLogin: getUserLogin,
            GetCRApprovals: GetCRApprovals,
            BlacklistById: BlacklistById,
            sendApproval: sendApproval,
            UpdateBlacklist: UpdateBlacklist,
            sendEmail: sendEmail

        };

        return service;
        function sendEmail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/blacklist/SendMail", param).then(successCallback, errorCallback);
        }
        function UpdateBlacklist(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/blacklist/UpdateBlacklist", param).then(successCallback, errorCallback);
        }
        function sendApproval(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/SendApprove", param).then(successCallback, errorCallback);
        }
        function BlacklistById(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/ById", param).then(successCallback, errorCallback);
        }
        
        function GetCRApprovals(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/GetCRApprovals", param).then(successCallback, errorCallback);
        }
        function getUserLogin(successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/GetUserLogin").then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/select", param).then(successCallback, errorCallback);
        }

        function GetBlacklist(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/reference/blacklist").then(successCallback, errorCallback);

        }

        function editBlacklist(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/UpdateBlacklist", param).then(successCallback, errorCallback);
        }

        function SelectEmployeeVendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/employeevendor/getID", param).then(successCallback, errorCallback);
        }

        function SelectVendorStock(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/vendorstock/getID", param).then(successCallback, errorCallback);
        }

        function SelectBlacklistByID(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/question1/getID", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/insert", param).then(successCallback, errorCallback);
        }

        function insertDetail(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/insertDetail", param).then(successCallback, errorCallback);
        }

        function InsertBlacklist(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/insertblack", param).then(successCallback, errorCallback);
        }
        function SelectType( successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.get(endpoint + "/reference/blacklist").then(successCallback, errorCallback);
        }
        function selectblacklist(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/selectblacklist", param).then(successCallback, errorCallback);
        } 
        function selecthistoryblacklist(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/selecthistoryblacklist", param).then(successCallback, errorCallback);
        }
        function selectvendor(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/selectvendor", param).then(successCallback, errorCallback);
        }
        function cek(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/cek", param).then(successCallback, errorCallback);
        } 
        function selectApproval(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/selectApproval", param).then(successCallback, errorCallback);
        }
        function editApproval(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/editApproval", param).then(successCallback, errorCallback);
        }
        function selectdetailStock(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/SelectDetailStock", param).then(successCallback, errorCallback);
        }
        function selectdetailCompPers(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/SelectDetailCompPers", param).then(successCallback, errorCallback);
        }
        function selectdetailPerson(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/blacklist/SelectDetailPerson", param).then(successCallback, errorCallback);
        }    
    }
})();