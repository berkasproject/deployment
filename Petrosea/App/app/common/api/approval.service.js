﻿(function () {
	'use strict';

	angular.module("app").factory("ApprovalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {

		};

		return service;

		// implementation
		function selectByUser(param, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/approval/select/byuser").then(successCallback, errorCallback);
		}
	}
})();