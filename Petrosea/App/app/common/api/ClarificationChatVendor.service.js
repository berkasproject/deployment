﻿(function () {
    'use strict';

    angular.module("app").factory("ClarificationChatVendorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            GetStepInfo: GetStepInfo,
            IsAllowed: IsAllowed,
            GetChats: GetChats,
            AddChat: AddChat,
        };

        return service;

        function GetStepInfo(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/getstepinfo", param).then(successCallback, errorCallback);
        }
        function IsAllowed(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/isallowed", param).then(successCallback, errorCallback);
        }
        function GetChats(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/getchats", param).then(successCallback, errorCallback);
        }
        function AddChat(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/clarification/addchat", param).then(successCallback, errorCallback);
        }
    }
})();