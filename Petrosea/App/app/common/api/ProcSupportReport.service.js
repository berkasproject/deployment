(function () {
    'use strict';

    angular.module("app")
        .factory("ProcSupportReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            procSupportReport: procSupportReport,
            weekByYear:weekByYear
        };
        return service;

        // implementation

        function procSupportReport(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/procSupportReport", param).then(successCallback, errorCallback);
        }
        function weekByYear(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstWeek/weekByYear", param).then(successCallback, errorCallback);
        }
    }
})();