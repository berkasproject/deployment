﻿(function () {
	'use strict';

	angular.module('app').factory('MstPaymentTermService', dataService);

	dataService.$inject = ['GlobalConstantService'];

	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		var service = {
			selectPaymentTerm: selectPaymentTerm,
			getPaymentTerm: getPaymentTerm,
			savePaymentTerm: savePaymentTerm,
			ActivateInactivate: ActivateInactivate
		};

		return service;
		function selectPaymentTerm(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MstPaymentTerm/selectPaymentTerm', param).then(successCallback, errorCallback);
		}

		function getPaymentTerm(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MstPaymentTerm/getPaymentTerm', param).then(successCallback, errorCallback);
		}

		function savePaymentTerm(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MstPaymentTerm/savePaymentTerm', param).then(successCallback, errorCallback);
		}

		function ActivateInactivate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MstPaymentTerm/ActivateInactivate', param).then(successCallback, errorCallback);
		}
	}
})()