﻿(function () {
    'use strict';

    angular.module("app").factory("TenderVerificationService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            HasRoleCP: HasRoleCP,
            GetStatusOptions: GetStatusOptions,
            SelectCR: SelectCR,
            SelectDetail: SelectDetail,
            SaveReview: SaveReview,
            GetCRApprovals: GetCRApprovals,
            //SaveCRApprovals: SaveCRApprovals
        };

        return service;

        // implementation
        function HasRoleCP(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tenderverification/hasrolecp").then(successCallback, errorCallback);
        }

        function GetStatusOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tenderverification/statusoptions").then(successCallback, errorCallback);
        }

        function SelectCR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tenderverification/selectcr", param).then(successCallback, errorCallback);
        }

        function SelectDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tenderverification/selectdetailcr", param).then(successCallback, errorCallback);
        }

        function SaveReview(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tenderverification/savereview", param).then(successCallback, errorCallback);
        }

        function GetCRApprovals(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tenderverification/getcrapp", param).then(successCallback, errorCallback);
        }

        //function SaveCRApprovals(param, successCallback, errorCallback) {
        //    GlobalConstantService.post(endpoint + "/tenderverification/savecrapp", param).then(successCallback, errorCallback);
        //}
    }
})();