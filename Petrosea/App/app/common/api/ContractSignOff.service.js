﻿(function () {
	'use strict';

	angular.module('app').factory('ContractSignOffService', contractSignOffService);

	contractSignOffService.$inject = ['GlobalConstantService'];

	function contractSignOffService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('admin_endpoint');
		var apipoint = GlobalConstantService.getConstant('api_endpoint');

		var service = {
			getSignOff: getSignOff,
			save: save,
			SendApproval: SendApproval,
			CekEmployee: CekEmployee,
			GetApproval: GetApproval,
			Step: Step,
			getLogin: getLogin,
			getApprovalSignOff: getApprovalSignOff,
			SendEmail: SendEmail,
			contractAwardForm: contractAwardForm,
			contractVariationForm: contractVariationForm,
			isInternalOnly: isInternalOnly,
			getRegretLetter: getRegretLetter,
			saveMail: saveMail,
			isContractVariation: isContractVariation,
			getEmailByVendor: getEmailByVendor,
			sendToSubmittingVendor: sendToSubmittingVendor,
            update:update
		};

		return service;
		function contractAwardForm(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOffApproval/contractAwardForm', param).then(successCallback, errorCallback);
		}
		function contractVariationForm(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOffApproval/contractVariationForm', param).then(successCallback, errorCallback);
		}
		function getEmailByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/getEmailByVendor', param).then(successCallback, errorCallback);
		}
		function saveMail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/saveMail', param).then(successCallback, errorCallback);
		}
		function getRegretLetter(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/dataKontrakVar', param).then(successCallback, errorCallback);
		}
		function isInternalOnly(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOffApproval/isInternalOnly', param).then(successCallback, errorCallback);
		}
		function isContractVariation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOffApproval/isContractVariation', param).then(successCallback, errorCallback);
		}
		function SendEmail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOffApproval/SendEmail', param).then(successCallback, errorCallback);
		}
		function getApprovalSignOff(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOffApproval/getSignOff', param).then(successCallback, errorCallback);
		}
		function getLogin(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/CekCE', param).then(successCallback, errorCallback);
		}
		function sendToSubmittingVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/sendToSubmittingVendor', param).then(successCallback, errorCallback);
		}
		function Step(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/CekStep', model).then(successCallback, errorCallback);
		}
		function GetApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/GetApproval', model).then(successCallback, errorCallback);
		}
		function CekEmployee(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/CekEmployee', model).then(successCallback, errorCallback);
		}
		function SendApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/SendApproval', model).then(successCallback, errorCallback);
		}
		function getSignOff(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/getSignOff', model).then(successCallback, errorCallback);
		}

		function save(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/ContractSignOff/save', model).then(successCallback, errorCallback);
		}

		function update(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/ContractSignOff/update', model).then(successCallback, errorCallback);
		}
	}
})();