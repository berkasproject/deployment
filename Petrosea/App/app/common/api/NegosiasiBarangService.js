﻿(function () {
	'use strict';

	angular.module("app").factory("NegosiasiBarangService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			select: select,
			insert: insert,
			bychat: bychat,
			insertchat: insertchat,
			itemall: itemall,
			update: update,
			updatedetail: updatedetail,
			editactive: editactive,
			CanReOpenNego: CanReOpenNego,
			ReOpenNegotiation: ReOpenNegotiation,
			cek: cek,
			updatedeal: updatedeal,
			selectv: selectv,
			insertv: insertv,
			bychatv: bychatv,
			insertchatv: insertchatv,
			itemallv: itemallv,
			updatev: updatev,
			updatedetailv: updatedetailv,
			editactivev: editactivev,
			updateitem: updateitem,
			selectStep: selectStep,
			GetSteps: GetSteps,
			sendMail: sendMail,
			isOvertime: isOvertime,
			InsertApproval: InsertApproval,
			GetApproval: GetApproval,
			GetNegoToApproval: GetNegoToApproval,
			updateApproval: updateApproval,
			sendMailToApprover: sendMailToApprover,
			sendMailToBuyer: sendMailToBuyer,
			loadStepNego: loadStepNego,
			selectHistory: selectHistory
		};

		return service;

		// --- admin
		function loadStepNego(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/getTenderStep", param).then(successCallback, errorCallback);
		}
		function sendMailToBuyer(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/sendMailToBuyer", param).then(successCallback, errorCallback);
		}
		function sendMailToApprover(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/sendMailToApprover", param).then(successCallback, errorCallback);
		}
		function updateApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/UpdateApproval", param).then(successCallback, errorCallback);
		}
		function GetNegoToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/GetNegoToApproval", param).then(successCallback, errorCallback);
		}
		function GetApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/GetApproval", param).then(successCallback, errorCallback);
		}
		function InsertApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/InsertApproval", param).then(successCallback, errorCallback);
		}
		function isOvertime(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tahapan/Goodsnegosiasi/getsteps", param).then(successCallback, errorCallback);
		}
		function sendMail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/sendMail", param).then(successCallback, errorCallback);
		}
		function GetSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorendpoint + "/GoodsOfferEntry/tahapan/getsteps", param).then(successCallback, errorCallback);
		}
		function selectStep(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/Insert", param).then(successCallback, errorCallback);
		}
		function bychat(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/ByChat", param).then(successCallback, errorCallback);
		}
		function insertchat(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/InsertChat", param).then(successCallback, errorCallback);
		}
		function itemall(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/ItemAll", param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/Update", param).then(successCallback, errorCallback);
		}
		function updatedetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/UpdateDetail", param).then(successCallback, errorCallback);
		}
		function editactive(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/EditActive", param).then(successCallback, errorCallback);
		}
		function CanReOpenNego(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/CanReOpenNego", param).then(successCallback, errorCallback);
		}
		function ReOpenNegotiation(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/ReOpenNegotiation", param).then(successCallback, errorCallback);
		}
		function cek(param, successCallback, errorCallback) {
			//console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/Cek", param).then(successCallback, errorCallback);
		}
		function updatedeal(param, successCallback, errorCallback) {
			//console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/Goodsnegosiasi/UpdateDeal", param).then(successCallback, errorCallback);
		}
		function selectHistory(param, successCallback, errorCallback) {
		    //console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/Goodsnegosiasi/SelectHistory", param).then(successCallback, errorCallback);
		}
		// --- vendor
		function selectv(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi", param).then(successCallback, errorCallback);
		}
		function insertv(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/Insert", param).then(successCallback, errorCallback);
		}
		function bychatv(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/ByChat", param).then(successCallback, errorCallback);
		}
		function insertchatv(param, successCallback, errorCallback) {
			//console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/InsertChat", param).then(successCallback, errorCallback);
		}
		function itemallv(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/ItemAll", param).then(successCallback, errorCallback);
		}
		function updatev(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/Update", param).then(successCallback, errorCallback);
		}
		function updatedetailv(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/UpdateDetail", param).then(successCallback, errorCallback);
		}
		function editactivev(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/EditActive", param).then(successCallback, errorCallback);
		}
		function updateitem(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorendpoint + "/Goodsnegosiasi/UpdatePerItem", param).then(successCallback, errorCallback);
		}
	}
})();