﻿(function () {
	'use strict';

	angular.module("app").factory("vhsfpaTechEvalResulDetermineVendorService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");

		var service = {
			GetStep: GetStep
		};

		return service;

		function GetStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhsfpaTechEvalResulDetermineVendor/GetStep", param).then(successCallback, errorCallback);
		}
	}
})();