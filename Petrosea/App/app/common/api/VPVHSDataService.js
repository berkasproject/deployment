﻿(function () {
    'use strict';

    angular.module("app").factory("VPVHSDataService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            insert: insert,
            insertDefault: insertDefault,
            update: update,
            selectbyid: selectbyid,
            switchactive: switchactive,
            cekRole: cekRole,
            getByType: getByType,
            getContractVHS: getContractVHS,
            inserturl: inserturl,
            publish: publish,
            reject: reject,
            sendToVendor: sendToVendor,
            getapprovalhistories: getapprovalhistories,
            //insertWL: insertWL,
            getDocs: getDocs,
            saveDoc: saveDoc,
            deleteDoc: deleteDoc,
            nodraft: nodraft,
            getSponsorAndDept: getSponsorAndDept,
            getReminder: getReminder,
            checkAndGetVHSAward: checkAndGetVHSAward
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/select", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/insert", param).then(successCallback, errorCallback);
        }

        function insertDefault(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/insertDefault", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/update", param).then(successCallback, errorCallback);
        }

        function selectbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/selectbyid", param).then(successCallback, errorCallback);
        }

        function switchactive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/switchactive", param).then(successCallback, errorCallback);
        }

        function cekRole(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/cekRole").then(successCallback, errorCallback);
        }

        function getByType(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/selectEvalByType", param).then(successCallback, errorCallback);
        }

        function getContractVHS(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/getdatacontractVHS", param).then(successCallback, errorCallback);
        }

        function inserturl(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/insertUrl", param).then(successCallback, errorCallback);
        }

        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/publish", param).then(successCallback, errorCallback);
        }

        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/reject", param).then(successCallback, errorCallback);
        }

        function sendToVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/sendToVendor", param).then(successCallback, errorCallback);
        }

        function getapprovalhistories(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/getapprovalhistories", param).then(successCallback, errorCallback);
        }
        //function insertWL(param, successCallback, errorCallback) {
        //    GlobalConstantService.post(endpoint + "/vpvhs/insertWL", param).then(successCallback, errorCallback);
        //}

        function getDocs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/getDocs", param).then(successCallback, errorCallback);
        }
        function saveDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/saveDoc", param).then(successCallback, errorCallback);
        }
        function deleteDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/deleteDoc", param).then(successCallback, errorCallback);
        }
        function nodraft(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/nodraft", param).then(successCallback, errorCallback);
        }
        function getSponsorAndDept(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/getSponsorAndDept", param).then(successCallback, errorCallback);
        }
        function getReminder(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/getreminder", param).then(successCallback, errorCallback);
        }
        function checkAndGetVHSAward(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpvhs/checkAndGetVHSAward", param).then(successCallback, errorCallback);
        }
    }
})();