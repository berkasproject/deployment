﻿(function () {
    'use strict';

    angular.module("app").factory("VPKPIService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            docTypes: docTypes,
            saveVPKPI: saveVPKPI,
            saveVPKPIDocs: saveVPKPIDocs,
            saveSAPSPdel: saveSAPSPdel,
            saveSAPSPmdr: saveSAPSPmdr,
            saveSAPSPpoh:saveSAPSPpoh,
            vpkpidocsByID: vpkpidocsByID,
            supplierPerformance: supplierPerformance,
            vpkpidocs: vpkpidocs,
            saveSAPSPhdritm: saveSAPSPhdritm,
            hapus: hapus,
            scoreHistory: scoreHistory,
            countScore: countScore,
            sinkronisasiVendor: sinkronisasiVendor,
            detailUploadDEL: detailUploadDEL,
            detailUploadPOH: detailUploadPOH,
            detailUploadMDR: detailUploadMDR,
            detailUploadHdrItm:detailUploadHdrItm
        };

        return service;

        // implementation
        function docTypes(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/docTypes", param).then(successCallback, errorCallback);
        }
        function saveVPKPI(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/saveVPKPI", param).then(successCallback, errorCallback);
        }
        function saveVPKPIDocs(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/saveVPKPIDocs", param).then(successCallback, errorCallback);
        }
        function saveSAPSPdel(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/saveSAPSPDEL", param).then(successCallback, errorCallback);
        }
        function saveSAPSPpoh(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/saveSAPSPPOH", param).then(successCallback, errorCallback);
        }
        function saveSAPSPmdr(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/saveSAPSPMDR", param).then(successCallback, errorCallback);
        }
        function vpkpidocsByID(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/vpkpidocsByID", param).then(successCallback, errorCallback);
        }
        function supplierPerformance(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/supplierperformance", param).then(successCallback, errorCallback);
        }
        function vpkpidocs(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/vpkpidocs", param).then(successCallback, errorCallback);
        }
        function saveSAPSPhdritm(param, successCallback, errorCallback) {
            //console.info("param:" + JSON.stringify(param));
            GlobalConstantService.post(adminpoint + "/vpkpi/saveSAPSPhdritm", param).then(successCallback, errorCallback);
        }
        function hapus(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/hapus", param).then(successCallback, errorCallback);
        }
        function scoreHistory(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/scoreHistory", param).then(successCallback, errorCallback);
        }
        function countScore(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/countScore", param).then(successCallback, errorCallback);
        }
        function sinkronisasiVendor(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/sinkronisasiVendor").then(successCallback, errorCallback);
        }
        function detailUploadDEL(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/detailUploadDEL", param).then(successCallback, errorCallback);
        }
        function detailUploadPOH(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/detailUploadPOH", param).then(successCallback, errorCallback);
        }
        function detailUploadMDR(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/detailUploadMDR", param).then(successCallback, errorCallback);
        }
        function detailUploadHdrItm(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vpkpi/detailUploadHdrItm", param).then(successCallback, errorCallback);
        }
    }
})();