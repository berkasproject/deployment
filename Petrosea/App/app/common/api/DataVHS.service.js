﻿(function () {
    'use strict';

    angular.module('app').factory('DataVHSService', svc);

    svc.$inject = ['GlobalConstantService'];

    //@ngInject
    function svc(GlobalConstantService) {
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var Vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var Publicendpoint = GlobalConstantService.getConstant("api_endpoint");
        //interfaces
        var service = {
            selectMonitoringMRKO: selectMonitoringMRKO,
            selectUploadDataMRKO: selectUploadDataMRKO,
            Select: Select,
            Addendum: Addendum,
            AddAddendum: AddAddendum,
            Approval: Approval,
            Reject: Reject,
            DetailAddendum : DetailAddendum,
            CreateAddendum: CreateAddendum,
            EditAddendum: EditAddendum,
            UpdateAddendum: UpdateAddendum,
            Currency: Currency,
            Country: Country,
            DetailDoc: DetailDoc,
            CreateAddendumDetail: CreateAddendumDetail,
            SelectDataMRKO: SelectDataMRKO,
            InsertFile: InsertFile,
            InsertExcel: InsertExcel,
            DeleteFile: DeleteFile,
            DetailDocUpload: DetailDocUpload,
            Compare: Compare,
            ViewToCreate: ViewToCreate,
            InsertApproval: InsertApproval,
            getDataForApproval: getDataForApproval,
            SendApproval: SendApproval,
            sentMRKO: sentMRKO,
            GetApproval: GetApproval,
            sendMailToApprover: sendMailToApprover,
            sendMailToVendor: sendMailToVendor,
            selectById:selectById,
            selectStorageLoc: selectStorageLoc,
            selectVendorByStorageLoc:selectVendorByStorageLoc,
            generateItemMRKO: generateItemMRKO,
            cancelMRKO:cancelMRKO
        };

        return service;
        function sendMailToVendor(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/sendMailToVendor', model).then(successCallback, errorCallback);
        }
        function sendMailToApprover(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/sendMailToApprover', model).then(successCallback, errorCallback);
        }
        function GetApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/GetApproval', model).then(successCallback, errorCallback);
        }
        function sentMRKO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/sentMRKO', model).then(successCallback, errorCallback);
        }
        function SendApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/SendApproval', model).then(successCallback, errorCallback);
        }
        function getDataForApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/getDataToApproval', model).then(successCallback, errorCallback);
        }
        function InsertApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/InsertApproval', model).then(successCallback, errorCallback);
        }
        function ViewToCreate(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/ViewToCreateMRKO', model).then(successCallback, errorCallback);
        }
        function Compare(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/CompareMRKO', model).then(successCallback, errorCallback);
        }
        function DetailDocUpload(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/selectUploadDataDetailMRKO', model).then(successCallback, errorCallback);
        }
        function DeleteFile(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/DeleteFile', model).then(successCallback, errorCallback);
        }
        function InsertExcel(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/InsertExcel', model).then(successCallback, errorCallback);
        }
        function InsertFile(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/InsertFile', model).then(successCallback, errorCallback);
        }
        function selectUploadDataMRKO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/selectUploadDataMRKO', model).then(successCallback, errorCallback);
        }
        function selectMonitoringMRKO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO', model).then(successCallback, errorCallback);
        }
        function Select(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/dataVHS', model).then(successCallback, errorCallback);
        }
        function Addendum(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detailAdendum', model).then(successCallback, errorCallback);
        }

        function AddAddendum(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/addAdendum', model).then(successCallback, errorCallback);
        }
        function CreateAddendum(AddendumModel, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/createAdendum', AddendumModel).then(successCallback, errorCallback);
        }
        function Approval(AddendumModel, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/approve', AddendumModel).then(successCallback, errorCallback);
        }
        function Reject(AddendumModel, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/reject', AddendumModel).then(successCallback, errorCallback);
        }
        function CreateAddendumDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/createAdendum/Upload', param).then(successCallback, errorCallback);
        }
        function EditAddendum(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/editAdendum', model).then(successCallback, errorCallback);
        }
        function UpdateAddendum(AddendumModel, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/updateAdendum', AddendumModel).then(successCallback, errorCallback);
        }
        function Currency(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/currency').then(successCallback, errorCallback);
        }
        function Country(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/negara').then(successCallback, errorCallback);
        }
        function DetailDoc(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detailDoc', model).then(successCallback, errorCallback);
        }
        function DetailAddendum(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/dataVHS/detailAddendum', model).then(successCallback, errorCallback);
        }
        function SelectDataMRKO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectDataMRKO', model).then(successCallback, errorCallback);
        }
        function selectById(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/selectById', model).then(successCallback, errorCallback);
        }
        function selectStorageLoc(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/selectStorageLoc', model).then(successCallback, errorCallback);
        }
        function selectVendorByStorageLoc(model,successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/selectVendorByStorageLoc',model).then(successCallback, errorCallback);
        }
        function generateItemMRKO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/generateItemMRKO', model).then(successCallback, errorCallback);
        }
        function cancelMRKO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringMRKO/cancelMRKO', model).then(successCallback, errorCallback);
        }
    }
})();