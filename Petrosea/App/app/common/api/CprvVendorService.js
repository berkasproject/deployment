﻿(function () {
    'use strict';

    angular.module("app").factory("CprvVendorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            select: select,
            selectbyid: selectbyid,
            getDocs: getDocs,
            CPRbyContract: CPRbyContract,
            submitbyvendor: submitbyvendor,
            selectDocsCPRId:selectDocsCPRId
            //cekRole: cekRole
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/select", param).then(successCallback, errorCallback);
        }

        function selectbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/selectbyId", param).then(successCallback, errorCallback);
        }

        function getDocs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/getDocs", param).then(successCallback, errorCallback);
        }

        function CPRbyContract(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/cprByContract", param).then(successCallback, errorCallback);
        }

        function submitbyvendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/submitbyvendor", param).then(successCallback, errorCallback);
        }
        function selectDocsCPRId(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/selectDocsCPRId", param).then(successCallback, errorCallback);
        }
        /*
        function cekRole(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprv-vendor/cekRole").then(successCallback, errorCallback);
        }
        */
    }
})();