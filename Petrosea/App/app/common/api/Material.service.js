﻿(function () {
	'use strict';

	angular.module("app").factory("MasterMaterialService", service);

	service.$inject = ['$upload', 'GlobalConstantService'];

	/* @ngInject */
	function service($upload, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
		    GetAllCommodities: GetAllCommodities,
		    InsertMaterial: InsertMaterial,
		    InsertExcel: InsertExcel,
		    EditMaterial: EditMaterial,
		    InsertMarc: InsertMarc,
			SelectMaterials: SelectMaterials,
			SelectHarmonizedCodes: SelectHarmonizedCodes,
			SelectConcessions: SelectConcessions,
			GetFormTemplate: GetFormTemplate
		};

		return service;
		
		function GetAllCommodities(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/commodities").then(successCallback, errorCallback);
		}
		function InsertMaterial(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/insert", param).then(successCallback, errorCallback);
		}
		function InsertExcel(file, successCallback, errorCallback) {
		    var param = {
		        url: endpoint + '/mastermaterial/insertexcel',
		        file: file,
		        fields: {}
		    };
		    $upload.upload(param).then(successCallback, errorCallback);
		}
		function InsertMarc(file, successCallback, errorCallback) {
		    var param = {
		        url: endpoint + '/mastermaterial/insertexcelmarc',
		        file: file,
		        fields: {}
		    };
		    $upload.upload(param).then(successCallback, errorCallback);
		}
		function EditMaterial(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/edit", param).then(successCallback, errorCallback);
		}
		function SelectMaterials(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/select", param).then(successCallback, errorCallback);
		}
		function SelectHarmonizedCodes(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/selectharmonizedcodes", param).then(successCallback, errorCallback);
		}
		function SelectConcessions(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/selectconcessions", param).then(successCallback, errorCallback);
		}
		function GetFormTemplate(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/mastermaterial/template").then(successCallback, errorCallback);
		}
	}
})();