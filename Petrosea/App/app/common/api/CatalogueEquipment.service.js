﻿(function () {
	'use strict';

	angular.module("app").factory("CatalogueEquipmentService", service);

	service.$inject = ['$upload', 'GlobalConstantService'];

	/* @ngInject */
	function service($upload, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
		    SelectCatalogues: SelectCatalogues,
		    GetCosts: GetCosts,
		    InsertCatalogue: InsertCatalogue,
		    EditCatalogue: EditCatalogue,
		    RemoveCatalogue: RemoveCatalogue,
		    SaveCost: SaveCost,
		    RemoveCost: RemoveCost
		};

		return service;
		
		function SelectCatalogues(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/select", param).then(successCallback, errorCallback);
		}
		function GetCosts(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/costs", param).then(successCallback, errorCallback);
		}
		function InsertCatalogue(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/insert", param).then(successCallback, errorCallback);
		}
		function EditCatalogue(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/edit", param).then(successCallback, errorCallback);
		}
		function RemoveCatalogue(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/remove", param).then(successCallback, errorCallback);
		}
		function SaveCost(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/savecost", param).then(successCallback, errorCallback);
		}
		function RemoveCost(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/catalogue/equipment/removecost", param).then(successCallback, errorCallback);
		}
	}
})();