﻿(function () {
	'use strict';

	angular.module('app').factory('budgetTypeService', serviceMethod);

	serviceMethod.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function serviceMethod(GlobalConstantService) {
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
			editCode: editCode,
			loadTypes: loadTypes,
			inactivate: inactivate,
			getByID: getByID
		};
		return service;

		function editCode(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/budgetType/editCode", param).then(successCallback, errorCallback);
		}

		function inactivate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/budgetType/inactivate", param).then(successCallback, errorCallback);
		}

		function loadTypes(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/budgetType/loadTypes", param).then(successCallback, errorCallback);
		}

		function getByID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/budgetType/getByID", param).then(successCallback, errorCallback);
		}
	}
})()