﻿(function () {
	'use strict';

	angular.module('app').factory('DataFPAService', svc);

	svc.$inject = ['GlobalConstantService'];

	//@ngInject
	function svc(GlobalConstantService) {
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var Vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var Publicendpoint = GlobalConstantService.getConstant("api_endpoint");
		//interfaces
		var service = {
			Select: Select,
			Addendum: Addendum,
			AddAddendum: AddAddendum,
			Approval: Approval,
			Reject: Reject,
			DetailAddendum: DetailAddendum,
			CreateAddendum: CreateAddendum,
			EditAddendum: EditAddendum,
			UpdateAddendum: UpdateAddendum,
			Currency: Currency,
			Country: Country,
			DetailDoc: DetailDoc,
			CreateAddendumDetail: CreateAddendumDetail,
			SelectDataMRKO: SelectDataMRKO,
			detailAdendumPOFPA: detailAdendumPOFPA,
			SelectMonitoringPO: SelectMonitoringPO,
			getCodeAddendumFPA: getCodeAddendumFPA,
			DetailItemAdendum: DetailItemAdendum,
			cekExcel: cekExcel,
			loadAddendumById: loadAddendumById,
			loadDetailAddendumByID: loadDetailAddendumByID,
			excelvendorFPA: excelvendorFPA,
			DetailDocUpload: DetailDocUpload,
			DetailDocUploadFPA: DetailDocUploadFPA,
			InsertEmail: InsertEmail,
			InsertAddendumApproval: InsertAddendumApproval,
			sendMailToApproval: sendMailToApproval,
			GetApproval: GetApproval,
			UpdateAddendumApproval: UpdateAddendumApproval,
			GetListApproval: GetListApproval,
			setAck: setAck
		};

		return service;
		function GetListApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MonitoringVHS/GetApprovalAddendum', model).then(successCallback, errorCallback);
		}
		function UpdateAddendumApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MonitoringVHS/UpdateAddendumApproval', model).then(successCallback, errorCallback);
		}
		function GetApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/AddendumApproval/getData', model).then(successCallback, errorCallback);
		}
		function sendMailToApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MonitoringVHS/SendMailApproval', model).then(successCallback, errorCallback);
		}
		function InsertAddendumApproval(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MonitoringVHS/InsertAddendumApproval', model).then(successCallback, errorCallback);
		}
		function InsertEmail(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MonitoringVHS/SendEmailToVendor', model).then(successCallback, errorCallback);
		}
		function setAck(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/MonitoringVHS/SetAsAcknowledged', model).then(successCallback, errorCallback);
		}
		function DetailDocUploadFPA(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailUploadMRKO/selectUploadDataDetailFPA', model).then(successCallback, errorCallback);
		}
		function DetailDocUpload(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailUploadMRKO/selectUploadDataDetailMRKO', model).then(successCallback, errorCallback);
		}
		function excelvendorFPA(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ExcelVendorFPA', model).then(successCallback, errorCallback);
		}
		function loadDetailAddendumByID(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailPOFPAAddendumByID', model).then(successCallback, errorCallback);
		}
		function loadAddendumById(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/getAddendum', model).then(successCallback, errorCallback);
		}
		function cekExcel(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/cekPOUploadItem', model).then(successCallback, errorCallback);
		}
		function DetailItemAdendum(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailPOFPAAddendum', model).then(successCallback, errorCallback);
		}
		function getCodeAddendumFPA(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/getCodeAddendumFPA', model).then(successCallback, errorCallback);
		}
		function SelectMonitoringPO(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectmonitoringPO', model).then(successCallback, errorCallback);
		}
		function detailAdendumPOFPA(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailPOFPA', model).then(successCallback, errorCallback);
		}
		function Select(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/dataFPA', model).then(successCallback, errorCallback);
		}
		function Addendum(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailAdendumFPA', model).then(successCallback, errorCallback);
		}

		function AddAddendum(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/addAdendumFPA', model).then(successCallback, errorCallback);
		}
		function CreateAddendum(AddendumModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/createAdendumFPA', AddendumModel).then(successCallback, errorCallback);
		}
		function Approval(AddendumModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/approveFPA', AddendumModel).then(successCallback, errorCallback);
		}
		function Reject(AddendumModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/rejectFPA', AddendumModel).then(successCallback, errorCallback);
		}
		function CreateAddendumDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/createAdendumFPA/Upload', param).then(successCallback, errorCallback);
		}
		function EditAddendum(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/editAdendumFPA', model).then(successCallback, errorCallback);
		}
		function UpdateAddendum(AddendumModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/updateAdendumFPA', AddendumModel).then(successCallback, errorCallback);
		}
		function Currency(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/currencyFPA').then(successCallback, errorCallback);
		}
		function Country(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/negaraFPA').then(successCallback, errorCallback);
		}
		function DetailDoc(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailDocFPA', model).then(successCallback, errorCallback);
		}
		function DetailAddendum(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/dataFPA/detailAddendum', model).then(successCallback, errorCallback);
		}
		function SelectDataMRKO(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDataMRKO', model).then(successCallback, errorCallback);
		}
	}
})();