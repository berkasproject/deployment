﻿(function () {
	'use strict';

	angular.module('app').factory('TenderTrackingService', dataService);

	dataService.$inject = ['GlobalConstantService'];

	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant('admin_endpoint');

		var service = {
			Select: Select,
			SelectByID: SelectByID,
			GetStep: GetStep
		};

		return service;

		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tendertrack/select', param).then(successCallback, errorCallback);
		}

		function SelectByID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tendertrack/selectbyid', param).then(successCallback, errorCallback);
		}

		function GetStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/tendertrack/getstep', param).then(successCallback, errorCallback);
		}
	}
})();