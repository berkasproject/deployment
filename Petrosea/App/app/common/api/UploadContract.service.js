﻿(function () {
    'use strict';

    angular.module("app").factory("UploadContractService", service);

    service.$inject = ['$upload', 'GlobalConstantService'];

    /* @ngInject */
    function service($upload, GlobalConstantService) {
        var endpoint = GlobalConstantService.getConstant("admin_endpoint");

        // interfaces
        var service = {
            UploadExcel: UploadExcel
        };

        return service;

        function UploadExcel(file, successCallback, errorCallback) {
            var param = {
                url: endpoint + '/uploadcontract/uploadexcel',
                file: file,
                fields: {}
            };
            $upload.upload(param).then(successCallback, errorCallback);
        }
    }
})();