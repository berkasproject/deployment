﻿(function () {
    'use strict';

    angular.module("app").factory("TenderAnnouncementApprovalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            getdetails: getdetails,
            approve: approve,
            reject: reject
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/announapproval/select", param).then(successCallback, errorCallback);
        }
        function getdetails(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/announapproval/getdetails", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/announapproval/approve", param).then(successCallback, errorCallback);
        }
        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/announapproval/reject", param).then(successCallback, errorCallback);
        }

    }
})();