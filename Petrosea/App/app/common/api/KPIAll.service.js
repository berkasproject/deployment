(function () {
    'use strict';

    angular.module("app")
        .factory("KPIAllService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        
        // interfaces
        var service = {
            CostSavingService: CostSavingService,
            CostSavingGoods: CostSavingGoods,
            CostSavingVhs:CostSavingVhs
        };
        return service;

        // implementation

        function CostSavingService(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/kpiall/costsavingservice", param).then(successCallback, errorCallback);
        }
        function CostSavingGoods(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/kpiall/costsavinggoods", param).then(successCallback, errorCallback);
        }
        function CostSavingVhs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/kpiall/costsavingvhs", param).then(successCallback, errorCallback);
        }
    }
})();