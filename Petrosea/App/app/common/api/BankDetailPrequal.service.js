﻿(function () {
	'use strict';

	angular.module("app").factory("BankDetailPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			Select: Select,
			insert: insert,
			Update: Update,
			remove: remove,
			SelectVend: SelectVend,
			getCRbyVendor: getCRbyVendor,
			getCurrencies: getCurrencies,
			loadPrequalStep: loadPrequalStep,
			Submit: Submit
		};

		return service;

	    // implementation
		function loadPrequalStep(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
		}
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBankDetailPrequal/Submit", param).then(successCallback, errorCallback);
		}
		function getCurrencies(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/bankdetail/currency/list")
                .then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/bank").then(successCallback, errorCallback);
		}
		function Select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBankDetailPrequal", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBankDetailPrequal/insert", param).then(successCallback, errorCallback);
		}

		function Update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBankDetailPrequal/update", param).then(successCallback, errorCallback);
		}

		function remove(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorBankDetailPrequal/remove", param).then(successCallback, errorCallback);
		}
		function SelectVend(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/getByVendorID", param).then(successCallback, errorCallback);
		}

		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
	}
})();