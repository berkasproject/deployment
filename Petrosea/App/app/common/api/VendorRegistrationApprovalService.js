﻿(function () {
	'use strict';

	angular.module("app").factory("VendorRegistrationApprovalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			select: select,
			approve: approve,
			reject: reject,
			loadCommodity: loadCommodity
		};

		return service;

		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/VendorRegistrationApproval/select", param).then(successCallback, errorCallback);
		}

		function approve(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/VendorRegistrationApproval/approve", param).then(successCallback, errorCallback);
		}

		function reject(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/VendorRegistrationApproval/reject", param).then(successCallback, errorCallback);
		}
		function loadCommodity(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/VendorRegistrationApproval/loadCommodity", param).then(successCallback, errorCallback);
		}
	}
})()