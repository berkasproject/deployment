(function () {
	'use strict';

	angular.module("app").factory("NeracaService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			editActive: editActive
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/jabatan/jabataneditactive").then(successCallback, errorCallback);
		}

	}
})();