﻿(function () {
    'use strict';

    angular.module("app").factory("DataContractRequisitionService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
        	getUserLogin: getUserLogin,
        	isAllowCreate: isAllowCreate,
        	SelectById: SelectById,
        	GetRequestTypeOptions: GetRequestTypeOptions,
        	GetBudgetOptions: GetBudgetOptions,
        	SelectById2: SelectById2,
        	Select: Select,
        	IsRequestor: IsRequestor,
        	IsSOADocsUploaded: IsSOADocsUploaded,
        	IsSOWASaved: IsSOWASaved,
        	IsResponsibilityMxSaved: IsResponsibilityMxSaved,
        	IsCRDataComplete: IsCRDataComplete,
        	IsCostEstimateLineUsedAll: IsCostEstimateLineUsedAll,
        	IsTotalLineLessOrEqualTotalValue: IsTotalLineLessOrEqualTotalValue,
        	IsTotalLineNotTooLow: IsTotalLineNotTooLow,
        	IsCRDetailSaved: IsCRDetailSaved,
        	IsWeightingMxSaved: IsWeightingMxSaved,
        	IsDAFormComplete: IsDAFormComplete,
        	SendToApproval: SendToApproval,
        	SendOERevision: SendOERevision,
        	SelectDocs: SelectDocs,
        	SaveDoc: SaveDoc,
        	DeleteDoc: DeleteDoc,
        	SelectByApprover: SelectByApprover,
        	IsApprover: IsApprover,
        	IsApprovingTurn: IsApprovingTurn,
        	GetCRDApprovals: GetCRDApprovals,
        	SetApprovalStatus: SetApprovalStatus,
        	AddReviewer: AddReviewer,
        	DeleteReviewer: DeleteReviewer,
        	GetCRReviewers: GetCRReviewers,
        	Update2: Update2,
        	SelectDetail: SelectDetail,
        	SaveDetail: SaveDetail,
        	GetStates: GetStates,
            GetCities: GetCities,
        	SelectCSMS: SelectCSMS,
        	SaveCSMS: SaveCSMS,
        	GetDepartments: GetDepartments,
        	GetRespMxTemplate: GetRespMxTemplate,
        	SelectRespMxDetail: SelectRespMxDetail,
        	SaveRespMxDetail: SaveRespMxDetail,
        	GetRefCRDet: GetRefCRDet,
        	SelectVendor: SelectVendor,
        	CountVendor: CountVendor,
        	SelectEmployee: SelectEmployee,
        	CountEmployee: CountEmployee,
        	SelectDirectAward: SelectDirectAward,
        	GetVendorContact: GetVendorContact,
        	SaveDirectAward: SaveDirectAward,
        	SelectSOWAQuestion: SelectSOWAQuestion,
        	SelectNewSOWAQuestion: SelectNewSOWAQuestion,
        	InsertSOWA: InsertSOWA,
        	UpdateSOWA: UpdateSOWA,
        	GetCETemplate: GetCETemplate,
        	SelectCELine: SelectCELine,
        	SelectAvailableCELine: SelectAvailableCELine,
        	SelectCESub: SelectCESub,
        	SaveCESub: SaveCESub,
        	DelCESub: DelCESub,
        	GetDetailedCESub: GetDetailedCESub,
        	SelectCESubDet: SelectCESubDet,
        	SaveCELines: SaveCELines,
        	EditCELine: EditCELine,
        	DeleteCELine: DeleteCELine,
        	SetNotForQuotationCELine: SetNotForQuotationCELine,
        	SetIsForQuotationCELine: SetIsForQuotationCELine,
        	CELineForExport: CELineForExport,
        	SelectSOWDocs: SelectSOWDocs,
        	SaveSOWDoc: SaveSOWDoc,
        	DeleteSOWDoc: DeleteSOWDoc,
        	SelectAvaliableEM: SelectAvaliableEM,
        	SaveWeightingMatrix: SaveWeightingMatrix,
        	GetVariationByID: GetVariationByID,
        	CreateVariant: CreateVariant,
        	EditVariant: EditVariant,
        	IsContractSuspended: IsContractSuspended,
        	CanCreateVariant: CanCreateVariant,
        	GetCurrentVariantNumber: GetCurrentVariantNumber,
        	//GetLastVariation: GetLastVariation,
        	GetCurrentEndateAndTotalValue: GetCurrentEndateAndTotalValue,
        	GetContractByCRID: GetContractByCRID,
        	GetCRVariationReasons: GetCRVariationReasons,
        	//EvaluateApprovalStatuses: EvaluateApprovalStatuses,
        	GenerateUser: GenerateUser,
        	isCompliance: isCompliance
        };

        return service;

        // implementation
        function getUserLogin(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/blacklist/GetUserLogin", param).then(successCallback, errorCallback);
        }

        function isCompliance(successCallback, errorCallback) {
        	GlobalConstantService.post(endpoint + "/contractRequisition/isCompliance").then(successCallback, errorCallback);
        }

        function isAllowCreate(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isallowcreate").then(successCallback, errorCallback);
        }

        function SelectById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectbyid", param).then(successCallback, errorCallback);
        }

        function GetRequestTypeOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getrequesttypeoptions").then(successCallback, errorCallback);
        }

        function GetBudgetOptions(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getbudgetoptions", param).then(successCallback, errorCallback);
        }

        function SelectById2(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectbyid2", param).then(successCallback, errorCallback);
        }

        function Select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/select", param).then(successCallback, errorCallback);
        }

        function IsRequestor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isrequestor", param).then(successCallback, errorCallback);
        }

        function IsSOADocsUploaded(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/issoadocsuploaded", param).then(successCallback, errorCallback);
        }

        function IsSOWASaved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/issowasaved", param).then(successCallback, errorCallback);
        }

        function IsResponsibilityMxSaved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isresponsibilitymxsaved", param).then(successCallback, errorCallback);
        }

        function IsCRDataComplete(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/iscrdatacomplete", param).then(successCallback, errorCallback);
        }

        function IsCostEstimateLineUsedAll(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/iscelineusedall", param).then(successCallback, errorCallback);
        }

        function IsTotalLineLessOrEqualTotalValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/istotallinelessorequaltotalvalue", param).then(successCallback, errorCallback);
        }

        function IsTotalLineNotTooLow(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/istotallinenottoolow", param).then(successCallback, errorCallback);
        }

        function IsCRDetailSaved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/iscrdetailsaved", param).then(successCallback, errorCallback);
        }

        function IsWeightingMxSaved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isweightingmxsaved", param).then(successCallback, errorCallback);
        }

        function IsDAFormComplete(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isdaformcomplete", param).then(successCallback, errorCallback);
        }

        function SendToApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/sendtoapproval", param).then(successCallback, errorCallback);
        } 

        function SendOERevision(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/sendoerevision", param).then(successCallback, errorCallback);
        }

        function SelectDocs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectdocs", param).then(successCallback, errorCallback);
        }

        function SaveDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savedoc", param).then(successCallback, errorCallback);
        }

        function DeleteDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/deletedoc", param).then(successCallback, errorCallback);
        }

        function SelectByApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectbyappr", param).then(successCallback, errorCallback);
        }

        function GetCRDApprovals(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcrdapp", param).then(successCallback, errorCallback);
        }        

        function IsApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isapprover", param).then(successCallback, errorCallback);
        } 

        function IsApprovingTurn(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/isapprovingturn", param).then(successCallback, errorCallback);
        }

        function SetApprovalStatus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/setapprovalstatus", param).then(successCallback, errorCallback);
        }

        function AddReviewer(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/addreviewer", param).then(successCallback, errorCallback);
        }

        function DeleteReviewer(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/deletereviewer", param).then(successCallback, errorCallback);
        }

        function GetCRReviewers(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcrrevs", param).then(successCallback, errorCallback);
        }

        function Update2(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/update2", param).then(successCallback, errorCallback);
        }

        function SelectDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectdetail", param).then(successCallback, errorCallback);
        }

        function SaveDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savedetail", param).then(successCallback, errorCallback);
        }

        function SaveCSMS(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savecsms", param).then(successCallback, errorCallback);
        }

        function GetStates(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getstates").then(successCallback, errorCallback);
        }

        function GetCities(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcities", param).then(successCallback, errorCallback);
        }

        function SelectCSMS(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectcsms", param).then(successCallback, errorCallback);
        }
        
        function GetDepartments(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/departments").then(successCallback, errorCallback);
        }
        
        function GetRespMxTemplate(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getrmtemplate").then(successCallback, errorCallback);
        }

        function SelectRespMxDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectrmdet", param).then(successCallback, errorCallback);
        }

        function SaveRespMxDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savermdet", param).then(successCallback, errorCallback);
        }

        function GetRefCRDet(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/contractRequisition/getrefcrdet").then(successCallback, errorCallback);
        }

        function SelectVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectvendor", param).then(successCallback, errorCallback);
        }

        function CountVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/countvendor", param).then(successCallback, errorCallback);
        }

        function SelectEmployee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectemployee", param).then(successCallback, errorCallback);
        }

        function CountEmployee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/countemployee", param).then(successCallback, errorCallback);
        }

        function SelectDirectAward(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectda", param).then(successCallback, errorCallback);
        }

        function SaveDirectAward(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/saveda", param).then(successCallback, errorCallback);
        }

        function GetVendorContact(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getvendorcontact", param).then(successCallback, errorCallback);
        }

        function SelectSOWAQuestion(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectsowaq", param).then(successCallback, errorCallback);
        }

        function SelectNewSOWAQuestion(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectnewsowaq", param).then(successCallback, errorCallback);
        }

        function InsertSOWA(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/insertsowa", param).then(successCallback, errorCallback);
        }

        function UpdateSOWA(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/updatesowa", param).then(successCallback, errorCallback);
        }

        function GetCETemplate(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcetemplate").then(successCallback, errorCallback);
        }

        function SelectCELine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectceline", param).then(successCallback, errorCallback);
        }

        function SelectAvailableCELine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectavailcel", param).then(successCallback, errorCallback);
        }

        function SelectCESub(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectcesub", param).then(successCallback, errorCallback);
        }

        function SaveCESub(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savecesub", param).then(successCallback, errorCallback);
        }

        function DelCESub(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/delcesub", param).then(successCallback, errorCallback);
        }

        function GetDetailedCESub(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/detailedcesub", param).then(successCallback, errorCallback);
        }

        function SelectCESubDet(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectcesubdet", param).then(successCallback, errorCallback);
        }

        function SaveCELines(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savecelines", param).then(successCallback, errorCallback);
        }

        function EditCELine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/editceline", param).then(successCallback, errorCallback);
        }

        function DeleteCELine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/deleteceline", param).then(successCallback, errorCallback);
        }

        function CELineForExport(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/celineforexport", param).then(successCallback, errorCallback);
        }

        function SetNotForQuotationCELine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/setnotforquotationceline", param).then(successCallback, errorCallback);
        }

        function SetIsForQuotationCELine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/setisforquotationceline", param).then(successCallback, errorCallback);
        }

        function SelectSOWDocs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectsowdocs", param).then(successCallback, errorCallback);
        }

        function SaveSOWDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/savesowdoc", param).then(successCallback, errorCallback);
        }

        function DeleteSOWDoc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/deletesowdoc", param).then(successCallback, errorCallback);
        }

        function SelectAvaliableEM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/selectavaliableem", param).then(successCallback, errorCallback);
        }

        function SaveWeightingMatrix(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/saveweightmx", param).then(successCallback, errorCallback);
        }
        
        function GetVariationByID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getvariationbyid", param).then(successCallback, errorCallback);
        }

        function CreateVariant(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/createvariant", param).then(successCallback, errorCallback);
        }

        function EditVariant(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/editvariant", param).then(successCallback, errorCallback);
        }
        
        function IsContractSuspended(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/iscontractsuspended", param).then(successCallback, errorCallback);
        }

        function CanCreateVariant(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/cancreatevariant", param).then(successCallback, errorCallback);
        }

        function GetCurrentVariantNumber(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcurrentvariantnumber", param).then(successCallback, errorCallback);
        }

        //function GetLastVariation(param, successCallback, errorCallback) {
        //    GlobalConstantService.post(endpoint + "/contractRequisition/getlastvariation", param).then(successCallback, errorCallback);
        //}

        function GetCurrentEndateAndTotalValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcurrentendateandtotalvalue", param).then(successCallback, errorCallback);
        }

        function GetContractByCRID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/contractbycrid", param).then(successCallback, errorCallback);
        }

        function GetCRVariationReasons(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/getcrvariationreasons").then(successCallback, errorCallback);
        }

        /*
        function EvaluateApprovalStatuses(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractRequisition/evaluateapprovalstatus").then(successCallback, errorCallback);
        }
        */

        function GenerateUser(param, successCallback, errorCallback) {
        	GlobalConstantService.post(endpoint + "/contractRequisition/generateUser", param).then(successCallback, errorCallback);
        }
    }
})();