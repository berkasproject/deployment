﻿(function () {
	'use strict';

	angular.module("app").factory("GoodOfferEntryService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
			select: select,
			selectTechnical: selectTechnical,
			getCurrencies: getCurrencies,
			GetExchangeRate: GetExchangeRate,
			InsertDetail: InsertDetail,
			getTypeFreightCost: getTypeFreightCost,
			GetFreightTime: GetFreightTime,
			getAll: getAll,
			getByTenderStepID: getByTenderStepID,
			GetStep: GetStep,
			GetRFQ: GetRFQ,
			GetRFQGoods: GetRFQGoods,
			GetFreightCost: GetFreightCost,
			sendToApproval: sendToApproval,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			Upload: Upload,
			UploadOther: UploadOther,
			getOtherDoc: getOtherDoc,
			DeliveryTerm: DeliveryTerm,
			getPaymentTerm: getPaymentTerm,
			getIncoTerms: getIncoTerms,
			selectFreight: selectFreight,
			getTypeTender: getTypeTender,
			getOptionsTender: getOptionsTender,
			getDeliveryTender: getDeliveryTender,
			Insert: Insert,
			InsertSubmit: InsertSubmit,
			cekSubmit: cekSubmit,
			GetSteps: GetSteps,
			GetTenderReg: GetTenderReg,
			DeleteItem: DeleteItem,
			loadVendorCurrency: loadVendorCurrency,
			isApprovalSent: isApprovalSent,
			detailApproval: detailApproval,
			InsertSubmitTechnical: InsertSubmitTechnical,
			GetStepTechnical: GetStepTechnical,
			getAllTechnical: getAllTechnical,
			isNeedTenderStepApprovalTechnical: isNeedTenderStepApprovalTechnical,
			resendToApproval: resendToApproval,
			deleteRow: deleteRow
		};

		return service;

		// implementation
		function loadVendorCurrency(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/getVendorCurr", param).then(successCallback, errorCallback);
		}
		function DeleteItem(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/DeleteItem", param).then(successCallback, errorCallback);
		}
		function deleteRow(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/deleteRow", param).then(successCallback, errorCallback);
		}
		function GetSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/tahapan/getsteps", param).then(successCallback, errorCallback);
		}

		function GetTenderReg(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/tahapan/gettenderreg", param).then(successCallback, errorCallback);
		}
		function InsertSubmit(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/InsertIsSubmit', param).then(successCallback, errorCallback);
		}

		function InsertSubmitTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/InsertIsSubmitTechnical', param).then(successCallback, errorCallback);
		}

		function cekSubmit(param, successCallback, errorCallback) {
			console.info(param);
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/GetMaxItem', param).then(successCallback, errorCallback);
		}
		function getTypeTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_TYPE_RFQ" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getDeliveryTender(successCallback, errorCallback) {
			var param = { Keyword: "RFQ_DELIVERY_TERMS" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getOptionsTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_OPTIONS_RFQ" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function selectFreight(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/cekIncoFreight", param).then(successCallback, errorCallback);
		}
		function getIncoTerms(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/cekInco", param).then(successCallback, errorCallback);
		}
		function getPaymentTerm(successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getPymentTerm').then(successCallback, errorCallback);
		}
		function DeliveryTerm(param, successCallback, errorCallback) {
			var param = { Keyword: "RFQ_DELIVERY_TERMS" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getOtherDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/GetDetailDoc', param).then(successCallback, errorCallback);
		}
		function UploadOther(param, successCallback, errorCallback) {
			console.info(param);
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/UploadUrlGoods', param).then(successCallback, errorCallback);
		}
		function Upload(param, successCallback, errorCallback) {
			console.info(param);
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/UploadUrl', param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getByVendorID', param).then(successCallback, errorCallback);
		}
		function selectTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getByVendorIDTechnical', param).then(successCallback, errorCallback);
		}
		function getAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/getAll', param).then(successCallback, errorCallback);
		}

		function getAllTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/getAllTechnical', param).then(successCallback, errorCallback);
		}

		function getByTenderStepID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/getByTenderStep', param).then(successCallback, errorCallback);
		}

		function GetFreightCost(successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getFreightCost').then(successCallback, errorCallback);
		}
		function GetStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/SelectStep', param).then(successCallback, errorCallback);
		}

		function GetStepTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/SelectStepTechnical', param).then(successCallback, errorCallback);
		}

		function GetRFQ(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getRFQ', param).then(successCallback, errorCallback);
		}
		function GetRFQGoods(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getRFQGoods', param).then(successCallback, errorCallback);
		}

		function getCurrencies(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/currencies").then(successCallback, errorCallback);
		}
		function GetExchangeRate(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/getExchangeRate", param).then(successCallback, errorCallback);
		}
		function Insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/Insert", param).then(successCallback, errorCallback);
		}
		function InsertDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/InsertItem", param).then(successCallback, errorCallback);
		}
		function getTypeFreightCost(successCallback, errorCallback) {
			var param = { Keyword: "FREIGHT_COST_TRANSPORT" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function GetFreightTime(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/GoodsOfferEntry/getFreightTime", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/sendToApproval', param).then(successCallback, errorCallback);
		}

		function resendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/resendToApproval', param).then(successCallback, errorCallback);
		}

		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApprovalTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/isNeedTenderStepApprovalTechnical', param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/isApprovalSent', param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/GoodsOfferEntry/detailApproval', param).then(successCallback, errorCallback);
		}
	}
})();