﻿(function () {
    'use strict';

    angular.module("app").factory("DetailAuctionVendorService", dataService);

    dataService.$inject = ['GlobalConstantService'];
    /* @ngInject */
    function dataService(GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            insert: insert,
            inactivate: inactivate,
            activate: activate,
            getByID: getByID,
            update: update
        };

        return service;

        //implementaion
        function select(param, successCallback, errorCallback) {
            return GlobalConstantService.post(endpoint + '/manufacturer/select', param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            return GlobalConstantService.post(endpoint + '/manufacturer/insert', param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            return GlobalConstantService.post(endpoint + '/manufacturer/update', param).then(successCallback, errorCallback);
        }

        function inactivate(param, successCallback, errorCallback) {
            return GlobalConstantService.post(endpoint + '/manufacturer/inactivate', param).then(successCallback, errorCallback);
        }

        function activate(param, successCallback, errorCallback) {
            return GlobalConstantService.post(endpoint + '/manufacturer/activate', param).then(successCallback, errorCallback);
        }

        function getByID(param, successCallback, errorCallback) {
            return GlobalConstantService.post(endpoint + '/manufacturer/getByID', param).then(successCallback, errorCallback);
        }
    }
})()