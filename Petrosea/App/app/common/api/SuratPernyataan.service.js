(function () {
	'use strict';

	angular.module("app").factory("SuratPernyataanService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			All: All,
			selectNamaDir: selectNamaDir,
			selectLegalDoc: selectLegalDoc,
			selectContactID: selectContactID,
			selectAddressID: selectAddressID,
			selectAddress: selectAddress,
			selectCountryID: selectCountryID,
			selectCountry: selectCountry,
			insertAgree: insertAgree
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function All(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/GetAllByVendorID", param).then(successCallback, errorCallback);
		}
		function insertAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/insertAgree", param).then(successCallback, errorCallback);
		}

		function selectNamaDir(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getDirNameByVendorID", param).then(successCallback, errorCallback);
		}
		function selectLegalDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getLegalDocByVendorID", param).then(successCallback, errorCallback);
		}
		function selectContactID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getContactID", param).then(successCallback, errorCallback);
		}
		function selectAddressID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getAddressID", param).then(successCallback, errorCallback);
		}
		function selectAddress(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getAddress", param).then(successCallback, errorCallback);
		}
		function selectCountryID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getCountryID", param).then(successCallback, errorCallback);
		}
		function selectCountry(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getCountry", param).then(successCallback, errorCallback);
		}
	}
})();