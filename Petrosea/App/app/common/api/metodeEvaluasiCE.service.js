﻿(function () {
    'use strict';

    angular.module("app").factory("MetodeEvaluasiCEService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            selectById: selectById,
            selectDCByMethod: selectDCByMethod,
            select: select,
            count: count,
            update: update,
            isUsed: isUsed
        };

        return service;

        // implementation
        function selectById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasiCE/selectbyid", param).then(successCallback, errorCallback);
        }

        function selectDCByMethod(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasiCE/selectdcbymethod", param).then(successCallback, errorCallback);
        }

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasiCE/select", param).then(successCallback, errorCallback);
        }

        function count(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasiCE/count", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasiCE/update", param).then(successCallback, errorCallback);
        }
        
        function isUsed(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasiCE/isused", param).then(successCallback, errorCallback);
        }
    }
})();