﻿(function () {
    'use strict';

    angular.module("app").factory("dataContractManagementService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            select: select,
            selectEmployee: selectEmployee,
            getContractCommittee:getContractCommittee,
            insertContractManagementCommitte: insertContractManagementCommitte,
            getTanggalTopik: getTanggalTopik,
            getForumContractManagement: getForumContractManagement,
            selectLampiran: selectLampiran,
            updateTanggalTopik: updateTanggalTopik,
            updateForumContractManagement: updateForumContractManagement,
            insertLampiran: insertLampiran,
            insertDetailTemplate: insertDetailTemplate,
            UpdateIsActiveLampiran: UpdateIsActiveLampiran,
            getHistoryContractManagement: getHistoryContractManagement,
            getDetailTemplate: getDetailTemplate,
            getContractManagementFull: getContractManagementFull,
            insertHistory: insertHistory,
            selectApprover: selectApprover,
            insertDetailForumContract: insertDetailForumContract,
            getDetailForumCM: getDetailForumCM,
            generateDetailForum: generateDetailForum,
            updateSummaryContractManagement: updateSummaryContractManagement,
            updateDetailTemplateIsActive: updateDetailTemplateIsActive,
            getContractSignature: getContractSignature,
            insertContractSignature: insertContractSignature,
            getPenerimaChat: getPenerimaChat,
            insertContractSignature: insertContractSignature,
            selectCMCommittee: selectCMCommittee,
            getCMbyID: getCMbyID,
            getAttachment: getAttachment,
            getForum: getForum,
            getDetailForum: getDetailForum,
            insertApproveCommittee: insertApproveCommittee,
            approveForumCommittee: approveForumCommittee,
            getCommitteeLogin: getCommitteeLogin,
            getAllSignature: getAllSignature,
            insertSignature: insertSignature,
            insertRemarkCompleted: insertRemarkCompleted,
            closeProject: closeProject,
            expiredTender: expiredTender,
            extTender: extTender,
            getTypeTender: getTypeTender,
            sendMail: sendMail,
            selectDataESAF: selectDataESAF
        };
        return service;

        function select(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/select", param).then(successCallback, errorCallback);
        }

        function selectEmployee(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getEmployee", param).then(successCallback, errorCallback);
        }

        function getContractCommittee(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getContractCommittee", param).then(successCallback, errorCallback);
        }

        function insertContractManagementCommitte(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/insertCommittee", param).then(successCallback, errorCallback);
        }

        function getTanggalTopik(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getTanggalTopik", param).then(successCallback, errorCallback);
        }

        function selectLampiran(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/selectLampiran", param).then(successCallback, errorCallback);
        }

        function getForumContractManagement(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getForumContractManagement", param).then(successCallback, errorCallback);
        }

        function insertLampiran(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/insertLampiran", param).then(successCallback, errorCallback);
        }

        function insertDetailTemplate(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/insertDetailTemplate", param).then(successCallback, errorCallback);
        }

        function UpdateIsActiveLampiran(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/UpdateIsActiveLampiran", param).then(successCallback, errorCallback);
        }

        function updateTanggalTopik(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/updateTanggalTopik", param).then(successCallback, errorCallback);
        }

        function updateForumContractManagement(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/updateForumContractManagement", param).then(successCallback, errorCallback);
        }

        function getHistoryContractManagement(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getHistoryContractManagement", param).then(successCallback, errorCallback);
        }

        function getDetailTemplate(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getDetailTemplate", param).then(successCallback, errorCallback);
        } 
        
        function getContractManagementFull(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getContractManagementFull", param).then(successCallback, errorCallback);
        }

        function insertHistory(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/insertHistory", param).then(successCallback, errorCallback);
        }

        function insertDetailForumContract(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/insertDetailForumContract", param).then(successCallback, errorCallback);
        } 

        function selectApprover(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/selectApprover", param).then(successCallback, errorCallback);
        }

        function getDetailForumCM(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getDetailForumCM", param).then(successCallback, errorCallback);
        }

        function generateDetailForum(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/generateDetailForum", param).then(successCallback, errorCallback);
        } 
        
        function updateSummaryContractManagement(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/updateSummaryContractManagement", param).then(successCallback, errorCallback);
        }

        function updateDetailTemplateIsActive(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/updateDetailTemplateIsActive", param).then(successCallback, errorCallback);
        }

        function getContractSignature(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getContractSignature", param).then(successCallback, errorCallback);
        }
        function insertContractSignature(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/insertContractSignature", param).then(successCallback, errorCallback);
        }
        function getPenerimaChat(param, successCallback, errorCallback) {
            //console.info("service");
            GlobalConstantService.post(endpoint + "/contract-management/getPenerimaChat", param).then(successCallback, errorCallback); 
        }
        function selectCMCommittee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/selectCMCommittee", param).then(successCallback, errorCallback);
        }
        function getCMbyID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/getCMbyID", param).then(successCallback, errorCallback);
        }
        function getAttachment(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/getAttachment", param).then(successCallback, errorCallback);
        }
        function getForum(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/getForum", param).then(successCallback, errorCallback);
        }
        function getDetailForum(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/getDetailForumCommittee", param).then(successCallback, errorCallback);
        }
        function insertApproveCommittee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/insertApproveCommittee", param).then(successCallback, errorCallback);
        }
        function approveForumCommittee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/approveForumCommittee", param).then(successCallback, errorCallback);
        }
        function getCommitteeLogin(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/getCommitteeLogin", param).then(successCallback, errorCallback);
        }
        function getAllSignature(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/getAllContractSignature", param).then(successCallback, errorCallback);
        }
        function insertSignature(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/insertSignature", param).then(successCallback, errorCallback);
        }
        function insertRemarkCompleted(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/insertRemarkCompleted", param).then(successCallback, errorCallback);
        }
        function closeProject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/closeProject", param).then(successCallback, errorCallback);
        }
        function expiredTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/expiredTender", param).then(successCallback, errorCallback);
        }
        function extTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/extTender", param).then(successCallback, errorCallback);
        }
        function getTypeTender(successCallback, errorCallback) {
            var param = { Keyword: "STATUS_TENDER" };
            GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
        }
        
        function sendMail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/sendMail", param).then(successCallback, errorCallback);
        }

        function selectDataESAF(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contract-management/selectDataESAF", param).then(successCallback, errorCallback);
        }
    }
})();