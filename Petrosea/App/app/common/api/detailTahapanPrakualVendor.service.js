﻿(function () {
    'use strict';

    angular.module("app")
        .factory("DetailTahapanPrakualifikasiVendorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("vendor_endpoint");

        // interfaces
        var service = {
            GetPrequalReg: GetPrequalReg,
            GetPrequalSteps: GetPrequalSteps
        };
        return service;

        // implementation
        function GetPrequalReg(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tahapanprakual/getprequalreg", param).then(successCallback, errorCallback);
        }

        function GetPrequalSteps(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tahapanprakual/getprequalsteps", param).then(successCallback, errorCallback);
        }
    }
})();