(function () {
	'use strict';

	angular.module("app").factory("KriteriaEvaluasiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			editActive: editActive,
			selectCriteria: selectCriteria,
			countCriteria: countCriteria,
			getOptions: getOptions,
			insertCriteria: insertCriteria,
			updateCriteria: updateCriteria,
			deleteCriteria: deleteCriteria,
			selectCriteriaByUser: selectCriteriaByUser,
			countCriteriaByUser: countCriteriaByUser
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {

        }

		function editActive(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "").then(successCallback, errorCallback);
		}

		function selectCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/select", param).then(successCallback, errorCallback);
		}

		function countCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/count", param).then(successCallback, errorCallback);
		}

		function getOptions(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/getoptions", param).then(successCallback, errorCallback);
		}

		function insertCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/insert", param).then(successCallback, errorCallback);
		}

		function updateCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/update", param).then(successCallback, errorCallback);
		}

		function deleteCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/delete", param).then(successCallback, errorCallback);
		}

		function selectCriteriaByUser(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/selectbyuser", param).then(successCallback, errorCallback);
		}

		function countCriteriaByUser(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/kriteriaevaluasi/countbyuser", param).then(successCallback, errorCallback);
		}
	}
})();