﻿(function () {
	'use strict';

	angular.module("app").factory("RoleService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminEndpoint = GlobalConstantService.getConstant('admin_endpoint');
		// interfaces
		var service = {
			all: all,
			checkAuthority: checkAuthority,
			select: select,
			getRoleData: getRoleData,
			remove: remove,
			saveRole: saveRole
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {

		}

		function checkAuthority(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/roles/check_authority" + id).then(successCallback, errorCallback);
		}

		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminEndpoint + '/selectRoles', param).then(successCallback, errorCallback);
		}

		function getRoleData(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminEndpoint + '/getRoleData', param).then(successCallback, errorCallback);
		}

		function saveRole(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminEndpoint + '/role/createEdit', param).then(successCallback, errorCallback);
		}

		function remove(list, data) {
			var i = 0;

			while (i < (list.length)) {
				if (list[i] === data) {
					list.splice(i, 1);
					return list;
				}
				i++;
			}
		}
	}
})();