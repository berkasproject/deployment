﻿(function () {
	'use strict';

	angular.module('app').factory('AccessLogService', serviceMethod);

	serviceMethod.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function serviceMethod(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

		// interfaces
		var service = {
			Select: Select
		};
		return service;

		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/accessLog/getAllLogs", param).then(successCallback, errorCallback);
		}
	}
})();