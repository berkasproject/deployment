﻿(function () {
	'use strict';

	angular.module("app").factory("EvaluationTechnicalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			select: select,
			getVendorInfo: getVendorInfo,
			selectByVendor: selectByVendor,
			selectByEval: selectByEval,
			selectByCode: selectByCode,
			selectByEmployee: selectByEmployee,
			Insert: Insert,
			InsertTech: InsertTech,
			InsertQuestionnaireDetail: InsertQuestionnaireDetail,
			selectByContract: selectByContract,
			allTechnicalScoreByVendor: allTechnicalScoreByVendor,
			selectCriterias: selectCriterias,
			getTechnicalDetails: getTechnicalDetails,
			getTechnicalDetailsByEvaluatorAndVendor: getTechnicalDetailsByEvaluatorAndVendor,
			getCPRScore: getCPRScore,
			saveAllDetail: saveAllDetail,
			getBelowMustHaveScores: getBelowMustHaveScores,
			isEvaluator: isEvaluator,
			getEvaluatorName: getEvaluatorName,
			getSummaryScore: getSummaryScore,
			calculateSummaryScore: calculateSummaryScore,
			saveSummaryScore: saveSummaryScore,
			sendToApproval: sendToApproval,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			detailApproval: detailApproval,
			isApprovalSent: isApprovalSent,
			checkTime: checkTime,
			isLess3Approved: isLess3Approved,
			sendToApprovalSafety: sendToApprovalSafety
		};

		return service;

		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/getAll", param).then(successCallback, errorCallback);
		}
		function getVendorInfo(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/getvendorinfo", param).then(successCallback, errorCallback);
		}
		function selectByVendor(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/getParent", param).then(successCallback, errorCallback);
		}
		function selectByEval(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/getStep", param).then(successCallback, errorCallback);
		}
		function selectByCode(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/qualification/getByTenderVendorForm", param).then(successCallback, errorCallback);
		}
		function selectByEmployee(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/getAllDetail", param).then(successCallback, errorCallback);
		}
		function selectByContract(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/getEvaluator", param).then(successCallback, errorCallback);
		}
		function Insert(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/InsertDetail", param).then(successCallback, errorCallback);
		}
		function InsertTech(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/insert", param).then(successCallback, errorCallback);
		}
		function InsertQuestionnaireDetail(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/qualification/insertQuestionnaire", param).then(successCallback, errorCallback);
		}
		function allTechnicalScoreByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/allTechnicalScoreByVendor", param).then(successCallback, errorCallback);
		}
		function selectCriterias(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/selectCriterias", param).then(successCallback, errorCallback);
		}
		function getTechnicalDetails(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/getTechnicalDetails", param).then(successCallback, errorCallback);
		}
		function getTechnicalDetailsByEvaluatorAndVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/getTechnicalDetailsByEvaluatorAndVendor", param).then(successCallback, errorCallback);
		}
		function getCPRScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/getCPRScore", param).then(successCallback, errorCallback);
		}
		function saveAllDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/saveAllDetail", param).then(successCallback, errorCallback);
		}
		function getBelowMustHaveScores(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/technical/GetBelowMustHaveScores", param).then(successCallback, errorCallback);
		}
		function isEvaluator(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/IsEvaluator", param).then(successCallback, errorCallback);
		}
		function getEvaluatorName(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/GetEvaluatorName", param).then(successCallback, errorCallback);
		}
		function getSummaryScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/GetSummaryScore", param).then(successCallback, errorCallback);
		}
		function calculateSummaryScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/CalculateSummaryScore", param).then(successCallback, errorCallback);
		}
		function saveSummaryScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/technical/SaveSummaryScore", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/sendToApproval', param).then(successCallback, errorCallback);
		}

		function sendToApprovalSafety(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/sendToApprovalSafety', param).then(successCallback, errorCallback);
		}

		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/detailApproval', param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/isApprovalSent', param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}
		function checkTime(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/checkTime', param).then(successCallback, errorCallback);
		}
		function isLess3Approved(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/technical/isless3approved', param).then(successCallback, errorCallback);
		}
	}
})();