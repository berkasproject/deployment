﻿(function () {
	'use strict';

	angular.module("app").factory("PenetapanPemenangVHSservice", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		var Vendorendpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var Publicendpoint = GlobalConstantService.getConstant("api_endpoint");

		// interfaces
		var service = {
			select: select,
			update: update,
			excelvendorVHS: excelvendorVHS,
			excelvendorFPA: excelvendorFPA,
			selectVendor: selectVendor,
			SelectStep: SelectStep,
			uploadDoc: uploadDoc,
			sendMail: sendEmail,
			selectVendorNotDeal: selectVendorNotDeal,
			SendApproval: SendApproval,
			GetApproval: GetApproval,
			CekEmployee: CekEmployee,
			CekRequestor: CekRequestor,
			selectTaxCode: selectTaxCode,
			selectApproval: selectApproval,
			SelectStepAdmin: SelectStepAdmin,
			saveVhsfpaSummary: saveVhsfpaSummary,
			loadStoreLoc: loadStoreLoc
		};

		return service;
		function selectApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/selectApproval', param).then(successCallback, errorCallback);
		}
		function loadStoreLoc(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + '/storeLoc').then(successCallback, errorCallback);
		}
		function selectTaxCode(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + '/taxcode').then(successCallback, errorCallback);
		}
		function CekRequestor(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/VHSAward/CekRequestor', model).then(successCallback, errorCallback);
		}
		function CekEmployee(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/VHSAward/CekEmployee', model).then(successCallback, errorCallback);
		}
		function GetApproval(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/GetApproval", param).then(successCallback, errorCallback);
		}
		function SendApproval(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/SendApproval", param).then(successCallback, errorCallback);
		}
		function selectVendorNotDeal(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/NotDeal", param).then(successCallback, errorCallback);
		}
		function sendEmail(mail, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/VHSAward/SendEmail", mail).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
			console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/Update", param).then(successCallback, errorCallback);

		}
		function uploadDoc(param, successCallback, errorCallback) {
			console.info("masuk service!! " + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/UpdateDoc", param).then(successCallback, errorCallback);

		}

		function saveVhsfpaSummary(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/VHSAward/saveVhsfpaSummary', model).then(successCallback, errorCallback);
		}

		function excelvendorVHS(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/ExcelVendorVHS", param).then(successCallback, errorCallback);
		} function excelvendorFPA(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/ExcelVendorFPA", param).then(successCallback, errorCallback);
		}
		function updatedoc(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/UpdateApproval", param).then(successCallback, errorCallback);
		}

		function selectVendor(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/VHSAward", param).then(successCallback, errorCallback);
		}
		function SelectStep(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Vendorendpoint + "/VHSAward/SelectStep", param).then(successCallback, errorCallback);
		}
		function SelectStepAdmin(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/VHSAward/SelectStep", param).then(successCallback, errorCallback);
		}
	}
})();