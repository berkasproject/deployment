(function () {
    'use strict';

    angular.module("app")
        .factory("CSMSReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            CPRCSMSCount: CPRCSMSCount,
            safetyCompliance: safetyCompliance,
            activeContracts: activeContracts,
            rankSafetyPerformance: rankSafetyPerformance,
            cprDepartement:cprDepartement
        };
        return service;

        // implementation

        function CPRCSMSCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/CSMSReport/CPRCSMSCount", param).then(successCallback, errorCallback);
        }
        function safetyCompliance(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/CSMSReport/safetyCompliance", param).then(successCallback, errorCallback);
        }
        function activeContracts(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/CSMSReport/activeContracts", param).then(successCallback, errorCallback);
        }
        function rankSafetyPerformance(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/CSMSReport/rankSafetyPerformance", param).then(successCallback, errorCallback);
        }
        function dataContractPerformance(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/dataContractPerformance", param).then(successCallback, errorCallback);
        }
        function cprDepartement(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/CSMSReport/CPRCSMSDepartment", param).then(successCallback, errorCallback);
        }
    }
})();