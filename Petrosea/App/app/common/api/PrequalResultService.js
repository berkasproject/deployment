﻿(function () {
	'use strict';

	angular.module('app').factory('PrequalResultService', serviceMethod);

	serviceMethod.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function serviceMethod(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

		// interfaces
		var service = {
			getVendorVehicleAndEquipment: getVendorVehicleAndEquipment,
			getDataExperience: getDataExperience
		};

		return service;

		function getVendorVehicleAndEquipment(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getVendorVehicleAndEquipment", param).then(successCallback, errorCallback);
		}

		function getDataExperience(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getVendorExperts", param).then(successCallback, errorCallback);
		}
	}
})();