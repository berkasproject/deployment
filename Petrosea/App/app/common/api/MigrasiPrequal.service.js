﻿(function () {
    'use strict';

    angular.module("app").factory("MigrasiPrakualService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var service = {
            GetStepData: GetStepData,
            GetDataMigrasi: GetDataMigrasi,
            sentApproval: sentApproval,
            GetDataMigrasiApproval: GetDataMigrasiApproval,
            GetDetailAss: GetDetailAss,
            approve: approve,
            reject: reject,
            sendMail: sendMail,
            sendMigrasi: sendMigrasi,
            cekProsesMigrasi:cekProsesMigrasi
        }
        return service;

        // implementation
        function sendMigrasi(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakual/sentMigrasi", param).then(successCallback, errorCallback);
        }
        function sendMail(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakual/sentEmail", param).then(successCallback, errorCallback);
        }
        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakualApprv/Reject", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakualApprv/Approve", param).then(successCallback, errorCallback);
        }
        function GetDetailAss(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakualApprv/GetDetailAssessment", param).then(successCallback, errorCallback);
        }
        function GetDataMigrasiApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakualApprv", param).then(successCallback, errorCallback);
        }
        function sentApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakual/sentApproval", param).then(successCallback, errorCallback);
        }
        function GetDataMigrasi(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakual", param).then(successCallback, errorCallback);
        }
        function GetStepData(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakual/loadPrequalStep", param).then(successCallback, errorCallback);
        }
        function cekProsesMigrasi(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MigrasiDataPrakual/cekProsesMigrasi", param).then(successCallback, errorCallback);
        }
    }
})();