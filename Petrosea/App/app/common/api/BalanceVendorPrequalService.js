﻿(function () {
	'use strict';

	angular.module("app").factory("BalanceVendorPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
		    select: select,
		    balanceDocUrl: balanceDocUrl,
		    insert: insert,
		    update: update,
		    dlete: dlete,
		    Submit: Submit,
		    loadPrequalStep: loadPrequalStep
		};

		return service;
		function loadPrequalStep(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
		}
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorbalancePrequal/Submit", param).then(successCallback, errorCallback);
		}
		function dlete(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorbalancePrequal/Delete", param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorbalancePrequal/Update", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorbalancePrequal/Insert", param).then(successCallback, errorCallback);
		}
		function balanceDocUrl(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorbalancePrequal/balanceDocUrl", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorbalancePrequal", param).then(successCallback, errorCallback);
		}
	}
})();