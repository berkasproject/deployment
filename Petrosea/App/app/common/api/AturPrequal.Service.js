﻿(function () {
	'use strict';

	angular.module('app').factory('AturPrakualService', serviceMethod);

	serviceMethod.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function serviceMethod(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

		// interfaces
		var service = {
			Select: Select,
			getPrequalMethod: getPrequalMethod,
			getVPEvalMethod: getVPEvalMethod,
			getPrequalStep: getPrequalStep,
			createPrequalMethod: createPrequalMethod,
			editPrequalMethod: editPrequalMethod,
			getDetailMethod: getDetailMethod,
			publish: publish,
			getPrequalCode: getPrequalCode,
			selectposition: selectposition,
			selectemployee: selectemployee,
			emailContent: emailContent,
			getVendor: getVendor,
			viewVendor: viewVendor,
			loadDetailPrequal: loadDetailPrequal,
			loadAllKelengkapan: loadAllKelengkapan,
			getPrequalById: getPrequalById,
			getCompanyScale: getCompanyScale,
			getTechnicalClassification: getTechnicalClassification,
            getBusinessField:getBusinessField
		};
		return service;

		function getVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/prequalSetup/getVendor", param).then(successCallback, errorCallback);
		}
		function getPrequalById(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/prequal/getPrequalSetupById", param).then(successCallback, errorCallback);
		}
		function loadAllKelengkapan(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/prequalSetup/getKelengkapanPrakual", param).then(successCallback, errorCallback);
		}
		function loadDetailPrequal(successCallback, errorCallback) {
		    //var param = { Keyword: "DETAIL_PREQUAL_TYPE" };
		    GlobalConstantService.post(adminpoint + "/prequal/getDetailPrequal").then(successCallback, errorCallback);
		}
		function viewVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/praqualAnnouncement/viewVendor", param).then(successCallback, errorCallback);
		}
		function emailContent(successCallback, errorCallback) {
		    GlobalConstantService.get(adminpoint + "/prequalAnnouncement/EmailContent").then(successCallback, errorCallback);
		}
		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getAllPrequalSetup", param).then(successCallback, errorCallback);
		}

		function getPrequalMethod(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getPrequalMethod").then(successCallback, errorCallback);
		}

		function getVPEvalMethod(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getVPEvalMethod").then(successCallback, errorCallback);
		}

		function getPrequalStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getPrequalStep", param).then(successCallback, errorCallback);
		}

		function createPrequalMethod(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/insertPrequalSetup", param).then(successCallback, errorCallback);
		}

		function editPrequalMethod(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/editPrequalMethod", param).then(successCallback, errorCallback);
		}

		function getDetailMethod(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getPrequalmethodDetail", param).then(successCallback, errorCallback);
		}

		function publish(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/publish", param).then(successCallback, errorCallback);
		}

		function getPrequalCode(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/prequal/getPrequalCode").then(successCallback, errorCallback);
		}

		function selectposition(successCallback, errorCallback) {
		    var param = { Keyword: "COMMITE_PREQUAL" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function selectemployee(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/prequal/selectemployee', param).then(successCallback, errorCallback);
		}
		function getCompanyScale(successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/prequal/getCompanyScale").then(successCallback, errorCallback);
		}
		function getTechnicalClassification(successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/prequal/getTechnicalClassification").then(successCallback, errorCallback);
		}
		function getBusinessField(successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/prequal/getBusinessField").then(successCallback, errorCallback);
		}
	}
})();