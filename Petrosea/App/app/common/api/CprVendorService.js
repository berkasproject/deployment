﻿(function () {
    'use strict';

    angular.module("app").factory("CprVendorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            select: select,
            selectbyid: selectbyid,
            CPRbyContract: CPRbyContract,
            submitbyvendor: submitbyvendor,
            selectDocsCPRId:selectDocsCPRId
            //cekRole: cekRole
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cpr-vendor/select", param).then(successCallback, errorCallback);
        }

        function selectbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cpr-vendor/selsectbyId", param).then(successCallback, errorCallback);
        }

        function CPRbyContract(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cpr-vendor/cprByContract", param).then(successCallback, errorCallback);
        }

        function submitbyvendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cpr-vendor/submitbyvendor", param).then(successCallback, errorCallback);
        }

        function selectDocsCPRId(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cpr-vendor/selectDocsCPRId", param).then(successCallback, errorCallback);
        }

        /*
        function cekRole(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cpr-vendor/cekRole").then(successCallback, errorCallback);
        }
        */
    }
})();