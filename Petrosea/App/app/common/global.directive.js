﻿(function () {
	'use strict';

    angular.module("app")
        .directive('maskingNpwp', npwp)
        .directive('maskingTelepon', function () {
		return {
			restrict: "EA",
			link: function (scope, elm, attr) {
				$(elm).mask("(999) 999-9999");
			}
		};})
        .directive('noSpace', nospace)
        .directive('fileReader', function () {
            return {
                scope: {
                    fileReader: "="
                },
                link: function (scope, element) {
                    $(element).on('change', function (changeEvent) {
                        var files = changeEvent.target.files;
                        if (files.length) {
                            var r = new FileReader();
                            r.onload = function (e) {
                                var contents = e.target.result;
                                scope.$apply(function () {
                                    scope.fileReader = contents;
                                    scope.testing = contents;
                                });
                            };

                            r.readAsText(files[0]);
                        }
                    });
                }
            };
        })
        .directive('bismillah', function () {
            return {
                restrict: 'C',
                link: function (scope, elt, attrs) {
                    alert('Tree clicked!');
                    $(elt).tree();
                }
            };
        });


	function npwp() {
		return {
			restrict: "EA",
			link: function (scope, elm, attr) {
				$(elm).mask("99.999.999.9-999.999");
			}
		};
	}

	function nospace() {
		return {
			restrict: "EA",
			link: function (scope, elm, attr) {
				$(elm).on({
					keydown: function (e) {
						if (e.which === 32) {
							return false;
						}
					},
					change: function () {
						this.value = this.value.replace(/\s/g, "");
					}
				});
			}
		};
	}
})();