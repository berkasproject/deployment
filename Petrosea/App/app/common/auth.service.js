﻿(function () {
	'use strict';

	angular.module('app').factory('AuthService', authService);

	authService.$inject = ['$http', '$q', '$rootScope', 'GlobalConstantService'];

	//@ngInject
	function authService($http, $q, $rootScope, GlobalConstantService) {
		var getAuth = GlobalConstantService.getConstant('auth_endpoint');
		var getLoginUrl = GlobalConstantService.getConstant('login_endpoint');
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		var logoutEndpoint = GlobalConstantService.getConstant("logout_endpoint");
		var api = GlobalConstantService.getConstant('api');
		var headers = {};

		//interfaces
		var service = {
			isLoggedIn: isLoggedIn,
			login: login,
			getMenus: getMenus,
			getUserLogin: getUserLogin,
			getRoleUserLogin: getRoleUserLogin,
			logout: logout,
			changePassword: changePassword,
			getUserData: getUserData,
			remove: remove,
			saveUser: saveUser
		};

		return service;

		//public methods
		function getUserData(param, successCallback, errorCallback) {
			GlobalConstantService.post(api + "/api/User/getUserData", param).then(successCallback, errorCallback);
		}

		function saveUser(param, successCallback, errorCallback) {
			GlobalConstantService.post(api + '/api/User/SaveUser', param).then(successCallback, errorCallback);
		}

		function remove(list, data) {
			var i = 0;

			while (i < (list.length)) {
				if (list[i] === data) {
					list.splice(i, 1);
					return list;
				}
				i++;
			}
		}

		function changePassword(param, successCallback, errorCallback) {
			GlobalConstantService.post(api + '/api/User/ChangePassword', param).then(successCallback, errorCallback);
		}

		function getRoleUserLogin(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/GetRoles", param).then(successCallback, errorCallback);
		}
		function getUserLogin(successCallback, errorCallback) {
			GlobalConstantService.get(getAuth + "/GetBaseData").then(successCallback, errorCallback);
		}

		function isLoggedIn(successCallback, errorCallback) {
			getIsLoggedIn().then(successCallback, errorCallback);
		}

		function login(loginModel, successCallback, errorCallback) {
			doLogin(loginModel.username, loginModel.password).then(successCallback, errorCallback);
		}

		function getMenus(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/menu-roles").then(successCallback, errorCallback);
		}

		function logout(successCallback, errorCallback) {
			GlobalConstantService.get(logoutEndpoint).then(successCallback, errorCallback);
		}

		//private methods
		function getIsLoggedIn() {
			var def = $q.defer();

			headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

			$http.get(getAuth + '/isloggedin', { headers: headers }).then(function (data) {
				def.resolve(data);
			}, function () {
				def.reject('Failed to get data');
			});

			return def.promise;
		}

		function doLogin(username, password) {
			var def = $q.defer();
			var encodedUsername = encodeURIComponent(username);


			$http.post(getLoginUrl, 'grant_type=password&username=' + encodedUsername + '&password=' + password, {
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function (data) {
				def.resolve(data);
			}, function (error) {
				def.reject(error);
				//def.reject('Failed to get data');
			});

			return def.promise;
		}
	}
})();