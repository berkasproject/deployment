﻿(function () {
	'use strict';

	angular.module("app")
    .directive('numberMasking', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue === undefined)
                        return '';
                    var transformedInput = inputValue.replace(/[^0-9]/g, '');
                    if (transformedInput !== inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });
            }
        };
    }).controller("MainController", mainController);

	mainController.$inject = ['$rootScope', '$uibModalStack', '$state', 'Idle', '$scope', '$window', '$translate', '$location', 'SocketService', 'PageComponentService', 'AuthService', 'GlobalConstantService', '$translatePartialLoader', 'UIControlService', 'NotificationService', '$uibModal','FileSaver'];

	function mainController($rootScope, $uibModalStack, $state, Idle, $scope, $window, $translate, $location, SocketService, PageComponentService, AuthService, GlobalConstantService, $translatePartialLoader, UIControlService, NotificationService, $uibModal, FileSaver) {
		var vm = this;
		vm.isActive = isActiveState;
		vm.setActive = setActiveState;
		vm.isLoggedIn = isLoggedIn;
		vm.moduleLayer = getModuleLayer;
		vm.logout = logout;
		vm.currUser = '';
		vm.currTime = '';
		vm.getCurrUser = getCurrUser;
		vm.getCurrLang = getCurrLang;
		vm.getLangList = getLangList;
		vm.setLang = setLang;
		vm.isAdmin = isAdmin;
		vm.redirect = redirect
		vm.requireHideSideMenu = hideSideMenu;
		vm.api_endpoint = GlobalConstantService.getConstant('api');
		vm.loadMenus = loadMenus;
		vm.mapMenu = mapMenu;
		vm.jumlahRequestAktivasi = 0;
		vm.jumlahRequestVerifikasi = 0;
		vm.jumlahRequestUbahData = 0;
		vm.jumlahRequestVerifikasiOther = 0;
		vm.jumlahSubmitUbahDataOther = 0;
		vm.bellCount = 0;
		vm.isLangChanged = false;
		vm.download = '';
		vm.list = 0;
		vm.listCount = 0;

		function checkTime(i) {
			if (i < 10) {
				i = "0" + i;
			}  // add zero in front of numbers < 10

			return i;
		}

		var pageActive = 0;
		var isChildrenActive = false;
		var hideSideBar = ['/', '/panitia', '/home', '/vendor', '/pre-daftar', '/daftar', '/daftar_kuesioner', '/pengumuman-pengadaan-client', '/login-rekanan', '/login'];

		const MODULE_LAYER_ADMIN = 1;
		const MODULE_LAYER_VENDOR = 2;

		// function declarations
		vm.initialize = initialize;
		function initialize() {
			$translatePartialLoader.addPart('main');
			vm.currUser = getCurrUser();
			//redirect();

			//if (vm.currUser != '') SocketService.emit("KickClient", vm.currUser);

			if (isLoggedIn() == 'true') {
				Idle.watch(); // ng-idle run
			} else {
				Idle.unwatch();
			}

			PageComponentService.config("app/setting/page.component.json").then(function (response) {
				vm.pageComponent = response.data;
				if (vm.isLoggedIn() === 'true' && vm.moduleLayer() === MODULE_LAYER_ADMIN) {
					loadMenus();

					setTimeout(function () {
						SocketService.emit("KickClient", vm.currUser);
					}, 2000);
				}
			}, function (err) {
				console.info(JSON.stringify(err));
				UIControlService.handleRequestError(err.data, err.status);
			});
		}

		vm.downloadManualBook = downloadManualBook;
		function downloadManualBook() {
			//console.info(vm.currRoles[0]);
			NotificationService.dataManualBook({ Keyword: vm.currRoles[0] }, function (reply) {
				if (reply.status === 200) {
					$scope.list = reply.data;
					$scope.listCount == reply.data.length;

					console.info($scope.list);
					console.info($scope.list.length);

					var item = {
						list: $scope.list,
						count: $scope.list.length
					}
					var modalInstance = $uibModal.open({
						templateUrl: 'app/modules/panitia/manual-book/manualBook.modal.html',
						controller: ManualBookModalCtrl,
						controllerAs: ManualBookModalCtrl,
						resolve: { item: function () { return item } }
					})
					modalInstance.result.then(function () {

					})
				}
			}, function (err) {
				//nothing to do here.
			});
		}

		var ManualBookModalCtrl = function ($scope, $uibModalInstance, item, $http, $location, $window, $sce) {
			$scope.url = "http://localhost:" + 52738 + "/UploadRoot/Manual Book/";
			$scope.countCheck = 0;
			$scope.is_checked_all = 0;
			$scope.tempFile = [];

			$scope.init = function () {
				$scope.list = item.list;
				$scope.listCount = item.count;
			}

			$scope.cancel = function () {
				$uibModalInstance.close();
			}

			//var saveData = (function () {
			//	var a = document.createElement("a");
			//	document.body.appendChild(a);
			//	a.style = "display: none";
			//	return function (data, fileName) {
			//		var json = JSON.stringify(data),
			//			blob = new Blob([json], { type: "application/pdf" }),
			//			url = window.URL.createObjectURL(blob);
			//		a.href = url;
			//		a.download = fileName;
			//		a.click();
			//		window.URL.revokeObjectURL(url);
			//	};
			//}());

			$scope.getFile = function (param) {
				var path = $scope.url + param.Path_file;
				window.open(path, '_blank', '');

				
				//var blob = new Blob([path], { type: 'application/pdf;base64'});
				//var fileURL = URL.createObjectURL(blob);
				//FileSaver.saveAs(blob, param.Path_file);
				//window.open(fileURL)
				//saveData(path, param.Path_file);
				// var file = new Blob([ $scope.html], {type: 'application/pdf'});
				//	var fileURL = URL.createObjectURL(file);
				// $scope.content = fileURL;
				////console.info(param.Path_file);
				//var path = $scope.url + param.Path_file;
				//var defaultFileName = "";
				//var blob = "";

				//defaultFileName = param.Deskripsi;
				//blob = new Blob([path], {
				//	type: "application/pdf"
				//});
				//saveAs(blob, defaultFileName);
				//console.info(path);

				//NotificationService.downloadManualBook({ Keyword: param.Path_file }, function (reply) {
				//	console.info(reply);
				//	if (reply.status === 200) {
				//		//var file = new Blob([data], { type: 'application/pdf' });
				//		saveAs(reply.data, 'filename.pdf');
				//	}
				//}, function (err) {
				//	//nothing to do here.
				//});
				

			}

			$scope.checkAll = function () {
				var checkBox = document.getElementById("selectAll");
				if (checkBox.checked == true) {
					for (var i = 0; i < $scope.list.length; i++) {
						$scope.list[i].is_checked = 1;
						$scope.countCheck = 1;
						//$scope.list[i].trx_modul_mst_id = $scope.Module[i].id_module;
					}
				} else {
					for (var i = 0; i < $scope.list.length; i++) {
						$scope.list[i].is_checked = 0;
						$scope.countCheck = 0;
						//$scope.list[i].trx_modul_mst_id = $scope.Module[i].id_module;
					}
				}
			};   

			$scope.option = function (param, index) {
				//console.info(param);
				var checkBox = document.getElementById("selectAll");
				checkBox.checked = false;
				if (param.is_checked == 1) {
					$scope.list[index].is_checked = 0;
				} else {
					$scope.list[index].is_checked = 1;
				}

				for (var i = 0; i < $scope.list.length; i++) {
					if ($scope.list[i].is_checked == 1) {
						$scope.tempFile.push($scope.list[i].Path_file);
                    }
				}
			}

        }


		SocketService.on('txServerDateTime', function (reply) {
			vm.currTime = reply;
			vm.bellCount = Number(vm.jumlahRequestAktivasi + vm.jumlahRequestVerifikasi + vm.jumlahRequestUbahData + vm.jumlahSubmitUbahData + vm.jumlahRequestVerifikasiOther + vm.jumlahSubmitUbahDataOther);
			//SocketService.emit("realtimeReview", {ContractManagementID:1,userID:2});

		});

		$scope.$on('IdleTimeout', function () {
			//$uibModalInstance.close();
			$uibModalStack.dismissAll();
			logout();
		});

		$rootScope.$on('$translateLoadingError', function (name) {
			var parts = $translatePartialLoader.getRegisteredParts();
			var firstPart = parts[0]
			var lastPart = parts[parts.length - 1]

			// remove all
			for (var i = 0; i < parts.length; i++) {
				$translatePartialLoader.deletePart(parts[i], true)
			}

			$translatePartialLoader.addPart(firstPart)
			$translatePartialLoader.addPart(lastPart)
		});

		$scope.$on('$stateChangeStart', function (event, toState) {
			var returnUrl = toState.url.replace(/\//g, '_');
			if (toState.url === '/rfqvhs/approval' && isLoggedIn() !== 'true') {
				window.location.href = '/#/login/' + returnUrl;
			} else if (toState.url === '/rfqvhs' && isLoggedIn() !== 'true') {
				window.location.href = '/#/login/' + returnUrl;
			} else if (toState.url === '/requisition-list' && isLoggedIn() !== 'true') {
				window.location.href = '/#/login/' + returnUrl;
			} else if (toState.url === '/approval-totalEval' && isLoggedIn() !== 'true') {
				window.location.href = '/#/login/' + returnUrl;
			} else if (toState.url === '/rfqGoods-draft-approval' && isLoggedIn() !== 'true') {
				window.location.href = '/#/login/' + returnUrl;
			} else if (toState.url === '/approval-no-sap' && isLoggedIn() !== 'true') {
				window.location.href = '/#/login/' + returnUrl;
			}
		});

		SocketService.on('KickClient', function (reply) {
			if (isLoggedIn() === 'true' && getCurrUser() === reply) {
				logout();
			}
		});

		SocketService.on('GetCid', function (reply) {
			if (isLoggedIn() === 'true') {
				NotificationService.kickUsers(reply, function (reply1) {
					if (reply1.status === 200) { }
				}, function (err) {
					//nothing to do here.
				});
			}
		});

		SocketService.on('actionDaftarRekanan', function (reply) {
			if (isLoggedIn() === 'true' && (vm.currRoles.indexOf('APPLICATION.ROLE_ADMINPROC') !== -1 || vm.currRoles.indexOf('APPLICATION.ROLE_MGRPROC') !== -1)) {
				NotificationService.activationReqCount(function (reply) {
					if (reply.status === 200) {
						vm.jumlahRequestAktivasi = reply.data[0];
						vm.jumlahRequestVerifikasi = reply.data[1];
						vm.jumlahRequestVerifikasiOther = reply.data[2];
					}
				}, function (err) {
					//nothing to do here.
				});
			}
		});

		SocketService.on('actionPermintaanUbahData', function (reply) {
			if (isLoggedIn() === 'true' && (vm.currRoles.indexOf('APPLICATION.ROLE_ADMINPROC') !== -1 || vm.currRoles.indexOf('APPLICATION.ROLE_MGRPROC') !== -1)) {
				NotificationService.dataChgReqCount(function (reply) {
					if (reply.status === 200) {
						vm.jumlahRequestUbahData = reply.data;
					}
				}, function (err) {
					//nothing to do here.
				});
			}
		});

		SocketService.on('submitUbahData', function (reply) {
			if (isLoggedIn() === 'true' && (vm.currRoles.indexOf('APPLICATION.ROLE_ADMINPROC') !== -1 || vm.currRoles.indexOf('APPLICATION.ROLE_MGRPROC') !== -1)) {
				NotificationService.dataChgSubmitCount(function (reply) {
					if (reply.status === 200) {
						vm.jumlahSubmitUbahData = reply.data[0];
						vm.jumlahSubmitUbahDataOther = reply.data[1];
					}
				}, function (err) {
					//nothing to do here.
				});
			}
		});

		SocketService.on('actionContractRequisitionApproval', function (reply) {
			if (isLoggedIn() === 'true') {
				NotificationService.dataCntrctReqApprv(function (reply) {
					if (reply.status === 200) {
						vm.jumlahRequestUbahData = reply.data;
					}
				}, function (err) {
					//nothing to do here.
				});
			}
		});

		function redirect() {
			if (isLoggedIn() === 'true') {
				if (isAdmin()) {
					$location.path('/homeadmin');
				}
			} else {
				return false;
			}
		}

		function loadMenus() {
			getCurrLang();
			//console.log("Start loading menus . . .");
			AuthService.getMenus(function (response) {
				vm.menuTemp = response.data;

				for (var i = 0; i < vm.menuTemp.length; i++) {
					if (localStorage.getItem("currLang") == 'id') {
						vm.menuTemp[i].Label = vm.menuTemp[i].locale_id
						for (var j = 0; j < vm.menuTemp[i].Children.length; j++) {
							vm.menuTemp[i].Children[j].Label = vm.menuTemp[i].Children[j].locale_id
							for (var k = 0; k < vm.menuTemp[i].Children[j].Children.length; k++) {
								vm.menuTemp[i].Children[j].Children[k].Label = vm.menuTemp[i].Children[j].Children[k].locale_id
							}
						}
					} else {
						vm.menuTemp[i].Label = vm.menuTemp[i].locale_en
						for (var j = 0; j < vm.menuTemp[i].Children.length; j++) {
							vm.menuTemp[i].Children[j].Label = vm.menuTemp[i].Children[j].locale_en
							for (var k = 0; k < vm.menuTemp[i].Children[j].Children.length; k++) {
								vm.menuTemp[i].Children[j].Children[k].Label = vm.menuTemp[i].Children[j].Children[k].locale_en
							}
						}
					}
				}

				vm.menus = mapMenu(vm.menuTemp);
				vm.menusMin = mapMenuMin(vm.menuTemp);

				var a = 0;
			}, function (response) {
				if (!hideSideMenu()) {
					UIControlService.handleRequestError(response.Message);
					$location.path('/login');
				}
			});

			setTimeout(function () {
				vm.jumlahRequestAktivasi = 0;
				vm.jumlahRequestVerifikasi = 0;
				vm.jumlahRequestUbahData = 0;
				vm.jumlahSubmitUbahData = 0;

				if (isLoggedIn() === 'true' && (vm.currRoles.indexOf('APPLICATION.ROLE_ADMINPROC') !== -1 || vm.currRoles.indexOf('APPLICATION.ROLE_MGRPROC') !== -1)) {
					SocketService.emit("daftarRekanan");
					SocketService.emit("PermintaanUbahData");
					SocketService.emit("SubmitUbahData");
				}
			}, 2000);
		}

		function mapMenuMin(menus) {
			var result = [];
			for (var i = 0; i < menus.length; i++) {
				result.push({
					//title: menus[i].Label,
					//state: menus[i].StateName,
					iconClass: menus[i].Icon,
					IsChecked: menus[i].IsChecked
				});
			}
			vm.menuLoaded = true;
			return result;
		}

		function mapMenu(menus) {
			var result = [];
			for (var i = 0; i < menus.length; i++) {
				var submenus = [];
				if (menus[i].Children.length > 0) {
					for (var j = 0; j < menus[i].Children.length; j++) {
						var subs = [];
						var current = menus[i].Children[j];
						if (current.Children.length > 0) {
							for (var k = 0; k < current.Children.length; k++) {
								subs.push({
									title: current.Children[k].Label,
									state: current.Children[k].StateName,
									iconClass: current.Children[k].Icon,
									IsChecked: current.Children[k].IsChecked,
									submenu: []
								});
							}
						}
						submenus.push({
							title: current.Label,
							state: current.StateName,
							iconClass: current.Icon,
							IsChecked: current.IsChecked,
							submenu: subs
						});
					}
				}
				result.push({
					title: menus[i].Label,
					state: menus[i].StateName,
					iconClass: menus[i].Icon,
					IsChecked: menus[i].IsChecked,
					submenu: submenus
				});
			}
			vm.menuLoaded = true;
			return result;
		}

		vm.menujuAktivasi = menujuAktivasi;
		function menujuAktivasi() {
			$location.path('/data-rekanan/verifikasi-data/');
		}

		vm.toChangeRequest = toChangeRequest;
		function toChangeRequest() {
			$location.path('/data-rekanan/cr-openlock/');
		}

		vm.toCRSubmitted = toCRSubmitted;
		function toCRSubmitted() {
			$location.path('/data-rekanan/cr-openlock//6///');
		}

		function isActiveState(path) {
			//return path === $location.path();
			return pageActive === path;
		}

		function logout() {
			$state.transitionTo('logout', {});
			//$window.refresh();
		}

		function setActiveState(state) {
			pageActive = state;
			//pageActive = state;
			//isChildrenActive = childrenActive;
		}

		function setLang(lang) {
			localStorage.setItem('currLang', lang);
			$translate.preferredLanguage(getCurrLang()); //untuk ganti bahasa

			$translate.use(getCurrLang());
			$translate.refresh(getCurrLang());
			//$window.location.reload();

			AuthService.getMenus(function (response) {
				var menuTemp = response.data;

				for (var i = 0; i < menuTemp.length; i++) {
					if (localStorage.getItem("currLang") == 'id') {
						menuTemp[i].Label = menuTemp[i].locale_id
						for (var j = 0; j < menuTemp[i].Children.length; j++) {
							menuTemp[i].Children[j].Label = menuTemp[i].Children[j].locale_id
							for (var k = 0; k < menuTemp[i].Children[j].Children.length; k++) {
								menuTemp[i].Children[j].Children[k].Label = menuTemp[i].Children[j].Children[k].locale_id
							}
						}
					} else {
						menuTemp[i].Label = menuTemp[i].locale_en
						for (var j = 0; j < menuTemp[i].Children.length; j++) {
							menuTemp[i].Children[j].Label = menuTemp[i].Children[j].locale_en
							for (var k = 0; k < menuTemp[i].Children[j].Children.length; k++) {
								menuTemp[i].Children[j].Children[k].Label = menuTemp[i].Children[j].Children[k].locale_en
							}
						}
					}
				}

				vm.menus = vm.mapMenu(menuTemp);
				vm.menusMin = mapMenuMin(vm.menuTemp);
				if (vm.isLangChanged)
					vm.isLangChanged = false
				else
					vm.isLangChanged = true
			});
		}

		function getCurrUser() {
			if (localStorage.getItem('username') === null || localStorage.getItem("username") === '') return '';
			if (localStorage.getItem('roles') === null || localStorage.getItem('roles') === '') return '';

			var usr = localStorage.getItem("username").toString();
			var roles = localStorage.getItem('roles').toString().split(',');

			vm.currUser = usr;
			vm.currRoles = roles;

			return usr;
		}

		function getCurrLang() {
			if (localStorage.getItem("currLang") === null || localStorage.getItem("currLang") === '') return '';
			return localStorage.getItem("currLang");
		}

		function isLoggedIn() {
			var result = GlobalConstantService.getLoginState();// === "true" || GlobalConstantService.getLoginState() === 1;
			//return GlobalConstantService.getLoginState() === "true" || GlobalConstantService.getLoginState() === 1;

			return result;
		}

		function getModuleLayer() {
			return Number(GlobalConstantService.getModuleLayer());
		}

		var langList = new Array();
		langList = [{
			id: 'id',
			bahasa: 'Bahasa Indonesia'
		}, {
			id: 'en',
			bahasa: 'English'
		}];

		function getLangList() {
			//var langList = new Array();
			//langList = ['id', 'en'];

			return langList;
		}

		function isAdmin() {
			var result = false;

			if (localStorage.getItem("roles") === null || localStorage.getItem("roles") === '') return false;
			var roles = localStorage.getItem("roles").toString().split(',');

			for (var i = 0; i < roles.length; i++)
				if (roles[i] === "SYSTEM.ROLE_ADMIN")
					result = true;

			return result;
		}

		function hideSideMenu() {
			var path = $location.path();
			//console.info(path);
			return hideSideBar.indexOf(path) >= 0;
		}

		// services and events
		SocketService.on('feed', function (data) {
			vm.serverTime = data.timestamp;
		});

		SocketService.on('OK', function (data) {
			alert(data.toString());
		});
	}
})();