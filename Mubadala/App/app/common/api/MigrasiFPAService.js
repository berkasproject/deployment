﻿(function () {
    'use strict';

    angular.module('app').factory('dataUploadMigrasiFPAService', svc);

    svc.$inject = ['$http', '$q', 'GlobalConstantService'];

    //@ngInject
    function svc($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

        //interfaces
        var service = {
            Select: Select,
            Insert: Insert,
            Detail: Detail,
            Compare: Compare,
            InsertFile: InsertFile,
            DeleteFile: DeleteFile
        };

        return service;


        function DeleteFile(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectDataupload/deleteFile', model).then(successCallback, errorCallback);
        }
        function Select(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectDataUpload', param).then(successCallback, errorCallback);
        }
        function InsertFile(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/selectDataupload/insertFile", model).then(successCallback, errorCallback);
        }
        function Insert(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/migrasiFPA/Insert", model).then(successCallback, errorCallback);
        }
        function Detail(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/detailDataUpload", model).then(successCallback, errorCallback);
        }
        function Compare(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/compareDataUpload", model).then(successCallback, errorCallback);
        }
    }
})();