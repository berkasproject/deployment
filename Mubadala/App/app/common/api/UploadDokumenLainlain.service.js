(function () {
    'use strict';

    angular.module("app").factory("UploadDokumenLainlainService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            all: all,
            Select: Select,
            insert: insert,
            Update: Update,
            remove: remove,
            SelectVend: SelectVend,
            isUploaded:isUploaded
        };

        return service;

        // implementation
        function all(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/dokumen").then(successCallback, errorCallback);
        }
        function Select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/select", param).then(successCallback, errorCallback);
        }
        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/insert", param).then(successCallback, errorCallback);
        }

        function Update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/update", param).then(successCallback, errorCallback);
        }

        function remove(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/remove", param).then(successCallback, errorCallback);
        }
        function SelectVend(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/getByVendorID", param).then(successCallback, errorCallback);
        }
        function isUploaded(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dokumen/isUploaded", param).then(successCallback, errorCallback);
        }

    }
})();