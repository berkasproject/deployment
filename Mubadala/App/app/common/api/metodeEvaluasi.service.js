﻿(function () {
    'use strict';

    angular.module("app").factory("MetodeEvaluasiService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            selectById: selectById,
            selectDetailById: selectDetailById,
            selectDetailCriteria: selectDetailCriteria,
            selectDCByMethod: selectDCByMethod,
            saveDetailCriteria: saveDetailCriteria,
            select: select,
            count: count,
            selectByUser: selectByUser,
            countByUser: countByUser,
            insert: insert,
            update: update,
            switchActive: switchActive,
            isUsed: isUsed,
            getEmpPos: getEmpPos,
            allowChangeGoods: allowChangeGoods,
            allowChangeService: allowChangeService
        };

        return service;

        // implementation
        function selectById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/selectbyid", param).then(successCallback, errorCallback);
        }

        function selectDetailById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/selectdetailbyid", param).then(successCallback, errorCallback);
        }

        function selectDetailCriteria(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/selectdetailcriteria", param).then(successCallback, errorCallback);
        }

        function selectDCByMethod(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/selectdcbymethod", param).then(successCallback, errorCallback);
        }

        function saveDetailCriteria(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/savedetailcriteria", param).then(successCallback, errorCallback);
        }

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/select", param).then(successCallback, errorCallback);
        }

        function count(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/count", param).then(successCallback, errorCallback);
        }

        function selectByUser(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/selectbyuser", param).then(successCallback, errorCallback);
        }

        function countByUser(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/countbyuser", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/insert", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/update", param).then(successCallback, errorCallback);
        }

        function switchActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/switchactive", param).then(successCallback, errorCallback);
        }

        function isUsed(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/isused", param).then(successCallback, errorCallback);
        }
        function getEmpPos(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/getEmployeePosition", param).then(successCallback, errorCallback);
        }

        function allowChangeGoods(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/allowchangegoods").then(successCallback, errorCallback);
        }

        function allowChangeService(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/metodeEvaluasi/allowchangeservice").then(successCallback, errorCallback);
        }
    }
})();