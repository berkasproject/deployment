﻿(function () {
    'use strict';

    angular.module("app").factory("TenderStepDateApprovalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            getsteps: getsteps,
            approve: approve,
            reject: reject
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/stepdateapproval/select", param).then(successCallback, errorCallback);
        }
        function getsteps(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/stepdateapproval/getsteps", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/stepdateapproval/approve", param).then(successCallback, errorCallback);
        }
        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/stepdateapproval/reject", param).then(successCallback, errorCallback);
        }

    }
})();