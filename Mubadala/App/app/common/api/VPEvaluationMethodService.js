﻿(function () {
    'use strict';

    angular.module("app").factory("VPEvaluationMthodService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            selectVPEvaluasi: selectVPEvaluasi,
            selectById: selectById,
            insertEval: insertEval,
            updateEval: updateEval,
            switchActive: switchActive,
            selectDetailById: selectDetailById,
            selectdetailcriteria: selectdetailcriteria,
            saveDetailCriteria: saveDetailCriteria,
            selectDCbyID: selectDCbyID,
            getScoreforCpr: getScoreforCpr,
            isLevel3Complete: isLevel3Complete,
            publish: publish,
            copyevaltolevel2: copyevaltolevel2,
            isprocsupp: isprocsupp,
            ispm: ispm,
            isallowadd: isallowadd,
        };

        return service;

        // implementation
        function selectVPEvaluasi(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/select", param).then(successCallback, errorCallback);
        }

        function selectById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/selectbyid", param).then(successCallback, errorCallback);
        }

        function insertEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/insert", param).then(successCallback, errorCallback);
        }

        function updateEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/update", param).then(successCallback, errorCallback);
        }

        function switchActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/switchactive", param).then(successCallback, errorCallback);
        }

        function selectDetailById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/selectdetailbyid", param).then(successCallback, errorCallback);
        }

        function selectdetailcriteria(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/selectdetailcriteria", param).then(successCallback, errorCallback);
        }

        function saveDetailCriteria(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/savedetailcriteria", param).then(successCallback, errorCallback);
        }

        function selectDCbyID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/selectdcbymethod", param).then(successCallback, errorCallback);
        }

        function getScoreforCpr(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/getscoreforcpr", param).then(successCallback, errorCallback);
        }

        function isLevel3Complete(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/islevel3complete", param).then(successCallback, errorCallback);
        }

        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/publish", param).then(successCallback, errorCallback);
        }

        function copyevaltolevel2(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/copyevaltolevel2", param).then(successCallback, errorCallback);
        }

        function isprocsupp(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/isprocsupp").then(successCallback, errorCallback);
        }

        function ispm(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/ispm").then(successCallback, errorCallback);
        }

        function isallowadd(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpeval/isallowadd").then(successCallback, errorCallback);
        }
    }
})();