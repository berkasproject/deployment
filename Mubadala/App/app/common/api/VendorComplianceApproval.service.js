﻿(function () {
    'use strict';

    angular.module("app")
        .factory("VendorComplianceApprovalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            dataApproval: dataApproval,
            aktaPendirian: aktaPendirian,
            approve:approve
        };
        return service;

        // implementation

        function dataApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vendorComplianceApproval/dataApproval", param).then(successCallback, errorCallback);
        }
        function aktaPendirian(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vendorComplianceApproval/aktaPendirian", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/vendorComplianceApproval/approve", param).then(successCallback, errorCallback);
        }
    }
})();