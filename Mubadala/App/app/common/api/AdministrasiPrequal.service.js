﻿(function () {
    'use strict';

    angular.module("app").factory("AdministrasiPrequalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            loadCommodity: loadCommodity,
            loadCurrency: loadCurrency,
            loadContact: loadContact,
            insert: insert,
            loadPrequalStep: loadPrequalStep,
            Submit: Submit,
            CekVendorEntryPrequal: CekVendorEntryPrequal,
            InsertBusinessField: InsertBusinessField,
            SubmitBusinessField: SubmitBusinessField,
            deleteBusinessField: deleteBusinessField,
            SelectBusinessFieldPrequal: SelectBusinessFieldPrequal,
            cekSubmit: cekSubmit
        }
        return service;

        // implementation
        function cekSubmit(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/cekSubmit", param).then(successCallback, errorCallback);
        }
        function SelectBusinessFieldPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/businessField/goodsorservice", param).then(successCallback, errorCallback);
        }
        function deleteBusinessField(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/deleteBusinessField", param).then(successCallback, errorCallback);
        }
        function SubmitBusinessField(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/SubmitBusinessField", param).then(successCallback, errorCallback);
        }
        function InsertBusinessField(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/InsertBusinessFieldPrequal", param).then(successCallback, errorCallback);
        }
        function CekVendorEntryPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/CekVendorEntry", param).then(successCallback, errorCallback);
        }
        function Submit(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/Submit", param).then(successCallback, errorCallback);
        }
        function loadPrequalStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
        }
        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/insertAdministrasiPrequal", param).then(successCallback, errorCallback);
        }
        function loadCommodity(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadVendorCommodity", param).then(successCallback, errorCallback);
        }
        function loadCurrency(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadVendorCurrency", param).then(successCallback, errorCallback);
        }
        function loadContact(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadVendorContact", param).then(successCallback, errorCallback);
        }
    }
})();