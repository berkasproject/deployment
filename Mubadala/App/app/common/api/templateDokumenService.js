﻿(function () {
	angular.module("app").factory("TemplateDokumenService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		var service = {
			selectTemplate: selectTemplate,
			selectTemplateByID: selectTemplateByID,
			saveTemplate: saveTemplate
		};

		return service;

		function selectTemplate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/template-dokumen/selectTemplate", param).then(successCallback, errorCallback);
		}
		function selectTemplateByID(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/template-dokumen/selectTemplateByID", param).then(successCallback, errorCallback);
		}
		function saveTemplate(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/template-dokumen/saveTemplate", param).then(successCallback, errorCallback);
		}
	}
})();