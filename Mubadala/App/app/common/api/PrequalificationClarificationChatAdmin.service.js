﻿(function () {
	'use strict';

	angular.module("app").factory("PrequalificationClarificationChatAdminService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			GetStepInfo: GetStepInfo,
			GetVendorInfo: GetVendorInfo,
			GetVendors: GetVendors,
			GetChats: GetChats,
			AddChat: AddChat,
			SendMailToVendor: SendMailToVendor,
			GetIsPeriodOpen: GetIsPeriodOpen,
			SendDraft: SendDraft,
			DeleteDraft: DeleteDraft,
			EditChat: EditChat,
			reviseVendor: reviseVendor
		};

		return service;

		function reviseVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/reviseVendor", param).then(successCallback, errorCallback);
		}
		function EditChat(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/EditChat", param).then(successCallback, errorCallback);
		}
		function GetIsPeriodOpen(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/isPeriodOpen", param).then(successCallback, errorCallback);
		}
		function GetStepInfo(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/getstepinfo", param).then(successCallback, errorCallback);
		}
		function GetVendorInfo(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/getvendorinfo", param).then(successCallback, errorCallback);
		}
		function GetVendors(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/getvendors", param).then(successCallback, errorCallback);
		}
		function GetChats(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/getchats", param).then(successCallback, errorCallback);
		}
		function AddChat(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/addchat", param).then(successCallback, errorCallback);
		}
		function SendMailToVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/sendmailtovendor", param).then(successCallback, errorCallback);
		}
		function SendDraft(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/SendDraft", param).then(successCallback, errorCallback);
		}
		function DeleteDraft(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequalClarification/DeleteDraft", param).then(successCallback, errorCallback);
		}
	}
})();