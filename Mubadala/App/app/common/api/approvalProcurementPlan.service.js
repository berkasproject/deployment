﻿(function () {
    'use strict';

    angular.module("app").factory("ApprovalProcurementPlanService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            selectProcurementPlan: selectProcurementPlan,
            selectEntity: selectEntity,
            selectApproverProcurementPlan: selectApproverProcurementPlan,
            updateApproval:updateApproval,
            sendMail: sendMail,
            getProcurementPlan: getProcurementPlan,
            getDept: getDept,
            getTFP: getTFP,
            getEntity: getEntity,
            getMaterialService: getMaterialService,
            getSubSector: getSubSector,
            getProcPlanStatus: getProcPlanStatus,
            getMarketSector: getMarketSector,
            getCategory: getCategory,
            getExistingContract: getExistingContract,
            getExistingContractAward: getExistingContractAward,
            getSCMFocalPoint: getSCMFocalPoint,
            getSourcingActivity: getSourcingActivity,
            getContractType: getContractType,
            selectApproverProcurementPlanSCM: selectApproverProcurementPlanSCM
        };

        return service;

        // implementation
        function selectProcurementPlan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/selectProcurementPlan", param).then(successCallback, errorCallback);
        }

        function selectEntity(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/selectEntity", param).then(successCallback, errorCallback);
        }

        function selectApproverProcurementPlan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/selectApproverProcurementPlan", param).then(successCallback, errorCallback);
        }

        function updateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/updateApproval", param).then(successCallback, errorCallback);
        }

        function sendMail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/sendMail", param).then(successCallback, errorCallback);
        }

        function getProcurementPlan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/getProcurementPlan", param).then(successCallback, errorCallback);
        }

        function getTFP(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/getTechnicalFocalPoint", param).then(successCallback, errorCallback);
        }

        function getDept(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getDept").then(successCallback, errorCallback);
        }

        function getEntity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getEntity").then(successCallback, errorCallback);
        }

        function getMaterialService(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getMaterialService").then(successCallback, errorCallback);
        }

        function getSubSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getSubSector").then(successCallback, errorCallback);
        }

        function getProcPlanStatus(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getProcPlanStatus").then(successCallback, errorCallback);
        }

        function getMarketSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getMarketSector").then(successCallback, errorCallback);
        }

        function getCategory(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getCategory").then(successCallback, errorCallback);
        }

        function getExistingContract(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getExistingContract").then(successCallback, errorCallback);
        }

        function getExistingContractAward(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getExistingContractAward").then(successCallback, errorCallback);
        }

        function getSCMFocalPoint(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getSCMFocalPoint").then(successCallback, errorCallback);
        }

        function getSourcingActivity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getSourcingActivity").then(successCallback, errorCallback);
        }

        function getContractType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/approval-procurement-plan/getContractType").then(successCallback, errorCallback);
        }

        function selectApproverProcurementPlanSCM(param,successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/selectApproverProcurementPlanSCM",param).then(successCallback, errorCallback);
        }

        
    }
})();