﻿(function () {
    'use strict';

    angular.module("app").factory("PrequalQuestionnaireService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            getVendorPrequalQuest: getVendorPrequalQuest,
            getParents: getParents,
            getSubCriterias: getSubCriterias,
            saveQuestionnaire: saveQuestionnaire
        }
        return service;

        // implementation
        function getVendorPrequalQuest(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalquest/getvendorprequalquest", param).then(successCallback, errorCallback);
        }
        function getParents(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalquest/getparents", param).then(successCallback, errorCallback);
        }
        function getSubCriterias(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalquest/getsubcriterias", param).then(successCallback, errorCallback);
        }
        function saveQuestionnaire(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/prequalquest/savequestionnaire", param).then(successCallback, errorCallback);
        }
    }
})();