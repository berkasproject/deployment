﻿(function () {
	'use strict';

	angular.module("app").factory("RateService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
		    all: all,
		    Select: Select,
            Insert: Insert
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {
	    //GlobalConstantService.get(endpoint + "").then(successCallback, errorCallback);
		}

		function Select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/rate/select", param).then(successCallback, errorCallback);
		}

		function Insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/rate/insert", param).then(successCallback, errorCallback);
		}
	}
})();