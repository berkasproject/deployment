﻿(function () {
    'use strict';

    angular.module("app").factory("MasterQualificationService", service);

    service.$inject = ['$upload', 'GlobalConstantService'];

    /* @ngInject */
    function service($upload, GlobalConstantService) {
        var endpoint = GlobalConstantService.getConstant("admin_endpoint");

        // interfaces
        var service = {
            GetQualifications: GetQualifications,
            UpdateQualification: UpdateQualification,
        };

        return service;

        function GetQualifications(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstqualification/getqualifications").then(successCallback, errorCallback);
        }
        function UpdateQualification(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstqualification/update", param).then(successCallback, errorCallback);
        }
    }
})();