﻿(function () {
	'use strict';

	angular.module("app").factory("BusinessFieldService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");

		// interfaces
		var service = {
			select: select,
			getCommodities: getCommodities,
			remove: remove,
			create: create,
			getByID: getByID,
			update: update,
			getLicenses: getLicenses,
			inactivate: inactivate
		};

		return service;

		// implementation
		function select(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField', model).then(successCallback, errorCallback);
		}

		function getCommodities(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField/availCommodities', model).then(successCallback, errorCallback);
		}

		function getLicenses(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField/availLicenses', model).then(successCallback, errorCallback);
		}

		function create(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField/create', model).then(successCallback, errorCallback);
		}

		function remove(list, data) {
			var i = 0;

			while (i < (list.length)) {
				if (list[i] === data) {
					list.splice(i, 1);
					return list;
				}
				i++;
			}
		}

		function getByID(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField/getByID', model).then(successCallback, errorCallback);
		}

		function update(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField/update', model).then(successCallback, errorCallback);
		}

		function inactivate(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/businessField/inactivate', model).then(successCallback, errorCallback);
		}
	}
})();