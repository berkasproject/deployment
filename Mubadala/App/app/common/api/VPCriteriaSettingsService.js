﻿(function () {
    'use strict';

    angular.module("app").factory("VPCriteriaSettings", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            count: count,
            select: select,
            insert: insert,
            update: update,
            hapus: hapus,
            getType: getType,
            getstandards: getstandards,
            getallbfcomm: getallbfcomm,
            selectedbfcomm: selectedbfcomm,
            getVendorTypes: getVendorTypes
        };

        return service;

        // implementation
        function count(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/count", param).then(successCallback, errorCallback);
        }

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/select", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/insert", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/update", param).then(successCallback, errorCallback);
        }

        function hapus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/delete", param).then(successCallback, errorCallback);
        }

        function getType(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getType").then(successCallback, errorCallback);
        }
        
        function getstandards(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getstandard", param).then(successCallback, errorCallback);
        }

        function getallbfcomm(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getallbfcomm").then(successCallback, errorCallback);
        }

        function selectedbfcomm(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/selectedbfcomm", param).then(successCallback, errorCallback);
        }

        function getVendorTypes(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getVendorTypes").then(successCallback, errorCallback);
        }

    }
})();