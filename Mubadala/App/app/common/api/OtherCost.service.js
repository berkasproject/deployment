﻿(function () {
    'use strict';

    angular.module("app").factory("OtherCostService", service);

    service.$inject = ['$upload', 'GlobalConstantService'];

    /* @ngInject */
    function service($upload, GlobalConstantService) {
        var endpoint = GlobalConstantService.getConstant("admin_endpoint");

        // interfaces
        var service = {
            Select: Select,
            GetById: GetById,
            InsertOtherCost: InsertOtherCost,
            UpdateOtherCost: UpdateOtherCost,
            DeleteOtherCost: DeleteOtherCost
        };

        return service;

        function Select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstothercost/select", param).then(successCallback, errorCallback);
        }
        function GetById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstothercost/getbyid", param).then(successCallback, errorCallback);
        }
        function InsertOtherCost(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstothercost/insertothercost", param).then(successCallback, errorCallback);
        }
        function UpdateOtherCost(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstothercost/updateothercost", param).then(successCallback, errorCallback);
        }
        function DeleteOtherCost(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/mstothercost/deleteothercost", param).then(successCallback, errorCallback);
        }
    }
})();