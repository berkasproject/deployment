﻿(function () {
    'use strict';

    angular.module("app").factory("LocalAreaPrequalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            deleteData: deleteData,
            insert: insert,
            selectAllDistrict: selectAllDistrict,
            select: select
        }
        return service;

        // implementation
        function deleteData(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MstLocalAreaPrequal/deleteData", param).then(successCallback, errorCallback);
        }
        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MstLocalAreaPrequal/Insert", param).then(successCallback, errorCallback);
        }
        function selectAllDistrict(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/getAllDistrict").then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/MstLocalAreaPrequal", param).then(successCallback, errorCallback);
        }
    }
})();