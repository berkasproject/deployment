﻿(function () {
	'use strict';

	angular.module("app").factory("BankDetailService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			Select: Select,
			insert: insert,
			Update: Update,
			remove: remove,
			SelectVend: SelectVend,
			getCRbyVendor: getCRbyVendor,
			getCurrencies: getCurrencies,
            cekCR: cekCR,
			getDocumentForDownload:getDocumentForDownload,
			saveStatementLetter: saveStatementLetter,
			cekVendor: cekVendor,
			cekVendorBankDetail: cekVendorBankDetail,
			getDataCIVD: getDataCIVD,
            saveCIVD: saveCIVD
		};

		return service;

	    // implementation
		function getCurrencies(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/bankdetail/currency/list")
                .then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/bank").then(successCallback, errorCallback);
		}
		function Select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/select", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/insert", param).then(successCallback, errorCallback);
		}
		function Update(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/update", param).then(successCallback, errorCallback);
		}
		function remove(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/remove", param).then(successCallback, errorCallback);
		}
		function SelectVend(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/bank/getByVendorID", param).then(successCallback, errorCallback);
		}
		function saveStatementLetter(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/bank/saveStatementLetter", param).then(successCallback, errorCallback);
		}
		function getDocumentForDownload(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/bank/getDocumentForDownload", param).then(successCallback, errorCallback);
		}
		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
		function cekCR(successCallback, errorCallback) {
		    GlobalConstantService.get(vendorpoint + "/changerequest/cekCR").then(successCallback, errorCallback);
		}

		function cekVendor(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/afiliasi/cekVendor").then(successCallback, errorCallback);
		}

		function cekVendorBankDetail(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/bank/cekVendorBankDetail").then(successCallback, errorCallback);
		}

		function getDataCIVD(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/afiliasi/getCIVDData", param).then(successCallback, errorCallback);
		}
        
		function saveCIVD(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/bank/saveCIVD", param).then(successCallback, errorCallback);
		}
	}
})();