﻿(function () {
    'use strict';

    angular.module("app").factory("ComplainTypeService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            all: all,
            allActive:allActive,
            select: select,
            editActive: editActive,
            cekData: cekData,
            insert: insert,
            update: update
        };

        return service;

        // implementation
        function all(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/complainType").then(successCallback, errorCallback);
        }

        function allActive(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/complainTypeActive").then(successCallback, errorCallback);
        }

        function select(param, successCallback, errorCallback) {            
            GlobalConstantService.post(endpoint + "/complainType/select", param).then(successCallback, errorCallback);
        }

        function editActive(param, successCallback, errorCallback) {
            console.info("serv:" + JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/complainType/editActive", param).then(successCallback, errorCallback);
        }

        function cekData(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/complainType/cek", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/complainType/insert", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/complainType/update", param).then(successCallback, errorCallback);
        }
    }
})();