(function () {
	'use strict';

	angular.module("app")
    .factory("PengumumanPengadaanService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

		// interfaces
		var service = {
		    isNeedAnnouncement: isNeedAnnouncement,
			selectPengumuman: selectPengumuman,
			getCommodity: getCommodity,
			getIncoTerms: getIncoTerms,
			getClasification: getClasification,
			getTechnical: getTechnical,
			getDataTender: getDataTender,
			getDataAnnouncementByVendor: getDataAnnouncementByVendor,
			getAllDataAnnouncementByVendor: getAllDataAnnouncementByVendor,
			insertAnnouncement: insertAnnouncement,
			getAllVendors: getAllVendors,
			insertRegistration: insertRegistration,
			getByOpenTender: getByOpenTender,
			sendEmailToVendors: sendEmailToVendors,
			sendEmailToVendor: sendEmailToVendor,
			selectDataVendor: selectDataVendor,
			loadTemplateOfferEntry: loadTemplateOfferEntry,
			getTenderReg: getTenderReg,
			getTenderRegAdmin: getTenderRegAdmin,
			GetSteps: GetSteps,
			getKelengkapanDocVendor: getKelengkapanDocVendor,
			getDataChecklistVendor: getDataChecklistVendor,
			viewVendor: viewVendor,
			cekEmails: cekEmails,
			currentAnnouncementApproval: currentAnnouncementApproval,
			sendAnnouncementApproval: sendAnnouncementApproval,
			Publish: Publish,
			sendMail: sendMail,
			getVendorEmail: getVendorEmail,
			sendMailAssosiasi: sendMailAssosiasi,
			isOvertime: isOvertime,
			getEmailContent: getEmailContent,
			getCR: getCR,
			getByAdmin: getByAdmin,
			getDeliveryPoint: getDeliveryPoint,
            insertRegistrationROI:insertRegistrationROI
		};

		return service;
	    // implementation

		function isNeedAnnouncement(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/announcement/isNeedAnnouncement", param).then(successCallback, errorCallback);
		}

		function getCR(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/getContractRequisition", param).then(successCallback, errorCallback);
		}
		function getEmailContent(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/emailcontent/select", param).then(successCallback, errorCallback);
		}
		function isOvertime(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/isOvertime", param).then(successCallback, errorCallback);
		}

		function getIncoTerms(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/getIncoTerms", param).then(successCallback, errorCallback);
		}

		function getDeliveryPoint(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/getDeliveryPoint", param).then(successCallback, errorCallback);
		}

		function sendMailAssosiasi(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/send-email-Assosiasi", param).then(successCallback, errorCallback);
		}
		function getVendorEmail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/getVendorEmail", param).then(successCallback, errorCallback);
		}
		function sendMail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/sendMail", param).then(successCallback, errorCallback);
		}
		function currentAnnouncementApproval(model, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + '/announcement/currentannouncementapproval', model).then(successCallback, errorCallback);
		}
		function sendAnnouncementApproval(model, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + '/announcement/sendannouncementapproval', model).then(successCallback, errorCallback);
		}
		function Publish(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/publish", param).then(successCallback, errorCallback);
		}
		function cekEmails(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/rfqgoods/cekEmail", param).then(successCallback, errorCallback);
		}
		function viewVendor(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/announcement/viewVendor', model).then(successCallback, errorCallback);
		}
		function selectDataVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/administrasi-select", param).then(successCallback, errorCallback);
		}
		function sendEmailToVendor(mail, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/registration/send-email", mail).then(successCallback, errorCallback);
		}
		function getKelengkapanTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getReqDocs", param).then(successCallback, errorCallback);
		}

		function sendEmailToVendors(mail, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/registration/send-email-to-vendors", mail).then(successCallback, errorCallback);
		}

		function getByOpenTender(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/announcement/ByIsOpen', model).then(successCallback, errorCallback);
		}

		function getAllVendors(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/announcement/getAllVendor', model).then(successCallback, errorCallback);
		}

		function insertAnnouncement(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/insert", param).then(successCallback, errorCallback);
		}
		function insertRegistration(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/tenderregister/register", param).then(successCallback, errorCallback);
		}
		function getDataAnnouncementByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/ByIsOpenVendor", param).then(successCallback, errorCallback);
		}
		function getAllDataAnnouncementByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/byvendor", param).then(successCallback, errorCallback);
		}
		function getDataTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/tender/getinfo", param).then(successCallback, errorCallback);
		}
		function selectPengumuman(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/getID", param).then(successCallback, errorCallback);
		}
		function getCommodity(param, successCallback, errorCallback) {
			console.info(JSON.stringify(param));
			if (param.type === 'CR') {
				GlobalConstantService.post(adminpoint + "/announcement/getBusinessFieldByType", { column: 2 }).then(successCallback, errorCallback);
			} else {
				GlobalConstantService.get(endpoint + "/businessField/getAll").then(successCallback, errorCallback);
			}
		}
		function getClasification(successCallback, errorCallback) {
			var param = { Keyword: "COMPANY_SCALE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function getTechnical(successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/announcement/getbycodetechnical").then(successCallback, errorCallback);
		}

		function loadTemplateOfferEntry(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry", param).then(successCallback, errorCallback);
		}

		function getTenderReg(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/tahapan/gettenderreg", param).then(successCallback, errorCallback);
		}

		function getTenderRegAdmin(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/tahapan/gettenderreg", param).then(successCallback, errorCallback);
		}

		function getByAdmin(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/GoodsOfferEntry/getByAdmin", param).then(successCallback, errorCallback);
		}

		function GetSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/tahapan/getsteps", param).then(successCallback, errorCallback);
		}

		function getKelengkapanDocVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getByVendorID", param).then(successCallback, errorCallback);
		}
		function getDataChecklistVendor(param, successCallback, errorCallback) {
			console.info(param);
			GlobalConstantService.post(vendorpoint + "/ServiceOfferEntry/getChecklistByVendorID", param).then(successCallback, errorCallback);
		}

		function insertRegistrationROI(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/tenderregister/registerROI", param).then(successCallback, errorCallback);
		}

	}
})();