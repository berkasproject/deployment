﻿(function () {
	'use strict';

	angular.module("app").factory("PPVHSAdminService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var Adminendpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			select: select,
			GetStep: GetStep,
			/*Insert: Insert,
            selectDetail: selectDetail,
            InsertOpen: InsertOpen,
            selectTemplate: selectTemplate,*/
			selectAll: selectAll,
			selectByTenderStepID: selectByTenderStepID,
			GetAll: GetAll,
			sendToApproval: sendToApproval,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			detailApproval: detailApproval
		};

		return service;
		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Adminendpoint + "/vhsOfferEntry", param).then(successCallback, errorCallback);
		}
		function GetStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(Adminendpoint + '/vhsOfferEntry/SelectStep', param).then(successCallback, errorCallback);
		}
		function selectAll(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Adminendpoint + "/vhsOfferEntry/selectAll", param).then(successCallback, errorCallback);
		}
		function selectByTenderStepID(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Adminendpoint + "/vhsOfferEntry/getByTenderStep", param).then(successCallback, errorCallback);
		}
		function GetAll(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(Adminendpoint + "/vhsOfferEntry/getAll", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(Adminendpoint + '/vhsOfferEntry/sendToApproval', param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(Adminendpoint + '/vhsOfferEntry/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(Adminendpoint + '/vhsOfferEntry/isApprovalSent', param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(Adminendpoint + '/vhsOfferEntry/detailApproval', param).then(successCallback, errorCallback);
		}
	}
})();