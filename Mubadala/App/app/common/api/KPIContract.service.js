(function () {
    'use strict';

    angular.module("app")
        .factory("KPIContractService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            dataKPI: dataKPI,
            tenderStepCount: tenderStepCount,
            weekByYear: weekByYear,
            dataContractPerformance: dataContractPerformance,
            dataCostSaving: dataCostSaving,
            dataIndividualCostSaving: dataIndividualCostSaving,
            exportContract:exportContract
        };
        return service;

        // implementation

        function dataKPI(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/dataKPI", param).then(successCallback, errorCallback);
        }
        function tenderStepCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/tenderStepCount", param).then(successCallback, errorCallback);
        }
        function weekByYear(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstWeek/weekByYear", param).then(successCallback, errorCallback);
        }
        function dataCostSaving(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/dataCostSaving", param).then(successCallback, errorCallback);
        }
        function dataIndividualCostSaving(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/dataIndividualCostSaving", param).then(successCallback, errorCallback);
        }
        function dataContractPerformance(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/dataContractPerformance", param).then(successCallback, errorCallback);
        }
        function exportContract(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiContract/exportContract", param).then(successCallback, errorCallback);
        }
    }
})();