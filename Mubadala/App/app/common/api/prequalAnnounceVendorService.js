﻿(function () {
	'use strict';

	angular.module('app').factory('PrequalAnnounceVendorService', serviceMethod);

	serviceMethod.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function serviceMethod(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

		// interfaces
		var service = {
			getPrequalAnnounce: getPrequalAnnounce,
			insertRegistration: insertRegistration,
			getByOpenPrequal: getByOpenPrequal,
			isRegistrationOpened: isRegistrationOpened,
            cekPrakualifikasiVendor:cekPrakualifikasiVendor
		};
		return service;

		function getPrequalAnnounce(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/prequal/getAllPrequalAnnounce", param).then(successCallback, errorCallback);
		}

		function insertRegistration(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/prequal/insertRegistration", param).then(successCallback, errorCallback);
		}

		function getByOpenPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/prequal/getByOpenPrequal", param).then(successCallback, errorCallback);
		}

		function isRegistrationOpened(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/prequal/isRegistrationOpened", param).then(successCallback, errorCallback);
		}

		function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
		}
	}
})();