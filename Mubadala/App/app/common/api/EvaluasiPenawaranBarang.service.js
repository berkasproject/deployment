﻿(function () {
    'use strict';

    angular.module("app").factory("EvaluasiPenawaranBarangService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            isItemize: isItemize,
            getPaymentTermOptions: getPaymentTermOptions,
            getCurrencyOptions: getCurrencyOptions,
            getByTenderStepData: getByTenderStepData,
            saveEvaluation: saveEvaluation,
            getOfferEntries: getOfferEntries,
            getItemPRs: getItemPRs,
            getPagedItemPRs: getPagedItemPRs,
            getTotalHistoricalValue: getTotalHistoricalValue,
            getPagedOEDetail: getPagedOEDetail,
            getOtherCosts: getOtherCosts,
            getOtherCostsbyDefault: getOtherCostsbyDefault,
            getCriterias: getCriterias,
            getCriteriasFromEMDC: getCriteriasFromEMDC,
            saveScoring: saveScoring,
            isless3approved: isless3approved,
            setItemPRAward: setItemPRAward,
            getAwardedItemPR: getAwardedItemPR,
            saveEPV: saveEPV,
            getEPVByStep: getEPVByStep,
            saveItemPRSaveEPValues: saveItemPRSaveEPValues,
            getItemDeliveryDate: getItemDeliveryDate,
            insertDeliveryDateApproval: insertDeliveryDateApproval,
            getCurrentDeliveryDateApproval: getCurrentDeliveryDateApproval
        };

        return service;

        function isItemize(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/isitemize", param).then(successCallback, errorCallback);
        }

        function getPaymentTermOptions(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getPaymentTermOptions", param).then(successCallback, errorCallback);
        }

        function getCurrencyOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getCurrencyOptions").then(successCallback, errorCallback);
        }

        function getByTenderStepData(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getbytenderstepdata", param).then(successCallback, errorCallback);
        }

        function saveEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/saveevaluation", param).then(successCallback, errorCallback);
        }
        
        function getOfferEntries(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getofferentries", param).then(successCallback, errorCallback);
        }

        function getItemPRs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getitemprs", param).then(successCallback, errorCallback);
        }

        function getPagedItemPRs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getpageditemprs", param).then(successCallback, errorCallback);
        }

        function getTotalHistoricalValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/gettotalhistoricalvalue", param).then(successCallback, errorCallback);
        }

        function getPagedOEDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getpagedoedetail", param).then(successCallback, errorCallback);
        }

        function getOtherCosts(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getothercosts", param).then(successCallback, errorCallback);
        }

        function getOtherCostsbyDefault(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getothercostsbydefault", param).then(successCallback, errorCallback);
        }

        function getCriterias(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getcriterias", param).then(successCallback, errorCallback);
        }

        function getCriteriasFromEMDC(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getcriteriasbyemdc", param).then(successCallback, errorCallback);
        }

        function saveScoring(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/savescorings", param).then(successCallback, errorCallback);
        }

        function isless3approved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/isless3approved", param).then(successCallback, errorCallback);
        }

        function setItemPRAward(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/setitempraward", param).then(successCallback, errorCallback);
        }

        function getAwardedItemPR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getawardeditempr", param).then(successCallback, errorCallback);
        }

        function saveEPV(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/saveEPV", param).then(successCallback, errorCallback);
        }

        function getEPVByStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getEPVByStep", param).then(successCallback, errorCallback);
        }

        function saveItemPRSaveEPValues(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/saveItemPREPValues", param).then(successCallback, errorCallback);
        }

        function getItemDeliveryDate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getitemdeliverydate", param).then(successCallback, errorCallback);
        }

        function insertDeliveryDateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/insertdeliverydateapproval", param).then(successCallback, errorCallback);
        }

        function getCurrentDeliveryDateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/goevaluation/getcurrentdeliverydateapproval", param).then(successCallback, errorCallback);
        }
    }
})();