﻿(function () {
	'use strict';

	angular.module("app").factory("goodsTechEvalResulDetermineVendorService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");

		var service = {
			GetStep: GetStep
		};

		return service;

		function GetStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/goodsTechEvalResulDetermineVendor/GetStep", param).then(successCallback, errorCallback);
		}
	}
})();