﻿(function () {
    'use strict';

    angular.module("app").factory("KontenEmailService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            update: update,
            selectLocalVariable: selectLocalVariable,
            selectGlobalVariable: selectGlobalVariable,
            insertGlobalVariable: insertGlobalVariable,
            updateGlobalVariable: updateGlobalVariable,
            deleteGlobalVariable: deleteGlobalVariable,
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/select", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/update", param).then(successCallback, errorCallback);
        }

        function selectLocalVariable(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/selectlocalvariable", param).then(successCallback, errorCallback);
        }

        function selectGlobalVariable(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/selectglobalvariable", param).then(successCallback, errorCallback);
        }

        function insertGlobalVariable(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/insertglobalvariable", param).then(successCallback, errorCallback);
        }

        function updateGlobalVariable(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/updateglobalvariable", param).then(successCallback, errorCallback);
        }

        function deleteGlobalVariable(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/emailcontent/deleteglobalvariable", param).then(successCallback, errorCallback);
        }

    }
})();