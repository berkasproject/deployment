﻿(function () {
	'use strict';

	angular.module('app').factory('DataKontrakService', svc);

	svc.$inject = ['GlobalConstantService'];

	//@ngInject
	function svc(GlobalConstantService) {
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		//interfaces
		var service = {

			Select: Select,
			Distribusi: Distribusi,
			VariasiKontrak: VariasiKontrak,
			insertDok: insertDok,
			DetailDok: DetailDok,
			AddDistribusi: AddDistribusi,
			EditDistribusi: EditDistribusi,
			Create: Create,
			Update: Update,
			InsertEmail: InsertEmail,
			getContent: getContent,
			docType: docType,
			selectUploadPOKontrak: selectUploadPOKontrak,
			InsertFile: InsertFile,
			Insert: Insert,
			DetailPO: DetailPO,
			Compare: Compare,
			DeleteFile: DeleteFile,
			GenerateDetailPO: GenerateDetailPO,
			setAck: setAck
		};

		return service;
		function GenerateDetailPO(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDatauploadSAPContract/GenerateDetailPO', model).then(successCallback, errorCallback);
		}
		function DeleteFile(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDatauploadSAPContract/Delete', model).then(successCallback, errorCallback);
		}
		function Compare(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/CompareUploadSAPContract', model).then(successCallback, errorCallback);
		}

		function setAck(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ContractMonitoring/setAck', model).then(successCallback, errorCallback);
		}

		function DetailPO(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDatauploadSAPContract/DetailPO', model).then(successCallback, errorCallback);
		}
		function Insert(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDatauploadSAPContract/Insert', model).then(successCallback, errorCallback);
		}
		function InsertFile(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDatauploadSAPContract/InsertFile', model).then(successCallback, errorCallback);
		}
		function selectUploadPOKontrak(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/selectDatauploadSAPContract', model).then(successCallback, errorCallback);
		}
		function getContent(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/getMailContentContractSignOff', model).then(successCallback, errorCallback);
		}
		function InsertEmail(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/ContractMonitoring/SendEmailToVendor', model).then(successCallback, errorCallback);
		}
		function Select(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/dataKontrak', model).then(successCallback, errorCallback);
		}
		function Distribusi(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/distribusiKontrak', model).then(successCallback, errorCallback);
		}

		function DetailDok(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/detailDokumen', model).then(successCallback, errorCallback);
		}

		function AddDistribusi(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/TambahDistribusi', model).then(successCallback, errorCallback);
		}
		function Create(DistributionBudgetModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/createDistribusi', DistributionBudgetModel).then(successCallback, errorCallback);
		}
		function EditDistribusi(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/EditDistribusi', model).then(successCallback, errorCallback);
		}
		function Update(DistributionBudgetModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/updateDistribusi', DistributionBudgetModel).then(successCallback, errorCallback);
		}
		function VariasiKontrak(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/variasiKontrak', model).then(successCallback, errorCallback);
		}
		function insertDok(MonitoringDocContractDetailModel, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/tambahDokumen', MonitoringDocContractDetailModel).then(successCallback, errorCallback);
		}
		function docType(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/monitoringDocContractDocType', model).then(successCallback, errorCallback);
		}

	}
})();