﻿(function () {
	'use strict';

	angular.module("app").factory("PrequalCertificateService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		var service = {
			GetStepData: GetStepData,
			publish: publish,
			GetStepDataVendor: GetStepDataVendor,
			GenerateCertificate: GenerateCertificate,
			UpdateVendorPreqCert: UpdateVendorPreqCert,
			publishAll: publishAll
		};

		return service;

		function GetStepData(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/PrequalCertificate/GetStepData", param).then(successCallback, errorCallback);
		}

		function publishAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/PrequalCertificate/PublishAll", param).then(successCallback, errorCallback);
		}

		function publish(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/PrequalCertificate/Publish", param).then(successCallback, errorCallback);
		}

		function GetStepDataVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/PrequalCertificate/GetStepDataVendor", param).then(successCallback, errorCallback);
		}

		function GenerateCertificate(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/PrequalCertificate/GenerateCertificate", param).then(successCallback, errorCallback);
		}

		function UpdateVendorPreqCert(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/PrequalCertificate/UpdateVendorPreqCert", param).then(successCallback, errorCallback);
		}
	}
})();