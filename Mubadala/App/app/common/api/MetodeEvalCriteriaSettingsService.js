﻿(function () {
    'use strict';

    angular.module("app").factory("MetodeEvalCriteriaSettingsService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            count: count,
            selectCriteriaDesc: selectCriteriaDesc,
            insert: insert,
            update: update,
            hapus: hapus,
            insertEval:insertEval,
            updateEval: updateEval,
            getbyid: getbyid,
            getType: getType,
            getstandards: getstandards,
            getallbfcomm: getallbfcomm,
            selectedbfcomm: selectedbfcomm,
            getVendorTypes: getVendorTypes
        };

        return service;

        // implementation
        function count(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/count", param).then(successCallback, errorCallback);
        }

        function selectCriteriaDesc(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectCriteriaDescByIdTypeReff", param).then(successCallback, errorCallback);
        }

        function getbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/getbyid", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/insert", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/update", param).then(successCallback, errorCallback);
        }

        function insertEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/insert", param).then(successCallback, errorCallback);
        }

        function updateEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/update", param).then(successCallback, errorCallback);
        }

        function hapus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/delete", param).then(successCallback, errorCallback);
        }

        function getType(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/getType").then(successCallback, errorCallback);
        }
        
        function getstandards(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getstandard", param).then(successCallback, errorCallback);
        }

        function getallbfcomm(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getallbfcomm").then(successCallback, errorCallback);
        }

        function selectedbfcomm(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/selectedbfcomm", param).then(successCallback, errorCallback);
        }

        function getVendorTypes(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vpsettings/getVendorTypes").then(successCallback, errorCallback);
        }

    }
})();