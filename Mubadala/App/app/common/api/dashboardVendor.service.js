(function () {
	'use strict';

	angular.module("app")
        .factory("DashboardVendorService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");

		// interfaces
		var service = {
			SelectTender: SelectTender,
			SelectPrequal: SelectPrequal,
			MonitoringPO: MonitoringPO,
			getDataCR: getDataCR,
			MonitoringMRKO: MonitoringMRKO,
			MonitoringFPA: MonitoringFPA,
			loadPrequalAnnounce: loadPrequalAnnounce,
			cekAcknowledgement: cekAcknowledgement,
			newsbyarea: newsbyarea,
			CPR: CPR,
			CPRVHS: CPRVHS,
			getDataWarning: getDataWarning,
			stateRevise: stateRevise,
			stateComplete: stateComplete,
			getDataCSO: getDataCSO,
			acknowledgeFPA: acknowledgeFPA,
			acknowledgeContract: acknowledgeContract,
			selectUploadDataMRKO: selectUploadDataMRKO,
			cekAcknowledgementFPA: cekAcknowledgementFPA,
			cekAcknowledgementContract: cekAcknowledgementContract,
			acknowledgeMRKO: acknowledgeMRKO,
			getDataVendorReject: getDataVendorReject
		};
		return service;

		// implementation
		function MonitoringMRKO(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/MonitoringMRKO", param).then(successCallback, errorCallback);
		}

		function MonitoringFPA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/MonitoringFPA", param).then(successCallback, errorCallback);
		}
		function MonitoringPO(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorMonitoringPO/select", param).then(successCallback, errorCallback);
		}
		function getDataCR(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/getDataCR", param).then(successCallback, errorCallback);
		}
		function SelectTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/selecttender", param).then(successCallback, errorCallback);
		}
		function SelectPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/selectprequal", param).then(successCallback, errorCallback);
		}
		function acknowledgeFPA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/acknowledgeFPA", param).then(successCallback, errorCallback);
		}
		function acknowledgeContract(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/acknowledgeContract", param).then(successCallback, errorCallback);
		}
		function loadPrequalAnnounce(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/prequal/getPrequalAnnounce", param).then(successCallback, errorCallback);
		}
		function cekAcknowledgementFPA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/cekAcknowledgementFPA", param).then(successCallback, errorCallback);
		}
		function cekAcknowledgementContract(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/cekAcknowledgementContract", param).then(successCallback, errorCallback);
		}
		function cekAcknowledgement(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/cekAcknowledgement", param).then(successCallback, errorCallback);
		}
		function newsbyarea(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/newsbyarea", param).then(successCallback, errorCallback);
		}
		function CPR(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/CPR", param).then(successCallback, errorCallback);
		}
		function CPRVHS(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/CPRVHS", param).then(successCallback, errorCallback);
		}

		function getDataWarning(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/getDataWarning", param).then(successCallback, errorCallback);
		}

		function stateRevise(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/stateRevise", param).then(successCallback, errorCallback);
		}

		function stateComplete(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/stateComplete", param).then(successCallback, errorCallback);
		}
		function getDataCSO(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/ContractSignOff", param).then(successCallback, errorCallback);
		}
		function selectUploadDataMRKO(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/dashboardvendor/selectUploadDataMRKO", param).then(successCallback, errorCallback);
		}
		function acknowledgeMRKO(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/dashboardvendor/acknowledgeMRKO", param).then(successCallback, errorCallback);
		}
		function getDataVendorReject(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/dashboardvendor/getDataVendorReject", param).then(successCallback, errorCallback);
		} 

	}
})();