﻿(function () {
    'use strict';

    angular.module("app").factory("ApprovalRekananService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            selectVendorVerified: selectVendorVerified,
            selectDataApprover: selectDataApprover,
            updateApproval: updateApproval
        };

        return service;

        // implementation
        function selectVendorVerified(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-rekanan/selectVendorVerified", param).then(successCallback, errorCallback);
        }

        function selectDataApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-rekanan/selectDataApprover", param).then(successCallback, errorCallback);
        }

        function updateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-rekanan/updateApproval", param).then(successCallback, errorCallback);
        }
        
    }
})();