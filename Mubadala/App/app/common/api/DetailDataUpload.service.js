﻿(function () {
    'use strict';

    angular.module('app').factory('detailDataUploadService', svc);

    svc.$inject = ['GlobalConstantService'];

    //@ngInject
    function svc(GlobalConstantService) {
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

        //interfaces
        var service = {

            Select: Select,

        };

        return service;

        function Select(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detailDataUpload', model).then(successCallback, errorCallback);
        }
      
    }
})();