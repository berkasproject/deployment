(function () {
    'use strict';

    angular.module("app")
        .factory("RekapitulasiIjinUsahaService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

        // interfaces
        var service = {
            rekapIjinUsaha:rekapIjinUsaha
        };
        return service;

        // implementation

        function rekapIjinUsaha(param,successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/rekapIjinUsaha",param).then(successCallback, errorCallback);
        }
    }
})();