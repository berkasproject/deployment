﻿(function () {
	'use strict';

	angular.module("app").factory("QuestionnaireService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
		    all: all,
		    SelectTypeForm: SelectTypeForm,
		    InsertQuestion: InsertQuestion,
            UpdateQuestion: UpdateQuestion,
		    SelectQuestion: SelectQuestion,
		    SelectQuestionByID: SelectQuestionByID,
		    EditActive: EditActive,
		    SelectOption: SelectOption,
		    InsertAnswer: InsertAnswer,
		    UpdateAnswer: UpdateAnswer,
            SelectAnswerByID: SelectAnswerByID
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "").then(successCallback, errorCallback);
		}

		function SelectTypeForm(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/questionnaire/listFormType").then(successCallback, errorCallback);
		}

		function InsertQuestion(param,successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/questionnaire/insert", param).then(successCallback, errorCallback);
		}

		function UpdateQuestion(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/questionnaire/update", param).then(successCallback, errorCallback);
		}

		function SelectQuestion(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/questionnaire/select", param).then(successCallback, errorCallback);
		}

		function SelectQuestionByID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/question/getID", param).then(successCallback, errorCallback);
		}

		function SelectOption(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/optionType/listType").then(successCallback, errorCallback);
		}

		function EditActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/question/editActive", param).then(successCallback, errorCallback);
		}

		function InsertAnswer(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/option/create", param).then(successCallback, errorCallback);
		}

		function UpdateAnswer(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/option/update", param).then(successCallback, errorCallback);
		}

		function SelectAnswerByID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/option/getID", param).then(successCallback, errorCallback);
		}
	}
})();