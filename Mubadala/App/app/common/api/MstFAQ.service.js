(function () {
    'use strict';

    angular.module("app")
        .factory("MstFAQService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            dataFaq: dataFaq,
            cekRole: cekRole,
            insertFaq: insertFaq,
            editFaq: editFaq,
            deleteFaq:deleteFaq
        };
        return service;

        // implementation

        function dataFaq(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstfaq/dataFaq").then(successCallback, errorCallback);
        }
        function cekRole(successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstfaq/cekRole").then(successCallback, errorCallback);
        }
        function insertFaq(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstfaq/insertFaq", param).then(successCallback, errorCallback);
        }
        function editFaq(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstfaq/editFaq", param).then(successCallback, errorCallback);
        }
        function deleteFaq(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstfaq/deleteFaq", param).then(successCallback, errorCallback);
        }
    }
})();