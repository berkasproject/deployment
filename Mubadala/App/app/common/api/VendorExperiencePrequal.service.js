(function () {
	'use strict';

	angular.module("app").factory("VendorExperienceService", dataService);

	dataService.$inject = ['GlobalConstantService'];
	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var apipoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
	    var service = {
	        loadPrequalStep: loadPrequalStep,
	        loadAllExperience: loadAllExperience,
			editActive: editActive,
			typeExperience: typeExperience,
			getTypeTender: getTypeTender,
			SelectBusinessField: SelectBusinessField,
			Insert: Insert,
			Update: Update,
			Delete: Delete,
			selectVendor: selectVendor,
			getCurrencies: getCurrencies,
			getCountries: getCountries,
			getRegions: getRegions,
			getStates: getStates,
			SelectVendorCommodity: SelectVendorCommodity,
            Submit: Submit
		};

		return service;

		function Submit(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExperienceprequal/Submit", model).then(successCallback, errorCallback);
		}
		function loadAllExperience(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExperienceprequal/select", model).then(successCallback, errorCallback);
		}
		function loadPrequalStep(model, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorPrequalEntry/loadPrequalStep", model).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/jabatan/jabataneditactive").then(successCallback, errorCallback);
		}

		function typeExperience(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_EXPERIENCE" };
			GlobalConstantService.post(apipoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getTypeTender(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_TYPE" };
			GlobalConstantService.post(apipoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function SelectBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/businessField/goodsorservice", param).then(successCallback, errorCallback);
		}

		function Insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperiencePrequal/insert", param).then(successCallback, errorCallback);
		}

		function Update(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/update", param).then(successCallback, errorCallback);
		}

		function Delete(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperiencePrequal/editActive", param).then(successCallback, errorCallback);
		}
		function selectVendor(successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}
		function getCurrencies(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/getCurrency").then(successCallback, errorCallback);

		}
		function getRegions(param, successCallback, errorCallback) {
			GlobalConstantService.get(apipoint + "/vendor/registration/continents", param)
                .then(successCallback, errorCallback);
		}

		function getCountries(successCallback, errorCallback) {
			GlobalConstantService.get(apipoint + "/vendor/registration/countries")
                .then(successCallback, errorCallback);

		}

		function getStates(country, successCallback, errorCallback) {
			GlobalConstantService.get(apipoint + "/vendor/registration/states/" + country)
                .then(successCallback, errorCallback);
		}

		function SelectVendorCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/vendor/commodityall", param).then(successCallback, errorCallback);
		}
	}
})();