(function () {
    'use strict';

    angular.module("app")
        .factory("WorkloadProcurementService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            dataWorkloadProc: dataWorkloadProc,
            procurementType: procurementType,
            weekByYear: weekByYear,
            employeePositionName:employeePositionName
        };
        return service;

        // implementation

        function dataWorkloadProc(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/workloadProc", param).then(successCallback, errorCallback);
        }
        function weekByYear(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstWeek/weekByYear", param).then(successCallback, errorCallback);
        }
        function procurementType(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiProc/procurementType", param).then(successCallback, errorCallback);
        }
        function employeePositionName(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiProc/employeePositionName", param).then(successCallback, errorCallback);
        }
    }
})();