﻿(function () {
    'use strict';

    angular.module("app").factory("ApprovalSCMProcurementPlanService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            SelectApproval: SelectApproval,
            selectEntity: selectEntity,
            selectApproverProcurementPlan: selectApproverProcurementPlan,
            updateApproval: updateApproval,
            getProcurementPlan: getProcurementPlan,
            getDept: getDept,
            getTFP: getTFP,
            getEntity: getEntity,
            getMaterialService: getMaterialService,
            getSubSector: getSubSector,
            getProcPlanStatus: getProcPlanStatus,
            getMarketSector: getMarketSector,
            getCategory: getCategory,
            getExistingContract: getExistingContract,
            getExistingContractAward: getExistingContractAward,
            getSCMFocalPoint: getSCMFocalPoint,
            getSourcingActivity: getSourcingActivity,
            getContractType: getContractType,
            selectApproverProcurementPlanNonSCM: selectApproverProcurementPlanNonSCM
        };

        return service;

        // implementation
        function SelectApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/SelectApproval", param).then(successCallback, errorCallback);
        }

        function selectEntity(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/selectEntity", param).then(successCallback, errorCallback);
        }

        function selectApproverProcurementPlan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/selectApproverProcurementPlan", param).then(successCallback, errorCallback);
        }

        function updateApproval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/updateApproval", param).then(successCallback, errorCallback);
        }

        function getProcurementPlan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/getProcurementPlan", param).then(successCallback, errorCallback);
        }

        function getTFP(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/getTechnicalFocalPoint", param).then(successCallback, errorCallback);
        }

        function getDept(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getDept").then(successCallback, errorCallback);
        }

        function getEntity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getEntity").then(successCallback, errorCallback);
        }

        function getMaterialService(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getMaterialService").then(successCallback, errorCallback);
        }

        function getSubSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getSubSector").then(successCallback, errorCallback);
        }

        function getProcPlanStatus(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getProcPlanStatus").then(successCallback, errorCallback);
        }

        function getMarketSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getMarketSector").then(successCallback, errorCallback);
        }

        function getCategory(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getCategory").then(successCallback, errorCallback);
        }

        function getExistingContract(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getExistingContract").then(successCallback, errorCallback);
        }

        function getExistingContractAward(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getExistingContractAward").then(successCallback, errorCallback);
        }

        function getSCMFocalPoint(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getSCMFocalPoint").then(successCallback, errorCallback);
        }

        function getSourcingActivity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getSourcingActivity").then(successCallback, errorCallback);
        }

        function getContractType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/ApprovalSCM/getContractType").then(successCallback, errorCallback);
        }

        function selectApproverProcurementPlanNonSCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ApprovalSCM/selectApproverProcurementPlanNonSCM", param).then(successCallback, errorCallback);
        } 
    }
})();