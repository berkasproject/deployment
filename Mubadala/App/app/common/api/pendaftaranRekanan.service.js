(function () {
    'use strict';

    angular.module("app")
    .factory("VendorRegistrationService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            save: save,
            getStockOption: getStockOption,
            getRegions: getRegions,
            getCountries: getCountries,
            getStates: getStates,
            getCities: getCities,
            getDistricts: getDistrict,
            getBusiness: getBusiness,
            getCurrencies: getCurrencies,
            getStockTypes: getStockTypes,
            getOfficeType: getOfficeType,
            checkNpwp: checkNpwp,
            checkTender: checkTender,
            isAnotherStockHolder: isAnotherStockHolder,
            getUploadPrefix: getUploadPrefix,
            register: register,
            sendMail: sendEmail,
            saveQuestionaire: questionaire,
            saveQuestionairePrequal: questionairePrequal,
            checkTenderName: checkTenderName,
            getLegal: getLegal,
            checkLegal: checkLegal,
            checkEmail: checkEmail,
            selectQuestionnaire: selectQuestionnaire,
            selectQuestionnairePrequal: selectQuestionnairePrequal,
            selectVendor: selectVendor,
            saveQuestionaireUrl: saveQuestionaireUrl,
            selectVendorCurrencies: selectVendorCurrencies,
            selectVendorStock: selectVendorStock,
            selectVendorLegal: selectVendorLegal,
            registerUpdate: registerUpdate,
            CekVendor: CekVendor,
            CekVendorPrequal: CekVendorPrequal,
            selectVendorContactPrequal: selectVendorContactPrequal,
            loadPrequalStep: loadPrequalStep,
            getBusinessReq: getBusinessReq,
            checkTenderById: checkTenderById,
            insertDataCIVD: insertDataCIVD,
            insertBankDetailCIVD: insertBankDetailCIVD,
            insertAfiliasiCIVD: insertAfiliasiCIVD,
            getCIVDData: getCIVDData,
            CheckNamaPerusahaan: CheckNamaPerusahaan,
            getMstAssosiasi: getMstAssosiasi,
            checkUsername: checkUsername
        };

        return service;

        // implementation
        function loadPrequalStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
        }
        function selectVendorContactPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadVendorContact", param).then(successCallback, errorCallback);
        }
        function save(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/lupaPassword").then(successCallback, errorCallback);
        }

        function getStockOption(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/reference/stock-option").then(successCallback, errorCallback);
        }

        function checkLegal(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-license", param).then(successCallback, errorCallback);
        }

        function CekVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-vendor", param).then(successCallback, errorCallback);
        }
        function CekVendorPrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-vendor-prequal", param).then(successCallback, errorCallback);
        }

        function checkEmail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-email", param).then(successCallback, errorCallback);
        }

        function getLegal(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/license").then(successCallback, errorCallback);
        }
        function list(keyword, offset, limit) {

        }

        function getRegions(param, successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/continents", param)
                .then(successCallback, errorCallback);
        }

        function getCountries(successCallback, errorCallback) {
                GlobalConstantService.get(endpoint + "/vendor/registration/countries")
                    .then(successCallback, errorCallback);
            
        }

        function getStates(country, successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/states/" + country)
                .then(successCallback, errorCallback);
        }

        function getCities(state, successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/cities/" + state)
                .then(successCallback, errorCallback);
        }

        function getDistrict(city, successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/districts/" + city)
                .then(successCallback, errorCallback);
        }

        function getBusiness(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/business/list")
                .then(successCallback, errorCallback);
        }

        function getBusinessReq(param,successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/businessReq",param)
                .then(successCallback, errorCallback);
        }

        function getCurrencies(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/vendor/registration/currency/list")
                .then(successCallback, errorCallback);
        }

        function getStockTypes(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/reference/stock-unit")
                .then(successCallback, errorCallback);
        }

        function getOfficeType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/reference/vendor-office-type")
            .then(successCallback, errorCallback);
        }

        function checkNpwp(npwp, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-npwp",
                {
                    Keyword: npwp
                })
                .then(successCallback, errorCallback);
        }
        function checkTender(tender, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-tender",
                {
                    Keyword: tender
                })
                .then(successCallback, errorCallback);
        }
        function checkTenderById(tender, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/checkTenderById",
                {
                    Status: tender
                })
                .then(successCallback, errorCallback);
        }
        function checkTenderName(tender, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/check-TenderName",
                {
                    Keyword: tender
                })
                .then(successCallback, errorCallback);
        }
        function getUploadPrefix(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/reference/upload-prefix")
            .then(successCallback, errorCallback);
        }

        function register(vendorData, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/register", vendorData)
            .then(successCallback, errorCallback);
        }

        function questionaire(questionaireData, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/questionaire", questionaireData)
            .then(successCallback, errorCallback);
        }
        function questionairePrequal(questionaireData, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/questionairePrequal", questionaireData)
            .then(successCallback, errorCallback);
        }

        function isAnotherStockHolder(questionaireData, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/isanotherstockholder", questionaireData)
            .then(successCallback, errorCallback);
        }

        function sendEmail(mail, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/send-email", mail)
            .then(successCallback, errorCallback);
        } 
        function selectQuestionnairePrequal(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/mstquestionairePrequal", param)
            .then(successCallback, errorCallback);
        }
        function selectQuestionnaire(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/mstquestionaire", param)
            .then(successCallback, errorCallback);
        }
        function selectVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/selectByVendor", param)
            .then(successCallback, errorCallback);
        } 
        function saveQuestionaireUrl(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/UploadQuestionnaire", param).then(successCallback, errorCallback);
        } 
        function selectVendorCurrencies(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/selectByVendorCurrency", param).then(successCallback, errorCallback);
        }
        function selectVendorStock(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/selectByVendorStock", param).then(successCallback, errorCallback);
        } 
        function selectVendorLegal(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/selectByVendorLicense", param).then(successCallback, errorCallback);
        } 
        function registerUpdate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/update", param).then(successCallback, errorCallback);
        }
        function insertDataCIVD(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/insertDataCIVD", param).then(successCallback, errorCallback);
        }
        function insertBankDetailCIVD(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/insertBankDetailCIVD", param).then(successCallback, errorCallback);
        }
        function insertAfiliasiCIVD(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/insertAfiliasiCIVD", param).then(successCallback, errorCallback);
        }
        function getCIVDData(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/getCIVDData", param).then(successCallback, errorCallback);
        }
        function CheckNamaPerusahaan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/CheckNamaPerusahaan", param).then(successCallback, errorCallback);
        }
        function checkUsername(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/checkUsername", param).then(successCallback, errorCallback);
        } 
        function getMstAssosiasi(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/getMstAssosiasi", param).then(successCallback, errorCallback);
        } 
    }
})();