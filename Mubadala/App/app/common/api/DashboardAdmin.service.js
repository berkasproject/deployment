(function () {
    'use strict';

    angular.module("app")
        .factory("DashboardAdminService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            selectNews:selectNews,
            SelectTender: SelectTender,
            getDataCR: getDataCR,
            getEmpPos: getEmpPos,
            getVendorActivatedCount: getVendorActivatedCount,
            getVerificationReqCount: getVerificationReqCount,
            getVerifiedVendorCount: getVerifiedVendorCount,
            getUsername: getUsername,
            getNewVendorCount: getNewVendorCount,
            procurementPerformanceValue: procurementPerformanceValue,
            costSavingValue: costSavingValue,
            POValueByArea: POValueByArea,
            contractPerformanceValue: contractPerformanceValue,
            contractCSValue: contractCSValue,
            contractValueByArea: contractValueByArea
        };
        return service;

        // implementation

        function getVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getVendor", param).then(successCallback, errorCallback);
        }
        function getDataCR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dashboardvendor/getDataCR", param).then(successCallback, errorCallback);
        }
        function SelectTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/dashboardvendor/selecttender", param).then(successCallback, errorCallback);
        }
        function selectNews(param, successCallback, errorCallback) {
            console.info("param" + JSON.stringify(param));
            GlobalConstantService.get(api_point + "/berita/select", param).then(successCallback, errorCallback);
        }
        function getEmpPos(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getEmployeePosition", param).then(successCallback, errorCallback);
        }
        function getUsername(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getUsername", param).then(successCallback, errorCallback);
        }
        function getVendorActivatedCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getVendorActivatedCount", param).then(successCallback, errorCallback);
        }
        function getVerificationReqCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getVerificationReqCount", param).then(successCallback, errorCallback);
        }
        function getVerifiedVendorCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getVerifiedVendorCount", param).then(successCallback, errorCallback);
        }
        function getNewVendorCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/getNewVendorCount", param).then(successCallback, errorCallback);
        }
        function procurementPerformanceValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/procurementPerformanceValue", param).then(successCallback, errorCallback);
        }
        function costSavingValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/costSavingValue", param).then(successCallback, errorCallback);
        }
        function POValueByArea(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/POValueByArea", param).then(successCallback, errorCallback);
        }
        function contractPerformanceValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/contractPerformanceValue", param).then(successCallback, errorCallback);
        }
        function contractCSValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/contractCSValue", param).then(successCallback, errorCallback);
        }
        function contractValueByArea(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/dashboardAdmin/contractValueByArea", param).then(successCallback, errorCallback);
        }
    }
})();