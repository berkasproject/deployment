﻿(function () {
    'use strict';

    angular.module("app").factory("ContractEngineerService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            isAllowEdit: isAllowEdit,
            select: select,
            approveByCE: approveByCE,
            rejectByCE: rejectByCE,
            selectcommite: selectcommite,
            selectposition: selectposition,
            selectemployee: selectemployee,
            insert: insert,
            selectemployeeAll: selectemployeeAll,
            selectemployeeCommite: selectemployeeCommite,
            CekCR: CekCR,
            selecttemplate: selecttemplate,
            getMailContent: getMailContent,
            getMailContent1: getMailContent1,
            sendMail: sendMail,
            sendMailRequestor: sendMailRequestor,
            CekCommittee: CekCommittee,
            ShowBaseLineInput: ShowBaseLineInput,
            UpdateBaseLine: UpdateBaseLine
        };

        return service;

        // implementation
        function isAllowEdit(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/isAllowEdit", param).then(successCallback, errorCallback);
        }
        function CekCommittee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/CekCommittee", param).then(successCallback, errorCallback);
        }
        function sendMailRequestor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/sendMailRequestor/send-email", param).then(successCallback, errorCallback);
        }
        function sendMail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/ce/send-email", param).then(successCallback, errorCallback);
        }
        function getMailContent1(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/getMailContent-requestor", param).then(successCallback, errorCallback);
        }
        function getMailContent(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/getMailContent", param).then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/selectAll", param).then(successCallback, errorCallback);
        }
        function approveByCE(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/approveByCE", param).then(successCallback, errorCallback);
        }
        function rejectByCE(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/rejectByCE", param).then(successCallback, errorCallback);
        }
        function selectcommite(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/selectcommitteAll", param).then(successCallback, errorCallback);
        }
        function selectposition(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/selectcommitteAllPosition").then(successCallback, errorCallback);
        } 

        function selectemployee(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/employee", param).then(successCallback, errorCallback);
        }
        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/insertCommittee", param).then(successCallback, errorCallback);
        }
        function selectemployeeAll(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/selectMstcommitteAll", param).then(successCallback, errorCallback);
        }
        function selectemployeeCommite(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/selectcommitte", param).then(successCallback, errorCallback);
        }
        function CekCR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/cekCR", param).then(successCallback, errorCallback);
        }
        function selecttemplate(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineercommittee/selecttemplate", param).then(successCallback, errorCallback);
        }
        function ShowBaseLineInput(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/ShowBaseLineInput", param).then(successCallback, errorCallback);
        }
        function UpdateBaseLine(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractEngineerData/UpdateBaseLine", param).then(successCallback, errorCallback);
        }
    }
})();