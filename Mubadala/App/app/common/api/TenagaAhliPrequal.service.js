﻿(function () {
	'use strict';

	angular.module("app").factory("TenagaAhliPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			all: all,
			update: update,
			insert: insert,
			editActive: editActive,
			selectCertificate: selectCertificate,
			editCertificateActive: editCertificateActive,
			updateExpertCertificate: updateExpertCertificate,
			insertExpertCertificate: insertExpertCertificate,
			selectVendor: selectVendor,
			GetAllNationalities: GetAllNationalities,
			getCRbyVendor: getCRbyVendor,
			loadPrequalStep: loadPrequalStep,
            Submit : Submit
		};

		return service;

	    // implementation
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificatePrequal/Submit", param).then(successCallback, errorCallback);
		}
	    function loadPrequalStep(param, successCallback, errorCallback) {
        GlobalConstantService.post(vendorpoint + "/vendorPrequalEntry/loadPrequalStep", param).then(successCallback, errorCallback);
        }
		function all(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsPrequal/all", param).then(successCallback, errorCallback);
		}
		function selectCertificate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificatePrequal/select", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsPrequal/update", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsPrequal/create", param).then(successCallback, errorCallback);
		}
		function editActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsPrequal/updateActive", param).then(successCallback, errorCallback);
		}
		function editCertificateActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificatePrequal/updateActive", param).then(successCallback, errorCallback);
		}
		function updateExpertCertificate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificatePrequal/update", param).then(successCallback, errorCallback);
		}
		function insertExpertCertificate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorExpertsCertificatePrequal/create", param).then(successCallback, errorCallback);
		}

		function selectVendor(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}

		function GetAllNationalities(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExpertsCertificate/getallnationalities").then(successCallback, errorCallback);
		}
		function getCRbyVendor(param, successCallback, errorCallback) {
			//console.info("service");
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
	}
})();