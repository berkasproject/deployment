﻿(function () {
	'use strict'

	angular.module("app").factory("delegationService", dataService)

	dataService.$inject = ['$http', '$q', 'GlobalConstantService']

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint")
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint")
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint")

		// interfaces
		var service = {
			getDelegation: getDelegation,
			selectemployee: selectemployee,
			saveDelegation: saveDelegation,
			IsAdmin: IsAdmin,
			GetEmpData: GetEmpData
		}

		return service

		function getDelegation(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/delegation/getDelegation", param).then(successCallback, errorCallback)
		}

		function selectemployee(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/delegation/selectemployee", param).then(successCallback, errorCallback)
		}

		function saveDelegation(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/delegation/saveDelegation", param).then(successCallback, errorCallback)
		}

		function IsAdmin(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/delegation/IsAdmin", param).then(successCallback, errorCallback)
		}

		function GetEmpData(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/delegation/GetEmpData", param).then(successCallback, errorCallback)
		}
	}
})()