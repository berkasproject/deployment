﻿(function () {
    'use strict';

    angular.module("app").factory("anncPrequalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        var publicpoint = GlobalConstantService.getConstant("api_endpoint");
        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        // interfaces
        var service = {
            getSetup: getSetup,
            isOvertime: isOvertime,
            select: select,
            insert: insert,
            update: update,
            publish: publish,
            viewVendor: viewVendor,
            insertURL: insertURL,
            emailContent: emailContent,
            loadPrequal: loadPrequal,
            loadDetailPrequal: loadDetailPrequal,
            getVendor: getVendor,
            sendEmail: sendEmail,
            getPrequalAnnounce: getPrequalAnnounce,
            getPrequalRegis: getPrequalRegis,
            getCompanyScale: getCompanyScale,
            getTechnicalClassification: getTechnicalClassification,
            getBusinessField:getBusinessField
        };

        return service;

        // implementation
        function getPrequalAnnounce(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/prequal/GetPrequalStepAnnounce", param).then(successCallback, errorCallback);

        }
        function getPrequalRegis(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/prequal/GetPrequalStepRegis", param).then(successCallback, errorCallback);
        }
        function sendEmail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/send-email", param).then(successCallback, errorCallback);
        }
        function getVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/getVendor", param).then(successCallback, errorCallback);
        }
        function loadDetailPrequal(successCallback, errorCallback) {
            var param = { Keyword: "DETAIL_PREQUAL_TYPE" };
            GlobalConstantService.post(publicpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
        }
        function loadPrequal(successCallback, errorCallback) {
            var param = { Keyword: "PREQUAL_TYPE" };
            GlobalConstantService.post(publicpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
        }
        function emailContent(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/prequalAnnouncement/EmailContent").then(successCallback, errorCallback);
        }
        function getSetup(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/getID", param).then(successCallback, errorCallback);
        }

        function isOvertime(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/prequalAnnouncement/isOvertime", param).then(successCallback, errorCallback);
        }

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/select", param).then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/insertprequal", param).then(successCallback, errorCallback);
        }

        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/update", param).then(successCallback, errorCallback);
        }

        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/publish", param).then(successCallback, errorCallback);
        }

        function viewVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/viewVendor", param).then(successCallback, errorCallback);
        }

        function insertURL(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/insertURL", param).then(successCallback, errorCallback);
        }
        function getCompanyScale(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/getCompanyScale").then(successCallback, errorCallback);
        }
        function getBusinessField(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/getBusinessField").then(successCallback, errorCallback);
        }
        function getTechnicalClassification(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/praqualAnnouncement/getTechnicalClassification").then(successCallback, errorCallback);
        }
       
    }
})();