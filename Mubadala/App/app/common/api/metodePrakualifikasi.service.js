(function () {
	'use strict';

	angular.module("app").factory("MetodePrakualifikasiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

	    var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
	    var service = {
	        getbyid: getbyid,
		    select: select,
		    insert: insert,
		    update: update,
		    remove: remove,
		    getformtypes: getformtypes,
		    getformtypespreqmethod:getformtypespreqmethod,
		    stepsbyformtype: stepsbyformtype,
		    getTipePrakual: getTipePrakual
		};

		return service;

		function getbyid(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/getbyid", param).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/select", param).then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/insert", param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/update", param).then(successCallback, errorCallback);
		}
		function remove(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/remove", param).then(successCallback, errorCallback);
		}
		function getformtypes(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/getformtypes").then(successCallback, errorCallback);
		}
		function getformtypespreqmethod(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/getformtypespreqmethod", param).then(successCallback, errorCallback);
		}
		function stepsbyformtype(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/stepsbyformtype", param).then(successCallback, errorCallback);
		}
		function getTipePrakual(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalmethod/gettipeprakual").then(successCallback, errorCallback);
		}

	}
})();