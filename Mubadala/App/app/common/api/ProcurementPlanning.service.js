﻿(function () {
    'use strict';

    angular.module("app").factory("ProcurementPlanningService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            getDept: getDept,
            getTFP: getTFP,
            getEntity: getEntity,
            getMaterialService: getMaterialService,
            getSubSector: getSubSector,
            getProcPlanStatus: getProcPlanStatus,
            getMarketSector: getMarketSector,
            getCategory: getCategory,
            getExistingContract: getExistingContract,
            getExistingContractAward: getExistingContractAward,
            getSCMFocalPoint: getSCMFocalPoint,
            getSourcingActivity: getSourcingActivity,
            getContractType: getContractType,
            insert: insert,
            update: update,
            publish: publish,
            hapus: hapus,
            selectSCM: selectSCM,
            getDeptSCM: getDeptSCM,
            getTFPSCM: getTFPSCM,
            getEntitySCM: getEntitySCM,
            getMaterialServiceSCM: getMaterialServiceSCM,
            getSubSectorSCM: getSubSectorSCM,
            getProcPlanStatusSCM: getProcPlanStatusSCM,
            getMarketSectorSCM: getMarketSectorSCM,
            getCategorySCM: getCategorySCM,
            getExistingContractSCM: getExistingContractSCM,
            getExistingContractAwardSCM: getExistingContractAwardSCM,
            getSCMFocalPointSCM: getSCMFocalPointSCM,
            getSourcingActivitySCM: getSourcingActivitySCM,
            getContractTypeSCM: getContractTypeSCM,
            updateSCM: updateSCM,
            setuju: setuju,
            tolak: tolak,
            aktif: aktif,
            nonAktif: nonAktif,
            getContentEmail: getContentEmail,
            getContentEmailSCM: getContentEmailSCM,
            selectApproverProcurementPlan: selectApproverProcurementPlan,
            sendEmail: sendEmail,
            sendEmailSCM: sendEmailSCM,
            selectApproverProcurementPlanNonScm: selectApproverProcurementPlanNonScm,
            selectApproverProcurementPlanSCM: selectApproverProcurementPlanSCM
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/procurement-plan/select", param).then(successCallback, errorCallback);
        }

        function getTFP(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/procurement-plan/getTechnicalFocalPoint", param).then(successCallback, errorCallback);
        }

        function getDept(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getDept").then(successCallback, errorCallback);
        }

        function getEntity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getEntity").then(successCallback, errorCallback);
        }

        function getMaterialService(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getMaterialService").then(successCallback, errorCallback);
        }

        function getSubSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getSubSector").then(successCallback, errorCallback);
        }

        function getProcPlanStatus(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getProcPlanStatus").then(successCallback, errorCallback);
        }

        function getMarketSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getMarketSector").then(successCallback, errorCallback);
        }

        function getCategory(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getCategory").then(successCallback, errorCallback);
        }

        function getExistingContract(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getExistingContract").then(successCallback, errorCallback);
        }

        function getExistingContractAward(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getExistingContractAward").then(successCallback, errorCallback);
        }

        function getSCMFocalPoint(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getSCMFocalPoint").then(successCallback, errorCallback);
        }

        function getSourcingActivity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getSourcingActivity").then(successCallback, errorCallback);
        }

        function getContractType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getContractType").then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
                GlobalConstantService.post(endpoint + "/procurement-plan/insert", param).then(successCallback, errorCallback);
        }
        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/procurement-plan/update", param).then(successCallback, errorCallback);
        }
        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/procurement-plan/publish", param).then(successCallback, errorCallback);
        }
        function hapus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/procurement-plan/hapus", param).then(successCallback, errorCallback);
        }
        function getContentEmail(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/procurement-plan/getContentEmail").then(successCallback, errorCallback);
        }
        function sendEmail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/procurement-plan/sendEmail", param).then(successCallback, errorCallback);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        function selectSCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/select", param).then(successCallback, errorCallback);
        }
        function getTFPSCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/getTechnicalFocalPoint", param).then(successCallback, errorCallback);
        }

        function getDeptSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getDept").then(successCallback, errorCallback);
        }

        function getEntitySCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getEntity").then(successCallback, errorCallback);
        }

        function getMaterialServiceSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getMaterialService").then(successCallback, errorCallback);
        }

        function getSubSectorSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getSubSector").then(successCallback, errorCallback);
        }

        function getProcPlanStatusSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getProcPlanStatus").then(successCallback, errorCallback);
        }

        function getMarketSectorSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getMarketSector").then(successCallback, errorCallback);
        }

        function getCategorySCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getCategory").then(successCallback, errorCallback);
        }

        function getExistingContractSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getExistingContract").then(successCallback, errorCallback);
        }

        function getExistingContractAwardSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getExistingContractAward").then(successCallback, errorCallback);
        }

        function getSCMFocalPointSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getSCMFocalPoint").then(successCallback, errorCallback);
        }

        function getSourcingActivitySCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getSourcingActivity").then(successCallback, errorCallback);
        }

        function getContractTypeSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getContractType").then(successCallback, errorCallback);
        }
        function updateSCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/update", param).then(successCallback, errorCallback);
        }
        function setuju(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/setuju", param).then(successCallback, errorCallback);
        }
        function tolak(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/tolak", param).then(successCallback, errorCallback);
        }
        function aktif(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/aktif", param).then(successCallback, errorCallback);
        }
        function nonAktif(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/nonAktif", param).then(successCallback, errorCallback);
        }
        function selectApproverProcurementPlan(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/selectApproverProcurementPlan", param).then(successCallback, errorCallback);
        }
        function selectApproverProcurementPlanNonScm(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/selectApproverProcurementPlan", param).then(successCallback, errorCallback);
        }
        function selectApproverProcurementPlanSCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/approval-procurement-plan/selectApproverProcurementPlanSCM", param).then(successCallback, errorCallback);
        } 
        function getContentEmailSCM(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/scm-procurement-plan/getContentEmail").then(successCallback, errorCallback);
        }
        function sendEmailSCM(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/scm-procurement-plan/sendEmail", param).then(successCallback, errorCallback);
        }
    }
})();