﻿(function () {
    'use strict';

    angular.module("app").factory("DelayedPOService", service);

    service.$inject = ['$upload', 'GlobalConstantService'];

    /* @ngInject */
    function service($upload, GlobalConstantService) {
        var endpoint = GlobalConstantService.getConstant("admin_endpoint");

        // interfaces
        var service = {
            Select: Select,
            GetDELs: GetDELs,
            CreateWL: CreateWL,
            CreateWLAll: CreateWLAll
        };

        return service;

        function Select(model, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/delayedpo/select', model).then(successCallback, errorCallback);
        }
        function GetDELs(model, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/delayedpo/getdels', model).then(successCallback, errorCallback);
        }
        function CreateWL(model, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/delayedpo/createwl', model).then(successCallback, errorCallback);
        }
        function CreateWLAll(model, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/delayedpo/createwlall', model).then(successCallback, errorCallback);
        }
    }
})();