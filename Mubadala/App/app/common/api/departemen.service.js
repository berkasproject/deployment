﻿(function () {
	'use strict';

	angular.module("app").factory("DepartemenService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			select: select,
			editActive: editActive,
			cekData: cekData,
			insert: insert,
            update: update
		};

		return service;

		// implementation
		function all(data, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/department", data).then(successCallback, errorCallback);
		}

		function select(param, successCallback, errorCallback) {
		    //console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/departmen/select", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
		    console.info("serv:" + JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/MDC/editActive",param).then(successCallback, errorCallback);
		}

		function cekData(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/MDC/cek",param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/MDC/insert", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/MDC/update", param).then(successCallback, errorCallback);
		}
	}
})();