(function () {
	'use strict';

	angular.module("app").factory("KriteriaEvaluasiPrakualifikasiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			editActive: editActive,
			selectCriteria: selectCriteria,
			countCriteria: countCriteria,
			getOptions: getOptions,
			insertCriteria: insertCriteria,
			updateCriteria: updateCriteria,
			deleteCriteria: deleteCriteria,
			selectCriteriaByUser: selectCriteriaByUser,
			countCriteriaByUser: countCriteriaByUser,
			selectTypeEvaluation: selectTypeEvaluation,
			selectDataByID: selectDataByID
		};

		return service;

		// implementation
		function all(successCallback, errorCallback) {

        }

		function editActive(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "").then(successCallback, errorCallback);
		}

		function selectCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/select", param).then(successCallback, errorCallback);
		}

		function countCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/count", param).then(successCallback, errorCallback);
		}

		function getOptions(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/getoptions", param).then(successCallback, errorCallback);
		}

		function insertCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/insert", param).then(successCallback, errorCallback);
		}

		function updateCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/update", param).then(successCallback, errorCallback);
		}

		function deleteCriteria(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/delete", param).then(successCallback, errorCallback);
		}

		function selectCriteriaByUser(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/selectbyuser", param).then(successCallback, errorCallback);
		}

		function countCriteriaByUser(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/countbyuser", param).then(successCallback, errorCallback);
		}

		function selectTypeEvaluation(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/getTypeEvaluation").then(successCallback, errorCallback);
		}

		function selectDataByID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/PrequalCriteriaDescription/selectDataByID", param).then(successCallback, errorCallback);
		}
	}
})();