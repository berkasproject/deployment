﻿(function () {
    'use strict';

    angular.module('app').factory('DataMonitoringService', svc);

    svc.$inject = ['GlobalConstantService'];

    //@ngInject
    function svc(GlobalConstantService) {
        var adminpoint = GlobalConstantService.getConstant('admin_endpoint');
        var endpoint = GlobalConstantService.getConstant('api_endpoint');
        var vendorpoint = GlobalConstantService.getConstant('vendor_endpoint');

        //interfaces
        var service = {
            Select: Select,
            Detail: Detail,
            Aknowledgement: Aknowledgement,
            InsertDoc: InsertDoc,
            SentPO: SentPO,
            GetApproval: GetApproval,
            SelectApproval: SelectApproval,
            SendApproval: SendApproval,
            DeleteFile: DeleteFile,
            DetailVendorMonitoring: DetailVendorMonitoring,
            ExportPurchase: ExportPurchase,
            loadItem: loadItem,
            Acknowledge: Acknowledge,
            AgreePO: AgreePO,
            AgreePOAck: AgreePOAck,
            AgreePOAll: AgreePOAll,
            selectLibraryShipping: selectLibraryShipping,
            sendMail: sendMail,
            cekShipping: cekShipping,
            sendMailAmanded: sendMailAmanded,
            loadAwardAll: loadAwardAll,
            cancelPO: cancelPO,
            ExportDataPO: ExportDataPO

        };

        return service;
        function ExportDataPO(model, successCallback, errorCallback) {
        	GlobalConstantService.post(adminpoint + '/ExportDataPO', model).then(successCallback, errorCallback);
        }

        function cancelPO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoring/cancelPO', model).then(successCallback, errorCallback);
        }
        function loadAwardAll(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoring/SelectAllExcelVendor', model).then(successCallback, errorCallback);
        }
        function sendMailAmanded(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoring/sendMailAmanded', model).then(successCallback, errorCallback);
        }
        function cekShipping(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoring/cekShipping', model).then(successCallback, errorCallback);
        }
        function sendMail(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoring/sendemail', model).then(successCallback, errorCallback);
        }
        function selectLibraryShipping(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectLibraryShipping', model).then(successCallback, errorCallback);
        }
        function AgreePOAll(model, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + '/Agree-All', model).then(successCallback, errorCallback);
        }
        function AgreePOAck(model, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + '/Agree-Acknowledge', model).then(successCallback, errorCallback);
        }
        function AgreePO(model, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + '/Agree', model).then(successCallback, errorCallback);
        }
        function loadItem(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detail-PurchaseOrder-Item', model).then(successCallback, errorCallback);
        }
        function Acknowledge(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detail-Acknowledge', model).then(successCallback, errorCallback);
        }
        function ExportPurchase(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detail-PurchaseOrder', model).then(successCallback, errorCallback);
        }
        function DetailVendorMonitoring(model, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + '/vendorMonitoringPO/detailselect', model).then(successCallback, errorCallback);
        }
        function SendApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/SendApproval', model).then(successCallback, errorCallback);
        }
        function SelectApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectmonitoringPOApproval', model).then(successCallback, errorCallback);
        }
        function GetApproval(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/monitoringPO/GetApproval', model).then(successCallback, errorCallback);
        }
        function SentPO(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/sentPO', model).then(successCallback, errorCallback);
        }
        function DeleteFile(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectDataupload/deleteFile', model).then(successCallback, errorCallback);
        }
        function InsertDoc(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/insert/doc-SI', model).then(successCallback, errorCallback);
        }
        function Select(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/selectmonitoringPO', model).then(successCallback, errorCallback);
        }
        function Detail(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/detail', model).then(successCallback, errorCallback);
        }
        function Aknowledgement(model, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + '/aknowledgement', model).then(successCallback, errorCallback);
        }
    }
})();