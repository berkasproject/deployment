﻿(function () {
    'use strict';

    angular.module("app").factory("ListProcurementPlanningService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            getDept: getDept,
            getTFP: getTFP,
            getEntity: getEntity,
            getMaterialService: getMaterialService,
            getSubSector: getSubSector,
            getProcPlanStatus: getProcPlanStatus,
            getMarketSector: getMarketSector,
            getCategory: getCategory,
            getExistingContract: getExistingContract,
            getExistingContractAward: getExistingContractAward,
            getSCMFocalPoint: getSCMFocalPoint,
            getSourcingActivity: getSourcingActivity,
            getContractType: getContractType,
            insert: insert,
            update: update,
            publish: publish,
            hapus: hapus,
            sendEmail: sendEmail,
            getContentEmail: getContentEmail
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/select", param).then(successCallback, errorCallback);
        }

        function getTFP(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/getTechnicalFocalPoint", param).then(successCallback, errorCallback);
        }

        function getDept(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getDept").then(successCallback, errorCallback);
        }

        function getEntity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getEntity").then(successCallback, errorCallback);
        }

        function getMaterialService(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getMaterialService").then(successCallback, errorCallback);
        }

        function getSubSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getSubSector").then(successCallback, errorCallback);
        }

        function getProcPlanStatus(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getProcPlanStatus").then(successCallback, errorCallback);
        }

        function getMarketSector(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getMarketSector").then(successCallback, errorCallback);
        }

        function getCategory(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getCategory").then(successCallback, errorCallback);
        }

        function getExistingContract(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getExistingContract").then(successCallback, errorCallback);
        }

        function getExistingContractAward(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getExistingContractAward").then(successCallback, errorCallback);
        }

        function getSCMFocalPoint(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getSCMFocalPoint").then(successCallback, errorCallback);
        }

        function getSourcingActivity(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getSourcingActivity").then(successCallback, errorCallback);
        }

        function getContractType(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getContractType").then(successCallback, errorCallback);
        }

        function insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/insert", param).then(successCallback, errorCallback);
        }
        function update(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/update", param).then(successCallback, errorCallback);
        }
        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/publish", param).then(successCallback, errorCallback);
        }
        function hapus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/hapus", param).then(successCallback, errorCallback);
        }
        function getContentEmail(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/list-procurement-plan/getContentEmail").then(successCallback, errorCallback);
        }
        function sendEmail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/list-procurement-plan/sendEmail", param).then(successCallback, errorCallback);
        }
    }
})();