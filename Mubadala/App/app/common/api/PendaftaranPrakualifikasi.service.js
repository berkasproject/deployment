(function () {
	'use strict';

	angular.module("app").factory("PendaftaranPrakualifikasiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
		    GetByPrequal: GetByPrequal,
		    GetPrequalRegDoc: GetPrequalRegDoc,
		    UploadDoc: UploadDoc,
			RegisterPrequal: RegisterPrequal
		};

		return service;

		// implementation
		function GetByPrequal(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalreg/getByPrequal", param).then(successCallback, errorCallback);
		}
		function GetPrequalRegDoc(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalreg/getPrequalRegDoc", param).then(successCallback, errorCallback);
		}
		function UploadDoc(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalreg/uploadDoc", param).then(successCallback, errorCallback);
		}
		function RegisterPrequal(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequalreg/registerPrequal", param).then(successCallback, errorCallback);
		}
	}
})();