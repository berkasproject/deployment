﻿(function () {
	'use strict';

	angular.module("app").factory("CRApprovalConfigService", service);

	service.$inject = ['$upload', 'GlobalConstantService'];

	/* @ngInject */
	function service($upload, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
		    Select: Select,
		    GetById: GetById,
		    InsertCfg: InsertCfg,
		    UpdateCfg: UpdateCfg,
		    DeleteCfg: DeleteCfg,
		    SelectPosition: SelectPosition,
		    SelectEmployee: SelectEmployee
		};

		return service;
		
		function Select(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/select", param).then(successCallback, errorCallback);
		}
		function GetById(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/getbyid", param).then(successCallback, errorCallback);
		}
		function InsertCfg(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/insertcfg", param).then(successCallback, errorCallback);
		}
		function UpdateCfg(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/updatecfg", param).then(successCallback, errorCallback);
		}
		function DeleteCfg(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/deletecfg", param).then(successCallback, errorCallback);
		}
		function SelectPosition(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/selectpos", param).then(successCallback, errorCallback);
		}
		function SelectEmployee(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/crapprovalcfg/selectemp", param).then(successCallback, errorCallback);
		}
		
	}
})();