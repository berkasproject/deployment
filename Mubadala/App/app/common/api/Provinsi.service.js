﻿(function () {
	'use strict';

	angular.module("app").factory("ProvinsiService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			select: select,
			editActive: editActive,
			cekData: cekData,
			insert: insert,
			update: update,
			getRegion: getRegion,
			getCountries: getCountries,
			getStates: getStates,
			getCities: getCities,
			getDistrict: getDistrict,
			getCityByID: getCityByID
		};

		return service;

		// implementation
		function all(data, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/stateall", data).then(successCallback, errorCallback);
		}

		function select(param, successCallback, errorCallback) {
		    //console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/state/select", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
		    //console.info("serv:" + JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/province/remove", param).then(successCallback, errorCallback);
		}

		function cekData(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/province/cek", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/province/insert", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/province/update", param).then(successCallback, errorCallback);
		}

		function getRegion(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/registration/continents").then(successCallback, errorCallback);
		}

		function getCountries(continent, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/countries/" + continent).then(successCallback, errorCallback);
		}

		function getStates(country, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/states/" + country).then(successCallback, errorCallback);
		}

		function getCities(state, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/cities/" + state).then(successCallback, errorCallback);
		}

		function getDistrict(city, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/vendor/registration/districts/" + city).then(successCallback, errorCallback);
		}

		function getCityByID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/city/selectbyid", param).then(successCallback, errorCallback);
		}
	}
})();