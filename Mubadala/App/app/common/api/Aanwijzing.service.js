﻿(function () {
	'use strict';

	angular.module("app").factory("AanwijzingService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			isAllowedEdit: isAllowedEdit,
			getDataTender: getDataTender,
			getDataStepTender: getDataStepTender,
			getTypeAanwijzing: getTypeAanwijzing,
			createAanwijzing: createAanwijzing,
			getDataAanwijzingByTender: getDataAanwijzingByTender,
			postingQuestionByVendor: postingQuestionByVendor,
			getPostingByVendor: getPostingByVendor,
			getDataQuestions: getDataQuestions,
			getDataQuestionsAll: getDataQuestionsAll,
			saveAdminPost: saveAdminPost,
			getAdminPostByStep: getAdminPostByStep,
			getAdminPostByStepForVendor: getAdminPostByStepForVendor,
			updateAanwijzing: updateAanwijzing,
			updateSummaryAanwijzing: updateSummaryAanwijzing,
			SelectVendorTender: SelectVendorTender,
			updateVendorAanwijzing: updateVendorAanwijzing,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			sendToApproval: sendToApproval,
			detailApproval: detailApproval,
			isAanwjzCommittee: isAanwjzCommittee,
			getReviewerpostbystep: getReviewerpostbystep
		}
		return service;

		// implementation
		function isAllowedEdit(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/isAllowedEdit", param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/detailApproval", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/sendToApproval", param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/isApprovalSent", param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/isNeedTenderStepApproval", param).then(successCallback, errorCallback);
		}
		function updateVendorAanwijzing(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/updateVendor", param).then(successCallback, errorCallback);
		}
		function SelectVendorTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/tenderRegistrationDetail/selectAll", param).then(successCallback, errorCallback);
		}
		function updateSummaryAanwijzing(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/updateSummary", param).then(successCallback, errorCallback);
		}
		function getDataQuestions(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/getDataQuestions", param).then(successCallback, errorCallback);
		}

		function getDataQuestionsAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/getDataQuestionsAll", param).then(successCallback, errorCallback);
		}

		function saveAdminPost(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/saveadminpost", param).then(successCallback, errorCallback);
		}
		function getAdminPostByStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/getadminpostbystep", param).then(successCallback, errorCallback);
		}
		function getAdminPostByStepForVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/Aanwijzing/getadminpostbystep", param).then(successCallback, errorCallback);
		}
		function getPostingByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/Aanwijzing/getDataPostingVendor", param).then(successCallback, errorCallback);
		}
		function postingQuestionByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/Aanwijzing/posting", param).then(successCallback, errorCallback);
		}
		function getDataAanwijzingByTender(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/getByTender", param).then(successCallback, errorCallback);
		}
		function getDataTender(param, successCallback, errorCallback) {
			//{ProcPackageType: vm.ProcPackType, TenderRefID: vm.IDTender}
			GlobalConstantService.post(adminpoint + "/tender/getinfo", param).then(successCallback, errorCallback);
		}
		function getDataStepTender(param, successCallback, errorCallback) {
			//{ID: vm.IDStepTender}
			GlobalConstantService.post(adminpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
		}
		function getTypeAanwijzing(successCallback, errorCallback) {
			var param = { Keyword: "STEPPUBLISHTYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}
		function createAanwijzing(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/create", param).then(successCallback, errorCallback);
		}

		function updateAanwijzing(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/update", param).then(successCallback, errorCallback);
		}
		function isAanwjzCommittee(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/isAanwjzCommittee", param).then(successCallback, errorCallback);
		}
		function getReviewerpostbystep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/Aanwijzing/getReviewerpostbystep", param).then(successCallback, errorCallback);
		}
	}
})();