(function () {
    'use strict';

    angular.module("app")
        .factory("KPIProcurementService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            dataKPIProc: dataKPIProc,
            weekByYear: weekByYear,
            procurementType: procurementType,
            employeePositionName: employeePositionName,
            exportKPIItemPr:exportKPIItemPr
        };
        return service;

        // implementation

        function dataKPIProc(param, successCallback, errorCallback) {
            //console.info("param" + JSON.stringify(param));
            GlobalConstantService.post(adminpoint + "/kpiProc", param).then(successCallback, errorCallback);
        }
        function weekByYear(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/mstWeek/weekByYear", param).then(successCallback, errorCallback);
        }
        function procurementType(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiProc/procurementType", param).then(successCallback, errorCallback);
        }
        function employeePositionName(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiProc/employeePositionName", param).then(successCallback, errorCallback);
        }
        function exportKPIItemPr(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/kpiProc/exportKPIItemPr", param).then(successCallback, errorCallback);
        }
    }
})();