﻿(function () {
	angular.module("app").factory("hasilPrakualApprvSvc", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		var service = {
			getVendors: getVendors,
			getStep: getStep,
			getApproval: getApproval,
			sendApproval: sendApproval,
			isIssuer: isIssuer,
			detailApproval: detailApproval,
			approve: approve,
			reject: reject,
			getStatus: getStatus,
			isNeedApproval: isNeedApproval,
			GetPrequal: GetPrequal,
			submitApproval: submitApproval,
			saveSummary: saveSummary,
			getSummary: getSummary
		};

		return service;

		function getSummary(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getSummary", param).then(successCallback, errorCallback);
		}
		function saveSummary(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/saveSummary", param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/detailApproval", param).then(successCallback, errorCallback);
		}
		function submitApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/submitApproval", param).then(successCallback, errorCallback);
		}
		function getVendors(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getVendors", param).then(successCallback, errorCallback);
		}
		function getApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getApproval", param).then(successCallback, errorCallback);
		}
		function getStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getStep", param).then(successCallback, errorCallback);
		}
		function isNeedApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/isNeedApproval", param).then(successCallback, errorCallback);
		}
		function sendApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/sendApproval", param).then(successCallback, errorCallback);
		}
		function isIssuer(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getIsIssuer", param).then(successCallback, errorCallback);
		}
		function approve(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/approve", param).then(successCallback, errorCallback);
		}
		function reject(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/reject", param).then(successCallback, errorCallback);
		}
		function getStatus(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getStatus", param).then(successCallback, errorCallback);
		}
		function GetPrequal(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/hasilPrakualApprv/getPrequal", param).then(successCallback, errorCallback);
		}
	}
})();