﻿(function () {
	'use strict';

	angular.module("app").factory("PermintaanUbahDataService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			getOpsiChangeReq: getOpsiChangeReq,
			insertChangeReq: insertChangeReq,
			getCRbyVendor: getCRbyVendor,
			isVerified: isVerified,
			getDataCR: getDataCR,
			getDetailDataCR: getDetailDataCR,
			openLockCR: openLockCR,
			approve: approve,
			reject: reject,
			selectBuilding: selectBuilding,
			selectVehicle: selectVehicle,
			selectEquipment: selectEquipment,
			selectVendorExperience: selectVendorExperience,
			selectDataAdministrasi: selectDataAdministrasi,
			selectVendorLicense: selectVendorLicense,
			selectVendorStock: selectVendorStock,
			loadVendorLegalDoc: loadVendorLegalDoc,
			selectCompanyPerson: selectCompanyPerson,
			selectBusinessField: selectBusinessField,
			selectNeraca: selectNeraca,
			selectDataAdministrasiCurr: selectDataAdministrasiCurr,
			selectDataAdministrasiContact: selectDataAdministrasiContact,
			selectVendorExperts: selectVendorExperts,
			selectBankDetail: selectBankDetail,
			commit: commit,
			loadStatement: loadStatement,
			GetStatementDetail: GetStatementDetail,
			selectBalance: selectBalance,
			loadQuestionnaire: loadQuestionnaire,
			loadQuestionnaireUrl: loadQuestionnaireUrl,
			rejectCR: rejectCR,
			loadVendorPerubahan: loadVendorPerubahan,
			loadVendorKemkumham: loadVendorKemkumham,
			cekPrakualifikasiVendor: cekPrakualifikasiVendor,
			selectVendorAfiliasi: selectVendorAfiliasi
		}

		return service;

		function selectCompanyPerson(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getCompPersCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectVendorStock(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getCompStockCRbyVendor", param).then(successCallback, errorCallback);
		}

		function loadVendorLegalDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getCompLegalDocsCRbyVendor", param).then(successCallback, errorCallback);
		}

		function loadVendorPerubahan(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getCompPerubahanCRbyVendor", param).then(successCallback, errorCallback);
		}

		function loadVendorKemkumham(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getCompKemkumhamCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectVendorLicense(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVendLicCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectVendorExperts(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVendExpertsCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectVendorExperience(model, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVendExpCRbyVendor", model).then(successCallback, errorCallback);
		}

		function selectBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getBussFldCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectDataAdministrasi(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVendorCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectVehicle(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVehiclebyVendor", param).then(successCallback, errorCallback);
		}

		function selectEquipment(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getEquipCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectBankDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/selectBankDetail", param).then(successCallback, errorCallback);
		}

		function selectBuilding(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getBldgCRbyVendor", param).then(successCallback, errorCallback);
		}

		function selectNeraca(param, successCallback, errorCallback) {
			//GlobalConstantService.post(endpoint + "/verifiedvendor/balance", param).then(successCallback, errorCallback);
			GlobalConstantService.post(adminpoint + "/changerequest/balance", param).then(successCallback, errorCallback);
		}

		function openLockCR(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/updateApproveLock", param).then(successCallback, errorCallback);
		}

		function approve(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/approve", param).then(successCallback, errorCallback);
		}

		function reject(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/reject", param).then(successCallback, errorCallback);
		}

		function rejectCR(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/rejectFront", param).then(successCallback, errorCallback);
		}

		function getDetailDataCR(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getDetailCR", param).then(successCallback, errorCallback);
		}

		function getDataCR(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getDataAll", param).then(successCallback, errorCallback);
		}

		function isVerified(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}

		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/getByVendorID", param).then(successCallback, errorCallback);
		}

		function getOpsiChangeReq(param,successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/changerequest/getOpsiChangeReq", param).then(successCallback, errorCallback);
		}

		function insertChangeReq(param, successCallback, errorCallback) {
			console.info("param service:" + JSON.stringify(param));
			GlobalConstantService.post(vendorpoint + "/changerequest/insert", param).then(successCallback, errorCallback);
		}

		function commit(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/submit", param).then(successCallback, errorCallback);
		}

		function selectDataAdministrasiCurr(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVendorCurrencybyVendor", param).then(successCallback, errorCallback);
		}

		function selectDataAdministrasiContact(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/getVendorContactbyVendor", param).then(successCallback, errorCallback);
		}

		function loadStatement(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/loadStatement", param).then(successCallback, errorCallback);
		}

		function GetStatementDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/GetStatementDetail", param).then(successCallback, errorCallback);
		}
		function selectBalance(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/balance", param).then(successCallback, errorCallback);
		}
		function loadQuestionnaire(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/loadQuestionnaire", param).then(successCallback, errorCallback);
		}
		function loadQuestionnaireUrl(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/changerequest/loadQuestionnaireUrl", param).then(successCallback, errorCallback);
		}
		function selectVendorAfiliasi(param, successCallback, errorCallback) {
		    GlobalConstantService.post(adminpoint + "/changerequest/selectVendorAfiliasi", param).then(successCallback, errorCallback);
		} 
		function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
		}
	}
})();
