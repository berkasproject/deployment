﻿(function () {
    'use strict';

    angular.module("app").factory("PersetujuanPRService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            selectDokumenPR: selectDokumenPR,
            getJenisKomoditi: getJenisKomoditi,
            updateSetuju: updateSetuju,
            updateTolak: updateTolak,
            getDetailPRLineByPRID: getDetailPRLineByPRID,
            getDataHistoryPR: getDataHistoryPR
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/persetujuan-pr/select", param).then(successCallback, errorCallback);
        }

        function selectDokumenPR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/persetujuan-pr/selectDokumenPR", param).then(successCallback, errorCallback);
        }

        function getJenisKomoditi(successCallback, errorCallback) {
            var param = { Keyword: "VENDOR_TYPE" };
            GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
        }

        function updateSetuju(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/persetujuan-pr/updateSetuju", param).then(successCallback, errorCallback);
        }

        function updateTolak(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/persetujuan-pr/updateTolak", param).then(successCallback, errorCallback);
        }

        function getDetailPRLineByPRID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/persetujuan-pr/getDetailPRLineByPRID", param).then(successCallback, errorCallback);
        }

        function getDataHistoryPR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/persetujuan-pr/getDataHistoryPR", param).then(successCallback, errorCallback);
        } 
    }
})();