﻿(function () {
	'use strict';

	angular.module("app").factory("vhsTechEntryService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
			getAllTechnical: getAllTechnical,
			GetStepTechnical: GetStepTechnical,
			isNeedTenderStepApprovalTechnical: isNeedTenderStepApprovalTechnical,
			isApprovalSent: isApprovalSent,
			sendToApproval: sendToApproval,
			detailApproval: detailApproval
		};

		return service;

		function getAllTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/VHSFPATechEntry/getAllTechnical', param).then(successCallback, errorCallback);
		}

		function GetStepTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/VHSFPATechEntry/SelectStepTechnical', param).then(successCallback, errorCallback);
		}

		function isNeedTenderStepApprovalTechnical(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/VHSFPATechEntry/isNeedTenderStepApprovalTechnical', param).then(successCallback, errorCallback);
		}

		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/VHSFPATechEntry/isApprovalSent', param).then(successCallback, errorCallback);
		}

		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/VHSFPATechEntry/sendToApproval', param).then(successCallback, errorCallback);
		}

		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + '/VHSFPATechEntry/detailApproval', param).then(successCallback, errorCallback);
		}
	}
})();