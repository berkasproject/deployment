﻿(function () {
    'use strict';

    angular.module("app").factory("VerifiedSendService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            update: update,
            selectlicense: selectlicense,
            updateVerifikasi: updateVerifikasi,
            selectbusinessfieldlicense: selectbusinessfieldlicense,
            loadAkta: loadAkta,
            selectSaham: selectSaham,
            selectDokumen: selectDokumen,
            selectVerifikasi: selectVerifikasi,
            sendMail: sendMail,
            selectcontact: selectcontact,
            role: role,
            getCurrencies: getCurrencies,
            vendorContactByType: vendorContactByType,
            kirimVerifikasiSendEmail: kirimVerifikasiSendEmail,
            getVendorVerificationReview: getVendorVerificationReview
        };

        return service;
        function getCurrencies(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/verifiedvendor-bankdetail/currency/list")
                .then(successCallback, errorCallback);
        }
        function role(successCallback, errorCallback) {
            GlobalConstantService.get(adminpoint + "/menu-roles").then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
        }
        function sendMail(mail, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/send-email", mail)
            .then(successCallback, errorCallback);
        }
        function kirimVerifikasiSendEmail(mail, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/registration/kirim-verifikasi-send-email", mail)
            .then(successCallback, errorCallback);
        }
        

        function update(successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.get(endpoint + "/vendor/verified/update").then(successCallback, errorCallback);
        }
        function selectlicense(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/vendor/license-select", param).then(successCallback, errorCallback);
        }
        function updateVerifikasi(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/update/verifikasi-vendor", param).then(successCallback, errorCallback);
        }
        function selectbusinessfieldlicense(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/vendor/license/selectID", param).then(successCallback, errorCallback);
        } 
        function loadAkta(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/vendor/legal-document/select", param).then(successCallback, errorCallback);
        }
        function selectSaham(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/vendorStock/getByVendor", param).then(successCallback, errorCallback);
        }
        function selectDokumen(param, successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/legalDocument/getLegalDocumentByVendor", param).then(successCallback, errorCallback);
        }
        function selectVerifikasi(successCallback, errorCallback) {
            //console.info("masuk service!! "+JSON.stringify(param));
            GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
        }
        function selectcontact(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
        }
        function vendorContactByType(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedvendor/vendorContactByType", param).then(successCallback, errorCallback);
        }
        function getVendorVerificationReview(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendor/verified/getVendorVerificationReview", param).then(successCallback, errorCallback);
        } 
    }
})();