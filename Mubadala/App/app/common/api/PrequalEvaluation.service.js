﻿(function () {
    'use strict';

    angular.module("app").factory("PrequalEvaluationService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            GetStepData: GetStepData,
            CheckTime: CheckTime,
            GetVendorName: GetVendorName,
            GetEvaluations: GetEvaluations,
            GetEquipments: GetEquipments,
            GetVehicles: GetVehicles,
            GetAssets: GetAssets,
            GetDebts: GetDebts,
            GetBusinessFields: GetBusinessFields,
            GetExpsByField: GetExpsByField,
            CalculateTotalMonths: CalculateTotalMonths,
            GetCommodityPrequals: GetCommodityPrequals,
            GetSavedEvaluation: GetSavedEvaluation,
            SaveEvaluation: SaveEvaluation
        }
        return service;

        // implementation
        function GetStepData(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getstepdata", param).then(successCallback, errorCallback);
        }
        function CheckTime(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/checktime", param).then(successCallback, errorCallback);
        }
        function GetVendorName(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getvendorname", param).then(successCallback, errorCallback);
        }
        function GetEvaluations(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getevaluations", param).then(successCallback, errorCallback);
        }
        function GetEquipments(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getequipments", param).then(successCallback, errorCallback);
        }
        function GetVehicles(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getvehicles", param).then(successCallback, errorCallback);
        }
        function GetAssets(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getassets", param).then(successCallback, errorCallback);
        }
        function GetDebts(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getdebts", param).then(successCallback, errorCallback);
        }
        function GetBusinessFields(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getbusinessfields", param).then(successCallback, errorCallback);
        }
        function GetExpsByField(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getexpsbyfield", param).then(successCallback, errorCallback);
        }
        function CalculateTotalMonths(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/calculatetotalmonths", param).then(successCallback, errorCallback);
        }
        function GetCommodityPrequals(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getcommprequals", param).then(successCallback, errorCallback);
        }
        function GetSavedEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/getsavedeval", param).then(successCallback, errorCallback);
        }
        function SaveEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/preqeval/saveevaluation", param).then(successCallback, errorCallback);
        }
    }
})();