﻿(function () {
    'use strict';

    angular.module("app").factory("NegotiationFinanceApprovalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            select: select,
            approve: approve,
            reject: reject
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negoFinAppr/select", param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negoFinAppr/approve", param).then(successCallback, errorCallback);
        }
        function reject(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/negoFinAppr/reject", param).then(successCallback, errorCallback);
        }

    }
})();