﻿(function () {
    'use strict';

    angular.module('app').factory('TenderSummaryService', tenderStepService);

    tenderStepService.$inject = ['GlobalConstantService'];

    //@ngInject
    function tenderStepService(GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant('admin_endpoint');

        //interfaces
        var service = {
            getsteps: getsteps,
        };

        return service;

        function getsteps(model, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + '/tendersummary/getsteps', model).then(successCallback, errorCallback);
        }
    }
})();