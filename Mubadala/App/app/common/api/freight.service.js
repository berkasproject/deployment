﻿(function () {
	'use strict';

	angular.module("app").factory("FreightService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
	    // interfaces
		var service = {
		    all: all,
		    select: select,
		    editActive: editActive,
		    cekData: cekData,
		    insert: insert,
		    update: update,
		    getTypeRegion: getTypeRegion,
		    getTypeDelivery: getTypeDelivery,
		    getIncoTerms: getIncoTerms,
		    insertDetail: insertDetail,
		    removeDetail: removeDetail,
		    selectDetail: selectDetail
		};

		return service;

	    // implementation
		function all(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/freightcost").then(successCallback, errorCallback);
		}

		function select(param, successCallback, errorCallback) {
		    //console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/freightcost/select", param).then(successCallback, errorCallback);
		}

		function getIncoTerms(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcost/getIncoTerms").then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/frieghtcost/remove", param).then(successCallback, errorCallback);
		}

		function cekData(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcost/cek", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcost/insert", param).then(successCallback, errorCallback);
		}

		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcost/update", param).then(successCallback, errorCallback);
		}

		function getTypeRegion(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/freightcost/listType").then(successCallback, errorCallback);
		}

		function getTypeDelivery(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/freightcost/listTransport").then(successCallback, errorCallback);
		}

		function insertDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcostdetail/insert", param).then(successCallback, errorCallback);
		}

		function selectDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcostdetail/select", param).then(successCallback, errorCallback);
		}
		function removeDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/freightcostdetail/remove", param).then(successCallback, errorCallback);
		}
	}
})();