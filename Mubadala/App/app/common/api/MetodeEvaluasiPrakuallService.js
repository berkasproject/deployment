﻿(function () {
    'use strict';

    angular.module("app").factory("MetodeEvaluasiPrakuallService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            selectVPEvaluasi: selectVPEvaluasi,
            selectById: selectById,
            insertEval: insertEval,
            updateEval: updateEval,
            switchActive: switchActive,
            selectDetailById: selectDetailById,
            selectdetailcriteria: selectdetailcriteria,
            saveDetailCriteria: saveDetailCriteria,
            selectDCbyID: selectDCbyID,
            getScoreforCpr: getScoreforCpr,
            isLevel3Complete: isLevel3Complete,
            publish: publish,
            copyevaltolevel2: copyevaltolevel2,
            isprocsupp: isprocsupp,
            ispm: ispm,
            isallowadd: isallowadd,
            selectAll: selectAll,
            selectCriteriaLevel1: selectCriteriaLevel1,
            selectCriteriaLevel2: selectCriteriaLevel2,
            selectCriteriaLevel3: selectCriteriaLevel3,
            saveDetailCriteriaAboveLevel1: saveDetailCriteriaAboveLevel1,
            addOrEditlvl2: addOrEditlvl2,
            addOrEditlvl3: addOrEditlvl3,
            selectCriteriaLevel2Edit: selectCriteriaLevel2Edit,
            selectCriteriaLevel3Edit: selectCriteriaLevel3Edit,
            getTipePengisian: getTipePengisian,
            getSumberData: getSumberData
        };

        return service;

        // implementation
        function selectAll(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectAll", param).then(successCallback, errorCallback);
        }

        function selectVPEvaluasi(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/select", param).then(successCallback, errorCallback);
        }

        function selectCriteriaLevel1(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectCriteriaLevel1", param).then(successCallback, errorCallback);
        }

        function selectCriteriaLevel2(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectCriteriaLevel2", param).then(successCallback, errorCallback);
        }

        function selectCriteriaLevel3(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectCriteriaLevel3", param).then(successCallback, errorCallback);
        }

        function saveDetailCriteriaAboveLevel1(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/saveDetailCriteriaAboveLevel1", param).then(successCallback, errorCallback);
        }

        function selectById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectbyid", param).then(successCallback, errorCallback);
        }

        function insertEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/insert", param).then(successCallback, errorCallback);
        }

        function updateEval(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/update", param).then(successCallback, errorCallback);
        }

        function switchActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/switchactive", param).then(successCallback, errorCallback);
        }

        function selectDetailById(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectdetailbyid", param).then(successCallback, errorCallback);
        }

        function selectdetailcriteria(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectdetailcriteria", param).then(successCallback, errorCallback);
        }

        function saveDetailCriteria(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/savedetailcriteria", param).then(successCallback, errorCallback);
        }

        function selectDCbyID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectdcbymethod", param).then(successCallback, errorCallback);
        }

        function getScoreforCpr(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/getscoreforcpr", param).then(successCallback, errorCallback);
        }

        function isLevel3Complete(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/islevel3complete", param).then(successCallback, errorCallback);
        }

        function publish(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/publish", param).then(successCallback, errorCallback);
        }

        function copyevaltolevel2(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/copyevaltolevel2", param).then(successCallback, errorCallback);
        }

        function isprocsupp(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/isprocsupp").then(successCallback, errorCallback);
        }

        function ispm(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/ispm").then(successCallback, errorCallback);
        }

        function isallowadd(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/isallowadd").then(successCallback, errorCallback);
        }

        function addOrEditlvl2(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/addOrEditlvl2", param).then(successCallback, errorCallback);
        }

        function addOrEditlvl3(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/addOrEditlvl3", param).then(successCallback, errorCallback);
        }

        function selectCriteriaLevel2Edit(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectCriteriaLevel2Edit", param).then(successCallback, errorCallback);
        }

        function selectCriteriaLevel3Edit(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectCriteriaLevel3Edit", param).then(successCallback, errorCallback);
        }

        function getTipePengisian(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectTipePengisian").then(successCallback, errorCallback);
        }

        function getSumberData(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/PrequalEvaluationMethod/selectSumberData").then(successCallback, errorCallback);
        }
    }
})();