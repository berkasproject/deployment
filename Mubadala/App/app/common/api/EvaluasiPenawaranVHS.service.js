﻿(function () {
    'use strict';

    angular.module("app").factory("EvaluasiPenawaranVHSService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            isItemize: isItemize,
            getPaymentTermOptions: getPaymentTermOptions,
            getCurrencyOptions: getCurrencyOptions,
            getrfqvhs: getrfqvhs,
            getByTenderStepData: getByTenderStepData,
            getVerifiedDocScore: getVerifiedDocScore,
            saveEvaluation: saveEvaluation,
            getOfferEntries: getOfferEntries,
            getItemPRs: getItemPRs,
            getPagedItemPRs: getPagedItemPRs,
            getTotalHistoricalValue: getTotalHistoricalValue,
            getPagedOEDetail: getPagedOEDetail,
            getOtherCosts: getOtherCosts,
            getOtherCostsbyDefault: getOtherCostsbyDefault,
            getCriterias: getCriterias,
            getCriteriasFromEMDC: getCriteriasFromEMDC,
            saveScoring: saveScoring,
            isless3approved: isless3approved,
            setItemPRAward: setItemPRAward,
            getAwardedItemPR: getAwardedItemPR,
            saveEPV: saveEPV,
            getEPVByStep: getEPVByStep,
            saveItemPRSaveEPValues: saveItemPRSaveEPValues
        };

        return service;

        function isItemize(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/isitemize", param).then(successCallback, errorCallback);
        }

        function getPaymentTermOptions(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getPaymentTermOptions", param).then(successCallback, errorCallback);
        }

        function getCurrencyOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getCurrencyOptions").then(successCallback, errorCallback);
        }

        function getrfqvhs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getrfqvhs", param).then(successCallback, errorCallback);
        }

        function getByTenderStepData(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getbytenderstepdata", param).then(successCallback, errorCallback);
        }

        function getVerifiedDocScore(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getverifieddocscore", param).then(successCallback, errorCallback);
        }

        function saveEvaluation(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/saveevaluation", param).then(successCallback, errorCallback);
        }
        
        function getOfferEntries(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getofferentries", param).then(successCallback, errorCallback);
        }

        function getItemPRs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getitemprs", param).then(successCallback, errorCallback);
        }

        function getPagedItemPRs(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getpageditemprs", param).then(successCallback, errorCallback);
        }

        function getTotalHistoricalValue(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/gettotalhistoricalvalue", param).then(successCallback, errorCallback);
        }

        function getPagedOEDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getpagedoedetail", param).then(successCallback, errorCallback);
        }

        function getOtherCosts(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getothercosts", param).then(successCallback, errorCallback);
        }

        function getOtherCostsbyDefault(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getothercostsbydefault", param).then(successCallback, errorCallback);
        }

        function getCriterias(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getcriterias", param).then(successCallback, errorCallback);
        }

        function getCriteriasFromEMDC(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getcriteriasbyemdc", param).then(successCallback, errorCallback);
        }

        function saveScoring(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/savescorings", param).then(successCallback, errorCallback);
        }

        function isless3approved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/isless3approved", param).then(successCallback, errorCallback);
        }

        function setItemPRAward(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/setitempraward", param).then(successCallback, errorCallback);
        }

        function getAwardedItemPR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getawardeditempr", param).then(successCallback, errorCallback);
        }

        function saveEPV(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/saveEPV", param).then(successCallback, errorCallback);
        }

        function getEPVByStep(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/getEPVByStep", param).then(successCallback, errorCallback);
        }

        function saveItemPRSaveEPValues(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vhsoevaluation/saveItemPREPValues", param).then(successCallback, errorCallback);
        }
    }
})();