﻿(function () {
	'use strict';

	angular.module("app").factory("BalanceVendorService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			select: select,
			getAsset: getAsset,
			getCOA: getCOA,
			getSubCOA: getSubCOA,
			getUnit: getUnit,
			insert: insert,
			editActive: editActive,
			update: update,
			balanceDocUrl: balanceDocUrl,
			selectVendor: selectVendor,
			getCRbyVendor: getCRbyVendor,
			isVerified: isVerified,
			cekPrakualifikasiVendor: cekPrakualifikasiVendor,
			cekCR: cekCR,
			getSisaMstBalance: getSisaMstBalance,
			getCurrencies: getCurrencies
		};

		return service;

		function select(param, successCallback, errorCallback) {
			//console.info("masuk service!! "+JSON.stringify(param));
			GlobalConstantService.post(endpoint + "/vendor/balance", param).then(successCallback, errorCallback);
		}

		function getAsset(successCallback, errorCallback) {
			var param = { Keyword: "WEALTH_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getCOA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getSubCOA(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getUnit(successCallback, errorCallback) {
			var param = { Keyword: "UNIT_TYPE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/balance-insert", param).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/balance-editActive", param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/balance-update", param).then(successCallback, errorCallback);
		}
		function balanceDocUrl(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendor/balanceDocUrl").then(successCallback, errorCallback);
		}
		function selectVendor(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}
		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
			//GlobalConstantService.post(vendorpoint + "/changerequest/getApprovedByVendorID", param).then(successCallback, errorCallback);
		}
		function isVerified(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}
		function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
		}
		function cekCR(successCallback, errorCallback) {
		    GlobalConstantService.get(vendorpoint + "/changerequest/cekCR").then(successCallback, errorCallback);
		}

		function getSisaMstBalance(param, successCallback, errorCallback) {
		    //console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/vendor/balance-getSisaMstBalance", param).then(successCallback, errorCallback);
		}
		function getCurrencies(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/bankdetail/currency/list")
                .then(successCallback, errorCallback);
		}
	}
})();