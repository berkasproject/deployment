﻿(function () {
    'use strict';

    angular.module("app").factory("AfiliasiService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            getCRbyVendor: getCRbyVendor,
            getAfiliasi: getAfiliasi,
            getVendor: getVendor,
            save: save,
            remove: remove,
            selectMaster: selectMaster,
            saveMaster: saveMaster,
            nonAktifkanMaster: nonAktifkanMaster,
            AktifkanMaster: AktifkanMaster,
            cekVendor: cekVendor,
            getDataCIVD: getDataCIVD,
            cekVendorAfiliasi: cekVendorAfiliasi,
            getAfiliasiID: getAfiliasiID,
            saveCIVD: saveCIVD,
            cekCR: cekCR
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/afiliasi/select", param).then(successCallback, errorCallback);
        }

        function getCRbyVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
        }

        function getAfiliasi(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/afiliasi/getAfiliasi").then(successCallback, errorCallback);
        }

        function getVendor(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/afiliasi/getVendor").then(successCallback, errorCallback);
        }

        function save(param, successCallback, errorCallback) {
            if (param.create == 1) {
                GlobalConstantService.post(endpoint + "/afiliasi/insert", param).then(successCallback, errorCallback);
            } else if (param.create == 0) {
                GlobalConstantService.post(endpoint + "/afiliasi/update", param).then(successCallback, errorCallback);
            }
        }

        function remove(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/afiliasi/remove", param).then(successCallback, errorCallback);
        }

        function selectMaster(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/master-afiliasi/select", param).then(successCallback, errorCallback);
        }

        function saveMaster(param, successCallback, errorCallback) {
            if (param.create == 1) {
                GlobalConstantService.post(endpoint + "/master-afiliasi/insert", param).then(successCallback, errorCallback);
            } else if (param.create == 0) {
                GlobalConstantService.post(endpoint + "/master-afiliasi/update", param).then(successCallback, errorCallback);
            }
        }

        function nonAktifkanMaster(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/master-afiliasi/nonaktif", param).then(successCallback, errorCallback);
        }

        function AktifkanMaster(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/master-afiliasi/aktif", param).then(successCallback, errorCallback);
        }

        function cekVendor(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/afiliasi/cekVendor").then(successCallback, errorCallback);
        }

        function getDataCIVD(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/afiliasi/getCIVDData", param).then(successCallback, errorCallback);
        }
        function cekVendorAfiliasi(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/afiliasi/cekVendorAfiliasi").then(successCallback, errorCallback);
        }
        function getAfiliasiID(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/afiliasi/getAfiliasiID", param).then(successCallback, errorCallback);
        }
        function saveCIVD(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/afiliasi/saveCIVD", param).then(successCallback, errorCallback);
        }
        function cekCR(successCallback, errorCallback) {
            GlobalConstantService.get(vendorpoint + "/changerequest/cekCR").then(successCallback, errorCallback);
        }
    }
})();