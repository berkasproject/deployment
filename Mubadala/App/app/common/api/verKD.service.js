﻿(function () {
    'use strict';

    angular.module("app").factory("verKDService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        var publicendpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            selectDetail: selectDetail,
            Insert: Insert,
            selectStep: selectStep,
            Step: Step,
            CheckOption: CheckOption,
            selectVendor: selectVendor,
            SelectVendorCommodity: SelectVendorCommodity,
            selectEquipment: selectEquipment,
            selectVehicle: selectVehicle,
            selectBuilding: selectBuilding,
            selectVendorExperience: selectVendorExperience,
            GetByVendorComPer: GetByVendorComPer,
            isless3approved: isless3approved,
            selectBuildingTender: selectBuildingTender,
            selectVehicleTender: selectVehicleTender,
            selectEquipmentTender: selectEquipmentTender,
            selectVendorExperienceTender: selectVendorExperienceTender
        };

        return service;

        // implementation
        function selectVendorExperienceTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/vendorExperience/select", param).then(successCallback, errorCallback);
        }
        function selectVehicleTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/vehicle/select", param).then(successCallback, errorCallback);
        }
        function selectEquipmentTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/Equipment/select", param).then(successCallback, errorCallback);
        }
        function selectBuildingTender(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/Building/select", param).then(successCallback, errorCallback);
        }
        function GetByVendorComPer(param, successCallback, errorCallback) {
            GlobalConstantService.post(publicendpoint + "/verifiedvendor/companyPerson/getByVendorID", param).then(successCallback, errorCallback);
        }
        function selectVendorExperience(param, successCallback, errorCallback) {
            GlobalConstantService.post(publicendpoint + "/verifiedvendor/vendorExperience/select", param).then(successCallback, errorCallback);
        }
        function selectBuilding(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/Building/select", param).then(successCallback, errorCallback);
        }
        function selectVehicle(param, successCallback, errorCallback) {
            GlobalConstantService.post(publicendpoint + "/verifiedvendor/vehicle/select", param).then(successCallback, errorCallback);
        }
        function selectEquipment(param, successCallback, errorCallback) {
            GlobalConstantService.post(publicendpoint + "/verifiedvendor/Equipment/select", param).then(successCallback, errorCallback);
        }
        function SelectVendorCommodity(param, successCallback, errorCallback) {
            GlobalConstantService.post(publicendpoint + "/vendor/commodityall", param).then(successCallback, errorCallback);
        }
        function selectVendor(param, successCallback, errorCallback) {
            GlobalConstantService.post(publicendpoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
        }
        function CheckOption(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/CheckOption", param).then(successCallback, errorCallback);
        }
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/SelectAll", param).then(successCallback, errorCallback);
        }
        function selectDetail(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/SelectDetail", param).then(successCallback, errorCallback);
        }
        function Step(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/SelectStep", param).then(successCallback, errorCallback);
        }
        function selectStep(param, successCallback, errorCallback) {
            console.info(param);
            GlobalConstantService.post(endpoint + "/verifiedVHS/SelectStep", param).then(successCallback, errorCallback);
        }
        function Insert(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/Insert", param).then(successCallback, errorCallback);
        }
        function isless3approved(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/verifiedVHS/isless3approved", param).then(successCallback, errorCallback);
        }
    }
})();