﻿(function () {
    'use strict';

    angular.module("app")
        .factory("TenderInterestApprovalService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            getData: getData,
            approve:approve
        };
        return service;

        // implementation

        function getData(param,successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/tenderInterestApproval/getData",param).then(successCallback, errorCallback);
        }
        function approve(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/tenderInterestApproval/approve", param).then(successCallback, errorCallback);
        }
    }
})();