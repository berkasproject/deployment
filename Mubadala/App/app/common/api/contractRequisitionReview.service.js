﻿(function () {
    'use strict';

    angular.module("app").factory("ContractRequisitionReviewService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("admin_endpoint");
        // interfaces
        var service = {
            SelectCR: SelectCR,
            IsReviewer: IsReviewer,
            SetReviewStatus: SetReviewStatus,
            GetCRReviewers: GetCRReviewers
        };

        return service;

        // implementation
        function SelectCR(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractreqreviewer/selectcr", param).then(successCallback, errorCallback);
        }

        function IsReviewer(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractreqreviewer/isreviewer", param).then(successCallback, errorCallback);
        }

        function SetReviewStatus(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractreqreviewer/setreviewstatus", param).then(successCallback, errorCallback);
        }

        function GetCRReviewers(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/contractreqreviewer/getcrrevs", param).then(successCallback, errorCallback);
        }
    }
})();