﻿(function () {
	angular.module("app").factory("VhsFpaEvalResDetermineService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		var service = {
			GetStepData: GetStepData,
			determine: determine,
			publish: publish
		};

		return service;

		function determine(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/VhsFpaEvalResDetermine/determine", param).then(successCallback, errorCallback);
		}

		function GetStepData(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/VhsFpaEvalResDetermine/getStepData", param).then(successCallback, errorCallback);
		}

		function publish(param, successCallback, errorCallback) {
			GlobalConstantService.post(adminpoint + "/VhsFpaEvalResDetermine/publish", param).then(successCallback, errorCallback);
		}
	}
})();