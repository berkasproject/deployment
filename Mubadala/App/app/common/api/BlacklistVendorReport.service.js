(function () {
    'use strict';

    angular.module("app")
        .factory("BlacklistVendorReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            blVendorCount:blVendorCount
            //getModifyVendorCount: getModifyVendorCount
        };
        return service;

        // implementation

        function blVendorCount(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/blacklistVendorReport/blVendorCount", param).then(successCallback, errorCallback);
        }
    }
})();