﻿(function () {
    'use strict';

    angular.module("app").factory("CfgfileService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            all: all,
            select: select,
            selectbypagename: selectbypagename,
            getSizeUnit: getSizeUnit,
            getSize: getSize,
            getType: getType,
            switchActive: switchActive,
            updateFileSize: updateFileSize,
            insertnewfiletype: insertnewfiletype

        };

        return service;

        function all(data, successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/cfgfile", data).then(successCallback, errorCallback);
        }

        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/select", param).then(successCallback, errorCallback);
        }

        function selectbypagename(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/selectbypagename", param).then(successCallback, errorCallback);
        }

        function getSizeUnit(successCallback, errorCallback) {
            var param = { Keyword: "FILE_SIZE_UNIT" };
            GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
        }

        function getSize(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/getSize").then(successCallback, errorCallback);
        }

        function getType(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/getType").then(successCallback, errorCallback);
        }

        function switchActive(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/switchactive", param).then(successCallback, errorCallback);
        }

        function updateFileSize(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/updateFileSize", param).then(successCallback, errorCallback);
        }

        function insertnewfiletype(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cfgfile/insertFile", param).then(successCallback, errorCallback);
        }
    }
})();