﻿(function () {
	'use strict';

	angular.module("app").factory("IzinUsahaPrequalService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
			all: all,
			getClasification: getClasification,
			selectLicensi: selectLicensi,
			updateLicensi: updateLicensi,
			deleteLic: deleteLic,
			loadContact: loadContact,
			Submit: Submit,
			CekSubmit: CekSubmit,
			loadLicense: loadLicense,
            cekDate: cekDate
		};

		return service;

	    // implementation
		function cekDate(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/cekDate", param).then(successCallback, errorCallback);
		}
		function loadLicense(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal", param).then(successCallback, errorCallback);
		}
		function CekSubmit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/CekSubmit", param).then(successCallback, errorCallback);
		}
		function Submit(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/Submit", param).then(successCallback, errorCallback);
		}
		function loadContact(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/loadVendorContact", param).then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function getClasification(successCallback, errorCallback) {
			var param = { Keyword: "COMPANY_SCALE" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function selectLicensi(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal", param).then(successCallback, errorCallback);
		}

		function updateLicensi(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/updateLicense", param).then(successCallback, errorCallback);
		}

		function deleteLic(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/vendorLicensePrequal/deleteLicense", param).then(successCallback, errorCallback);
		}
	}
})();