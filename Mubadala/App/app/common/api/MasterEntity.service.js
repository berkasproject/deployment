﻿(function () {
    'use strict';

    angular.module("app").factory("EntityService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            selectMaster: selectMaster,
            saveMaster: saveMaster,
            nonAktifkanMaster: nonAktifkanMaster,
            AktifkanMaster: AktifkanMaster
        };

        return service;

        // implementation
        function selectMaster(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/master-entity/select", param).then(successCallback, errorCallback);
        }

        function saveMaster(param, successCallback, errorCallback) {
            if (param.create == 1) {
                GlobalConstantService.post(endpoint + "/master-entity/insert", param).then(successCallback, errorCallback);
            } else if (param.create == 0) {
                GlobalConstantService.post(endpoint + "/master-entity/update", param).then(successCallback, errorCallback);
            }
        }

        function nonAktifkanMaster(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/master-entity/nonaktif", param).then(successCallback, errorCallback);
        }

        function AktifkanMaster(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/master-entity/aktif", param).then(successCallback, errorCallback);
        }
    }
})();