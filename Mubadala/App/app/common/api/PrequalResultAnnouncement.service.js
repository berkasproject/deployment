(function () {
  'use strict';

  angular.module("app")
      .factory("PrequalResultAnnouncementService", dataService);

  dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
  /* @ngInject */
  function dataService($http, $q, GlobalConstantService) {

    var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
    var api_point = GlobalConstantService.getConstant("api_endpoint");

    // interfaces
    var service = {
      pqStep: pqStep,
      announc: announc,
      updateAnnounc: updateAnnounc,
      createAnnounc: createAnnounc,
      publishAnnounc: publishAnnounc,
      getIsPeriodOpen: getIsPeriodOpen,
      sendEmail: sendEmail
    };
    return service;

    // implementation
    
    function sendEmail(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/sendEmail", param).then(successCallback, errorCallback);
    }
    function pqStep(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/pqResultAnnouncStep", param).then(successCallback, errorCallback);
    }
    function announc(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/announc", param).then(successCallback, errorCallback);
    }
    function updateAnnounc(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/updateAnnounc", param).then(successCallback, errorCallback);
    }
    function createAnnounc(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/createAnnounc", param).then(successCallback, errorCallback);
    }
    
    function getIsPeriodOpen(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/getIsPeriodOpen", param).then(successCallback, errorCallback);
    }
    function publishAnnounc(param, successCallback, errorCallback) {
      GlobalConstantService.post(adminpoint + "/pqResultAnnounc/publishAnnounc", param).then(successCallback, errorCallback);
    }
  }
})();