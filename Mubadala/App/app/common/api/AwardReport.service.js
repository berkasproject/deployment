﻿(function () {
    'use strict';

    angular.module("app")
        .factory("AwardReportService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var adminpoint = GlobalConstantService.getConstant("admin_endpoint");
        var api_point = GlobalConstantService.getConstant("api_endpoint");

        // interfaces
        var service = {
            awardQuestionnaire: awardQuestionnaire,
            saveQuestionnaire: saveQuestionnaire,
            goodsAwardReport: goodsAwardReport,
            vhsAwardReport: vhsAwardReport,
            contractAwardReport: contractAwardReport,
            isContractApprover:isContractApprover
        };
        return service;

        // implementation

        function awardQuestionnaire(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/awardReport/questionnaire", param).then(successCallback, errorCallback);
        }
        function saveQuestionnaire(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/awardReport/saveQuestionnaire", param).then(successCallback, errorCallback);
        } function goodsAwardReport(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/awardReport/goodsAwardReport", param).then(successCallback, errorCallback);
        }
        function vhsAwardReport(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/awardReport/vhsAwardReport", param).then(successCallback, errorCallback);
        }
        function contractAwardReport(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/awardReport/contractAwardReport", param).then(successCallback, errorCallback);
        }
        function isContractApprover(param, successCallback, errorCallback) {
            GlobalConstantService.post(adminpoint + "/awardReport/isContractApprover", param).then(successCallback, errorCallback);
        }
    }
})();