(function () {
	'use strict';

	angular.module("app").factory("VendorExperienceService", dataService);

	dataService.$inject = ['GlobalConstantService'];
	/* @ngInject */
	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var apipoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			select: select,
			editActive: editActive,
			typeExperience: typeExperience,
			getTypeTender: getTypeTender,
			SelectBusinessField: SelectBusinessField,
			Insert: Insert,
			Update: Update,
			Delete: Delete,
			selectVendor: selectVendor,
			getCRbyVendor: getCRbyVendor,
			getCurrencies: getCurrencies,
			getCountries: getCountries,
			getRegions: getRegions,
			getStates: getStates,
			SelectVendorCommodity: SelectVendorCommodity,
			cekPrakualifikasiVendor: cekPrakualifikasiVendor,
			cekCR: cekCR,
			insertVendorExperiencesCIVD: insertVendorExperiencesCIVD,
			getMstBusinessFieldTree: getMstBusinessFieldTree,
			getBusinessType: getBusinessType
		};

		return service;

		// implementation
		function getCRbyVendor(param, successCallback, errorCallback) {
			//console.info("service");
			GlobalConstantService.post(endpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}

		function select(model, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/select", model).then(successCallback, errorCallback);
		}

		function editActive(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/jabatan/jabataneditactive").then(successCallback, errorCallback);
		}

		function typeExperience(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_EXPERIENCE" };
			GlobalConstantService.post(apipoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getTypeTender(successCallback, errorCallback) {
			var param = { Keyword: "VENDOR_TYPE" };
			GlobalConstantService.post(apipoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function SelectBusinessField(param, successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/businessField/goodsorservice", param).then(successCallback, errorCallback);
		}

		function Insert(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/insert", param).then(successCallback, errorCallback);
		}

		function Update(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/update", param).then(successCallback, errorCallback);
		}

		function Delete(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/editActive", param).then(successCallback, errorCallback);
		}
		function selectVendor(successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}
		function getCurrencies(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorExperience/getCurrency").then(successCallback, errorCallback);

		}
		function getRegions(param, successCallback, errorCallback) {
			GlobalConstantService.get(apipoint + "/vendor/registration/continents", param)
                .then(successCallback, errorCallback);
		}

		function getCountries(successCallback, errorCallback) {
			GlobalConstantService.get(apipoint + "/vendor/registration/countries")
                .then(successCallback, errorCallback);

		}

		function getStates(country, successCallback, errorCallback) {
			GlobalConstantService.get(apipoint + "/vendor/registration/states/" + country)
                .then(successCallback, errorCallback);
		}

		function SelectVendorCommodity(param, successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/vendor/commodityall", param).then(successCallback, errorCallback);
		}
        function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
        }
        function cekCR(successCallback, errorCallback) {
            GlobalConstantService.get(endpoint + "/changerequest/cekCR").then(successCallback, errorCallback);
        }

        function insertVendorExperiencesCIVD(param,successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendorExperience/insertVendorExperiencesCIVD",param).then(successCallback, errorCallback);
        }
        function getMstBusinessFieldTree(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendorExperience/getMstBusinessFieldTree", param).then(successCallback, errorCallback);
        }
        function getBusinessType(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/vendorExperience/getBusinessType", param).then(successCallback, errorCallback);
        }
	}
})();