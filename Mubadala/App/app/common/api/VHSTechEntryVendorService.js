﻿(function () {
	'use strict';

	angular.module("app").factory("vhsTechEntryVendorService", dataService);

	dataService.$inject = ['GlobalConstantService'];

	function dataService(GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var adminpoint = GlobalConstantService.getConstant("admin_endpoint");

		var service = {
			selectRFQId: selectRFQId,
			selectStep: selectStep,
			getDeliveryTerms: getDeliveryTerms,
			select: select,
			DeliveryTerm: DeliveryTerm,
			getOptionsTender: getOptionsTender,
			getPaymentTerm: getPaymentTerm,
			getIncoTerms: getIncoTerms,
			selectFreight: selectFreight,
			GetSteps: GetSteps,
			InsertAll: InsertAll,
			cekLimit: cekLimit,
			selectTemplate: selectTemplate,
			selectDetail: selectDetail,
			InsertSubmit: InsertSubmit,
			deleteData: deleteData
		};

		return service;

		function selectRFQId(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/GetRFQId", param).then(successCallback, errorCallback);
		}

		function selectTemplate(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/GetVHSTechnical", param).then(successCallback, errorCallback);
		}

		function selectDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/selectDetail", param).then(successCallback, errorCallback);
		}

		function cekLimit(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/cekLimit", param).then(successCallback, errorCallback);
		}

		function selectFreight(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/cekIncoFreight", param).then(successCallback, errorCallback);
		}

		function GetSteps(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/VOE/tahapan/getsteps", param).then(successCallback, errorCallback);
		}

		function InsertAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/InsertAll", param).then(successCallback, errorCallback);
		}

		function InsertSubmit(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/VHSOfferEntry/InsertIsSubmitTechnical', param).then(successCallback, errorCallback);
		}

		function deleteData(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/DeleteData", param).then(successCallback, errorCallback);
		}

		function selectStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/Step", param).then(successCallback, errorCallback);
		}

		function getDeliveryTerms(successCallback, errorCallback) {
			var param = { Keyword: "RFQ_DELIVERY_TERMS" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntryTechnical", param).then(successCallback, errorCallback);
		}

		function DeliveryTerm(param, successCallback, errorCallback) {
			var param = { Keyword: "TENDER_TYPE_RFQ" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getOptionsTender(successCallback, errorCallback) {
			var param = { Keyword: "TENDER_OPTIONS_RFQ" };
			GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getPaymentTerm(successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + '/GoodsOfferEntry/getPymentTerm').then(successCallback, errorCallback);
		}

		function getIncoTerms(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/vhsOfferEntry/cekInco", param).then(successCallback, errorCallback);
		}
	}
})();