﻿(function () {
	'use strict';

	angular.module("app").factory("EvalSafetyService", service);

	service.$inject = ['$http', '$q', 'GlobalConstantService'];

	/* @ngInject */
	function service($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("admin_endpoint");

		// interfaces
		var service = {
		    select: select,
		    isEvaluator: isEvaluator,
		    isAllowedSeeDetail: isAllowedSeeDetail,
			selectDetail: selectDetail,
			getStep: getStep,
			InsertDetail: InsertDetail,
			selectApproval: selectApproval,
			sendToApproval: sendToApproval,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isLess3Approved: isLess3Approved,
			isApprovalSent: isApprovalSent,
			detailApproval: detailApproval
		};

		return service;

		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/safety/getByTender", param).then(successCallback, errorCallback);
		}
		function isEvaluator(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/safety/isEvaluator", param).then(successCallback, errorCallback);
		}
		function isAllowedSeeDetail(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/safety/isAllowedSeeDetail", param).then(successCallback, errorCallback);
		}
		function selectDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/safety/getByDetailID", param).then(successCallback, errorCallback);
		}
		function selectApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/safety/getByApprovalID", param).then(successCallback, errorCallback);
		}
		function getStep(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/tender/stepbyid", param).then(successCallback, errorCallback);
		}
		function InsertDetail(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/safety/insertDetail", param).then(successCallback, errorCallback);
		}
		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/safety/sendToApproval', param).then(successCallback, errorCallback);
		}
		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/safety/isNeedTenderStepApproval', param).then(successCallback, errorCallback);
		}
		function isLess3Approved(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/safety/isless3approved', param).then(successCallback, errorCallback);
		}
		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/safety/isApprovalSent', param).then(successCallback, errorCallback);
		}
		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/safety/detailApproval', param).then(successCallback, errorCallback);
		}
	}
})();