﻿(function () {
    'use strict';

    angular.module("app").factory("VPCPRSponsorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        // interfaces
        var service = {
            select: select,
            getStatusOptions: getStatusOptions,
            selectbyid: selectbyid,
            getapprovalhistories: getapprovalhistories,
            selectDocsCPRId: selectDocsCPRId
        };

        return service;

        // implementation
        function select(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprsponsor/select", param).then(successCallback, errorCallback);
        }
        function getStatusOptions(successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprsponsor/getStatusOptions").then(successCallback, errorCallback);
        }
        function selectbyid(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprsponsor/selectbyId", param).then(successCallback, errorCallback);
        }        
        function getapprovalhistories(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprsponsor/getapprovalhistories", param).then(successCallback, errorCallback);
        }        
        function selectDocsCPRId(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/cprsponsor/selectDocsCPRId", param).then(successCallback, errorCallback);
        }
    }
})();