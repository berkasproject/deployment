﻿(function () {
	'use strict';

	angular.module("app").factory("NewsService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];

	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var endpoint = GlobalConstantService.getConstant("api_endpoint");

		// interfaces
		var service = {
			all: all,
			select: select,
			pagedselect: pagedselect,
			create: create,
			update: update,
			remove: remove,
			getNewsByID: getNewsByID,
			getFrontNews: getFrontNews,
			getOpsiViewer: getOpsiViewer,
            savedocs:savedocs
		};

		return service;

		// implementation
		function getFrontNews(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/frontNews").then(successCallback, errorCallback);
		}

		function getOpsiViewer(successCallback, errorCallback) {
		    var param = { Keyword: "NEWS_VIEWER" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/news").then(successCallback, errorCallback);
		}

		function create(newsModel, successCallback, errorCallback) {
		    console.info("param" + JSON.stringify(newsModel));
			GlobalConstantService.post(endpoint + "/news/create", newsModel).then(successCallback, errorCallback);
		}

		function update(newsModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/news/update", newsModel).then(successCallback, errorCallback);
		}

		function remove(newsModel, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/news/remove", newsModel).then(successCallback, errorCallback);
		}

		function getNewsByID(newsModel, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + '/news/GetNewsByID', newsModel).then(successCallback, errorCallback);
		}

		function find(id, successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/detail/" + id).then(successCallback, errorCallback);
		}

		function list(keyword, offset, limit) { }

		function select(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/berita/select", param).then(successCallback, errorCallback);
		}

		function pagedselect(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/berita/pagedselect", param).then(successCallback, errorCallback);
		}

		function count(param, successCallback, errorCallback) {
			//GlobalConstantService.get(endpoint + "/berita/count").then(successCallback, errorCallback);
		}

		function savedocs(newsModel, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/news/savedocs", newsModel).then(successCallback, errorCallback);
		}
	}
})();