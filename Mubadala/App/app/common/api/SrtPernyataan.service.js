﻿(function () {
	'use strict';

	angular.module("app").factory("SrtPernyataanService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
			all: all,
			All: All,
			AllMain: AllMain,
			selectDocLibrary: selectDocLibrary,
			selectUrlAgree: selectUrlAgree,
			selectUrlBConduct: selectUrlBConduct,
			selectUrlBConductEN: selectUrlBConductEN,
			selectUrlKuesioner: selectUrlKuesioner,
			selectNamaDir: selectNamaDir,
			selectLegalDoc: selectLegalDoc,
			selectContactID: selectContactID,
			selectAddressID: selectAddressID,
			selectAddress: selectAddress,
			selectCountryID: selectCountryID,
			selectCountry: selectCountry,
			insertAgree: insertAgree,
			updateAgree: updateAgree,
			insertDoc: insertDoc,
			updateDoc: updateDoc,
			insertDoc2: insertDoc2,
			selectCek: selectCek,
			selectCek2: selectCek2,
			CekAgree: CekAgree,
			selectData: selectData,
			isUploadAllowed: isUploadAllowed,
			loadDDQuest: loadDDQuest,
			answerQuest: answerQuest,
			sendAnswerQuest: sendAnswerQuest,
            cekCR: cekCR
		};

		return service;

		// implementation

		function CekAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/CekAgree", param).then(successCallback, errorCallback);
		}
		function all(successCallback, errorCallback) {
			GlobalConstantService.get(endpoint + "/jabatan/getallposition").then(successCallback, errorCallback);
		}

		function All(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/GetAllByVendorID", param).then(successCallback, errorCallback);
		}
		function AllMain(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/SelectAgree", param).then(successCallback, errorCallback);
		}
		function insertAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/insertAgree", param).then(successCallback, errorCallback);
		}
		function selectCek(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/Cek", param).then(successCallback, errorCallback);
		}
		function selectCek2(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/Cek2", param).then(successCallback, errorCallback);
		}
		function selectData(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/SelectData", param).then(successCallback, errorCallback);
		}
		function selectNamaDir(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getDirNameByVendorID", param).then(successCallback, errorCallback);
		}
		function selectDocLibrary(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getDocUrlLibrary", param).then(successCallback, errorCallback);
		}
		function selectUrlAgree(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getDocUrlAgreement", param).then(successCallback, errorCallback);
		}
		function selectUrlBConduct(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getDocUrlBConduct", param).then(successCallback, errorCallback);
		}
		function selectUrlBConductEN(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getDocUrlBConductEN", param).then(successCallback, errorCallback);
		}
		function selectUrlKuesioner(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreement/getDocUrlKuesioner", param).then(successCallback, errorCallback);
		}
		function selectLegalDoc(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getLegalDocByVendorID", param).then(successCallback, errorCallback);
		}
		function selectContactID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getContactID", param).then(successCallback, errorCallback);
		}
		function selectAddressID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getAddressID", param).then(successCallback, errorCallback);
		}
		function selectAddress(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getAddress", param).then(successCallback, errorCallback);
		}
		function selectCountryID(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getCountryID", param).then(successCallback, errorCallback);
		}
		function selectCountry(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/getCountry", param).then(successCallback, errorCallback);
		}
		function updateAgree(param, successCallback, errorCallback) {
			console.info("wdqqwweeqrggg");
			GlobalConstantService.post(endpoint + "/vendorAgreement/UpdateAgree", param).then(successCallback, errorCallback);
		}
		function insertDoc(param, successCallback, errorCallback) {
			console.info("awdawdawd");
			GlobalConstantService.post(endpoint + "/vendorAgreement/insertDoc", param).then(successCallback, errorCallback);
		}
		function updateDoc(param, successCallback, errorCallback) {
			console.info("awdawdawdawdawd");
			GlobalConstantService.post(endpoint + "/vendorAgreement/UpdateDoc", param).then(successCallback, errorCallback);
		}
		function insertDoc2(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vendorAgreement/insertDoc2", param).then(successCallback, errorCallback);
		}
		function isUploadAllowed(param, successCallback, errorCallback) {
			GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}
		function loadDDQuest(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreement/loadDDQuest", param).then(successCallback, errorCallback);
		}
		function answerQuest(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreement/answerQuest", param).then(successCallback, errorCallback);
		}
		function sendAnswerQuest(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendorAgreement/sendAnswerQuest", param).then(successCallback, errorCallback);
		}
		function cekCR(successCallback, errorCallback) {
		    GlobalConstantService.get(vendorpoint + "/changerequest/cekCR").then(successCallback, errorCallback);
		}
	}
})();