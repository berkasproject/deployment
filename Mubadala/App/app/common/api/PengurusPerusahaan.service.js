(function () {
	'use strict';

	angular.module("app").factory("PengurusPerusahaanService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var apipoint = GlobalConstantService.getConstant("api_endpoint");

		// interfaces
		var service = {
			Submit: Submit,
			GetByVendor: GetByVendor,
			CreateSingle: CreateSingle,
			EditSingle: EditSingle,
			Delete: Delete,
			GetPositionTypes: GetPositionTypes,
			isVerified: isVerified,
			getCRbyVendor: getCRbyVendor,
			selectContact: selectContact,
			getCountries: getCountries,
			selectCountryID: selectCountryID,
            cekCR: cekCR
		};

		return service;

	    // implementation

		function getCountries(successCallback, errorCallback) {
		    GlobalConstantService.get(apipoint + "/vendor/registration/countries")
                .then(successCallback, errorCallback);

		}

		function selectCountryID(param, successCallback, errorCallback) {
		    GlobalConstantService.post(apipoint + "/vendorAgreement/getCountryID", param).then(successCallback, errorCallback);
		}
		function isVerified(successCallback, errorCallback) {
			GlobalConstantService.post(apipoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}

		function Submit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/changerequest/submit", param).then(successCallback, errorCallback);
		}

		function GetByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/companyPerson/getByVendorID", param).then(successCallback, errorCallback);
		}

		function CreateSingle(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/companyPerson/createsingle", param).then(successCallback, errorCallback);
		}

		function EditSingle(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/companyPerson/updatesingle", param).then(successCallback, errorCallback);
		}

		function Delete(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/companyPerson/delete", param).then(successCallback, errorCallback);
		}

		function GetPositionTypes(successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/companyPerson/getPositionTypes").then(successCallback, errorCallback);
		}
		function getCRbyVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}

		function selectContact(param, successCallback, errorCallback) {
			console.info("masuk service");
			GlobalConstantService.post(apipoint + "/verifiedvendor/selectcontact", param).then(successCallback, errorCallback);
		}
		function cekCR(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/changerequest/cekCR").then(successCallback, errorCallback);
		}
	}
})();