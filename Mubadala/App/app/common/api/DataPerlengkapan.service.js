(function () {
	'use strict';

	angular.module("app").factory("DataPerlengkapanService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

	    var endpoint = GlobalConstantService.getConstant("api_endpoint");
	    var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		// interfaces
		var service = {
		    selectEquipment: selectEquipment,
		    selectBuilding: selectBuilding,
		    selectVehicle: selectVehicle,
		    getCategoryBuilding: getCategoryBuilding,
		    getOwnership: getOwnership,
		    insertBuilding: insertBuilding,
		    updateBuilding: updateBuilding,
		    getConditionEq: getConditionEq,
		    insertNonBuilding: insertNonBuilding,
		    updateNonBuilding: updateNonBuilding,
		    editActiveBulding: editActiveBulding,
		    editActiveNonBulding: editActiveNonBulding,
		    selectVendor: selectVendor,
		    getCRbyVendor: getCRbyVendor,
            cekPrakualifikasiVendor:cekPrakualifikasiVendor
		};

		return service;

	    // implementation
		function getCRbyVendor(param, successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/changerequest/isAllowedEdit", param).then(successCallback, errorCallback);
		}

		function selectVehicle(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/vehicle/select", param).then(successCallback, errorCallback);
		}

		function selectEquipment(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/Equipment/select", param).then(successCallback, errorCallback);
		}

		function selectBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/Building/select", param).then(successCallback, errorCallback);
		}

		function getCategoryBuilding(successCallback, errorCallback) {
		    var param = { Keyword: "BUILDING_CATEGORY" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function getOwnership(successCallback, errorCallback) {
		    var param = { Keyword: "OWNERSHIP_STATUS" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function insertBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/building/insert", param).then(successCallback, errorCallback);
		}

		function updateBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/building/update", param).then(successCallback, errorCallback);
		}

		function getConditionEq(successCallback, errorCallback) {
		    var param = { Keyword: "CONDITION_TYPE" };
		    GlobalConstantService.post(endpoint + "/reference/getbycode", param).then(successCallback, errorCallback);
		}

		function insertNonBuilding(param, successCallback, errorCallback) {
		    console.info("ini?");
		    GlobalConstantService.post(endpoint + "/vendor/equipment/insert", param).then(successCallback, errorCallback);
		}

		function updateNonBuilding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/equipment/update", param).then(successCallback, errorCallback);
		}

		function editActiveBulding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/building/editActive", param).then(successCallback, errorCallback);
		}

		function editActiveNonBulding(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/vendor/equipment/editActive", param).then(successCallback, errorCallback);
		}
		function selectVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/verifiedvendor/selectByVendor").then(successCallback, errorCallback);
		}
		function cekPrakualifikasiVendor(successCallback, errorCallback) {
		    GlobalConstantService.post(vendorpoint + "/prequal/cekPrakualifikasiVendor").then(successCallback, errorCallback);
		}
	}
})();