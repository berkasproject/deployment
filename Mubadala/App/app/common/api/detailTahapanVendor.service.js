﻿(function () {
    'use strict';

    angular.module("app")
        .factory("DetailTahapanVendorService", dataService);

    dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
    /* @ngInject */
    function dataService($http, $q, GlobalConstantService) {

        var endpoint = GlobalConstantService.getConstant("vendor_endpoint");

        // interfaces
        var service = {
            GetSteps: GetSteps,
            GetTenderReg: GetTenderReg
        };
        return service;

        // implementation
        function GetSteps(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tahapan/getsteps", param).then(successCallback, errorCallback);
        }

        function GetTenderReg(param, successCallback, errorCallback) {
            GlobalConstantService.post(endpoint + "/tahapan/gettenderreg", param).then(successCallback, errorCallback);
        }
    }
})();