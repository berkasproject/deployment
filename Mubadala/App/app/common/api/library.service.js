﻿(function () {
	'use strict';

	angular.module("app").factory("LibraryService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("api_endpoint");
		// interfaces
		var service = {
		    all: all,
		    select: select,
		    SelectTypeForm: SelectTypeForm,
		    insert: insert,
		    update: update,
            editActive:editActive
		};

		return service;

		// implementation
		function all(data, successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/libraryall", data).then(successCallback, errorCallback);
		}
		function select(param, successCallback, errorCallback) {
		    //console.info("masuk service!! "+JSON.stringify(param));
		    GlobalConstantService.post(endpoint + "/library/select", param).then(successCallback, errorCallback);
		}

		function SelectTypeForm(successCallback, errorCallback) {
		    GlobalConstantService.get(endpoint + "/reference/document-type").then(successCallback, errorCallback);
		}
		function insert(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/library/insert", param).then(successCallback, errorCallback);
		}
		function update(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/library/update", param).then(successCallback, errorCallback);
		}
		function editActive(param, successCallback, errorCallback) {
		    GlobalConstantService.post(endpoint + "/library/editActive", param).then(successCallback, errorCallback);
		}
	}
})();