﻿(function () {
	'use strict';

	angular.module("app").factory("EvaluasiTeknisVHSService", dataService);

	dataService.$inject = ['$http', '$q', 'GlobalConstantService'];
	/* @ngInject */
	function dataService($http, $q, GlobalConstantService) {

		var endpoint = GlobalConstantService.getConstant("admin_endpoint");
		// interfaces
		var service = {
			isEvaluator: isEvaluator,
			getEvaluator: getEvaluator,
			selectOfferEntries: selectOfferEntries,
			getEvaluationScoresByVendor: getEvaluationScoresByVendor,
			getRFQVHSLimit: getRFQVHSLimit,
			getEvaluationScore: getEvaluationScore,
			selectEvaluations: selectEvaluations,
			setEvaluationResult: setEvaluationResult,
			setEvaluationResultAll: setEvaluationResultAll,
			isCheckedAll: isCheckedAll,
			setEvaluationScore: setEvaluationScore,
			isLess3Approved: isLess3Approved,
			isNeedTenderStepApproval: isNeedTenderStepApproval,
			isApprovalSent: isApprovalSent,
			sendToApproval: sendToApproval,
			detailApproval: detailApproval
		};

		return service;

		function isEvaluator(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/isevaluator", param).then(successCallback, errorCallback);
		}

		function getEvaluator(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/getevaluator", param).then(successCallback, errorCallback);
		}
		function selectOfferEntries(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/selectofferentries", param).then(successCallback, errorCallback);
		}

		function getEvaluationScoresByVendor(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/getevaluationscorebyvendor", param).then(successCallback, errorCallback);
		}

		function getRFQVHSLimit(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/getrfqvhslimit", param).then(successCallback, errorCallback);
		}

		function getEvaluationScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/getevaluationscore", param).then(successCallback, errorCallback);
		}

		function selectEvaluations(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/selectevaluations", param).then(successCallback, errorCallback);
		}

		function setEvaluationResult(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/setevaluationresult", param).then(successCallback, errorCallback);
		}

		function setEvaluationResultAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/setevaluationresultall", param).then(successCallback, errorCallback);
		}

		function isCheckedAll(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/ischeckedall", param).then(successCallback, errorCallback);
		}

		function setEvaluationScore(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/setevaluationscore", param).then(successCallback, errorCallback);
		}

		function isLess3Approved(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/isless3approved", param).then(successCallback, errorCallback);
		}

		function isNeedTenderStepApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + "/vhstechnicalevaluation/isNeedTenderStepApproval", param).then(successCallback, errorCallback);
		}

		function isApprovalSent(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/vhstechnicalevaluation/isApprovalSent', param).then(successCallback, errorCallback);
		}

		function sendToApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/vhstechnicalevaluation/sendToApproval', param).then(successCallback, errorCallback);
		}

		function detailApproval(param, successCallback, errorCallback) {
			GlobalConstantService.post(endpoint + '/vhstechnicalevaluation/detailApproval', param).then(successCallback, errorCallback);
		}
	}
})();