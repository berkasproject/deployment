﻿(function () {
	'use strict';

	angular.module('app').run(runner);

	runner.$inject = ['Idle', '$rootScope', '$translate', '$state', '$stateParams', '$http', '$q', 'routerHelper', '$urlRouter', 'UIControlService'];

	function runner(Idle, $rootScope, $translate, $state, $stateParams, $http, $q, routerHelper, $urlRouter, UIControlService) {
		//Idle.watch(); // ng-idle run

		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;


		// Register events and broadcasts
		$rootScope.$on('$translatePartialLoaderStructureChanged', function () {
			console.log("change translation structure");
			$translate.refresh();
		});

		$rootScope.$on("$stateChangeSuccess", function (event, currentRoute) {
			console.log("change state success!");

			//console.log("logged in: " + (!localStorage.getItem("login")) ? false : true);
			$rootScope.pageTitle = currentRoute.pageTitle;
			$rootScope.namePage = currentRoute.name;
			//filterState(currentRoute.name);
		});

		$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
			console.log("change state start!");
		});

		getStates($http, $q).then(function (data) {
			routerHelper.configureStates(data.states, data.otherwise);
			$urlRouter.sync();
			$urlRouter.listen();
			//console.log($state.get());
		});

		function filterState(stateName) {
			var whitelist = [
                'login',
                'home'
			];

			if (whitelist.indexOf(stateName) < 0) {
				UIControlService.handleUnauthorizedAccess('home');
			}
		}
	}

	function getStates($http, $q) {
		var defer = $q.defer();

		$http.get('app/setting/page.routes.json?cache=' + (new Date()).getTime()).then(function (data) {
			defer.resolve(data.data);
		},
		function (err) {
			defer.reject(err);
		});
		return defer.promise;
	}


})();