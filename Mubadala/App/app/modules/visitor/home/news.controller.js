﻿(function () {
	'use strict';

	angular.module("app")
    .controller("FrontNewsCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', 'GlobalConstantService', 'NewsService', '$stateParams', '$state'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, GlobalConstantService, NewsService, $stateParams, $state) {

		/* jshint validthis: true */
	    var vm = this;
	    vm.newsid = Number($stateParams.id);
	    vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		// bindable variables
		vm.news = {};

		// functions
		vm.init = init;


		// function declarations
		function init() {
			$translatePartialLoader.addPart('home');
		    console.info("1");
			// Load partial traslastion
			//$translatePartialLoader.addPart('home');
			vm.panels = [1, 2, 3];
			console.info("newsID" + vm.newsid);
			loadNews();


		}

		vm.loadNews = loadNews;
		function loadNews() {
		    NewsService.getNewsByID({ NewsID: vm.newsid }, function (reply) {
		       // UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            console.info("newss" + JSON.stringify(reply));
		            vm.news = reply.data;
		        } else {
		            //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.gotohome = gotohome;
		function gotohome() {
		    //console.info("newsid" + newsid);
		    $state.transitionTo('home');
		}
        /*
		function loadNews() {
		    NewsService.getNewsByID({NewsID: vm.newsid},function (response) {
				response = response.data;
				if (response.status == 200) {
				    console.info("newss" + JSON.stringify(response));
					vm.news = response.result.data;
				} else {

				}
			}, function (error) {

			});
		}*/

		// services and events



	}
})();