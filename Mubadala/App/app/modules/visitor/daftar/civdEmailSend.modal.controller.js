﻿(function () {
    'use strict';

    angular.module("app").controller("CivdEmailSendModalController", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'UploaderService', 'VendorRegistrationService', 'SocketService','$state','$q'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, UploadFileConfigService, UploaderService, VendorRegistrationService,SocketService,$state,$q) {
        var vm = this;
        vm.data = item.item;
        vm.bolehEmail = false;
        vm.bolehEmailAlt = false;
        if (vm.data.Email == "") {
            vm.bolehEmail = true;
        }

        if (vm.data.EmailAlt == "") {
            vm.bolehEmailAlt = true;
        }
     
        vm.init = init;
        function init() {
           
        }

        function checkEmail(flag) {
            var defer = $q.defer();
            if (flag === true)
                var data = {
                    Keyword: vm.data.Email
                };
            else
                var data = {
                    Keyword: vm.data.EmailAlt
                };
            VendorRegistrationService.checkEmail(data,
                function (response) {
                    vm.EmailAvailable = (response.data == false || response.data == 'false');

                    //UIControlService.unloadLoading();
                    if (!vm.EmailAvailable) {
                        UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');
                        UIControlService.unloadLoadingModal();

                        if (flag == true) {
                            vm.data.Email = '';
                        }
                        else {
                            vm.data.EmailAlt = '';
                        }

                    }

                    defer.resolve(vm.EmailAvailable);
                }, function(err){
                    // console.log(err)
                    defer.reject();
                    return;
                });

            return defer.promise;
        }

        vm.simpan = simpan;

        function simpan() {

            if (vm.data.Email == "") {
                if (!vm.bolehEmail) {
                    UIControlService.msg_growl("warning", "ERRORS.EMAIL");
                    return;
                }
                
            }

            if (vm.data.EmailAlt == "") {
                if (!vm.bolehEmailAlt) {
                    UIControlService.msg_growl("warning", "ERRORS.EMAILALT");
                    return;
                }
            }

            if (vm.data.EmailAlt != "" && vm.data.Email != "") {
                if (vm.data.EmailAlt == vm.data.Email) {
                    UIControlService.msg_growl("warning", "ERRORS.EMAILSAME");
                    return;
                }
            }

            UIControlService.loadLoadingModal("LOADERS.LOADING");
            checkEmail(true).then(function (response) {
                if (response) {
                    checkEmail(false).then(function (response2) {
                        if (response2) {
                            VendorRegistrationService.insertDataCIVD(vm.data, function (response) {
                                //UIControlService.unloadLoadingModal();
                                if (response.status == 200) {
                                    vm.vendorToEmail = response.data;

                                    UIControlService.msg_growl("notice", "Registration complete!");
                                    SocketService.emit("daftarRekanan");
                                    sendEmail();
                                } else {
                                    UIControlService.handleRequestError(response.data);

                                }
                            }, function (err) {
                                console.log(err)
                            })
                        }
                    }) 
                }
                
            })

           

          
            
        }

        function sendEmail() {
            VendorRegistrationService.sendMail(vm.vendorToEmail, function (response) {
                UIControlService.unloadLoadingModal();
                if (response.status == 200) {
            		$uibModalInstance.close();

                    $state.go('login-panitia');
                    UIControlService.msg_growl("notice", "Email Sent!");
                } else {
                    UIControlService.handleRequestError(response.data);
                }

            }, function (response) {
                UIControlService.handleRequestError(response.data);
                UIControlService.unloadLoading();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();