(function () {
    'use strict';

    angular.module("app")
        .directive('numberMasking', function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, modelCtrl) {
                    modelCtrl.$parsers.push(function (inputValue) {
                        if (inputValue === undefined)
                            return '';
                        var transformedInput = inputValue.replace(/[^0-9]/g, '');
                        if (transformedInput !== inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }
                        return transformedInput;
                    });
                }
            };
        }).controller("DaftarCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$state', '$translate', '$window', 'VendorRegistrationService', 'UIControlService', 'UploadFileConfigService', 'UploaderService', 'BlacklistService', 'MailerService', 'SocketService', '$stateParams', '$filter', '$q', '$http', '$uibModal'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $state, $translate, $window, VendorRegistrationService, UIControlService, UploadFileConfigService, UploaderService, BlacklistService, MailerService, SocketService, $stateParams, $filter, $q, $http, $uibModal) {
        // attributes
        var vm = this;
        var input;
        vm.TenderID = Number($stateParams.TenderID);
        if (vm.TenderID != 0) {
            UIControlService.loadLoading("LOADERS.CHECKING_TENDER");
            VendorRegistrationService.checkTenderById(vm.TenderID, function (response) {
                vm.tenderData = response.data;
                if (vm.tenderData == "tender is expired") {
                    UIControlService.msg_growl('error', 'FORM.EXPIREDTENDER');
                    UIControlService.unloadLoading();
                } else if (vm.tenderData == "tender is not available") {
                    UIControlService.msg_growl('error', 'FORM.TENDERNOTVALID');
                    UIControlService.unloadLoading();
                } else {
                    vm.vendor.TenderCodeGenerate = vm.tenderData;
                    checkTender();
                }
            }, handleRequestError);
        }
        // register forms for autocomplete purpose only
        vm.regionalForm;
        vm.informationForm;
        vm.companyContact;
        vm.vendorContactForm;
        vm.vendorAddressForm;
        vm.stockForm;
        vm.tenderName = "";
        vm.contactPers = "";
        vm.flag = 0;
        //label
        vm.nama = "NM_KOSONG";
        vm.tgl = "TGL_KOSONG";
        vm.identitas = "NO_KOSONG";

        vm.myConfig3 = {
            maxItems: 1,
            optgroupField: "Name",
            labelField: "Name",
            valueField: "CountryID",
            searchField: "Name"
        };
        // constants
        const MODULE_ID = 2;
        const STOCK_OWNER_ID_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.ID';
        const VENDOR_REGISTRATION_NO_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.NPWP';
        const VENDOR_LEGAL_NO_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.SIUP';
        const UPLOAD_PREFIX = 'UPLOAD_PREFIX';
        const UPLOAD_PREFIX_ID = 'UPLOAD_PREFIX_ID';
        const UPLOAD_PREFIX_NPWP = 'UPLOAD_PREFIX_NPWP';
        const UPLOAD_PREFIX_SIUP = 'UPLOAD_PREFIX_SIUP';
        const SIZE_UNIT_KB = 'SIZE_UNIT_KB';
        const SIZE_UNIT_MB = 'SIZE_UNIT_MB';
        const COUNTRY_INDONESIA = 'IDN';
        const STOCK_UNIT_CURRENCY = 'STOCK_UNIT_CURRENCY';
        const VENDOR_OFFICE_TYPE_MAIN = 'VENDOR_OFFICE_TYPE_MAIN';


        vm.passCaptcha = "";
        vm.checked = false;
        vm.enabledButton = true;
        vm.isCalendarOpened = false;
        vm.isDobCalendarOpened = false;
        vm.modulePath = "app/modules/visitor/daftar/";
        vm.formState = {
            regional: false
        };
        vm.vendor = {
            TenderCodeGenerate: null,
            name: null,
            founded: null,
            currency: {},
            npwp: null,
            tendercodetemp: null,
            siup: null,
            contacts: [],
            addresses: [],
            stocks: [],
            TenderInterestVendor: null
        };

        vm.tenderInterest = {
            TenderStepDataID: null,
            TenderID: null,
            TenderInterestVendorDoc: []
        }

        vm.tenderInterestDoc = [];

        vm.tenderInterestDocData = null;
        vm.stock = {
            quantity: 0,
            unit: {},
            type: {},
            owner: {
                name: "",
                dob: "",
                id: "",
                doc_id: ""
            },
            Position: "",
            Tmpblacklist: false
        };

        vm.address = {
            type: '',
            city: '',
            state: '',
            country: '',
            detail:'',
            AssociationID:''
        };

        vm.contact = {};

        vm.stocks = [];
        vm.stockOption = [];
        vm.contacts = [];
        vm.addresses = [];
        vm.addressCityList = [];
        vm.addressDistrictList = [];
        vm.formValid = false;
        vm.Tmpblacklist = false;

        // functions
        vm.initialize = initialize;
        vm.captcha = Captcha;
        vm.validCaptcha = validCaptcha;
        vm.loadRegions = loadRegions;
        vm.reloadRegions = reloadRegions;
        vm.loadCountries = loadCountries;
        vm.reloadCountries = reloadCountries;
        vm.loadStates = loadStates;
        vm.reloadStates = reloadStates;
        vm.loadCities = loadCities;
        vm.loadDistricts = loadDistricts;
        vm.loadBusiness = loadBusiness;
        vm.openCalendar = openCalendar;
        vm.addContact = addContact;
        vm.removeContact = removeContact;
        vm.openDobCalendar = openDobCalendar;
        vm.addStockData = addStockData;
        vm.removeStock = removeStock;
        vm.stockOwnerIDSelected = stockOwnerIDSelected;

        vm.npwpChanged = checkNpwp;
        vm.tenderChanged = checkTender;
        vm.vendorNameChanged = updateUsername;
        vm.npwpDocSelected = npwpDocSelected;
        vm.sktDocSelected = sktDocSelected;

        vm.reloadAddressCity = reloadAddressCity;
        vm.reloadAddressDistrict = reloadAddressDistrict;
        vm.addAddress = addAddress;
        vm.addCurrency = addCurrency;
        vm.removeAddress = removeAddress;
        vm.removeCurrency = removeCurrency;
        vm.checkLegal = checkLegal;
        vm.checkEmail = checkEmail;
        vm.setLang = setLang;
        vm.getCurrLang = getCurrLang;
        vm.getCurrCountry = getCurrCountry;
        vm.register = register;
        vm.cekOwnerID = cekOwnerID;
        vm.npwpPass = false;

        vm.isTenderJasa = false;
        vm.isOpenTender = false;
        vm.listAssosiasi = [];
        // implementations
        function initialize() {
            $translatePartialLoader.addPart('daftar');
            Captcha();
            loadOptionStock();
            loadFilePrefix();
            loadCountries();
            reloadCountries();
            loadBusiness();
            loadCurrencies();
            loadStockUnits();
            loadConfig();
            loadPhoneCodes();
            loadOfficeType();
            loadLegal();
            loadMstAssosiasi();
        }

        vm.labelChange = labelChange;
        function labelChange() {
            vm.stock.owner.dob = undefined;
            if (vm.stockOpt.Name == 'STOCK_PERSONAL') {
                //PERSONAL
                vm.nama = "NM_PERSONAL";
                vm.tgl = "TGL_PERSONAL";
                vm.identitas = "NO_PERSONAL";
            } else if (vm.stockOpt.Name == 'STOCK_COMPANY') {
                //COMPANY
                vm.nama = "NM_COMPANY";
                vm.tgl = "TGL_COMPANY";
                vm.identitas = "NO_COMPANY";
            }

        }

        function loadOptionStock(data) {
            VendorRegistrationService.getStockOption(function (response) {
                vm.stockOption = response.data;
                if (data != undefined) {
                    for (var i = 0; i < vm.stockOption.length; i++) {
                        if (vm.stockOption[i].RefID === data.SysReference.RefID) {
                            vm.stockOpt = vm.stockOption[i];
                            vm.flag = 1;
                        }
                    }

                }
                UIControlService.unloadLoading();
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.calculateAge = calculateAge;
        function calculateAge(birthday) {
            if (birthday !== '' && birthday !== undefined) {
                //birthday = birthday.split("-").reverse().join("-");
                //birthday = new Date(birthday);
                //if (isNaN(birthday.getTime())) {
                //    vm.stock.owner.dob = undefined;
                //    UIControlService.msg_growl('error', "FORM.INVALID_DATE");
                //    return;
                //}
                if (vm.stockOpt.Name == 'STOCK_PERSONAL') {


                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    var age = Math.abs(ageDate.getUTCFullYear() - 1970);
                    if (age < 17) {
                        vm.stock.owner.dob = undefined;
                        UIControlService.msg_growl("error", "FORM.AGE");
                        return;
                    }
                }
            } else {
                vm.stock.owner.dob = undefined;
                UIControlService.msg_growl('error', "FORM.INVALID_DATE");
                return;
            }
        }

        function loadLegal(data) {
            UIControlService.loadLoading("LOADERS.LOADING");
            VendorRegistrationService.getLegal(
                function (response) {
                    vm.LegalList = response.data;
                    for (var i = 0; i < vm.LegalList.length; i++) {
                        if (data !== undefined) {
                            for (var j = 0; j < data.length; j++) {
                                if (data[j].LicenseID == vm.LegalList[i].ID) {
                                    vm.vendor.legal = vm.LegalList[i];
                                }
                            }

                        }
                    }
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }

        function checkLegal() {
            if (vm.vendor.siup == "" || vm.vendor.siup == null) {
                UIControlService.msg_growl("error", "ERRORS.LEGALNo");
                return;
            }
            if (vm.vendor.legal === undefined) {
                UIControlService.msg_growl("error", "FORM.LEGAL");
                return;
            } else {
                UIControlService.loadLoading("LOADERS.LOADING");
                VendorRegistrationService.checkLegal({ Status: vm.vendor.legal.ID, Keyword: vm.vendor.siup }, function (response) {
                    vm.legalAvailable = response.data;
                    //console.info(response);
                    if (vm.legalAvailable) {
                        UIControlService.msg_growl('notice', 'FORM.VALIDATION_OK.LEGAL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.LEGAL_AVAILABLE.TITLE');
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.LEGAL_NOT_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.LEGAL_NOT_AVAILABLE.TITLE');
                        UIControlService.unloadLoading();
                        vm.vendor.legal = '';
                    }
                }, handleRequestError);
            }
        }

        function checkEmail(flag) {
            var defer = $q.defer();
            if (flag === true)
                var data = {
                    Keyword: vm.vendor.email
                };
            else
                var data = {
                    Keyword: vm.contact.email
                };
            VendorRegistrationService.checkEmail(data,
                function (response) {
                    vm.EmailAvailable = (response.data == false || response.data == 'false');
                    defer.resolve(vm.EmailAvailable);
                    //UIControlService.unloadLoading();
                    if (!vm.EmailAvailable) {
                        UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');
                        UIControlService.unloadLoading();

                        if (flag == true) {
                            vm.vendor.email = '';
                        }
                        else {
                            vm.contact.email = '';
                        }

                    }
                }, handleRequestError);

            return defer.promise
        }

        function loadFilePrefix() {
            UIControlService.loadLoading("LOADERS.LOADING_PREFIX");
            VendorRegistrationService.getUploadPrefix(
                function (response) {
                    var prefixes = response.data;
                    vm.prefixes = {};
                    for (var i = 0; i < prefixes.length; i++) {
                        vm.prefixes[prefixes[i].Name] = prefixes[i];
                    }
                    //console.log(vm.prefixes);
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }

        function loadRegions(data) {
            UIControlService.loadLoading("LOADERS.LOADING_REGION");
            VendorRegistrationService.getRegions({ CountryID: data },
                function (response) {
                    vm.vendor.region = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }

        function reloadRegions(data) {
            UIControlService.loadLoading("LOADERS.LOADING_REGION");
            VendorRegistrationService.getRegions({ CountryID: data },
                function (response) {
                    vm.address.region = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }

        function loadCountries(data) {
            UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
            VendorRegistrationService.getCountries(
                function (response) {
                    vm.countryList = response.data;
                    vm.a = getCurrCountry();
                    vm.dataVen = getDataVendor();
                    if (vm.dataVen) loadVendor1(vm.dataVen);
                    for (var i = 0; i < vm.countryList.length; i++) {
                        if (vm.a != undefined || vm.a != null) {
                            if (vm.a === vm.countryList[i].CountryID) {
                                // if (vm.a.Code == "IDN") {
                                //     if (!vm.dataVen) {
                                //         vm.currencies.push({
                                //             CurrencyID: 61,
                                //             Id: 0,
                                //             Label: "Indonesia Rupiah",
                                //             Symbol: "IDR",
                                //             IsDelete: true
                                //         });
                                //     }
                                // }
                                vm.vendor.country = vm.countryList[i].CountryID;
                                UIControlService.loadLoading("LOADERS.LOADING_STATE");
                                loadRegions(vm.a);
                                VendorRegistrationService.getStates(vm.vendor.country,
									function (response) {
									    vm.stateList = response.data;
									    // localStorage.removeItem('country');
									    // vm.dataVenState = getDataVendorState();
									    // vm.dataVenCity = getDataCity();
									    // vm.dataVenDistric = getDataDistric();
									    // for (var i = 0; i < vm.stateList.length; i++) {
									    //     if (vm.dataVenState !== "" && vm.dataVenState.StateID === vm.stateList[i].StateID) {
									    //         vm.vendor.state = vm.stateList[i];
									    //         localStorage.removeItem('state1');
									    //         localStorage.removeItem('city1');
									    //         localStorage.removeItem('distric1');
									    //         if (vm.vendor.state.Country.Code === 'IDN') {
									    //             loadCities(vm.vendor.state);
									    //             break;
									    //         }
									    //     }

									    // }
									    UIControlService.unloadLoading();
									}, handleRequestError);
                                break;
                            }
                        }
                        if (data !== undefined) {
                            //console.info("sss");
                            if (data.CountryID === vm.countryList[i].CountryID) {
                                vm.vendor.country = vm.countryList[i].CountryID;
                                UIControlService.loadLoading("LOADERS.LOADING_STATE");
                                loadRegions(vm.a);
                                VendorRegistrationService.getStates(vm.vendor.country,
									function (response) {
									    vm.stateList = response.data;
									    for (var i = 0; i < vm.stateList.length; i++) {
									        if (vm.selectedState1 !== "" && vm.selectedState1.StateID === vm.stateList[i].StateID) {
									            vm.vendor.state = vm.stateList[i];
									            if (vm.vendor.state.Country.Code === 'IDN') {
									                loadCities(vm.vendor.state);
									                break;
									            }
									        }

									    }
									    UIControlService.unloadLoading();
									}, handleRequestError);
                                break;
                            }
                        }
                    }
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }


        function reloadCountries() {
            UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
            VendorRegistrationService.getCountries(
                function (response) {
                    vm.addresscountryList = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }

        function loadStates(country) {
            //console.info(vm.dataVendor);
            if (country != undefined || vm.vendor.country != undefined) {
                vm.vendor.npwp = "";
                if (vm.dataVendor.length !== 0) {
                    localStorage.setItem('dataVendor', JSON.stringify(vm.dataVendor));
                    localStorage.setItem('state1', JSON.stringify(vm.selectedState1));
                    if (country.Code === 'IDN') {
                        localStorage.setItem('city1', JSON.stringify(vm.selectedCity1));
                        localStorage.setItem('distric1', JSON.stringify(vm.selectedDistrict1));
                    }
                }

                if (!country) {
                    var CountryGrep = $.grep(vm.countryList, function (n) { return n.CountryID == vm.vendor.country; });

                    country = CountryGrep[0];
                    console.log(country)
                    vm.vendor.state = "";
                    vm.vendor.city = "";
                    vm.vendor.district = "";
                    vm.selectedState1 = "";
                }
                if (country.Code === 'IDN') {
                    loadRegions(country.CountryID); vm.dataVendor
                    $translatePartialLoader.addPart('daftar');
                    localStorage.setItem('currLang', 'ID');
                    localStorage.setItem('country', JSON.stringify(country.CountryID));
                    $translate.preferredLanguage(getCurrLang());
                    loadCountries(country.CountryID)
                    // $window.location.reload();

                } else {
                    loadRegions(country.CountryID);
                    $translatePartialLoader.addPart('daftar');
                    localStorage.setItem('currLang', 'EN');
                    localStorage.setItem('country', JSON.stringify(country.CountryID));
                    $translate.preferredLanguage(getCurrLang());
                    loadCountries(country.CountryID)
                    // $window.location.reload();
                }
                $translate.use(getCurrLang());
                $translate.refresh(getCurrLang());
            }
            
        }
        function getCurrCountry() {
            return Number(localStorage.getItem('country'));
        }

        function getDataVendor() {
            return JSON.parse(localStorage.getItem('dataVendor'));
        }
        vm.getDataVendorState = getDataVendorState;
        function getDataVendorState() {
            return JSON.parse(localStorage.getItem('state1'));
        }
        vm.getDataCity = getDataCity;
        function getDataCity() {
            return JSON.parse(localStorage.getItem('city1'));
        }
        vm.getDataDistric = getDataDistric;
        function getDataDistric() {
            return JSON.parse(localStorage.getItem('distric1'));
        }
        function reloadStates(country) {
            //if (!country)
            vm.address.state = "";
            vm.address.city = "";
            vm.address.district = "";

            if (vm.address.country != undefined) {
                var CountryGrepAddress = $.grep(vm.addresscountryList, function (n) { return n.CountryID == vm.address.country; });
                country = CountryGrepAddress[0];
                
                //vm.addressDistrictList = [];
                //vm.addressCityList = [];
                if (country != undefined) {
                    reloadRegions(country.CountryID);
                    UIControlService.loadLoading("LOADERS.LOADING_STATE");
                    VendorRegistrationService.getStates(country.CountryID,
                        function (response) {
                            vm.addressStateList = response.data;
                            UIControlService.unloadLoading();
                        }, handleRequestError);

                }
            }
            
        }

        function loadCities(state) {
            vm.vendor.city = "";
            vm.vendor.district = "";
            if (state != undefined || vm.vendor.state != undefined) {
                if (!state) {
                    var stateGerp = $.grep(vm.stateList, function (n) { return n.StateID == vm.vendor.state; });

                    state = stateGerp[0];
                    
                    vm.selectedCity1 = "";
                }
                UIControlService.loadLoading("LOADERS.LOADING_CITY");
                VendorRegistrationService.getCities(state.StateID,
                    function (response) {
                        vm.cityList = response.data;
                        if (vm.dataVenCity) {
                            for (var i = 0; i < vm.cityList.length; i++) {
                                if (vm.dataVenCity !== "" && vm.dataVenCity.CityID === vm.cityList[i].CityID) {
                                    vm.vendor.city = vm.cityList[i];
                                    if (state.Country.Code === 'IDN') {
                                        loadDistricts(vm.vendor.city);
                                        break;
                                    }
                                }
                            }
                        }
                        UIControlService.unloadLoading();
                    }, handleRequestError);
            }
            
        }

        function loadDistricts(city) {
            vm.vendor.district = "";

            if (city != undefined || vm.vendor.city != undefined) {
                if (!city) {
                    var cityGerp = $.grep(vm.cityList, function (n) { return n.CityID == vm.vendor.city; });

                    city = cityGerp[0];
                    vm.selectedDistrict1 = "";
                }
                UIControlService.loadLoading("LOADERS.LOADING_DISTRICT");
                VendorRegistrationService.getDistricts(city.CityID,
                    function (response) {
                        vm.districtList = response.data;
                        if (vm.dataVenDistric) {
                            for (var i = 0; i < vm.districtList.length; i++) {
                                if (vm.dataVenDistric !== "" && vm.dataVenDistric.DistrictID === vm.districtList[i].DistrictID) {
                                    vm.vendor.district = vm.districtList[i];
                                    break;
                                }
                            }
                        }
                        UIControlService.unloadLoading();
                    }, handleRequestError);
            }
           
        }

        function loadPhoneCodes(data) {
            UIControlService.loadLoading("LOADERS.LOADING_PHONE");
            VendorRegistrationService.getCountries(
              function (response) {
                  vm.phoneCodeList = response.data;
                  for (var i = 0; i < vm.phoneCodeList.length; i++) {
                      vm.phoneCodeList[i].PrefixName = vm.phoneCodeList[i].Name + ' ( ' + vm.phoneCodeList[i].PhonePrefix + ' )';
                  }
                  if (data) {
                      for (var i = 0; i < vm.phoneCodeList.length; i++) {
                          if (vm.phoneCodeList[i].PhonePrefix === data) {
                              vm.phoneCode = vm.phoneCodeList[i].PhonePrefix;
                          }
                      }
                  }

                  UIControlService.unloadLoading();
              }, handleRequestError);
        }

        function setLang(lang) {
            localStorage.setItem('currLang', lang);
            $translate.preferredLanguage(getCurrLang());
            //initialize();
        }

        function getCurrLang() {
            if (localStorage.getItem("currLang") === null || localStorage.getItem("currLang") === '')
                return '';
            return localStorage.getItem("currLang");

        }
        function loadBusiness(data) {
            UIControlService.loadLoading("LOADERS.LOADING_BUSINESS");
            VendorRegistrationService.getBusiness(
                function (response) {
                    vm.businessList = response.data;
                    for (var i = 0; i < vm.businessList.length; i++) {
                        if (data !== undefined && data === vm.businessList[i].BusinessID) {
                            vm.vendor.business = vm.businessList[i];
                            break;
                        }
                    }
                    UIControlService.unloadLoading();
                },
                handleRequestError);
        }

        vm.cekBusinessReq = cekBusinessReq;
        function cekBusinessReq() {
            var businessId = vm.vendor.business.BusinessID;

            VendorRegistrationService.getBusinessReq({ BusinessID: businessId },
            function (response) {
                vm.businessReq = response.data;
                console.info("req" + JSON.stringify(vm.businessReq));
                UIControlService.unloadLoading();
            },
            handleRequestError);

        }

        function loadCurrencies() {
            UIControlService.loadLoading("LOADERS.LOADING_CURRENCY");
            VendorRegistrationService.getCurrencies(
                function (response) {
                    vm.currencyList = response.data;
                    UIControlService.unloadLoading();
                },
                handleRequestError);
        }

        function loadStockUnits(data) {
            UIControlService.loadLoading("LOADERS.LOADING_STOCK_TYPE");
            VendorRegistrationService.getStockTypes(
                function (response) {
                    vm.stockUnits = response.data;
                    if (data !== undefined) {
                        for (var i = 0; i < vm.stockUnits.length; i++) {
                            if (vm.stockUnits[i].RefID === data.Unit.RefID) {
                                vm.stockUnit = vm.stockUnits[i];
                                vm.flag = 1;
                            }
                        }

                    }

                    UIControlService.unloadLoading();
                },
                handleRequestError);
        }

        function loadOfficeType() {
            UIControlService.loadLoading("LOADERS.LOADING_OFFICE_TYPE");
            VendorRegistrationService.getOfficeType(
                function (response) {
                    if (response.status == 200) {
                        vm.officeTypes = response.data;
                    } else {
                        handleRequestError(response);
                    }
                },
                handleRequestError);
        }

        function reloadAddressCity() {
            vm.address.city = "";
            vm.address.district = "";
            if (vm.address.state != undefined) {
                var stateGerpAddress = $.grep(vm.addressStateList, function (n) { return n.StateID == vm.address.state; });
                var state = stateGerpAddress[0];

                UIControlService.loadLoading("LOADERS.LOADING_CITY");
                VendorRegistrationService.getCities(state.StateID,
                    function (response) {
                        vm.addressCityList = response.data;
                        UIControlService.unloadLoading();
                    }, handleRequestError);
            }
            
        }

        function reloadAddressDistrict(city) {
            vm.address.district = "";

            if (vm.address.city != undefined) {
                var cityGerpAddress = $.grep(vm.addressCityList, function (n) { return n.CityID == vm.address.city; });
                var city = cityGerpAddress[0];
                UIControlService.loadLoading("LOADERS.LOADING_DISTRICT");
                VendorRegistrationService.getDistricts(city.CityID,
                    function (response) {
                        vm.addressDistrictList = response.data;
                        UIControlService.unloadLoading();
                    }, handleRequestError);
            }
            
        }

        function handleRequestError(response) {
            UIControlService.log(response);
            UIControlService.handleRequestError(response.data, response.status);
            UIControlService.unloadLoading();
        }

        function openCalendar() {
            vm.isCalendarOpened = true;
        }

        function openDobCalendar() {
            vm.isDobCalendarOpened = true;
        }

        function loadConfig() {
            // load npwp filetype upload config

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_CONFIG");
            UploadFileConfigService.getByPageName(VENDOR_REGISTRATION_NO_PAGE_NAME, function (response) {
                if (response.status == 200) {
                    vm.npwpUploadConfigs = response.data;
                    vm.npwpFileTypes = generateFilterStrings(response.data);
                    vm.npwpFileSize = vm.npwpUploadConfigs[0];
                    UIControlService.unloadLoading();
                } else {
                    handleRequestError(response);
                }
            },
            handleRequestError);

            // load siup filetype upload config

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_CONFIG");
            UploadFileConfigService.getByPageName(VENDOR_LEGAL_NO_PAGE_NAME, function (response) {
                if (response.status == 200) {
                    vm.siupUploadConfigs = response.data;
                    vm.siupFileTypes = generateFilterStrings(response.data);
                    vm.siupFileSize = vm.siupUploadConfigs[0];
                    UIControlService.unloadLoading();
                } else {
                    handleRequestError(response);
                }
            },
            handleRequestError);

            // load id filetype upload config

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_CONFIG");
            UploadFileConfigService.getByPageName(STOCK_OWNER_ID_PAGE_NAME, function (response) {
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                    UIControlService.unloadLoading();
                } else {
                    handleRequestError(response);
                }
            },
            handleRequestError);
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        function validateFileTypeNPWP(file, allowedFileTypes) {
            var valid_size = allowedFileTypes[0].Size;
            var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
            var selectedFileType = file[0].name;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('.') + 1);

            ////jika excel
            var allowedFileType = 0;
            var allowedFileSize = 0;
            for (var i = 0; i < allowedFileTypes.length; i++) {
                if (allowedFileTypes[i].Name == selectedFileType) {
                    allowedFileType = 1;
                }
            }

            if (allowedFileType == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_INVALID_FILETYPE_NPWP");
                return false;
            }

            if (size_file > valid_size) {
                UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE_NPWP");
                return false;
            } else {
                allowedFileSize = 1;
            }

            if (allowedFileType == 1 && allowedFileSize == 1) {
                return true;
            }
        }

        function validateFileTypeSIUP(file, allowedFileTypes) {
            var valid_size = allowedFileTypes[0].Size;
            var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
            var selectedFileType = file[0].name;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('.') + 1);

            ////jika excel
            var allowedFileType = 0;
            var allowedFileSize = 0;
            for (var i = 0; i < allowedFileTypes.length; i++) {
                if (allowedFileTypes[i].Name == selectedFileType) {
                    allowedFileType = 1;
                }
            }

            if (allowedFileType == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_INVALID_FILETYPE_SIUP");
                return false;
            }

            if (size_file > valid_size) {
                UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE_SIUP");
                return false;
            } else {
                allowedFileSize = 1;
            }

            if (allowedFileType == 1 && allowedFileSize == 1) {
                return true;
            }
        }

        function stockOwnerIDSelected() {
            vm.stock.owner.id_doc = vm.stockOwnerID;
        }

        function npwpDocSelected() {
            vm.vendor.npwp_doc = vm.npwpDoc;
            //console.log(vm.npwpDoc);
        }

        function sktDocSelected() {
            vm.vendor.skt_doc = vm.sktDoc;
        }

        function upload(file, config, filters, callback) {
            //console.info(file);
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == SIZE_UNIT_KB) {
                size *= 1024;
            }

            if (unit == SIZE_UNIT_MB) {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadRegistration(file, vm.vendor.npwp, config.Prefix, size, filters,
                function (response) {
                    if (response.status == 200) {
                        var url = response.data.Url;
                        UIControlService.unloadLoading();
                        callback(response.data);
                    } else {
                        UIControlService.handleRequestError("ERR_UPLOAD_NPWP");
                        UIControlService.unloadLoading();
                    }
                },
                function (response) {
                    UIControlService.handleRequestError("ERR_UPLOAD_NPWP");
                    UIControlService.unloadLoading();
                });

        }

        function uploadStock(id, prefix, file, config, filters, callback) {
            //console.info(file);
            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == SIZE_UNIT_KB) {
                size *= 1024;
            }

            if (unit == SIZE_UNIT_MB) {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFileStock(id, prefix, file, size, filters,
                function (response) {
                    if (response.status == 200) {
                        var url = response.data.Url;
                        UIControlService.unloadLoading();
                        callback(response.data);
                    } else {
                        UIControlService.handleRequestError("ERR_UPLOAD_NPWP");
                        UIControlService.unloadLoading();
                    }
                },
                function (response) {
                    UIControlService.handleRequestError("ERR_UPLOAD_NPWP");
                    UIControlService.unloadLoading();
                });

        }
        vm.tglSekarang = UIControlService.getDateNow("");
        vm.urlTenderDoc = null;
        vm.addTenderDoc = addTenderDoc;
        function addTenderDoc(url) {
            if (vm.tenderInterestDocData != null) {
                if (url != '') {
                    vm.tenderInterestDocData.DocumentUrl = url;
                    vm.tenderInterestDoc.push(vm.tenderInterestDocData);
                    vm.tenderInterestDocData = null;
                }
                else {
                    if (vm.docTender == undefined || vm.tenderInterestDocData.DocumentName == undefined) {
                        UIControlService.msg_growl("err", "Tidak ada file yang dipilih");
                        return
                    }
                    else {
                        if (UIControlService.validateFileType(vm.docTender, vm.idUploadConfigs)) {
                            uploadSingleDoc(vm.docTender, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
                        }
                    }
                }
            }
            else {
                UIControlService.msg_growl('warning', "Dokumen tender harus lengkap");
                return;
            }
            console.info("temp Doc:" + JSON.stringify(vm.tenderInterestDoc));
        }

        vm.removeTenderDoc = removeTenderDoc;
        function removeTenderDoc(obj) {
            vm.tenderInterestDoc = remove(vm.tenderInterestDoc, obj);
        }

        function uploadSingleDoc(file, config, filters, dates) {
            //console.info("doctype:" + doctype);
            //console.info("file"+JSON.stringify(file));
            var size = config.Size;

            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.pathFile = url;
                        addTenderDoc(vm.pathFile);
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

        }

        function addContact() {
            vm.contact.countryCode = vm.phoneCode;
            vm.contactPers = vm.contact.phone;
            checkTelp(false);
            if (!validateRequiredField(vm.contact.name)) return;
            //if (!validateRequiredField(vm.contact.phone)) return;
            //if (!validateRequiredField(vm.contact.countryCode)) return;
            var cekEmail = $.grep(vm.contacts, function (n) { return n.email == vm.contact.email; });
            if (cekEmail.length > 0) {
                UIControlService.msg_growl('error', "ERRORS.EMAIL_EXIST");
                return;
            }
            if (vm.contact.email == vm.vendor.email) {
                UIControlService.msg_growl('error', "ERRORS.VENDOR_EMAIL_EXIST");
                return;
            }
            UIControlService.loadLoading();

            checkEmail(false).then(function (response) {
                if (response) {
                    UIControlService.unloadLoading();

                    vm.contacts.push(vm.contact);
                    vm.contact = {};
                    vm.vendorContactForm.$setPristine();
                }

            })


        }

        function removeContact(obj) {
            vm.contacts = remove(vm.contacts, obj);
        }

        function addStockData() {
            var sameId = false;
           
            if (vm.stockOpt == undefined || vm.stockOpt == '') {
                UIControlService.msg_growl('error', "FORM.STOCK_OPTION_EMPTY");
                return;
            }

            if (vm.stock.owner.name == undefined || vm.stock.owner.name == '') {
                UIControlService.msg_growl('error', "FORM.STOCK_OWNER_NAME_EMPTY");
                return;
            }

            for (var i = 0; i < vm.stocks.length; i++) {
                if (vm.stock.owner.id === vm.stocks[i].owner.id) {
                    sameId = true;
                    i = vm.stocks.length;
                }
            }
            if (sameId === true) {
                UIControlService.msg_growl('error', "FORM.SAME_ID_STOCK");
                return;
            }
            if (vm.stock.owner.dob == undefined || vm.stock.owner.dob == "") {
                UIControlService.msg_growl('error', "FORM.INVALID_DATE");
                return;
            }

            if (vm.stock.owner.id == undefined || vm.stock.owner.id == '') {
                if (vm.stockOpt.Name == 'STOCK_PERSONAL') {
                    UIControlService.msg_growl('error', $filter('translate')("FORM.STOCK_OWNER_ID_EMPTY") + $filter('translate')("FORM.NO_KTP"));

                } else {
                    UIControlService.msg_growl('error', $filter('translate')("FORM.STOCK_OWNER_ID_EMPTY") + $filter('translate')("FORM.NO_NPWP"));

                }
                return;
            }

            if (vm.stockOwnerID == undefined || vm.stockOwnerID == '') {
              
                if (vm.stockOpt.Name == 'STOCK_PERSONAL') {
                    UIControlService.msg_growl('error',  $filter('translate')("FORM.STOCK_OWNER_FILE_EMPTY") + $filter('translate')("FORM.KET_FILE_PERSONAL"));

                } else {
                    UIControlService.msg_growl('error', $filter('translate')("FORM.STOCK_OWNER_FILE_EMPTY") + $filter('translate')("FORM.KET_FILE_COMPANY"));

                }
                return;
            }

            if (vm.stockUnit == undefined || vm.stockUnit == '') {
                UIControlService.msg_growl('error', "FORM.STOCK_UNIT_EMPTY");
                return;
            }
            //var tanggal = vm.stock.owner.dob.split("-").reverse().join("-");
            //var date = new Date(tanggal);
            //if (isNaN(date.getTime())) {
            //    UIControlService.msg_growl('error', "FORM.INVALID_DATE");
            //    return;
            //}
            if (vm.stock.quantity <= 0) {
                UIControlService.msg_growl('error', "FORM.NOTNULL");
                return;
            }
            if (vm.stockUnit.Name === "STOCK_UNIT_PERCENTAGE") {
                if (vm.stock.quantity > 100) {
                    UIControlService.msg_growl('error', "FORM.MAX_PERSEN");
                    return;
                }
                else {
                    var jumlah = 0;
                    for (var i = 0; i < vm.stocks.length; i++) {
                        jumlah += +vm.stocks[i].quantity;
                        if (i == (vm.stocks.length - 1)) {
                            if ((+vm.stock.quantity + +jumlah).toFixed(2) > 100) {
                                UIControlService.msg_growl('error', "FORM.MAX_PERSEN");
                                return;
                            }
                        }
                    }
                }
            }
            //console.info("vm.stocks" + JSON.stringify(vm.stock));
            if (vm.stockForm.$invalid) return;

            UIControlService.loadLoading();


            cekOwnerID();

            VendorRegistrationService.isAnotherStockHolder({
                VendorID: 0,
                OwnerID: vm.stock.owner.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 && reply.data === true) {
                    UIControlService.msg_growl('error', "ERRORS.IS_ANOTHER_STOCKHOLDER");
                } else {
                    if (UIControlService.validateFileType(vm.stockOwnerID, vm.idUploadConfigs)) {
                        vm.idFileSize.Prefix = vm.prefixes.UPLOAD_PREFIX_ID.Value;
                        uploadStock(0, vm.stock.owner.id, vm.stockOwnerID, vm.idFileSize, vm.idFileTypes, addStockToList);
                    }
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_VERIFY_STOCKHOLDER");
                if (UIControlService.validateFileType(vm.stockOwnerID, vm.idUploadConfigs)) {
                    vm.idFileSize.Prefix = vm.prefixes.UPLOAD_PREFIX_ID.Value;
                    uploadStock(0, vm.stock.owner.id, vm.stockOwnerID, vm.idFileSize, vm.idFileTypes, addStockToList);
                }
            });
        }

        function addStockToList(obj) {
            vm.stock.owner.idDoc = obj.Url;
            vm.stock.owner.idFilename = obj.FileName;
            vm.stock.unit = vm.stockUnit;
            vm.stock.type = vm.stockOpt;
            vm.stock.Position = obj.Position;
            vm.stock.Currency = vm.stockUnitCurrency;
            if (vm.stockUnit.Value === 'STOCK_UNIT_CURRENCY')
                vm.stock.CurrencySymbol = vm.stockUnitCurrency.Symbol;
            vm.flag = 1;
            //vm.stock.currency = vm.stockUnitCurrency;
            vm.stock.Tmpblacklist = vm.Tmpblacklist;
            vm.stocks.push(vm.stock);
            vm.stock = {
                quantity: 0,
                unit: {},
                type: {},
                owner: {
                    name: "",
                    dob: "",
                    id: "",
                    doc_id: ""
                },
                Position: ""
            };
            //vm.stockUnit = "";
            //vm.stockUnitCurrency = "";
            vm.stockForm.$setPristine();
        }

        function removeStock(obj) {
            vm.stocks = remove(vm.stocks, obj);
            if (vm.stocks.length === 0) {
                vm.stockUnit = {};
                vm.flag = 0;
            }
        }

        function remove(list, obj) {
            var index = list.indexOf(obj);
            if (index >= 0) {
                list.splice(index, 1);
            } else {
                UIControlService.msg_growl('error', "ERRORS.OBJECT_NOT_FOUND");
            }
            return list;
        }

       

       

        function addAddress() {
            var address = vm.address;

            if (address.type == '') {
                UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_TYPE_EMPTY.MESSAGE", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_TYPE_EMPTY.TITLE");
                return;
            }

            if (address.type.Value == '') {
                UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_EXIST.MESSAGE", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_EXIST.TITLE");
                return;
            }

            if (address.detail == '') {
                UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.DETAIL_ADDRESS_EMPTY.MESSAGE", "FORM.VALIDATION_ERRORS.DETAIL_ADDRESS_EMPTY.TITLE");
                return;
            }

            if (address.country == '' || address.country == undefined) {
                UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.COUNTRY_EMPTY.MESSAGE", "FORM.VALIDATION_ERRORS.COUNTRY_EMPTY.TITLE");
                return;
            }

            if (address.state == '' || address.state == undefined) {
                UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.STATE_EMPTY.MESSAGE", "FORM.VALIDATION_ERRORS.STATE_EMPTY.TITLE");
                return;
            }

            if (address.country == 360) {
                if (vm.addressCityList.length != 0) {
                    if (address.city == '' || address.city == undefined) {
                        UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.CITY_EMPTY.MESSAGE", "FORM.VALIDATION_ERRORS.CITY_EMPTY.TITLE");
                        return;
                    }
                }

                if (vm.addressDistrictList.length != 0) {
                    if (address.district == '' || address.district == undefined) {
                        UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.DISTRICT_EMPTY.MESSAGE", "FORM.VALIDATION_ERRORS.DISTRICT_EMPTY.TITLE");
                        return;
                    }
                }
            }

            if (!address.postalCode)
                address.postalCode = "";

            //if (vm.addressCityList.length != 0) {
            //    if (address.city == "") {
            //        UIControlService.msg_growl("error", "ERRORS.CITY_FILL");
            //        return;
            //    }
            //}

            //if (vm.addressDistrictList.length != 0) {
            //    if (address.district == "") {
            //        UIControlService.msg_growl("error", "ERRORS.DISTRICT_FILL");
            //        return;
            //    }
            //}

            var CountryGrepAddress = $.grep(vm.addresscountryList, function (n) { return n.CountryID == address.country; });
            var cityGerpAddress = $.grep(vm.addressCityList, function (n) { return n.CityID == address.city; });
            var stateGerpAddress = $.grep(vm.addressStateList, function (n) { return n.StateID == address.state; });
            var districtGrepAddress = $.grep(vm.addressDistrictList, function (n) { return n.DistrictID == address.district; });

            address.country = CountryGrepAddress[0];
            address.city = cityGerpAddress[0];
            address.state = stateGerpAddress[0];
            address.district = districtGrepAddress[0];
            address.Association = vm.address.AssociationID;
            if (vm.address.AssociationID != '') {
                vm.vendor.AssociationID = vm.address.AssociationID.AssosiationID;
            }
            address.info = "";
            if (address.city) {
                address.info += address.city.Name+",";
            }

            address.info += address.state.Name + "," + address.state.Country.Name;

            if (address.type.Name == VENDOR_OFFICE_TYPE_MAIN) {
                if (hasMainOffice()) {
                    UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_EXIST.MESSAGE", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_EXIST.TITLE");
                    return;
                }
            }
            vm.addresses.push(address);
            //console.info("alamats" + JSON.stringify(vm.addresses));
            vm.address = {
                type: '',
                city: '',
                state: '',
                country: '',
                detail: '',
                AssociationID: ''
            };
            //vm.addressStateList = [];
            //vm.addressCityList = [];
            //vm.addressDistrictList = [];
            vm.vendorAddressForm.$setPristine();
        }

        vm.currencies = [];
        function addCurrency() {
            vm.flagCurrVendor = true;
            var i = 0;
            if (vm.vendor.currency.Symbol === undefined || vm.vendor.currency.Symbol === null) {
                UIControlService.msg_growl('error', "ERRORS.CURRENCY_NOT_FOUND");
                return;
            } else {
                for (i = 0; i < vm.currencies.length; i++) {
                    if (vm.currencies[i].CurrencyID == vm.vendor.currency.CurrencyID) {
                        vm.flagCurrVendor = false;
                        UIControlService.msg_growl('error', "ERRORS.CURRENCY_EXIST");
                        return;
                    }
                }
                //console.info(i);
                if (i == vm.currencies.length && vm.flagCurrVendor === true) {

                    //console.info(vm.vendor.currency);
                    var currency = vm.vendor.currency;
                    vm.currencies.push(currency);
                }

                vm.vendor.currency = {};
            }
        }

        function hasMainOffice() {
            for (var i = 0; i < vm.addresses.length; i++) {
                if (vm.addresses[i].type.Name == VENDOR_OFFICE_TYPE_MAIN) {
                    return true;
                }
            }
            return false;
        }

        function removeAddress(address) {
            vm.addresses = remove(vm.addresses, address);
        }

        function removeCurrency(currency) {
            vm.currencies = remove(vm.currencies, currency);
        }

        function validateRequiredField(value) {
            if (!value) {
                UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.REQUIRED.MESSAGE", "FORM.VALIDATION_ERRORS.REQUIRED.TITLE");
                UIControlService.unloadLoading();
                return false;
            }
            return true;
        }

        function checkCIVD(npwp) {
            var defer = $q.defer();
            var npwpArr = npwp.split("");
            var npwpKirim = npwp;
            console.log(npwpArr)
            if (vm.vendor.country == 360) {

                npwpKirim = '';
                for (var i = 0; i < npwpArr.length; i++) {
                    npwpKirim += npwpArr[i];
                    if (i == 1) {
                        npwpKirim += ".";
                    }
                    if (i == 4) {
                        npwpKirim += ".";
                    }
                    if (i == 7) {
                        npwpKirim += "."
                    }
                    if (i == 8) {
                        npwpKirim += "-";
                    }
                    if (i == 11) {
                        npwpKirim += "."
                    }
                }

            }
            var url = "https://apiprovider.civd-migas.com/vendor/allVendor?npwp=" + npwpKirim;
            UIControlService.loadLoading();

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                //UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply);
                } else {
                    defer.reject(false)
                }
            }, function (error) {
                // UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject();
            });

            return defer.promise;
        }

        function checkAfiliasi(npwp) {
            var defer = $q.defer();
            var url = "https://apiprovider.civd-migas.com/vendor/afiliasi?vendorNPWP=" + npwp;

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply);
                } else {
                    defer.reject(false)
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject();
            });

            return defer.promise;
        }

        function checkBankDetail(vendorName) {
            var defer = $q.defer();
            var url = "https://apiprovider.civd-migas.com/vendor/rekeningBank?vendorName=" + vendorName;

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply);
                } else {
                    defer.reject(false)
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject();
            });

            return defer.promise;
        }

        function checkNpwp() {
            //var kirimSimpan = {
            //    Name: '',
            //    Npwp: '',
            //    NpwpUrl: '',
            //    VendorTypeID: '',
            //    SupplierID: '',
            //    CityName: '',
            //    PostalCode: '',
            //    Email: 'avidaditya12@gmail.com',
            //    EmailAlt: 'rekanan.itdc1@gmail.com',
            //    addressString: '',
            //    Phone: '',
            //    Fax: '',
            //    provName: '',
            //    CIVDID: '',
            //    Website: '',
            //    spdaFile: '',
            //    spdaNo: '',
            //    expiredDate: '',
            //    contactPerson: '',
            //    BusinessID: ''

            //}

            //var data = {
            //    item: kirimSimpan
            //};
            //var modalInstance = $uibModal.open({
            //    templateUrl: 'app/modules/visitor/daftar/civdEmailSend.modal.html',
            //    backdrop: "static",
            //    controller: 'CivdEmailSendModalController',
            //    controllerAs: 'CivdEmailSendModalCtrl',
            //    resolve: {
            //        item: function () {
            //            return data;
            //        }
            //    }
            //});
            //return;
            if (vm.vendor.country == 360) {
                console.log(vm.vendor.npwp)
                if (vm.vendor.country.length < 12 && vm.vendor.country.length > 0) {
                    UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.NPWP_NOT_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_NOT_AVAILABLE.TITLE');

                }
            }

            if (vm.vendor.country == undefined || vm.vendor.country == '') {
                UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.COUNTRY_EMPTY.MESSAGE', 'FORM.VALIDATION_ERRORS.COUNTRY_EMPTY.TITLE');
                return;
            }

            var swal = {
                titleNotification: $filter('translate')('SWAL.NOTIFICATION'),
                textNotActivated: $filter('translate')('SWAL.NOTACTIVATED'),
                textCivd: $filter('translate')('SWAL.TEXTCIVD'),
                textIsActivated: $filter('translate')('SWAL.ISACTIVATED')
            }
            //var isCivd = false;
            if (vm.vendor.npwp && vm.vendor.npwp != '') {
                UIControlService.loadLoading("LOADERS.CHECKING_NPWP");

                VendorRegistrationService.checkNpwp(vm.vendor.npwp, function (response) {
                    var data = response.data;
                    if (!vm.npwpPass) {
                        if (!data.IsCheckedNpwp) {
                            UIControlService.unloadLoading();
                            //loadCountries();
                            vm.npwpPass = true;
                            return;
                            checkCIVD(vm.vendor.npwp).then(function (response) {
                                if (response.result.length > 0) {
                                    UIControlService.unloadLoading();
                                    Swal.fire({
                                        title: swal.titleNotification,
                                        text: swal.textCivd,
                                        icon: 'info',
                                        showCancelButton: false,
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: '<i class="fa fa-envelope"></i> Kirim Email !',
                                    }).then((result) => {
                                        var data = response.result[0];

                                        if (result.value) {
                                            // Fungsi Insert ke Eproc commit or commitToUpdate terus email butuh json e
                                            var VendorTypeID = 0;
                                            if (data.jenisUsaha == "Barang") {
                                                VendorTypeID = 3090;
                                            } else if (data.jenisUsaha == "Jasa") {
                                                VendorTypeID = 3091;
                                            } else if (data.jenisUsaha == "Barang & Jasa") {
                                                VendorTypeID = 3092;
                                            }

                                            var supplierID = 0;
                                            if (data.pabrikan == "Pabrikan") {
                                                supplierID = 3093;
                                            } else if (data.pabrikan == "Non pabrikan/Agen/Distributor") {
                                                supplierID = 3094;
                                            } else if (data.pabrikan == "Agen/Distributor") {
                                                supplierID = 3095;
                                            } else {
                                                supplierID = 3096;
                                            }

                                            var BusinessID = 9;
                                            if (data.compType == "PT") {
                                                BusinessID = 1
                                            }
                                            if (data.compType == "CV") {
                                                BusinessID = 2
                                            }
                                            if (data.compType == "FA") {
                                                BusinessID = 3
                                            }
                                            if (data.compType == "UD") {
                                                BusinessID = 5
                                            }
                                            if (data.compType == "Koperasi") {
                                                BusinessID = 6
                                            }
                                            if (data.compType == "PD") {
                                                BusinessID = 8
                                            }

                                            data.cityName = data.cityName.replace('KOTA ', '');

                                            var kirimSimpan = {
                                                Name: data.name,
                                                Npwp: vm.vendor.npwp,
                                                NpwpUrl: data.docNPWP,
                                                VendorTypeID: VendorTypeID,
                                                SupplierID: supplierID,
                                                CityName: data.cityName,
                                                PostalCode: data.zipCode,
                                                Email: data.vendorEmail1,
                                                EmailAlt: data.altEmail,
                                                addressString: data.address,
                                                Phone: data.phoneNumber,
                                                Fax: data.faxNumber,
                                                provName: data.provName,
                                                CIVDID: data.vendorId,
                                                Website: data.website,
                                                spdaFile: data.spdaFile,
                                                spdaNo: data.spdaNo,
                                                expiredDate: data.expiredDate,
                                                contactPerson: data.contactPerson,
                                                BusinessID: BusinessID

                                            }

                                            var data = {
                                                item: kirimSimpan
                                            };
                                            var modalInstance = $uibModal.open({
                                                templateUrl: 'app/modules/visitor/daftar/civdEmailSend.modal.html',
                                                backdrop:"static",
                                                controller: 'CivdEmailSendModalController',
                                                controllerAs: 'CivdEmailSendModalCtrl',
                                                resolve: {
                                                    item: function () {
                                                        return data;
                                                    }
                                                }
                                            });

                                        }
                                    })
                                    console.log(data)
                                } else {
                                    UIControlService.unloadLoading();
                                    loadCountries();
                                    vm.npwpPass = true;
                                }
                            })
                        }
                        if (data.IsCheckedNpwp && data.IsActived == null) {
                            UIControlService.unloadLoading();
                            Swal.fire({
                                title: swal.titleNotification,
                                icon: 'info',
                                text: swal.textNotActivated
                            })
                        }
                        if (data.IsCheckedNpwp && data.IsActived == 1) {
                            UIControlService.unloadLoading();
                            //Swal.fire({
                            //    title: swal.titleNotification,
                            //    icon: 'question',
                            //    text: swal.textIsActivated,
                            //    confirmButtonText: 'Login',
                            //    cancelButtonText: 'Cancel',
                            //    showCancelButton: true,
                            //    showCloseButton: true
                            //})
                            Swal.fire({
                                title: swal.titleNotification,
                                icon: 'info',
                                text: swal.textIsActivated
                            }).then(function () {
                                window.location = "#/login/";
                            })
                        }
                    } else {
                        if (data.IsCheckedNpwp == false) {
                            UIControlService.msg_growl('notice', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.TITLE');
                            if (data.VendorID !== 0) {
                                loadVendor(data.VendorID);
                            }
                            UIControlService.unloadLoading();
                        } else {
                            UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.NPWP_NOT_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_NOT_AVAILABLE.TITLE');
                            UIControlService.unloadLoading();
                            vm.vendor.npwp = '';

                        }

                    }
                }, handleRequestError);
                return;
            }
            else {
                UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.NPWP_EMPTY.MESSAGE', 'FORM.VALIDATION_OK.NPWP_EMPTY.TITLE');

            }
        }

        vm.checkCekCompany = checkCekCompany;
        function checkCekCompany() {
            if (vm.vendor.npwp) {
                UIControlService.loadLoading("LOADERS.CHECKING_NPWP");
                VendorRegistrationService.checkNpwp(vm.vendor.npwp, function (response) {
                    var data = response.data;
                    if (data.IsCheckedNpwp == false) {
                        UIControlService.msg_growl('notice', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.TITLE');
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.TITLE');
                        UIControlService.unloadLoading();
                        vm.vendor.npwp = '';

                    }
                }, handleRequestError);
            }
        }

        vm.dataTender = null;
        function checkTender() {
            if (vm.vendor.TenderCodeGenerate) {
                UIControlService.loadLoading("LOADERS.CHECKING_TENDER");
                VendorRegistrationService.checkTender(vm.vendor.TenderCodeGenerate, function (response) {
                    vm.tenderAvailable = response.data;
                    if (vm.tenderAvailable == 2) {
                        if (vm.vendor.TenderCodeGenerate.includes("PREQUAL")) UIControlService.msg_growl('notice', 'FORM.PREQUALVALID');
                        else UIControlService.msg_growl('notice', 'FORM.TENDERVALID');
                        UIControlService.unloadLoading();
                        VendorRegistrationService.checkTenderName(vm.vendor.TenderCodeGenerate, function (response) {
                            console.info("tenderdata:" + JSON.stringify(response.data));
                            vm.dataTender = response.data;
                            vm.vendor.tendercodetemp = response.data.TenderCode;
                            vm.tenderName = response.data.TenderName;
                            if (response.data.ProcPackageType == 4189) {
                                vm.isTenderJasa = true;
                                if (vm.dataTender.TenderStepDatas.length > 0) {
                                    for (var i = 0; i <= vm.dataTender.TenderStepDatas.length - 1; i++) {
                                        if (vm.dataTender.TenderStepDatas[i].IsOpenTender == true) {
                                            vm.isOpenTender = true;
                                            i = vm.dataTender.TenderStepDatas.length - 1;
                                        }
                                    }
                                }
                            }
                            else {
                                vm.isTenderJasa = false;
                            }
                        }, handleRequestError);
                    } else if (vm.tenderAvailable == 1) {
                        if (vm.vendor.TenderCodeGenerate.includes("PREQUAL")) UIControlService.msg_growl('error', 'FORM.EXPIREDPREQUAL');
                        else UIControlService.msg_growl('error', 'FORM.EXPIREDTENDER');
                        UIControlService.unloadLoading();
                        vm.vendor.TenderCodeGenerate = null;
                        vm.vendor.tendercodetemp = null;
                        vm.tenderName = "";
                        vm.isTenderJasa = false;
                        vm.isOpenTender = false;
                    } else if (vm.tenderAvailable == 0) {
                        if (vm.vendor.TenderCodeGenerate.includes("PREQUAL")) UIControlService.msg_growl('error', 'FORM.PREQUALNOTVALID');
                        else UIControlService.msg_growl('error', 'FORM.TENDERNOTVALID');
                        UIControlService.unloadLoading();
                        vm.vendor.TenderCodeGenerate = null;
                        vm.vendor.tendercodetemp = null;
                        vm.tenderName = "";
                        vm.isTenderJasa = false;
                        vm.isOpenTender = false;
                    }
                }, handleRequestError);
            }
        }

        //vm.checkTenderByRegist = checkTenderByRegist;
        function checkTenderByRegist() {
            if (vm.vendor.tendercodetemp || vm.vendor.TenderCodeGenerate) {
                UIControlService.loadLoading("LOADERS.CHECKING_TENDER");
                VendorRegistrationService.checkTender(vm.vendor.TenderCodeGenerate,
                    function (response) {
                        UIControlService.unloadLoading();
                        vm.tenderAvailable = response.data;
                        if (vm.tenderAvailable != 2) {
                            vm.cek = 1;
                            UIControlService.msg_growl('error', 'ERRORS.TENDERCODENOTAVAIL');
                            return;
                        } else {
                            saveData();
                        }
                    }, handleRequestError);
            }
            else {
                saveData();
            }
        }

        vm.checkInclude = checkInclude;
        function checkInclude() {
            if (vm.vendor.TenderCodeGenerate.includes("PREQUAL")) return false;
            else return true;
        }

        function updateUsername() {
            vm.vendor.username = vm.vendor.name.replace(/ /gi, '_').toLowerCase();
        }

        function Captcha() {
            vm.passCaptcha = UIControlService.generateCaptcha('textCanvas', 'image');
        };

        function validCaptcha() {
            var valid = UIControlService.verifyCaptcha(vm.passCaptcha, 'txtInput');

            if (!valid) {
                Captcha();
            }
            return valid;
        };

        function uploadNpwp(callback, param) {
            var size = vm.npwpFileSize.Size;
            var unit = vm.npwpFileSize.SizeUnitName;
            if (unit == SIZE_UNIT_KB) {
                size *= 1024;
            }

            if (unit == SIZE_UNIT_MB) {
                size *= (1024 * 1024);
            }
            if (vm.vendor.npwp == vm.Npwp && vm.npwpDoc == vm.NpwpUrl) {
                vm.vendor.npwpUrl = vm.NpwpUrl;
                if (vm.vendor.country == 360) {
                    //uploadSiup();
                    if (validateFileTypeSIUP(vm.sktDoc, vm.siupUploadConfigs)) {
                        uploadSiup();
                    } else {
                        return;
                    }
                } else {
                    commitUpdate();
                }
            } else {
                UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
                UploaderService.uploadRegistration(vm.npwpDoc, vm.vendor.npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, vm.npwpFileTypes, function (response) {
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.vendor.npwpUrl = url;
                        UIControlService.unloadLoading();
                        if (vm.vendor.country == 360) {
                            if (vm.vendor.siup == vm.Siup && vm.sktDoc == vm.siupUrl) {
                                vm.vendor.siupUrl = vm.siupUrl;
                                if (validateFileTypeSIUP(vm.sktDoc, vm.siupUploadConfigs)) {
                                    uploadSiup();
                                } else {
                                    return;
                                }
                            }
                            else {
                                if (validateFileTypeSIUP(vm.sktDoc, vm.siupUploadConfigs)) {
                                    callback.apply(this, param);
                                } else {
                                    return;
                                }
                            }
                                
                        } else {
                            if (vm.vendor.npwp == vm.Npwp)
                                commitUpdate();
                            else
                                commit();
                        }
                    } else {
                        UIControlService.msg_growl('error', 'FORM.NOTCOMPLETENPWP');
                        UIControlService.unloadLoading();
                    }
                }, function (response) {
                    UIControlService.msg_growl('error', 'FORM.NOTCOMPLETENPWP');
                    UIControlService.unloadLoading();
                });
            }
        }

        function uploadSiup() {
            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            var size = vm.siupFileSize.Size;
            var unit = vm.siupFileSize.SizeUnitName;
            if (unit == SIZE_UNIT_KB) {
                size *= 1024;
            }

            if (unit == SIZE_UNIT_MB) {
                size *= (1024 * 1024);
            }
            if (vm.vendor.siup == vm.Siup && vm.sktDoc == vm.siupUrl) {
                vm.vendor.siupUrl = vm.siupUrl;
                commitUpdate();
            } else {
                var direktoriLegalDocument = vm.vendor.siup.toString().replace(/ /g, "_");
                UploaderService.uploadRegistration(vm.sktDoc, direktoriLegalDocument, vm.prefixes.UPLOAD_PREFIX_SIUP.Value, size, vm.siupFileTypes, function (response) {
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.vendor.siupUrl = url;
                        UIControlService.unloadLoading();
                        if (vm.vendor.npwp == vm.Npwp)
                            commitUpdate();
                        else
                            commit();
                    } else {
                        UIControlService.handleRequestError("FORM.NOTCOMPLETESIUP");
                        UIControlService.unloadLoading();
                    }
                }, function (response) {
                    UIControlService.handleRequestError("FORM.NOTCOMPLETESIUP");
                    UIControlService.unloadLoading();
                });

            }
        }

        function commit() {
            var phoneComp = vm.vendor.phone;
            vm.vendor.phone = '(' + vm.phoneCode + ') ' + vm.vendor.phone;
            vm.vendor.FoundedDate = null;

            var CountryGrep = $.grep(vm.countryList, function (n) { return n.CountryID == vm.vendor.country; });
            var stateGerp = $.grep(vm.stateList, function (n) { return n.StateID == vm.vendor.state; });

            if (vm.vendor.city != "") {
                var cityGerp = $.grep(vm.cityList, function (n) { return n.CityID == vm.vendor.city; });
                vm.vendor.city = cityGerp[0];

            }
            if (vm.vendor.district != "") {
                var districtGrep = $.grep(vm.districtList, function (n) { return n.DistrictID == vm.vendor.district; });
                vm.vendor.district = districtGrep[0];
            }

            vm.vendor.country = CountryGrep[0];
            vm.vendor.state = stateGerp[0];

            UIControlService.loadLoading("LOADERS.LOADING_REGISTER");
            VendorRegistrationService.register(vm.vendor,
			function (response) {
			    UIControlService.unloadLoading();
			    if (response.status == 200) {
			        vm.vendorToEmail = response.data;
			        UIControlService.msg_growl("notice", "Registration complete!");
			        sendEmail();
			        SocketService.emit("daftarRekanan");
			        $state.go('login-panitia');
			    } else {
			        UIControlService.handleRequestError(response.data);
			        vm.vendor.phone = phoneComp;

			    }
			},
			function (response) {
			    UIControlService.handleRequestError(response.data);
			    UIControlService.unloadLoading();
			    vm.vendor.phone = phoneComp;
			});
        }

        function commitUpdate() {
            var phoneComp = vm.vendor.phone;
            vm.vendor.phone = '(' + vm.phoneCode + ') ' + vm.vendor.phone;
            vm.vendor.FoundedDate = null;
            UIControlService.loadLoading("LOADERS.LOADING_REGISTER");
            VendorRegistrationService.registerUpdate(vm.vendor,
			function (response) {
			    UIControlService.unloadLoading();
			    if (response.status == 200) {
			        vm.vendorToEmail = response.data;
			        UIControlService.msg_growl("notice", "MESSAGE.REG_COMPLETE");
			        sendEmail();
			        $state.go('login-panitia');
			    } else {
			        UIControlService.handleRequestError(response.data);
			        vm.vendor.phone = phoneComp;
			    }
			},
			function (response) {
			    UIControlService.handleRequestError(response.data);
			    UIControlService.unloadLoading();
			    vm.vendor.phone = phoneComp;
			});
        }

        function checkNamaPerusahaan() {
            var defer = $q.defer();
            VendorRegistrationService.CheckNamaPerusahaan({ Keyword: vm.vendor.name },
           function (response) {
               UIControlService.unloadLoading();
               if (response.status == 200) {
                   var data = response.data;
                   var send = !response.data;
                   console.log(data)
                   console.log(send)
                   defer.resolve(send);
               } else {
                   UIControlService.handleRequestError(response.data);
                   //UIControlService.msg_growl('error', 'ERRORS.CITY');
                   defer.reject();
               }
           },
           function (response) {
               UIControlService.handleRequestError(response.data);
               defer.reject();
               UIControlService.unloadLoading();
           });
            return defer.promise;
        }

        function checkUsername() {
            var defer = $q.defer();
            VendorRegistrationService.checkUsername({ Keyword: vm.vendor.username },
            function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var data = response.data;
                    var send = !response.data;
                    console.log(data)
                    console.log(send)
                    defer.resolve(send);
                } else {
                    UIControlService.handleRequestError(response.data);
                    //UIControlService.msg_growl('error', 'ERRORS.CITY');
                    defer.reject();
                }
            },
            function (response) {
                UIControlService.handleRequestError(response.data);
                defer.reject();
                UIControlService.unloadLoading();
            });
            return defer.promise;
        }
        

        function saveData() {
            var emailvendor = vm.vendor.email;
            vm.cek = 0;

            if (vm.vendor.country === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.COUNTRY');
                return;
            }

            if (vm.stateList.length !== 0) {
                if (vm.vendor.state === undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.STATE');
                    return;
                }
            }

            if (vm.vendor.country === 360) {
                if (vm.vendor.city === '' || vm.vendor.city == undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.CITY');
                    return;
                }
                else if (vm.vendor.district === '' || vm.vendor.city == undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.DISTRICT');
                    return;
                }
                else if (vm.vendor.business === undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.BUSINESS');
                    return;
                }
            }



            //cektenderJasa
            if (vm.isTenderJasa == true && vm.isOpenTender == true) {
                if (vm.tenderInterestDoc.length == 0 || vm.tenderInterestDoc == null) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'Dokumen pendaftaran tender harus diisi');
                    return;
                }
                else {
                    var tahapan = '';
                    var tenderStepDataid = 0;
                    for (var i = 0; i <= vm.dataTender.TenderStepDatas.length - 1; i++) {
                        tahapan = vm.dataTender.TenderStepDatas[i].step.FormTypeName;
                        if (tahapan.includes("Pengumuman")) {
                            tenderStepDataid = vm.dataTender.TenderStepDatas[i].ID;
                            i = vm.dataTender.TenderStepDatas.length - 1;
                        }
                    }
                    if (tenderStepDataid != 0) {
                        vm.tenderInterest.TenderStepDataID = tenderStepDataid;
                        vm.tenderInterest.TenderID = vm.dataTender.ID;
                        vm.tenderInterest.TenderInterestVendorDoc = vm.tenderInterestDoc;
                        vm.vendor.TenderInterestVendor = vm.tenderInterest;
                    }
                }
            }

            //if (vm.vendor.TenderCodeGenerate != null) {
            //    checkTenderByRegist();
            //}
            if (vm.vendor.name === null) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.NAME');
                return;
            }
            //else if (vm.vendor.founded === null) {
            //    vm.cek = 1;
            //    UIControlService.msg_growl('error', 'ERRORS.FOUNDED');
            //    return;
            //}
            if (vm.vendor.npwp === null || vm.vendor.npwp == '') {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.NPWP');
                return;
            }
            if (vm.npwpDoc === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.NPWPURL');
                return;
            }
            if (!(validateFileTypeNPWP(vm.npwpDoc, vm.npwpUploadConfigs))) {
                return;
            }
            if (vm.vendor.country === 360) {
                if (vm.vendor.legal === null) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.LEGAL');
                    return;
                } else if (vm.vendor.siup === null || vm.vendor.siup === "") {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.LEGALNo');
                    return;
                } else if (vm.sktDoc === undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.SIUPURL');
                    return;
                }
            }

            //if (vm.currencies.length === 0) {
            //	vm.cek = 1;
            //	UIControlService.msg_growl('error', 'ERRORS.CURRENCY');
            //	return;
            //}

            if (vm.vendor.email == undefined || vm.vendor.email == '') {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.EMAIL');
                return;
            }
            if (vm.vendor.username === undefined || vm.vendor.username == '') {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.USERNAME_EMPTY');
                return;
            }
            if (vm.vendor.username.length < 7) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.USERNAME_MIN_LENGTH');
                return;
            }
            if (vm.vendor.username.length > 10) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.USERNAME_MAX_LENGTH');
                return;
            }

            var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
            var username = vm.vendor.username.split("");
            var arrayNumber = [];
            var arrayUnique = [];

            for (var i = 0; i < username.length; i++) {
                if (!isNaN(username[i])) {
                    arrayNumber.push(username[i]);
                }
                if (format.test(username[i])) {
                    arrayUnique.push(username[i]);
                }
            }

            if (arrayNumber.length > 4) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.USERNAME_MAX_LENGTH_NUMBER');
                return;
            }

            if (arrayUnique.length > 2) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.USERNAME_MAX_LENGTH_UNIQUE_CHAR');
                return;
            }
            if (vm.vendor.password === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.PASSWORD');
                return;
            }

            if (vm.vendor.confirmPassword === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.CONFIRMPASSWORD');
                return;
            }
            if (vm.vendor.password != vm.vendor.confirmPassword) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.CONFIRM_PASSWORD');
                return;
            }
            if (vm.vendor.country == 360) {
                if (vm.vendor.AreaCode == undefined || vm.vendor.AreaCode == '') {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.AREA_CODE');
                    return;
                }
            }
            if (vm.phoneCode === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.PHONECODE');
                return;
            }
            if (vm.vendor.phone === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.PHONE');
                return;
            }
            if (vm.contacts.length === 0) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.CONTACT');
                return;
            }
            var EmailBoleh = true;
            for (var i = 0; i < vm.contacts.length; i++) {
                if (vm.contacts[i].email == vm.vendor.email) {
                    EmailBoleh = false;
                }
            }
            if (!EmailBoleh) {
                UIControlService.msg_growl('error', 'ERRORS.VENDOR_EMAIL_EXIST_REVERSE');
                return;
            }
            if (vm.addresses.length === 0) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.ADDRESS');
                return;
            }
            var boleh = false;
            for (var i = 0; i < vm.addresses.length; i++) {
                if (vm.addresses[i].type.Value == 'VENDOR_OFFICE_TYPE_MAIN') {
                    boleh = true;
                }
            }
            if (!boleh) {
                vm.cek = 1;
                UIControlService.msg_growl('error', 'ERRORS.MAIN_OFFICE');
                return;
            }
            if (vm.vendor.country === 360) {
                if (vm.stocks.length === 0) {
                    vm.cek = 1;
                    UIControlService.msg_growl('error', 'ERRORS.STOCK');
                    return;
                }
            }
            UIControlService.loadLoading();
            checkNamaPerusahaan().then(function (res) {
                if (res) {
                    checkUsername().then(function (res) {
                        if (res) {
                            checkEmail(true).then(function (res) {
                                if (res) {
                                    UIControlService.unloadLoading();

                                    if (vm.cek === 0) {
                                        vm.countSize = 0;
                                        vm.vendor.addresses = vm.addresses;
                                        vm.vendor.contacts = vm.contacts;
                                        vm.vendor.stocks = vm.stocks;
                                        vm.vendor.currencies = vm.currencies;

                                        var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^\&*\)\(+=._-]).{6,20}$/;
                                        if (!vm.vendor.password.match(passw)) {
                                            UIControlService.msg_growl('error', 'ERRORS.MATCHPassword');
                                            return;
                                        }
                                        else if (vm.vendor.country === 360) {
                                            if (vm.vendor.business !== null) {
                                                if (vm.vendor.business.Name === "PT") {
                                                    if (vm.stocks.length === 1) {
                                                        UIControlService.msg_growl('error', 'ERRORS.STOCKHOLDER');
                                                        return;
                                                    }
                                                }
                                            }
                                            if (vm.stocks.length != 0) {
                                                if (vm.stocks[0].unit.Name === "STOCK_UNIT_PERCENTAGE") {
                                                    for (var i = 0; i < vm.stocks.length; i++) {
                                                        vm.countSize += +vm.stocks[i].quantity;
                                                        if (i === (vm.stocks.length - 1)) {
                                                            if (vm.countSize.toFixed(4) < 100) {
                                                                UIControlService.msg_growl('error', 'FORM.MIN_PERSEN');
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                UIControlService.msg_growl('error', 'ERRORS.STOCKHOLDER_TABLE_FILL');
                                                return;
                                            }
                                        }
                                        uploadNpwp(uploadSiup);
                                    }
                                } else {
                                    UIControlService.unloadLoading();
                                    return;
                                }

                            })
                        } else {
                            UIControlService.msg_growl('error', 'ERRORS.USERNAME_EXIST');
                            return;
                        }
                    })
                    
                } else {
                    UIControlService.msg_growl('error', 'ERRORS.NAME_EXIST');
                    return;
                }
            })




        }

        function register() {
            if (validCaptcha()) {
                //saveData();
                checkTenderByRegist();
            }
        }



        function cekOwnerID() {
            BlacklistService.cek({
                Status: vm.stock.owner.id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 && reply.data.length > 0) {
                    vm.Tmpblacklist = true;
                }
                else {
                    vm.Tmpblacklist = false;

                }
            }, function (err) {

                UIControlService.msg_growl("error", "ERRORS.API");
                UIControlService.unloadLoading();
            });
        }

        vm.checkTelp = checkTelp;
        function checkTelp(flag) {
            if (flag == true) {
                if (vm.vendor.phone.length > 20) {
                    UIControlService.msg_growl("error", "ERRORS.MAKS_TELP");
                    return;
                }
            } else {
                if (vm.contactPers.length > 20) {
                    vm.contactPers = "";
                    UIControlService.msg_growl("error", "ERRORS.MAKS_TELP");
                    return;
                }
            }
        }


        vm.loadVendor = loadVendor();
        function loadVendor(VendorID) {
            VendorRegistrationService.selectVendor({
                VendorID: VendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.contactVendor = reply.data;
                    vm.dataVendor = vm.contactVendor;
                    vm.contacts = [];
                    vm.addresses = [];
                    loadStock(VendorID);
                    loadLicense(VendorID);
                    for (var i = 0; i < vm.contactVendor.length; i++) {
                        if (vm.contactVendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {

                            vm.selectedState1 = vm.contactVendor[i].Contact.Address.State;
                            if (vm.contactVendor[i].Contact.Address.State.Country.Code === "IDN") {
                                vm.selectedCity1 = vm.contactVendor[i].Contact.Address.City;
                                vm.selectedDistrict1 = vm.contactVendor[i].Contact.Address.Distric;
                            }
                            loadStates(vm.contactVendor[i].Contact.Address.State.Country);
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendor1 = loadVendor1();
        function loadVendor1(data) {
            if (data != undefined) {
                localStorage.removeItem('dataVendor');
                vm.contactVendor = [];
                vm.addresses = [];
                loadStock(data[0].VendorID);
                loadLicense(data[0].VendorID);
                vm.contactVendor = data;
                for (var i = 0; i < vm.contactVendor.length; i++) {
                    if ((vm.contactVendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contactVendor[i].IsPrimary == null) || (vm.contactVendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contactVendor[i].IsPrimary == null)) {
                        vm.addressInfo = vm.contactVendor[i].Contact.Address.AddressInfo;
                        vm.address1 = vm.contactVendor[i].Contact.Address.AddressInfo;
                        vm.postalcode = vm.contactVendor[i].Contact.Address.PostalCode;
                        vm.info = "";
                        if (vm.contactVendor[i].Contact.Address.City) {
                            vm.info += vm.contactVendor[i].Contact.Address.City.Name + ",";
                        }
                        vm.info += vm.contactVendor[i].Contact.Address.State.Name + "," + vm.contactVendor[i].Contact.Address.State.Country.Name;


                        var address = {
                            ContactID: vm.contactVendor[i].Contact.ContactID,
                            type: vm.contactVendor[i].VendorContactType,
                            city: vm.contactVendor[i].Contact.Address.City,
                            state: vm.contactVendor[i].Contact.Address.State,
                            country: vm.contactVendor[i].Contact.Address.State.Country,
                            district: vm.contactVendor[i].Contact.Address.District,
                            region: vm.contactVendor[i].Contact.Address.State.Country.Continent,
                            detail: vm.address1,
                            PostalCode: vm.postalcode,
                            info: vm.info
                        }
                        vm.addresses.push(address);

                    }
                    else if (vm.contactVendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {
                        var Phone = vm.contactVendor[i].Contact.Phone;

                        var data = {
                            VendorContactType: vm.contactVendor[i].VendorContactType,
                            ContactID: vm.contactVendor[i].Contact.ContactID,
                            name: vm.contactVendor[i].Contact.Name,
                            phone: Phone,
                            email: vm.contactVendor[i].Contact.Email
                        }
                        vm.contacts.push(data);


                    }
                    else if (vm.contactVendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                        vm.vendor.ContactID = vm.contactVendor[i].Contact.ContactID;
                        vm.Npwp = vm.contactVendor[i].Vendor.Npwp;
                        vm.vendor.npwp = vm.contactVendor[i].Vendor.Npwp;
                        vm.Siup = vm.contactVendor[i].Vendor.LegalNumber;
                        vm.NpwpUrl = vm.contactVendor[i].Vendor.NpwpUrl;
                        vm.siupUrl = vm.contactVendor[i].Vendor.LegalDocument;
                        vm.npwpDoc = vm.contactVendor[i].Vendor.NpwpUrl;
                        vm.sktDoc = vm.contactVendor[i].Vendor.LegalDocument;
                        vm.vendor.VendorID = vm.contactVendor[i].Vendor.VendorID;
                        vm.vendor.tendercodetemp = vm.contactVendor[i].Vendor.TenderCodeTemp;
                        vm.vendor.email = vm.contactVendor[i].Contact.Email;
                        vm.vendor.username = vm.contactVendor[i].Vendor.user.Username;
                        vm.vendor.website = vm.contactVendor[i].Contact.Website;
                        vm.phone = vm.contactVendor[i].Contact.Phone.split(' ');
                        vm.vendor.phone = vm.phone[1];
                        vm.phone = vm.phone[0].split(')');
                        vm.phone = vm.phone[0].split('(');
                        loadPhoneCodes(vm.phone[1]);
                        vm.vendor.fax = vm.contactVendor[i].Contact.Fax;
                        loadBusiness(vm.contactVendor[i].Vendor.BusinessID);
                        vm.vendor.name = vm.contactVendor[i].Vendor.VendorName;
                        vm.vendor.founded = new Date(Date.parse(vm.contactVendor[i].Vendor.FoundedDate));
                        vm.vendor.siup = vm.contactVendor[i].Vendor.LegalNumber;
                        loadCurrenciesData(vm.contactVendor[i].Vendor.VendorID);
                    }
                }
            }

        }



        vm.loadCurrenciesData = loadCurrenciesData;
        function loadCurrenciesData(vendorID) {

            vm.currencies = [];
            VendorRegistrationService.selectVendorCurrencies({
                VendorID: vendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].MstCurrency.Symbol == "IDR") {
                            data[i].MstCurrency.IsDelete = true;
                        }
                        data[i].MstCurrency.Id = data[i].ID;
                        vm.currencies.push(data[i].MstCurrency);
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadStock = loadStock;
        function loadStock(vendorID) {
            vm.stocks = [];
            VendorRegistrationService.selectVendorStock({
                VendorID: vendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    for (var i = 0; i < data.length; i++) {
                        var stock = {
                            StockID: data[i].StockID,
                            quantity: data[i].Quantity,
                            unit: data[i].Unit,
                            type: data[i].SysReference,
                            owner: {
                                name: data[i].OwnerName,
                                dob: data[i].OwnerDOB,
                                id: data[i].OwnerID,
                                idDoc: data[i].OwnerIDUrl
                            },
                            Position: data[i].Position,
                            CurrencySymbol: data[i].CurrencySymbol
                        };
                        vm.stocks.push(stock);
                    }
                    //console.info(vm.stocks);
                    loadStockUnits(data[0]);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadLicense = loadLicense;
        function loadLicense(vendorID) {
            vm.legals = [];
            VendorRegistrationService.selectVendorLegal({
                VendorID: vendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    loadLegal(data);
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        function sendEmail() {
            VendorRegistrationService.sendMail(vm.vendorToEmail, function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    UIControlService.msg_growl("notice", "Email Sent!");
                } else {
                    UIControlService.handleRequestError(response.data);
                }

            }, function (response) {
                UIControlService.handleRequestError(response.data);
                UIControlService.unloadLoading();
            });
        }

        vm.forceLower = forceLower;
        function forceLower() {
            vm.vendor.username = vm.vendor.username.toLowerCase();
        }

        function loadMstAssosiasi() {
            VendorRegistrationService.getMstAssosiasi(vm.vendorToEmail, function (response) {
                if (response.status == 200) {
                    var data = response.data;
                    vm.listAssosiasi = data;
                    //console.log(data)
                } else {
                    //UIControlService.handleRequestError(response.data);
                }

            }, function (response) {
                $.growl.error({ message: "Gagal mendapatkan data Master Departemen" });
                UIControlService.unloadLoading();
            });
        }

    }
})();