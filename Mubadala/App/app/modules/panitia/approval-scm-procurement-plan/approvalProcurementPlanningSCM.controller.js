﻿(function () {
    'use strict';

    angular.module("app")
            .controller("approvalProcurementPlanningSCMController", ctrl);

    ctrl.$inject = ['$timeout', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', '$rootScope', 'ApprovalSCMProcurementPlanService', '$stateParams', '$state', '$q'];
    /* @ngInject */
    function ctrl($timeout, $http, $uibModal, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, $rootScope, ApprovalSCMProcurementPlanService, $stateParams, $state, $q) {
        var vm = this;

        vm.init = init;
        vm.searchBy = '';
        vm.maxSize = 10;
        //vm.vendorID = $stateParams.vendorID;
        vm.statusName = 'ALL';
        vm.currentPage = 1;
        vm.totalItems = 0;
        vm.keyword = '';
        vm.startDate = new Date();
        vm.endDate = new Date();
        vm.UIControl = UIControlService;
        vm.isCalendarOpened = [false, false];
        vm.materialService = "";

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        function init() {
            $translatePartialLoader.addPart('approval-procurement-plan');
            UIControlService.loadLoading("MESSAGE.LOADING");
            UIControlService.unloadLoading();
            jLoad(1)
            getDepartment().then(function () {
                jLoad(1)
            });
        }

        vm.resetAll = resetAll;
        function resetAll() {
            vm.keyword = '';
            vm.startDate = new Date();
            vm.endDate = new Date();
        }

        vm.entityData = [];
        function getDepartment() {
            var defer = $q.defer();
            ApprovalSCMProcurementPlanService.selectEntity({

            }, function (reply) {
                if (reply.status == 200) {
                    console.log(reply)
                    vm.entityData = reply.data.List;
                    defer.resolve();
                } else {
                    defer.reject();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                defer.reject();
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoading();
            })
            return defer.promise;
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            ApprovalSCMProcurementPlanService.SelectApproval({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.searchBy,
                Keyword2: vm.keyword,
                Keyword3: vm.statusName,
                Date1: vm.startDate,
                Date2: vm.endDate,
                IntParam1: vm.materialService == "" ? null : vm.materialService
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.approvalData = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoading();
            });
        }

        vm.detailProc = detailProc;
        function detailProc(data) {
            var data = {
                data: data
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-scm-procurement-plan/form-detail-procurement-scm-modal.html',
                controller: 'formDetailProcurementSCMModalController',
                controllerAs: 'formDetailProcurementSCMModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        vm.approvedOrNot = approvedOrNot;
        function approvedOrNot(dataApproval, stats) {
            //var swal = {
            //    titleNotification: $filter('translate')('SWAL.NOTIFICATION'),
            //    textNotActivated: $filter('translate')('SWAL.NOTACTIVATED'),
            //    textCivd: $filter('translate')('SWAL.TEXTCIVD')
            //}

            var data = {
                dataApproval: dataApproval,
                stats: stats
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-scm-procurement-plan/approverProcurementPlanSCM.modal.html',
                controller: 'ApproverProcurementPlanSCMModalController',
                controllerAs: 'ApproverProcurementPlanSCMModalController',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.listApprover = listApprover;
        function listApprover(data) {

            var data = {
                item: data
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-scm-procurement-plan/detailApprovalProcurementPlanSCM.modal.html',
                controller: 'DetailApprovalProcurementPlanSCMModalController',
                controllerAs: 'detailApprovalProcurementPlanSCMModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

    }
})();