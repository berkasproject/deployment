﻿(function () {
    'use strict';

    angular.module("app").controller("ApproverProcurementPlanSCMModalController", ctrl);

    ctrl.$inject = ['item', '$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', '$uibModal', '$stateParams', '$state', 'GlobalConstantService', '$uibModalInstance', 'ApprovalSCMProcurementPlanService', 'VerifikasiDataService'];
    function ctrl(item, $http, $filter, $translate, $translatePartialLoader, $location, SocketService, UIControlService, $uibModal, $stateParams, $state, GlobalConstantService, $uibModalInstance, ApprovalSCMProcurementPlanService, VerifikasiDataService) {
        //console.info("atur: "+JSON.stringify(item));
        var vm = this;
        //vm.Vendor = item.data;
        vm.stats = item.stats;
        vm.dataApproval = item.dataApproval;
        vm.remark = "";

        vm.init = init;
        function init() {
            //console.info()
            //console.info("vendor" + JSON.stringify(item));
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal('MESSAGE.LOADLOADING');
            ApprovalSCMProcurementPlanService.updateApproval({
                ProcurementPlanID: vm.dataApproval.ProcurementPlanID,
                ID: vm.dataApproval.ID,
                StatusVerif: vm.stats,
                ApproverEmployeeId: vm.dataApproval.ApproverEmployeeId,
                Remarks: vm.remark
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    if (data[0] != "") {
                        var konten = data[0].split('#split');
                        console.log(localStorage.getItem('currLang'))
                        if (localStorage.getItem("currLang").toLowerCase() == 'id') {
                            konten = konten[0];
                        }
                        if (localStorage.getItem("currLang").toLowerCase() == 'en') {
                            konten = konten[1];

                        }
                        if (data[2] != "") {
                            if (vm.stats == 1) {
                                konten = konten.replace('#approvername', data[2]);
                                konten = konten.replace('#description', data[3]);
                            } else {
                                konten = konten.replace('#approvername', data[2])
                                konten = konten.replace('#description', data[3]);
                                konten = konten.replace('#rejectby', data[5]);
                                konten = konten.replace('#rejectdate', UIControlService.getDateNow("-"));
                                konten = konten.replace('#rejectremarks', vm.remark);
                            }
                        }
                        sendMail(konten, [data[1]], data[4]);
                    } else {
                        $uibModalInstance.close();
                    }

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.UPDATE_FAIL");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
            });
        }

        function sendMail(mailContent, emailAddress, subject) {
            var email = {
                subject: subject,
                mailContent: mailContent,
                isHtml: false,
                addresses: emailAddress
            };

            UIControlService.loadLoadingModal("MESSAGE.LOADING_SEND_EMAIL");
            VerifikasiDataService.sendMailActived(email, function (response) {
                UIControlService.unloadLoadingModal();
                if (response.status === 200) {
                    // UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT_VENDOR")
                    UIControlService.msg_growl("notice", "MESSAGE.SUCCESS");
                    $uibModalInstance.close();
                } else {
                    UIControlService.handleRequestError(response.data);
                }
            }, function (response) {
                UIControlService.handleRequestError(response.data);
                UIControlService.unloadLoadingModal();
                //$state.go('daftar_kuesioner');
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();