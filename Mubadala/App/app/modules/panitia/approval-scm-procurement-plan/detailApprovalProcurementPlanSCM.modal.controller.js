﻿(function () {
    'use strict';

    angular.module("app").controller("DetailApprovalProcurementPlanSCMModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', 'ApprovalSCMProcurementPlanService', '$stateParams','$q'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, ApprovalSCMProcurementPlanService, $stateParams,$q) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.ProcurementPlanID = item.item.ProcurementPlanID;
        vm.UIControl = UIControlService;
        vm.currentPage = 1;
        vm.totalItems = 0;

        vm.currentPageNonScm = 1;
        vm.totalItemsNonScm = 0;
        vm.dataApproverNonScm = [];

        function init() {
            console.log('Init')
            getDetailApproverNonScm(1).then(function () {
                jLoad(1);
            })
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            ApprovalSCMProcurementPlanService.selectApproverProcurementPlan({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.searchBy,
                IntParam1: vm.ProcurementPlanID
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApprover = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
            })
        }

        vm.getDetailApproverNonScm = getDetailApproverNonScm;

        function getDetailApproverNonScm(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");

            var defer = $q.defer();

            vm.currentPageNonScm = current;
            var offset = (current * 10) - 10;

            ApprovalSCMProcurementPlanService.selectApproverProcurementPlanNonSCM({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.searchBy,
                IntParam1: vm.ProcurementPlanID
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                    UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApproverNonScm = data.List;
                    vm.totalItemsNonScm = data.Count;
                    defer.resolve(true)
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    UIControlService.unloadLoadingModal();
                    defer.reject();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
                defer.reject();
            })

            return defer.promise;
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();