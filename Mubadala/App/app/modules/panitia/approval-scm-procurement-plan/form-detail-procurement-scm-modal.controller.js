﻿(function () {
    'use strict';

    angular.module("app").controller("formDetailProcurementSCMModalController", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'ApprovalSCMProcurementPlanService', '$q'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, ApprovalSCMProcurementPlanService, $q) {
        var vm = this;
        vm.data = item.data;
        vm.isDept = 0;
        vm.isDataSCM = 0;
        vm.isDataMarket = 0;
        vm.isDataSub = 0;
        vm.isDataCat = 0;
        vm.isDataSourcing = 0;
        vm.newContractDuration = 0;
        vm.newContractIncludeExtensionsDuration = 0;

        vm.procurementPlanID = vm.data.ProcurementPlanID;
        vm.isCalendarOpened = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('procurement-plan');
            UIControlService.loadLoadingModal("Loading...");
            vm.getDataProc().then(function (resp) {
                vm.loadDept();
            });
        }

        vm.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        vm.getDataProc = getDataProc;
        function getDataProc() {
            var defer = $q.defer();
            ApprovalSCMProcurementPlanService.getProcurementPlan(
                {
                    procurementPlanID: vm.procurementPlanID
                }, function (reply) {
                    if (reply.status === 200) {
                        console.log(reply.data)
                        vm.detail = reply.data.List[0];

                        vm.currency = 'Rp';
                        vm.newContractExtensionsDuration = vm.detail.NewContractExtensionOption;
                        vm.deptName = vm.detail.DepartmentName;
                        vm.deptID = vm.detail.DepartmentID;
                        vm.marketSector = vm.detail.MarketSectorName;
                        vm.subSector = vm.detail.SubSectorName;
                        vm.categoryName = vm.detail.CategoryName;
                        vm.techFocalPointName = vm.detail.TechFocalPointName;
                        vm.techFocalPointID = vm.detail.TechFocalPoint;
                        vm.description = vm.detail.Description;
                        vm.localNumber = vm.detail.LocalNumber;
                        vm.scmFocalPoint = vm.detail.SCMFocalPointName;
                        vm.scmFocalPointID = vm.detail.SCMFocalPoint;
                        vm.sourcingActivity = vm.detail.SourcingActivityName;
                        vm.entity = vm.detail.EntityID;
                        vm.existingContractAward = vm.detail.ExistingContractAward;
                        vm.existingContract = vm.detail.ExistingContract;
                        vm.materialService = vm.detail.MaterialOrService;
                        vm.prNumber = vm.detail.PRNumber;
                        vm.CCStartDate = new Date(vm.detail.CurrentContractStartDate);
                        vm.CCEndDate = new Date(vm.detail.CurrentContractEndDate);
                        vm.newContractStartDate = new Date(vm.detail.NewContractStartDate);
                        vm.newContractEndDate = new Date(vm.detail.NewContractEndDate);
                        vm.sourcingDateStart = new Date(vm.detail.SourcingDateStart);
                        vm.cgcApprovalDate = new Date(vm.detail.CGCApprovalDate);
                        vm.bidIssueDate = new Date(vm.detail.BidIssueDate);
                        vm.bidDuedDate = new Date(vm.detail.BidDueDate);
                        vm.bidEvalCompletionDate = new Date(vm.detail.BidEvaluationCompletionDate);
                        vm.negotiationDate = new Date(vm.detail.NegotiationDate);
                        vm.awardRecomDate = new Date(vm.detail.AwardRecommendation);
                        vm.excomBoardApproval = new Date(vm.detail.ExcomBoardApproval);
                        vm.consessPartApproval = new Date(vm.detail.PartnersApproval);
                        vm.contractExecutionDate = new Date(vm.detail.ContractExecutionDate);
                        vm.comments = vm.detail.Comments;
                        vm.RCValue = vm.detail.RemainingContractValue != null ? vm.detail.RemainingContractValue : "";
                        vm.ECValue = vm.detail.ExistingContractValue != null ? vm.detail.ExistingContractValue : "";
                        vm.ECNumber = vm.detail.ExistingContractNumber != null ? vm.detail.ExistingContractNumber : "";
                        if (vm.ECValue && vm.RCValue) {
                            vm.RCValueP = (Number(vm.RCValue) / Number(vm.ECValue)) * 100;
                        }
                        defer.resolve(true);

                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                        UIControlService.unloadLoading();
                        defer.reject(true);
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                    defer.reject
                });
            return defer.promise;
        }

        vm.ubahDept = ubahDept;
        function ubahDept($item, $model, $label) {
            if ($item) {
                vm.deptID = $item.DepartmentID;
                vm.deptName = $item.DepartmentName;
                vm.loadTechFocalPoint();
            } else {
                vm.isDept = 0;
            }
        }
        vm.ubahManualDept = ubahManualDept;
        function ubahManualDept(deptname) {
            if (deptname == "") {
                vm.isDept = 0;
                vm.techFocalPointName = "";
            }
            vm.datadept.forEach(function (item) {
                if (deptname != item.DepartmentName) {
                    vm.isDept = 0;
                    vm.techFocalPointName = "";
                }
            })
        }

        vm.ubahTech = ubahTech;
        function ubahTech($item, $model, $label) {
            if ($item) {
                vm.techFocalPointName = $item.FullName;
                vm.techFocalPointID = $item.employee_id;
            }
        }

        vm.ubahSCM = ubahSCM;
        function ubahSCM($item, $model, $label) {
            if ($item) {
                vm.scmFocalPoint = $item.FullName;
                vm.scmFocalPointID = $item.employee_id;
            }
        }

        vm.ubahMarket = ubahMarket;
        function ubahMarket($item, $model, $label) {
            if ($item) {
                vm.marketSector = $item.CategoryName;
                vm.marketSectorID = $item.ID;
            }
        }

        vm.ubahSub = ubahSub;
        function ubahSub($item, $model, $label) {
            if ($item) {
                vm.subSector = $item.CategoryName;
                vm.subSectorID = $item.ID;
            }
        }

        vm.ubahCategory = ubahCategory;
        function ubahCategory($item, $model, $label) {
            if ($item) {
                vm.categoryName = $item.CategoryName;
                vm.categoryID = $item.ID;
            }
        }

        vm.ubahSourcing = ubahSourcing;
        function ubahSourcing($item, $model, $label) {
            if ($item) {
                vm.sourcingActivity = $item.MethodName;
                vm.sourcingActivityID = $item.MethodID;
            }
        }

        vm.loadDept = loadDept;
        function loadDept() {
            ApprovalSCMProcurementPlanService.getDept(function (reply) {
                if (reply.status === 200) {
                    vm.datadept = reply.data;
                    if (vm.datadept) {
                        loadEntity();
                    }

                    vm.datadept.forEach(function (item) {
                        if (vm.deptName == item.DepartmentName) {
                            console.info("Masuk");
                            vm.loadTechFocalPoint();
                        }
                    });
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadTechFocalPoint = loadTechFocalPoint;
        function loadTechFocalPoint() {
            ApprovalSCMProcurementPlanService.getTFP({ Keyword: vm.deptName }, function (reply) {
                if (reply.status === 200) {
                    vm.dataTFP = reply.data.List;
                    if (vm.dataTFP.length > 0) {
                        vm.isDept = 1;
                    } else {
                        vm.isDept = 0;
                        vm.techFocalPointName = "";
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.ubahVendor = ubahVendor;
        function ubahVendor(data) {
            vm.vendorID = data.VendorID;
        }

        vm.ubahJumlahSaham = ubahJumlahSaham;
        function ubahJumlahSaham(jumlah) {
            if (jumlah < 0 || jumlah > 100) {
                vm.jumlahSaham = 0;
            }
        }

        vm.loadEntity = loadEntity;
        function loadEntity() {
            ApprovalSCMProcurementPlanService.getEntity(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataEntity = reply.data;
                    if (vm.dataEntity) {
                        loadSubSector();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSubSector = loadSubSector;
        function loadSubSector() {
            ApprovalSCMProcurementPlanService.getSubSector(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSubSec = reply.data;
                    if (vm.dataSubSec.length > 0) {
                        vm.isDataSub = 1;
                    }
                    if (vm.dataSubSec) {
                        loadProcPlanStatus();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadProcPlanStatus = loadProcPlanStatus;
        function loadProcPlanStatus() {
            ApprovalSCMProcurementPlanService.getProcPlanStatus(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataProcPlanStatus = reply.data;
                    vm.ProcPlanStatusData = [];
                    vm.dataProcPlanStatus.forEach(function (item) {
                        if (item.Value == "Plan") {
                            vm.status = item.RefID;
                            var param = {
                                RefID: item.RefID,
                                Type: item.Type,
                                Value: item.Value
                            }
                            vm.ProcPlanStatusData.push(param);
                        }
                    });
                    if (vm.dataProcPlanStatus) {
                        loadMarketSector();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadMarketSector = loadMarketSector;
        function loadMarketSector() {
            ApprovalSCMProcurementPlanService.getMarketSector(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataMarketSec = reply.data;
                    if (vm.dataMarketSec.length > 0) {
                        vm.isDataMarket = 1;
                    }
                    if (vm.dataMarketSec) {
                        loadCategory();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadCategory = loadCategory;
        function loadCategory() {
            ApprovalSCMProcurementPlanService.getCategory(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataCategory = reply.data;
                    if (vm.dataCategory.length > 0) {
                        vm.isDataCat = 1;
                    }
                    if (vm.dataCategory) {
                        loadExistingContract();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadExistingContract = loadExistingContract;
        function loadExistingContract() {
            ApprovalSCMProcurementPlanService.getExistingContract(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataExistingContract = reply.data;
                    if (vm.dataExistingContract) {
                        loadExistingContractAward();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadExistingContractAward = loadExistingContractAward;
        function loadExistingContractAward() {
            ApprovalSCMProcurementPlanService.getExistingContractAward(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataExistingContractAward = reply.data;
                    if (vm.dataExistingContractAward) {
                        loadSCMFocalPoint();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSCMFocalPoint = loadSCMFocalPoint;
        function loadSCMFocalPoint() {
            ApprovalSCMProcurementPlanService.getSCMFocalPoint(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSCMFocalPoint = reply.data.List;
                    if (vm.dataSCMFocalPoint.length > 0) {
                        vm.isDataSCM = 1;
                    }
                    if (vm.dataSCMFocalPoint) {
                        loadSourcingActivity();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSourcingActivity = loadSourcingActivity;
        function loadSourcingActivity() {
            ApprovalSCMProcurementPlanService.getSourcingActivity(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSourcingActivity = reply.data;
                    if (vm.dataSourcingActivity.length > 0) {
                        vm.isDataSourcing = 1;
                    }
                    if (vm.dataSourcingActivity) {
                        loadContractType();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadContractType = loadContractType;
        function loadContractType() {
            ApprovalSCMProcurementPlanService.getContractType(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataContractType = reply.data;
                    if (vm.dataContractType) {
                        loadMaterialService();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadMaterialService = loadMaterialService;
        function loadMaterialService() {
            ApprovalSCMProcurementPlanService.getMaterialService(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataMaterialService = reply.data;
                    if (vm.dataMaterialService) {
                        UIControlService.unloadLoadingModal();
                    }

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.UbahNewStartDate = UbahNewStartDate;
        function UbahNewStartDate(date) {

            if (date && vm.newContractEndDate) {
                if (date > vm.newContractEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWSTARTTOEND");
                    vm.newContractStartDate = "";
                    return;
                }
                var day = getDays(vm.newContractEndDate, date);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.UbahNewEndDate = UbahNewEndDate;
        function UbahNewEndDate(date) {
            if (date && vm.newContractStartDate) {
                if (date < vm.newContractStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWENDTOSTART");
                    vm.newContractEndDate = "";
                    return;
                }
                var day = getDays(date, vm.newContractStartDate);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.ubahCCStart = ubahCCStart;
        function ubahCCStart(date) {
            if (date && vm.CCEndDate) {
                if (date > vm.CCEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRSTARTTOEND");
                    vm.CCStartDate = "";
                    return;
                }
            }
        }

        vm.ubahCCEnd = ubahCCEnd;
        function ubahCCEnd(date) {
            if (date && vm.CCStartDate) {
                if (date < vm.CCStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRENDTOSTART");
                    vm.CCEndDate = "";
                    return;
                }
            }
        }

        function getDays(endDt, startDt) {

            var end = endDt;
            var start = startDt;

            var ONE_DAY = 1000 * 60 * 60 * 24;
            var differenceMs = Math.abs(end - start);
            return Math.round(differenceMs / ONE_DAY);

        }

        vm.ubahNewContractExt = ubahNewContractExt;
        function ubahNewContractExt(number) {
            vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(number);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();