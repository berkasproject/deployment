﻿(function () {
	'use strict';

	angular.module("app").controller("AturAssessmentCtrl", ctrl);

	ctrl.$inject = ['PPPrequalService', 'UIControlService', 'UploaderService', '$uibModal', '$stateParams', 'item', '$uibModalInstance', ];

	function ctrl(PPPrequalService, UIControlService, UploaderService, $uibModal, $stateParams, item, $uibModalInstance) {
		//console.info("atur: "+JSON.stringify(item));
		var vm = this;
		vm.PrequalSetupID = item.PrequalSetupID;
		vm.PrequalStepID = item.PrequalStepID;
		vm.data = item.data;
		vm.isCalendarOpened = [false, false, false, false];
		vm.dateNow = new Date();
		vm.timezone = vm.dateNow.getTimezoneOffset();
		vm.timezoneClient = vm.timezone / 60;

	    //angular detectBrowser
		vm.isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
		vm.isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
		vm.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0; // At least Safari 3+: "[object HTMLElementConstructor]"
		vm.isChrome = !!window.chrome && !vm.isOpera; // Chrome 1+
		vm.isIE = /*@cc_on!@*/ false || !!document.documentMode; // At least IE6

		vm.init = init;
		function init() {
			prequalAssessmentData();
		}


		vm.prequalAssessmentData = prequalAssessmentData;
		function prequalAssessmentData() {
		    PPPrequalService.prequalAssessmentData({
		        PrequalVendorEntryStepID: vm.data.PrequalSetupStepID,
                VendorPrequalID:vm.data.VendorPrequalID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.assessmentdata = reply.data;
		            //console.info("assessmentdt:" + JSON.stringify(vm.assessmentdata));
		            if (vm.assessmentdata == null) {
		                vm.AssessmentID = 0;
		            }
		            else {
		                vm.AssessmentID = vm.assessmentdata.ID;
		                vm.AssessmentStartDate = new Date(Date.parse(vm.assessmentdata.AssessmentStartDate));
		                vm.AssessmentEndDate = new Date(Date.parse(vm.assessmentdata.AssessmentEndDate));
		                vm.offset = vm.AssessmentStartDate.getTimezoneOffset();
		                if (vm.isChrome === true) {
		                    var objappVersion = navigator.appVersion; var objAgent = navigator.userAgent; var objbrowserName = navigator.appName; var objfullVersion = '' + parseFloat(navigator.appVersion); var objBrMajorVersion = parseInt(navigator.appVersion, 10); var objOffsetName, objOffsetVersion, ix;
		                    if ((objOffsetVersion = objAgent.indexOf("Chrome")) != -1) { objbrowserName = "Chrome"; objfullVersion = objAgent.substring(objOffsetVersion + 7); }
		                    if ((ix = objfullVersion.indexOf(";")) != -1) objfullVersion = objfullVersion.substring(0, ix);
		                    if ((ix = objfullVersion.indexOf(" ")) != -1) objfullVersion = objfullVersion.substring(0, ix); objBrMajorVersion = parseInt('' + objfullVersion, 10);
		                    if (isNaN(objBrMajorVersion)) { objfullVersion = '' + parseFloat(navigator.appVersion); objBrMajorVersion = parseInt(navigator.appVersion, 10); };
		                    if (objBrMajorVersion >= 58) {
		                        vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() + 0));
		                        vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() + 0));
		                    }
		                    else if (objBrMajorVersion <= 57) {
		                        vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() + vm.timezoneClient));
		                        vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() + vm.timezoneClient));
		                    }
		                }
		                else {
		                    vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() - 0));
		                    vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() - 0));
		                }
		            }
		            //console.info("id" + vm.AssessmentID);
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		vm.cekAssessmentDate = cekAssessmentDate;
		function cekAssessmentDate() {
		    if (UIControlService.getStrDate(vm.AssessmentStartDate) <= UIControlService.getStrDate(vm.data.PrequalSetupStep.EndDate)) {
		        if (UIControlService.getStrDate(vm.AssessmentStartDate) < UIControlService.getStrDate(vm.data.PrequalSetupStep.EndDate)){
		            UIControlService.msg_growl("warning", "MESSAGE.ERR_STARTDATE");
		            vm.AssessmentStartDate = null;
		            return;
		        }
		    }
		    if (UIControlService.getStrDate(vm.AssessmentEndDate) < UIControlService.getStrDate(vm.AssessmentStartDate)) {
				UIControlService.msg_growl("warning", "MESSAGE.ERR_ENDDATE");
				vm.AssessmentEndDate = null;
				return;
			}
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
		vm.save = save;
		function save() {
		    if (vm.AssessmentStartDate == null || vm.AssessmentEndDate == null) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_NODATA");
		        return;
		    }
		    else {
		        vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() - vm.timezoneClient));
		        vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() - vm.timezoneClient));
		        if (vm.AssessmentID == 0) {
		            PPPrequalService.insertAssessmentDate({
		                PrequalVendorEntryStepID: vm.data.PrequalSetupStepID,
		                VendorPrequalID: vm.data.VendorPrequalID,
		                AssessmentStartDate: vm.AssessmentStartDate,
		                AssessmentEndDate: vm.AssessmentEndDate
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                //console.info("prq:: " + JSON.stringify(reply));
		                if (reply.status === 200) {
		                    UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_ASSESS");
		                    $uibModalInstance.close();
		                } else {
		                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
		                    return;
		                }
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		                UIControlService.unloadLoadingModal();
		            });
		        }
		        else {
		            PPPrequalService.updateAssessmentDate({
                        ID:vm.AssessmentID,
		                PrequalVendorEntryStepID: vm.data.PrequalSetupStepID,
		                VendorPrequalID: vm.data.VendorPrequalID,
		                AssessmentStartDate: vm.AssessmentStartDate,
		                AssessmentEndDate: vm.AssessmentEndDate
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                //console.info("prq:: " + JSON.stringify(reply));
		                if (reply.status === 200) {
		                    UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_ASSESS");
		                    $uibModalInstance.close();
		                } else {
		                    UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
		                    return;
		                }
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		                UIControlService.unloadLoadingModal();
		            });
		        }
		    }
		}
	}
})();