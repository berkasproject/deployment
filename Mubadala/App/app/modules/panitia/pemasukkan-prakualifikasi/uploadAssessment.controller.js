﻿(function () {
	'use strict';

	angular.module("app").controller("UploadAssessmentCtrl", ctrl);

	ctrl.$inject = ['PPPrequalService', 'UIControlService', 'UploaderService', '$uibModal', '$stateParams', 'item', '$uibModalInstance', 'UploadFileConfigService'];

	function ctrl(PPPrequalService, UIControlService, UploaderService, $uibModal, $stateParams, item, $uibModalInstance, UploadFileConfigService) {
		//console.info("atur: "+JSON.stringify(item));
		var vm = this;
		vm.PrequalSetupID = item.PrequalSetupID;
		vm.PrequalStepID = item.PrequalStepID;
		vm.data = item.data;
		vm.isCalendarOpened = [false, false, false, false];
		vm.dateNow = new Date();
		vm.timezone = vm.dateNow.getTimezoneOffset();
		vm.timezoneClient = vm.timezone / 60;
		vm.tglSekarang = UIControlService.getDateNow("");

	    //angular detectBrowser
		vm.isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
		vm.isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
		vm.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0; // At least Safari 3+: "[object HTMLElementConstructor]"
		vm.isChrome = !!window.chrome && !vm.isOpera; // Chrome 1+
		vm.isIE = /*@cc_on!@*/ false || !!document.documentMode; // At least IE6

		vm.init = init;
		function init() {
		    cfgFile();
			prequalAssessmentData();
		}

		function cfgFile() {
		    UploadFileConfigService.getByPageName("PAGE.ADMIN.PREQUAL.ASSESSMENT", function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            vm.idUploadConfigs = [];
		            vm.list = response.data;
		            console.info("cfgfile" + JSON.stringify(vm.list));
		            for (var i = 0; i < vm.list.length; i++) {
		                vm.idUploadConfigs.push(vm.list[i]);
		            }
		            vm.idFileTypes = UIControlService.generateFilterStrings(vm.idUploadConfigs);
		            console.info("idfiletypes" + JSON.stringify(vm.idFileTypes));
		            vm.idFileSize = vm.idUploadConfigs[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		        UIControlService.unloadLoading();
		        return;
		    });
		}


		vm.prequalAssessmentData = prequalAssessmentData;
		function prequalAssessmentData() {
		    PPPrequalService.prequalAssessmentData({
		        PrequalVendorEntryStepID: vm.data.PrequalSetupStepID,
                VendorPrequalID:vm.data.VendorPrequalID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.assessmentdata = reply.data;
		            //console.info("assessmentdt:" + JSON.stringify(vm.assessmentdata));
		            if (vm.assessmentdata == null) {
		                vm.AssessmentID = 0;
		            }
		            else {
		                vm.AssessmentID = vm.assessmentdata.ID;
		                vm.AssessmentStartDate = new Date(Date.parse(vm.assessmentdata.AssessmentStartDate));
		                vm.AssessmentEndDate = new Date(Date.parse(vm.assessmentdata.AssessmentEndDate));
		                vm.offset = vm.AssessmentStartDate.getTimezoneOffset();
		                if (vm.isChrome === true) {
		                    var objappVersion = navigator.appVersion; var objAgent = navigator.userAgent; var objbrowserName = navigator.appName; var objfullVersion = '' + parseFloat(navigator.appVersion); var objBrMajorVersion = parseInt(navigator.appVersion, 10); var objOffsetName, objOffsetVersion, ix;
		                    if ((objOffsetVersion = objAgent.indexOf("Chrome")) != -1) { objbrowserName = "Chrome"; objfullVersion = objAgent.substring(objOffsetVersion + 7); }
		                    if ((ix = objfullVersion.indexOf(";")) != -1) objfullVersion = objfullVersion.substring(0, ix);
		                    if ((ix = objfullVersion.indexOf(" ")) != -1) objfullVersion = objfullVersion.substring(0, ix); objBrMajorVersion = parseInt('' + objfullVersion, 10);
		                    if (isNaN(objBrMajorVersion)) { objfullVersion = '' + parseFloat(navigator.appVersion); objBrMajorVersion = parseInt(navigator.appVersion, 10); };
		                    if (objBrMajorVersion >= 58) {
		                        vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() + 0));
		                        vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() + 0));
		                    }
		                    else if (objBrMajorVersion <= 57) {
		                        vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() + vm.timezoneClient));
		                        vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() + vm.timezoneClient));
		                    }
		                }
		                else {
		                    vm.AssessmentStartDate = new Date(vm.AssessmentStartDate.setHours(vm.AssessmentStartDate.getHours() - 0));
		                    vm.AssessmentEndDate = new Date(vm.AssessmentEndDate.setHours(vm.AssessmentEndDate.getHours() - 0));
		                }
		            }
		            //console.info("id" + vm.AssessmentID);
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.submitfile = submitfile;
		function submitfile() {
		    if (vm.fileUpload == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.NO_FILE");
		        return;
		    }
		    if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		        uploadAssessmentFile(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
		    }
		}

		function uploadAssessmentFile(file, config, filters, dates) {
		    var size = config.Size;

		    var unit = config.SizeUnitName;
		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		    }
		    UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
            function (response) {
                //console.info("upload:" + JSON.stringify(response.data));
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    var fileName = response.data.FileName;
                    vm.pathFile = url;
                    updateAssessmentURL();
                    //console.info("url" + vm.pathFile);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            },
            function (response) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                UIControlService.unloadLoading();
            });
		}

		function updateAssessmentURL() {
		    PPPrequalService.uploadAssessmentFile({
		        ID:vm.AssessmentID,
		        PrequalVendorEntryStepID: vm.data.PrequalSetupStepID,
		        VendorPrequalID: vm.data.VendorPrequalID,
		        AssessmentDocURL: vm.pathFile
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        //console.info("prq:: " + JSON.stringify(reply));
		        if (reply.status === 200) {
		            UIControlService.msg_growl("success", "MESSAGE.SUCC_UPLOAD");
		            $uibModalInstance.close();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		        UIControlService.unloadLoadingModal();
		    });
		    }

	}
})();