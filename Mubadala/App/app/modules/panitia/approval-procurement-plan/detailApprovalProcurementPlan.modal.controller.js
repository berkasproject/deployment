﻿(function () {
    'use strict';

    angular.module("app").controller("DetailApprovalProcurementPlanModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', 'ApprovalProcurementPlanService', '$stateParams','$q'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, ApprovalProcurementPlanService, $stateParams,$q) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.SCMFocalPoint = item.item.SCMFocalPoint;
        vm.ProcurementPlanID = item.item.ProcurementPlanID;
        vm.UIControl = UIControlService;
        vm.currentPage = 1;
        vm.totalItems = 0;
        vm.currentPageSCM = 1;
        vm.totalItemsSCM = 0;
        vm.dataApproverSCM = [];
        vm.dataApprover = [];

        function init() {
            console.log('Init')
            jLoad(1).then(function (reply) {
                if (vm.SCMFocalPoint != 0) {
                    getDataApproverSCM(1);

                } else {
                    UIControlService.unloadLoadingModal();
                }
            })
        }

        vm.getDataApproverSCM = getDataApproverSCM;
        function getDataApproverSCM(current) {
            vm.currentPageSCM = current;
            var offset = (current * 10) - 10;

            ApprovalProcurementPlanService.selectApproverProcurementPlanSCM({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.SCMFocalPoint
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApproverSCM = data.List;
                    vm.totalItemsSCM = data.Count;
                    defer.resolve();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    //UIControlService.unloadLoadingModal();
                    defer.reject()
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
                defer.reject();
            })
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            var defer = $q.defer();
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            ApprovalProcurementPlanService.selectApproverProcurementPlan({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.searchBy,
                IntParam1: vm.ProcurementPlanID
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApprover = data.List;
                    vm.totalItems = data.Count;
                    defer.resolve();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    //UIControlService.unloadLoadingModal();
                    defer.reject()
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
                defer.reject();
            })

            return defer.promise;
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();