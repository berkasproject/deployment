﻿(function () {
	'use strict';

	angular.module("app").controller("templateDokumenCtrl", ctrl);

	ctrl.$inject = ['UIControlService', 'TemplateDokumenService', '$uibModal', '$translatePartialLoader'];

	function ctrl(UIControlService, TemplateDokumenService, $uibModal, $translatePartialLoader) {
		var vm = this;
		vm.maxSize = 10;
		vm.currentPage = 1;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('template-dokumen');
			selectTemplate();
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			selectTemplate();
		}

		vm.editTemplate = editTemplate;
		function editTemplate(id) {
			var item = {
				ID: id
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/template-dokumen/templateDokumenEdit.html',
				controller: 'editTemplateCtrl',
				controllerAs: 'editTemplateCtrl',
				resolve: { item: function () { return item; } }
			});

			modalInstance.result.then(function () {
				selectTemplate();
			});
		}

		function selectTemplate() {
			UIControlService.loadLoading('LOADING.GETTEMPLATES.MESSAGE');
			TemplateDokumenService.selectTemplate({
				Limit: vm.maxSize,
				Offset: (vm.currentPage - 1) * vm.maxSize
			}, function (reply) {
				if (reply.status === 200) {
					vm.templates = reply.data.List;
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GETTEMPLATES.ERROR', "NOTIFICATION.GETTEMPLATES.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GETTEMPLATES.ERROR', "NOTIFICATION.GETTEMPLATES.TITLE");
			});
		}
	}
})();