﻿(function () {
	'use strict';

	angular.module("app").controller("VerifiedProcessCtrl", ctrl);

	ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VerifiedSendService', 'VerifikasiDataService', 'SrtPernyataanService', 'RoleService', 'UIControlService', '$uibModal', '$stateParams', '$state', 'GlobalConstantService','$q','VendorRegistrationService','FileSaver','AuthService'];
	function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, VerifiedSendService, VerifikasiDataService, SrtPernyataanService,
        RoleService, UIControlService, $uibModal, $stateParams, $state, GlobalConstantService, $q, VendorRegistrationService, FileSaver, AuthService) {

	    var vm = this;
	    vm.dataBalance = {
	        ScaleReffS: null,
	        TechnicalReffS: null
	    };
		vm.verified = [];
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.fullSize = 10;
		vm.id = Number($stateParams.id);
		vm.init = init;
		vm.Vendor = {};
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.flag = false;
		vm.flagBC = false;
		vm.flagAG = false;
		vm.keyword = '';
		vm.administrasi = [];
		vm.license = [];
		vm.DocType;
		vm.CertificatePengalaman = [];
		vm.CertificatePendidikan = [];
		vm.CertificateSertifikat = [];
		vm.titleBC = "Download";
		vm.bankDetailVendorCategory = false;
		vm.afiliasiID = 0;
		vm.langID = true;
		vm.flagTab = [];
		vm.totalItemsLicense = 0;
		vm.currentPageLicense = 1;
		vm.fullAccess = false;
		vm.readAccess = false;
		vm.editAccess = false;
		vm.createAccess = false;
		vm.deleteAccess = false;
		vm.labelCurr = "";

		vm.verificationRemark = "";
		vm.isVendorVerified = false;
		if ($stateParams.stats != undefined) {
		    vm.isVendorVerified = true;
		}

		vm.FirstInit = FirstInit;

		function FirstInit() {
		    loadGetDataVendor().then(function () {
		        LoadTab().then(function (response) {
		            init();
		        });
		    });
		    
		}

		function init() {
			$translatePartialLoader.addPart('verifikasi-data');
			//$translatePartialLoader.addPart('pengurus-perusahaan');
			//$translatePartialLoader.addPart('akta-pendirian');
			//$translatePartialLoader.addPart('data-izinusaha');
			//$translatePartialLoader.addPart('data-administrasi');
			//$translatePartialLoader.addPart('surat-pernyataan');
			//$translatePartialLoader.addPart('data-pengalaman');
			//$translatePartialLoader.addPart('bank-detail');
			//$translatePartialLoader.addPart('other-docs');
			//$translatePartialLoader.addPart('vendor-balance');
			//$translatePartialLoader.addPart('data-afiliasi');

			if (localStorage.getItem('currLang').toLowerCase() != 'id') {
			    vm.langID = false;
			}

			getSysAccess();

			UIControlService.loadLoading("MESSAGE.LOADING");

			var prom1 = jLoad(1).then(function (response) {
			    return response
			});
			var prom2 = loadlicense().then(function (response) {
			    UIControlService.loadLoading();

			    return response
			});
			var prom3 = loadAkta().then(function (response) {
			    return response
			});
			var prom4 = loadPengurus().then(function (response) {
			    return response
			});
			var prom5 = loadTenagaAhli(1).then(function (response) {
			    return response
			});
			var prom6 = loadBuilding(1).then(function (response) {
			    return response
			});
			var prom7 = loadEquipmentVehicle(1).then(function (response) {
			    return response
			});
			var prom8 = loadEquipmentTools(1).then(function (response) {
			    return response
			});
		    //var prom9 = loadPengalaman().then(function (response) {
		    //    return response
		    //});
			var prom10 = loadSaham().then(function (response) {
			    return response
			});
			var prom11 = loadDokumen().then(function (response) {
			    return response
			});
			var prom12 = loadNeraca().then(function (response) {
			    return response
			});
			var prom13 = loadCommodity().then(function (response) {
			    return response
			});
			var prom14 = otherdoc().then(function (response) {
			    return response
			});
			var prom15 = loadBankDetail().then(function (response) {
			    return response
			});
			var prom16 = loadUrlStatementLetter().then(function (response) {
			    return response
			});
			var prom17 = loadKuesionerDD().then(function (response) {
			    return response
			});
			var prom18 = loadVendorAfiliasi(1).then(function (response) {
			    return response
			});
			$q.all([prom1, prom2, prom3, prom4, prom5, prom6, prom7, prom8, prom10, prom11, prom12, prom13, prom14, prom15, prom16, prom17, prom18]).then(function (result) {
			    UIControlService.unloadLoading();
			    loadPengalaman()
			})
			
		}

		function LoadTab() {
		    var defer = $q.defer();
		    VerifikasiDataService.getMstCompanyDataVendor({
		        IntParam1: vm.VendorCategoryID,
		        IntParam2: vm.id
		    }, function (reply) {
		        if (reply.status == 200) {
		            var data = reply.data;
		            vm.tabCompanyDataVendor = data;
		            for (var i = 0; i < vm.tabCompanyDataVendor.length; i++) {
		                vm.tabCompanyDataVendor[i].disabled = true;
		                if (i == 0 && vm.VerifiedApprovalStatus == null && vm.VerifiedSendDate != null) {
		                    vm.tabCompanyDataVendor[i].disabled = false;
		                }
		            }
		            console.log(data)
		            defer.resolve(true);
		        }
		    }, function (error) {
		        $.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		        defer.reject()
		    })
		    return defer.promise;
		}

		function loadGetDataVendor() {
		    var defer = $q.defer();
		    VerifikasiDataService.getDataVendor({
		        VendorID: vm.id
		    }, function (reply) {
		        if (reply.status == 200) {
		            var data = reply.data;
		            vm.VendorCategoryID = data.VendorCategoryID;
		            vm.VerifiedApprovalStatus = data.VerifiedApprovalStatus;
		            vm.VerifiedSendDate = data.VerifiedSendDate;
		            defer.resolve(true);
		        }
		    }, function (error) {
		        $.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
                defer.reject()
		    })
		    return defer.promise;
		}


		vm.isSendQuest = false
		vm.sendQuestionnaire = sendQuestionnaire;
		function sendQuestionnaire(vendorID) {

		    bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_RESENDQUEST') + '</h4>', function (res) {
		        if (res) {

		            UIControlService.loadLoading("MESSAGE.LOADING");
		            VerifikasiDataService.sendQuestionnaire({
		                VendorID: vendorID
		            }, function (reply) {
		                if (reply.status === 200) {
		                    vm.isSendQuest = reply.data;
		                    console.info("issend?" + vm.isSendQuest);
		                    if (vm.isSendQuest == true) {
		                        UIControlService.msg_growl("success", "Berhasil mengirim kuesioner");
		                        init();
		                    }
		                    else {
		                        UIControlService.msg_growl("warning", "Kuesioner tidak ditemukan. Silakan membuat kuesioner terlebih dahulu.");
		                    }
		                    UIControlService.unloadLoading();
		                } else {
		                    $.growl.error({ message: "Gagal mengirim kuesioner" });
		                    UIControlService.unloadLoading();
		                }
		            }, function (err) {
		                $.growl.error({ message: "Gagal Akses API >" + err });
		                UIControlService.unloadLoading();
		            });

		            //SocketService.emit("daftarRekanan");
		        }
		    });

		}

		vm.exportDataPerusahaan = exportDataPerusahaan;
		function exportDataPerusahaan(moduleId) {
		    $state.transitionTo('export-data-perusahaan', { moduleId: moduleId,VendorID:vm.id });
		}

		vm.stopPropagation = stopPropagation;
		function stopPropagation(e) {
		    //console.log(angular.element(e.currentTarget));
		    console.log('mantapu')
		    if (angular.element(e.currentTarget).prop('tagName') === 'INPUT') {
		        e.stopPropagation();
		    }
		}

		vm.disabledTab = disabledTab;
		
		function disabledTab(index, data) {

		    if (vm.VerifiedApprovalStatus == null && vm.VerifiedSendDate != null) {
		        for (var i = 0; i < vm.tabCompanyDataVendor.length; i++) {
		            vm.tabCompanyDataVendor[i].disabled = true;
		        }

		        var active = $('#uibTab' + index).attr('class');

		        var activeArray = active.split(' ');

		        var act = $.grep(activeArray, function (n) { return n == 'active-add'; });
		        var act2 = $.grep(activeArray, function (n) { return n == 'active'; });


		        if (act.length == 1 || act2.length == 1) {
		            data.disabled = false;
		        }
		    }

		    
		}

		

		vm.jLoad = jLoad;
		function jLoad(current) {
            var defer = $q.defer();
			vm.addressBranch = '';
			vm.addressMain = '';
			VerifikasiDataService.select({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
				    vm.verified = reply.data;
                    
				    if (vm.verified[0].Vendor.VerificationRemark != null) {
				        vm.verificationRemark = vm.verified[0].Vendor.VerificationRemark;
					}
					console.log("BB");
					console.log(vm.verified)
					for (var i = 0; i < vm.verified.length ; i++) {
						if (vm.verified[i].VendorContactType.Value === "VENDOR_CONTACT_TYPE_COMPANY") {
							vm.cityID = vm.verified[i].Contact.Address.State.Country.CountryID;
							vm.State = vm.verified[i].Contact.Address.State.Country.Code;
							vm.StateName = vm.verified[i].Contact.Address.State.Name;
							vm.CountryName = vm.verified[i].Contact.Address.State.Country.Name;
							vm.City = vm.verified[i].Contact.Address.City.Name;
							if (vm.verified[i].Contact.Address.State.Country.Code === 'IDN')
							{
							    vm.cek = true;
							    if (vm.City != null) {
							        vm.addressMain = vm.City + ", " + vm.StateName + ", " + vm.CountryName;

							    } else {
							        vm.addressMain = vm.StateName + ", " + vm.CountryName;

							    }
							} else {
							    vm.addressMain = vm.StateName + ", " + vm.CountryName;

							}
							vm.TenderName = vm.verified[i].Vendor.TenderName;
							if (vm.verified[i].Contact.Fax !== null) {
							    vm.Fax = vm.verified[i].Contact.Fax;
								vm.ld = vm.verified[i].Contact.Fax.split(' ');
							}
							vm.VendorTypeId = vm.verified[i].Vendor.VendorTypeID;
							vm.SupplierTypeId = vm.verified[i].Vendor.SupplierID;
							console.log("JJ "+vm.VendorTypeId)
							vm.QuestionnaireUrl = vm.verified[i].Vendor.QuestionnaireUrl;
						}
						if (vm.verified[i].VendorContactType.Value === "VENDOR_OFFICE_TYPE_BRANCH") {
							vm.addressBranch = vm.verified[i].Contact.Address.AddressInfo + vm.verified[i].Contact.Address.AddressDetail; 

						}
						if (vm.verified[i].VendorContactType.Value === "VENDOR_OFFICE_TYPE_MAIN") {
						}    
					}
                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject("Gagal Mendapatkan data Perusahaan")
				}
			}, function (err) {
			    //$.growl.error({ message: "Gagal Akses API >" + err });
			    defer.reject("Gagal Mendapatkan data Perusahaan")

				UIControlService.unloadLoading();
			});
			return defer.promise;
		}

		vm.addVerifikasi = addVerifikasi;
		function addVerifikasi(data, emailAddress) {
		    if (data === true) {
                /*
				bootbox.confirm($filter ('translate')('MESSAGE.SURE_VERIF'), function (res) {
					if (res) {
						simpan(data);
						if (localStorage.getItem("currLang") === 'id') {
							sendMail('Kami telah berhasil melakukan verifikasi terhadap data perusahaan anda. Selanjutnya anda bisa mengikuti pengadaan yang kami selenggarakan di aplikasi eprocurement. Terimakasih', emailAddress);
						}
						else if (localStorage.getItem("currLang") === 'en') {
							sendMail('We have successfully verified your company data. Your company can take part in the procurement that we held in the eprocurement application. Thank you', emailAddress);
						}

						SocketService.emit("daftarRekanan");
					}
				});*/
		    } else if (data === false) {

		        var boleh = false;

		        for (var i = 0; i < vm.tabCompanyDataVendor.length; i++) {
		            if (vm.tabCompanyDataVendor[i].ReviewStatus == 4525) {
		                boleh = true;
		            }
		        }

		        if (!boleh) {
		            UIControlService.msg_growl("warning", "MESSAGE.TIDAK_ADA_DITOLAK")
		            return
		        }

				var dataVendor = {
					IsVerified: data,
					emailAddress: emailAddress,
					VendorID: vm.id
				}
				
				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/verifyVendor.html',
					controller: 'VerifyVendorCtrl',
					controllerAs: 'VerifyVendorCtrl',
					resolve: {
						item: function () {
							return dataVendor;
						}
					}
				});
				modalInstance.result.then(function () {
					init();
				});
				// bootbox.confirm($filter('translate')('MESSAGE.SURE_DENY'), function (res) {
				// 	if (res) {
				// 		simpan(data);
				// 		if (localStorage.getItem("currLang") === 'id') {
				// 			sendMail('Kami telah melakukan verifikasi terhadap data perusahaan anda. Kami menemukan beberapa data yang tidak sesuai. Mohon segera dilakukan perbaikan dan dikirim kembali untuk verifikasi. Terimakasih', emailAddress);
				// 		}
				// 		else if (localStorage.getItem("currLang") === 'en') {
				// 			sendMail('We have verified your company data. We found some data that was not appropriate. Please correct the data immediately and send it back for verification. Thank you', emailAddress);
				// 		}
				// 		SocketService.emit("daftarRekanan");
				// 	}
				// });
			}
		}


		vm.verifyVendor = verifyVendor;
		function verifyVendor(isverified, emailAddress, vendorName) {
		    var boleh = true;

		    for (var i = 0; i < vm.tabCompanyDataVendor.length; i++) {
		        if (!vm.tabCompanyDataVendor[i].Checked) {
		            boleh = false;
		        }
		    }

		    if (!boleh) {
		        UIControlService.msg_growl("warning", "MESSAGE.CHECKED_ALL_FIRST")
		        return;
		    }

		    var data = {
		        IsVerified: isverified,
		        emailAddress: emailAddress,
		        VendorID: vm.id,
                VendorName: vendorName
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/verifyVendor.html',
		        controller: 'VerifyVendorCtrl',
		        controllerAs: 'VerifyVendorCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}

		vm.revisi = revisi;
		function revisi(vendorID) {
		    console.log(vm.tabCompanyDataVendor)

		    var boleh = false;

		    for (var i = 0; i < vm.tabCompanyDataVendor.length; i++) {
		        if (vm.tabCompanyDataVendor[i].ReviewStatus == 4525) {
		            boleh = true;
		        }
		    }

		    if (!boleh) {
		        UIControlService.msg_growl("warning", "MESSAGE.TIDAK_ADA_REVISI")
                return
		    }
		   
		    var data = {
		        tabCompanyDataVendor: vm.tabCompanyDataVendor,
		        langID: vm.langID,
		        vendorName: vm.verified[0].Vendor.VendorName
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/RevisiVerifikasi.modal.html',
		        controller: 'RevisiVerifikasiModalController',
		        controllerAs: 'RevisiVerifikasiModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        //init()
		        $state.go('verifikasi-data');

		    });
		    
		}

		function sendMail(mailContent, emailAddress) {
			var email = {
				subject: 'Vendor Verification Notification',
				mailContent: mailContent,
				isHtml: false,
				addresses: [emailAddress]
			};

			UIControlService.loadLoading("MESSAGE.LOADING_SEND_EMAIL");
			VerifikasiDataService.sendMailActived(email, function (response) {
				UIControlService.unloadLoading();
				if (response.status === 200) {
					UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT_VENDOR")
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
				//$state.go('daftar_kuesioner');
			});
		}

		vm.ClickButtonRevisi = ClickButtonRevisi;
		function ClickButtonRevisi(data) {
		    //if (data.ReviewStatus == 4527) {
                
		    //    UIControlService.msg_growl("warning", "Data " + (vm.langID ? data.Locale_Id : data.Locale_En) + " " + $filter('translate')('MESSAGE.TELAH_LENGKAP'))
		    //    return;
		    //}

		    //if (data.ReviewStatus == 4526) {
		    //    UIControlService.msg_growl("warning", "Data " + (vm.langID ? data.Locale_Id : data.Locale_En) + " " + $filter('translate')('MESSAGE.TELAH_DIREVISI'))
		    //    return;
		    //}

		    if (data.ButtonRevisi) {
		        data.ButtonRevisi = false;
		    } else {
		        data.ButtonRevisi = true;
		    }
		}

		vm.simpanRevisiRemark = simpanRevisiRemark;
		function simpanRevisiRemark(data) {

		    if (data.ReviewStatus == 4527) {
		        UIControlService.msg_growl("warning", "Data " + (vm.langID ? data.Locale_Id : data.Locale_En) + " " + $filter('translate')('MESSAGE.TELAH_LENGKAP'))
		        return;
		    }

		    if (data.ReviewStatus == 4526) {
		        UIControlService.msg_growl("warning", "Data " + (vm.langID ? data.Locale_Id : data.Locale_En) + " " + $filter('translate')('MESSAGE.TELAH_DIREVISI'))
		        return;
		    }

		    if (data.Remarks == '' || data.Remarks == null) {
		        UIControlService.msg_growl("warning", "MESSAGE.PLEASE_FILL_REMARKS");
		        return;
		    }

		    UIControlService.loadLoading();

		    VerifikasiDataService.updateRemarksVendorReview({ ReviewID: data.ReviewID, Remarks: data.Remarks }, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status === 200) {
		            UIControlService.msg_growl("notice", "MESSAGE.SUCCESS")
		        } else {
		            UIControlService.handleRequestError(response.data);
		        }
		    }, function (response) {
		        UIControlService.handleRequestError(response.data);
		        UIControlService.unloadLoading();
		        //$state.go('daftar_kuesioner');
		    });
		}

		vm.simpan = simpan;
		function simpan(data) {

			if (data === true) {
				vm.Vendor = {
					VendorID: vm.id,
					Isverified: 1
				}
			} else if (data === false) {
				vm.Vendor = {
					VendorID: vm.id,
					Isverified: 0,
                    VerificationRemark:""
				}
			}
			UIControlService.loadLoading("MESSAGE.LOADING");
			VerifiedSendService.updateVerifikasi(vm.Vendor, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var msg = "";
					if (data === false) msg = $filter ('translate')('Tolak Verifikasi');
					if (data === true) msg = $filter ('translate')('Verifikasi') ;
					UIControlService.msg_growl("success", $filter ('translate')('MESSAGE.SUCC_VERIFY') + msg);
					$state.transitionTo('verifikasi-data');
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_VERIFY");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		vm.loadUrlStatementLetter = loadUrlStatementLetter;
		function loadUrlStatementLetter() {
		    var defer = $q.defer();
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.DocType = 4225;
		    }
		    else {
		        vm.DocType = 4232;
		    }
		    VerifikasiDataService.DocConduct({
		        DocType: vm.DocType,
                VendorId: vm.id
		    }, function (reply) {
		        if (reply.status === 200) {
		            var data = reply.data;
		            if (data == null || data == "") {
		                vm.flagBC = true;
		                vm.titleBC = "File Tidak Tersedia";
		            } else if ( data != null)
		            {
		                var ada = false;
		                for (var i = 0; i < data.length; i++) {
		                    if (data[i].DocumentUrl !== null && data[i].DocumentUrl!=="") {
		                        ada = true;
		                        vm.flagBC = false;
		                        vm.titleBC = "Downloads";
		                        vm.urlBusinessConduct = data[i].DocumentUrl;
		                        i = data.length;
		                    }
		                }
		                if (ada == false) {
		                    vm.flagBC = true;
		                    vm.titleBC = "File Tidak Tersedia";
		                }
                        /*
		                if (data[0].DocumentUrl == "") {
		                    vm.flagBC = true;
		                    vm.titleBC = "File Tidak Tersedia";
		                } else {
		                    vm.titleBC = "Downloads";
		                    vm.flagBC = false;
		                    vm.urlBusinessConduct = data[0].DocumentUrl;
		                }*/
		            }
		            loadAggreement();
		            defer.resolve(true)
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan dokumen" });
		            UIControlService.unloadLoading();
                    defer.reject(false)
		        }
		    }, function (err) {
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
                defer.reject(false)
		    });
		    return defer.promise;
		}

		vm.loadAggreement = loadAggreement;
		function loadAggreement() {
		    VerifikasiDataService.DocConduct({
		        DocType: 4226,
                VendorId: vm.id
		    }, function (reply) {
		        if (reply.status === 200) {
		            var data = reply.data;
		            if (data == null || data == "")
		            {
		                vm.flagAG = true;
		                vm.titleAG = "File Tidak Tersedia";
		            }
		            else if (data != null || data != "")
		            {
		                if (data[0].DocumentUrl == "") {
		                    vm.flagAG = true;
		                    vm.titleAG = "File Tidak Tersedia";
		                } else {
		                    vm.titleAG = "Downloads";
		                    vm.flagAG = false;
		                    vm.urlAggrement = data[0].DocumentUrl;
		                }
		            }
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan dokumen" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

        vm.loadKuesionerDD = loadKuesionerDD;
        function loadKuesionerDD() {
            var defer = $q.defer();
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    VerifikasiDataService.loadDDQuest({
                VendorID: vm.id
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.loadDDQuest = reply.data;
		            console.info("loadDDquest:" + JSON.stringify(vm.loadDDQuest));
                    defer.resolve(true)
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan dokumen" });
		            UIControlService.unloadLoading();
                    defer.reject(false)
		        }
		    }, function (err) {
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
                defer.reject(false)
		    });
		    return defer.promise;
		}

		vm.loadlicense = loadlicense;
		function loadlicense(current) {
		    UIControlService.loadLoading();

		    var defer = $q.defer();
		    vm.currentPageLicense = current;
			var offset = (current * 10) - 10;
			VerifikasiDataService.selectlicensi({
				Status: vm.id,
				Offset: offset,
				Limit: vm.fullSize,
			    IntParam1: vm.VendorCategoryID
			}, function (reply) {
				if (reply.status === 200) {
				    vm.license = reply.data;
				    vm.totalItemsLicense = reply.data.Count;
				    UIControlService.unloadLoading();

                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
			    //$.growl.error({ message: "Gagal Akses API >" + err });
			    defer.reject(false)

				UIControlService.unloadLoading();
			});
			return defer.promise;
		}

		vm.detail = detail;
		function detail(flag, datas) {
			var data = {
				flag: flag,
				item: datas,
				VendorCategoryID: vm.VendorCategoryID,
				city: vm.cityID,
				vendorID: vm.id
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/modallicense.html',
				controller: 'ModalLicenseCtrl',
				controllerAs: 'ModalLicenseCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.jLoad(1);
			});
		}

		vm.loadAkta = loadAkta;
		function loadAkta() {
		    var defer = $q.defer();
			VerifikasiDataService.GetByVendor({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
					vm.Akta = reply.data;
					vm.countPendirian = 0;
					vm.countPerubahan = 0;
					vm.countPengesahan = 0;
					vm.AktaPendirian = [];
					vm.AktaPerubahan = [];
					vm.PengesahanKemenkumham = [];
					for (var i = 0; i < vm.Akta.length; i++) {
						vm.Akta[i].Filesize = (vm.Akta[i].Filesize / 1024).toFixed(1);
						if (vm.Akta[i].DocumentType === 'LEGAL_DOC_PENDIRIAN') {
							vm.countPendirian = 1;
							vm.AktaPendirian.push(vm.Akta[i]);
						} else if (vm.Akta[i].DocumentType === 'LEGAL_DOC_PERUBAHAN') {
							vm.countPerubahan = 1;
							vm.AktaPerubahan.push(vm.Akta[i]);
						} else if (vm.Akta[i].DocumentType === 'LEGAL_DOC_PENGESAHAN') {
							vm.countPengesahan = 1;
							vm.PengesahanKemenkumham.push(vm.Akta[i]);
						}
						
					}
                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
            return defer.promise
		}

		vm.loadPengurus = loadPengurus;
		function loadPengurus() {
		    var defer = $q.defer();
			VerifikasiDataService.GetByVendorComPer({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
				    vm.compPersons = reply.data;
                    defer.resolve(true)
				} else {
					UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD');
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD');
				UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}
		vm.pageNumber = 1;
		vm.loadTenagaAhli = loadTenagaAhli;
		function loadTenagaAhli() {
		    var defer = $q.defer();
			vm.vendorexperts = [];
			vm.currentPage = vm.pageNumber;
			var offset = (vm.pageNumber - 1 ) * 10;
			VerifikasiDataService.allTenagaahli({
				Status: vm.id,
				Offset: offset,
				Limit: vm.fullSize,
			}, function (reply) {
				if (reply.status === 200) {
				    vm.vendorexperts = reply.data.List;
				    vm.vendorexpertsCount = reply.data.Count;
                    defer.resolve(true)
					//loadCertificate(vm.vendorexperts);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.loadCertificate = loadCertificate;
		function loadCertificate(data) {
		    var defer = $q.defer();
			var offset = (1 * 10) - 10;
			VerifikasiDataService.selectCertificate({
				Offset: offset,
				Limit: vm.fullSize,
				Status: data.ID
			}, function (reply) {
				if (reply.status === 200) {
					vm.vendorexpertsCertificate = reply.data;
					for (var i = 0; i < vm.vendorexpertsCertificate.length; i++) {
						if (vm.vendorexpertsCertificate[i].SysReference.RefID === 3128 && vm.vendorexpertsCertificate[i].IsActive === true) {
							vm.CertificatePengalaman.push(vm.vendorexpertsCertificate[i]);
						} else if (vm.vendorexpertsCertificate[i].SysReference.RefID === 3129 && vm.vendorexpertsCertificate[i].IsActive === true) {
							vm.CertificatePendidikan.push(vm.vendorexpertsCertificate[i]);
						} else if (vm.vendorexpertsCertificate[i].SysReference.RefID === 3130 && vm.vendorexpertsCertificate[i].IsActive === true) {
							vm.CertificateSertifikat.push(vm.vendorexpertsCertificate[i]);
						}
					}
                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.listBuilding = [];
		function loadBuilding(current) {
		    var defer = $q.defer();
		    var offset = (current * vm.fullSize) - vm.fullSize;
			UIControlService.loadLoading(vm.msgLoading);
			VerifikasiDataService.selectBuilding({
				Status: vm.id, Ofsset: offset, Limit: vm.fullSize
			}, function (reply) {
                defer.resolve(true)
				vm.listBuilding = reply.data.List;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.listVehicle = [];
		function loadEquipmentVehicle(current) {
		    var defer = $q.defer();
			var offset = (current * vm.fullSize) - vm.fullSize;
			VerifikasiDataService.selectVehicle({
				Status: vm.id, Ofsset: offset, Limit: vm.fullSize
			}, function (reply) {
				vm.listVehicle = reply.data.List;
				for (var i = 0; i < vm.listVehicle.length; i++) {
					vm.listVehicle[i].MfgDate = UIControlService.getStrDate(vm.listVehicle[i].MfgDate);
				}
                defer.resolve(true)
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.listEquipmentTools = [];
		function loadEquipmentTools(current) {
		    var defer = $q.defer();
			var offset = (current * vm.fullSize) - vm.fullSize;
			UIControlService.loadLoading(vm.msgLoading);
			VerifikasiDataService.selectEquipment({
				Status: vm.id, Ofsset: offset, Limit: vm.fullSize
			}, function (reply) {
				vm.listEquipmentTools = reply.data.List;
				for (var i = 0; i < vm.listEquipmentTools.length; i++) {
					vm.listEquipmentTools[i].MfgDate = UIControlService.getStrDate(vm.listEquipmentTools[i].MfgDate);
				}
                defer.resolve(true)
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.hubIstimewa = [];
		function loadVendorAfiliasi(current) {
		    var defer = $q.defer();
		    var offset = (current * vm.fullSize) - vm.fullSize;
		    VerifikasiDataService.selectVendorAfiliasi({
		        Status: vm.id, Ofsset: offset, Limit: vm.fullSize
		    }, function (reply) {
		        vm.hubIstimewa = reply.data.List;
		        if (vm.hubIstimewa.length > 0) {
		            for (var a = 0; a < vm.hubIstimewa.length; a++) {
		                if (vm.hubIstimewa[a].AfiliasiID == 6) {
		                    vm.afiliasiID = 6;
		                    break;
		                }
		            }
		        }
		        //console.log(reply)
                defer.resolve();
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
                defer.reject(false)
		    });
		    return defer.promise;
		}

		vm.viewDetail = viewDetail;
		function viewDetail(data) {
			vm.namavendor = data.Name;
			vm.flag = true;
			vm.CertificatePengalaman = [];
			vm.CertificatePendidikan = [];
			vm.CertificateSertifikat = [];
			loadCertificate(data);
			vm.dataExpert = data;
		}

		function CheckCommodity() {
		    var defer = $q.defer();
		    VerifikasiDataService.CheckVendorCommodity({
		        VendorID: vm.id
		    }, function (reply) {
		        if (reply.status === 200) {
		            UIControlService.unloadLoading();
		            var data = reply.data;
		            console.log(data)
		            defer.resolve(data);
		        } else {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", 'MESSAGE.CHECKED_FAILED', "MESSAGE.CHECKED_FAILED");
		            defer.reject();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.GAGAL_API', "MESSAGE.GAGAL_API");
		        defer.reject();

		    });
		    return defer.promise;
		}

		vm.updateChecked = updateChecked;
		function updateChecked(data) {
		    UIControlService.loadLoading();

		    console.log(data)
		    data.Remarks = '';
		    if (data.MenuID == 1038) {
		        CheckCommodity().then(function (param) {
		            if (param == 0) {
		                if (!data.Checked) {
		                    data.ReviewStatus = 4525;
		                } else {
		                    data.ReviewStatus = 4527;
		                }

		                VerifikasiDataService.updateReviewStatus({
		                    ReviewStatus: data.ReviewStatus,
		                    ReviewID: data.ReviewID
		                }, function (reply) {
		                    if (reply.status === 200) {
		                        UIControlService.msg_growl("success", 'MESSAGE.SUCCESS');
		                        UIControlService.unloadLoading();
		                    } else {
		                        UIControlService.unloadLoading();
		                        UIControlService.msg_growl("error", 'MESSAGE.CHECKED_FAILED', "MESSAGE.CHECKED_FAILED");
		                    }
		                }, function (err) {
		                    UIControlService.unloadLoading();
		                    UIControlService.msg_growl("error", 'MESSAGE.GAGAL_API', "MESSAGE.GAGAL_API");
		                });
		            } else {
		                data.ReviewStatus = 4525;
		                data.Checked = false;
		                UIControlService.msg_growl("warning", 'MESSAGE.SCALA_VENDOR');
		                UIControlService.unloadLoading();
		            }
		        })
		        return;
		    }

		    if (!data.Checked) {
		        data.ReviewStatus = 4525;
		    } else {
		        data.ReviewStatus = 4527;
		    }

		    VerifikasiDataService.updateReviewStatus({
		        ReviewStatus: data.ReviewStatus,
		        ReviewID: data.ReviewID
		    }, function (reply) {
		        if (reply.status === 200) {
		            UIControlService.msg_growl("success", 'MESSAGE.SUCCESS');
		            UIControlService.unloadLoading();
		        } else {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", 'MESSAGE.CHECKED_FAILED', "MESSAGE.CHECKED_FAILED");
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.GAGAL_API', "MESSAGE.GAGAL_API");
		    });

		}

		vm.ChangeRevisi = ChangeRevisi;
		function ChangeRevisi(param) {

		}

		vm.addFlag = addFlag;
		function addFlag() {
			vm.flag = false;
		}

		vm.detailForm = detailForm;
		function detailForm(type, datas, isAdd) {
			var data = {
				type: type,
				data: datas,
				isForm: isAdd
			}
			var ctrl;
			var ctrlAs;
			if (type === "building") {
				ctrl = "FormBuildingController";
				ctrlAs = "FormBuildingCtrl";
			} else {
				ctrl = "FormNonBuildingController";
				ctrlAs = "FormNonBuildingCtrl";
			}
			var modalInstance = $uibModal.open({
				templateUrl: "app/modules/panitia/data-rekanan/verifikasi-data/detailData.html",
				controller: ctrl,
				controllerAs: ctrlAs,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				jLoad(1);
			});
		}

		vm.loadPengalaman = loadPengalaman;
		function loadPengalaman() {
			UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
			VerifikasiDataService.selectExperience({
				Offset: (vm.currentPage - 1) * vm.fullSize,
				Limit: 100,
				Keyword: vm.keyword,
				column: 1,
				Status: vm.id
			}, function (reply) {
				if (reply.status === 200) {
					vm.listFinishExp = reply.data.List;
					for (var i = 0; i < vm.listFinishExp.length; i++) {
						vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);
					}
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
			});

			UIControlService.loadLoading('MESSAGE.LOADING_VENDOREXPERIENCE');
			VerifikasiDataService.selectExperience({
				Offset: (vm.currentPage - 1) * vm.fullSize,
				Limit: 100,
				Keyword: vm.keyword,
				column: 2,
				Status: vm.id
			}, function (reply) {
				if (reply.status === 200) {
					vm.listCurrentExp = reply.data.List;
					for (var i = 0; i < vm.listCurrentExp.length; i++) {
						vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
					}
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
			});

		}

		vm.loadSaham = loadSaham;
		function loadSaham() {
		    var defer = $q.defer();
			VerifikasiDataService.selectSaham({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
				    vm.verifiedSaham = reply.data;
                    console.log(vm.verifiedSaham)
                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.loadDokumen = loadDokumen;
		function loadDokumen() {
		    var defer = $q.defer();
			VerifikasiDataService.GetByVendor({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
				    vm.verifiedDokumen = reply.data;
                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}


		vm.loadNeraca = loadNeraca;
		function loadNeraca() {
		    var defer = $q.defer();
		    loadBalanceUrl().then(function (response) {
		        if (response) {
		            vm.asset = 0;
		            vm.hutang = 0;
		            vm.modal = 0;
		            VerifikasiDataService.selectNeraca({
		                VendorID: vm.id
		            }, function (reply) {
		                if (reply.status === 200) {
		                    vm.vendorbalance = reply.data;
		                    vm.currID = vm.vendorbalance[0].listDataBalanceModel[0].subCategory[0].CurrencyID;
		                    loadCurrency(vm.currID);
		                    for (var i = 0; i < vm.vendorbalance.length; i++) {
		                        if (vm.vendorbalance[i].BalanceName == "WEALTH_TYPE_ASSET") {
		                            for (var c = 0; c < vm.vendorbalance[i].listDataBalanceModel.length; c++) {
		                                for (var g = 0; g < vm.vendorbalance[i].listDataBalanceModel[c].subCategory.length; g++) {
		                                    vm.asset += Number(vm.vendorbalance[i].listDataBalanceModel[c].subCategory[g].Nominal);
		                                }
		                            }
		                        }
		                        if (vm.vendorbalance[i].BalanceName == "WEALTH_TYPE_DEBTH") {
		                            for (var c = 0; c < vm.vendorbalance[i].listDataBalanceModel.length; c++) {
		                                for (var g = 0; g < vm.vendorbalance[i].listDataBalanceModel[c].subCategory.length; g++) {
		                                    vm.hutang += Number(vm.vendorbalance[i].listDataBalanceModel[c].subCategory[g].Nominal);
		                                }

		                                
		                            }
		                        }
		                    }

		                    VerifikasiDataService.getSisaMstBalance(
                            { assetBalance: vm.asset, hutangBalance: vm.hutang, VendorID:vm.id },
                            function (reply) {
                                if (reply.status == 200) {
                                    var data = reply.data;
                                    var dataAsset = data[0];
                                    var dataHutang = data[1];

                                    for (var i = 0; i < dataAsset.length; i++) {
                                        vm.vendorbalance.push(dataAsset[i]);
                                    }
                                    for (var i = 0; i < dataHutang.length; i++) {
                                        vm.vendorbalance.push(dataHutang[i]);
                                    }
                                    console.log(data)
                                }
                            }, function (err) {
                                $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                                UIControlService.unloadLoading();
                            })

		                    vm.modal = +vm.asset - +vm.hutang;
		                    defer.resolve(true)
		                } else {
		                    $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
		                    UIControlService.unloadLoading();
		                    defer.reject(false)
		                }
		            }, function (err) {
		                //$.growl.error({ message: "Gagal Akses API >" + err });
		                UIControlService.unloadLoading();
		                defer.reject(false)
		            });
		            
		        }
		    });
		    return defer.promise;
		}

		vm.loadBalanceUrl = loadBalanceUrl;
		function loadBalanceUrl() {
		    var defer = $q.defer();
		    VerifikasiDataService.balanceDocUrl({VendorID: vm.id},function (reply) {
		        if (reply.status === 200) {
		            vm.balanceDocUrl = reply.data;
                    defer.resolve(true)
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
                defer.reject(false)
		    });
		    return defer.promise;
		}

		vm.loadCompanyScale = loadCompanyScale;
		function loadCompanyScale(data) {
			VerifikasiDataService.getCompanyScale(function (reply) {
				if (reply.status === 200) {
					vm.listCompanyScale = reply.data.List;

					for (var j = 0; j < data.length; j++) {
						if (data[j].ScaleReff != 0) {
							for (var i = 0; i < vm.listCompanyScale.length; i++) {
								if (vm.listCompanyScale[i].RefID === vm.dataComm[j].ScaleReff) {
									vm.dataComm[j].ScaleReffS = vm.listCompanyScale[i];
								}
							}
						}
					}
                    console.log(vm.dataComm)
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
			});
		}

        //technical
		vm.loadTechnical = loadTechnical;
		function loadTechnical(data) {
		    VerifikasiDataService.getTechnical(function (reply) {
		        if (reply.status === 200) {
		            vm.listTech = reply.data.List;
		            for (var j = 0; j < data.length; j++) {
		                if (data[j].TechnicalReff != 0) {
		                    for (var i = 0; i < vm.listTech.length; i++) {
		                        if (vm.listTech[i].RefID === vm.dataComm[j].TechnicalReff) {
		                            vm.dataComm[j].TechnicalReffS = vm.listTech[i];
		                        }
		                    }
		                }
		            }
		        } else {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.VENDOREXPERIENCE_ERROR', "MESSAGE.VENDOREXPERIENCE_TITLE");
		    });
		}

		vm.cancel = cancel;
		function cancel() {
		    if (!vm.isVendorVerified) {
			    $state.transitionTo('verifikasi-data');

		    } else {
		        $state.transitionTo('approval-data-rekanan', { vendorID: vm.id });
		    }
		}


		vm.sendApprovalVendorCompliance = sendApprovalVendorCompliance;
		function sendApprovalVendorCompliance(vendorComplianceApprovalID) {

		    bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_SEND_APPROVAL') + '</h4>', function (res) {
		        if (res) {
		            VerifikasiDataService.sendApprovalVendorCompliance({ ID: vendorComplianceApprovalID }, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.status === 200) {
		                    UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SAVE', '');
		                    UIControlService.unloadLoading();
		                    init();
		                } else {
		                    UIControlService.msg_growl('error', 'MESSAGE.ERR_SAVE', '');
		                    UIControlService.unloadLoading();
		                }
		            }, function (err) {
		                UIControlService.unloadLoading();
		            });

		            //SocketService.emit("daftarRekanan");
		        }
		    });

		}

		vm.resetdata = resetdata;
		function resetdata(data) {
		    var komoditas = '';
		    if (data.CommodityID != null && data.BusinessField.Name != 'Lain - lain') {
		        komoditas = data.Commodity.Name;
		    }
		    else if (data.CommodityID == null && data.BusinessField.Name != 'Lain - lain') {
		        komoditas = data.BusinessField.Name;
		    }
		    else if (data.BusinessField.Name != 'Lain - lain') {
		        komoditas = data.BusinessField.Name + data.Remark;
		    }
		    bootbox.confirm('<h4 class="afta-font center-block">'+$filter('translate')('MESSAGE.SURE_RESETSCALE')+komoditas+'?</h4>', function (res) {
		        if (res) {
		            vm.dataBalance = {
		                ID: data.ID,
		                ScaleReffS: null,
		                TechnicalReffS: null
		            };
		            VerifikasiDataService.UpdateScaleBalance(vm.dataBalance, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.status === 200) {
		                    UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SAVE', '');
		                    UIControlService.unloadLoading();
		                    init();
		                } else {
		                    UIControlService.msg_growl('error', 'MESSAGE.ERR_SAVE', '');
		                    UIControlService.unloadLoading();
		                }
		            }, function (err) {
		                UIControlService.unloadLoading();
		            });

		            //SocketService.emit("daftarRekanan");
		        }
		    });
		}

		vm.listDataBalance = [];
		vm.changeScale = changeScale;
		function changeScale(data) {
			/*var dataBalance = {
				ID: data.ID,
				ScaleReffS: data.ScaleReffS,
				TechnicalReff: data.TechnicalReff

			};
			VerifikasiDataService.UpdateScaleBalance(dataBalance, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    $.growl.error({ message: "Data Berhasil Tersimpan" });
				    UIControlService.unloadLoading();
				} else {
				    $.growl.error({ message: "Data Gagal Tersimpan" });
				    UIControlService.unloadLoading();
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});*/
		    vm.dataBalance = {
		        ID: data.ID,
		        ScaleReffS: data.ScaleReffS,
                TechnicalReffS : data.TechnicalReffS
		    };
		    if (vm.listDataBalance.length == 0) {
		        vm.listDataBalance.push(vm.dataBalance);
		    }
		    else {
		        for (var i = 0; i <= vm.listDataBalance.length - 1; i++) {
		            if (vm.listDataBalance[i].ID == vm.dataBalance.ID) {
		                vm.listDataBalance = remove(vm.listDataBalance, vm.listDataBalance[i]);
		                i = vm.listDataBalance.length - 1;
		            }
		        }
		        vm.listDataBalance.push(vm.dataBalance);

		    }
		}

		function remove(list, obj) {
		    var index = list.indexOf(obj);
		    if (index >= 0) {
		        list.splice(index, 1);
		    } else {
		        UIControlService.msg_growl('error', "ERRORS.OBJECT_NOT_FOUND");
		    }
		    return list;
		}

		vm.save = save;
		function save() {
		    console.info("listdatascale" + JSON.stringify(vm.listDataBalance));
		    var boleh = true;
		    for (var i = 0; i < vm.dataComm.length; i++) {
		        if (vm.dataComm[i].TechnicalReffS == null) {
		            boleh = false;
		        }
		        if (vm.dataComm[i].ScaleReffS == null) {
		            boleh = false;
		        }
		    }
		    if (!boleh) {
		        UIControlService.msg_growl('warning', "MESSAGE.SKALA_VENDOR_NULL");
		        return;
		    }

		    UIControlService.loadLoading();
		    VerifikasiDataService.UpdateScaleBalance({ VendorCommodityData: vm.dataComm }, function (reply) {
		        if (reply.status === 200) {
		            UIControlService.unloadLoading();
		            init();
		        } else {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_SAVE', '');
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		    UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SAVE', '');
		    UIControlService.unloadLoading();
		    
		}

		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.otherdoc = otherdoc;
		function otherdoc() {
		    var defer = $q.defer();
			VerifikasiDataService.SelectVend({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
				    var data = reply.data;
				    console.info("doklain2" + JSON.stringify(data));
					vm.document = data;
					vm.document.forEach(function (cr) {
						cr.ValidDateConverted = convertDate(cr.ValidDate);
					});
					defer.resolve(true);
				} else {
					$.growl.error({ message: "Gagal mendapatkan dokumen" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.openForm = openForm;
		function openForm(data, flag, isForm) {

			if (flag === 6 && isForm != true) {
				var data = {
					item: {
						BalanceID: isForm.BalanceID,
						Wealth: data,
						COA: isForm.COAType,
						Unit: isForm.Unit,
						Amount: isForm.Amount,
						DocUrl: isForm.DocUrl,
						Nominal: isForm.nominal
					}, flag: flag, isForm: true
				}
				var modalInstance = $uibModal.open({
				    templateUrl: "app/modules/panitia/data-rekanan/verifikasi-data/form-izin-usaha.html?v=1.000001",
					controller: 'FormIzinCtrl',
					controllerAs: 'FormIzinCtrl',
					resolve: {
						item: function () {
							return data;
						}
					}
				});
				modalInstance.result.then(function () {
					init();
				});
			}
			else if ((flag === 22||flag===23) && isForm == true) {
			    var data = {
			        item: {
                        item:data
			        }, flag: flag, isForm: true
			    }
			    var modalInstance = $uibModal.open({
			        templateUrl: "app/modules/panitia/data-rekanan/verifikasi-data/form-izin-usaha.html?v=1.000001",
			        controller: 'FormIzinCtrl',
			        controllerAs: 'FormIzinCtrl',
			        resolve: {
			            item: function () {
			                return data;
			            }
			        }
			    });
			    modalInstance.result.then(function () {
			        init();
			    });
			}
			else {
				console.log('MASUK SINI KOK')
				var data = {
					item: data,
					flag: flag,
					isForm: isForm,
					city: vm.cityID,
					vendorID: vm.id,
					VendorCategoryID: vm.VendorCategoryID
				}
				console.log(data);
				var modalInstance = $uibModal.open({
				    templateUrl: "app/modules/panitia/data-rekanan/verifikasi-data/form-izin-usaha.html?v=1.000001",
					controller: 'FormIzinCtrl',
					controllerAs: 'FormIzinCtrl',
					resolve: {
						item: function () {
							return data;
						}
					}
				});
				modalInstance.result.then(function () {
					init();
				});
			}

		}

		vm.deleteObj = deleteObj;
		function deleteObj(data, flag) {
			vm.listdata = data;
			if (data.IsActive == true) vm.listdata.IsActive = false;
			else vm.listdata.IsActive = true;
			UIControlService.loadLoading();

			if (flag === 1) {
				VerifikasiDataService.DeleteVendorLicensi({
					LicenseID: vm.listdata.LicenseID,
					VendorID: vm.listdata.VendorID
				}, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
						init();
						vm.flag = false;
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoadingModal();
				});
			} else if (flag === 2) {
				VerifikasiDataService.DeleteVendorStock({
					IsActive: vm.listdata.IsActive,
					StockID: vm.listdata.StockID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 3) {
				VerifikasiDataService.DeleteVendorLegal({
					IsActive: vm.listdata.IsActive,
					StockID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 6) {
				VerifikasiDataService.DeleteVendorBalance({
					IsActive: vm.listdata.IsActive,
					BalanceID: vm.listdata.BalanceID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 7) {
				VerifikasiDataService.DeletePengurus({
					ID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 8) {
				VerifikasiDataService.DeleteVendorExperts({
					IsActive: vm.listdata.IsActive,
					ID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 9 || flag === 10 || flag === 11) {
				VerifikasiDataService.DeleteVendorExpertCertificate({
					IsActive: vm.listdata.IsActive,
					ID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
						addFlag();
						viewDetail(vm.dataExpert);

					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 12) {
				VerifikasiDataService.DeleteVendorBuilding({
					IsActive: vm.listdata.IsActive,
					ID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 13 || flag === 14) {
				VerifikasiDataService.DeleteVendorEquipment({
					IsActive: vm.listdata.IsActive,
					ID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			} else if (flag === 16) {
				VerifikasiDataService.DeleteOtherDoc({
					ID: vm.listdata.ID
				}, function (reply2) {
					UIControlService.unloadLoading();
					if (reply2.status === 200) {
						UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
						init();
					} else
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
				});
			}
		}

		vm.openFormBuilding = openFormBuilding;
		function openFormBuilding(type, data, isAdd) {
			var data = {
				type: type,
				data: data,
				isForm: isAdd
			}
			var temp;
			var ctrl;
			var ctrlAs;
			if (type === "building") {
				temp = "app/modules/panitia/data-rekanan/verifikasi-data/formBuilding.html";
				ctrl = "FormBuildingController";
				ctrlAs = "FormBuildingCtrl";
			} else {
				temp = "app/modules/panitia/data-rekanan/verifikasi-data/formNonBuilding.html";
				ctrl = "FormNonBuildingController";
				ctrlAs = "FormNonBuildingCtrl";
			}
			var modalInstance = $uibModal.open({
				templateUrl: temp,
				controller: ctrl,
				controllerAs: ctrlAs,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.openFormTenagaAhli = openFormTenagaAhli;
		function openFormTenagaAhli(data, flag) {
			var data = {
				item: data,
				flag: flag
			}
			var modalInstance = $uibModal.open({
				templateUrl: "app/modules/panitia/data-rekanan/verifikasi-data/FormTenagaAhli.html",
				controller: 'formTenagaAhliCtrl',
				controllerAs: 'formTenagaAhliCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.loadCommodity = loadCommodity;
		function loadCommodity() {
		    var defer = $q.defer();
		    vm.dataComm = [];
			VerifikasiDataService.VendorCommodity({
				VendorID: vm.id
			}, function (reply2) {
				if (reply2.status === 200) {
					for (var i = 0; i < reply2.data.length; i++) {
					    if (reply2.data[i].IsActive === true) vm.dataComm.push(reply2.data[i]);
					    if ((reply2.data.length - 1) == i){
					        loadCompanyScale(vm.dataComm);
					        loadTechnical(vm.dataComm);
					    } 
                        
					}
					defer.resolve(true);
				} else {
				    UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
                    defer.reject(false)
				}

			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.loadBankDetail = loadBankDetail;
		function loadBankDetail() {
		    var defer = $q.defer();
			vm.currentPage = 1;
			var offset = (1 * 10) - 10;
			vm.bankdetail = [];
			VerifikasiDataService.selectBankDetail({
				VendorID: vm.id
			}, function (reply) {
				if (reply.status === 200) {
				    vm.bankdetail = reply.data;
				    vm.bankdetail.List.forEach(function (item) {
				        if (item.VendorCategoryID == 33) {
				            vm.bankDetailVendorCategory = true;
				        }
				    })
                    defer.resolve(true)
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
                    defer.reject(false)
				}
			}, function (err) {
				//$.growl.error({ message: "Gagal Akses API >" + err });
			    UIControlService.unloadLoading();
                defer.reject(false)
			});
			return defer.promise;
		}

		vm.updateBankDetail = updateBankDetail;
		function updateBankDetail(data) {
			var data = {
				act: 0,
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.deleteBankDetail = deleteBankDetail;
		function deleteBankDetail(doc) {
			bootbox.confirm('<h3 class="afta-font center-block">' + "Yakin ingin menghapus?" + '<h3>', function (reply) {
				if (reply) {
					//UIControlService.loadLoading(loadmsg);
					VerifikasiDataService.deleteBankDetail({ ID: doc.ID }, function (reply2) {
						UIControlService.unloadLoading();
						if (reply2.status === 200) {
							UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
							init();
						} else
							UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
					});
				}
			});
		};

		vm.removeAfiliasi = removeAfiliasi;
		function removeAfiliasi(data) {
		    bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
		        if (reply) {
		            UIControlService.loadLoading('MESSAGE.LOADING');
		            VerifikasiDataService.removeAfiliasi({
		                VendorAfiliasiID: data.VendorAfiliasiID
		            }, function (reply2) {
		                UIControlService.unloadLoading();
		                if (reply2.status === 200) {
		                    UIControlService.msg_growl('notice', 'MESSAGE.SUCC_DEL');
		                    vm.init();
		                } else {
		                    UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
		                }
		            }, function (error) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
		            });
		        }
		    });
		}

		vm.afiliasiUpdateModal = afiliasiUpdateModal;
        function afiliasiUpdateModal(data){
            var data = {
                act: 0,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/afiliasi-tambah-ubah.modal.html',
                controller: "AfiliasiModalCtrl",
                controllerAs: "AfiliasiModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadVendorAfiliasi(1);
            });
        }

        vm.downloadFileCivd = downloadFileCivd;
        function downloadFileCivd(url, name) {
            UIControlService.loadLoading();

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    var byteCharacters = atob(reply.result);

                    var byteNumbers = new Array(byteCharacters.length);
                    for (let i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    var blob = new Blob([byteArray], {
                        type: "application/pdf"
                    });

                    FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
                    UIControlService.unloadLoading();

                } else {
                    UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

            });
        }

        vm.DetailVerifikasiApproval = DetailVerifikasiApproval;
        function DetailVerifikasiApproval(data) {
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/verifikasiApproval.modal.html',
                controller: 'DetailVerifikasiApprovalModalApprovalCtrl',
                controllerAs: 'DetailVerifikasiApprovalModalApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });

        }

        vm.detailKontak = detailKontak;
        function detailKontak() {
            var data = {
                VendorID: $stateParams.id
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/detailKontak.modal.html',
                controller: "DetailKontakModalCtrl",
                controllerAs: "DetailKontakModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        vm.detailKantor = detailKantor;
        function detailKantor() {
            var data = {
                VendorID: $stateParams.id
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/detailKantor.modal.html',
                controller: "DetailKantorModalCtrl",
                controllerAs: "DetailKantorModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        function getSysAccess() {
            var roleId = localStorage.getItem('RoleIDOnly');

            var defer = $q.defer();
            AuthService.getSysAccess({ IntParam1: roleId, IntParam2: 2070 }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
					console.log(data)
					vm.fullAccess = data[0].FullControl;
					vm.readAccess = data[0].ReadAccess;
					vm.editAccess = data[0].EditAccess;
					vm.createAccess = data[0].CreateAccess;
					vm.deleteAccess = data[0].DeleteAccess;
					console.log("Akses "+vm.fullAccess);
                    //vm.CompanyScale = data.CompanyScale;

                    defer.resolve(true);
                } else {
                    defer.reject(false);
                }
            }, function (err) {
                defer.reject(false);
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            })
            return defer.promise;
        }

        function loadCurrency(id) {
            VerifiedSendService.getCurrencies(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.currencyList = reply.data;
                    if (id != undefined) {
                        for (var i = 0; i < vm.currencyList.length; i++) {
                            if (vm.currencyList[i].CurrencyID == id) {
                                vm.labelCurr = vm.currencyList[i].Symbol;
                                break;
                            }
                        }
                    }
                } else {
                    $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

	}
})();
