﻿(function () {
	'use strict';

	angular.module("app").controller("VerifyVendorCtrl", ctrl);

	ctrl.$inject = ['item', '$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VerifiedSendService', 'VerifikasiDataService', 'SrtPernyataanService', 'RoleService', 'UIControlService', '$uibModal', '$stateParams', '$state', 'GlobalConstantService', '$uibModalInstance'];
	function ctrl(item,$http, $filter, $translate, $translatePartialLoader, $location, SocketService, VerifiedSendService, VerifikasiDataService, SrtPernyataanService,
        RoleService, UIControlService, $uibModal, $stateParams, $state, GlobalConstantService, $uibModalInstance) {
		//console.info("atur: "+JSON.stringify(item));
		var vm = this;
		//vm.Vendor = item.data;
		vm.emailAddress = item.emailAddress.Email;
		vm.namaPerusahaan = item.emailAddress.Name.toUpperCase();
		vm.IsVerified = item.IsVerified;
		vm.VendorID = item.VendorID;
		vm.VendorName = item.VendorName;
		

		vm.init = init;
		function init() {
            //console.info()
		    //console.info("vendor" + JSON.stringify(item));
		}

		vm.simpan = simpan;
		function simpan(verificationRemark) {

		    if (verificationRemark == undefined || verificationRemark == null) {
		        verificationRemark = "";
		    }

		    if (vm.IsVerified === true) {
		        vm.Vendor = {
		            VendorID: vm.VendorID,
		            Isverified: 1,
		            VerificationRemark: verificationRemark
		        }
		    } else if (vm.IsVerified === false) {
		        vm.Vendor = {
		            VendorID: vm.VendorID,
		            Isverified: 0,
		            VerificationRemark: verificationRemark
		        }
		    }
		    UIControlService.loadLoadingModal("MESSAGE.LOADING");
		    VerifiedSendService.updateVerifikasi(vm.Vendor, function (reply) {
		        if (reply.status === 200) {
		            var msg = "";
		            var data = reply.data[1].replace(/#prekanan/g, vm.namaPerusahaan);
                    
		            
		           
		            if (vm.IsVerified === false) msg = $filter('translate')('Tolak Verifikasi');
		            if (vm.IsVerified === true) msg = $filter('translate')('Verifikasi');
		            //$state.transitionTo('verifikasi-data');
		            if (vm.IsVerified) {
		                var emailApprover = reply.data[0];
		                var ApproverContentEmail = reply.data[2].replace('#vendorname',vm.VendorName);
		                console.log(emailApprover);
		                var listEmailApprover = [];

		                for (var i = 0; i < emailApprover.length; i++) {
		                    listEmailApprover.push(emailApprover[i].Email);
		                }
		                
		                data = data.split('#split');
		                ApproverContentEmail = ApproverContentEmail.split('#split');

		                if (localStorage.getItem("currLang").toLowerCase() === 'id') {
		                    var kontenSent = ApproverContentEmail[0].replace('#approvername', emailApprover[0].FullName);
		                    sendMail(kontenSent, listEmailApprover);
		                }
		                else if (localStorage.getItem("currLang").toLowerCase() === 'en') {
		                    var kontenSent = ApproverContentEmail[1].replace('#approvername', emailApprover[0].FullName);
		                    sendMail(kontenSent, listEmailApprover);
		                }
		            } else {
		                if (localStorage.getItem("currLang").toLowerCase() === 'id') {
		                    var kontenSent = data[0].replace('#keterangan', verificationRemark);
		                    sendMail(kontenSent, [vm.emailAddress]);
		                }
		                else if (localStorage.getItem("currLang").toLowerCase() === 'en') {
		                    var kontenSent = data[1].replace('#keterangan', verificationRemark);
		                    sendMail(kontenSent, [vm.emailAddress]);
		                }
		            }

		            

		            SocketService.emit("daftarRekanan");

		            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_VERIFY') + msg);
		            UIControlService.unloadLoadingModal();
		            $uibModalInstance.close();
		            $state.go('verifikasi-data');
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_VERIFY");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}


		function sendMail(mailContent, emailAddress) {
		    var email = {
		        subject: 'Vendor Verification Notification',
		        mailContent: mailContent,
		        isHtml: false,
		        addresses: emailAddress
		    };

		    UIControlService.loadLoading("MESSAGE.LOADING_SEND_EMAIL");
		    VerifikasiDataService.sendMailActived(email, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status === 200) {
		            // UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT_VENDOR")
		        } else {
		            UIControlService.handleRequestError(response.data);
		        }
		    }, function (response) {
		        UIControlService.handleRequestError(response.data);
		        UIControlService.unloadLoading();
		        //$state.go('daftar_kuesioner');
		    });
		}
       


		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();