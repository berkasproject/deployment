﻿(function () {
    'use strict';

    angular.module("app").controller("DetailVerifikasiApprovalModalApprovalCtrl", ctrl);
    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VerifikasiDataService', 'UIControlService', 'item', '$uibModalInstance', '$uibModal', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        VerifikasiDataService, UIControlService, item, $uibModalInstance, $uibModal, GlobalConstantService) {
        var vm = this;
        vm.isAdd = item.item;
        vm.VendorID = item.item.VendorID;
        vm.act = item.act;
        vm.aktifasi = "";
        vm.pageSize = 10;
        vm.Keyword = "";
        vm.VendorName = item.item.VendorName;
        vm.currentPage = 1;
        vm.totalItems = 0;

        vm.initApproval = initApproval;
        function initApproval() {
            $translatePartialLoader.addPart('approval-data-rekanan');
            UIControlService.loadLoadingModal();

            jLoadApproval(1);


        };

        vm.jLoadApproval = jLoadApproval;
        function jLoadApproval(current) {
            //console.info("curr "+current)
            vm.dataApprover = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            VerifikasiDataService.selectApprovalVendor({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.Keyword,
                IntParam1: vm.VendorID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApprover = data.List;
                    vm.totalItems = data.Count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data ApprovalVendor" });
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();
