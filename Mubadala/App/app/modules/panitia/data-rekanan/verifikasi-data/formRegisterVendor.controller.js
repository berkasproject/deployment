(function () {
	'use strict';

	angular.module("app").controller("FormRegisterVendorCtrl", ctrl);
	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', 'VendorRegistrationService', 'DataAdministrasiService', '$location', 'SocketService',
		'UIControlService', '$uibModal', 'VerifikasiDataService', 'UploadFileConfigService', 'UploaderService',
		'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService', 'BlacklistService', '$state'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, VendorRegistrationService, DataAdministrasiService, $location, SocketService,
		 UIControlService, $uibModal, VerifikasiDataService, UploadFileConfigService, UploaderService, AuthService,
            $filter, ProvinsiService, GlobalConstantService, BlacklistService,$state) {
		var vm = this;
		vm.init = init;
		vm.aktifasi = "";
		vm.pageSize = 10;
		vm.Keyword = "";
		vm.folderFile = GlobalConstantService.getConstant('api') + "/"
		vm.tglSekarang = UIControlService.getDateNow("");
		vm.formValid = false;
		vm.Tmpblacklist = false;
		vm.vendorSAPCode = null;
		vm.showPass = false;
       

		const MODULE_ID = 2;
		const STOCK_OWNER_ID_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.ID';
		const VENDOR_REGISTRATION_NO_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.NPWP';
		const VENDOR_LEGAL_NO_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.SIUP';
		const UPLOAD_PREFIX = 'UPLOAD_PREFIX';
		const UPLOAD_PREFIX_ID = 'UPLOAD_PREFIX_ID';
		const UPLOAD_PREFIX_NPWP = 'UPLOAD_PREFIX_NPWP';
		const UPLOAD_PREFIX_SIUP = 'UPLOAD_PREFIX_SIUP';
		const SIZE_UNIT_KB = 'SIZE_UNIT_KB';
		const SIZE_UNIT_MB = 'SIZE_UNIT_MB';
		const COUNTRY_INDONESIA = 'IDN';
		const STOCK_UNIT_CURRENCY = 'STOCK_UNIT_CURRENCY';
		const VENDOR_OFFICE_TYPE_MAIN = 'VENDOR_OFFICE_TYPE_MAIN';

		vm.address = {
		    Type: {},
		    City: {},
		    State: {},
		    Country: {}
		};


		vm.stocks = [];
		vm.stockOption = [];

		vm.addresses = [];

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    $translatePartialLoader.addPart('daftar');
		    $translatePartialLoader.addPart('verifikasi-data');
		    $translatePartialLoader.addPart('data-administrasi');
		    loadCountry();
		};

		vm.selectedCountry = null;
		vm.listCountry = [];
		function loadCountry() {
		    DataAdministrasiService.SelectCountry(function (response) {
		        vm.listCountry = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.changeCountry = changeCountry;
		function changeCountry(data) {
		    //console.info("selectedCountry" + JSON.stringify(vm.selectedCountry.Code));
		    if (!data) {
		        data = vm.selectedCountry;
		        vm.selectedState = null;
		        vm.selectedCity = null;
		        vm.selectedDistrict = null;
		    }

            //load all config here
		    loadRegion(data.CountryID);
		    loadState(data);
		    loadFilePrefix();
		    loadConfigFileNPWP();
		    loadCurrencies();
		    loadPhoneCodes();
		    loadOfficeType();
		    reloadCountries();
		    if (vm.selectedCountry.Code == 'IDN') {
		        loadBusinessEntity();
		        loadLegal();
		        loadConfigLegal();
		        loadOptionStock();
		        loadConfigStock();
		        loadStockUnits();
		    }
		}

		vm.region = null;
		vm.loadRegion = loadRegion;
		function loadRegion(countryID) {
		    DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
		        vm.region = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.listState = [];
		vm.selectedState = null;
		vm.loadState = loadState;
		function loadState(country) {
		    DataAdministrasiService.SelectState(country.CountryID, function (response) {
		        vm.listState = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.changeState = changeState;
		function changeState(data) {
		    if (!data) {

		        data = vm.selectedState;
		        vm.selectedCity = "";
		        vm.selectedDistrict = "";
		    }
		    loadCity(data);
		}

		vm.listCity = [];
		vm.selectedCity = null;
		vm.loadCity = loadCity;
		function loadCity(data) {
		    DataAdministrasiService.SelectCity(data.StateID, function (response) {
		        vm.listCity = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.changeCity = changeCity;
		function changeCity(city) {
		    if (!city) {
		        city = vm.selectedCity;
		        vm.selectedDistrict = "";

		    }
		    loadDistrict(city);
		}

		vm.listDistrict = [];
		vm.selectedDistrict = null;
		vm.loadDistrict = loadDistrict;
		function loadDistrict(city) {
		    DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
		        vm.listDistrict = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.Username = '';
		vm.Password = '';
		vm.vendorName = '';
		vm.updateUsername = updateUsername;
		function updateUsername(data) {
		    if (data != '') {
		        vm.Username = data.replace(/ /gi, '_').toLowerCase();
		    }
		    else {
		        vm.Username = '';
		    }
		    //console.info("username:" + vm.Username);
		    VerifikasiDataService.cekUsername({Keyword:vm.Username}, function (response) {
		        vm.dataUsername = response.data;
		        if (vm.dataUsername > 0) {
		            vm.Username = vm.Username + vm.dataUsername.toString();
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.selectedBE = null;
		vm.listBusinessEntity = [];
		vm.loadBusinessEntity = loadBusinessEntity;
		function loadBusinessEntity() {
		    DataAdministrasiService.SelectBusinessEntity(
               function (response) {
                   if (response.status == 200) {
                       vm.listBusinessEntity = response.data;
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                   return;
               });
		}

		vm.foundedDate = null;
		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		function loadConfigFileNPWP() {
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.NPWP", function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            vm.nameNPWP = response.data.name;
		            vm.idUploadConfigsNPWP = response.data;
		            vm.idFileTypesNPWP = generateFilterStrings(response.data);
		            vm.idFileSizeNPWP = vm.idUploadConfigsNPWP[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		        return;
		    });
		}

		vm.selectFileNpwp = selectFileNpwp;
		function selectFileNpwp() {
		    if (vm.fileUploadNPWP != null) {
		        uploadAdministrasiNpwp(vm.fileUploadNPWP, vm.idFileSizeNPWP, vm.idFileTypesNPWP, "");
		    }
		}

		function generateFilterStrings(allowedTypes) {
		    var filetypes = "";
		    for (var i = 0; i < allowedTypes.length; i++) {
		        filetypes += "." + allowedTypes[i].Name + ",";
		    }
		    return filetypes.substring(0, filetypes.length - 1);
		}

		vm.NpwpUrl = '';
		vm.npwp = '';
		vm.uploadAdministrasiNpwp = uploadAdministrasiNpwp;
		function uploadAdministrasiNpwp(file, config, filters, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
		    UploaderService.uploadRegistration(file, vm.npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            var url = response.data.Url;
		            vm.pathFile1 = url;
		            vm.FileName = response.data.FileName;
		            var s = response.data.FileLength;
		            vm.NpwpUrl = vm.pathFile1;
		            //saveAdministrasi();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (response) {
		        if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
		            UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
		            UIControlService.unloadLoading();
		        }
		    });
		}

		vm.loadFilePrefix = loadFilePrefix;
		function loadFilePrefix() {
		    //UIControlService.loadLoading("LOADERS.LOADING_PREFIX");
		    VerifikasiDataService.getUploadPrefix(
                function (response) {
                    var prefixes = response.data;
                    vm.prefixes = {};
                    for (var i = 0; i < prefixes.length; i++) {
                        vm.prefixes[prefixes[i].Name] = prefixes[i];
                    }
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		function handleRequestError(response) {
		    UIControlService.log(response);
		    UIControlService.handleRequestError(response.data, response.status);
		    UIControlService.unloadLoading();
		}

		vm.checkNpwp = checkNpwp;
		function checkNpwp() {
		    //console.log("npwp: " + vm.vendor.npwp);
		    if (vm.npwp != '') {
		        UIControlService.loadLoading("LOADERS.CHECKING_NPWP");
		        VendorRegistrationService.checkNpwp(vm.npwp, function (response) {
		            var data = response.data;
		            if (data.IsCheckedNpwp == false) {
		                
                        
		                if (data.VendorID != 0) {
		                    UIControlService.msg_growl('error', 'FORM.NPWP_ISUSED', 'FORM.NPWP_ISUSED');
		                    //loadVendor(data.VendorID);
		                }
		                else {
		                    UIControlService.msg_growl('notice', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_AVAILABLE.TITLE');
		                }
		                UIControlService.unloadLoading();
		            } else {
		                UIControlService.msg_growl('error', 'FORM.NPWP_ISUSED', '');
		                UIControlService.unloadLoading();
		                vm.npwp = '';

		            }
		        }, handleRequestError);
		    }
                /*
		    else {
		        UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.NPWP_NOT_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.NPWP_NOT_AVAILABLE.TITLE');
		    }*/
		}

		vm.loadConfigSPPKP = loadConfigSPPKP;
		function loadConfigSPPKP() {
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.ADMINISTRATION.PKP", function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            vm.PkpName = response.data.name;
		            vm.idUploadConfigsPkp = response.data;
		            vm.idFileTypesPkp = generateFilterStrings(response.data);
		            vm.idFileSizePkp = vm.idUploadConfigsPkp[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		        return;
		    });
		}


		vm.pkpUrl = '';
		vm.pkp = '';
		vm.uploadAdministrasiPkp = uploadAdministrasiPkp;
		function uploadAdministrasiPkp(file, config, filters, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
		    UploaderService.uploadRegistration(file, vm.npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            var url = response.data.Url;
		            vm.pathFile1 = url;
		            vm.FileName = response.data.FileName;
		            var s = response.data.FileLength;
		            vm.NpwpUrl = vm.pathFile1;
		            //saveAdministrasi();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (response) {
		        if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
		            UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
		            UIControlService.unloadLoading();
		        }
		    });
		}

		vm.businessReq=null;
		vm.changeBE=changeBE;
		function changeBE() {
		    if (vm.selectedBE != null) {
		        var businessId = vm.selectedBE.BusinessID;
		        VendorRegistrationService.getBusinessReq({ BusinessID: businessId },
                function (response) {
                    vm.businessReq = response.data;
                    console.info("req" + JSON.stringify(vm.businessReq));
                    UIControlService.unloadLoading();
                },
                handleRequestError);
		    }
		    else {
		        vm.businessReq = null;
		    }
		    console.info("be.Req:" + JSON.stringify(vm.businessReq));
		}

		function loadLegal(data) {
		    UIControlService.loadLoading("LOADERS.LOADING");
		    VendorRegistrationService.getLegal(
                function (response) {
                    vm.LegalList = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		function loadConfigLegal() {
		    UploadFileConfigService.getByPageName(VENDOR_LEGAL_NO_PAGE_NAME, function (response) {
		        if (response.status == 200) {
		            vm.siupUploadConfigs = response.data;
		            console.info("siupconfig" + JSON.stringify(vm.siupUploadConfigs));
		            vm.siupFileTypes = generateFilterStrings(response.data);
		            vm.siupFileSize = vm.siupUploadConfigs[0];
		            UIControlService.unloadLoading();
		        } else {
		            handleRequestError(response);
		        }
		    },
            handleRequestError);
		}

		vm.checkLegal = checkLegal;
		function checkLegal() {
		    if (vm.legalNumber == undefined) {
		        UIControlService.msg_growl("error", "FORM.LEGAL");
		        return;
		    } else {
		        UIControlService.loadLoading("Validasi nomor ijin usaha");
		        VendorRegistrationService.checkLegal({ Status: vm.legalNumber, Keyword: vm.legal.ID }, function (response) {
		            vm.legalAvailable = response.data;
		            //console.info(response);
		            if (vm.legalAvailable) {
		                UIControlService.msg_growl('notice', 'FORM.VALIDATION_OK.LEGAL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.LEGAL_AVAILABLE.TITLE');
		                UIControlService.unloadLoading();
		            } else {
		                UIControlService.msg_growl('error', 'FORM.VALIDATION_OK.LEGAL_NOT_AVAILABLE.MESSAGE', 'FORM.VALIDATION_OK.LEGAL_NOT_AVAILABLE.TITLE');
		                UIControlService.unloadLoading();
		                vm.legal = '';
		            }
		        }, handleRequestError);
		    }
		}
		vm.legalUrl = null;
		vm.legalDoc=null;
		vm.selectLegalDoc = selectLegalDoc;
		function selectLegalDoc() {
		    if (vm.legalDoc != null) {
		        uploadSingleDoc_legal(vm.legalDoc, vm.siupFileSize, vm.siupFileTypes,vm.tglSekarang)
		    }
		}
		function uploadSingleDoc_legal(file, config, filters, dates) {
		    //console.info("doctype:" + doctype);
		    //console.info("file"+JSON.stringify(file));
		    var size = config.Size;

		    var unit = config.SizeUnitName;
		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		    }

		    UIControlService.loadLoading("LOADING");
		    UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.legalUrl = url;
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

		}
		vm.currencyList = [];
		vm.currency = null;
		function loadCurrencies() {
		    UIControlService.loadLoading("LOADERS.LOADING_CURRENCY");
		    VendorRegistrationService.getCurrencies(
                function (response) {
                    vm.currencyList = response.data;
                    UIControlService.unloadLoading();
                },
                handleRequestError);
		}

		vm.currencies = [];
		vm.addCurrency = addCurrency;
		function addCurrency() {
		    vm.flagCurrVendor = true;
		    var i = 0;
		    if (vm.currency.Symbol === undefined || vm.currency.Symbol === null) {
		        UIControlService.msg_growl('error', "ERRORS.CURRENCY_NOT_FOUND");
		        return;
		    } else {
		        for (i = 0; i < vm.currencies.length; i++) {
		            if (vm.currencies[i].CurrencyID == vm.currency.CurrencyID) {
		                vm.flagCurrVendor = false;
		                UIControlService.msg_growl('error', "ERRORS.CURRENCY_EXIST");
		                return;
		            }
		        }
		        //console.info(i);
		        if (i == vm.currencies.length && vm.flagCurrVendor === true) {

		            //console.info(vm.currency);
		            var currency = vm.currency;
		            vm.currencies.push(currency);
		        }

		        vm.currency = {};
		    }
		}

		vm.removeCurrency = removeCurrency;
		function removeCurrency(currency) {
		    vm.currencies = remove(vm.currencies, currency);
		}

		function remove(list, obj) {
		    var index = list.indexOf(obj);
		    if (index >= 0) {
		        list.splice(index, 1);
		    } else {
		        UIControlService.msg_growl('error', "ERRORS.OBJECT_NOT_FOUND");
		    }
		    return list;
		}
		vm.emailVendor = null;
		vm.checkEmail = checkEmail;
		function checkEmail(flag) {
		    if (flag == true) {
		        var data = {
		            Keyword: vm.emailVendor
		        };
		    }
		    else {
		        var data = {
		            Keyword: vm.contactemail
		        };
		    }
		    //if (data.Keyword != '' || data.Keyword!=null) {
		        UIControlService.loadLoading("Validasi email");
		        VendorRegistrationService.checkEmail(data,
                    function (response) {
                        UIControlService.unloadLoading();
                        vm.EmailAvailable = (response.data == false || response.data == 'false');
                        if (!vm.EmailAvailable) {
                            UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');
                            if (flag == true) {
                                vm.emailVendor = '';
                            }
                            else {
                                vm.contactemail = '';
                            }
                        }
                    }, handleRequestError);
		    //}
		}

		vm.fax = null;
		vm.phone = null;
		vm.website = null;
		vm.phoneCode = null;
		vm.phoneCodeList = [];
		function loadPhoneCodes() {
		    UIControlService.loadLoading("LOADERS.LOADING_PHONE");
		    VendorRegistrationService.getCountries(
              function (response) {
                  vm.phoneCodeList = response.data;

                  UIControlService.unloadLoading();
              }, handleRequestError);
		}
		vm.contacts = [];
		vm.contact = {};
		vm.contactphone = 0;
		vm.addContact = addContact;
		function addContact() {
		    //checkTelp(false);
		    //if (!validateRequiredField(vm.contactname)) return;
		    //if (!validateRequiredField(vm.contact.phone)) return;
		    //if (!validateRequiredField(vm.contact.countryCode)) return;
		    vm.contact = {
		        Phone:  vm.contactphone,
		        Email: vm.contactemail,
                Name:vm.contactname
		    };
		    vm.contacts.push(vm.contact);
		    vm.contact = {};
		    vm.vendorContactForm.$setPristine();
		}

		vm.removeContact = removeContact;
		function removeContact(obj) {
		    vm.contacts = remove(vm.contacts, obj);
		}


		vm.checkTelp = checkTelp;
		function checkTelp(flag) {
		    if (flag == true) {
		        if (vm.phone.length > 20) {
		            UIControlService.msg_growl("error", "ERRORS.MAKS_TELP");
		            return;
		        }
		    } else {
		        if (vm.contactphone.length > 20) {
		            vm.contactphone = "";
		            UIControlService.msg_growl("error", "ERRORS.MAKS_TELP");
		            return;
		        }
		    }
		}

		function validateRequiredField(value) {
		    if (!value) {
		        UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.REQUIRED.MESSAGE", "FORM.VALIDATION_ERRORS.REQUIRED.TITLE");
		        UIControlService.unloadLoading();
		        return false;
		    }
		    return true;
		}

		function loadOfficeType() {
		    UIControlService.loadLoading("LOADERS.LOADING_OFFICE_TYPE");
		    VendorRegistrationService.getOfficeType(
                function (response) {
                    if (response.status == 200) {
                        vm.officeTypes = response.data;
                    } else {
                        handleRequestError(response);
                    }
                },
                handleRequestError);
		}


		function reloadCountries() {
		    UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
		    VendorRegistrationService.getCountries(
                function (response) {
                    vm.addresscountryList = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.reloadStates = reloadStates;
		function reloadStates(country) {
		    if (!country)
		        country = vm.address.Country;
		    vm.address.State = "";
		    vm.address.City = "";
		    vm.address.District = "";
		    reloadRegions(country.CountryID);
		    UIControlService.loadLoading("LOADERS.LOADING_STATE");
		    VendorRegistrationService.getStates(country.CountryID,
                function (response) {
                    vm.addressStateList = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.reloadAddressCity = reloadAddressCity;
		function reloadAddressCity(state) {
		    if (!state)
		        state = vm.address.State;
		    vm.address.City = "";
		    vm.address.District = "";
		    UIControlService.loadLoading("LOADERS.LOADING_CITY");
		    VendorRegistrationService.getCities(state.StateID,
                function (response) {
                    vm.addressCityList = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.reloadAddressDistrict = reloadAddressDistrict;
		function reloadAddressDistrict(city) {
		    if (!city)
		        city = vm.address.City;
		    vm.address.District = "";
		    UIControlService.loadLoading("LOADERS.LOADING_DISTRICT");
		    VendorRegistrationService.getDistricts(city.CityID,
                function (response) {
                    vm.addressDistrictList = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.addAddress = addAddress;
		function addAddress() {
		    var address = vm.address;

		    if (!address.PostalCode)
		        address.PostalCode = "";

		    address.Info = "";
		    if (address.City) {
		        address.Info += " " + address.City.Name;
		    }

		    address.Info += " " + address.State.Name + " " + address.State.Country.Name;

		    if (address.Type.Name == VENDOR_OFFICE_TYPE_MAIN) {
		        if (hasMainOffice()) {
		            UIControlService.msg_growl("error", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_EXIST.MESSAGE", "FORM.VALIDATION_ERRORS.MAIN_OFFICE_EXIST.TITLE");
		            return;
		        }
		    }
		    vm.addresses.push(address);
		    //console.info("alamats" + JSON.stringify(vm.addresses));
		    vm.address = {};
		    vm.vendorAddressForm.$setPristine();
		}

		function hasMainOffice() {
		    for (var i = 0; i < vm.addresses.length; i++) {
		        if (vm.addresses[i].Type.Name == VENDOR_OFFICE_TYPE_MAIN) {
		            return true;
		        }
		    }
		    return false;
		}

		vm.reloadRegions = reloadRegions;
		function reloadRegions(data) {
		    UIControlService.loadLoading("LOADERS.LOADING_REGION");
		    VendorRegistrationService.getRegions({ CountryID: data },
                function (response) {
                    vm.address.Region = response.data;
                    UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.removeAddress = removeAddress;
		function removeAddress(address) {
		    vm.addresses = remove(vm.addresses, address);
		}


		function loadOptionStock() {
		    VendorRegistrationService.getStockOption(function (response) {
		        vm.stockOption = response.data;
		        UIControlService.unloadLoadingModal();
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		vm.labelChange = labelChange;
		function labelChange() {
		    if (vm.stockOpt.Name == 'STOCK_PERSONAL') {
		        //PERSONAL
		        vm.nama = "NM_PERSONAL";
		        vm.tgl = "TGL_PERSONAL";
		        vm.identitas = "NO_PERSONAL";
		    } else if (vm.stockOpt.Name == 'STOCK_COMPANY') {
		        //COMPANY
		        vm.nama = "NM_COMPANY";
		        vm.tgl = "TGL_COMPANY";
		        vm.identitas = "NO_COMPANY";
		    }

		}

		vm.stockForm;
		vm.stock = {
		    Quantity: 0,
		    Unit: {},
		    Type: {},
		    OwnerName: "",
		    OwnerDOB: "",
		    OwnerID: "",
		    OwnerIDUrl: "",
		    UnitCurrencyID: null,
		    CurrencySymbol:"",
		    Position: "",
		    Tmpblacklist: false
		};

		vm.calculateAge = calculateAge;
		function calculateAge(birthday) {
		    if (birthday !== '') {
		        var ageDifMs = Date.now() - birthday.getTime();
		        var ageDate = new Date(ageDifMs); // miliseconds from epoch
		        var age = Math.abs(ageDate.getUTCFullYear() - 1970);
		        if (age < 17) {
		            vm.stock.OwnerDOB = undefined;
		            UIControlService.msg_growl("error", "FORM.AGE");
		            return;
		        }
		    }
		}

		vm.openDobCalendar = openDobCalendar;
		function openDobCalendar() {
		    vm.isDobCalendarOpened = true;
		}

		function stockOwnerIDSelected() {
		   // vm.stock.OwnerID_doc = vm.stockOwnerID;
		}

		function loadConfigStock() {
		    UploadFileConfigService.getByPageName(STOCK_OWNER_ID_PAGE_NAME, function (response) {
		        if (response.status == 200) {
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];
		            UIControlService.unloadLoading();
		        } else {
		            handleRequestError(response);
		        }
		    },
            handleRequestError);
		}

		function loadStockUnits() {
		    //UIControlService.loadLoading("LOADERS.LOADING_STOCK_TYPE");
		    VendorRegistrationService.getStockTypes(
                function (response) {
                    vm.stockUnits = response.data;

                    UIControlService.unloadLoading();
                },
                handleRequestError);
		}

		vm.addStockData = addStockData;
		function addStockData() {
		    var sameId = false;
		    for (var i = 0; i < vm.stocks.length; i++) {
		        if (vm.stock.OwnerID === vm.stocks[i].OwnerID) {
		            sameId = true;
		            i = vm.stocks.length;
		        }
		    }
		    if (sameId === true) {
		        UIControlService.msg_growl('error', "FORM.SAME_ID_STOCK");
		        return;
		    }
		    if (vm.stock.Quantity <= 0) {
		        UIControlService.msg_growl('error', "FORM.NOTNULL");
		        return;
		    }
		    if (vm.stockUnit.Name === "STOCK_UNIT_PERCENTAGE") {
		        if (vm.stock.Quantity > 100) {
		            UIControlService.msg_growl('error', "FORM.MAX_PERSEN");
		            return;
		        }
		        else {
		            var jumlah = 0;
		            for (var i = 0; i < vm.stocks.length; i++) {
		                jumlah += +vm.stocks[i].quantity;
		                if (i == (vm.stocks.length - 1)) {
		                    if ((+vm.stock.Quantity + +jumlah).toFixed(2) > 100) {
		                        UIControlService.msg_growl('error', "FORM.MAX_PERSEN");
		                        return;
		                    }
		                }
		            }
		        }
		    }
		    //console.info("vm.stocks" + JSON.stringify(vm.stock));
		    if (vm.stockForm.$invalid) return;

		    cekOwnerID();

		    VendorRegistrationService.isAnotherStockHolder({
		        VendorID: 0,
		        OwnerID: vm.stock.OwnerID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200 && reply.data === true) {
		            UIControlService.msg_growl('error', "ERRORS.IS_ANOTHER_STOCKHOLDER");
		        } else {
		            if (validateFileType(vm.stockOwnerID, vm.idUploadConfigs)) {
		                vm.idFileSize.Prefix = vm.prefixes.UPLOAD_PREFIX_ID.Value;
		                uploadStock(0, vm.stock.OwnerID, vm.stockOwnerID, vm.idFileSize, vm.idFileTypes, addStockToList);
		            }
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_VERIFY_STOCKHOLDER");
		        if (validateFileType(vm.stockOwnerID, vm.idUploadConfigs)) {
		            vm.idFileSize.Prefix = vm.prefixes.UPLOAD_PREFIX_ID.Value;
		            uploadStock(0, vm.stock.OwnerID, vm.stockOwnerID, vm.idFileSize, vm.idFileTypes, addStockToList);
		        }
		    });
		}

		function validateFileType(file, allowedFileTypes) {
		    if (!file || file.length == 0) {
		        UIControlService.handleRequestError("ERRORS.INVALID_FILETYPE!");
		        return false;
		    }

		    var selectedFileType = file[0].type;
		    selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
		    var allowed = false;
		    for (var i = 0; i < allowedFileTypes.length; i++) {
		        if (allowedFileTypes[i].Name == selectedFileType) {
		            allowed = true;
		            return allowed;
		        }
		    }

		    if (!allowed) {
		        UIControlService.handleRequestError("ERRORS.INVALID_FILETYPE!");
		        return false;
		    }
		}

		function uploadStock(id, prefix, file, config, filters, callback) {
		    //console.info(file);
		    var size = config.Size;
		    var unit = config.SizeUnitName;
		    if (unit == SIZE_UNIT_KB) {
		        size *= 1024;
		    }

		    if (unit == SIZE_UNIT_MB) {
		        size *= (1024 * 1024);
		    }

		    UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
		    UploaderService.uploadSingleFileStock(id, prefix, file, size, filters,
                function (response) {
                    if (response.status == 200) {
                        var url = response.data.Url;
                        UIControlService.unloadLoading();
                        callback(response.data);
                    } else {
                        UIControlService.handleRequestError("ERR_UPLOAD_NPWP");
                        UIControlService.unloadLoading();
                    }
                },
                function (response) {
                    UIControlService.handleRequestError("ERR_UPLOAD_NPWP");
                    UIControlService.unloadLoading();
                });

		}

		vm.addStockToList = addStockToList;
		function addStockToList(obj) {
		    vm.stock.OwnerIDUrl = obj.Url;
		    //vm.stock.OwnerIDFilename = obj.FileName;
		    vm.stock.Unit= vm.stockUnit;
		    vm.stock.Type = vm.stockOpt;
		    vm.stock.Position = obj.Position;
		    
		    if (vm.stockUnit.Value === 'STOCK_UNIT_CURRENCY') {
		        vm.stock.CurrencySymbol = vm.stockUnitCurrency.Symbol;
		        vm.stock.UnitCurrencyID = vm.stockUnitCurrency.CurrencyID;
		    }
		    vm.flag = 1;
		    //vm.stock.currency = vm.stockUnitCurrency;
		    vm.stock.Tmpblacklist = vm.Tmpblacklist;
		    vm.stocks.push(vm.stock);
		    vm.stock = {
		        Quantity: 0,
		        Unit: {},
		        Type: {},
		        OwnerName: "",
		        OwnerDOB: "",
		        OwnerID: "",
		        OwnerIDUrl: "",
		        UnitCurrencyID: null,
		        CurrencySymbol: "",
		        Position: "",
		        Tmpblacklist: false
		    };
		    //vm.stockUnit = "";
		    //vm.stockUnitCurrency = "";
		    vm.stockForm.$setPristine();
		}

		vm.removeStock = removeStock;
		function removeStock(obj) {
		    vm.stocks = remove(vm.stocks, obj);
		    if (vm.stocks.length === 0) {
		        vm.stockUnit = {};
		        vm.flag = 0;
		    }
		}


		function cekOwnerID() {
		    BlacklistService.cek({
		        Status: vm.stock.OwnerID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200 && reply.data.length > 0) {
		            vm.Tmpblacklist = true;
		        }
		        else {
		            vm.Tmpblacklist = false;

		        }
		    }, function (err) {

		        UIControlService.msg_growl("error", "ERRORS.API");
		        UIControlService.unloadLoading();
		    });
		}


		vm.save = save;
		function save() {
		    var valid = true;
		    var legalType = null;
		    if (vm.legal != null) {
		        legalType = vm.legal.ID;
		    }
		    var beId = null;
		    if (vm.selectedBE != null) {
		        beId = vm.selectedBE.BusinessID;
		    }
		    if (vm.selectedCountry == null || vm.selectedState==null) {
		        UIControlService.msg_growl("error", "Negara dan Provinsi harus diisi");
		        valid = false;
		        return;
		    }
		    if (vm.Username == '' || vm.Password == '') {
		        UIControlService.msg_growl("error", "Username dan Password harus diisi");
		        valid = false;
		        return;
		    }
		    if (vm.addresses.length == 0) {
		        UIControlService.msg_growl("error", "Alamat Perusahaan harus diisi");
		        valid = false;
		        return;
		    }
		    if (vm.vendorName=='') {
		        UIControlService.msg_growl("error", "Nama Perusahaan harus diisi");
		        valid = false;
		        return;
		    }
		    var phone = vm.phone;
		    if (vm.phoneCode != null) {
		        phone = '(' + vm.phoneCode.PhonePrefix + ')' + vm.phone;
		    }
            
		    if (valid == true) {
		        vm.Vendor = {
		            Name: vm.vendorName,
		            Email: vm.emailVendor,
		            Fax: vm.fax,
		            Phone: phone,
		            Website: vm.website,
		            SAPCode: vm.vendorSAPCode,
		            Business: vm.selectedBE,
		            FoundedDate: vm.foundedDate,
		            Npwp: vm.npwp,
		            NpwpUrl: vm.NpwpUrl,
		            LegalNumberTypeId: legalType,
		            Siup: vm.legalNumber,
		            SiupUrl: vm.legalUrl,
		            currencies: vm.currencies,
		            Stocks: vm.stocks,
		            Contacts: vm.contacts,
		            Addresses: vm.addresses,
		            Username: vm.Username,
		            Password: vm.Password,
		            Region: vm.region,
		            Country: vm.selectedCountry,
		            City: vm.selectedCity,
		            State: vm.selectedState,
		            District: vm.selectedDistrict
		        };


		        UIControlService.loadLoading("Menyimpan data...");
		        VerifikasiDataService.registerVendorManual(vm.Vendor,
                    function (response) {
                        UIControlService.unloadLoading();
                        console.info("response:" + response.status);
                        if (response.status == 200) {
                            UIControlService.msg_growl("success", "Data berhasil disimpan");
                            $state.transitionTo('verifikasi-data');
                        }

                    }, handleRequestError);
		    }
		}

		vm.back = back;
		function back(){
            $state.transitionTo('verifikasi-data');
		}
	}
})();
