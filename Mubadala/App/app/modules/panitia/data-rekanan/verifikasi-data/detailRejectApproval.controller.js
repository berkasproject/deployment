﻿(function () {
    'use strict';

    angular.module("app").controller("DetailRejectApproval", ctrl);
    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VerifikasiDataService', 'UIControlService','$uibModalInstance', '$uibModal', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        VerifikasiDataService, UIControlService, $uibModalInstance, $uibModal, GlobalConstantService) {
        var vm = this;
        
        vm.init = init;
        vm.aktifasi = "";
        vm.pageSize = 10;
        vm.Keyword = "";
        vm.Description = "";

        function init() {
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        vm.reject = reject;
        function reject() {
            if (vm.Description == "") {
                UIControlService.msg_growl("error", "MESSAGE.NO_REASON");
                return;
            }
            else  $uibModalInstance.close(vm.Description);
        }
    }
})();
