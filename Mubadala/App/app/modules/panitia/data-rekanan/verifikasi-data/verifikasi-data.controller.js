(function () {
    'use strict';

    angular.module("app").controller("VerifikasiDataCtrl", ctrl);
    ctrl.$inject = ['$http',  '$translate', '$translatePartialLoader', '$location', 'SocketService', '$stateParams',
        'VerifikasiDataService', 'UIControlService', '$uibModal', '$state', 'GlobalConstantService', 'Excel', '$timeout', '$filter','$rootScope','$q','AuthService'];
    /* @ngInject */
    function ctrl($http,  $translate, $translatePartialLoader, $location, SocketService, $stateParams,
        VerifikasiDataService, UIControlService, $uibModal, $state, GlobalConstantService, Excel, $timeout,$filter,$rootScope,$q,AuthService) {
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        var vm = this;
        vm.datarekanan = [];
        vm.status = Number($stateParams.status);

        vm.startDate = new Date($stateParams.startDate);
        vm.currentPage = 1;
        vm.fullSize = 10;
        vm.offset = (vm.currentPage * 10) - 10;
        vm.totalRecords = 0;
        vm.nCompany = '';
        vm.activator;
        vm.verificator;
        vm.menuhome = 0;
        vm.cmbStatus = 100;
        vm.rekanan_id = '';
        vm.flag = 0;
        var apipoint = GlobalConstantService.getConstant("api_endpoint");
        vm.date = "";
        vm.year = "";
        vm.datemonth = "";
        //vm.loadingExport = false;
        vm.waktuMulai1 = (vm.year - 1) + '-' + vm.datemonth;
        vm.waktuMulai2 = vm.date;

        vm.sStatus = -1;
        vm.thisPage = 12;
        vm.verificationPage = 130;
        vm.verifikasi = {};
        vm.isCalendarOpened = [false, false, false, false];
        //functions
        vm.init = init;
        vm.jLoad = jLoad;
        vm.openCalendar = openCalendar;
        vm.show = show;
        vm.add = add;
        vm.addVerifikasi = addVerifikasi;
        vm.listExpired = listExpired;
        vm.flagButton = true;
        vm.ViewModelVerified = {};
        vm.currentPage = 1;
        function init() {
            vm.listDropdown =
            [
                { Value: 0, Name: "SELECT.ALL" },
                { Value: 1, Name: "SELECT.NOT_ACTIVED" },
                { Value: 2, Name: "SELECT.ACTIVED" },
                { Value: 6, Name: "SELECT.END_ACTIVED" },
                { Value: 3, Name: "SELECT.NOT_VERIFIED" },
                { Value: 4, Name: "SELECT.VERIFIED" },
                { Value: 7, Name: "SELECT.APPROVAL_PROCCESS" },
                { Value: 9, Name: "SELECT.REJECTED_APPROVAL" },
                { Value: 8, Name: "SELECT.END_VERIFIED" },
                { Value: 10, Name: "SELECT.NOT_ACTIVE" },
                
            ]

            vm.listDropdown2 =
            [
                { Value: 0, Name: "SELECT.ALL" },
                { Value: 1, Name: "SELECT.QUEST_SENT" },
                { Value: 2, Name: "SELECT.NEED_QUEST" },
                { Value: 6, Name: "SELECT.NEED_REVIEW" },
                { Value: 3, Name: "SELECT.ON_PROCESS" },
                { Value: 4, Name: "SELECT.APPROVED" },
                { Value: 5, Name: "SELECT.REJECTED" },

            ]

            if (vm.status !== 0) {
                showFilteredData();
            }
            var getItemLocal = JSON.parse(localStorage.getItem('ViewModelVerified'));
            if (getItemLocal != null) {
                if (getItemLocal.Status != undefined) {
                    for (var i = 0; i < vm.listDropdown.length; i++) {
                        if (getItemLocal.Status.Value == vm.listDropdown[i].Value) {
                            vm.Status = vm.listDropdown[i];
                            vm.cmbStatus = getItemLocal.Status.Value;
                        }
                    }
                }
                if (getItemLocal.StartDate) vm.verifikasi.StartDate = new Date(Date.parse(getItemLocal.StartDate));
                if (getItemLocal.EndDate) vm.verifikasi.EndDate = new Date(Date.parse(getItemLocal.StartDate));
                if (getItemLocal.Keyword != "") vm.nCompany = getItemLocal.Keyword;
                if (vm.currentPage) vm.currentPage = getItemLocal.currentPage;
                localStorage.removeItem('ViewModelVerified');
            }
            $translatePartialLoader.addPart('verifikasi-data');
            jLoad(vm.currentPage);
            getSysAccess();
            convertToDate();
            getUserLogin();
        };

        function showFilteredData() {
            vm.cmbStatus = vm.status;
            for (var i = 0; i < vm.listDropdown.length; i++) {
                if (vm.cmbStatus == vm.listDropdown[i].Value) {
                    vm.Status = vm.listDropdown[i];
                    i = vm.listDropdown.length;
                }
            }
            if (vm.cmbStatus !== 3) {
                vm.verifikasi.StartDate = new Date();
                vm.verifikasi.StartDate.setDate(1);
                vm.currentMonth = new Date().getMonth() + 1;
                vm.currentYear = new Date().getFullYear();
                vm.lastDay = getDaysInMonth(vm.currentMonth, vm.currentYear);
                vm.verifikasi.EndDate = new Date();
                vm.verifikasi.EndDate.setDate(vm.lastDay);
                //console.info("startdate" + vm.verifikasi.StartDate);
                //console.info("endate" + vm.verifikasi.EndDate);
                jLoad(1);
            }
        }


        function exportToExcel() { // ex: '#my-table'
            var exportHref = Excel.tableToExcel('#print', 'data rekanann');
            $timeout(function () { location.href = exportHref; }, 100); // trigger download
        }

        function getDaysInMonth(m, y) {
            return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
        }

        vm.getUserLogin = getUserLogin;
        function getUserLogin() {
            VerifikasiDataService.getUserLogin(function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] == 'LO' || data[i] === 'LA' || data[i] === 'LT' ||  data[i] === 'L1') {
                            vm.flagButton = false;
                            break;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data " });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.verifyEndDate = verifyEndDate;
        function verifyEndDate(selectedEndDate,selectedStartDate) {
            var convertedEndDate = UIControlService.getStrDate(selectedEndDate);
            var convertedStartDate = UIControlService.getStrDate(selectedStartDate);
            //console.info("selected end date" + JSON.stringify(convertedEndDate));
            //console.info("selected start date" + JSON.stringify(convertedStartDate));
            if (convertedEndDate < convertedStartDate) {
                UIControlService.msg_growl("warning", "MESSAGE.TGL_BATAS_AKHIR");
                vm.verifikasi.EndDate = " ";
            }
            //else {
              //  console.info("masak");
            //}
        }

        function show() {
            //console.info(vm.Status);
            //console.info("startdate" + vm.verifikasi.StartDate);
            //console.info("enddate" + vm.verifikasi.EndDate);
            if (vm.verifikasi.StartDate === undefined && vm.verifikasi.EndDate !== undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_DATE1");
                return;
            }
            if (vm.verifikasi.EndDate === undefined && vm.verifikasi.StartDate !== undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_DATE2");
                return;
            }
            if (vm.Status === undefined) {
                vm.cmbStatus = 0;
            }
            else {
                vm.cmbStatus = vm.Status.Value;
            }
            jLoad(1);
        }

        function jLoad(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.verifikasidata = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            var status2 = 0;
            if (vm.statusApprv != undefined) {
                vm.status2 = vm.statusApprv.Value;
            }
            

            VerifikasiDataService.all({
                Offset: offset,
                Limit: vm.fullSize,
                Keyword: vm.nCompany,
                Status: vm.cmbStatus,
                Date1: UIControlService.getStrDate(vm.verifikasi.StartDate),
                Date2: UIControlService.getStrDate(vm.verifikasi.EndDate),
                FilterType:vm.status2
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.verifikasidata = data.List;
                    if (vm.verifikasidata.length > 0) {
                        for (var i = 0; i < vm.verifikasidata.length; i++) {
                            vm.vendorID = vm.verifikasidata[i].VendorID;
                            vm.IsViewDetail = vm.verifikasidata[i].IsViewDetail;
                            
                            loadContact(vm.vendorID, i);
                        }
                        
                    }
                    vm.totalItems = Number(data.Count);
                    vm.flag = vm.cmbStatus;
                    UIControlService.unloadLoading();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Rekanan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.export = [];
        vm.getDataVendor2 = getDataVendor2;
        function getDataVendor2() {
            var swal = {
                titleNotification:  $filter('translate')('SWAL.NOTIFICATION'),
                text: $filter('translate')('SWAL.TEXT')
            }
            UIControlService.loadLoadingExport("MESSAGE.RUN_IN_BACKGROUND");
            $rootScope.loadingExport = true;
            $rootScope.bellCount += 1;
            vm.export = [];
            Swal.fire({
                title: swal.titleNotification,
                icon: 'info',
                text: swal.text
            })
            VerifikasiDataService.exportDataVendor2({
                Keyword: vm.nCompany,
                Status: vm.cmbStatus,
                Date1: UIControlService.getStrDate(vm.verifikasi.StartDate),
                Date2: UIControlService.getStrDate(vm.verifikasi.EndDate)
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.export = data;
                    console.info("export:" + JSON.stringify(vm.export));
                    vm.filterdata = [];
                    //console.info("total semua data:" + JSON.stringify(vm.alldata.length));
                    
                    var no = 0;
                    var indexF = 0;

                    for (var i = 0; i <= vm.export.length - 1; i++) {
                        //UIControlService.loadLoading("MESSAGE.LOADING");
                        var bidang = '';
                        var vendorSap = '';

                        //area,email,telp
                        var email = '';
                        var telp = '';
                        var area = '';
                        if (vm.export[i].Contacts != null) {
                            for (var a = 0; a <= vm.export[i].Contacts.length - 1; a++) {
                                if (vm.export[i].Contacts[a].VendorContactType.Name == 'VENDOR_OFFICE_TYPE_MAIN' && vm.export[i].Contacts[a].IsActive==true) {
                                    area = vm.export[i].Contacts[a].Contact.Address.AddressDetail;
                                }

                                if (vm.export[i].Contacts[a].VendorContactType.Name == 'VENDOR_CONTACT_TYPE_COMPANY' && vm.export[i].Contacts[a].IsActive == true) {
                                    if (vm.export[i].Contacts[a].Contact.Email != null) {
                                        email = vm.export[i].Contacts[a].Contact.Email;
                                    }
                                    telp = vm.export[i].Contacts[a].Contact.Phone;
                                }
                            }
                        }

                        //asosiasi
                        var asosiasi = '';
                        if (vm.export[i].AssociationDetail != null) {
                            asosiasi = vm.export[i].AssociationDetail.AssosiationName;
                        }

                        //tipeVendor
                        if (vm.export[i].VendorTypeID == 3090) {
                            bidang = 'Barang';
                        }
                        else if (vm.export[i].VendorTypeID == 3091) {
                            bidang = 'Jasa';
                        }
                        else if (vm.export[i].VendorTypeID == 3092) {
                            bidang = 'Barang & Jasa';
                        }

                        //if (vm.export[i].SAPCode != null) {
                        //    vendorSap = vm.export[i].SAPCode;
                        //}

                        if (telp == null) {
                            telp = '';
                        }


                        if (vm.export[i].commodity != null && vm.export[i].VendorLicense != null) {
                            if (vm.export[i].VendorLicense.length >= vm.export[i].commodity.length) {
                                for (var f = 0; f < vm.export[i].VendorLicense.length; f++) {

                                    var LicenseName = vm.export[i].VendorLicense[f].LicenseName;
                                    var LicenseNo = vm.export[i].VendorLicense[f].LicenseNo;

                                    if ((f + 1) <= vm.export[i].commodity.length) {
                                        var BussinesFieldName = vm.export[i].commodity[f].BusinessField.Name == null ? '-' : vm.export[i].commodity[f].BusinessField.Name;
                                    } else {
                                        var BussinesFieldName = '';
                                        
                                    }

                                    var vendorNameVar = "";
                                    var BusinessNameVar = "";
                                    var statusVendVar = 10;
                                    var NpwpVar = "";
                                    var codeVar = "";
                                    var EmailVar = "";
                                    var AreaVar = "";
                                    var BidangVar = "";
                                    var CreatedByVar = "";
                                    var ActivedByVar = "";
                                    var VerifiedByVar = "";
                                    var NonActivedDateVar = "";
                                    var VerifiedDateNameVar = "";

                                    if (indexF == 0) {
                                        no = no + 1;

                                        vendorNameVar = vm.export[i].VendorName;
                                        BusinessNameVar = vm.export[i].businessName;
                                        statusVendVar = vm.export[i].StatusVend;
                                        NpwpVar = vm.export[i].Npwp;
                                        EmailVar = email;
                                        AreaVar = area;
                                        BidangVar = bidang;
                                        CreatedByVar = vm.export[i].CreatedDate == null ? '-' : UIControlService.convertDate(vm.export[i].CreatedDate);
                                        ActivedByVar = vm.export[i].ActivedDate == null ? '-' : UIControlService.convertDate(vm.export[i].ActivedDate);
                                        VerifiedByVar = vm.export[i].VerifiedSendDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedSendDate)
                                        NonActivedDateVar = vm.export[i].LastInactiveApprovalDate == null ? '-' : UIControlService.convertDate(vm.export[i].LastInactiveApprovalDate)
                                        VerifiedDateNameVar = vm.export[i].VerifiedDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedDate);

                                    } else if (vm.filterdata[(indexF - 1)].VendorID != vm.export[i].VendorID) {
                                        no = no + 1;

                                        vendorNameVar = vm.export[i].VendorName;
                                        BusinessNameVar = vm.export[i].businessName;
                                        statusVendVar = vm.export[i].StatusVend;
                                        NpwpVar = vm.export[i].Npwp;
                                        codeVar = vm.export[i].Code;
                                        EmailVar = email;
                                        AreaVar = area;
                                        BidangVar = bidang;
                                        CreatedByVar = vm.export[i].CreatedDate == null ? '-' : UIControlService.convertDate(vm.export[i].CreatedDate);
                                        ActivedByVar = vm.export[i].ActivedDate == null ? '-' : UIControlService.convertDate(vm.export[i].ActivedDate);
                                        VerifiedByVar = vm.export[i].VerifiedSendDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedSendDate)
                                        NonActivedDateVar = vm.export[i].LastInactiveApprovalDate == null ? '-' : UIControlService.convertDate(vm.export[i].LastInactiveApprovalDate)
                                        VerifiedDateNameVar = vm.export[i].VerifiedDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedDate);
                                       

                                    }

                                    var forExcel = {
                                        No: no,
                                        Bidang: BidangVar,
                                        VendorCode: vendorSap,
                                        Area: AreaVar,
                                        VendorID: vm.export[i].VendorID,
                                        Entity: BusinessNameVar,
                                        VendorName: vendorNameVar,
                                        Email: EmailVar,
                                        StatusVend: statusVendVar,
                                        Npwp: NpwpVar,
                                        Code: codeVar,
                                        CreatedBy: CreatedByVar,
                                        ActivedBy: ActivedByVar,
                                        VerifiedBy: VerifiedByVar,
                                        NonActivedDate: NonActivedDateVar,
                                        No_Telp: telp.toString(),
                                        Asosiasi: asosiasi,
                                        LicenseName: LicenseName,
                                        LicenseNo: LicenseNo == null ? '-' : LicenseNo,
                                        BusinessFieldName: BussinesFieldName,
                                        VerifiedDateName: VerifiedDateNameVar
                                    }

                                    vm.filterdata.push(forExcel);
                                    indexF = indexF + 1;
                                }
                            }


                            else {
                                for (var f = 0; f <= vm.export[i].commodity.length - 1; f++) {

                                    if ((f + 1) <= vm.export[i].VendorLicense.length) {
                                        var LicenseName = vm.export[i].VendorLicense[f].LicenseName;
                                        var LicenseNo = vm.export[i].VendorLicense[f].LicenseNo;
                                    } else {
                                        var LicenseName = "";
                                        var LicenseNo = "";
                                    }


                                    var vendorNameVar = "";
                                    var BusinessNameVar = "";
                                    var statusVendVar = 10;
                                    var NpwpVar = "";
                                    var codeVar = "";
                                    var EmailVar = "";
                                    var AreaVar = "";
                                    var BidangVar = "";
                                    var CreatedByVar = "";
                                    var ActivedByVar = "";
                                    var VerifiedByVar = "";
                                    var NonActivedDateVar = "";
                                    var VerifiedDateNameVar = "";

                                    if (indexF == 0) {
                                        no = no + 1;
                                        vendorNameVar = vm.export[i].VendorName;
                                        BusinessNameVar = vm.export[i].businessName;
                                        statusVendVar = vm.export[i].StatusVend;
                                        NpwpVar = vm.export[i].Npwp;
                                        EmailVar = email;
                                        AreaVar = area;
                                        BidangVar = bidang;
                                        CreatedByVar = vm.export[i].CreatedDate == null ? '-' : UIControlService.convertDate(vm.export[i].CreatedDate);
                                        ActivedByVar = vm.export[i].ActivedDate == null ? '-' : UIControlService.convertDate(vm.export[i].ActivedDate);
                                        VerifiedByVar = vm.export[i].VerifiedSendDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedSendDate)
                                        NonActivedDateVar = vm.export[i].LastInactiveApprovalDate == null ? '-' : UIControlService.convertDate(vm.export[i].LastInactiveApprovalDate)
                                        VerifiedDateNameVar = vm.export[i].VerifiedDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedDate);

                                    } else if (vm.filterdata[(indexF - 1)].VendorID != vm.export[i].VendorID) {
                                        no = no + 1;
                                        vendorNameVar = vm.export[i].VendorName;
                                        BusinessNameVar = vm.export[i].businessName;
                                        statusVendVar = vm.export[i].StatusVend;
                                        NpwpVar = vm.export[i].Npwp;
                                        codeVar = vm.export[i].Code;
                                        EmailVar = email;
                                        AreaVar = area;
                                        BidangVar = bidang;
                                        CreatedByVar = vm.export[i].CreatedDate == null ? '-' : UIControlService.convertDate(vm.export[i].CreatedDate);
                                        ActivedByVar = vm.export[i].ActivedDate == null ? '-' : UIControlService.convertDate(vm.export[i].ActivedDate);
                                        VerifiedByVar = vm.export[i].VerifiedSendDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedSendDate)
                                        NonActivedDateVar = vm.export[i].LastInactiveApprovalDate == null ? '-' : UIControlService.convertDate(vm.export[i].LastInactiveApprovalDate)
                                        VerifiedDateNameVar = vm.export[i].VerifiedDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedDate);


                                    }

                                    var forExcel = {
                                        No: no,
                                        Bidang: BidangVar,
                                        VendorCode: vendorSap,
                                        Area: AreaVar,
                                        VendorID: vm.export[i].VendorID,
                                        Entity: BusinessNameVar,
                                        VendorName: vendorNameVar,
                                        Email: EmailVar,
                                        StatusVend: statusVendVar,
                                        Npwp: NpwpVar,
                                        Code: codeVar,
                                        CreatedBy: CreatedByVar,
                                        ActivedBy: ActivedByVar,
                                        VerifiedBy: VerifiedByVar,
                                        NonActivedDate: NonActivedDateVar,
                                        No_Telp: telp.toString(),
                                        Asosiasi: asosiasi,
                                        LicenseName: LicenseName,
                                        LicenseNo: LicenseNo == null ? '-' : LicenseNo,
                                        BusinessFieldName: BussinesFieldName,
                                        VerifiedDateName: VerifiedDateNameVar
                                    }

                                    vm.filterdata.push(forExcel);
                                    indexF = indexF + 1;
                                }

                            }
                        }
                        else {
                            no = no + 1;

                            var forExcel = {
                                No: no,
                                Bidang: bidang,
                                VendorCode: vendorSap,
                                Area: area,
                                VendorID: vm.export[i].VendorID,
                                Entity: vm.export[i].businessName,
                                VendorName: vm.export[i].VendorName,
                                Email: email,
                                StatusVend: vm.export[i].StatusVend,
                                Npwp: vm.export[i].Npwp,
                                Code: vm.export[i].Code,
                                CreatedBy: vm.export[i].CreatedDate == null ? '-' : UIControlService.convertDate(vm.export[i].CreatedDate),
                                ActivedBy: vm.export[i].ActivedDate == null ? '-' : UIControlService.convertDate(vm.export[i].ActivedDate),
                                VerifiedBy: vm.export[i].VerifiedSendDate == null ? '-' : UIControlService.convertDate(vm.export[i].VerifiedSendDate),
                                NonActivedDate: vm.export[i].LastInactiveApprovalDate == null ? '-' : UIControlService.convertDate(vm.export[i].LastInactiveApprovalDate),
                                No_Telp: telp.toString(),
                                Asosiasi: asosiasi,
                                LicenseName: "-",
                                LicenseNo: "-",
                                BusinessFieldName: "-",
                                VerifiedDateName: '-'

                            }

                            vm.filterdata.push(forExcel);
                            indexF = indexF + 1;

                        }

                    }

                    

                    
                    
                    console.log(vm.filterdata);
                    exportDataVendor(vm.filterdata);
           
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data export data vendor" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.exportDataVendor = exportDataVendor;
        function exportDataVendor(filterData) {
            //UIControlService.loadLoading();

            var headers = {};
            headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

            $http({
                method: 'POST',
                url: endpoint + '/verifiedvendor/exportExcel',
                headers: headers,
                data:filterData,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                var linkElement = document.createElement('a');
                var fileName = "DataPBJ_" + UIControlService.getDateNow("") + ".xlsx";

                try {
                    var blob = new Blob([data], { type: headers('content-type') });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute('download', fileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);
                } catch (e) {
                    console.log(e);
                }
                $rootScope.bellCount -= 1;
                UIControlService.unLoadLoadingExport();
                //var swal = {
                //    titleNotification: $filter('translate')('SWAL.EXPORTDONETITLE'),
                //    text: $filter('translate')('SWAL.TEXTEXPORTDONE')
                //}
                //Swal.fire({
                //    title: swal.titleNotification,
                //    icon: 'success',
                //    text: swal.text
                //})
                $rootScope.loadingExport = false;
            });

            
        }

        vm.isSendQuest = false
        vm.sendQuestionnaire = sendQuestionnaire;
        function sendQuestionnaire(vendorID,vendorname) {
            bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_SENDQUEST') +vendorname+ '?</h4>', function (res) {
                if (res) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    VerifikasiDataService.sendQuestionnaire({
                        VendorID: vendorID
                    }, function (reply) {
                        if (reply.status === 200) {
                            vm.isSendQuest = reply.data;
                            console.info("issend?" + vm.isSendQuest);
                            if (vm.isSendQuest == true) {
                                UIControlService.msg_growl("success", "Berhasil mengirim kuesioner");
                                init();
                            }
                            else {
                                UIControlService.msg_growl("warning", "Kuesioner tidak ditemukan. Silakan membuat kuesioner terlebih dahulu.");
                            }
                            UIControlService.unloadLoading();
                        } else {
                            $.growl.error({ message: "Gagal mengirim kuesioner" });
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        $.growl.error({ message: "Gagal Akses API >" + err });
                        UIControlService.unloadLoading();
                    });

                    //SocketService.emit("daftarRekanan");
                }
            });

        }


        vm.exportDataPerusahaan = exportDataPerusahaan;
        function exportDataPerusahaan() {
            var headers = {};
            headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

            $http({
                method: 'POST',
                url: apipoint + '/verifiedvendor/exportDataPerusahaan',
                headers: headers,
                data: {
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                var linkElement = document.createElement('a');
                var fileName = "Data Perusahaan.docx";

                try {
                    var blob = new Blob([data], { type: headers('content-type') });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute('download', fileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);
                } catch (e) {
                    console.log(e);
                }
            });
        }


        vm.exportExcel = exportExcel;
        function exportExcel() {
            //console.info("total:" + vm.totalItems);
            var offset = (1 * 10) - 10;
            VerifikasiDataService.all({
                Offset: offset,
                Limit: vm.totalItems,
                Keyword: vm.nCompany,
                Status: vm.cmbStatus,
                Date1: UIControlService.getStrDate(vm.verifikasi.StartDate),
                Date2: UIControlService.getStrDate(vm.verifikasi.EndDate)
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.alldata = data.List;
                    //console.info("datasampel:" + JSON.stringify(vm.alldata[1]));
                    vm.totalItemAlldata = Number(data.Count);
                    vm.filterdata = [];
                    //console.info("total semua data:" + JSON.stringify(vm.alldata.length));
                    for (var i = 0; i < vm.totalItemAlldata; i++) {
                        if (vm.alldata[i].categoryvendor == null || vm.alldata[i].categoryvendor == undefined) {
                            vm.alldata[i].categoryvendor = " ";
                        }
                        if (vm.alldata[i].Code == null) {
                            vm.alldata[i].Code = " ";
                        }
                        if (vm.alldata[i].SAPCode == null) {
                            vm.alldata[i].SAPCode = " ";
                        }
                        
                        if (vm.alldata[i].business != null) {
                            if (vm.alldata[i].business.Name == null) {
                                vm.jenisPerusahaan = " ";
                            }
                            else if (vm.alldata[i].business.Name != null) {
                                vm.jenisPerusahaan = vm.alldata[i].business.Name;
                            }
                        }
                        else if (vm.alldata[i].business == null) {
                            vm.jenisPerusahaan = " ";
                        }
                        if (vm.alldata[i].LegalNumber == null) {
                            vm.alldata[i].LegalNumber = " ";
                        }
                        //console.info("jenis" + JSON.stringify(vm.alldata[i].business.Name));

                        var forExcel = {
                            VendorCode: vm.alldata[i].VendorID,
                            VendorName: vm.alldata[i].VendorName,
                            EProcurementVendorCode: vm.alldata[i].Code,
                            FoundedDate: UIControlService.getStrDate(vm.alldata[i].FoundedDate),
                            RegistrationDate: UIControlService.getStrDate(vm.alldata[i].CreatedDate),
                            SAPCode: vm.alldata[i].SAPCode,
                            BusinessType: vm.jenisPerusahaan,
                            Category: vm.alldata[i].categoryvendor
                        }
                        vm.filterdata.push(forExcel);
                    }
                    //console.info("dataForExcel:" + JSON.stringify(vm.filterdata));
                    JSONToCSVConvertor(vm.filterdata, true);
                    vm.flag = vm.cmbStatus;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Rekanan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });


        }



        vm.JSONToCSVConvertor = JSONToCSVConvertor;
        function JSONToCSVConvertor(JSONData, ShowLabel) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = JSONData;
            //console.info(arrData[0]);
            var CSV = '';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "sep=,\n\n\n\n" + '\n';

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);
                //console.info(row);
                //append Label row with line break
                CSV += row + '\r\n';
                //console.info(CSV);
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "Data Rekanan";

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }


        vm.loadContact = loadContact;
        function loadContact(vendorID,i) {
            vm.addressBranch = '';
            vm.addressMain = '';
            VerifikasiDataService.select({
                VendorID: vendorID
            }, function (reply) {
                if (reply.status === 200) {
                    vm.verified = reply.data;
                    for (var j = 0; j < vm.verified.length; j++) {
                        if (vm.verified[j].VendorContactTypeID == 20) {
                            if (vm.verified[j].Contact.Address.State.CountryID == 360) {
                                if (vm.verified[j].Contact.Address.DistrictID == 5101 || vm.verified[j].Contact.Address.DistrictID == 5102 || vm.verified[j].Contact.Address.DistrictID == 5104 || vm.verified[j].Contact.Address.DistrictID == 5108) {
                                    vm.verifikasidata[i]["keterangan"] = "Local";
                                    ///console.info("lokal");
                                }
                                else {
                                    
                                        vm.verifikasidata[i]["keterangan"] = "National"; //console.info("nas");
                                    
                                }
                            }
                            else {
                                vm.verifikasidata[i]["keterangan"] = "International"; //console.info("intern");
                            }
                        }
                    }
                    //console.info("kethmm" + JSON.stringify(vm.verifikasidata[i]));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        function convertAllDateToString() { // TIMEZONE (-)
            if (vm.verifikasi.StartDate) {
                vm.verifikasi.StartDate = UIControlService.getStrDate(vm.verifikasi.StartDate);
            }
            if (vm.verifikasi.EndDate) {
                vm.verifikasi.EndDate = UIControlService.getStrDate(vm.verifikasi.EndDate);
            }
        };

        function convertToDate() {
            if (vm.verifikasi.StartDate) {
                vm.verifikasi.StartDate = new Date(Date.parse(vm.verifikasi.StartDate));
            }
            if (vm.verifikasi.EndDate) {
                vm.verifikasi.EndDate = new Date(Date.parse(vm.verifikasi.EndDate));
            }
        }

        vm.formUpdateSAPCode = formUpdateSAPCode;
        function formUpdateSAPCode(id,sapcode) {
            var data = {
                VendorID: id,
                SAPCode:sapcode
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/updateSAPCode.html',
                controller: 'UpdateSAPCodeCtrl',
                controllerAs: 'UpdateSAPCodeCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });
        }
       
        function add(act, data, name) {
            var data = {
                TenderName : name, 
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailActified.html',
                controller: 'FormActivedVendorCtrl',
                controllerAs: 'FrmActivedVendorCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });
        }

        function addVerifikasi(data) {
            vm.ViewModelVerified = {
                Status: vm.Status,
                Keyword: vm.nCompany,
                StartDate: vm.verifikasi.StartDate,
                EndDate: vm.verifikasi.EndDate,
                currentPage: vm.currentPage
            }
            console.info(vm.ViewModelVerified);
            localStorage.setItem('ViewModelVerified', JSON.stringify(vm.ViewModelVerified));
            $state.transitionTo('proses-verifikasi', { id: data.VendorID });
        }

        function listExpired() {
            var data = {
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/expired-license.html',
                controller: 'ExpiredLicenseController',
                controllerAs: 'ExpiredLicenseController',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });
        }

        vm.editActive = editActive;
        function editActive(active, data) {
            var data = {
                act: active,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailAktivasiVendor.html',
                controller: 'FormActiveVendorCtrl',
                controllerAs: 'FrmActiveVendorCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });

        }

        vm.approval = approval;
        function approval() {
            $state.transitionTo('detail-approval-vendor');
        }

        vm.Detailapproval = Detailapproval;
        function Detailapproval(data) {
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailModalApproval.html',
                controller: 'DetailModalApprovalCtrl',
                controllerAs: 'DetailModalApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });

        }

        vm.DetailVerifikasiApproval = DetailVerifikasiApproval;
        function DetailVerifikasiApproval(data) {
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/verifikasiApproval.modal.html',
                controller: 'DetailVerifikasiApprovalModalApprovalCtrl',
                controllerAs: 'DetailVerifikasiApprovalModalApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(vm.currentPage);
            });

        }

        vm.saveSAP = saveSAP;
        function saveSAP() {
            //console.info("sss");
            VerifikasiDataService.saveSAPCode(vm.verifikasidata, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    init();
                } else {
                    $.growl.error({ message: "Gagal akses API" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        vm.deleteInactiveVendor = deleteInactiveVendor;
        function deleteInactiveVendor(data) {
            //console.info("sss");
            //console.info("param" + vm.vendorID);
            bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_DELETEINACTIVE') + data.VendorName + '?</h4>', function (res) {
                if (res) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    VerifikasiDataService.deleteInactiveVendor({ VendorID: data.VendorID }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "MESSAGE.SUCC_DELETEDATAINACTIVEVENDOR");
                            init();
                        } else {
                            $.growl.error({ message: "Gagal mendapatkan data Rekanan" });
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        $.growl.error({ message: "Gagal Akses API >" + err });
                        UIControlService.unloadLoading();
                    });

                    //SocketService.emit("daftarRekanan");
                }
            });
        }

        function getSysAccess() {
            var roleId = localStorage.getItem('RoleIDOnly');
            var defer = $q.defer();
            AuthService.getSysAccess({ IntParam1: roleId, IntParam2: 2070 }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    console.log(data)
                    //vm.CompanyScale = data.CompanyScale;

                    defer.resolve(true);
                } else {
                    defer.reject(false);
                }
            }, function (err) {
                defer.reject(false);
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            })
            return defer.promise;
        }
    }
})();
