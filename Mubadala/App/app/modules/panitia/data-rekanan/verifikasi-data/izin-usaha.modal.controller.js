﻿(function () {
	'use strict';

	angular.module("app").controller("FormIzinCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader','VendorRegistrationService', 'DataAdministrasiService','$location', 'SocketService',
		'UIControlService', 'item', '$uibModal', 'VerifikasiDataService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService',
		'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService','$q','$stateParams'];

	function ctrl($http, $translate, $translatePartialLoader, VendorRegistrationService, DataAdministrasiService, $location, SocketService,
		 UIControlService, item, $uibModal, VerifikasiDataService, $uibModalInstance, UploadFileConfigService, UploaderService, AuthService,
            $filter, ProvinsiService, GlobalConstantService,$q,$stateParams)
	    {
	    var vm = this;
	    vm.NpwpNew = "";
	    vm.data = item.item;
	    var dataModal = vm.data;
	    vm.currencyID = vm.data.CurrencyID;
	    vm.flag = item.flag;
	    vm.administrasiDate = {};
	    vm.folderFile = GlobalConstantService.getConstant('api') + "/";
	    const STOCK_OWNER_ID_PAGE_NAME = 'PAGE.VENDOR.REGISTRATION.ID';
		vm.isCalendarOpened = [false, false, false, false];
		vm.pathFile;
		vm.getIDstate;
		vm.VendorLogin;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.mindate = new Date();
		vm.city = item.city;
		vm.langID = true;
		vm.id = $stateParams.id;
		vm.VendorID = item.id;

		vm.myConfig5 = {
		    maxItems: 1,
		    optgroupField: "Symbol",
		    labelField: "symbolLabel",
		    valueField: "CurrencyID",
		    searchField: ["Symbol", "Label"],
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.init = init;
		function init() {
		    if (localStorage.getItem('currLang').toLowerCase() != 'id') {
		        vm.langID = false;
		    }
		    console.info("flag" + vm.flag);
			//var parts = $translatePartialLoader.getRegisteredParts();
			//var firstPart = parts[0]
			//var lastPart = parts[parts.length - 1]

			//// remove all
			//for (var i = 0; i < parts.length; i++) {
			//	$translatePartialLoader.deletePart(parts[i], true)
			//}


			//$translatePartialLoader.addPart('data-administrasi');
		    //$translatePartialLoader.addPart('proses-verifikasi');
		    //$translatePartialLoader.addPart('daftar');
		    //$translatePartialLoader.addPart('pengurus-perusahaan');
		    //$translatePartialLoader.addPart('akta-pendirian');
		    //$translatePartialLoader.addPart("data-izinusaha");
		    //$translatePartialLoader.addPart('tenaga-ahli');
		    //$translatePartialLoader.addPart('surat-pernyataan');
		    //$translatePartialLoader.addPart('vendor-balance');
		    UIControlService.loadLoadingModal("MESSAGE.LOADING");
		    
		    if (vm.flag === 0) {
		        loadAllCurrencies();
		        loadConfigSPPKP();
		        loadFilePrefix();
		        loadOfficeType();
		        vm.addresses = [];
		        vm.addressType = "";
		        vm.AddressRemovedMain = [];
		        vm.AddressRemovedBranch = [];
		        vm.changeAddressType = changeAddressType;
		        function changeAddressType() {
		            vm.selectedCountry = [];
		            vm.selectedState = [];
		            vm.listRegion.Name = "";
		            vm.address1 = "";
		            vm.selectedAssociation = [];
		            vm.postalcode = "";
		        }
		        vm.addAddress = addAddress;
		        function addAddress() {

		            if (vm.selectedCountry.Code == "IDN") {
		                if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_MAIN") {
		                    if (!vm.addressType) { UIControlService.msg_growl("error", "Tipe alamat harus dipilih"); return; }
		                    else if (!vm.address1) { UIControlService.msg_growl("error", "Alamat harus diisi"); return; }
		                    else if (!vm.selectedCountry) { UIControlService.msg_growl("error", "Negara harus dipilih"); return; }
		                    else if (!vm.selectedState) { UIControlService.msg_growl("error", "Provinsi harus dipilih"); return; }
		                    else if (!vm.selectedCity) {
		                        UIControlService.msg_growl("error", "Kota harus dipilih"); return;
		                    } else if (!vm.selectedDistrict) { UIControlService.msg_growl("error", "Wilayah harus dipilih"); return; }
		                        //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		                    else if (!vm.selectedAssociation) { UIControlService.msg_growl("error", "Tipe asosiasi harus dipilih"); return; }
		                    else {
		                        var centerOffice = 0;
		                        var id = vm.addresses.length + 1;
		                        var VendorContactType = {
		                            Name: "VENDOR_OFFICE_TYPE_MAIN",
		                            RefID: 1042,
		                            Type: "VENDOR_OFFICE_TYPE",
		                            Value: "VENDOR_OFFICE_TYPE_MAIN",
		                            VendorContactTypeID: 1042
		                        };
		                        for (var i = 0; i < vm.addresses.length; i++) {
		                            if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                                UIControlService.msg_growl("error", "Kantor pusat sudah diisi"); return;
		                                centerOffice += 1;
		                            }
		                        }
		                        if (centerOffice <= 0) {
		                            vm.tmp = {
		                                id: vm.addresses.length + 1,
		                                AddressID: 0,
		                                type: vm.addressType.Value,
		                                address: vm.address1,
		                                countryID: vm.selectedCountry.CountryID,
		                                countryName: vm.selectedCountry.Name,
		                                stateID: vm.selectedState.StateID,
		                                stateName: vm.selectedState.Name,
		                                cityID: vm.selectedCity.CityID,
		                                cityName: vm.selectedCity.Name,
		                                districtID: vm.selectedDistrict.DistrictID,
		                                districtName: vm.selectedDistrict.Name,
		                                postalCode: vm.postcalcode,
		                                VendorContactType: VendorContactType,
		                                ContactID: 0
		                            }
		                            vm.addresses.push(vm.tmp);
		                            vm.selectedCountry = [];
		                            vm.selectedState = [];
		                            vm.listRegion.Name = "";
		                            vm.address1 = "";
		                            vm.selectedAssociation = [];
		                            vm.postalcode = "";
		                            vm.addressType = "";
		                        }
		                    }
		                } else if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_BRANCH") {
		                    if (!vm.addressType) { UIControlService.msg_growl("error", "Tipe alamat harus dipilih"); return; }
		                    else if (!vm.address1) { UIControlService.msg_growl("error", "Alamat harus diisi"); return; }
		                    else if (!vm.selectedCountry) { UIControlService.msg_growl("error", "Negara harus dipilih"); return; }
		                    else if (!vm.selectedState) { UIControlService.msg_growl("error", "Provinsi harus dipilih"); return; }
		                    else if (!vm.selectedCity) { UIControlService.msg_growl("error", "Kota harus dipilih"); return; }
		                    else if (!vm.selectedDistrict) { UIControlService.msg_growl("error", "Wilayah harus dipilih"); return; }
		                        //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		                    else {
		                        var centerOffice = 0;
		                        var id = vm.addresses.length + 1;
		                        var VendorContactType = {
		                            Name: "VENDOR_OFFICE_TYPE_BRANCH",
		                            RefID: 1043,
		                            Type: "VENDOR_OFFICE_TYPE",
		                            Value: "VENDOR_OFFICE_TYPE_BRANCH",
		                            VendorContactTypeID: 1043
		                        };
		                        //for (var i = 0; i < vm.addresses.length; i++) {
		                        //    if (vm.addresses[i].VendorContactType.Name == "VENDOR_OFFICE_TYPE_MAIN") {
		                        //        UIControlService.msg_growl("error", "Kantor pusat sudah diisi"); return;
		                        //        centerOffice += 1;
		                        //    }
		                        //}
		                        //if (centerOffice <= 0) {
		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            AddressID: 0,
		                            type: vm.addressType.Value,
		                            address: vm.address1,
		                            countryID: vm.selectedCountry.CountryID,
		                            countryName: vm.selectedCountry.Name,
		                            stateID: vm.selectedState.StateID,
		                            stateName: vm.selectedState.Name,
		                            cityID: vm.selectedCity.CityID,
		                            cityName: vm.selectedCity.Name,
		                            districtID: vm.selectedDistrict.DistrictID,
		                            districtName: vm.selectedDistrict.Name,
		                            postalCode: vm.postcalcode,
		                            VendorContactType: VendorContactType,
		                            ContactID: 0
		                        }
		                        vm.addresses.push(vm.tmp);
		                        vm.selectedCountry = [];
		                        vm.selectedState = [];
		                        vm.listRegion.Name = "";
		                        vm.address1 = "";
		                        vm.selectedAssociation = [];
		                        vm.postalcode = "";
		                        vm.addressType = "";
		                        //}
		                    }
		                }
		            } else {
		                if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_MAIN") {
		                    if (!vm.addressType) { UIControlService.msg_growl("error", "Tipe alamat harus dipilih"); return; }
		                    else if (!vm.address1) { UIControlService.msg_growl("error", "Alamat harus diisi"); return; }
		                    else if (!vm.selectedCountry) { UIControlService.msg_growl("error", "Negara harus dipilih"); return; }
		                    else if (!vm.selectedState) { UIControlService.msg_growl("error", "Provinsi harus dipilih"); return; }
		                        //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		                    else if (!vm.selectedAssociation) { UIControlService.msg_growl("error", "Tipe asosiasi harus dipilih"); return; }
		                    else {
		                        var centerOffice = 0;
		                        var id = vm.addresses.length + 1;
		                        var VendorContactType = {
		                            Name: "VENDOR_OFFICE_TYPE_MAIN",
		                            RefID: 1042,
		                            Type: "VENDOR_OFFICE_TYPE",
		                            Value: "VENDOR_OFFICE_TYPE_MAIN",
		                            VendorContactTypeID: 1042
		                        };
		                        for (var i = 0; i < vm.addresses.length; i++) {
		                            if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                                UIControlService.msg_growl("error", "Kantor pusat sudah diisi"); return;
		                                centerOffice += 1;
		                            }
		                        }
		                        if (centerOffice <= 0) {
		                            vm.tmp = {
		                                id: vm.addresses.length + 1,
		                                AddressID: 0,
		                                type: vm.addressType.Value,
		                                address: vm.address1,
		                                countryID: vm.selectedCountry.CountryID,
		                                countryName: vm.selectedCountry.Name,
		                                stateID: vm.selectedState.StateID,
		                                stateName: vm.selectedState.Name,
		                                //cityID: vm.selectedCity.CityID,
		                                //cityName: vm.selectedCity.Name,
		                                //districtID: vm.selectedDistrict.DistrictID,
		                                //districtName: vm.selectedDistrict.Name,
		                                postalCode: vm.postcalcode,
		                                VendorContactType: VendorContactType,
		                                ContactID: 0
		                            }
		                            vm.addresses.push(vm.tmp);
		                            vm.selectedCountry = [];
		                            vm.selectedState = [];
		                            vm.listRegion.Name = "";
		                            vm.address1 = "";
		                            vm.selectedAssociation = [];
		                            vm.postalcode = "";
		                            vm.addressType = "";
		                        }
		                    }
		                } else if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_BRANCH") {
		                    if (!vm.addressType) { UIControlService.msg_growl("error", "Tipe alamat harus dipilih"); return; }
		                    else if (!vm.address1) { UIControlService.msg_growl("error", "Alamat harus diisi"); return; }
		                    else if (!vm.selectedCountry) { UIControlService.msg_growl("error", "Negara harus dipilih"); return; }
		                    else if (!vm.selectedState) { UIControlService.msg_growl("error", "Provinsi harus dipilih"); return; }
		                        //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		                    else {
		                        var centerOffice = 0;
		                        var id = vm.addresses.length + 1;
		                        var VendorContactType = {
		                            Name: "VENDOR_OFFICE_TYPE_BRANCH",
		                            RefID: 1043,
		                            Type: "VENDOR_OFFICE_TYPE",
		                            Value: "VENDOR_OFFICE_TYPE_BRANCH",
		                            VendorContactTypeID: 1043
		                        };
		                        //for (var i = 0; i < vm.addresses.length; i++) {
		                        //    if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                        //        UIControlService.msg_growl("error", "Kantor pusat sudah diisi"); return;
		                        //        centerOffice += 1;
		                        //    }
		                        //}
		                        //if (centerOffice <= 0) {
		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            AddressID: 0,
		                            type: vm.addressType.Value,
		                            address: vm.address1,
		                            countryID: vm.selectedCountry.CountryID,
		                            countryName: vm.selectedCountry.Name,
		                            stateID: vm.selectedState.StateID,
		                            stateName: vm.selectedState.Name,
		                            //cityID: vm.selectedCity.CityID,
		                            //cityName: vm.selectedCity.Name,
		                            //districtID: vm.selectedDistrict.DistrictID,
		                            //districtName: vm.selectedDistrict.Name,
		                            postalCode: vm.postcalcode,
		                            VendorContactType: VendorContactType,
		                            ContactID: 0
		                        }
		                        vm.addresses.push(vm.tmp);
		                        vm.selectedCountry = [];
		                        vm.selectedState = [];
		                        vm.listRegion.Name = "";
		                        vm.address1 = "";
		                        vm.selectedAssociation = [];
		                        vm.postalcode = "";
		                        vm.addressType = "";
		                        //}
		                    }
		                }
		            }
		        }

		        vm.removeAddress = removeAddress;
		        function removeAddress(data) {
		            for (var i = 0; i < vm.addresses.length; i++) {
		                if (vm.addresses[i].id == data.id) {
		                    if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                        vm.AddressRemovedMain.push(vm.addresses[i]);
		                    } else {
		                        vm.AddressRemovedBranch.push(vm.addresses[i]);
		                    }
		                    vm.addresses.splice(i, 1);
		                }
		            }
		        }
		        VerifikasiDataService.selectcontact({
		            VendorID: item.item
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                vm.listPersonal = [];
		                vm.contact = reply.data;
		                vm.flagPrimary = 0;
		                for (var i = 0; i < vm.contact.length; i++) {
		                    if (vm.contact[i].IsPrimary === 2) { vm.flagPrimary = 1; }
		                    if (i == (vm.contact.length - 1)) {
		                        if (vm.flagPrimary == 0) {
		                            loadCountryAlternatif();
		                        }
		                    }
		                }
		                for (var i = 0; i < vm.contact.length; i++) {
		                    if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
		                        vm.VendorContactTypeCompany = vm.contact[i].VendorContactType;
		                        if (vm.contact[i].Contact.Fax !== null) {
		                            vm.fax = vm.contact[i].Contact.Fax;
		                        }
		                        vm.vendorCategoryID = vm.contact[i].Contact.VendorCategoryID;
		                        vm.administrasi = vm.contact[i].Vendor;
		                        loadTypeVendor(vm.contact[i].Vendor);
		                        loadAssociation(vm.contact[i].Vendor);
		                        loadVendorCommodity(item.item);
		                        vm.StartDate = new Date(Date.parse(vm.contact[i].Vendor.FoundedDate));
		                        vm.Username = vm.contact[i].Vendor.user.Username;
		                        vm.PKPNumber = vm.contact[i].Vendor.PKPNumber;
		                        vm.Npwp = vm.contact[i].Vendor.Npwp;
		                        vm.NpwpNew = vm.contact[i].Vendor.Npwp;
		                        vm.ContactID = vm.contact[i].Contact.ContactID;
		                        if (vm.contact[i].Contact.Phone == null) {
		                            vm.phone = [];
		                            vm.Phone = "";
		                            loadPhoneCodes();
		                        }
		                        else {

		                            vm.phone = vm.contact[i].Contact.Phone.split(' ');
		                            vm.Phone = (vm.phone[1]);
		                            vm.phone = vm.phone[0].split(')');
		                            vm.phone = vm.phone[0].split('(');
		                            loadPhoneCodes(vm.phone[1]);
		                        }
		                        vm.EmailOri = vm.contact[i].Contact.Email;
		                        vm.Email = vm.contact[i].Contact.Email;
		                        vm.Website = vm.contact[i].Contact.Website;
		                        vm.addressIdComp = vm.contact[i].Contact.AddressID;
		                        loadCountryAdmin(vm.contact[i].Contact.Address.State);
		                        vm.CityCompany = vm.contact[i].Contact.Address.City;
		                        vm.DistrictCompany = vm.contact[i].Contact.Address.Distric;
		                        vm.Region = vm.contact[i].Contact.Address.State.Country.Continent.Name;
		                        if (vm.contact[i].IsPrimary === 1 || vm.contact[i].IsPrimary === null) {
		                            vm.CountryCode = vm.contact[i].Contact.Address.State.Country.Code;
		                            loadCurrencies(vm.administrasi);
		                            if (vm.CountryCode == 'IDN') {
		                                loadBusinessEntity(vm.contact[i].Vendor);
		                            }
		                        }
		                        vm.contactpersonal = vm.contact[i];
		                    } else if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {
		                        vm.listPersonal.push(vm.contact[i]);
		                        vm.VendorContactTypePers = vm.contact[i].VendorContactType;
		                    } //else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === null) {
		                    else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN') {
		                        //if (vm.contact[i].Contact.Address.AddressInfo != undefined) {
		                        vm.arr = [];
		                        vm.arr.push(vm.contact[i]);

		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            AddressID: vm.arr[0].Contact.Address.AddressID,
		                            type: vm.arr[0].VendorContactType.Name,
		                            address: vm.arr[0].Contact.Address.AddressInfo,
		                            countryID: vm.arr[0].Contact.Address.State.Country.CountryID,
		                            countryName: vm.arr[0].Contact.Address.State.Country.Name,
		                            stateID: vm.arr[0].Contact.Address.State.StateID,
		                            stateName: vm.arr[0].Contact.Address.State.Name,
		                            cityID: vm.arr[0].Contact.Address.City.CityID,
		                            cityName: vm.arr[0].Contact.Address.City.Name,
		                            districtID: vm.arr[0].Contact.Address.Distric.DistrictID,
		                            districtName: vm.arr[0].Contact.Address.Distric.Name,
		                            postalCode: vm.arr[0].Contact.Address.PostalCode,
		                            VendorContactType: vm.arr[0].VendorContactType,
		                            ContactID: vm.arr[0].Contact.ContactID
		                        }
		                        vm.addresses.push(vm.tmp);
		                        //}
		                        //vm.addressFlag = 1;
		                        vm.ContactOfficeId = vm.contact[i].Contact.ContactID;
		                        //vm.ContactName = "Kantor Pusat";
		                        vm.Name = vm.contact[i].Contact.Name;
		                        //vm.AddressId = vm.contact[i].Contact.AddressID;
		                        //vm.VendorContactType = vm.contact[i].VendorContactType;

		                        //vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
		                        //vm.cekAddress = vm.contact[i].Contact.Address.AddressInfo;
		                        //vm.postalcode = vm.contact[i].Contact.Address.PostalCode;
		                        //vm.cekPostCode = vm.postalcode;
		                        loadCountry(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                        vm.selectedState1 = vm.contact[i].Contact.Address.State;
		                        if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                            vm.selectedCity1 = vm.contact[i].Contact.Address.City;
		                            vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

		                        }
		                    }
		                        //else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === null) {
		                    else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH') {
		                        vm.arr = [];
		                        vm.arr.push(vm.contact[i]);

		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            type: vm.arr[0].VendorContactType.Name,
		                            AddressID: vm.arr[0].Contact.Address.AddressID,
		                            address: vm.arr[0].Contact.Address.AddressInfo,
		                            countryID: vm.arr[0].Contact.Address.State.Country.CountryID,
		                            countryName: vm.arr[0].Contact.Address.State.Country.Name,
		                            stateID: vm.arr[0].Contact.Address.State.StateID,
		                            stateName: vm.arr[0].Contact.Address.State.Name,
		                            cityID: vm.arr[0].Contact.Address.City.CityID,
		                            cityName: vm.arr[0].Contact.Address.City.Name,
		                            districtID: vm.arr[0].Contact.Address.Distric.DistrictID,
		                            districtName: vm.arr[0].Contact.Address.Distric.Name,
		                            ContactID: vm.arr[0].Contact.ContactID
		                        }
		                        vm.addresses.push(vm.tmp);

		                        //if (vm.addressFlag == 0) {
		                        //	vm.AddressId = vm.contact[i].Contact.AddressID;
		                        vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
		                        vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
		                        //	vm.ContactName = "Kantor Cabang";
		                        vm.Name = vm.contact[i].Contact.Name;
		                        //	vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
		                        //	vm.cekAddress = vm.contact[i].Contact.Address.AddressInfo;
		                        //	vm.postalcode = vm.contact[i].Contact.Address.PostalCode;
		                        //	vm.cekPostCode = vm.postalcode;
		                        loadCountry(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                        vm.selectedState1 = vm.contact[i].Contact.Address.State;
		                        if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                            vm.selectedCity1 = vm.contact[i].Contact.Address.City;
		                            vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

		                        }
		                        //}


		                    }
                            //else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === null) {
		                    //    vm.addressFlag = 1;
		                    //    vm.ContactOfficeId = vm.contact[i].Contact.ContactID;
		                    //    vm.ContactName = "Kantor Pusat";
		                    //    vm.Name = vm.contact[i].Contact.Name;
		                    //    vm.AddressId = vm.contact[i].Contact.AddressID;
		                    //    vm.VendorContactType = vm.contact[i].VendorContactType;

		                    //    vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo;
		                    //    vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
		                    //    vm.cekAddress = vm.address1;
		                    //    vm.postcalcode = vm.contact[i].Contact.Address.PostalCode;
		                    //    vm.cekPostCode = vm.postcalcode;
		                    //    loadCountry(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                    //    vm.selectedState1 = vm.contact[i].Contact.Address.State;
		                    //    if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                    //        vm.selectedCity1 = vm.contact[i].Contact.Address.City;
		                    //        vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

		                    //    }
		                    //} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === null) {
		                    //    if (vm.addressFlag == 0) {
		                    //        vm.AddressId = vm.contact[i].Contact.AddressID;
		                    //        vm.ContactOfficeId = vm.contact[i].Contact.ContactID;
		                    //        vm.VendorContactType = vm.contact[i].VendorContactType;
		                    //        vm.ContactName = "Kantor Cabang";
		                    //        vm.Name = vm.contact[i].Contact.Name;
		                    //        vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo;
		                    //        vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
		                    //        vm.cekAddress = vm.address1;
		                    //        vm.postcalcode = vm.contact[i].Contact.Address.PostalCode;
		                    //        vm.cekPostCode = vm.postcalcode;
		                    //        loadCountry(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                    //        vm.selectedState1 = vm.contact[i].Contact.Address.State;
		                    //        if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                    //            vm.selectedCity1 = vm.contact[i].Contact.Address.City;
		                    //            vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

		                    //        }
		                    //    }


		                    //} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === 2) {
		                    //    vm.addressAlterFlag = 1;
		                    //    vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
		                    //    vm.AddressAlterId = vm.contact[i].Contact.AddressID;
		                    //    vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
		                    //    vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo;
		                    //    vm.addressinfo = vm.contact[i].Contact.Address.AddressInfo;
		                    //    vm.cekAddress1 = vm.addressinfo;
		                    //    vm.PostalCodeAlternatif = vm.contact[i].Contact.Address.PostalCode;
		                    //    vm.cekPostCode1 = vm.PostalCodeAlternatif;
		                    //    loadCountryAlternatif(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                    //    vm.selectedStateAlternatif1 = vm.contact[i].Contact.Address.State;
		                    //    if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                    //        vm.selectedCityAlternatif1 = vm.contact[i].Contact.Address.City;
		                    //        vm.selectedDistrictAlternatif1 = vm.contact[i].Contact.Address.Distric;

		                    //    }

		                    //} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === 2) {
		                    //    if (vm.addressAlterFlag == 0) {
		                    //        vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
		                    //        vm.AddressAlterId = vm.contact[i].Contact.AddressID;
		                    //        vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
		                    //        vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo;
		                    //        vm.addressinfo = vm.contact[i].Contact.Address.AddressInfo;
		                    //        vm.cekAddress1 = vm.addressinfo;
		                    //        vm.PostalCodeAlternatif = vm.contact[i].Contact.Address.PostalCode;
		                    //        vm.cekPostCode1 = vm.PostalCodeAlternatif;
		                    //        loadCountryAlternatif(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                    //        vm.selectedStateAlternatif1 = vm.contact[i].Contact.Address.State;
		                    //        if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                    //            vm.selectedCityAlternatif1 = vm.contact[i].Contact.Address.City;
		                    //            vm.selectedDistrictAlternatif1 = vm.contact[i].Contact.Address.Distric;

		                    //        }
		                    //    }
		                    //}

		                }

		            } else {
		                UIControlService.unloadLoadingModal();
		            }
		        }, function (err) {
		            UIControlService.unloadLoadingModal();
		        });
		    }
		    else if (vm.flag === 1) {
		        vm.isEdit = true;
		        if (dataModal.LicenseNo == undefined) {
		            vm.isEdit = false;
		        }

		        vm.dataSelectIzinUsaha = [];
		        vm.noJenisIzinUsaha = dataModal.LicenseNo == undefined ? '' : dataModal.LicenseNo;
		        vm.isDobCalendarOpened = false;
		        vm.dataSelectProvinsi = [];
		        vm.currency = 'Rp';
		        vm.dataBidangUsaha = [];
		        vm.tipeLicense = dataModal.LicenseType == undefined ? '' : dataModal.LicenseType;
		        vm.provinsi = dataModal.IssuedState == undefined ? '' : dataModal.IssuedState;
		        vm.instansiPemberi = dataModal.IssuedBy == undefined ? '' : dataModal.IssuedBy;
		        vm.kualifikasi = dataModal.LicenseQualification == undefined ? '' : dataModal.LicenseQualification;
		        //vm.city = dataModal.IssuedLocation == undefined ? '' : dataModal.IssuedLocation;
		        vm.CapitalAmount = dataModal.CapitalAmount == undefined ? '' : dataModal.CapitalAmount;
		        vm.IssuedDate = dataModal.IssuedDate == undefined ? '' : new Date(UIControlService.convertDateForDatepicker(dataModal.IssuedDate));
		        vm.DocUrl = dataModal.DocumentURL == undefined ? '' : dataModal.DocumentURL;
		        //vm.IssuedDate = data.IssuedDate == undefined ? '' : UIControlService.convertDateForDatepicker(data.IssuedDate);
		        vm.ExpiredDate = dataModal.ExpiredDate == undefined ? '' : new Date(UIControlService.convertDateForDatepicker(dataModal.ExpiredDate));
		        vm.jenisIzinUsaha = '';
		        vm.keterangan = dataModal.Remark == undefined ? '' : dataModal.Remark;
		        vm.datasimpan = {};
		        vm.VendorLicenseID = dataModal.VendorLicenseID == undefined ? null : dataModal.VendorLicenseID;
		        vm.cityID = item.city;

		        loadKlasifikasi();
		        getTypeSizeFile();

		        changeCountry(vm.cityID);

		        if (vm.isEdit) {
		            getVendorCommodity(1);
		        }

		        if (vm.tipeLicense != '') {
		            changeJenisIzin();
		        }

		    }
		    else if (vm.flag === 2) {
		        loadConfigStock();
		        loadStockUnits(vm.data);
		        vm.nameStock = vm.data.OwnerName;
		        vm.data.OwnerDOB = new Date(Date.parse(vm.data.OwnerDOB));
		        vm.OwnerId = vm.data.OwnerID;
		        vm.Quantity = vm.data.Quantity;
		    }
		    else if (vm.flag === 3) {
		        vm.data.DocumentDate = new Date(Date.parse(vm.data.DocumentDate));
		        loadDetailVendor();
		    }
		    else if (vm.flag === 4 || vm.flag === 5) {
		        vm.data.DocumentDate = new Date(Date.parse(vm.data.DocumentDate));
		        loadConfigAkta();
		    }
		    else if (vm.flag === 6) {
		        UIControlService.loadLoadingModal();

		        var prom1 = loadAsset().then(function (response) {
		            return response
		        });
		        //var prom2 = loadUnit().then(function (response) {
		        //    return response
		        //});

		        $q.all([prom1]).then(function (result) {

		            //get Sisa Balance
		            getSisaBalance();
		        })

		        loadCurrency();
		        //loadConfigNeraca();
		    }
		    else if (vm.flag === 8) {
		        //loadNational();
		        //loadAsset();
		        //loadUnit();
		        //loadConfigNeraca();
		        //vm.addresses = {
		        //    AddressInfo: ""
		        //};
		        //vm.countrys = {
		        //    Name: ""
		        //};
		        //vm.statuss = {
		        //    Name: ""
		        //};
		        //vm.radio = {
		        //    tipeM: "M",
		        //    tipeF: "F",
		        //    StatusK: "CONTRACT",
		        //    StatusI: "INTERNSHIP",
		        //    StatusP: "PERMANENT",
		        //}
		        //vm.nationalities = ["Indonesia"];
		        //vm.data.DateOfBirth = new Date(Date.parse(vm.data.DateOfBirth));
		       
            }
		    else if (vm.flag === 7) {
		        vm.data.DateOfBirth = new Date(Date.parse(vm.data.DateOfBirth));
		        vm.data.ServiceEndDate = new Date(Date.parse(vm.data.ServiceEndDate));
		        vm.dataServiceStartDate = new Date(Date.parse(vm.data.ServiceStartDate));
		        UploadFileConfigService.getByPageName("PAGE.VENDOR.COMPANYPERSON", function (response) {
		            if (response.status == 200) {
		                vm.idUploadConfigs = response.data;
		                vm.idFileTypes = generateFilterStrings(response.data);
		                vm.idFileSize = vm.idUploadConfigs[0];
		                VerifikasiDataService.GetPositionTypes(function (response) {
		                    UIControlService.unloadLoadingModal();
		                    if (response.status == 200) {
		                        vm.positionTypes = response.data;
		                        loadCountries(vm.data.Address.State);
		                        //loadCountries();getProvinsi();
		                    } else {
		                        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_POSITION");
		                    }
		                }, function (err) {
		                    UIControlService.unloadLoadingModal();
		                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_POSITION");
		                });
		            } else {
		                UIControlService.unloadLoadingModal();
		                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
		            }
		        }, function (err) {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
		        });
		    }
		    else if (vm.flag === 9 || vm.flag === 10 || vm.flag === 11) {
		        loadConfigExpertCertificate();
		        vm.data.StartDate = new Date(Date.parse(vm.data.StartDate));
		        vm.data.EndDate = new Date(Date.parse(vm.data.EndDate));
		        
		    }
		    else if (vm.flag === 16) {
		        UploadFileConfigService.getByPageName("PAGE.VENDOR.UPLOADDL", function (response) {
		            UIControlService.unloadLoadingModal();
		            if (response.status == 200) {
		                vm.name = response.data.name;
		                vm.idUploadConfigs = response.data;
		                vm.idFileTypes = generateFilterStrings(response.data);
		                vm.idFileSize = vm.idUploadConfigs[0];
		            } else {
		                UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            UIControlService.unloadLoadingModal();
		            return;
		        });
		        vm.data.ValidDate = new Date(Date.parse(vm.data.ValidDate));
		    }
		    else if (vm.flag == 19) {
		        VerifikasiDataService.selectcontact({
		            VendorID: item.item
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                vm.vendor = reply.data;
		                for (var i = 0; i < vm.vendor.length; i++) {
		                    if (vm.vendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.vendor[i].IsActive == true) {
		                        vm.address = vm.vendor[i].Contact.Address.AddressInfo + ' ' + vm.vendor[i].Contact.Address.AddressDetail;
		                        vm.VendorName = vm.vendor[i].Vendor.VendorName;
		                        vm.npwp = vm.vendor[i].Vendor.Npwp;
		                    }
		                    else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY' && vm.vendor[i].IsActive == true) {
		                        vm.email = vm.vendor[i].Contact.Email;
		                        vm.username = vm.vendor[i].Vendor.user.Username;
		                        vm.VendorName = vm.vendor[i].Vendor.VendorName;
		                    }
		                    else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL' && vm.vendor[i].IsActive == true) {

		                        if (vm.telp == undefined) {
		                            vm.name = vm.vendor[i].Contact.Name;
		                            vm.telp = vm.vendor[i].Contact.Phone;
		                        }
		                    }
		                }

		            }
		        }, function (err) {
		        });
		        loadQuestionnaire();

		    }
		    else if (vm.flag == 22 || vm.flag==23) {
		        console.info("item:" + JSON.stringify(vm.data));
		    } else if (vm.flag === 17) {
		        VerifikasiDataService.selectcontact({
		            VendorID: $stateParams.id
		        }, function (reply) {
		            if (reply.status === 200) {
		                vm.vendor = reply.data;
		                vm.vendorName = vm.vendor[0].Vendor.VendorName;
		                vm.Country = vm.vendor[0].Contact.Address.State.Country.Name;
		                vm.StateName = vm.vendor[0].Contact.Address.State.Name;

                        loadNamaDir()
                        
		            }
		        }, function (err) {
		        });

		        //loadQuestionnaire();
		    }
		    else if (vm.flag === 18) {
		        UIControlService.unloadLoadingModal();
		    } else {
		        UIControlService.unloadLoadingModal();

		    }
		        
		}

		function loadNamaDir() {
		    VerifikasiDataService.selectNamaDir({
		        VendorID: vm.vendor[0].Vendor.VendorID
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();

		        if (reply.status === 200) {
		            var data = reply.data;
		            //vm.Nama = data;
		            vm.NamaDir = generateFilterStringsNama(reply.data);
		            vm.positionRef = data[0].PositionRef;
		            vm.noID = generateFilterStrings2(reply.data);
		            vm.PersonAddress = data[0].PersonAddress;
		        } else {
		            $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}
		function loadOfficeType() {
		    UIControlService.loadLoadingModal("LOADERS.LOADING_OFFICE_TYPE");
		    VendorRegistrationService.getOfficeType(
                function (response) {
                    if (response.status == 200) {
                        vm.officeTypes = response.data;
                    } else {
                        handleRequestError(response);
                    }
                },
                handleRequestError);
		}

		function generateFilterStringsNama(nama) {
		    var namaDir = "";
		    for (var i = 0; i < nama.length; i++) {
		        namaDir += nama[i].PersonName + ",";
		    }
		    return namaDir.substring(0, namaDir.length - 1);
		}

		function generateFilterStrings2(nama) {
		    var namaDir = "";
		    for (var i = 0; i < nama.length; i++) {
		        namaDir += nama[i].NoID + ",";
		    }
		    return namaDir.substring(0, namaDir.length - 1);
		}

		vm.loadCountryAdmin = loadCountryAdmin;
		vm.selectedCountryAdmin;
		vm.listCountry = [];
		function loadCountryAdmin(data) {
		    DataAdministrasiService.SelectCountry(function (response) {
		        vm.listCountryAdmin = response.data;
		        for (var i = 0; i < vm.listCountryAdmin.length; i++) {
		            if (data !== undefined) {
		                if (data.CountryID === vm.listCountryAdmin[i].CountryID) {
		                    vm.selectedCountryAdmin = vm.listCountryAdmin[i];
		                    loadStateAdmin(data);
		                    break;
		                }

		            }
		        }


		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.loadRegion = loadRegion;
		vm.selectedRegion;
		vm.listRegion = [];
		function loadRegion(countryID) {
		    DataAdministrasiService.SelectRegion({
		        CountryID: countryID
		    }, function (response) {
		        vm.listRegion = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.loadRegionAdmin = loadRegionAdmin;
		vm.selectedRegionAdmin;
		vm.listRegionAdmin = [];
		function loadRegionAdmin(countryID) {
		    DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
		        vm.listRegionAdmin = response.data;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.loadStateAdmin = loadStateAdmin;
		vm.selectedStateAdmin;
		vm.listStateAdmin = [];
		function loadStateAdmin(data) {
		    if (!data) {
		        data = vm.selectedCountryAdmin;
		        vm.selectedStateAdmin = "";
		        vm.selectedCityAdmin = "";
		        vm.selectedDistrictAdmin = "";
		        vm.selectedStateAdmin1 = "";
		    }
		    loadRegionAdmin(data.CountryID);

		    DataAdministrasiService.SelectState(data.CountryID, function (response) {
		        vm.listStateAdmin = response.data;
		        for (var i = 0; i < vm.listStateAdmin.length; i++) {
		            if (data !== undefined) {
		                if (data.StateID === vm.listStateAdmin[i].StateID) {
		                    vm.selectedStateAdmin = vm.listStateAdmin[i];
		                    if (vm.selectedStateAdmin.Country.Code === 'IDN') {
		                        loadCityAdmin(vm.selectedStateAdmin);
		                        break;
		                    }
		                }
		            }
		        }


		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.loadCityAdmin = loadCityAdmin;
		vm.selectedCityAdmin;
		vm.listCityAdmin = [];
		function loadCityAdmin(data) {
		    if (!data) {

		        data = vm.selectedStateAdmin;
		        vm.selectedCityAdmin = "";
		        vm.selectedCityAdmin1 = "";
		        vm.selectedDistrictAdmin = "";
		    }
		    DataAdministrasiService.SelectCity(data.StateID, function (response) {
		        vm.listCityAdmin = response.data;
		        for (var i = 0; i < vm.listCityAdmin.length; i++) {
		            if (data !== undefined) {
		                if (vm.CityCompany.CityID === vm.listCityAdmin[i].CityID) {
		                    vm.selectedCityAdmin = vm.listCityAdmin[i];
		                    if (vm.selectedStateAdmin.Country.Code === 'IDN') {
		                        loadDistrictAdmin(vm.selectedCityAdmin);
		                        break;
		                    }
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}

		vm.loadDistrictAdmin = loadDistrictAdmin;
		vm.selectedDistrictAdmin;
		vm.listDistrictAdmin = [];
		function loadDistrictAdmin(city) {
		    if (!city) {
		        city = vm.selectedCityAdmin;
		        vm.selectedDistrictAdmin = "";
		        vm.selectedDistrictAdmin1 = "";

		    }
		    DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
		        vm.listDistrictAdmin = response.data;
		        for (var i = 0; i < vm.listDistrictAdmin.length; i++) {
		            if (city !== undefined) {
		                if (vm.DistrictCompany.DistrictID === vm.listDistrictAdmin[i].DistrictID) {
		                    vm.selectedDistrictAdmin = vm.listDistrictAdmin[i];
		                    break;
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        return;
		    });
		}


		vm.loadPhoneCodes = loadPhoneCodes;
		function loadPhoneCodes(data) {
		    UIControlService.loadLoadingModal("Loading");
		    VendorRegistrationService.getCountries(
              function (response) {
                  vm.phoneCodeList = response.data;
                  for (var i = 0; i < vm.phoneCodeList.length; i++) {
                      if (vm.phoneCodeList[i].PhonePrefix === data) {
                          vm.phoneCode = vm.phoneCodeList[i];
                      }
                  }
                  UIControlService.unloadLoadingModal();
              }, function (err) {
                  //$.growl.error({ message: "Gagal Akses API >" + err });
                  UIControlService.unloadLoadingModal();
              });
		}

		vm.loadFilePrefix = loadFilePrefix;
		function loadFilePrefix() {
		    //UIControlService.loadLoadingModal("LOADERS.LOADING_PREFIX");
		    VerifikasiDataService.getUploadPrefix(
                function (response) {
                    var prefixes = response.data;
                    vm.prefixes = {};
                    for (var i = 0; i < prefixes.length; i++) {
                        vm.prefixes[prefixes[i].Name] = prefixes[i];
                    }
                    UIControlService.unloadLoadingModal();
                }, handleRequestError);
		}

		vm.addCurrency = addCurrency;
		function addCurrency() {
		    vm.flagCurr = false;
		    for (var i = 0; i < vm.listCurrencies.length; i++) {
		        if (vm.Currency.CurrencyID == vm.listCurrencies[i].MstCurrency.CurrencyID && vm.listCurrencies[i].IsActive == true) { vm.flagCurr = true; }
		    }
		    if (vm.flagCurr == false) {
		        vm.listCurrencies.push({
		            ID: 0,
		            CurrencyID: vm.Currency.CurrencyID,
		            VendorID: item.item,
		            MstCurrency: vm.Currency,
		            IsActive: true
		        });
		    }
		}

		vm.updateUsername = updateUsername;
		function updateUsername(data) {
		    vm.Username = data.replace(/ /gi, '_').toLowerCase();
		}


		vm.loadQuestionnaire = loadQuestionnaire;
		function loadQuestionnaire() {
		    vm.data = [];
		    VerifikasiDataService.loadQuestionnaire({
		        IntParam1: $stateParams.id,
		        Keyword: localStorage.getItem('currLang')
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.data = reply.data;
                    console.log(vm.data)
		        }
		    }, function (err) {
		    });
		}

		vm.loadNational = loadNational;
		function loadNational() {
		    VerifikasiDataService.GetAllNationalities(function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.nationalities = reply.data;
		        }
		        else {
		            UIControlService.msg_growl("error", "ERR_GET_COUNTRY");
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "ERR_GET_COUNTRY");
		        UIControlService.unloadLoadingModal();
		    });
}
		vm.loadConfigSPPKP = loadConfigSPPKP;
		function loadConfigSPPKP() {
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.ADMINISTRATION.PKP", function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            vm.name = response.data.name;
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		        return;
		    });
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.NPWP", function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            vm.name1 = response.data.name;
		            vm.idUploadConfigs1 = response.data;
		            vm.idFileTypes1 = generateFilterStrings(response.data);
		            vm.idFileSize1 = vm.idUploadConfigs1[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		        return;
		    });
		}

		vm.loadConfigNeraca = loadConfigNeraca;
		function loadConfigNeraca() {
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.BALANCE", function (response) {
		        //UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            vm.name = response.data.name;
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		        return;
		    });
		}

		vm.loadConfigExpertCertificate = loadConfigExpertCertificate;
		function loadConfigExpertCertificate() {
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.EXPERTSCERTIFICATE", function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            vm.name = response.data.name;
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];

		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		        return;
		    });
		}

		vm.selectedTypeVendor;
		vm.listTypeVendor;
		function loadTypeVendor(data) {
		    DataAdministrasiService.getTypeVendor(function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listTypeVendor = reply.data.List;
		            if (data !== undefined) {
		                for (var i = 0; i < vm.listTypeVendor.length; i++) {
		                    if (data.VendorTypeID === vm.listTypeVendor[i].RefID) {
		                        vm.selectedTypeVendor = vm.listTypeVendor[i];
		                        changeTypeVendor(data);
		                        break;
		                    }
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.selectedSupplier;
		vm.listSupplier;
		function loadSupplier(data) {
		    DataAdministrasiService.getSupplier(function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listSupplier = reply.data.List;
		            if (data) {
		                for (var i = 0; i < vm.listSupplier.length; i++) {
		                    if (data.SupplierID === vm.listSupplier[i].RefID) {
		                        vm.selectedSupplier = vm.listSupplier[i];
		                        break;
		                    }
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}


		vm.listBussinesDetailField = []
		vm.changeTypeVendor = changeTypeVendor;
		function changeTypeVendor(data) {
		    if (vm.selectedTypeVendor !== undefined) {
		        if (vm.selectedTypeVendor.Value === "VENDOR_TYPE_SERVICE") {
		            vm.disablePemasok = true;
		            vm.listSupplier = {};
		            UIControlService.msg_growl("warning", "MESSAGE.NO_SUPP");
		        }
		        if (vm.selectedTypeVendor.Value !== "VENDOR_TYPE_SERVICE") {
		            vm.disablePemasok = false;
		            if (data) {

		                loadSupplier(data);
		            }
		            else {

		                loadSupplier();
		            }
		        }

		        vm.GoodsOrService = vm.selectedTypeVendor.RefID;
		        loadBusinessField();
		        vm.listBussinesDetailField = [];
		        vm.listComodity = [];
		    }

		}

		vm.deleteRowCurr = deleteRowCurr;
		function deleteRowCurr(index, data) {
		    var idx = index;
		    var _length = vm.listCurrencies.length; // panjangSemula
		    vm.listCurrencies.splice(idx, 1);
		};

		vm.deleteRowPers = deleteRowPers;
		function deleteRowPers(index, data) {
		    var idx = index;
		    var _length = vm.listPersonal.length; // panjangSemula
		    vm.listPersonal.splice(idx, 1);
		};

		vm.loadVendorCommodity = loadVendorCommodity;
		function loadVendorCommodity(data) {
		    DataAdministrasiService.SelectVendorCommodity({ VendorID: data }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listBussinesDetailField = reply.data;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.loadBusinessField = loadBusinessField;
		vm.selectedBusinessField;
		vm.listBusinessField = [];
		function loadBusinessField() {
		    DataAdministrasiService.SelectBusinessField({
		        GoodsOrService: vm.GoodsOrService
		    },
               function (response) {
                   if (response.status === 200) {
                       vm.listBusinessField = response.data;
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_GET_BUSS");
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                   return;
               });
		}

		vm.changeBussinesField = changeBussinesField;
		function changeBussinesField() {
		    if (vm.selectedBusinessField === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.NO_TYPEFIELD");
		        return;
		    }
		    else {
		        if (vm.selectedBusinessField.Name != "Lain - lain")
		            vm.loadComodity();
		    }
		}


		vm.loadComodity = loadComodity;
		vm.selectedComodity;
		vm.listComodity = [];
		function loadComodity() {
		    if (vm.selectedBusinessField.GoodsOrService === 3091) {
		        UIControlService.msg_growl("success", "MESSAGE.NO_COMMODITY");
		        vm.listComodity = [];
		    }
		    else {
		        DataAdministrasiService.SelectComodity({ ID: vm.selectedBusinessField.ID },
                   function (response) {
                       if (response.status === 200 && response.data.length > 0) {
                           vm.listComodity = response.data;
                       }
                       else if (response.status === 200 && response.data.length < 1) {
                           UIControlService.msg_growl("success", "MESSAGE.NO_COMMODITY");
                       }
                       else {
                           UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMM");
                           return;
                       }
                   }, function (err) {
                       UIControlService.msg_growl("error", "MESSAGE.API");
                       return;
                   });
		    }
		}


		vm.addDetailBussinesField = addDetailBussinesField;
		function addDetailBussinesField() {
		    if (vm.selectedBusinessField === undefined) {
		        UIControlService.msg_growl("warning", "MESSAGE.NO_TYPEFIELD");
		        return;
		    }

		    var comID;
		    if (vm.selectedComodity === undefined) {
		        //UIControlService.msg_growl("warning", "Komoditas Belum di Pilih");
		        //return;
		        comID = null;
		    } else if (!(vm.selectedComodity === undefined)) {
		        comID = vm.selectedComodity.ID;
		    }
		    countDetailBusinessField = [];
		    for (var i = 0; i < vm.listBusinessField.length; i++) {
		        if (vm.listBusinessField[i].Name != "Lain - lain")
		            countDetailBusinessField.push(vm.listBusinessField[i]);
		    }
		    var countDetailBusinessField = vm.listBussinesDetailField.length;
		    var addPermission = false; var sameItem = true;
		    var dataDetail = {
		        VendorID: item.item,
		        CommodityID: comID,
		        BusinessFieldID: vm.selectedBusinessField.ID,
		        Commodity: vm.selectedComodity,
		        BusinessField: vm.selectedBusinessField,
		        Remark: vm.RemarkBusinessField
		    }
		    /*
		    if (vm.selectedBusinessField.Name == "Lain - lain") {
		        vm.listBussinesDetailField.push(dataDetail);
		    }*/
		    //else {

		    if (countDetailBusinessField <= 6) {
		        var countGoodsDetail = 0; var countServiceDetail = 0;
		        for (var a = 0; a < vm.listBussinesDetailField.length; a++) {

		            if (vm.listBusinessField[a].Name != "Lain - lain") {
		            if (countDetailBusinessField > 0) {
		                if (dataDetail.BusinessField.GoodsOrService === 3091) {
		                    if (vm.listBussinesDetailField[a].BusinessField.GoodsOrService === 3091) {
		                        if (vm.listBussinesDetailField[a].BusinessFieldID !== dataDetail.BusinessFieldID) {
		                            countServiceDetail = +countServiceDetail + 1;
		                        }
		                        else {
		                            UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD_SERVICE");
		                            sameItem = false;
		                        }
		                    }
		                }
		                else if (dataDetail.BusinessField.GoodsOrService === 3090) {
		                    if (vm.listBussinesDetailField[a].BusinessField.GoodsOrService === 3090) {
		                        if (vm.listBussinesDetailField[a].CommodityID !== dataDetail.CommodityID) {
		                            countGoodsDetail = +countGoodsDetail + 1;
		                        }
		                        else {
		                            UIControlService.msg_growl("warning", "MESSAGE.COMMODITY2");
		                            sameItem = false;
		                        }
		                    }
		                }
		            }
		        }
		        }
		        //barang & jasa
		        if (vm.selectedBusinessField.Name != "Lain - lain") {
		        if (vm.GoodsOrService === 3092) {
		            if (dataDetail.BusinessField.GoodsOrService === 3090) {
		                if (countGoodsDetail < 3) {
		                    addPermission = true;
		                }
		                else {
		                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT1");
		                }
		            }
		            else if (dataDetail.BusinessField.GoodsOrService === 3091) {
		                if (countServiceDetail < 5) {
		                    addPermission = true;
		                }
		                else {
		                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT2");
		                }
		            }

		            if (countDetailBusinessField === 6) {
		                UIControlService.msg_growl("warning", "MESSAGE.LIMIT3");
		                addPermission = false;
		            }
		        }
		            //barang
		        else if (vm.GoodsOrService === 3090) {
		            if (dataDetail.BusinessField.GoodsOrService === 3090) {
		                if (countGoodsDetail < 3) {
		                    addPermission = true;
		                }
		                else {
		                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT4");
		                }
		            }
		        }

		            //jasa
		        else if (vm.GoodsOrService === 3091) {
		            if (dataDetail.BusinessField.GoodsOrService === 3091) {
		                if (countServiceDetail < 5) {
		                    addPermission = true;
		                }
		                else {
		                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT5");
		                }
		            }
		        }
		        }
		        else {
		            vm.listBussinesDetailField.push(dataDetail);
		        }


		    }
		    if (addPermission === true && sameItem === true) {
		        vm.listBussinesDetailField.push(dataDetail);
		    }
		    //}
            /*
		    VerifikasiDataService.businessfieldValidation({
		        BusinessFields: vm.listBussinesDetailField,
		        VendorID: vm.administrasi.VendorID,
		        GoodsOrService: vm.selectedTypeVendor.RefID,
		        CommodityParameter: dataDetail
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        //console.info("PMS:"+JSON.stringify(reply));
		        if (reply.status === 200) {
		            vm.validasi = reply.data;
		            if (vm.validasi == true) {
		                vm.listBussinesDetailField.push(dataDetail);
		            }
		            else {
		                UIControlService.msg_growl("warning", "Gagal menambahkan bidang usaha atau komoditas.");
		            }
		            //console.info("validasi" + vm.validasi);
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
            */
		    vm.selectedComodity = undefined;

		}

		vm.deleteRow = deleteRow;
		function deleteRow(index) {
		    var idx = index - 1;
		    var _length = vm.listBussinesDetailField.length; // panjangSemula
		    vm.listBussinesDetailField.splice(idx, 1);
		};

		vm.loadSelectedBusinessEntity = loadSelectedBusinessEntity;
		function loadSelectedBusinessEntity(selectedBE) {
		    vm.selectedBE = vm.selectedBusinessEntity.Description;
		    if (vm.selectedBE === "CV") {
		        for (var i = 0; i < vm.listTypeVendor.length; i++) {
		            if (vm.listTypeVendor[i].Value === "VENDOR_TYPE_GOODS") {
		                vm.listTV = vm.listTypeVendor[i];
		                i = vm.listTypeVendor.length;
		            }
		        }
		        vm.listTypeVendor = {};
		        vm.listTypeVendor[0] = vm.listTV;
		        changeTypeVendor();
		    }
		    else if (vm.selectedBE !== "CV") {
		        loadTypeVendor();
		    }
		}

		function showSelectedTypeVendor(selectedBE) {
		    DataAdministrasiService.getTypeVendor(function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listTypeVendor = reply.data.List;
		            for (var i = 0; i < vm.listTypeVendor.length; i++) {
		                if (vm.user.VendorTypeID === vm.listTypeVendor[i].VendorTypeID) {
		                    vm.selectedTypeVendor = vm.listTypeVendor[i];
		                    changeTypeVendor();
		                    break;
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.loadBusinessEntity = loadBusinessEntity;
		vm.selectedBusinessEntity;
		vm.listBusinessEntity = [];
		function loadBusinessEntity(data1) {
		    DataAdministrasiService.SelectBusinessEntity(
               function (response) {
                   if (response.status === 200) {
                       vm.listBusinessEntity = response.data;
                       for (var i = 0; i < vm.listBusinessEntity.length; i++) {
                           if (data1.business.BusinessID === vm.listBusinessEntity[i].BusinessID) {
                               vm.selectedBusinessEntity = vm.listBusinessEntity[i];
                               break;
                           }
                       }
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
                       return;
                   }
               }, function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                   return;
               });
		}

		vm.loadCurrencies = loadCurrencies;
		vm.listCurrencies = [];
		var CurrencyID;
		function loadCurrencies(data) {
		    VerifikasiDataService.getCurrencies({VendorID: vm.data},
            function (response) {
                if (response.status === 200) {
                    vm.listCurrencies = response.data;

                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
		}

		vm.loadAllCurrencies = loadAllCurrencies;
		vm.listAllCurrencies = [];
		function loadAllCurrencies() {
		    VerifikasiDataService.getAllCurrencies(
            function (response) {
                if (response.status === 200) {
                    vm.listAllCurrencies = response.data;

                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
		}

		vm.loadAssociation = loadAssociation;
		vm.selectedAssociation;
		vm.listAssociation = [];
		function loadAssociation(data) {
		    DataAdministrasiService.getAssociation({
		        Offset: 0,
		        Limit: 0,
		        Keyword: ""
		    },
            function (response) {
                if (response.status === 200) {
                    vm.listAssociation = response.data.List;
                    for (var i = 0; i < vm.listAssociation.length; i++) {
                        if (data.AssociationID === vm.listAssociation[i].AssosiationID) {
                            vm.selectedAssociation = vm.listAssociation[i];
                            break;
                        }
                    }
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
		}

		vm.loadRegion = loadRegion;
		vm.selectedRegion;
		vm.listRegion = [];
		function loadRegion(countryID) {
		    DataAdministrasiService.SelectRegion({ CountryID: countryID },
           function (response) {
               vm.listRegion = response.data;
           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadCountry = loadCountry;
		vm.selectedCountry;
		vm.listCountry = [];
		function loadCountry(data) {
		    DataAdministrasiService.SelectCountry(
           function (response) {
               vm.listCountry = response.data;
               for (var i = 0; i < vm.listCountry.length; i++) {
                   if (data.CountryID === vm.listCountry[i].CountryID) {
                       vm.selectedCountry = vm.listCountry[i];
                       loadState(data);
                       break;
                   }
               }


           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadState = loadState;
		vm.selectedState;
		vm.listState = [];
		function loadState(data) {
		    if (!data) {
		        data = vm.selectedCountry;
		        vm.selectedState = "";
		        vm.selectedCity = "";
		        vm.selectedDistrict = "";
		        vm.selectedState1 = "";
		    }
		    loadRegion(data.CountryID);

		    DataAdministrasiService.SelectState(data.CountryID,
           function (response) {
               vm.listState = response.data;
               for (var i = 0; i < vm.listState.length; i++) {
                   if (vm.selectedState1 !== "" && vm.selectedState1.StateID === vm.listState[i].StateID) {
                       vm.selectedState = vm.listState[i];
                       if (vm.selectedState.Country.Code === 'IDN') {
                           loadCity(vm.selectedState);
                           break;
                       }
                   }
               }


           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadCity = loadCity;
		vm.selectedCity;
		vm.listCity = [];
		function loadCity(data) {
		    if (!data) {

		        data = vm.selectedState;
		        vm.selectedCity = "";
		        vm.selectedCity1 = "";
		        vm.selectedDistrict = "";
		    }
		    DataAdministrasiService.SelectCity(data.StateID,
           function (response) {
               vm.listCity = response.data;
               for (var i = 0; i < vm.listCity.length; i++) {
                   if (vm.selectedCity1 !== "" && vm.selectedCity1.CityID === vm.listCity[i].CityID) {
                       vm.selectedCity = vm.listCity[i];
                       if (vm.selectedState.Country.Code === 'IDN') {
                           loadDistrict(vm.selectedCity);
                           break;
                       }
                   }
               }
           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadDistrict = loadDistrict;
		vm.selectedDistrict;
		vm.listDistrict = [];
		function loadDistrict(city) {
		    if (!city) {
		        city = vm.selectedCity;
		        vm.selectedDistrict = "";
		        vm.selectedDistrict1 = "";

		    }
		    DataAdministrasiService.SelectDistrict(city.CityID,
           function (response) {
               vm.listDistrict = response.data;
               for (var i = 0; i < vm.listDistrict.length; i++) {
                   if (vm.selectedDistrict1 !== "" && vm.selectedDistrict1.DistrictID === vm.listDistrict[i].DistrictID) {
                       vm.selectedDistrict = vm.listDistrict[i];
                       break;
                   }
               }
           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadRegionAlternatif = loadRegionAlternatif;
		vm.selectedRegionAlternatif;
		vm.listRegionAlternatif = [];
		function loadRegionAlternatif(countryID) {
		    DataAdministrasiService.SelectRegion({ CountryID: countryID },
           function (response) {
               vm.listRegionAlternatif = response.data;
           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadCountryAlternatif = loadCountryAlternatif;
		vm.selectedCountryAlternatif;
		vm.listCountryAlternatif = [];
		function loadCountryAlternatif(data) {
		    DataAdministrasiService.SelectCountry(
           function (response) {
               vm.listCountryAlternatif = response.data;
               for (var i = 0; i < vm.listCountryAlternatif.length; i++) {
                   if (data !== undefined) {
                       if (data.CountryID === vm.listCountryAlternatif[i].CountryID) {
                           vm.selectedCountryAlternatif = vm.listCountryAlternatif[i];
                           loadStateAlternatif(data);
                           break;
                       }

                   }
               }


           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadStateAlternatif = loadStateAlternatif;
		vm.selectedStateAlternatif;
		vm.listStateAlternatif = [];
		function loadStateAlternatif(data) {
		    if (!data) {
		        data = vm.selectedCountryAlternatif;
		        vm.selectedStateAlternatif = "";
		        vm.selectedCityAlternatif = "";
		        vm.selectedDistrictAlternatif = "";
		        vm.selectedStateAlternatif1 = "";
		    }
		    loadRegionAlternatif(data.CountryID);

		    DataAdministrasiService.SelectState(data.CountryID,
           function (response) {
               vm.listStateAlternatif = response.data;
               for (var i = 0; i < vm.listStateAlternatif.length; i++) {
                   if (vm.selectedStateAlternatif1 !== "" && vm.selectedStateAlternatif1.StateID === vm.listStateAlternatif[i].StateID) {
                       vm.selectedStateAlternatif = vm.listStateAlternatif[i];
                       if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
                           loadCityAlternatif(vm.selectedStateAlternatif);
                           break;
                       }
                   }
               }


           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadCityAlternatif = loadCityAlternatif;
		vm.selectedCityAlternatif;
		vm.listCityAlternatif = [];
		function loadCityAlternatif(data) {
		    if (!data) {

		        data = vm.selectedStateAlternatif;
		        vm.selectedCityAlternatif = "";
		        vm.selectedCityAlternatif1 = "";
		        vm.selectedDistrictAlternatif = "";
		    }
		    DataAdministrasiService.SelectCity(data.StateID,
           function (response) {
               vm.listCityAlternatif = response.data;
               for (var i = 0; i < vm.listCityAlternatif.length; i++) {
                   if (vm.selectedCityAlternatif1 !== "" && vm.selectedCityAlternatif1.CityID === vm.listCityAlternatif[i].CityID) {
                       vm.selectedCityAlternatif = vm.listCityAlternatif[i];
                       if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
                           loadDistrictAlternatif(vm.selectedCityAlternatif);
                           break;
                       }
                   }
               }
           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadDistrictAlternatif = loadDistrictAlternatif;
		vm.selectedDistrictAlternatif;
		vm.listDistrictAlternatif = [];
		function loadDistrictAlternatif(city) {
		    if (!city) {
		        city = vm.selectedCityAlternatif;
		        vm.selectedDistrictAlternatif = "";
		        vm.selectedDistrictAlternatif1 = "";

		    }
		    DataAdministrasiService.SelectDistrict(city.CityID,
           function (response) {
               vm.listDistrictAlternatif = response.data;
               for (var i = 0; i < vm.listDistrictAlternatif.length; i++) {
                   if (vm.selectedDistrictAlternatif1 !== "" && vm.selectedDistrictAlternatif1.DistrictID === vm.listDistrictAlternatif[i].DistrictID) {
                       vm.selectedDistrictAlternatif = vm.listDistrictAlternatif[i];
                       break;
                   }
               }
           }, function (err) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.changeSubCOA = changeSubCOA;
		function changeSubCOA(param) {
		    VerifikasiDataService.getUnit(function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listUnit = reply.data.List;
		            if (param.Name === 'SUB_CASH_TYPE2') {
		                vm.selectedUnit = vm.listUnit[0];
		            }
		            else if (param.Name === 'SUB_CASH_TYPE3') {
		                vm.selectedUnit = vm.listUnit[2];
		            }
		            else if (param === 0) {
		                vm.selectedUnit = vm.listUnit[1];
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.selectedAsset;
		vm.listAsset;
		vm.asset = 0;
		vm.hutang = 0;
		function loadAsset() {
		    var defer = $q.defer();

		    VerifikasiDataService.selectNeraca({ VendorID: vm.id }, function (reply) {
		        //UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listAsset = reply.data;

		            //for (var i = 0; i < vm.listAsset.length; i++) {
		            //    if (vm.data.Wealth.RefID === vm.listAsset[i].RefID) {
		            //        vm.selectedAsset = vm.listAsset[i];
		            //        loadCOA(vm.selectedAsset);
		            //        break;
		            //    }
		            //}

		            for (var i = 0; i < vm.listAsset.length; i++) {
		                if (vm.listAsset[i].listDataBalanceModel != null) {
		                    if (vm.listAsset[i].BalanceName == "WEALTH_TYPE_ASSET") {
		                        for (var c = 0; c < vm.listAsset[i].listDataBalanceModel.length; c++) {
		                            for (var g = 0; g < vm.listAsset[i].listDataBalanceModel[c].subCategory.length; g++) {
		                                vm.asset += Number(vm.listAsset[i].listDataBalanceModel[c].subCategory[g].Nominal);
		                            }
		                        }
		                    }
		                    if (vm.listAsset[i].BalanceName == "WEALTH_TYPE_DEBTH") {
		                        for (var c = 0; c < vm.listAsset[i].listDataBalanceModel.length; c++) {
		                            for (var g = 0; g < vm.listAsset[i].listDataBalanceModel[c].subCategory.length; g++) {
		                                vm.hutang += Number(vm.listAsset[i].listDataBalanceModel[c].subCategory[g].Nominal);
		                            }
		                        }
		                    }
		                }

		                if (vm.listAsset[i].BalanceName == "WEALTH_TYPE_MODAL") {
		                    vm.listAsset.splice(i, 1);
		                }

		                defer.resolve(true);

		            }

		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		        defer.reject();
		    });
		    return defer.promise;
		}

		function getSisaBalance() {

		    VerifikasiDataService.getSisaMstBalance(
            { assetBalance: vm.asset, hutangBalance: vm.hutang },
            function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    var dataAsset = data[0];
                    var dataHutang = data[1];
                    UIControlService.unloadLoadingModal();

                    for (var i = 0; i < dataAsset.length; i++) {
                        vm.listAsset.push(dataAsset[i]);
                    }
                    for (var i = 0; i < dataHutang.length; i++) {
                        vm.listAsset.push(dataHutang[i]);
                    }

                    UIControlService.unloadLoadingModal();

                    vm.selectedAsset = [];
                    for (var i = 0; i < vm.listAsset.length; i++) {
                        if (item.item.Wealth.Name === vm.listAsset[i].BalanceName) {
                            vm.selectedAsset = vm.listAsset[i];
                            loadCOA(vm.selectedAsset);
                            break;
                        }
                    }
                    


                    console.log(data)
                }
            }, function (err) {
                $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                UIControlService.unloadLoadingModal();
            })
		}

		vm.loadCOA = loadCOA;
		vm.selectedCOA;
		vm.disableLU;
		vm.listCOA = [];
		function loadCOA(data) {
		    vm.param = "";
		    if (data.BalanceName === 'WEALTH_TYPE_ASSET') {
		        vm.param = "COA_TYPE_ASSET"
		    }
		    else if (data.BalanceName === 'WEALTH_TYPE_DEBTH') {
		        vm.param = "COA_TYPE_DEBTH"
		    }
		    vm.selectedSubCOA;

		    VerifikasiDataService.getCOA({
		        Keyword: vm.param
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listCOA = reply.data.List;
		            for (var i = 0; i < vm.listCOA.length; i++) {
		                if (vm.data.COA.RefID === vm.listCOA[i].RefID) {
		                    vm.selectedCOA = vm.listCOA[i];
		                    loadSubCOA(vm.selectedCOA);
		                    break;
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}


		vm.loadSubCOA = loadSubCOA;
		vm.selectedSubCOA;
		vm.listSubCOA;
		function loadSubCOA(data) {
		    vm.param = "";
		    if (data.RefID === 3100) {
		        vm.param = "SUB_COA_CASH"
		    }
		    else if (data.RefID === 3101) {
		        vm.param = "SUB_COA_DEBTHSTOCK"
		    }
		    VerifikasiDataService.getSubCOA({
		        Keyword: vm.param
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listSubCOA = reply.data.List;
		            var param = vm.listSubCOA.length;
		            if (param === 0) {
		                changeSubCOA(param);
		            }
		            for (var i = 0; i < vm.listSubCOA.length; i++) {
		                if (vm.data.SubCOA != null) {
		                    if (vm.data.SubCOA.RefID === vm.listSubCOA[i].RefID) {
		                        vm.selectedSubCOA = vm.listSubCOA[i];
		                        break;
		                    }

		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.selectedUnit;
		vm.listUnit;
		function loadUnit() {
		    var defer = $q.defer();
		    VerifikasiDataService.getUnit(function (reply) {
		        //UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.listUnit = reply.data.List;
		            for (var i = 0; i < vm.listUnit.length; i++) {
		                if (vm.data.Unit.RefID === vm.listUnit[i].RefID) {
		                    vm.selectedUnit = vm.listUnit[i];
		                    break;
		                }
		            }
		            defer.resolve(true);
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
                defer.reject()
		    });
		    return defer.promise;
		}

		vm.loadDetailVendor = loadDetailVendor;
		function loadDetailVendor() {
		    VerifikasiDataService.GetDetailVendor({VendorID: vm.data.VendorID},function (reply) {
		        if (reply.status === 200) {
		            if (!(reply.data === null)) {
		                vm.vendorName = reply.data.Name;
		                vm.vendorID = reply.data.VendorID;
		                vm.businessID = reply.data.BusinessID;
		                vm.businessName = reply.data.BusinessName;
		                vm.vendorNpwp = reply.data.Npwp;
		                vm.vendorAddress = reply.data.AddressInfo + " , " + reply.data.AddressDetail;
		                UIControlService.unloadLoadingModal();

		            }
		        } else {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_USER');
		        }
		    }, function (error) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_USER');
		    });

		    VerifikasiDataService.GetCities(function (reply) {
		        if (reply.status === 200) {
		            vm.listKotaKab = reply.data;
		            for (var i = 0; i < vm.listKotaKab.length; i++) {
		                if (vm.data.NotaryLocation === vm.listKotaKab[i].CityID) {
		                    vm.selectedNotaryLocation = vm.listKotaKab[i];
		                }
		            }
		        } else {
		            UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD_CITIES');
		        }
		    }, function (err) {
		        UIControlService.msg_growl('error', 'MESSAGE.ERR_LOAD_CITIES');
		    });
		    loadConfigAkta();
		}

		vm.changeNotarisPlace = changeNotarisPlace;
		function changeNotarisPlace() {
		    vm.data.NotaryLocation = vm.selectedNotaryLocation.CityID;
		}
		vm.loadConfigAkta = loadConfigAkta;
		function loadConfigAkta(){
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.LEGALDOCS", function (response) {
		        if (response.status == 200) {
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
		    });
		}

		vm.loadConfigStock = loadConfigStock;
		function loadConfigStock() {
		    UIControlService.loadLoadingModal();
		    UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.ID", function (response) {
		        if (response.status == 200) {
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];
		            VerifikasiDataService.GetCurrencies(
                        function (response) {
                            vm.currencyList = response.data;
                            VerifikasiDataService.getUploadPrefix(
                                function (response) {
                                    var prefixes = response.data;
                                    vm.prefixes = {};
                                    for (var i = 0; i < prefixes.length; i++) {
                                        vm.prefixes[prefixes[i].Name] = prefixes[i];
                                    }
                                    UIControlService.unloadLoadingModal();
                                }, function (error) {
                                    UIControlService.unloadLoadingModal();
                                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_PREFIXES')
                                }
                            );
                        },
                        function (error) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_CURRENCY')
                        }
                    );
		        } else {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
		    });
		}

		vm.onUnitChange = onUnitChange;
		function onUnitChange() {
		    vm.showCurrencyField = false;
		    for (var i = 0; i < vm.stockUnits.length; i++) {
		        if (vm.stockUnits[i].RefID === vm.stock.UnitID) {
		            vm.showCurrencyField = vm.stockUnits[i].Name === 'STOCK_UNIT_CURRENCY';
		            break;
		        }
		    }
		}

		function generateFilterStrings(allowedTypes) {
		    var filetypes = "";
		    for (var i = 0; i < allowedTypes.length; i++) {
		        filetypes += "." + allowedTypes[i].Name + ",";
		    }
		    return filetypes.substring(0, filetypes.length - 1);
		}

		function handleRequestError(response) {
		    UIControlService.log(response);
		    UIControlService.handleRequestError(response.data, response.status);
		    UIControlService.unloadLoadingModal();
		}

		vm.loadStockUnits = loadStockUnits;
		function loadStockUnits(data) {
		    VerifikasiDataService.getStockTypes(
                function (response) {
                    vm.stockUnits = response.data;
                    if (data !== undefined) {
                        for (var i = 0; i < vm.stockUnits.length; i++) {
                            if (vm.stockUnits[i].RefID === data.Unit.RefID) {
                                vm.data.Unit = vm.stockUnits[i];
                            }

                        }

                    }
                    UIControlService.unloadLoadingModal();
                },
                handleRequestError);
		}

        /*get username*/
		function getUsLogin() {
			AuthService.getUserLogin(function (reply) {
			    vm.VendorLogin = reply.data.CurrentUsername;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});
		}

		vm.selectedIssuedDate = selectedIssuedDate;
		function selectedIssuedDate(sid) {
		    vm.sid = sid;
		}

		vm.selectedExpiredDate = selectedExpiredDate;
		function selectedExpiredDate(sed) {

		    if (sed > vm.sid) {
		    }
		    else if (sed < vm.sid) {
		        vm.data.ExpiredDate = moment();
		    }
		}

	    /*get city by id*/
		function getCityByID(id) {

		    ProvinsiService.getCityByID({ column: id }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        var data = reply.data.List[0];
		        vm.selectedCities = data;
		        vm.selectedState = data.State;
		        changeState(data.State.StateID);
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		/*open form date*/
		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		/*get combo klasifikasi*/
		vm.listClasification = [];
		vm.selectedClasification;
		function loadKlasifikasi() {
			//alert("load");
		    VerifikasiDataService.getClasification(function (reply) {
				UIControlService.unloadLoadingModal();
				vm.dataClass = reply.data.List;
				for (var i = 0; i < vm.dataClass.length; i++) {
				    if (vm.dataClass[i].Name != "ALL_COMPANY") {
				        vm.listClasification.push(vm.dataClass[i]);

				        if (vm.kualifikasi != '') {
				            if (vm.dataClass[i].RefID == vm.kualifikasi) {
				                vm.CompanyScaleName = vm.dataClass[i].Value;
				            }
				        }

				    }
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}
	   
	    /* combo Izin Usaha Load */
		vm.changeCountry = changeCountry;
		vm.listState = [];
		vm.selectedState;
		function changeCountry(idstate) {
		    if (idstate != undefined) {
		        ProvinsiService.getStates(idstate,
               function (response) {
                   vm.listState = response.data;

                   if (dataModal.IssuedState != undefined) {
                       changeState();

                       for (var i = 0; i < vm.listState.length; i++) {
                           if (vm.listState[i].StateID == vm.provinsi) {
                               vm.provinsiName = vm.listState[i].Name;
                           }
                       }
                   }
               },
               function (response) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                   return;
               });
		    }
		    
		}

		vm.changeState = changeState;
		vm.listCities = [];
		vm.selectedCities;
		function changeState() {
		    vm.city = '';
		    if (vm.provinsi != undefined) {
		        UIControlService.loadLoadingModal();
		        ProvinsiService.getCities(vm.provinsi, function (response) {
		            UIControlService.unloadLoadingModal();
		            vm.listCities = response.data;
		            if (vm.isEdit) {
		                vm.city = dataModal.IssuedLocation == undefined ? '' : dataModal.IssuedLocation;
		            }

		            if (vm.city != '') {
		                for (var i = 0; i < vm.listCities.length; i++) {
		                    if (vm.listCities[i].CityID == vm.city) {
		                        vm.cityName = vm.listCities[i].Name;
		                    }
		                }
		            }
		        }, function (response) {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            return;
		        });
		    }
		    
		}

		vm.changeJenisIzin = changeJenisIzin;
		function changeJenisIzin() {
		    UIControlService.loadLoadingModal();

		    VerifikasiDataService.getJenisIzinUsaha({
		        LicenseType: vm.tipeLicense
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status == 200) {
		            var data = reply.data;
		            vm.dataSelectIzinUsaha = data;
		            if (dataModal.LicenseID != undefined) {
		                for (var i = 0; i < data.length; i++) {
		                    if (data[i].ID == dataModal.LicenseID) {
		                        vm.jenisIzinUsaha = data[i];
		                    }
		                }

		            }
		            console.log(data)
		        } else {
		            UIControlService.msg_growl('error', "ERRORS.ERROR_API");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl('error', "ERRORS.ERROR_API");
		    })
		}

		function getVendorCommodity(current) {
		    //vm.currentPage = current;
		    //var offset = (current * 10) - 10;
		    //,
		    //Offset: offset,
		    //Limit: vm.pageSize
		    UIControlService.loadLoadingModal();
		    VerifikasiDataService.getVendorCommodityBy(
                {
                    IntParam1: vm.VendorLicenseID,
                    IntParam2: dataModal.LicenseID
                },
                function (reply) {
                    //UIControlService.unloadLoadingModal();
                    var data = reply.data.List;
                    vm.dataBidangUsaha = data;
                    console.log(data)
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
		}

		vm.changeCities = changeCities;
		function changeCities() {
		    vm.data.IssuedLocation = vm.selectedCities.CityID;
        }
        /* end combo country, state, city*/

	    /*get type n size file upload*/
        vm.selectUpload = selectUpload;
        vm.fileUpload;
        function selectUpload() {
            //vm.fileUpload = vm.fileUpload;
        }

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.VENDOR.LICENSI", function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				return;
			});
		}

		vm.CheckEmail = CheckEmail;
		function CheckEmail() {
		    UIControlService.loadLoadingModal("Check Email . . .");
		    if (vm.EmailPers !== '') {
		        var data = {
		            Keyword: vm.EmailPers
		        };
		        VerifikasiDataService.checkEmail(data,
                        function (response) {
                            vm.EmailAvailable = response.data;
                            if (vm.EmailAvailable) {
                                UIControlService.unloadLoadingModal();
                                UIControlService.msg_growl('error', 'MESSAGE.VALIDATION_ERRORS_EMAIL_AVAILABLE', 'MESSAGE.VALIDATION_ERRORS_EMAIL_AVAILABLE_TITLE');

                                vm.NamePers = undefined;
                                vm.PhonePers = undefined;
                                vm.EmailPers = undefined;
                            }
                            else {
                                vm.addContactPers();
                            }
                        }, handleRequestError);
		    }
		}

		vm.addContactPers = addContactPers;
		function addContactPers() {
		    vm.listPersonal.push({
		        Contact: {
		            ContactID: 0,
		            Name: vm.NamePers,
		            Phone: vm.PhonePers,
		            Email: vm.EmailPers
		        },
		        IsActive: true
		    });
		    vm.NamePers = undefined;
		    vm.PhonePers = undefined;
		    vm.EmailPers = undefined;

		    UIControlService.unloadLoadingModal();
		}

		vm.addtolist = addtolist;
		vm.vendor = {};
		vm.listcontact = [];
		function addtolist(data1, data2) {
		    if (!vm.selectedCityAdmin && !vm.selectedDistrictAdmin) {
		        var addressComp = {
		            AddressID: vm.addressIdComp,
		            StateID: vm.selectedStateAdmin.StateID
		        }
		    }
		    else {
		        var addressComp = {
		            AddressID: vm.addressIdComp,
		            StateID: vm.selectedStateAdmin.StateID,
		            CityID: vm.selectedCityAdmin.CityID,
		            DistrictID: vm.selectedDistrictAdmin.DistrictID
		        }
		    }
		    var contactdt = {
		        VendorContactType: vm.VendorContactTypeCompany,
		        Contact: {
		            ContactID: vm.ContactID,
		            Email: vm.Email,
		            Phone: vm.phoneCode ? '(' + vm.phoneCode.PhonePrefix + ') ' + vm.Phone : null,
		            Website: vm.Website,
		            Fax: vm.fax,
		            Address: addressComp
		        }
		    }
		    vm.listcontact.push(contactdt);
		    vm.address2 = [];
		    var count = 0;
		    for (var j = 0; j < vm.addresses.length; j++) {
		        if (vm.addresses[j].countryName == "Indonesia") {
		            if (vm.addresses[j].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                count++;
		                vm.ContactCenterOffice = vm.addresses[j].ContactID;
		                vm.address = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    DistrictID: vm.addresses[j].districtID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		            } else {
		                var par = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    DistrictID: vm.addresses[j].districtID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		                vm.address2.push(par);
		            }
		        } else {
		            if (vm.addresses[j].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                count++;
		                vm.ContactCenterOffice = vm.addresses[j].ContactID;
		                vm.address = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		            } else {
		                var par = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		                vm.address2.push(par);
		            }
		        }


		        //} else {


		        //}
		    }
		    if (count == 0) {
		        count = 0;
		        UIControlService.msg_growl("error", "Kantor pusat harus diisi");
		        return;
		    }

		    var VendorContactTypeBranch = {
		        Name: "VENDOR_OFFICE_TYPE_BRANCH",
		        RefID: 1043,
		        Type: "VENDOR_OFFICE_TYPE",
		        Value: "VENDOR_OFFICE_TYPE_BRANCH",
		        VendorContactTypeID: 1043
		    };
		    var VendorContactTypeMain = {
		        Name: "VENDOR_OFFICE_TYPE_MAIN",
		        RefID: 1042,
		        Type: "VENDOR_OFFICE_TYPE",
		        Value: "VENDOR_OFFICE_TYPE_MAIN",
		        VendorContactTypeID: 1042
		    };
		    if (vm.address.AddressInfo != "") {
		        var contact = {
		            ContactID: vm.ContactCenterOffice,
		            Address: vm.address
		        }
		        var contactdt = {
		            VendorContactType: VendorContactTypeMain,
		            Contact: contact,
		            IsEdit: vm.IsEdit
		        }
		        vm.listcontact.push(contactdt);
		    }
		    /*
			if (vm.addressInfo != '' && vm.selectedCountryAlternatif == undefined) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_LIMITBUSINESSFIELD");
			    return;
			}*/
		    //if (vm.selectedCountryAlternatif != undefined && vm.addressInfo != ' ') {
		    //if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {


		    //} else {


		    //}
		    if (vm.AddressRemovedMain.length > 0) {
		        if (vm.AddressRemovedMain[0].ContactID != 0) {
		            var contactRemoved = {
		                ContactID: vm.AddressRemovedMain[0].ContactID,
		                Address: vm.AddressRemovedMain[0]
		            }

		            var contactdtRemoved = {
		                VendorContactType: VendorContactTypeMain,
		                Contact: contactRemoved,
		                IsEdit: vm.IsEdit,
		                IsRemoved: true
		            }

		            vm.listcontact.push(contactdtRemoved);
		        }
		    }
		    vm.AddressRemovedBranch.forEach(function (item) {
		        if (item.ContactID != 0) {
		            var contactRemovedBranch = {
		                ContactID: item.ContactID,
		                Address: item
		            }

		            var contactdtRemovedBranch = {
		                VendorContactType: VendorContactTypeBranch,
		                Contact: contactRemovedBranch,
		                IsEdit: vm.IsEdit,
		                IsRemoved: true
		            }

		            vm.listcontact.push(contactdtRemovedBranch);
		        }
		    });

		    vm.address2.forEach(function (item) {
		        if (vm.AddressAlterId == 0) {
		            var contact = {
		                Name: vm.Name,
		                ModifiedBy: vm.administrasi.user.Username,
		                Address: item
		            }
		        } else {
		            var contact = {
		                ContactID: item.ContactID,
		                ModifiedBy: vm.administrasi.user.Username,
		                Address: item
		            }
		        }
		        if (vm.VendorContactTypeAlter == undefined) {
		            var contactdt = {
		                VendorContactType: VendorContactTypeBranch,
		                Contact: contact,
		                IsPrimary: 2,
		                IsEdit: vm.IsEditAlter
		            }
		        } else {
		            var contactdt = {
		                VendorContactType: VendorContactTypeBranch,
		                Contact: contact,
		                IsPrimary: 2,
		                IsEdit: vm.IsEditAlter
		            }
		        }

		        vm.listcontact.push(contactdt);
		    });

		    //if (vm.selectedCity == undefined && vm.selectedDistrict == undefined) {
		        
		    //    vm.address = {
		    //        AddressID: vm.AddressId,
		    //        AddressInfo: vm.address1,
		    //        PostalCode: vm.postcalcode,
		    //        StateID: vm.selectedState.StateID
		    //    }
		    //} else {
		    //    vm.address = {
		    //        AddressID: vm.AddressId,
		    //        AddressInfo: vm.address1,
		    //        PostalCode: vm.postcalcode,
		    //        StateID: vm.selectedState.StateID,
		    //        CityID: vm.selectedCity.CityID,
		    //        DistrictID: vm.selectedDistrict.DistrictID
		    //    }
		    //}
		    //var contact = {
		    //    ContactID: vm.ContactOfficeId,
		    //    Address: vm.address
		    //}
		    //var contactdt = {
		    //    VendorContactType: vm.VendorContactType,
		    //    Contact: contact,
		    //    IsEdit: vm.IsEdit
		    //}
		    //vm.listcontact.push(contactdt);

		    //if (vm.selectedCountryAlternatif !== undefined) {
		    //    if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {
		    //        vm.address2 = {
		    //            AddressID: vm.AddressAlterId,
		    //            AddressInfo: vm.AddressInfo,
		    //            PostalCode: vm.PostalCodeAlternatif,
		    //            StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null

		    //        }
		    //    } else {
		    //        vm.address2 = {
		    //            AddressID: vm.AddressAlterId,
		    //            AddressInfo: vm.AddressInfo,
		    //            PostalCode: vm.PostalCodeAlternatif,
		    //            StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null,
		    //            CityID: vm.selectedCityAlternatif.CityID,
		    //            DistrictID: vm.selectedDistrictAlternatif.DistrictID
		    //        }
		    //    }
		    //    if (vm.AddressAlterId == 0) {
		    //        var contact = {
		    //            Name: vm.Name,
		    //            ModifiedBy: vm.administrasi.user.Username,
		    //            Address: vm.address2
		    //        }
		    //    } else {
		    //        var contact = {
		    //            ContactID: vm.ContactOfficeAlterId,
		    //            ModifiedBy: vm.administrasi.user.Username,
		    //            Address: vm.address2
		    //        }
		    //    }
		    //    if (vm.VendorContactTypeAlter == undefined) {
		    //        var contactdt = {
		    //            VendorContactType: vm.VendorContactType,
		    //            Contact: contact,
		    //            IsPrimary: 2,
		    //            IsEdit: vm.IsEditAlter
		    //        }
		    //    } else {
		    //        var contactdt = {
		    //            VendorContactType: vm.VendorContactTypeAlter,
		    //            Contact: contact,
		    //            IsPrimary: 2,
		    //            IsEdit: vm.IsEditAlter
		    //        }
		    //    }

		    //    vm.listcontact.push(contactdt);
		        vm.listcontact.push(vm.contactpersonal);
		    //}
		    for (var i = 0; i < vm.listPersonal.length; i++) {
		        var contactdt = {
		            VendorContactType: vm.VendorContactTypePers,
		            Contact: {
		                ContactID: vm.listPersonal[i].Contact.ContactID,
		                Email: vm.listPersonal[i].Contact.Email,
		                Phone: vm.listPersonal[i].Contact.Phone,
		                Name: vm.listPersonal[i].Contact.Name
		            },
		            IsActive: true,
		            IsEdit: vm.listPersonal[i].IsEdit
		        }
		        vm.listcontact.push(contactdt);
		    }
		    for (var i = 0; i < vm.listPersFalse.length; i++) {
		        var contactdt = {
		            VendorContactType: vm.VendorContactTypePers,
		            Contact: {
		                ContactID: vm.listPersFalse[i].Contact.ContactID,
		                Email: vm.listPersFalse[i].Contact.Email,
		                Phone: vm.listPersFalse[i].Contact.Phone,
		                Name: vm.listPersFalse[i].Contact.Name
		            },
		            IsActive: false
		        }
		        vm.listcontact.push(contactdt);
		    }
		    for (var i = 0; i < vm.listCurrFalse.length; i++) {
		        vm.listCurrencies.push(vm.listCurrFalse[i]);
		    }

		    var asoc;
		    if (vm.selectedAssociation === undefined) {
		        asoc = null;
		    } else {
		        asoc = vm.selectedAssociation.AssosiationID
		    }

		    if (vm.selectedSupplier === null) {
		        vm.selectedSupplier = {
		            RefID: null
		        };
		    }
		    if (vm.CountryCode !== 'IDN') {
		        vm.selectedBusinessEntity = {
		            BusinessID: null
		        };
		    }
		    vm.vendor = {
		        SupplierID: data2,
		        NpwpUrl: vm.NpwpUrl,
		        VendorName: vm.administrasi.VendorName,
		        VendorID: item.item,
		        user: {
		            Username: vm.Username
		        },
		        FoundedDate: UIControlService.getStrDate(vm.administrasiDate.StartDate),
		        BusinessID: vm.selectedBusinessEntity.BusinessID,
		        PKPNumber: vm.PKPNumber,
		        PKPUrl: vm.PKPUrl,
		        ModifiedBy: vm.administrasi.user.Username,
		        AssociationID: asoc,
		        Contacts: vm.listcontact,
		        commodity: vm.listBussinesDetailField,
		        VendorTypeID: data1,
		        Currency: vm.listCurrencies

		    }
		    //DataAdministrasiService.insert(vm.vendor, function (reply) {
		    //    //console.info("reply" + JSON.stringify(reply))
		    //    UIControlService.unloadLoadingModal();
		    //    if (reply.status === 200) {
		    //        UIControlService.msg_growl("success", "Berhasil Simpan Data Administrasi !!");
		    //        $uibModalInstance.close();
		    //    } else {
		    //        UIControlService.msg_growl("error", "Gagal menyimpan data!!");
		    //        return;
		    //    }
		    //}, function (err) {
		    //    UIControlService.msg_growl("error", "Gagal Akses Api!!");
		    //    UIControlService.unloadLoadingModal();
		    //});



		}

		vm.changeNpwp = changeNpwp;
		function changeNpwp(data) {
		    vm.NpwpNew = data;
		}

		vm.saveAllAdmin = saveAllAdmin;
		function saveAllAdmin(data1, data2) {
		    UIControlService.loadLoadingModal("Loading...");
		    if (vm.NpwpNew == "") {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", "MESSAGE.NPWP_FILL");
		        return;
		    }
		    if (!vm.selectedCityAdmin && !vm.selectedDistrictAdmin) {
		        var addressComp = {
		            AddressID: vm.addressIdComp,
		            StateID: vm.selectedStateAdmin.StateID
		        }
		    }
		    else {
		        var addressComp = {
		            AddressID: vm.addressIdComp,
		            StateID: vm.selectedStateAdmin.StateID,
		            CityID: vm.selectedCityAdmin.CityID,
		            DistrictID: vm.selectedDistrictAdmin.DistrictID
		        }
		    }
		    var contactdt = {
		        VendorContactType: vm.VendorContactTypeCompany,
		        Contact: {
		            ContactID: vm.ContactID,
		            Email: vm.Email,
		            Phone: vm.phoneCode ? '(' + vm.phoneCode.PhonePrefix + ') ' + vm.Phone : null,
		            Website: vm.Website,
		            Fax: vm.fax,
		            Address: addressComp
		        }
		    }
		    vm.listcontact.push(contactdt);
		    vm.address2 = [];
		    var count = 0;
		    for (var j = 0; j < vm.addresses.length; j++) {
		        if (vm.addresses[j].countryName == "Indonesia") {
		            if (vm.addresses[j].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                count++;
		                vm.ContactCenterOffice = vm.addresses[j].ContactID;
		                vm.address = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    DistrictID: vm.addresses[j].districtID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		            } else {
		                var par = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    DistrictID: vm.addresses[j].districtID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		                vm.address2.push(par);
		            }
		        } else {
		            if (vm.addresses[j].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                count++;
		                vm.ContactCenterOffice = vm.addresses[j].ContactID;
		                vm.address = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		            } else {
		                var par = {
		                    AddressID: vm.addresses[j].AddressID,
		                    AddressInfo: vm.addresses[j].address,
		                    PostalCode: vm.addresses[j].postalCode,
		                    StateID: vm.addresses[j].stateID,
		                    CityID: vm.addresses[j].cityID,
		                    VendorContactType: vm.addresses[j].VendorContactType
		                }
		                vm.address2.push(par);
		            }
		        }


		        //} else {


		        //}
		    }
		    if (count == 0) {
		        count = 0;
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", "Kantor pusat harus diisi");
		        return;
		    }

		    var VendorContactTypeBranch = {
		        Name: "VENDOR_OFFICE_TYPE_BRANCH",
		        RefID: 1043,
		        Type: "VENDOR_OFFICE_TYPE",
		        Value: "VENDOR_OFFICE_TYPE_BRANCH",
		        VendorContactTypeID: 1043
		    };
		    var VendorContactTypeMain = {
		        Name: "VENDOR_OFFICE_TYPE_MAIN",
		        RefID: 1042,
		        Type: "VENDOR_OFFICE_TYPE",
		        Value: "VENDOR_OFFICE_TYPE_MAIN",
		        VendorContactTypeID: 1042
		    };
		    if (vm.address.AddressInfo != "") {
		        var contact = {
		            ContactID: vm.ContactCenterOffice,
		            Address: vm.address
		        }
		        var contactdt = {
		            VendorContactType: VendorContactTypeMain,
		            Contact: contact,
		            IsEdit: vm.IsEdit
		        }
		        vm.listcontact.push(contactdt);
		    }
		    /*
			if (vm.addressInfo != '' && vm.selectedCountryAlternatif == undefined) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_LIMITBUSINESSFIELD");
			    return;
			}*/
		    //if (vm.selectedCountryAlternatif != undefined && vm.addressInfo != ' ') {
		    //if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {


		    //} else {


		    //}
		    if (vm.AddressRemovedMain.length > 0) {
		        if (vm.AddressRemovedMain[0].ContactID != 0) {
		            var contactRemoved = {
		                ContactID: vm.AddressRemovedMain[0].ContactID,
		                Address: vm.AddressRemovedMain[0]
		            }

		            var contactdtRemoved = {
		                VendorContactType: VendorContactTypeMain,
		                Contact: contactRemoved,
		                IsEdit: vm.IsEdit,
		                IsRemoved: true
		            }

		            vm.listcontact.push(contactdtRemoved);
		        }
		    }
		    vm.AddressRemovedBranch.forEach(function (item) {
		        if (item.ContactID != 0) {
		            var contactRemovedBranch = {
		                ContactID: item.ContactID,
		                Address: item
		            }

		            var contactdtRemovedBranch = {
		                VendorContactType: VendorContactTypeBranch,
		                Contact: contactRemovedBranch,
		                IsEdit: vm.IsEdit,
		                IsRemoved: true
		            }

		            vm.listcontact.push(contactdtRemovedBranch);
		        }
		    });
		    vm.address2.forEach(function (item) {
		        if (vm.AddressAlterId == 0) {
		            var contact = {
		                Name: vm.administrasi.VendorName,
		                ModifiedBy: vm.administrasi.user.Username,
		                Address: item
		            }
		        } else {
		            var contact = {
		                Name: vm.administrasi.VendorName,
		                ContactID: item.ContactID,
		                ModifiedBy: vm.administrasi.user.Username,
		                Address: item
		            }
		        }
		        if (vm.VendorContactTypeAlter == undefined) {
		            var contactdt = {
		                Name: vm.administrasi.VendorName,
		                VendorContactType: VendorContactTypeBranch,
		                Contact: contact,
		                IsPrimary: 2,
		                IsEdit: vm.IsEditAlter
		            }
		        } else {
		            var contactdt = {
		                Name: vm.administrasi.VendorName,
		                VendorContactType: VendorContactTypeBranch,
		                Contact: contact,
		                IsPrimary: 2,
		                IsEdit: vm.IsEditAlter
		            }
		        }

		        vm.listcontact.push(contactdt);
		    });
		    //vm.address = {
		    //    AddressID: vm.AddressId,
		    //    AddressInfo: vm.address1,
		    //    PostalCode: vm.postcalcode,
		    //}
		    //if (vm.selectedCity == undefined && vm.selectedDistrict == undefined && vm.selectedState!=undefined) {
		    //    vm.address = {
		    //        AddressID: vm.AddressId,
		    //        AddressInfo: vm.address1,
		    //        PostalCode: vm.postcalcode,
		    //        StateID: vm.selectedState.StateID
		    //    }
		    //} else {

		    //    vm.address = {
		    //        AddressID: vm.AddressId,
		    //        AddressInfo: vm.address1,
		    //        PostalCode: vm.postcalcode,
		    //        StateID: vm.selectedState.StateID,
		    //        CityID: vm.selectedCity.CityID,
		    //        DistrictID: vm.selectedDistrict.DistrictID
		    //    }
		    //}
		    //var contact = {
		    //    ContactID: vm.ContactOfficeId,
		    //    Address: vm.address
		    //}
		    //var contactdt = {
		    //    VendorContactType: vm.VendorContactType,
		    //    Contact: contact,
		    //    IsEdit: vm.IsEdit
		    //}
		    //vm.listcontact.push(contactdt);

		    //if (vm.selectedCountryAlternatif !== undefined) {
		    //    if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {
		    //        vm.address2 = {
		    //            AddressID: vm.AddressAlterId,
		    //            AddressInfo: vm.AddressInfo,
		    //            PostalCode: vm.PostalCodeAlternatif,
		    //            StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null
		    //        }
		    //    }
		    //    else {
		    //        vm.address2 = {
		    //            AddressID: vm.AddressAlterId,
		    //            AddressInfo: vm.addressinfo,
		    //            PostalCode: vm.PostalCodeAlternatif,
		    //            StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null,
		    //            CityID: vm.selectedCityAlternatif.CityID,
		    //            DistrictID: vm.selectedDistrictAlternatif.DistrictID
		    //        }
		    //    }
		    //    if (vm.AddressAlterId == 0) {
		    //        var contact = {
		    //            Name: vm.Name,
		    //            ModifiedBy: vm.administrasi.user.Username,
		    //            Address: vm.address2
		    //        }
		    //    }
		    //    else {
		    //        var contact = {
		    //            ContactID: vm.ContactOfficeAlterId,
		    //            ModifiedBy: vm.administrasi.user.Username,
		    //            Address: vm.address2
		    //        }
		    //    }
		    //    if (vm.VendorContactTypeAlter == undefined) {
		    //        var contactdt = {
		    //            VendorContactType: vm.VendorContactType,
		    //            Contact: contact,
		    //            IsPrimary: 2,
		    //            IsEdit: vm.IsEditAlter
		    //        }
		    //    } else {
		    //        var contactdt = {
		    //            VendorContactType: vm.VendorContactTypeAlter,
		    //            Contact: contact,
		    //            IsPrimary: 2,
		    //            IsEdit: vm.IsEditAlter
		    //        }
		    //    }

		    //    vm.listcontact.push(contactdt);
		    //}
		    //    vm.listcontact.push(vm.contactpersonal);
		    //}

		    for (var i = 0; i < vm.listPersonal.length; i++) {
		        var contactdt = {
		            VendorContactType: vm.VendorContactTypePers,
		            Contact: {
		                ContactID: vm.listPersonal[i].Contact.ContactID,
		                Email: vm.listPersonal[i].Contact.Email,
		                Phone: vm.listPersonal[i].Contact.Phone,
		                Name: vm.listPersonal[i].Contact.Name
		            },
		            IsActive: true,
		            IsEdit: vm.listPersonal[i].IsEdit
		        }
		        vm.listcontact.push(contactdt);
		    }

		    var asoc;
		    if (vm.selectedAssociation === undefined) {
		        asoc = null;
		    } else {
		        asoc = vm.selectedAssociation.AssosiationID
		    }
		    if (vm.selectedSupplier === null) {
		        vm.selectedSupplier = {
		            RefID: null
		        };
		    }
		    if (vm.CountryCode !== 'IDN') {
		        vm.selectedBusinessEntity = {
		            BusinessID: null
		        };
		    }
		    vm.vendor = {
		        SupplierID: data2,
		        NpwpUrl: vm.NpwpUrl,
		        VendorName: vm.administrasi.VendorName,
		        VendorID: item.item,
		        user: {
		            Username: vm.Username
		        },
		        FoundedDate: UIControlService.getStrDate(vm.StartDate),
		        BusinessID: vm.selectedBusinessEntity.BusinessID,
		        PKPNumber: vm.PKPNumber,
		        PKPUrl: vm.PKPUrl,
		        ModifiedBy: vm.administrasi.user.Username,
		        AssociationID: asoc,
		        Contacts: vm.listcontact,
		        commodity: vm.listBussinesDetailField,
		        VendorTypeID: data1,
		        Currency: vm.listCurrencies,
		        Npwp: vm.NpwpNew

		    }
		    VerifikasiDataService.insert(vm.vendor, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_ADM_DATA");
		            $uibModalInstance.close();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
		            UIControlService.unloadLoadingModal();
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.saveAdministrasi = saveAdministrasi;
		function saveAdministrasi() {
		    if (vm.selectedTypeVendor !== undefined) {
		        vm.VendorTypeID = vm.selectedTypeVendor.RefID;
		        if (vm.selectedTypeVendor.Name === "VENDOR_TYPE_SERVICE") {
		            vm.SupplierID = null;
		            saveAllAdmin(vm.VendorTypeID, vm.SupplierID);
		        }
		        else {
		            if (vm.selectedSupplier != undefined) {
		                vm.SupplierID = vm.selectedSupplier.RefID;
		                saveAllAdmin(vm.VendorTypeID, vm.SupplierID);
		            }
		            else {
		                vm.SupplierID = null;
		                saveAllAdmin(vm.VendorTypeID, vm.SupplierID);
		            }
		        }
		    }
            else
		        saveAllAdmin(null, null);
		}
	    //simpanadministrasi
		vm.uploadFileAdministrasi = uploadFileAdministrasi;
		function uploadFileAdministrasi() {
		    if (vm.fileUpload === undefined) {
		        vm.PKPUrl = vm.administrasi.PKPUrl;
		        if (vm.fileUploadNPWP === undefined) {
		            vm.NpwpUrl = vm.administrasi.NpwpUrl;
		            saveAdministrasi();
		        }
		        else {
		            if (vm.fileUploadNPWP !== null) {
		                uploadAdministrasiNpwp(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		            }
		            else {
		                vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                saveAdministrasi();
		            }
		        }
		    }
		    else {
		        if (vm.fileUpload !== null) {
		            uploadPKP(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
		        }
		        else {
		            vm.PKPUrl = vm.administrasi.PKPUrl;
		            if (vm.fileUploadNPWP === undefined) {
		                vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                saveAdministrasi();
		            }
		            else {
		                if (vm.fileUploadNPWP !== null) {
		                    uploadAdministrasiNpwp(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		                }
		                else {
		                    vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                    saveAdministrasi();
		                }
		            }
		        }
		    }
		}

		vm.uploadPKP = uploadPKP;
		function uploadPKP(file, config, filters, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    //if (vm.administrasi.PKPUrl === null) {
		    UIControlService.loadLoadingModal("MESSAGE.LOADING_UPLOAD_FILE");
		    UploaderService.uploadSingleFileSPPKP(vm.administrasi.VendorID, file, size, filters, function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            var url = response.data.Url;
		            vm.pathFile = url;
		            vm.name = response.data.FileName;
		            var s = response.data.FileLength;
		            if (vm.flag == 0) {
		                vm.size = Math.floor(s)
		            }
		            if (vm.flag == 1) {
		                vm.size = Math.floor(s / (1024));
		            }
		            vm.PKPUrl = vm.pathFile;
		            if (vm.fileUploadNPWP === undefined) {
		                vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                saveAdministrasi();
		            }
		            else {
		                if (vm.fileUploadNPWP !== null) {
		                    uploadAdministrasiNpwp(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		                }
		                else {
		                    vm.NpwpUrl = vm.administrasi.NpwpUrl;
		                    saveAdministrasi();
		                }
		            }
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (response) {
		        if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
		            UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
		            UIControlService.unloadLoadingModal();
		        }
		    });
		    //} end if
		}

		vm.uploadAdministrasiNpwp = uploadAdministrasiNpwp;
		function uploadAdministrasiNpwp(file, config, filters, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    UIControlService.loadLoadingModal("MESSAGE.LOADING_UPLOAD_FILE");
		    UploaderService.uploadRegistration(file, vm.administrasi.Npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            var url = response.data.Url;
		            vm.pathFile1 = url;
		            vm.name1 = response.data.FileName;
		            var s = response.data.FileLength;
		            if (vm.flag == 0) {
		                vm.size = Math.floor(s)
		            }
		            if (vm.flag == 1) {
		                vm.size = Math.floor(s / (1024));
		            }
		            vm.NpwpUrl = vm.pathFile1;
		            saveAdministrasi();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (response) {
		        if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
		            UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
		            UIControlService.unloadLoadingModal();
		        }
		    });
		}

		



		//simpan
		vm.savedata = savedata;
		function savedata() {
		    if (vm.flag === 1) {
		        vm.datasimpan = {};

		        cekLicenseAda().then(function (response) {
		            if (!response) {
		                if (validateField()) {
		                    UIControlService.loadLoadingModal();
		                    if (!(vm.fileUpload === undefined || vm.fileUpload === '')) {
		                        uploadFile();
		                    } else {
		                        if (!vm.DocUrl) {
		                            UIControlService.unloadLoadingModal();
		                            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		                            return;
		                        }
		                        vm.datasimpan['DocumentURL'] = vm.DocUrl;
		                        //console.info("1-"+JSON.stringify(vm.datasimpan));
		                        saveprocess();
		                    }
		                }
		            } else {
		                UIControlService.msg_growl("error", "MESSAGE.LICENSE_EXIST");
		                UIControlService.unloadLoadingModal();
		            }
		        })

		    }
		        //vm.flag === 2 || vm.flag === 3 || vm.flag === 4 || vm.flag === 5 || vm.flag === 6
		    else if (vm.flag !== 1 && vm.flag !== 0) {
		        if (!(vm.fileUpload === undefined)) {
		            uploadFile();
		        } else {
		            saveprocess();
		        }
		    }
		}

		/*proses upload file*/
		function uploadFile() {
		    AuthService.getUserLogin(function (reply) {
		        vm.VendorLogin = reply.data.CurrentUsername;
		        if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.VendorLogin);
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		    });
			
		}

		function upload(file, config, filters, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}
			UIControlService.loadLoadingModal("MESSAGE.LOADING_UPLOAD_FILE");
			if (vm.flag === 0) {
			    UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, item.item,
                    function (response) {
                        UIControlService.unloadLoadingModal();
                        if (response.status == 200) {
                            var url = response.data.Url;
                            vm.PKPUrl = url;
                            UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                            saveprocess();

                        } else {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                            return;
                        }
                    },
                    function (response) {
                        UIControlService.msg_growl("error", "MESSAGE.API")
                        UIControlService.unloadLoadingModal();
                    });

			}
			else if (vm.flag === 1) {
			    UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, vm.jenisIzinUsaha.ID,
                    function (response) {
                        UIControlService.unloadLoadingModal();
                        if (response.status == 200) {
                            var url = response.data.Url;
                            vm.datasimpan['DocumentURL'] = url;
                            vm.pathFile = url;
                            UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                            saveprocess();

                        } else {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                            return;
                        }
                    },
                    function (response) {
                        UIControlService.msg_growl("error", "MESSAGE.API")
                        UIControlService.unloadLoadingModal();
                    });

			}
			else if (vm.flag === 2) {
			    UploaderService.uploadRegistration(file, vm.data.Vendor.Npwp, vm.prefixes['UPLOAD_PREFIX_ID'].Value, size, filters,

                    function (response) {
                        UIControlService.unloadLoadingModal();
                        if (response.status == 200) {
                            var url = response.data.Url;
                            vm.data.OwnerIDUrl = url;
                            UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                            saveprocess();

                        } else {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                            return;
                        }
                    },
                    function (response) {
                        UIControlService.msg_growl("error", "MESSAGE.API")
                        UIControlService.unloadLoadingModal();
                    });
			}

			else if (vm.flag === 3 || vm.flag === 4 || vm.flag === 5) {
			    UploaderService.uploadSingleFileLegalDocuments(vm.data.VendorID, file, size, filters,
           function (reply) {
               if (reply.status == 200) {
                   UIControlService.unloadLoadingModal();
                       vm.data.DocumentURL = reply.data.Url;
                       vm.data.FileSize = reply.data.FileLength;
                       vm.data.FileName = reply.data.FileName;
                   UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                   saveprocess();
               } else {
                   UIControlService.unloadLoadingModal();
                   UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
               }
           }, function (err) {
               UIControlService.unloadLoadingModal();
               UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
           });
			}

			else if (vm.flag === 7) {
			    UploaderService.uploadCompanyPersonID(vm.data.VendorID, file, size, types,
                           function (reply) {
                               if (reply.status == 200) {
                                   UIControlService.unloadLoadingModal();
                                   vm.data.IDUrl = reply.data.Url;
                               } else {
                                   UIControlService.unloadLoadingModal();
                                   UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
                               }
                           }, function (err) {
                               UIControlService.unloadLoadingModal();
                               UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                           });
			}

			else if (vm.flag === 6) {
			    if (vm.listSubCOA !== undefined) vm.prefix = vm.selectedSubCOA.RefID + '_' + vm.selectedUnit.RefID;
			    else vm.prefix = vm.selectedCOA.RefID + '_' + vm.selectedUnit.RefID;

			    UploaderService.uploadSingleFileBalance(vm.data.VendorID, vm.prefix, file, size, filters,
                function (response) {
                    UIControlService.unloadLoadingModal();
                    if (response.status == 200) {
                        UIControlService.unloadLoadingModal();
                        vm.data.DocUrl = response.data.Url;
                        UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                        saveprocess();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoadingModal();
                });
			}

            else if (vm.flag === 9 || vm.flag === 10 || vm.flag === 11) {
			    UploaderService.uploadSingleFileCertificate(vm.data.ID, file, size, filters,
                function (response) {
                    UIControlService.unloadLoadingModal();
                    if (response.status == 200) {
                        UIControlService.unloadLoadingModal();
                        vm.data.DocUrl = response.data.Url;
                        UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
                        saveprocess();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag == 16) {
                UploaderService.uploadSingleFileUploadDocument(file, size, filters,
               function (response) {
                   UIControlService.unloadLoadingModal();
                   if (response.status == 200) {
                       var url = response.data.Url;
                       vm.pathFile = url;
                       vm.name = response.data.FileName;
                       var s = response.data.FileLength;
                       vm.data.DocUrl = vm.pathFile;
                       saveprocess();
                   } else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                       return;
                   }
               },
               function (response) {
                   UIControlService.unloadLoadingModal();
               });
}
			




		}

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }

            var selectedFileType = file[0].type;
            selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
            if (selectedFileType === "vnd.ms-excel") {
                selectedFileType = "xls";
            }
            else if (selectedFileType === "vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                selectedFileType = "xlsx";
            }
            else {
                selectedFileType = selectedFileType;
            }
            //jika excel
            var allowed = false;
            for (var i = 0; i < allowedFileTypes.length; i++) {
                if (allowedFileTypes[i].Name == selectedFileType) {
                    allowed = true;
                    return allowed;
                }
            }

            if (!allowed) {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_TYPEFILE");
                return false;
            }
        }

		/* end proses upload*/
        vm.saveprocess = saveprocess;
        function saveprocess() {
            UIControlService.loadLoadingModal();

            if (vm.flag === 0) {
                addtolist();
                VerifikasiDataService.insert(vm.vendor, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.SUCC_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 1) {
                vm.datasimpan['VendorCategoryID'] = item.VendorCategoryID;
                //console.info("data simpan" + JSON.stringify(vm.datasimpan));
                vm.datasimpan['VendorID'] = $stateParams.id;
                vm.datasimpan['LicenseID'] = vm.jenisIzinUsaha.ID;
                vm.datasimpan['LicenseNo'] = vm.noJenisIzinUsaha;


                if (!vm.jenisIzinUsaha.IsEndDate) {
                    vm.datasimpan['ExpiredDate'] = null;
                } else {
                    vm.datasimpan['ExpiredDate'] = vm.ExpiredDate;

                }

                vm.datasimpan['IssuedBy'] = vm.instansiPemberi;
                vm.datasimpan['IssuedDate'] = vm.IssuedDate;
                vm.datasimpan['IssuedLocation'] = vm.city;
                vm.datasimpan['Remark'] = vm.keterangan;
                vm.datasimpan['BusinessFieldData'] = vm.dataBidangUsaha;

                if (vm.jenisIzinUsaha.Name == "SIUP") {
                    vm.datasimpan['CompanyScale'] = vm.kualifikasi;
                    vm.datasimpan['CapitalAmount'] = vm.CapitalAmount;

                } else {
                    vm.datasimpan['CompanyScale'] = null;
                    vm.datasimpan['CapitalAmount'] = null;

                }

                VerifikasiDataService.updateLicensi(vm.datasimpan, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 2) {
                vm.data.UnitID = vm.data.Unit.RefID;
                if (vm.stockUnitCurrency !== undefined)
                    vm.data.UnitCurrencyID = vm.stockUnitCurrency.CurrencyID;
                VerifikasiDataService.updateStock(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_STOCK");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 3 || vm.flag === 4 || vm.flag === 5) {
                VerifikasiDataService.updateLegal(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 6) {
                if (vm.listCOA.length != 0 && vm.selectedCOA == undefined) {
                    UIControlService.msg_growl("error", "MESSAGE.SELECTEDCOA_EMPTY");
                    return;
                }
                if (vm.listSubCOA.length != 0 && vm.selectedSubCOA == undefined) {
                    UIControlService.msg_growl("error", "MESSAGE.SELECTEDSUBCOA_EMPTY");
                    return;
                }
                //if (vm.selectedUnit == undefined) {
                //    UIControlService.msg_growl("error", "MESSAGE.SELECTEDUNIT_EMPTY");
                //    return;
                //}

                if (!vm.currency) {
                    UIControlService.msg_growl("warning", "MESSAGE.CURR");
                    return;
                }


                UIControlService.loadLoadingModal();
                if (vm.selectedCOA != null) {
                    vm.data.COAType = vm.selectedCOA.RefID;
                } else {
                    vm.data.COAType = null;

                }
                if (vm.selectedSubCOA != null) {
                    vm.data.SubCOAType = vm.selectedSubCOA.RefID;
                } else {
                    vm.data.SubCOAType = null;

                }
                //vm.data.UnitType = vm.selectedUnit.RefID;
                vm.data.WealthType = vm.selectedAsset.RefID;
                vm.data.CurrencyID = vm.currencyID;

                VerifikasiDataService.updateBalance(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 7) {
                //UIControlService.loadLoadingModal();

                VerifikasiDataService.updateCompany(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 8) {
                VerifikasiDataService.updateExperts(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 9 || vm.flag === 10 || vm.flag === 11) {
                VerifikasiDataService.updateExpertCertificate(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            else if (vm.flag === 16) {
                VerifikasiDataService.updateDoc(vm.data, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
            }
            
        }

		vm.batal = batal;
		function batal() {
		    //$uibModalInstance.dismiss('cancel');
		    $uibModalInstance.close();
		};

		vm.editcontact = editcontact;
		function editcontact(data) {
		    var data = {
		        item: data
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/data-rekanan/verifikasi-data/DetailContact.html',
		        controller: 'DetailContactCtrl',
		        controllerAs: 'DetailContactCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (data) {
		        vm.listPers = [];
		        vm.listPers = vm.listPersonal;
		        vm.listPersonal = [];
		        for (var i = 0; i < vm.listPers.length; i++) {
		            if (vm.listPers[i].ContactID === data.ContactID) {
		                var aish = {
		                    VendorContactType: vm.listPers[i].VendorContactType,
		                    ContactID: vm.listPers[i].ContactID,
		                    VendorID: vm.listPers[i].VendorID,
		                    Contact: {
		                        Name: data.Name,
		                        Email: data.Email,
		                        Phone: data.Phone
		                    }
		                }
		                vm.listPersonal.push(aish);
		            }
		            else {
		                vm.listPersonal.push(vm.listPers[i]);
		            }
		        }
		    });
		}

		vm.cekNpwp = cekNpwp;
		function cekNpwp(data) {
		    var datacek = {
		        Keyword: data,
		        column: item.item
		    }
		    VerifikasiDataService.cekNpwp(datacek, function (response) {
		        var data = response.data;
		        if (data.IsCheckedNpwp == true){
		            UIControlService.msg_growl('error', 'MESSAGE.VALIDATION_OK_NPWP_NOT_AVAILABLE', 'MESSAGE.VALIDATION_OK_NPWP_NOT_AVAILABLE_TITLE');
		            UIControlService.unloadLoadingModal();
		            vm.contact[0].Vendor.Npwp = null;

		        }
		    }, handleRequestError);
		}

		vm.addBidangUsaha = addBidangUsaha;
		function addBidangUsaha() {

		    if (vm.jenisIzinUsaha == '') {
		        UIControlService.msg_growl('error', "ERRORS.PILIH_JENIS_IJIN");
		        return;
		    }

		    var data = {
		        dataBidangUsaha: vm.dataBidangUsaha,
		        jenisIzinUsaha: vm.jenisIzinUsaha
		    };

		    var modalInstance = $uibModal.open({
		        templateUrl: "app/modules/panitia/data-rekanan/verifikasi-data/pilihGolonganBidangUsahaAdmin.modal.html",
		        controller: 'PilihGolonganBidangUsahaAdminModalController',
		        controllerAs: 'PilihGolonganBidangUsahaAdminModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (reply) {
		        console.log(reply)
		        if (reply != "") {
		            vm.dataBidangUsaha = reply;
                    
		        }
		    });
		}

		vm.removeBidangUsaha = removeBidangUsaha;
		function removeBidangUsaha(data) {
		    if (data.DataNew) {

		        var id = $.grep(vm.dataBidangUsaha, function (n) { return n.ID == data.ID; });
		        vm.dataBidangUsaha.splice(id, 1);

		    } else {
		        var id = vm.dataBidangUsaha.findIndex(x=> x.ID == data.ID);
		        console.log(id)
		        vm.dataBidangUsaha[id].IsActive = false;
		    }

		}

		function cekLicenseAda() {
		    var defer = $q.defer();
		    if (vm.jenisIzinUsaha.ID == dataModal.LicenseID) {
		        defer.resolve(false);
		        return defer.promise;
		    }
		    UIControlService.loadLoadingModal();
		    VerifikasiDataService.CekLicenseInVendorLicense(
                {
                    IntParam1: vm.jenisIzinUsaha.ID,
                    IntParam2: vm.VendorID
                },
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    var data = reply.data;

                    if (data == 0) {
                        defer.resolve(false);

                    } else {
                        defer.resolve(true);
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                    defer.reject();
                });
		    return defer.promise;
		}

		function validateField() {
		    //if (vm.LicenseNo == "") {
		    //    var lcnsname = vm.licensiname.substring(0, 3) == "NIB" ? vm.licensiname.substring(0, 9) : vm.licensiname.substring(0, 4);
		    //    localStorage.getItem("currLang").toLowerCase() == "id" ? UIControlService.msg_growl("error", "Nomor " + lcnsname + " belum diisi") :
		    //    UIControlService.msg_growl("error", lcnsname + " number was not filled");
		    //    return false;
		    //}
		    if (vm.tipeLicense == '') {
		        UIControlService.msg_growl("error", "MESSAGE.TIPE_LICENSE_EMPTY");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.jenisIzinUsaha == '') {
		        UIControlService.msg_growl("error", "MESSAGE.JENIS_IZIN_USAHA");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.noJenisIzinUsaha == '') {
		        UIControlService.msg_growl("error", "MESSAGE.JENIS_IZIN_USAHA");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.IssuedDate === "") {
		        UIControlService.msg_growl("error", "MESSAGE.ISSUED_DATE");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.jenisIzinUsaha.IsEndDate && vm.ExpiredDate === "") {
		        UIControlService.msg_growl("error", "MESSAGE.EXPIRED_DATE");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.instansiPemberi == "") {
		        UIControlService.msg_growl("error", "MESSAGE.INSTANSI_PEMBERI");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.provinsi == "") {
		        UIControlService.msg_growl("error", "MESSAGE.STATE");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.city == "") {
		        UIControlService.msg_growl("error", "MESSAGE.NO_CITY");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }


		    if (vm.jenisIzinUsaha.Name === 'SIUP' && vm.kualifikasi === "") {
		        UIControlService.msg_growl("error", "MESSAGE.KUALIFIKASI");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.jenisIzinUsaha.Name === 'SIUP' && vm.CapitalAmount === "") {
		        UIControlService.msg_growl("error", "MESSAGE.NOMINAL");
		        UIControlService.unloadLoadingModal();
		        return false;
		    }

		    if (vm.dataBidangUsaha.length == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.DATA_BIDANG_USAHA_EMPTY");
		        return false;
		    }

		    return true;
		}

		vm.cekDate = cekDate;
		function cekDate(flag) {
		    if (flag == 1) {
		        if (vm.IssuedDate === undefined) {
		            UIControlService.msg_growl("error", 'MESSAGE.STARTDATE_INVALID');
		            vm.IssuedDate = '';
		            return;
		        }
		    } else {
		        if (vm.ExpiredDate === undefined) {
		            UIControlService.msg_growl("error", 'MESSAGE.EXPIREDDATE_INVALID');
		            vm.ExpiredDate = '';
		            return;
		        }
		    }
		    VerifikasiDataService.cekDate(
                {
                    Status: flag,
                    Date1: UIControlService.getStrDate(vm.IssuedDate),
                    Date2: UIControlService.getStrDate(vm.ExpiredDate)
                },
               function (response) {
                   if (flag == 1 && response.data == 0) {
                       UIControlService.msg_growl("error", 'MESSAGE.STARTDATE_VALID');
                       vm.IssuedDate = null;
                       return;
                   }
                   else if (flag == 2 && response.data == 0) {
                       UIControlService.msg_growl("error", 'MESSAGE.DATE_VALID');
                       vm.ExpiredDate = null;
                       return;
                   }
                   else if (flag == 1 && response.data == 1) {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_ISSUEDDATE');

                       return;
                   }
                   else if (flag == 2 && response.data == 1) {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_DATE');
                       vm.ExpiredDate = null;
                       return;
                   }
               },
           function (response) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}

		vm.loadCurrency = loadCurrency;
		function loadCurrency() {
		    VerifikasiDataService.getCurrenciesList(function (reply) {
		        //UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.currencyList = reply.data;
		            for (var i = 0; i < vm.currencyList.length; i++) {
		                vm.currencyList[i].symbolLabel = vm.currencyList[i].Symbol + ' | ' + vm.currencyList[i].Label;
		                if (vm.currencyID == vm.currencyList[i].CurrencyID)
		                    vm.currency = vm.currencyList[i];
		            }
		        } else {
		            $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}


	}
})();