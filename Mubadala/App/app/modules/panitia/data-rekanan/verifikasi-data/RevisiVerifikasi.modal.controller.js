﻿(function () {
    'use strict';

    angular.module("app").controller("RevisiVerifikasiModalController", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'VerifiedSendService', 'SocketService', 'VerifikasiDataService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService','$stateParams'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, VerifiedSendService, SocketService, VerifikasiDataService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService,$stateParams) {
        var vm = this;
        vm.item = item;
        vm.data = vm.item.tabCompanyDataVendor;
        vm.langID = vm.item.langID;
        vm.vendorName = vm.item.vendorName;
        console.log(item)

        vm.init = init;
        function init() {

        }

        vm.simpan = simpan;
        function simpan() {

            var boleh = true;

            for (var i = 0; i < vm.data.length; i++) {
                if (vm.data[i].Remarks == null && vm.data[i].ReviewStatus != 4527 || vm.data[i].Remarks == "" && vm.data[i].ReviewStatus != 4527) {
                    boleh = false;
                }
            }

            if (!boleh) {
                UIControlService.msg_growl("warning", "MESSAGE.PLEASE_FILL_ALL_REMARKS");
                return;
            }


            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            VerifikasiDataService.vendorBack({
                VendorID: $stateParams.id,
                LangID: vm.langID,
                VendorName: vm.vendorName,
                tabCompanyDataVendor: vm.data,
                IsSave: 1
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();

                } else {
                    $.growl.error({ message: "Gagal" });
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss();
        }
       
    }
})();