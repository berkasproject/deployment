﻿(function () {
    'use strict';

    angular.module("app").controller("ModalLicenseCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'RoleService', 'UIControlService', 'item', '$uibModalInstance', '$state', 'GlobalConstantService', 'VendorRegistrationService', 'FileSaver', 'ProvinsiService','VerifikasiDataService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, 
        RoleService, UIControlService, item, $uibModalInstance, $state, GlobalConstantService, VendorRegistrationService, FileSaver, ProvinsiService, VerifikasiDataService) {
        var vm = this;
        var page_id = 141;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        var dataModal = item.item;
        vm.data = item.item;
        vm.isAdd = item.flag;
        vm.VendorCategoryID = item.VendorCategoryID;
        vm.countryID = 0;
        vm.lokasidetail = '';
        
        vm.init = init;
        function init() {
            if (vm.isAdd === 4) {
                vm.countryID = vm.data.StateLocationRef.CountryID;
                console.info(JSON.stringify(vm.countryID));
                if (vm.countryID === 360) {
                    vm.lokasidetail = vm.data.CityLocation.Name + "," + vm.data.StateLocationRef.Name + "," + vm.data.StateLocationRef.Country.Name + "," + vm.data.StateLocationRef.Country.Continent.Name;
                } else {
                    vm.lokasidetail = vm.data.StateLocationRef.Name + "," + vm.data.StateLocationRef.Country.Name + "," + vm.data.StateLocationRef.Country.Continent.Name;

                    console.info(JSON.stringify(vm.lokasidetail));
                }
            }
            if (vm.isAdd == 16) {
                vm.ValidDateConverted = convertDate(vm.data.ValidDate);
            }
            if (vm.isAdd == 1) {

                vm.dataSelectIzinUsaha = [];
                vm.noJenisIzinUsaha = dataModal.LicenseNo == undefined ? '' : dataModal.LicenseNo;
                vm.isDobCalendarOpened = false;
                vm.dataSelectProvinsi = [];
                vm.currency = 'Rp';
                vm.dataBidangUsaha = [];
                vm.tipeLicense = dataModal.LicenseType == undefined ? '' : dataModal.LicenseType;
                vm.provinsi = dataModal.IssuedState == undefined ? '' : dataModal.IssuedState;
                vm.instansiPemberi = dataModal.IssuedBy == undefined ? '' : dataModal.IssuedBy;
                vm.kualifikasi = dataModal.LicenseQualification == undefined ? '' : dataModal.LicenseQualification;
                vm.city = dataModal.IssuedLocation == undefined ? '' : dataModal.IssuedLocation;
                vm.CapitalAmount = dataModal.CapitalAmount == undefined ? '' : dataModal.CapitalAmount;
                vm.IssuedDate = dataModal.IssuedDate == undefined ? '' : new Date(UIControlService.convertDateForDatepicker(dataModal.IssuedDate));
                vm.DocUrl = dataModal.DocumentURL == undefined ? '' : dataModal.DocumentURL;
                //vm.IssuedDate = data.IssuedDate == undefined ? '' : UIControlService.convertDateForDatepicker(data.IssuedDate);
                vm.ExpiredDate = dataModal.ExpiredDate == undefined ? '' : new Date(UIControlService.convertDateForDatepicker(dataModal.ExpiredDate));
                vm.jenisIzinUsaha = '';
                vm.keterangan = dataModal.Remark == undefined ? '' : dataModal.Remark;
                vm.datasimpan = {};
                vm.VendorLicenseID = dataModal.VendorLicenseID == undefined ? null : dataModal.VendorLicenseID;
                vm.cityID = item.city;

                loadKlasifikasi();
                //getTypeSizeFile();

                changeCountry(vm.cityID);

                
                getVendorCommodity(1);
                

                if (vm.tipeLicense != '') {
                    changeJenisIzin();
                }
            }

        }
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.downloadFileCivd = downloadFileCivd;
        function downloadFileCivd(url, name) {
            UIControlService.loadLoadingModal();

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    var byteCharacters = atob(reply.result);

                    var byteNumbers = new Array(byteCharacters.length);
                    for (let i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    var blob = new Blob([byteArray], {
                        type: "application/pdf"
                    });

                    FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
                    UIControlService.unloadLoadingModal();

                } else {
                    UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

            });
        }

        /* combo Izin Usaha Load */
        vm.changeCountry = changeCountry;
        vm.listState = [];
        vm.selectedState;
        function changeCountry(idstate) {
            ProvinsiService.getStates(idstate,
               function (response) {
                   vm.listState = response.data;

                   if (dataModal.IssuedState != undefined) {
                       changeState();

                       for (var i = 0; i < vm.listState.length; i++) {
                           if (vm.listState[i].StateID == vm.provinsi) {
                               vm.provinsiName = vm.listState[i].Name;
                           }
                       }
                   }
               },
           function (response) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
        }

        vm.changeState = changeState;
        vm.listCities = [];
        vm.selectedCities;
        function changeState() {
            UIControlService.loadLoadingModal();
            ProvinsiService.getCities(vm.provinsi, function (response) {
                UIControlService.unloadLoadingModal();
                vm.listCities = response.data;
                if (vm.city != '') {
                    for (var i = 0; i < vm.listCities.length; i++) {
                        if (vm.listCities[i].CityID == vm.city) {
                            vm.cityName = vm.listCities[i].Name;
                        }
                    }
                }
            }, function (response) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.changeJenisIzin = changeJenisIzin;
        function changeJenisIzin() {
            UIControlService.loadLoadingModal();

            VerifikasiDataService.getJenisIzinUsaha({
                LicenseType: vm.tipeLicense
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.dataSelectIzinUsaha = data;
                    if (dataModal.LicenseID != undefined) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].ID == dataModal.LicenseID) {
                                vm.jenisIzinUsaha = data[i];
                            }
                        }

                    }
                    console.log(data)
                } else {
                    UIControlService.msg_growl('error', "ERRORS.ERROR_API");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "ERRORS.ERROR_API");
            })
        }

        function getVendorCommodity(current) {
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            //,
            //Offset: offset,
            //Limit: vm.pageSize
            VerifikasiDataService.getVendorCommodityBy(
                {
                    IntParam1: vm.VendorLicenseID,
                    IntParam2: dataModal.LicenseID
                },
                function (reply) {
                    UIControlService.unloadLoading();
                    var data = reply.data.List;
                    vm.dataBidangUsaha = data;
                    console.log(data)
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoading();
                });
        }

        /*get combo klasifikasi*/
        vm.listClasification = [];
        vm.selectedClasification;
        function loadKlasifikasi() {
            //alert("load");
            VerifikasiDataService.getClasification(function (reply) {
                UIControlService.unloadLoadingModal();
                vm.listClasification = reply.data.List;
                for (var i = 0; i < vm.listClasification.length; i++) {
                    if (vm.kualifikasi != '') {
                        if (vm.listClasification[i].RefID == vm.kualifikasi) {
                            vm.CompanyScaleName = vm.listClasification[i].Value;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

    }
})();