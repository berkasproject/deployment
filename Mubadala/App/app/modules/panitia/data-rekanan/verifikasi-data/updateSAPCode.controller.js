﻿(function () {
	'use strict';

	angular.module("app").controller("UpdateSAPCodeCtrl", ctrl);

	ctrl.$inject = ['item','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$stateParams',
        'VerifikasiDataService', 'UIControlService', '$uibModal', '$state', '$uibModalInstance'];

	function ctrl(item,$http, $translate, $translatePartialLoader, $location, SocketService, $stateParams,
        VerifikasiDataService, UIControlService, $uibModal, $state, $uibModalInstance) {
		//console.info("atur: "+JSON.stringify(item));
		var vm = this;
		vm.VendorID = item.VendorID;
		vm.SAPCode = item.SAPCode;

		vm.init = init;
		function init() {
			//prequalAssessmentData();
		}


		vm.saveSAPCode = saveSAPCode;
		function saveSAPCode(sapcode) {
		    VerifikasiDataService.updateSAPCode({
		        VendorID: vm.VendorID,
                SAPCode:sapcode
		    }, function (reply) {
		        if (reply.status == 200) {
		            UIControlService.msg_growl("success", "Berhasil Simpan Data");
		            $uibModalInstance.close();
		        }
		    }, function (error) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();