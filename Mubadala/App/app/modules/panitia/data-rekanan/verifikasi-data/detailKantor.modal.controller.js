﻿(function () {
    'use strict';

    angular.module("app").controller("DetailKantorModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'VerifiedSendService', 'VerifikasiDataService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'UploaderService', '$stateParams'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, VerifiedSendService, VerifikasiDataService, UIControlService, GlobalConstantService, UploadFileConfigService, UploaderService, $stateParams) {
        var vm = this;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.fullSize = 10;
        vm.keyword = "";
        vm.VendorID = item.VendorID;
        vm.dataKantor = [];

        vm.init = init;
        function init() {
            jLoad(1)
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            VerifikasiDataService.getDetailKantor({
                Keyword: vm.keyword,
                Offset: offset,
                Limit: vm.fullSize,
                IntParam1: vm.VendorID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataKantor = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();