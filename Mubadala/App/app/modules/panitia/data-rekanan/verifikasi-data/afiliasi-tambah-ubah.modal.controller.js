﻿(function () {
    'use strict';

    angular.module("app").controller("AfiliasiModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'VerifiedSendService', 'VerifikasiDataService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'UploaderService', '$stateParams'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, VerifiedSendService, VerifikasiDataService, UIControlService, GlobalConstantService, UploadFileConfigService, UploaderService,$stateParams) {
        var vm = this;
        vm.detail = item.item;
        vm.isAdd = item.act;
        vm.action = "";
        vm.tipeAfiliasi = "";
        vm.deskripsi = "";
        vm.jumlahSaham = 0;
        vm.klik = 0;
        vm.vendorAfiliasiID = 0;
        vm.selectedVendor;
        vm.vendorName = "";
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-afiliasi');
            loadVendor();
            if (vm.isAdd === 1) {
                vm.action = "Tambah";
            } else {
                vm.action = "Ubah";
                vm.vendorAfiliasiID = vm.detail.VendorAfiliasiID;
                vm.tipeAfiliasi = vm.detail.AfiliasiID.toString();
                vm.deskripsi = vm.detail.AfiliasiDetail;
                vm.jumlahSaham = vm.detail.StockQuantity;
                vm.vendorID = $stateParams.id;
                vm.vendorName = vm.detail.VendorName;
                vm.Dokumen_Url = vm.detail.Document_Url;
            }

            UploadFileConfigService.getByPageName("PAGE.ADMIN.AFFILIATION", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    //console.info(response);
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }

        vm.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        //vm.selectUpload = selectUpload;

        vm.uploadFile = uploadFile;
        function uploadFile() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            if (vm.klik > 0) {
                return;
            }

            if (vm.vendorName.VendorName) {
                vm.vendorSave = vm.vendorName.VendorName;
            } else {
                vm.vendorSave = vm.vendorName;
            }
            if (vm.vendorSave == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.VENDOR");
                return;
            }

            if (vm.tipeAfiliasi == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.TIPEAFILIASI");
                return;
            }

            //if (vm.deskripsi == "") {
            //    UIControlService.unloadLoadingModal();
            //    UIControlService.msg_growl("error", "MESSAGE.DESKRIPSI");
            //    return;
            //}

            if (vm.jumlahSaham == 0 || vm.jumlahSaham == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.JUMLAH");
                return;
            }
            vm.klik += 1;

            if (!(vm.fileUpload == null || vm.fileUpload == '')) {
                if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                        upload(1, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
                } else {
                    vm.klik = 0;
                    return false;
                    UIControlService.unloadLoadingModal();
                }
            } else {
                vm.klik = 0;
                if (vm.isAdd == 1) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
                    return;
                } else {
                    vm.simpan('');
                }
            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.NO_FILE");
                return false;
                UIControlService.unloadLoadingModal();
            }
            else return true;
        }

        vm.upload = upload;
        function upload(id, file, config, filters, callback) {
            console.info(id + "\n");
            console.info(file + "\n");
            console.info(config);
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoadingModal("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleAffiliation(id, file, size, filters, function (response) {
                //console.info("response:" + JSON.stringify(response));
                if (response.status == 200) {
                    //console.info(response);
                    var url = response.data.Url;
                    vm.pathFile = url;
                    vm.name = response.data.FileName;
                    var s = response.data.FileLength;
                    vm.DocUrl = vm.pathFile;
                    //console.info(vm.DocUrl);
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s);
                        //  console.info(vm.size);
                    }

                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }

                    vm.simpan(vm.DocUrl);
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    return;
                }
            }, function (response) {
                console.info(response);
                UIControlService.msg_growl("error", "MESSAGE.API")
                UIControlService.unloadLoadingModal();
            });


        }

        vm.ubahVendor = ubahVendor;
        function ubahVendor(data) {
            vm.vendorID = data.VendorID;
        }

        vm.ubahJumlahSaham = ubahJumlahSaham;
        function ubahJumlahSaham(jumlah) {
            if (jumlah < 0 || jumlah > 100) {
                vm.jumlahSaham = 0;
            }
        }

        vm.loadAfiliasi = loadAfiliasi;
        function loadAfiliasi() {
            VerifikasiDataService.getAfiliasi(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.afiliasi = reply.data;
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendor = loadVendor;
        function loadVendor() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            VerifikasiDataService.getVendor(function (reply) {
                if (reply.status === 200) {
                    vm.datavendor = reply.data;
                    vm.vendor = [];
                    for (var i = 0; i < vm.datavendor.length; i++) {
                        var param = {
                        VendorName: vm.datavendor[i].VendorName
                        }
                        vm.vendor.push(param);
                    }
                    loadAfiliasi();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
        vm.simpan = simpan;
        function simpan(url) {
            var DocUrl;
            if (url != '') {
                DocUrl = url;
            }

            
            VerifikasiDataService.saveAfiliasi({
                VendorAfiliasiID: vm.vendorAfiliasiID,
                VendorID: Number(vm.vendorID),
                VendorName: vm.vendorSave,
                AfiliasiID: Number(vm.tipeAfiliasi),
                StockQuantity: Number(vm.jumlahSaham),
                AfiliasiDetail: vm.deskripsi,
                Document_Url : DocUrl
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS");
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();