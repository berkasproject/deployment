﻿(function () {
    'use strict';

    angular.module("app").controller("VPVHSForm", ctrl);

    ctrl.$inject = ['$http', '$filter', '$window', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPVHSDataService', 'VPEvaluationMthodService',
        '$state', 'UIControlService', 'UploadFileConfigService', 'UploaderService', 'GlobalConstantService',
        '$uibModal', '$stateParams', 'WarningLetterService'];
    function ctrl($http, $filter, $window, $translate, $translatePartialLoader, $location, SocketService, VPVHSDataService, VPEvaluationMthodService,
        $state, UIControlService, UploadFileConfigService, UploaderService, GlobalConstantService,
        $uibModal, $stateParams, WarningLetterService) {
        var vm = this;

        vm.type = Number($stateParams.type);
        vm.act = Number($stateParams.act);
        vm.VPVHSDataId = Number($stateParams.id);
        vm.vhsAwardId = Number($stateParams.vhsawardid);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.isCalendarOpened = [false, false, false, false];
        vm.contract = [];

        vm.dateNow = new Date();
        vm.timezone = vm.dateNow.getTimezoneOffset();
        vm.timezoneClient = vm.timezone / 60;

        vm.metode = [];
        vm.SCORE = 0;
        vm.PMId = 0;
        vm.CEId = 0;
        vm.DeptId = 0;
        vm.kriteria = [];
        vm.IsVhsOrCpr = 1;
        vm.tipe = 1;

        vm.isNoCPR = false;

        var kriteria = [];
        var kriteriaLv1 = [];
        var kriteriaLv2 = [];
        var kriteriaLv3 = [];

        var kr = [];
        var krLv1 = [];
        var krLv2 = [];
        var krLv3 = [];

        var metode_evaluasi_id;
        vm.detail = [];
        vm.btnScoreFlag = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            if (vm.act == 0) {
                loadAwal();
            } else if (vm.act == 1) {
                VPVHSDataService.selectbyid({
                    VPVHSDataId: vm.VPVHSDataId
                }, function (reply) {
                    if (reply.status === 200) {
                        vm.vpdata = reply.data[0];
                        vm.viewVHS = true;
                        vm.CPRDate = new Date(vm.vpdata.VHSDate);
                        vm.vendor = vm.vpdata.VendorName;
                        vm.SCORE = vm.vpdata.Score;
                        vm.catatan = vm.vpdata.Note;
                        vm.noKontrakAndName = vm.vpdata.ContractNo + " | " + vm.vpdata.TanderName;
                        vm.buyerName = vm.vpdata.BuyerName;
                        vm.sponsorName = vm.vpdata.SponsorName;
                        vm.departmentName = vm.vpdata.DepartmentName;
                        vm.loadDocs();

                        if (vm.vpdata.IsVhsCpr == 1) {
                            vm.tipe = "1";
                        } else if (vm.vpdata.IsVhsCpr == 2) {
                            vm.tipe = "2";
                            //getKontrakVHS();
                        }

                        getEvalMethodByType();

                        for (var i = 0; i < vm.vpdata.VPVHSDataDetails.length; i++) {
                            kr.push(vm.vpdata.VPVHSDataDetails[i]);
                        }

                        for (var i = 0; i < kr.length; i++) {
                            if (kr[i].Level === 1) {
                                krLv1.push(kr[i]);
                            }
                            else if (kr[i].Level === 2) {
                                krLv2.push(kr[i]);
                            }
                            else if (kr[i].Level === 3) {
                                krLv3.push(kr[i]);
                            }
                        }

                        for (var i = 0; i < krLv2.length; i++) {
                            krLv2[i].sub = [];
                            for (var j = 0; j < krLv3.length; j++) {
                                if (krLv3[j].Parent === krLv2[i].CriteriaId) {
                                    krLv2[i].sub.push(krLv3[j]);
                                }
                            }
                        }

                        for (var i = 0; i < krLv1.length; i++) {
                            krLv1[i].sub = [];
                            for (var j = 0; j < krLv2.length; j++) {
                                if (krLv2[j].Parent === krLv1[i].CriteriaId) {
                                    krLv1[i].sub.push(krLv2[j]);
                                }
                            }
                        }
                        metode_evaluasi_id = vm.vpdata.VPEvaluationMethodId;
                        vm.kriteria = krLv1;
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
            }
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            vm.SCORE = 0;
            vm.vendor = "";
            vm.noKon = "";
            vm.contract = [];
            getEvalMethodByType();
            //getKontrakVHS();
            if(vm.vhsAwardId){ //Jika diakses dari halaman reminder
                UIControlService.loadLoadingModal("");
                VPVHSDataService.checkAndGetVHSAward({
                    ID: vm.vhsAwardId
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.data) {
                        setContractDatas(reply.data);
                    }
                }, function (error) {
                    if(error.Message.substring(0, 4) === 'ERR_'){
                        UIControlService.msg_growl('error', $filter('translate')('MESSAGE.' + error.Message));
                    } else {
                        UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_CHECK_CONTRACTS'));
                    }
                    UIControlService.unloadLoadingModal();
                });
            }
        };

        /*
        vm.NoContractChange = NoContractChange;
        function NoContractChange() {
            for (var i = 0; i < vm.totalItem; i++) {
                if (vm.noKontrak.SAPContractNo == vm.contract[i].SAPContractNo) {
                    vm.vendor = vm.contract[i].VendorName;
                    vm.vendorId = vm.contract[i].VendorID;
                    vm.buyerId = vm.contract[i].BuyerID;
                    vm.tenderName = vm.contract[i].TenderName;
                    break;
                }
            }
        };
        */

        vm.onMetodeEvalChange = onMetodeEvalChange;
        function onMetodeEvalChange() {
            if (vm.evalMethod !== null) {
                if (vm.evalMethod.IsVhsOrCpr == 1) {
                    vm.viewCPR = true;
                    vm.viewVHS = false;
                    metode_evaluasi_id = vm.evalMethod.VPEvaluationMethodId;
                    getEvalMetode();
                } else if (vm.evalMethod.IsVhsOrCpr == 2) {
                    vm.viewCPR = false;
                    vm.viewVHS = true;
                    metode_evaluasi_id = vm.evalMethod.VPEvaluationMethodId;
                    getEvalMetode();
                }
            } else if (vm.evalMethod === null) {
                vm.viewCPR = false;
                vm.viewVHS = false;
            }
            console.info(vm.evalMethod);
        };

        function getEvalMethodByType() {
            VPVHSDataService.getByType({
                IsVhsOrCpr: vm.type
            }, function (reply) {
                if (reply.status === 200) {
                    vm.metode = reply.data;
                    if (vm.act == 1) {
                        for (var i = 0; i < vm.metode.length; i++) {
                            if (vm.metode[i].VPEvaluationMethodId == vm.vpdata.VPEvaluationMethodId) {
                                vm.evalMethod = vm.metode[i];
                                break;
                            }
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        /*
        function getKontrakVHS() {
            VPVHSDataService.getContractVHS({
                Keyword: "",
                Offset: 0,
                Limit: 100
            }, function (reply) {
                if (reply.status === 200) {
                    vm.contract = reply.data;
                    vm.totalItem = vm.contract.length;
                    if (vm.act == 1) {
                        for (var i = 0; i < vm.contract.length; i++) {
                            if (vm.contract[i].SAPContractNo == vm.vpdata.ContractNo) {
                                vm.noKontrak = vm.contract[i];
                                break;
                            }
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };
        */

        function getEvalMetode() {
            kriteria = [];
            kriteriaLv1 = [];
            kriteriaLv2 = [];
            kriteriaLv3 = [];
            vm.kriteria = [];

            VPEvaluationMthodService.selectDCbyID({
                VPEvaluationMethodId: metode_evaluasi_id
            }, function (reply) {
                if (reply.status === 200) {
                    var hasil = reply.data;
                    for (var i = 0; i < hasil.length; i++) {
                        kriteria.push(hasil[i]);
                    }

                    for (var i = 0; i < kriteria.length; i++) {
                        if (kriteria[i].Level === 1) {
                            kriteriaLv1.push(kriteria[i]);
                        }
                        else if (kriteria[i].Level === 2) {
                            kriteriaLv2.push(kriteria[i]);
                        }
                        else if (kriteria[i].Level === 3) {
                            kriteriaLv3.push(kriteria[i]);
                        }
                    }

                    for (var i = 0; i < kriteriaLv2.length; i++) {
                        kriteriaLv2[i].sub = [];
                        for (var j = 0; j < kriteriaLv3.length; j++) {
                            if (kriteriaLv3[j].Parent === kriteriaLv2[i].CriteriaId) {
                                kriteriaLv2[i].sub.push(kriteriaLv3[j]);
                            }
                        }
                    }

                    for (var i = 0; i < kriteriaLv1.length; i++) {
                        kriteriaLv1[i].sub = [];
                        for (var j = 0; j < kriteriaLv2.length; j++) {
                            if (kriteriaLv2[j].Parent === kriteriaLv1[i].CriteriaId) {
                                kriteriaLv1[i].sub.push(kriteriaLv2[j]);
                            }
                        }
                    }

                    vm.kriteria = kriteriaLv1;
                    for (var i = 0; i < vm.kriteria.length; i++) {
                        vm.kriteria[i].Score = 0;
                        if (vm.kriteria[i].sub.length !== 0) {
                            for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                                vm.kriteria[i].sub[j].Score = 0;
                                if (vm.kriteria[i].sub[j].sub !== 0) {
                                    for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                                        vm.kriteria[i].sub[j].sub[k].Score = 0;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
                }
            }, function (err) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.selectContract = selectContract;
        function selectContract() {

            if (vm.act === 1) { //Tidak bisa ubah kontrak saat edit
                return;
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-selectcontract.modal.html',
                controller: 'selectVHSContractModal',
                controllerAs: 'selContractCtrl',
            });
            modalInstance.result.then(function (selectedContract) {
                setContractDatas(selectedContract);
            });
        };

        function setContractDatas(contract){

            vm.noKontrak = contract.SAPContractNo;
            vm.vhsAwardId = contract.ID;
            vm.vendor = contract.VendorName;
            vm.vendorId = contract.VendorID;
            vm.buyerId = contract.BuyerID;
            vm.buyerName = contract.Buyer;
            vm.tenderName = contract.TenderName;
            vm.noKontrakAndName = (vm.noKontrak ? vm.noKontrak : "") + " | " + vm.tenderName;

            VPVHSDataService.getSponsorAndDept({
                EmployeeID: vm.buyerId,
            }, function (reply) {
                vm.sponsor = reply.data.Sponsor;
                vm.sponsorName = reply.data.SponsorName;
                vm.departmentHolder = reply.data.DepartmentHolder;
                vm.departmentName = reply.data.DepartmentName;
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_SPONSOR'));
            });
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        vm.scoring = scoring;
        function scoring() {
            vm.SCORE = 0;
            var SLv1 = 0;
            var SLv2 = 0;
            var SLv3 = 0;

            for (var i = 0; i < vm.kriteria.length; i++) {
                SLv1 += (vm.kriteria[i].Weight / 100) * vm.kriteria[i].Score;
                for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                    SLv2 += ((vm.kriteria[i].sub[j].Weight / 100) * vm.kriteria[i].sub[j].Score) * (vm.kriteria[i].Weight / 100);
                }
            }
            var score = SLv1 + SLv2;
            vm.SCORE = score.toFixed(2);
            if (!vm.SCORE && vm.SCORE !== 0) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INVALID_SCORE'));
                return;
            };
            vm.btnScoreFlag = true;
        };

        vm.optionChange = optionChange;
        function optionChange(kriteria) {

            var maxScore = 0;

            for (var i = 0; i < kriteria.Standards.length; i++) {
                if (kriteria.Standards[i].Id === kriteria.SelectedStandard) {
                    maxScore = kriteria.Standards[i].MaxScore;
                }
            }

            //Langsung masukkan nilai skor Max apabila opsi kriteria berupa nilai fix, bukan range
            if (kriteria.IsOptionScoreFixed && kriteria.SelectedStandard > 0) {
                kriteria.Score = maxScore;
            };

            //Memastikan skor berada dalam range yang benar
            scoreChange(kriteria);
        };

        vm.scoreChange = scoreChange;
        function scoreChange(kriteria) {
            //Memastikan skor berada dalam range yang benar
            var minScore = 0;
            var maxScore = 100;

            for (var i = 0; i < kriteria.Standards.length; i++) {
                if (kriteria.Standards[i].Id === kriteria.SelectedStandard) {
                    minScore = kriteria.Standards[i].MinScore;
                    maxScore = kriteria.Standards[i].MaxScore;
                }
            }

            if (kriteria.Score > maxScore) {
                kriteria.Score = maxScore;
            }
            if (kriteria.Score < minScore) {
                kriteria.Score = minScore;
            }
        };

        vm.save = save;
        function save() {
            if (vm.btnScoreFlag == false && !vm.isNoCPR) {
                vm.SCORE = 0;
                var SLv1 = 0;
                var SLv2 = 0;
                var SLv3 = 0;

                for (var i = 0; i < vm.kriteria.length; i++) {
                    SLv1 += (vm.kriteria[i].Weight / 100) * vm.kriteria[i].Score;
                    for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                        SLv2 += ((vm.kriteria[i].sub[j].Weight / 100) * vm.kriteria[i].sub[j].Score) * (vm.kriteria[i].Weight / 100);
                    }
                }
                var score = SLv1 + SLv2;
                vm.SCORE = score.toFixed(2);
                if (!vm.SCORE && vm.SCORE !== 0) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INVALID_SCORE'));
                    return;
                };
            }

            kriteriaLv1 = [];
            for (var i = 0; i < vm.kriteria.length; i++) {
                kriteriaLv1.push(vm.kriteria[i]);
                for (var j = 0; j < vm.kriteria[i].sub.length; j++) {
                    kriteriaLv1.push(vm.kriteria[i].sub[j]);
                    for (var k = 0; k < vm.kriteria[i].sub[j].sub.length; k++) {
                        kriteriaLv1.push(vm.kriteria[i].sub[j].sub[k]);
                    }
                }
            }
            
            vm.CPRDate = new Date(vm.CPRDate.setHours(vm.CPRDate.getHours() - vm.timezoneClient));
            if (vm.act === 0) {
                if (!vm.noKontrak) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.NO_NOCONTRACT'));
                    return;
                }

                if (!vm.evalMethod && !vm.isNoCPR) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.NO_EVALMETHOD'));
                    return;
                }

                if (!vm.CPRDate) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.NO_DATE'));
                    return;
                }

                //Added: score checking for directly send to warning letter.
                //if (vm.SCORE < 60) {
                //    saveToWL();
                //}

                if (!vm.isNoCPR) {
                    VPVHSDataService.insert({
                        ContractNo: vm.noKontrak,
                        VHSAwardId: vm.vhsAwardId,
                        VendorId: vm.vendorId,
                        VHSDate: vm.CPRDate,
                        BuyerId: vm.buyerId,
                        Sponsor: vm.sponsor,
                        DepartmentHolder: vm.departmentHolder,
                        VPEvaluationMethodId: metode_evaluasi_id,
                        Score: vm.SCORE,
                        TanderName: vm.tenderName,
                        VPVHSDataDetails: kriteriaLv1,
                        Note: vm.catatan
                    }, function (reply) {
                        if (reply.status === 200) {
                            $state.transitionTo('vendor-performance-vhs');
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SAVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                    });
                } else {
                    VPVHSDataService.insertDefault({
                        ContractNo: vm.noKontrak,
                        VHSAwardId: vm.vhsAwardId,
                        VendorId: vm.vendorId,
                        VHSDate: vm.CPRDate,
                        BuyerId: vm.buyerId,
                        Sponsor: vm.sponsor,
                        DepartmentHolder: vm.departmentHolder,
                        //VPEvaluationMethodId: metode_evaluasi_id,
                        //Score: vm.SCORE,
                        TanderName: vm.tenderName,
                        //VPVHSDataDetails: kriteriaLv1,
                        Note: vm.catatan
                    }, function (reply) {
                        if (reply.status === 200) {
                            $state.transitionTo('vendor-performance-vhs');
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SAVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                    });
                }
            } else if (vm.act === 1) {
                VPVHSDataService.update({
                    VPVHSDataId: vm.VPVHSDataId,
                    VPEvaluationMethodId: metode_evaluasi_id,
                    Score: vm.SCORE,
                    VPVHSDataDetails: kriteriaLv1,
                    Note: vm.catatan
                }, function (reply) {
                    if (reply.status === 200) {
                        $state.transitionTo('vendor-performance-vhs');
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SAVE_CPR_SUCCES'));
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_CPR'));
                });
            }
        };

        ////////Dokumen CPR
        vm.loadDocs = loadDocs;
        function loadDocs() {
            UIControlService.loadLoading("");
            VPVHSDataService.getDocs({
                VPVHSDataId: vm.VPVHSDataId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoading();
            });
        };

        vm.addDoc = addDoc;
        function addDoc() {

            if (vm.act === 0) { //Tidak bisa tambah dokumen sebelum data CPR disimpan
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.SAVE_CPR_FIRST'));
                return;
            }

            var lempar = {
                doc: {
                    VPVHSDataId: vm.VPVHSDataId,
                    DocName: "",
                    DocUrl: "",
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-doc.modal.html',
                controller: 'vpvhsDocsModalCtrl',
                controllerAs: 'vpvhsDocsModalCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadDocs();
            });
        };

        vm.editDoc = editDoc;
        function editDoc(doc) {
            var lempar = {
                doc: {
                    ID: doc.ID,
                    VPVHSDataId: doc.vpvhsId,
                    DocName: doc.DocName,
                    DocUrl: doc.DocUrl,
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-doc.modal.html',
                controller: 'vpvhsDocsModalCtrl',
                controllerAs: 'vpvhsDocsModalCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadDocs();
            });
        };

        vm.delDoc = delDoc;
        function delDoc(doc) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_DEL_DOC'), function (yes) {
                if (yes) {
                    VPVHSDataService.deleteDoc(doc, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_DEL_DOC'));
                            vm.loadDocs();
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL_DOC'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL_DOC'));
                    });
                }
            });
        }
        ////////End Dokumen CPR


        vm.back = back
        function back() {
            if (vm.vhsAwardId) {
                $state.transitionTo('vendor-performance-vhs-reminder');
            } else {
                $state.transitionTo('vendor-performance-vhs');
            }
        };

        /*
        vm.saveToWL = saveToWL;
        function saveToWL() {

            WarningLetterService.insertWarningLetterByScoreVHS({
                VendorID: vm.vendorId,
                ReporterID: vm.departmentHolder,
                IDContract: vm.vhsAwardId

            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    console.log("succ. cpr-vhs data saved to WL.");
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoadingModal();
            });
        }
        */
    }
})();