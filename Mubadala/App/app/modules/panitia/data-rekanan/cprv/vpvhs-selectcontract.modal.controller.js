﻿(function () {
    'use strict';

    angular.module("app")
    .controller("selectVHSContractModal", ctrl);
    
    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPVHSDataService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, VPVHSDataService, UIControlService) {

        var vm = this;
        
        vm.count;
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.searchText = "";
        vm.contracts = [];

        vm.onBatalClick = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.onSearchSubmit = function (searchText) {
            vm.searchText = searchText;
            vm.loadData();
        };

        vm.init = init;
        
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            vm.loadData();
        };

        vm.loadData = loadData; 
        function loadData() {
            UIControlService.loadLoadingModal("");
            VPVHSDataService.getContractVHS({
                Keyword: vm.searchText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                vm.contracts = reply.data.List;
                vm.count = reply.data.Count;
                UIControlService.unloadLoadingModal();
            }, function (error) {
                UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_LOAD_CONTRACTS'));
                UIControlService.unloadLoadingModal();
            });
        };

        vm.onSelectClick = onSelectClick;
        function onSelectClick(contract) {
            UIControlService.loadLoadingModal("");
            VPVHSDataService.nodraft({
                ID: contract.ID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.data === true) {
                    $uibModalInstance.close(contract);
                } else {
                    UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_THERE_IS_DRAFT'));
                }
            }, function (error) {
                UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_CHECK_CONTRACTS'));
                UIControlService.unloadLoadingModal();
            });
        };
    }
})();