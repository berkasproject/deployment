﻿/*(function () {
    'use strict';

    angular.module("app")
    .controller("vpvhsDocsCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'VPVHSDataService', 'UIControlService', 'GlobalConstantService'];
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, VPVHSDataService, UIControlService, GlobalConstantService) {

        var vm = this;
        var vpvhsId = Number($stateParams.vpvhsId);
        var loadmsg = "MESSAGE.LOADING";

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.docs = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("vp-vhs");
            loadInfo();
        };

        function loadInfo() {
            VPVHSDataService.selectbyid({
                VPVHSDataId: vpvhsId
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vpdata = reply.data[0];
                    loadData();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
            });
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.loadData = loadData;
        function loadData() {
            VPVHSDataService.getDocs({
                VPVHSDataId: vpvhsId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoading();
            });
        };

        vm.add = add;
        function add() {
            var lempar = {
                doc: {
                    VPVHSDataId: vpvhsId,
                    VendorId: vm.vpdata.VendorId,
                    DocName: "",
                    DocUrl: "",
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-doc.modal.html',
                controller: 'vpvhsDocsModalCtrl',
                controllerAs: 'vpvhsDocsModalCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();
            });
        };

        vm.edit = edit;
        function edit(doc) {
            var lempar = {
                doc: {
                    ID: doc.ID,
                    VPVHSDataId: doc.vpvhsId,
                    VendorId: vm.vpdata.VendorId,
                    DocName: doc.DocName,
                    DocUrl: doc.DocUrl,
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cprv/vpvhs-doc.modal.html',
                controller: 'vpvhsDocsModalCtrl',
                controllerAs: 'vpvhsDocsModalCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadData();
            });
        };

        vm.del = del;
        function del(doc) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_DEL_DOC'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading(loadmsg);
                    VPVHSDataService.deleteDoc(doc, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_DEL_DOC'));
                            vm.loadData();
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL_DOC'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL_DOC'));
                    });
                }
            });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('vendor-performance-vhs');
        };
    }
})();*/