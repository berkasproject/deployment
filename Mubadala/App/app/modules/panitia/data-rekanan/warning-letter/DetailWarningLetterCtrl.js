﻿(function () {
    'use strict';

    angular.module("app").controller("DetailWarningLetterCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'RoleService', 'UIControlService', 'item', '$uibModal', '$uibModalInstance', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, WarningLetterService,
        RoleService, UIControlService, item, $uibModal, $uibModalInstance, GlobalConstantService) {
        var vm = this;
        var page_id = 141;

        vm.isAdd = item.act;
        vm.Area;
        vm.LetterID;
        vm.Description="";
        vm.action = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        
        vm.init = init;
        vm.warningLetter = {};
        vm.isCalendarOpened = [false, false, false, false];
        vm.endpointAttachment = GlobalConstantService.getConstant('api') + "/";

        function init() {
            $translatePartialLoader.addPart('master-warningLetter');
            if (item.item.Type == 1)
                vm.GoodOrService = 'Pengaduan';
            else if (item.item.Type == 2)
                vm.GoodOrService = 'CPR';
            else if (item.item.Type == 3)
                vm.GoodOrService = 'VHS';
            else if (item.item.Type == 4)
                vm.GoodOrService = 'PO';

            vm.CreatedDate = item.item.CreatedDate;
            vm.VendorName = item.item.Vendor.VendorName;
            vm.WarningType = item.item.ComplainType.TypeName;
            vm.StartDate = item.item.StartDate;
            vm.EndDate = item.item.EndDate;
            vm.Area = item.item.Area;
            vm.Description = item.item.Description;
            vm.DetailKontrak = item.item.DetailKontrak;
            vm.ReporterName = item.item.ReporterName;
            vm.LetterType = item.item.LetterType;
            
            if (item.item.Remark == null || item.item.Remark == "") {
                vm.Remark = "-";
            }
            else {
                vm.Remark = item.item.Remark;
            }

            if (item.item.department==null) {
                vm.Reporter = item.item.DepartmentName;
                vm.DeptCode = item.item.DepartmentCode;
                vm.docUrl = "-";
            }
            else {
                vm.Reporter = item.item.department.department.DepartmentName;
                vm.DeptCode = item.item.department.department.DepartmentCode;
                vm.docUrl = item.item.department.DocUrl;
            }

            vm.Status = item.item.SysReference.Name;                     
            vm.TemplateDoc = item.item.TemplateDoc;
            vm.businessType = item.item.Vendor.businessName;
           
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.printWL = printWL;
        function printWL(wl) {
            $uibModalInstance.close();
            var innerContents = wl;
            var innerLogo ='<div class="row" style="width:100%;"><div class="col-md-8"></div><div class="col-md-4"><img src="assets/img/logo_vale2.png" class="img-responsive pull-right" style="padding-top:10px;padding-left:690px;padding-bottom: 20px;display:block;text-align:right;" width="100px" height="40px" /></div></div>' ;
            /*
            var innerFooter = '<div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div>' +
                              '<div class="row"><span style="font-size: 9px;">Jakarta Office: Gedung The Energy Lt.31 LOT 11A Jl. Jendral Sudirman Kav 52-53 Senayan Kebayoran Baru Jakarta Selatan DKI Jakarta  Indonesia. T. (62) 21 524 9000 F. (62) 21 524 9020</span></div>' +
                              '<div class="row"><span style="font-size: 9px;">Makassar: Jl. Somba Opu No. 281, Makassar 90001, Indonesia. T. (62) 411 873 731 F. (62) 411 856 157</span></div>'+
                              '<div class="row"><span style="font-size: 9px;">Sorowako: Plant Site Sorowako, Luwu Timur 92984, Sulawesi Selatan, Indonesia. T. (62) 21 524 9100 F (62) 21 524 9565</span></div>'+
                              '<div class="row"><span style="font-size: 9px;">www.vale.com/indonesia</span></div>';
            */
            WarningLetterService.getFooter(function (reply) {

                var innerFooter = '<div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div>'
                    + '<span style="font-size: 9px;">' + reply.data + '</span>';

                var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                popupWindow.document.open();
                popupWindow.document.write('<html><head><title>Warning Letter - ' + vm.businessType + ' ' + vm.VendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print_wl.css" /></head><body onload="window.print()">' + innerLogo + innerContents + innerFooter + '</body></html>');
                popupWindow.document.close();
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_PRINT");
            });
        }

        /*
        vm.tambah = tambah;
        function tambah(warningtype) {
            var data = {
                act: warningtype,
                data1: item.item,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/UbahTemplate.html',
                controller: 'UbahWarningLetterCtrl',
                controllerAs: 'UbahwarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                $uibModalInstance.close();
                vm.jLoad(1);
            });
           
        }
        */
    }
})();