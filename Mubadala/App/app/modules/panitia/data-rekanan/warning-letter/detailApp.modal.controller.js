﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailAppWLCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'WarningLetterService', 'UploadFileConfigService', 'UploaderService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, WarningLetterService, UploadFileConfigService, UploaderService) {

        var vm = this;

        vm.WLID = item.LetterID;
        vm.flag = item.flag;
        vm.IsFromComplaint = item.IsFromComplaint;
        vm.remark = "";

        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('master-warningLetter');
            UploadFileConfigService.getByPageName("PAGE.ADMIN.COMPLAIN", function (response) {
                UIControlService.unloadLoadingModal();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                    //console.log("file types: " + vm.idFileTypes);
                    //console.log("upload config: " + JSON.stringify(vm.idUploadConfigs));
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
                return;
            });
        };

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload(file, config, types) {

            var size = config.Size;
            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                //vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                //vm.flag = 1;
            }
          
            UploaderService.uploadSingleFileComplaint(0, "", file, size, types,
                function (reply) {
                    if (reply.status == 200) {
                        UIControlService.unloadLoadingModal();
                        var url = reply.data.Url;
                        var size = reply.data.FileLength;

                        vm.DocUrl = url;

                        if (vm.flag === 2) {
                            stateCancel();
                        } else if (vm.flag === 3) {
                            sendToVendor();
                        } else if (vm.flag === 4) {
                            stateCancelPost();
                        } else if (vm.flag === 5) {
                            stateCompleteSkipVendor();
                        }

                    } else {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DOC'));
                });
            
        }

        vm.sendToVendor = sendToVendor;
        function sendToVendor() {
            WarningLetterService.stateSendVendor({
                LetterID: vm.WLID,
                DocUrl: vm.DocUrl 
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {                                
                    //sendMail(vm.WLID);
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_SEND_VENDOR'));
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_SEND_VENDOR'));
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_SEND_VENDOR'));
                UIControlService.unloadLoadingModal();
            });
        }
        
        function stateCancel() {
            WarningLetterService.stateCancel({
                LetterID: vm.WLID,
                Remark: vm.remark,
                DocUrl: vm.DocUrl
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    $uibModalInstance.close();
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_CANCEL'));
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_CANCEL'));
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_CANCEL'));
                UIControlService.unloadLoadingModal();
            });
        }

        function stateCancelPost() {
            WarningLetterService.stateCancelPost({
                LetterID: vm.WLID,
                Remark: vm.remark,
                DocUrl: vm.DocUrl
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    $uibModalInstance.close();
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_CANCEL_POST'));
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_CANCEL_POST'));
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_CANCEL_POST'));
                UIControlService.unloadLoadingModal();
            });
        }

        function stateCompleteSkipVendor() {
            WarningLetterService.stateCompleteSkipVendor({
                LetterID: vm.WLID,
                Remark: vm.remark,
                DocUrl: vm.DocUrl
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    $uibModalInstance.close();
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SKIP_VENDOR'));
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SKIP_VENDOR'));
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SKIP_VENDOR'));
                UIControlService.unloadLoadingModal();
            });
        }

        vm.goToProcess = goToProcess;
        function goToProcess() {
            if (vm.flag === 1) {
                if (!vm.isOnAnotherWL) {
                    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_BACK_INCOMPLETE'), function (yes) {
                        if (yes) {
                            UIControlService.loadLoadingModal("");
                            WarningLetterService.stateIncomplete({
                                LetterID: vm.WLID,
                                Remark: vm.remark
                            }, function (reply) {
                                UIControlService.unloadLoadingModal();
                                if (reply.status === 200) {
                                    $uibModalInstance.close();
                                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_INCOMPLETE'));
                                } else {
                                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_INCOMPLETE'));
                                    UIControlService.unloadLoadingModal();
                                }
                            }, function (err) {
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_INCOMPLETE'));
                                UIControlService.unloadLoadingModal();
                            });
                        }
                    });
                } else {
                    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CLOSED'), function (yes) {
                        if (yes) {
                            UIControlService.loadLoadingModal("");
                            WarningLetterService.stateClosed({
                                LetterID: vm.WLID,
                                Remark: vm.remark
                            }, function (reply) {
                                UIControlService.unloadLoadingModal();
                                if (reply.status === 200) {
                                    $uibModalInstance.close();
                                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_CLOSED'));
                                } else {
                                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_CLOSED'));
                                    UIControlService.unloadLoadingModal();
                                }
                            }, function (err) {
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                                UIControlService.unloadLoadingModal();
                            });
                        }
                    });
                }
            }
            else if (vm.flag === 2) { // pre cancel - cancel complaint
                bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CANCELED'), function (yes) {
                    if (yes) {
                        UIControlService.loadLoadingModal("");
                        if (vm.fileUpload) {
                            uploadFile();
                        }
                        else{
                            stateCancel();
                        }
                    }
                });
            }
            else if (vm.flag === 3) {
                bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_TO_VENDOR'), function (yes) {
                    if (yes) {
                        UIControlService.loadLoadingModal("");
                        if (vm.fileUpload) {
                            uploadFile();
                        }
                    }
                });
            }
            else if (vm.flag === 4) {
                bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CANCELED_POST'), function (yes) {
                    if (yes) {
                        UIControlService.loadLoadingModal("");
                        if (vm.fileUpload) {
                            uploadFile();
                        }
                        else {
                            stateCancelPost();
                        }
                    }
                });
            }
            else if (vm.flag === 5) {
                bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SKIP_VENDOR'), function (yes) {
                    if (yes) {
                        UIControlService.loadLoadingModal("");
                        if (vm.fileUpload) {
                            uploadFile();
                        }
                        else {
                            stateCompleteSkipVendor();
                        }
                    }
                });
            }
        }      

        vm.sendMail = sendMail;
        function sendMail(letterID) {
            WarningLetterService.sendMailToVendor({
                LetterID: letterID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SEND_MAIL'));
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_MAIL'));
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_MAIL'));
                UIControlService.unloadLoadingModal();
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.close();
        };
    }
})();