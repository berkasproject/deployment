﻿(function() {
    'use strict';

    angular.module("app").controller("UploadBadTrackRecordController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UploadBadTrackRecordService',
         'GlobalConstantService', 'UIControlService', '$uibModal', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UploadBadTrackRecordService,
         GlobalConstantService, UIControlService, $uibModal, $filter) {

        var vm = this;

        vm.init = init;
        function init() {

        };    

        vm.selectUpload = selectUpload;
        function selectUpload(fileUpload) {
            vm.fileUpload = fileUpload;
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload)) {
                upload(vm.fileUpload);
            }
        }

        function validateFileType(file) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        function upload(file) {
            UIControlService.loadLoading("");
            UploadBadTrackRecordService.UploadExcel(file,
                function (reply) {
                    UIControlService.msg_growl("notice", "MESSAGE.SUCC_UPLOAD");
                    UIControlService.unloadLoading();
                },function (error) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD")
                    UIControlService.unloadLoading();
                }
            );
        }
    }
})();
