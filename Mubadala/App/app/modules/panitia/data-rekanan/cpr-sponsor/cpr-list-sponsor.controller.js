(function () {
    'use strict';

    angular.module("app").controller("CPRSponsorCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCPRSponsorService',
        '$state', 'UIControlService', 'GlobalConstantService', '$uibModal', '$stateParams'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, VPCPRSponsorService,
        $state, UIControlService, GlobalConstantService, $uibModal, $stateParams) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.statusOptions = [];
        vm.status = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");

            VPCPRSponsorService.getStatusOptions(function (reply) {
                vm.statusOptions = reply.data;
                vm.statusOptions.push({
                    RefID: 0,
                    Name: "Semua"
                });
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
            });

            loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            UIControlService.loadLoading("");
            VPCPRSponsorService.select({
                Keyword: vm.srcText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
                Status: vm.status
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.cpr = reply.data.List;
                    vm.count = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.pageNumber = 1;
            loadAwal();
        };
        
        vm.detail = detail;
        function detail(wl) {
            var lempar = {
                VPCPRDataId: wl.VPCPRDataId,
                IsVhsCpr: wl.IsVhsCpr
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr-sponsor/cpr-list-sponsor.modal.html',
                controller: 'cprSponsorModal',
                controllerAs: 'cprListModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.appHistory = appHistory;
        function appHistory(wl) {
            var lempar = {
                VPCPRDataId: wl.VPCPRDataId,
                ContractName: wl.ContractNo + ' - ' + wl.TanderName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr-sponsor/approval-history.modal.html',
                controller: 'cprSponsorAppHistoryCtrl',
                controllerAs: 'listAppCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () { });
        };
    }
})();