﻿(function () {
	'use strict';

	angular.module("app").controller("rejectFormCtrl", ctrl);

	ctrl.$inject = ['$uibModalInstance', '$translatePartialLoader', 'UIControlService', 'item'];
	/* @ngInject */
	function ctrl($uibModalInstance, $translatePartialLoader, UIControlService, item) {
		var vm = this;
		vm.remark = "";

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('permintaan-ubah-data');
		};

		vm.reject = reject;
		function reject() {
			if (vm.remark == null || vm.remark == '') {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_INSERT_REASON.");
				return false;
			}

			$uibModalInstance.close(vm.remark);
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();