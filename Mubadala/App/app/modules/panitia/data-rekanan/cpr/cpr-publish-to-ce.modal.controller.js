﻿(function () {
    'use strict';

    angular.module("app").controller("SendCPRToCEController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VPCPRDataService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
		VPCPRDataService, UIControlService, item, $uibModalInstance) {
        //console.info("masuk modal");
        var vm = this;

        vm.VPCPRDataId = item.VPCPRDataId;

        vm.init = init;
        function init() {

        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

        vm.simpan = simpan;
        function simpan() {

            if (!vm.NoteForSkipVendorAcc) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NO_REASON");
                return;
            }

            UIControlService.loadLoadingModal("");
            VPCPRDataService.publishskipvendor({
                VPCPRDataId: vm.VPCPRDataId,
                NoteForSkipVendorAcc: vm.NoteForSkipVendorAcc
            }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "MESSAGE.SUCCESS_SUBMITDATA");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERROR_SUBMITDATA");
		    });
        }
    }
})();