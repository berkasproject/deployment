(function () {
    'use strict';

    angular.module("app").controller("CPRCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'CPRService', 'VPCPRDataService',
        '$state', 'UIControlService', 'UploadFileConfigService', 'UploaderService', 'GlobalConstantService',
        '$uibModal', '$stateParams', 'WarningLetterService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, CPRService, VPCPRDataService,
        $state, UIControlService, UploadFileConfigService, UploaderService, GlobalConstantService,
        $uibModal, $stateParams, WarningLetterService) {
        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        //vm.AddFlag = false;
        vm.srcText = '';
        vm.count = 0;
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.statusOptions = [];
        vm.status = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");

            VPCPRDataService.getStatusOptions(function (reply) {
                vm.statusOptions = reply.data;
                vm.statusOptions.push({
                    RefID: 0,
                    Name: "Semua"
                });
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
            });

            loadAwal();
        };

        vm.loadAwal = loadAwal;
        function loadAwal() {
            VPCPRDataService.cekRole(
            function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    //console.info("role: " +vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            UIControlService.loadLoading("");
            VPCPRDataService.select({
                Keyword: vm.srcText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
                Status: vm.status
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    
                    vm.cpr = reply.data.List;
                    vm.count = reply.data.Count;
                    /*
                    if (vm.cpr) {
                        if (vm.cpr[0].SysReference.Name == "STS_DRAFT")
                            vm.AddFlag = true;
                        else
                            vm.AddFlag = false;
                    } else {
                        vm.AddFlag = false;
                    }
                    */
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.pageNumber = 1;
            loadAwal();
        };
        
        vm.approve = approve;
        function approve(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_PUBLISH_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPCPRDataService.publish({
                        VPCPRDataId: VPdata.VPCPRDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            /*
                            if (VPdata.SysReference.Name == "STS_DRAFT") {
                                if (VPdata.Score < 66) {
                                    //VPCPRDataService.insertWL({
                                    //    VendorId: VPdata.VendorId,
                                    //    ProjManager: VPdata.ProjManager,
                                    //    DepartmentHolder: VPdata.DepartmentHolder,
                                    //    IsVhsCpr: VPdata.IsVhsCpr
                                    //}, function (reply) {
                                    //    if (reply.status === 200) {
                                    //        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.INSERT_WL_SUCC'));
                                    //    } else {
                                    //        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT_WL'));
                                    //    }
                                    //}, function (err) {
                                    //    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT_WL'));
                                    //});
                                    saveToWL(VPdata);
                                }
                            }
                            */
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.APPROVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APPROVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APPROVE_CPR'));
                    });
                }
            });
        };

        vm.reject = reject;
        function reject(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_REJECT_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPCPRDataService.reject({
                        VPCPRDataId: VPdata.VPCPRDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.REJECT_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_CPR'));
                    });
                }
            });
        };

        /*
        vm.saveToWL = saveToWL;
        function saveToWL(vpData) {

            WarningLetterService.insertWarningLetterByScoreCPR({
                VendorID: vpData.VendorId,
                ReporterID: vpData.DepartmentHolder,
                IDContract: vpData.ContractSignOffId

            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    console.log("succ. cpr data saved to WL");
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoadingModal();
            });
        }
        */

        vm.switchActive = switchActive;
        function switchActive(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_DELETE_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPCPRDataService.switchActive({
                        VPCPRDataId: VPdata.VPCPRDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SWITCH_DEACTIVE_CPR_SUCCES'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SWITCH_DEACTIVE_CPR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SWITCH_DEACTIVE_CPR'));
                    });
                }
            });
        };

        vm.detail = detail;
        function detail(wl) {
            var lempar = {
                flagRole: vm.FlagRole,
                VPCPRDataId: wl.VPCPRDataId,
                IsVhsCpr: wl.IsVhsCpr
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-list.modal.html?v=1.000004',
                controller: 'cprListModal',
                controllerAs: 'cprListModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.upload = upload;
        function upload(wl) {
            var lempar = {
                VPCPRDataId: wl.VPCPRDataId,
                VendorId: wl.VendorId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-form-lampiran.html',
                controller: 'frmUploadCPR',
                controllerAs: 'frmUploadCPR',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.sendToVendor = sendToVendor;
        function sendToVendor(VPdata) {
            bootbox.confirm($filter("translate")("CONFIRM_PUBLISH_CPR"), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPCPRDataService.sendToVendor({
                        VPCPRDataId: VPdata.VPCPRDataId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            loadAwal();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SEND_TO_VENDOR_SUCC'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_VENDOR'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_VENDOR_SUCC'));
                    });
                }
            });
        }

        vm.sendToCE = sendToCE;
        function sendToCE(wl) {
            var lempar = {
                VPCPRDataId: wl.VPCPRDataId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr/cpr-publish-to-ce.modal.html',
                controller: 'SendCPRToCEController',
                controllerAs: 'cprtoCECtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.appHistory = appHistory;
        function appHistory(wl) {
            var lempar = {
                VPCPRDataId: wl.VPCPRDataId,
                ContractName: wl.ContractNo + ' - ' + wl.TanderName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/cpr/approval-history.modal.html',
                controller: 'cprAppHistoryCtrl',
                controllerAs: 'listAppCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () { });
        };

        vm.tambahform = tambahform;
        function tambahform(type, act, id) {
            //if (vm.AddFlag == true) {
            //    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.FORBID_ADD_DATA'));
            //} else
                $state.transitionTo('form-cpr-vendor', { type: type, act: act, id: id });
        };

        vm.updateData = updateData;
        function updateData(type, act, id) {
            $state.transitionTo('form-cpr-vendor', { type: type, act: act, id: id });
        };

        vm.reminder = reminder;
        function reminder() {
            $state.transitionTo('cpr-reminder');
        };
    }
})();