﻿(function () {
    'use strict';

    angular.module("app")
    .controller("cprListModal", ctrl);

    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCPRDataService', 'UIControlService', 'item', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, VPCPRDataService, UIControlService, item, GlobalConstantService) {

        var vm = this;
        vm.VPCPRDataId = item.VPCPRDataId;
        vm.type = item.IsVhsCpr;
        vm.flagRole = item.flagRole;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        var kr = [];
        var krLv1 = [];
        var krLv2 = [];
        var krLv3 = [];
        var vendorName = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("cpr");

            VPCPRDataService.selectbyid({
                VPCPRDataId: vm.VPCPRDataId
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vpdata = reply.data[0];
                    console.info("vpdata" + JSON.stringify(vm.vpdata));
                    vendorName = vm.vpdata.VendorName;
                    loadDocs();

                    for (var i = 0; i < vm.vpdata.VPCPRDataDetails.length; i++) {
                        kr.push(vm.vpdata.VPCPRDataDetails[i]);
                    }

                    for (var i = 0; i < kr.length; i++) {
                        if (kr[i].Level === 1) {
                            krLv1.push(kr[i]);
                        }
                        else if (kr[i].Level === 2) {
                            krLv2.push(kr[i]);
                        }
                        else if (kr[i].Level === 3) {
                            krLv3.push(kr[i]);
                        }
                    }

                    for (var i = 0; i < krLv2.length; i++) {
                        krLv2[i].sub = [];
                        for (var j = 0; j < krLv3.length; j++) {
                            if (krLv3[j].Parent === krLv2[i].CriteriaId) {
                                krLv2[i].sub.push(krLv3[j]);
                            }
                        }
                    }

                    for (var i = 0; i < krLv1.length; i++) {
                        krLv1[i].sub = [];
                        for (var j = 0; j < krLv2.length; j++) {
                            if (krLv2[j].Parent === krLv1[i].CriteriaId) {
                                krLv1[i].sub.push(krLv2[j]);
                            }
                        }
                    }

                    vm.kriteria = krLv1;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_VPDATA'));
            });
        };

        function loadDocs() {
            VPCPRDataService.selectDocsCPRId({
                VPCPRDataId: vm.VPCPRDataId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status == 200) {
                    vm.docs = reply.data;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
                UIControlService.unloadLoading();
            });
        }

        vm.generateKriteria=[];
        vm.generateKriteria2 = [];
        vm.generateDoc = [];
        vm.generateDoc2 = [];

        vm.printForm = printForm;
        function printForm() {
            console.info("kriteria" + JSON.stringify(vm.kriteria));
            var note = '';
            if (vm.vpdata.Note != null) {
                note = vm.vpdata.Note;
            }
            else {
                note= '\n\n\n';
            }
            //header Kriteria
            var newmap1 = {
                text: $filter('translate')('KPI_DESCRIPTION'), color: 'white', fillColor: '#007E7A',bold:true
            };
            var newmap2 = {
                text: $filter('translate')('SCORE'), color: 'white', fillColor: '#007E7A', bold: true
            };
            var newmap3 = {
                text: $filter('translate')('REMARKS'), color: 'white', fillColor: '#007E7A', bold: true
            };
            vm.generateKriteria.push(newmap1, newmap2, newmap3);
            vm.generateKriteria2[0] = vm.generateKriteria;
            vm.generateKriteria = [];

            var indeks = 0;
            for (var i = 0; i <= vm.kriteria.length - 1; i++) {
                indeks = indeks + 1;

                var newmap1 = {
                    text: vm.kriteria[i].CriteriaName,
                    colSpan: 3,
                    bold: true
                }
                var newmap2 = {};
                var newmap3 = {};
                vm.generateKriteria.push(newmap1, newmap2, newmap3);
                vm.generateKriteria2[indeks] = vm.generateKriteria;
                vm.generateKriteria = [];

                if (vm.kriteria[i].sub.length > 0 || vm.kriteria[i].sub != null) {
                    for (var j = 0; j <= vm.kriteria[i].sub.length - 1; j++) {
                        indeks = indeks + 1;

                        var newmap1 = {
                            text: ' ' + vm.kriteria[i].sub[j].CriteriaName,
                            colSpan: 3,
                            bold: true
                        }
                        var newmap2 = {};
                        var newmap3 = {};

                        vm.generateKriteria.push(newmap1, newmap2, newmap3);
                        vm.generateKriteria2[indeks] = vm.generateKriteria;
                        vm.generateKriteria = [];

                        if (vm.kriteria[i].sub[j].sub.length > 0) {
                            for (var k = 0; k <= vm.kriteria[i].sub[j].sub.length - 1; k++) {
                                indeks = indeks + 1;
                                var remark = '';
                                if (vm.kriteria[i].sub[j].sub[k].Remark != null) {
                                    remark = vm.kriteria[i].sub[j].sub[k].Remark;
                                }
                                var newmap1 = {
                                    text: ' ' + vm.kriteria[i].sub[j].sub[k].CriteriaName
                                }
                                var newmap2 = {
                                    text: ' '+vm.kriteria[i].sub[j].sub[k].Score
                                };
                                var newmap3 = {
                                    text: remark
                                };

                                vm.generateKriteria.push(newmap1, newmap2, newmap3);
                                vm.generateKriteria2[indeks] = vm.generateKriteria;
                                vm.generateKriteria = [];

                            }
                        }

                    }
                }
            }

            //console.info("generateKriteria" + JSON.stringify(vm.generateKriteria2));


            //header Dokumen
            var newmap1 = {
                text: $filter('translate')('TABLE.COL_NO'),  bold: true
            };
            var newmap2 = {
                text: $filter('translate')('TABLE.NAMA_DOC'), bold: true
            };
            vm.generateDoc.push(newmap1, newmap2);
            vm.generateDoc2[0] = vm.generateDoc;
            vm.generateDoc = [];

            if (vm.docs!=null) {
                for (var i = 0; i <= vm.docs.length - 1; i++) {
                    var newmap1 = {
                        text: (i + 1).toString(),
                        style: "textRight"
                    }
                    var newmap2 = {
                        text: vm.DocName
                    };
                    vm.generateDoc.push(newmap1, newmap2);
                    vm.generateDoc2[i + 1] = vm.generateDoc;
                    vm.generateDoc = [];
                }
            }
            else {
                var newmap1 = {
                    text: $filter('translate')('NO_DATA'),
                    colSpan: 2
                }
                var newmap2 = {};
                vm.generateDoc.push(newmap1, newmap2);
                vm.generateDoc2[1] = vm.generateDoc;
                vm.generateDoc = [];
            }


            var businessName = '';
            if (vm.vpdata.businessName != undefined) {
                businessName = ', ' + vm.vpdata.businessName;
            }
            
            var documentDefinition = {
                pageMargins: [40, 40, 40, 100],
                footer: {
                    style:"tableStyleFooter",
                    table:
                        {
                            margin: [40, 20, 40, 'auto'],
                            widths: [160, 160, '*'],
                            body: [
                               [{ text: "Prepared By", alignment: "center", bold: true}, { text: "Reviewed By", alignment: "center", bold: true }, { text: "Accepted By", alignment: "center", bold: true }],
                               [{ text: "", rowSpan: 9 }, { text: "", rowSpan: 9 }, { text: "", rowSpan: 9 }],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{}, {}, {}],
                               [{ text: vm.vpdata.ProjectManagerId + ' - ' + vm.vpdata.ProjManager, alignment: "center", bold: true }, { text: vm.vpdata.ContractEngineerId + ' - ' + vm.vpdata.ContractEngineer, alignment: "center", bold: true }, { text: vm.vpdata.VendorName + businessName, alignment: "center", bold: true }],
                            ]
                        }
                },
                content:
                [
                    {
                        style: 'marginAtas',
                        columns: [
				            {
				                text: 'Contractor Performance Review',
				                bold: true,
				                alignment: "left",
				                fontSize: 12,
				                color: '#007E7A',
                                margin:[0,20,0,0]
				            },
                            {
                                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAyCAYAAACQyQOIAAANoklEQVR4nO1de5AcRRn/dmfvwsMcyWWXA8UUAgEVLVFBSIDyeJSISKkl6xPKKNYKyuZub3u2HzO70xcKpaBACUgBicgjDzhCILvJlbzEKrUUTSWlsXgEBBF5JbcbAwnJZe+u/WO6Z3pf98ru3uHdV9WV25nunn785vf9uuebCYBu0agB0ajh/U4kDoc0/XyI29xw7M0hbu8wHHuP4djDIW4Lw7GLBrd2hhzrTyFu3xzi9oUQj8+RpQPAO0PAIQhTZJxD8GkOoac5hAAgoJ0K1Dg+w41DsAQAafJpg7MVBrdeDnFbTDDtCGaYAzz1wap1N8H6+sDo64OKa265E1q23Akt5cdnAQHgsoC6a7m1xODWk/rEGtwaMbg1JNOIwa2RivOOPVx+3uDW3hC3bwBC5ldcp0HGOQR1ABRy8xcWcuGr8rnwukI2vLWQjbxSyIX/VchFthVy4dUD2QVXvLrmqPmqLMx4MPBk2ODWam9yHXvYcOyicgETSeVlDW69ZWSs77rXaRg7BCQAAgAA+Vz74nw28nA+F9lfyEVE9RQeKeQiopCNDOSz4d5dG8NzVV0NaN/0NyNjXWo49qty0oYMbg1Nwh1UAsJliqKmJx4BSiMAAMA7Q/Vqf18fGPJuhoENCz5WyIazpZMdLhZy4WIhGx4uSe6xIQWIfC78n0Ju/kIAjx1mlkmhJwzHHqwHAGoxhATHG6EMOw8AFBgmfffpbuCt++DIgWzk+kI2POje5XKi1V0/agoflEB4QnMrM5AVbPtEg1tvqLu4EWDQVhgjBrdGghmWBIDJ6oaALu4Kj0a+VMhGXpYAGHLv8rEmX6ZseFi6h1fe2vCBo1X99Rvc95kZGevryjU0CghV2OFujxXGpxsCT3MIKdrOP9x+XD4X6StzAeNggBK3MVLIhod2PRLuBHDdTONGebqb9Nchx/q5unMbCgaXFQ7K378DShfo7ahontwLAHmnvtYHhxeyC3A+F96jscDw+AHgAaFYyEVEPhtGAN4SckZbAAACEIu1hBzrj81ghhLAOdYLwOnJAODpBjX5umh7cQXMyW+M/EBzA8MTcgPVQJCLPAQw05lANzXgduoUw7F3N1ovlIPB4NYApOm5JW1RS8FH2z9cyC7AHgAknU/MDVTqgnwuvOO1vrZ2/VqzBuBRs5Gxvt0sVtCvYzj2oMHT3xICQv9cf+zpO7OReD4beSyfjbynTeIk3UCFLhjM59oXA8yyQXVTYODstmboBY0Zhlu5JSCdOeDccfHbQ/1tYncuInZlO3wqPyQAuCkvXcJAdkEcYFYXjGYuRcbjc0Lc/kuTmWGkxbGHIcPFlbdeNrJnU3tx36b5QzuzHZNzASUAiIh87ujink0LxK6NR68GmGWCsU356DQ+1eDWO83SCyq1cDYC9rViyY0/Es9u+IgY7p8rdmU7xECuY1Ig2JXtEIVsePjA5nniz32nPHv9nRceJXs6qwvGNF8vfK+ZrKDSYb1MQLpXzF9OxH33LhbF/jaxd3O72Jk9ZlyAyEsA7MweI97bPG/kYH+buGXVeQeO4YnPAQBEo7NsMH6TmzyGY93ZTL2g0pxeSwSdjID0chH9xeVi+8MnipH+uWLv5nYxkHMneVe2oySpY7tzEXFg8zwx1N8mtq0/qXjJzUsFsOt/AgDQWcfnHDPFXOrksSNC3N46FczQyi0xp9cSkF4ujuxlInH7V8XW9YvEe5vni+H+uaLY3yYG++eJwf55otjf5h3ble0Qj607TXx/RbTYyi0B7Lo1AADNjov4/zE1cGn8KRlf0FS9oLODwdMC7GtFK7fEhTddKfhdF4u1958pNq39jMiu+ay4/97F4mcrvyAuX/EdccrPlgnIOMOQXi5C3H72BByb1QWHbP6S8odTwQo+O7jaIcRtAeleAfa17r8Z7qb0cnlsuYB0ZqSVW6KVW4MtaXomAMyyQV3M1wu/ngq9UA6IOb2WOKyXiTm9lpATXnKsxbGKIW6LILeuAYC6xj/MdHMpNZWaG3Ks7RIME45eakbytq0de1YXNMTUgHLrdMOx90s30XS9MAYIXHA61nPAu+fJls/qgrqbpNhgxr56ql1EBQhU0KxjDwK3zgKACbNBNBo1Ojs7Q52dnaHoOMtOpMxk6geAgCrT2TkxF6dfb6w0kXpV7a5ekMGu0wUMqh3BjB0HgHroggCMzSbjyVMr70TKTktzG4/xUSHHeq6EkqcYBAa31gLApHUBShGKML3DTLGVyRS5qqS/lRYEAEim6BUI01UI07tQilyjnyvPaxKy1EyxlQjTVT0m+WaNvMoCAADxOI0kMV2BML0dYXpTPB5vG6Nd3jkTszjCdFUS0xVJTG9DhN6qkvqdxHQFIvTWUeoaxXy9cJYKep0qvaCB8PnJ6gJFjYiw603ChJtoobu7+9ga9QUAAJYuXXoYwuwlVSZJWEKvT897NSHzTUxf9+rH7PloNNo6SnuDAADLTHMRwnTIJEwgwt41TfOYcfQxCACAMH3KJEyo8lXSsNsW+vg4h6qKeXqBdU2Vi1DBsAa3DkKaLgaAybKBe8ea5okI070mYYMmYQKl6DKAiokF5d8Rxpf4A0rfisdluL42SV7eFL1S5t2PMD3gThC+VM9TrU2JBDnJz892d3WxjvJr1CqLCH1MAuGABMOwSvL3foTpmqvVS0iTNm9/wX5wKsDg6QJuLQOAQ9IF/uSy9dpd+9fR8pqErlZ5EaZ36ec0C8h6fy/zFmUSCLP1NcoA1IERTEyf9PtCe7swXmia5mkIsU8kMP7kNSn5WmIdtIp8HpFoNxzrxWbqBW2/YB24o3lI+wXld7k/+KRTP6/63NXFOkxCB9RAJ0w31K5sUoMAAD0YL/EAQ9i7inFMwvYlCDlBz1tetl5ASBISA6hkN5m3DqLVex5Bz631fmTDdIFjvQA+rR1qZwIA7kAhQrf7E0fvcbvp9tPTEz7VCxOzZ6oPjWSOFFupAWG5xg4CpQjV69WsvkBIkR8DAMRisSOi0WhrNBptjcViLVBbrE7ClF7gFmq0i6ijLqgwXzQS7FM+261RaBCqDLJaLVQTid3d3ccizPKKZbq6WIeJaUoD2natnD65zWKEOpuMbDK4taGRYPD3C1iXe926diwIANCF8UIT03cQpiMmYSKJ3WsplZ9M0o/79E4Hqk2OxhzdGqh+AwCQwPhUhOlBVb9pki/K+ivcSv00AnsGYXq3Sehak9DViND7TEI3Ioy/cqiDVm5SL6CjDcd6pRF6QdsveADckav7c4SqQpDQLQD65JK0JhLv1suVjAVAEGG2zadneoXKgAh9Qluqrq1SR92AUGP5WJRsZk16sGqa7EiI2+fXWys0SBdU6YLbh54Uu6CaaIzFYi2I0L/5IKkQk97fSYwv0tjg5VgsdgT4q4hvaIr+nS6MF8riQf3fOgFhxMT0vyamryNM30SYvmli+ppJ2KBiu/qbrxdYvVxEiS7g1hIAaMZTxQAidIs2kfcCACBCzh/v8hJh+qA22X9PEpZEKeKYmBKUYjeYhBW9uxVTBFDiw+urEeSEJxKJ9hjGR8Xj8bbu7u55cf9TRw0wXy9skhN5SMEs2n5Bt1t/YwWP5t+XaWp/z9KlSw9DhN5SPrhlAszbnDIJ26dYxdMDZTt73iRjtg18NgjA+1Ys6qZC4hk71uDWvw9FL2j7BQ8CQLPiCyoUvzvxlPlLS1pIJBIf0vMD+AOdxDSjfHENECiNMSJ3+0RPil3gdjFqQA0gqBWMlkdPnjYBqFg+XgUAEI/H50SjUUNP0AAX65vSCxl20WSBoJXZATzR1PcUy/cAEKZDiLB3te3ee/V8etsSicThJmY7tOVlOpkkZ/Sk2AWIkM6eFDuvJ8XOS2J8jonpP8rdz2hAmMgW89QzgjL/eYQzUb3g6QLHLkKang0ATY028oBg0rMVjWtJIELO1/PpfydT9DLNpbxxdRVxW2vPQrGMqqsECJjuRYgu7ibk+J4eevIy01y0zDQXJRLkpK5U6hTlNry2l2gEasfjNJJM0o/q5Xp66Mmau2mgKb3gWI9NRC9o+wUJt54piTt0FT6hf5AT8V4Nf65MPvVjmzQg/BKgkpLlrh4kCDkBYboXYXpQ35xSexaJBDnJJGxQ6oyDcv+iPO2Ty9z79LImpo/Ldh+Q+xYH9HKqPyahqxs/lN6n+1LHeZ/oGcNNaPsFfeD2bKriDvVnBfsUGySJ++mfaiIxmSRnaL5fICR3P6ts5Wqri4f11QVo/n6ZaS7yWKi21pAPseg6WW+rrHfUx9D+cfpAY4avsscyqol+eSwg+F9hs170vqYytZE87uNgjE81CX0AYbpVW/NXPm7G+FKE6YOI0HsQYT8dIyTNrduk5yJM15kpthIRek88lTpOZejqYh0y8OVuE7NfVU1usMu6ch2ACDFNQtciTFeNUm6NFoTTBPM/0XNdLb1Qogu4dQ4ATJco5PIndBMB5njyvq9D1iZjAQAIGI71VDW9oO0X9ADAdHsfIagttUYNE5tEgGpwlEDSkuDVMQJQS9zPeINXy8s13jy9QI43HPttxQJl+wUPyV5MByaYtYaZ/8r915Qm0D7X+9I00QWz1hTz9cKNig0Mbg15H9SaZYMZY66fjUYNg1u/lfsFjYgvmLVpb/5/DXC69z7CTPwY9jSx/wHaStM+GgZv+AAAAABJRU5ErkJggg==',
                                width: 70,
                                height: 28,
                                alignment: 'right'
                            }
                        ]
                    },
                    {
                        style:"tableStyleHeader",
                        table:
                        {
                            widths: [150, 240,'*'],
                            body: [
                               [{ text: 'Assessment Date :', color: 'white', fillColor: '#007E7A' }, { text: UIControlService.convertDate(vm.vpdata.CPRDate), colSpan:2 }, {}],
					           [{ text: 'Contract No :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.ContractNo + ' - ' + vm.vpdata.TanderName, colSpan: 2 }, {}],
                               [{ text: 'Contract Date :', color: 'white', fillColor: '#007E7A' }, {text: UIControlService.convertDate(vm.vpdata.ContractStartDate) + ' - ' + UIControlService.convertDate(vm.vpdata.ContractEndDate), colSpan: 2},{}],
                               [{ text: 'Contractor :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.VendorName + businessName, colSpan: 2 }, {}],
                               [{ text: 'Department Holder :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.Department, colSpan: 2 }, {}],
                               [{ text: 'Sponsor/Project Manager :', color: 'white', fillColor: '#007E7A' }, { text: vm.vpdata.Sponsor + '/' + vm.vpdata.ProjectManagerId + ' - ' + vm.vpdata.ProjManager }, { text: 'Score: ' + vm.vpdata.Score + ' %', fontSize: 12, bold: true, rowSpan: 2, margin: [0, 7, 0, 0] }],
                               [{ text: 'Contract Engineer :', color: 'white', fillColor: '#007E7A' }, vm.vpdata.ContractEngineerId + ' - ' + vm.vpdata.ContractEngineer, {}],
                            ]
                        }
                    },
                    {
                        style: "tableStyle",
                        table:
                        {
                            widths: [250, 25, '*'],
                            body: vm.generateKriteria2
                        }
                    },
                    {
                        text: $filter('translate')('FORM.NOTE'),
                        bold: true,
                        style: "marginAtas",
                        fontSize:7
                    },
                    {
                        style: "tableStyleMargin2",
                        table:
                        {
                            widths: ['*'],
                            body: [
                               [{ text: note }]
                            ]
                        }
                    }/*,
                    {
                        text: $filter('translate')('FORM.DOC'),
                        bold: true,
                        style: "marginAtas",
                        fontSize: 7
                    },
                    {
                        style: "tableStyleMargin2",
                        table:
                        {
                            widths: [15,250],
                            body: vm.generateDoc2
                        }
                    }*/
                ],



                styles:
                {
                    tableHeader: {
                        bold: true,
                        alignment: 'center',
                        fillColor: '#EAC2C2'
                    },
                    tFoot: {
                        bold: true,
                        alignment: 'right',
                        fillColor: '#EAC2C2'
                    },
                    textRight: {
                        alignment: 'right'
                    },
                    marginAtas: {
                        margin: [0, 10, 0, 0],
                    },
                    tableStyleHeader: {
                        fontSize: 10,
                        margin: [0, 20, 0, 0],
                        alignment: 'left'
                    },
                    tableStyle: {
                        fontSize: 7,
                        margin: [0, 10, 0, 0],
                        alignment: 'left'
                    },
                    tableHeader2: {
                        bold: true,
                        alignment: 'center'
                    },
                    tableStyle2: {
                        fontSize: 7,
                        margin: [0, 10, 0, 0],
                        alignment: 'left'
                    },
                    tableStyleMargin2: {
                        fontSize: 7,
                        margin: [0, 2, 0, 0],
                        alignment: 'left'
                    },
                    tableStyleMargin100: {
                        fontSize: 10,
                        margin: [0, 100, 0, 0],

                    },
                    tableStyleFooter: {
                        margin: [40, 10, 40, 'auto'],
                        fontSize: 8,
                        alignment: 'left'
                    }
                }
            };



            pdfMake.createPdf(documentDefinition).download('Contractor Performance Review: ' + vm.vpdata.VendorName + ' (' + vm.vpdata.ContractNo + '-' + vm.vpdata.TanderName + ').pdf');



            /*
            $uibModalInstance.close();

            var innerContents = document.getElementById('formCPR').innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Contractor Performance Review - ' + vendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();*/

            //$uibModalInstance.close();
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();