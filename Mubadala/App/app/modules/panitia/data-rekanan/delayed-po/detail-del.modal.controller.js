(function () {
    'use strict';

    angular.module("app")
    .controller("DetailDelayedDeliveryCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DelayedPOService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, DelayedPOService, UIControlService) {

        var vm = this;

        vm.dels = [];
        vm.PurchasingDoc = item.PurchasingDoc;
        vm.VendorName = item.VendorName;
        vm.RFQCode = item.RFQCode;

        vm.init = init;
        function init() {
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("");
            DelayedPOService.GetDELs({
                VendorSAP: item.VendorSAP,
                PurchasingDoc: item.PurchasingDoc
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.dels = reply.data;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DELS'));
            });
        }
               
        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();