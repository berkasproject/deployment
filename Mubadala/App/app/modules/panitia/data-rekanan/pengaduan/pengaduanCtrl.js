(function () {
    'use strict';

    angular.module("app").controller("ComplaintCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ComplaintService', 'RoleService', 'UIControlService', '$uibModal', 'WarningLetterService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, ComplaintService,
        RoleService, UIControlService, $uibModal, WarningLetterService) {
        var vm = this;
        vm.search = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.searchBy = "1";

        function init() {
            $translatePartialLoader.addPart('master-complaint');
            jLoad(1);
            cekRole();
            cekUser();
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("");
            vm.complaint = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            ComplaintService.select({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.search,
                column: vm.searchBy
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.complaint = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DEPARTMENT_COMPLAINT'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DEPARTMENT_COMPLAINT'));
                UIControlService.unloadLoading();
            });
        }
        vm.cariReporter = cariReporter;
        function cariReporter() {
            init();
        }

        vm.tambah = tambah;
        function tambah(data, act) {
            var data = {
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/warning-letter/FormWarningLetter.html?v=1.000003',
                controller: 'FormWarningLetterCtrl',
                controllerAs: 'frmWarningLetterCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            //UIControlService.loadLoading($filter('translate')('MESSAGE.LOADING'));
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CHANGESTATUS'), function (yes) {
                if (yes) {
                    ComplaintService.editActive({
                        ComplaintID: data.ComplaintID,
                        IsActive: active
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            var msg = "";
                            if (active === false) msg = "{{'Non-aktifkan'|translate}}";
                            if (active === true) msg = "{{'Aktifkan'|translate}}";
                            UIControlService.msg_growl("success", "{{'MESSAGE.NOTIF'|translate}} " + msg);
                            jLoad(1);
                        }
                        else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHANGE_STATUS'));
                            return;
                        }
                    }, function (err) {

                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHANGE_STATUS'));
                        UIControlService.unloadLoading();
                    });
                }
            });            
        }

        vm.lihatDetail = lihatDetail;
        function lihatDetail(data) {
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/pengaduan/DetailComplaint.html',
                controller: 'DetailPengaduanCtrl',
                controllerAs: 'DetailPengaduanCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        function cekRole() {
            WarningLetterService.cekRole(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.role = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_ROLE'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_ROLE'));
                UIControlService.unloadLoading();
            });
        };

        vm.cekUser = cekUser;
        function cekUser() {                        
            ComplaintService.getCurrentUser(
                function (response) {
                    if (response.status === 200) {                       
                        vm.currentUser = response.data;
                    }
                    else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CURR_USER'));
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_CURR_USER'));
                    return;
                });
            
        }

        // Send data to Procurement Supp with status draft       
        vm.sendToProc = sendToProc;
        function sendToProc(data) {
            /*
            var item = {
                ComplaintID: data.ComplaintID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/pengaduan/detailApp.modal.html',
                controller: 'detailAppPengaduanCtrl',
                controllerAs: 'detailAppCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
            */
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_TO_PROC'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    WarningLetterService.stateSent({
                        ComplaintID: data.ComplaintID
                    }, function (reply) {
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_SEND_DRAFT'));
                        UIControlService.unloadLoading();
                        vm.jLoad(1);
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_SEND_DRAFT'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        
    }
})();
//TODO



















//angular.module('eprocAppPanitia')

//.controller( 'pengaduanCtrl', function( $scope, $rootScope, $modal, $state, $cookieStore, $http ){
//    $scope.warningletter = [];
//    $scope.init = function(){
//        $scope.pengaduanlist = [
//            {id: 1, nm_pengadu:"Michael", department_pengadu:"DHS", tgl:"3-06-2016", vendor:"CV. Maju Karya",ket:"pelanggaran lalin"},
//            {id: 2, nm_pengadu:"Steven", department_pengadu:"Finance", tgl:"3-06-2016", vendor:"PT. Abadi Sentosa", ket:"pemalsuan bukti pembayaran"}
//        ];
//    };
    
//    $scope.formpengaduan = function(data,id) {
//        var kirim_data ={ id:id, data: data};
//        var modalInstance = $modal.open({
//            templateUrl: 'formPengaduan.html',
//            controller: formPengaduan,
//            resolve: {
//                item: function() {
//                    return kirim_data;
//                }
//            }
//        });
//        modalInstance.result.then(function() {
//            $scope.init();
//        });
//    };
    
//});
//var formPengaduan = function($scope, $modalInstance, $http, item, $cookieStore, $rootScope) {
//    $scope.id_data = item.id;
//    $scope.data = item.data;
//    $scope.action = "";
//    $scope.rincian = "";
//    $scope.customTinymce = {
//        theme: "modern",
//        plugins: [
//            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
//            "searchreplace wordcount visualblocks visualchars fullscreen",
//            "insertdatetime media nonbreaking save table contextmenu directionality",
//            "emoticons template paste textcolor"
//        ],
//        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
//        toolbar2: "print preview media | forecolor backcolor",
//        image_advtab: true,
//        height: "200px",
//        width: "auto"
//    };
    
//    $scope.init = function(){
//        if($scope.id_data === 0){ $scope.action = "Tambah "; } else { $scope.action = "Ubah "; }
//    };
        
//    $scope.batal = function() {
//        $modalInstance.dismiss('cancel');
//    };    
//};

//var formGenerate = function($scope, $modalInstance, $http, item, $cookieStore, $rootScope) {
//    $scope.batal = function() {
//        $modalInstance.dismiss('cancel');
//    };
//};