(function () {
    'use strict';

    angular.module("app").controller("DetailPengaduanCtrl", ctrl);

    ctrl.$inject = ['$http', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ComplaintService', 'RoleService', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService'];
    function ctrl($http,item, $translate, $translatePartialLoader, $location, SocketService, ComplaintService,
        RoleService, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService) {
        var vm = this;
        vm.search = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.endpointAttachment = GlobalConstantService.getConstant('api') + "/";
        function init() {
            $translatePartialLoader.addPart('master-complaint');
            vm.name = item.item.ReporterName;
            vm.departement = item.item.department.DepartmentName;
            vm.depCode = item.item.department.DepartmentCode;
            vm.vendor = item.item.vendor.VendorName;
            vm.businessName = item.item.vendor.businessName;;
            vm.date = item.item.Date;
            vm.description = item.item.Description;
            vm.typeName = item.item.ComplainType.TypeName;
            vm.statusApp = item.item.SysReference1.Name;
            vm.docUrl = item.item.DocUrl;

            if (vm.statusApp === "WL_INCOMPLETE" || vm.statusApp === "WL_CANCEL_BY_PROC" || vm.statusApp === "WL_CLOSED") {
                vm.note = item.item.CancelRemark;
                vm.noteDocUrl = item.item.CancelDocUrl;
            }
            else if (item.item.Remark == null || item.item.Remark == "") {
                vm.note = "-";
            }
            else {
                vm.note = item.item.Remark;
            }
            
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();