﻿(function () {
    'use strict';

    angular.module("app")
    .controller("PengaduanPilihVendorController", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'ComplaintService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, ComplaintService, UIControlService, item) {

        var vm = this;
        var loadmsg = "";

        vm.count;
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.searchText = "";
        vm.vendor = [];

        vm.onBatalClick = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.onSearchSubmit = function (searchText) {
            vm.searchText = searchText;
            vm.loadData();
        };

        vm.init = init;
        function init() {
            vm.loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("");
            ComplaintService.SelectVendor({
                Keyword: vm.searchText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                vm.vendor = reply.data.List;
                vm.count = reply.data.Count;
                UIControlService.unloadLoadingModal();
            }, function (error) {
                UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_LOAD_VENDOR'));
                UIControlService.unloadLoadingModal();
            });
        };

        vm.onSelectClick = onSelectClick;
        function onSelectClick(vendor) {
            $uibModalInstance.close(vendor);
        };
    }
})();