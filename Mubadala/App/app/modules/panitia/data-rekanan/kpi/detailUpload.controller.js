﻿(function () {
	'use strict';

	angular.module("app").controller("DetailUploadVendorKPICtrl", ctrl);

	ctrl.$inject = ['VPKPIService', 'UIControlService', 'UploaderService', '$uibModal', '$stateParams', 'item', '$uibModalInstance', '$translate', '$translatePartialLoader'];

	function ctrl(VPKPIService, UIControlService, UploaderService, $uibModal, $stateParams, item, $uibModalInstance, $translate, $translatePartialLoader) {
		console.info("atur: "+JSON.stringify(item));
		var vm = this;
		vm.VPKPIID = item.VPKPIID;
		vm.DocType = item.DocType;

		vm.totalItems = 10;
		vm.currentPage = 1;
		vm.count = 0;
		vm.pageNumber = 1;
		vm.pageSize = 10;

		vm.dataDEL = [];
		vm.dataMDR = [];
		vm.dataPOH = [];
		vm.dataHdr = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('vp-kpi');
		    if (vm.DocType == 4351) {
		        detailUploadDEL(1);
		    }
		    else if (vm.DocType == 4352) {
		        detailUploadPOH(1);
		    }
		    else if (vm.DocType == 4354) {
		        detailUploadMDR(1);
		    }
		    else if (vm.DocType == 4445) {
                detailUploadHdrItm(1)
		    }
		}


		vm.detailUploadDEL = detailUploadDEL;
		function detailUploadDEL(current) {
		    vm.currentPage = current;
		    VPKPIService.detailUploadDEL({
		        column: vm.VPKPIID,
		        Limit: vm.pageSize,
		        Offset: vm.pageSize * (vm.currentPage - 1),
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.dataDEL = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("del:" + JSON.stringify(vm.dataDEL));
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.detailUploadPOH = detailUploadPOH;
		function detailUploadPOH(current) {
		    vm.currentPage = current;
		    VPKPIService.detailUploadPOH({
		        column: vm.VPKPIID,
		        Limit: vm.pageSize,
		        Offset: vm.pageSize * (vm.currentPage - 1),
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.dataPOH = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("poh:" + JSON.stringify(vm.dataPOH));
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.detailUploadMDR = detailUploadMDR;
		function detailUploadMDR(current) {
		    vm.currentPage = current;
		    VPKPIService.detailUploadMDR({
		        column: vm.VPKPIID,
		        Limit: vm.pageSize,
		        Offset: vm.pageSize * (vm.currentPage - 1),
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.dataMDR = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("mdr:" + JSON.stringify(vm.dataMDR));
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.detailUploadHdrItm = detailUploadHdrItm;
		function detailUploadHdrItm(current) {
		    vm.currentPage = current;
		    VPKPIService.detailUploadHdrItm({
		        column: vm.VPKPIID,
		        Limit: vm.pageSize,
		        Offset: vm.pageSize * (vm.currentPage - 1),
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.dataHdr = reply.data.List;
		            vm.totalItems = reply.data.Count;
		            console.info("HDR:" + JSON.stringify(vm.dataHdr));
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};


		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();