﻿(function () {
	'use strict';

	angular.module("app").controller("ScoreHistoryVendorKPICtrl", ctrl);

	ctrl.$inject = ['VPKPIService', 'UIControlService', 'UploaderService', '$uibModal', '$stateParams', 'item', '$uibModalInstance', '$translate','$translatePartialLoader'];

	function ctrl(VPKPIService, UIControlService, UploaderService, $uibModal, $stateParams, item, $uibModalInstance, $translate, $translatePartialLoader) {
		//console.info("atur: "+JSON.stringify(item));
		var vm = this;
		vm.VendorID = item.VendorID;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('vp-kpi');
		    scoreHistory();
		}


		vm.scoreHistory = scoreHistory;
		function scoreHistory() {
		    VPKPIService.scoreHistory({
		        column: vm.VendorID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.dataHistory = reply.data;
		            var businessname = "";
		            if (vm.dataHistory[0].Vendor.businessName != null) {
		                businessname = vm.dataHistory[0].Vendor.businessName + ". ";
		            }
		            vm.vendorName = businessname + vm.dataHistory[0].Vendor.VendorName;
		            //console.info("datahistory:" + JSON.stringify(vm.dataHistory));
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		};

		vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
		};

	}
})();