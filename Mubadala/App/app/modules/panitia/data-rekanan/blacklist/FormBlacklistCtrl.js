﻿(function () {
    'use strict';

    angular.module("app").controller("FormBlacklistCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BlacklistService', 'UploadFileConfigService', 'RoleService', 'UIControlService', 'UploaderService', 'item', '$uibModalInstance', '$state', 'GlobalConstantService', '$uibModal'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, BlacklistService,UploadFileConfigService,
        RoleService, UIControlService, UploaderService,item, $uibModalInstance, $state, GlobalConstantService, $uibModal) {
        
        var vm = this;
        var page_id = 141;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.Area;
        vm.flag = item.act;
        vm.data = item.item;
        vm.BlacklistID = item.item.BlacklistID;
        vm.VendorID = item.item.VendorID;
        vm.VendorName = item.item.Vendor.VendorName;
        vm.Description = "";
        vm.action = "";
        vm.DetailPerson = "";
        vm.actionblacklst = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;

        vm.DetailPersonform;
        
        vm.isCalendarOpened = [false, false, false, false];

        vm.pathFile;
        vm.fileUpload="";
        vm.size;
        vm.name;
        vm.type;
        vm.flag;
        vm.employee = [];
        vm.DocUrl = "";
        vm.DocUrlWhitelist = "";
        vm.vendorstock = [];
        vm.listperson = [];
        vm.selectedMasaBlacklist ="";
        vm.blacklistdate = {};
        vm.cek1 = false;
        vm.cekWhite = false;
        vm.BlacklistType = item.type;
        function init() {
            console.info(item);
            $translatePartialLoader.addPart("blacklist-data");
            if (vm.flag == true) {
                //get tipe dan max.size file - 1
                UploadFileConfigService.getByPageName("PAGE.ADMIN.BLACKLIST", function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        vm.name = response.data.name;
                        vm.idUploadConfigs = response.data;
                        vm.idFileTypes = generateFilterStrings(response.data);
                        vm.idFileSize = vm.idUploadConfigs[0];

                    } else {
                        UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoading();
                    return;
                });
                if (vm.data.ApprovalStatus !== null) {
                    if (vm.data.ApprovalStatus.Name == "APPROVAL_VENDOR_REJECTED") {
                        if (item.tipedetail == true) {
                            getByBlacklistCompPers(vm.BlacklistID);
                            vm.DocUrl = item.item.DocUrl;
                            vm.ReferenceNo = item.item.ReferenceNo;
                            vm.Description = item.item.BlacklistDescription;
                            vm.flag = false;
                        }
                        else {
                            vm.ReferenceNo = "";
                            vm.DocUrl = "";
                            vm.DocUrlWhitelist = "";
                            vm.BlacklistDescription = "";
                            getByID(vm.VendorID);
                        }
                    }
                    else {
                        getByBlacklistCompPers(vm.BlacklistID);
                        getMasaBlacklist();
                        if(vm.data.MasaBlacklist != null){
                            if (vm.data.MasaBlacklist.Name == "BLACKLIST_NO") {
                                vm.StartDate = new Date(Date.parse(vm.data.StartDate));
                                vm.EndDate = new Date(Date.parse(vm.data.EndDate));
                            }
                        }
                        vm.DocUrl = item.item.DocUrl;
                        vm.ReferenceNo = item.item.ReferenceNo;
                        vm.Description = item.item.BlacklistDescription;
                    }
                }
                else {
                    getByID(vm.VendorID);
                    getMasaBlacklist();

                }
                
               
            }
            else {
                if (vm.BlacklistType == 'BLACKLIST_TYPE_YES') {
                    vm.ReferenceNo = item.item.ReferenceNo;
                    vm.DocUrl = item.item.DocUrl;
                    vm.Description = item.item.BlacklistDescription;
                    getByBlacklistCompPers(item.item.BlacklistID);
                    getMasaBlacklist();
                    if (vm.data.MasaBlacklist.Name == "BLACKLIST_NO") {
                        vm.StartDate = new Date(Date.parse(vm.data.StartDate));
                        vm.EndDate = new Date(Date.parse(vm.data.EndDate));
                    }
                }
                else {
                    vm.cekWhite = true;
                    vm.DocUrlWhiteList = item.item.DocUrlWhiteList;
                    vm.DocUrl = item.item.DocUrl;
                    vm.ReferenceNo = item.item.ReffBlacklistModel.ReferenceNo;
                    vm.ReferenceNoWhitelist = item.item.ReferenceNo;
                    getByBlacklistCompPers(item.item.ReffBlacklistID);
                }
            }

        }

        vm.getByBlacklistCompPers = getByBlacklistCompPers;
        function getByBlacklistCompPers(id) {
            BlacklistService.selectdetailCompPers({
                Status: id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listemployee = reply.data;
                    vm.listperson = vm.listemployee.DetailPerson;
                    if (vm.flag === true) getByID(vm.VendorID);
                    else {
                        vm.employee = vm.listemployee.CompPers;
                        vm.vendorstock = vm.listemployee.VendorStock;
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('DETAIL.MESSAGE.ERR_SELECT'));
                    UIControlService.unloadLoading();
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
                return;
            });
        }

        vm.getByID = getByID;
        function getByID(id) {
            BlacklistService.SelectEmployeeVendor({
                Status: id
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.employee = reply.data.List;
                    getStock();
                    if (vm.BlacklistID != 0) {
                        if (vm.listemployee != undefined) {
                            vm.employee.forEach(function (data) {
                                vm.listemployee.CompPers.forEach(function (list) {
                                    if (data.ID == list.ID) {
                                        data.check = true;
                                        data.DetailBlacklistID = vm.listemployee.DetailBlacklistID;
                                    }
                                });
                            });
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('DETAIL.MESSAGE.ERR_SELECT'));
                    UIControlService.unloadLoading();
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
                return;
            });

       

        }

        vm.getStock = getStock;
        function getStock() {
            BlacklistService.SelectVendorStock({
                Status: vm.VendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorstock = reply.data.List;
                    if (vm.BlacklistID != 0) {
                        if (vm.listemployee != undefined) {
                            vm.vendorstock.forEach(function (data) {
                                vm.listemployee.VendorStock.forEach(function (list) {
                                    if (data.StockID == list.StockID) data.check = true;
                                });
                            });
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('DETAIL.MESSAGE.ERR_SELECT'));
                    UIControlService.unloadLoading();
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
                return;
            });


        }


        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        function convertAllDateToString() { // TIMEZONE (-)

            if (vm.blacklistdate.StartDate) {
                vm.blacklistdate.StartDate = UIControlService.getStrDate(vm.blacklistdate.StartDate);
            }
            if (vm.blacklistdate.EndDate) {
                vm.blacklistdate.EndDate = UIControlService.getStrDate(vm.blacklistdate.EndDate);
            }
        };

        //Supaya muncul di date picker saat awal load
        function convertToDate() {

            if (vm.blacklistdate.StartDate) {
                vm.blacklistdate.StartDate = new Date(Date.parse(vm.blacklistdate.StartDate));
            }
            if (vm.blacklistdate.EndDate) {
                vm.blacklist.EndDate = new Date(Date.parse(vm.blacklistdate.EndDate));
            }
        }

        //get tipe dan max.size file - 2
        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectUpload = selectUpload;
        //vm.fileUpload;
        function selectUpload() {
            console.info(JSON.stringify(vm.fileUpload));
        }

        /*start upload */
        vm.uploadFile = uploadFile;
        function uploadFile(data) {
            if (vm.fileUpload) {
                upload(data, vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
            }
            else {
                if (vm.BlacklistType == "BLACKLIST_TYPE_YES" && vm.DocUrl != "") {
                    openmodal(vm.VendorID, vm.Description, vm.DocUrl, vm.listperson);
                }
                else if (vm.BlacklistType == "BLACKLIST_TYPE_NO" && vm.DocUrlWhitelist != "") {
                    openmodal(vm.VendorID, vm.Description, vm.DocUrlWhitelist, vm.listperson);
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                    return;
                }

            }
        }

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }

        vm.upload = upload;
        function upload(data, file, config, filters, callback) {

            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flagSize = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flagSize = 1;
            }


            UIControlService.loadLoadingModal("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFileBlacklist(file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = url;
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        if (vm.flagSize == 0) {

                            vm.size = Math.floor(s)
                        }

                        if (vm.flagSize == 1) {
                            vm.size = Math.floor(s / (1024));
                        }
                        openmodal(vm.VendorID, vm.Description, vm.pathFile, vm.listperson);

                        //msg(vm.blacklist,vm.VendorID, vm.selectedMasaBlacklist.RefID, vm.Description, vm.pathFile, vm.blacklistdate.StartDate, vm.blacklistdate.EndDate, vm.listperson);
                        

                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                    UIControlService.unloadLoading();
                });
        }

        vm.addCheck = addCheck;
        var cek = 0;
        var cek1 = 0;
        function addCheck(act, data, checklist) {
            console.info(act);
            if (act == 0) {
                for (var i = 0; i < data.length; i++) {
                    data[i]["check"] = checklist;
                    for (var j = 0; j < vm.vendorstock.length; j++) {
                        if (vm.vendorstock[j].SysReference == null) {
                            if (vm.vendorstock[j].OwnerID == data[i].NoID) {
                                vm.vendorstock[j]["check"] = checklist;
                                if (vm.vendorstock.length === 1) vm.cek2 = checklist;

                            }
                        }
                        else if (vm.vendorstock[j].SysReference.Name == "STOCK_PERSONAL") {
                            if (vm.vendorstock[j].OwnerID == data[i].NoID) {
                                vm.vendorstock[j]["check"] = checklist;
                                if (vm.vendorstock.length === 1) vm.cek2 = checklist;

                            }
                        }
                    }
                }
            }
            else if (act == 1) {
                for (var i = 0; i < data.length; i++) {
                    data[i]["check"] = checklist;
                    for (var j = 0; j < vm.employee.length; j++) {
                        if (data[i].SysReference == null) {
                            if (vm.employee[j].NoID == data[i].OwnerID) {
                                vm.employee[j]["check"] = checklist;
                                if (vm.employee.length === 1) vm.cek1 = checklist;
                            }
                        }
                        else if (data[i].SysReference.Name == "STOCK_PERSONAL") {
                            if (vm.employee[j].NoID == data[i].OwnerID) {
                                vm.employee[j]["check"] = checklist;
                                if (vm.employee.length === 1) vm.cek1 = checklist;
                            }
                        }
                       
                    }
                }
            }
        }

        vm.addCheckbox = addCheckbox;
        var cek = 0;
        function addCheckbox(flag, data, dataall) {
            
            if (flag == 1) {
                for (var i = 0; i < vm.employee.length; i++) {
                    if (vm.employee[i].NoID == dataall.NoID) {
                        vm.employee[i]["check"] = data;
                        if (vm.employee.length === 1) vm.cek1 = data;
                    }
                }
                for (var i = 0; i < vm.vendorstock.length; i++) {
                    if (vm.vendorstock[i].SysReference == null) {
                        if (vm.vendorstock[i].OwnerID == dataall.NoID) {
                            vm.vendorstock[i]["check"] = data;
                            if (vm.vendorstock.length === 1) vm.cek2 = data;
                        }
                    }
                    else if (vm.vendorstock[i].SysReference.Name == "STOCK_PERSONAL") {
                        if (vm.vendorstock[i].OwnerID == dataall.NoID) {
                            vm.vendorstock[i]["check"] = data;
                            if (vm.vendorstock.length === 1) vm.cek2 = data;
                        }
                    }
                    
                }
                if (vm.employee.length === 1) {
                    if (data == false) vm.cek1 = false;
                    else vm.cek1 = true;
                }
                else {
                    var ii =0;
                    for (var i = 0; i < vm.employee.length; i++) {
                        if (vm.employee[i]["check"] == true) ii += 1;
                    }
                    if (ii === vm.employee.length) {
                        vm.cek1 = true;
                    }
                    else {
                        vm.cek1 = false;
                    }
                }
            }
            else if (flag == 2) {
                for (var i = 0; i < vm.employee.length; i++) {
                    if (dataall.SysReference == null) {
                        if (vm.employee[i].NoID == dataall.OwnerID) {
                            vm.employee[i]["check"] = data;
                            if (vm.employee.length === 1) vm.cek1 = data;
                        }
                    }
                    else if (dataall.SysReference.Name == "STOCK_PERSONAL") {
                        if (vm.employee[i].NoID == dataall.OwnerID) {
                            vm.employee[i]["check"] = data;
                            if (vm.employee.length === 1) vm.cek1 = data;
                        }
                    }
                    
                }
                for (var i = 0; i < vm.vendorstock.length; i++) {
                    if (vm.vendorstock[i].OwnerID == dataall.NoID) {
                        vm.vendorstock[i]["check"] = data;
                        if (vm.vendorstock.length === 1) vm.cek2 = data;
                    }
                }
                if (vm.vendorstock.length === 1) {
                    if (data == false) vm.cek2 = false;
                    else vm.cek2 = true;
                }
                else {
                    var ii = 0;
                    for (var i = 0; i < vm.vendorstock.length; i++) {
                        if (vm.vendorstock[i]["check"] == true) ii += 1;
                    }
                    if (ii === vm.vendorstock.length) {
                        vm.cek2 = true;
                    }
                    else {
                        vm.cek2 = false;
                    }
                }
            }
            

        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        vm.batalkan = batalkan;
        function batalkan() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            BlacklistService.editBlacklist({
                BlacklistID: item.item.BlacklistID,
                BlacklistTypeID: item.item.BlacklistTypeID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //var msg = "";
                    //if (active === false) msg = " NonAktifkan ";
                    //if (active === true) msg = "Aktifkan ";
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_BL");
                    $uibModalInstance.close();
                    vm.jLoad(1);
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_INACTIVATE");
                    return;
                }
            }, function (err) {

                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoading();
            });

        }

        vm.msg = msg;
        function msg(data, VendorID, MasaBlacklistID, BlacklistDescription, DocUrl, StartDate, EndDate, DetailPerson) {
            bootbox.confirm('<h3 class="afta-font">Yakin akan blacklist ?</h3>', function (res) {
               if (res) {
                    BlacklistService.InsertBlacklist({
                        VendorID: VendorID,
                        MasaBlacklistID: MasaBlacklistID,
                        BlacklistDescription: BlacklistDescription,
                        DocUrl: DocUrl,
                        BlacklistTypeID: 2060,
                        StartDateBlacklist: StartDate,
                        EndDateBlacklist: EndDate
                    },
                              function (reply) {
                                  UIControlService.unloadLoadingModal();
                                  if (reply.status === 200) {
                                      $uibModalInstance.close();

                                  }
                                  else {
                                      UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
                                      return;
                                  }
                              },
                              function (err) {
                                  UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                                  UIControlService.unloadLoadingModal();
                              }
                          );


                    for (var i = 0; i < data.length; i++) {
                        if (i == ((data.length) - 1) && data.length != 0 ) {
                            BlacklistService.insertDetail({
                                VendorID: VendorID,
                                DetailPerson: DetailPerson
                            },
                                function (reply) {
                                    UIControlService.unloadLoadingModal();
                                    if (reply.status === 200) {
                                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_BL");
                                        $uibModalInstance.close();

                                    }
                                    else {
                                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
                                        return;
                                    }
                                },
                                function (err) {
                                    UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                                    UIControlService.unloadLoadingModal();
                                }
                            );
                        }
                        for (var y = 0; y < data[i].length; y++) {
                            if (i == 0 && data[i][y].check == true) {
                                BlacklistService.insert({
                                    VendorID: VendorID,
                                    VendorStockID: null,
                                    EmployeeVendorID: data[i][y].EmployeeVendorID,
                                    Description: data[i][y].Description
                                },
                                      function (reply) {
                                          UIControlService.unloadLoadingModal();
                                          if (reply.status === 200) {
                                              $uibModalInstance.close();

                                          }
                                          else {
                                              UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
                                              return;
                                          }
                                      },
                                      function (err) {
                                          UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                                          UIControlService.unloadLoadingModal();
                                      }
                                  );
                            }
                            else if (i == 1 && data[i][y].check == true) {
                                BlacklistService.insert({
                                    VendorID: VendorID,
                                    VendorStockID: data[i][y].StockID,
                                    EmployeeVendorID: null,
                                    Description: data[i][y].Description
                                },
                                      function (reply) {
                                          UIControlService.unloadLoadingModal();
                                          if (reply.status === 200) {
                                              UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_BLS");
                                              $uibModalInstance.close();

                                          }
                                          else {
                                              UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
                                              return;
                                          }
                                      },
                                      function (err) {
                                          UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                                          UIControlService.unloadLoadingModal();
                                      }
                                  );
                            }
                        }
                        
                    }
                }
                else {
                    console.info("sorry");
                }
            });
        }

        vm.addOpsi = addOpsi;
        function addOpsi() {
            if (vm.DetailPerson == "") {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API")
                return;
            }
            var data = {
                KTPNumber: vm.NoKTP,
                PersonName: vm.DetailPerson,
                BirthDate: vm.BirthDate
            }
            vm.listperson.push(data);
            vm.DetailPerson = "";
            vm.NoKTP = "";
            vm.BirthDate = "";
        }

        vm.deleteRow = deleteRow;
        function deleteRow(index) {
            var idx = index - 1;
            var _length = vm.listperson.length; // panjangSemula
            vm.listperson.splice(idx, 1);
        };


        vm.openmodal = openmodal;
        function openmodal(VendorID, BlacklistDescription, DocUrl, DetailPerson) {
            vm.listStock = [];
            vm.listCompPers = [];
            vm.listDetailPers = [];
            for (var i = 0; i < vm.vendorstock.length; i++) {
                if (vm.vendorstock[i].check !== undefined) {
                    if (vm.vendorstock[i].check === true) {
                        var data = {
                            StockID: vm.vendorstock[i].StockID,
                            Description: vm.vendorstock[i].Description
                        }
                        vm.listStock.push(data);
                    }
                }
            }
            for (var i = 0; i < vm.employee.length; i++) {
                if (vm.employee[i].check !== undefined) {
                    if (vm.employee[i].check === true) {
                        var data = {
                            ID: vm.employee[i].ID,
                            Description: vm.employee[i].Description
                        }
                        vm.listCompPers.push(data);
                    }
                }
            }
            for (var i = 0; i < DetailPerson.length; i++) {
                var pers = {
                    BirthDate:UIControlService.getStrDate(DetailPerson[i].BirthDate),
                    PersonName:DetailPerson[i].PersonName,
                    KTPNumber:DetailPerson[i].KTPNumber 
                }
                vm.listDetailPers.push(pers);
            }
            if (vm.BlacklistID == 0 || ( vm.BlacklistID != 0 && vm.data.ApprovalStatus.Name == "APPROVAL_VENDOR_REJECTED")  ) {
                var data = {
                    act: true,
                    vendorstock: vm.listStock,
                    employee: vm.listCompPers,
                    VendorID: VendorID,
                    BlacklistDescription: vm.Description,
                    DocUrl: DocUrl,
                    DetailPerson: vm.listDetailPers,
                    ReferenceNo: vm.ReferenceNo,
                    MasaBlacklistID: vm.selectedMasaBlacklist.RefID,
                    StartDate: UIControlService.getStrDate(vm.StartDate),
                    EndDate: UIControlService.getStrDate(vm.EndDate)
                }
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-rekanan/blacklist/formcancelblacklist.html',
                    controller: 'FormCancelBlacklistCtrl',
                    controllerAs: 'frmCancelBlacklistCtrl',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                modalInstance.result.then(function (flag) {
                    if (flag === true) {
                        $uibModalInstance.close();
                    }

                });
            }
            else {
                BlacklistService.UpdateBlacklist({
                    BlacklistID: vm.BlacklistID,
                    ReferenceNo: vm.ReferenceNo,
                    BlacklistType: {
                        Name: vm.BlacklistType,
                    },
                    BlacklistDescription: vm.BlacklistDescription,
                    DocUrl: vm.BlacklistType === "BLACKLIST_TYPE_YES" ? vm.DocUrl : vm.DocUrlWhitelist,
                    VendorStock: vm.listStock,
                    CompPers: vm.listCompPers,
                    DetailPerson: vm.listDetailPers,
                    BlacklistDescription : vm.Description
                },
                    function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_DATA");
                        $uibModalInstance.close(true);
                    }
                },
                    function (err) {
                        UIControlService.unloadLoadingModal();
                    }
                );
                             
            }
        }

        vm.addWhite = addWhite;
        function addWhite() {
            var data = {
                act: true,
                type: "BLACKLIST_TYPE_NO",
                item: vm.data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-rekanan/blacklist/FormBlacklist.html',
                controller: 'FormBlacklistCtrl',
                controllerAs: 'frmBlacklistCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                
            });
        }

        vm.getMasaBlacklist = getMasaBlacklist;
        function getMasaBlacklist() {
            BlacklistService.GetBlacklist(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listMasaBlacklist = reply.data;
                    if (vm.data.MasaBlacklist !== null) {
                        vm.listMasaBlacklist.forEach(function (data) {
                            if (data.RefID == vm.data.MasaBlacklistID) {
                                vm.selectedMasaBlacklist = data;
                            }
                        });
                    }
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
    }
})();