﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailAppWLApprovalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'WarningLetterService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, WarningLetterService) {

        var vm = this;
        
        vm.WLID = item.WLID;
        vm.flag = item.flag;
        vm.Status = item.Status;
        vm.crApps = [];
        vm.employeeFullName = "";
        vm.employeeID = 0;
        vm.information = "";
        vm.flagEmp = item.flag;
        vm.flagActive = true;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-warningLetter');           
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            vm.crApps = [];
            WarningLetterService.getApproval({
                WarningLetterID: vm.WLID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    
                    vm.list = reply.data;
                    for (var i = 0; i < vm.list.length; i++) {
                        vm.crApps.push({
                            IsActive: vm.list[i].IsActive,
                            ID: vm.list[i].ID,
                            EmployeeID: vm.list[i].EmployeeIDApp,
                            ApprovalDate: UIControlService.convertDateTime(vm.list[i].ApprovalDate),
                            ApprovalStatus: vm.list[i].AppStatusName,
                            Remark: vm.list[i].Remark,
                            EmployeeFullName: vm.list[i].MstEmployee.FullName + ' ' + vm.list[i].MstEmployee.SurName,
                            EmployeePositionName: vm.list[i].MstEmployee.PositionName,
                            EmployeeDepartmentName: vm.list[i].MstEmployee.DepartmentName,
                            IsHighPriority: vm.list[i].IsHighPriority
                        });
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_APP'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GET_APP'));
                UIControlService.unloadLoading();
            });
        }
           
        // Reject Process
        vm.stateRejected = stateRejected;
        function stateRejected(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REJECTED'), function (yes) {
                if (yes) {
                    WarningLetterService.stateRejected({
                        LetterID: vm.WLID,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            //  vm.jLoad(1);
                            $uibModalInstance.close();
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_REJECT'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_REJECT'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_REJECT'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
           
        vm.stateApprove = stateApprove;
        function stateApprove() {
            switch (item.approvalType) {
                case "L1SUPP": stateApproveByL1Support(); break;
                case "PM": stateApproveByPM(); break;
                case "L3": stateApproveByL3(); break;
            };
        }

        // Approve by L1 Procurement Support
        function stateApproveByL1Support() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L1_SUPP'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL1Support({
                        LetterID: vm.WLID,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            $uibModalInstance.close();
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        // Approve by Approval PM
        function stateApproveByPM() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApproved({
                        LetterID: vm.WLID,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            $uibModalInstance.close();
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        // Approve by L3 Dir. Of SCM
        function stateApproveByL3() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_L3'), function (yes) {
                if (yes) {
                    WarningLetterService.stateApprovedByL3({
                        LetterID: vm.WLID,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            $uibModalInstance.close();
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_APPROVE'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.close();
        };
    }
})();