﻿(function () {
	'use strict';

	angular.module("app").controller("GOEApprvCtrl", ctrlApproval);

	ctrlApproval.$inject = ['item', 'UIControlService', 'GoodOfferEntryService', '$uibModalInstance', '$translatePartialLoader'];
	function ctrlApproval(item, UIControlService, GOEService, $uibModalInstance, $translatePartialLoader) {
		var vm = this;
		vm.apprvs = [];
		vm.StepID = item;

		vm.getDetailApproval = getDetailApproval;
		function getDetailApproval() {
			$translatePartialLoader.addPart('pemasukkan-penawaran-barang');
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			GOEService.detailApproval({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.apprvs = reply.data;
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
				} else {
					$.growl.error({ message: "Get Approval Failed." });
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoadingModal();
			});
		}

		vm.resendToApproval = resendToApproval;
		function resendToApproval() {
			bootbox.confirm("Apakah anda yakin akan mengirimkan approval?", function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.SENDING');
					GOEService.resendToApproval({ ID: vm.StepID, StepID: vm.apprvs[0].Id }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'NOTIFICATION.SEND.SUCCESS.MESSAGE');
							$uibModalInstance.close();
							//init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.closeDetailApprv = closeDetailApprv;
		vm.cancel = closeDetailApprv;
		function closeDetailApprv() {
			$uibModalInstance.close();
		}
	}
})();