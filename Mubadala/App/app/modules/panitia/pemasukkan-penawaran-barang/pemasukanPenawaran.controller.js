(function () {
	'use strict';

	angular.module("app").controller("PemasukkanPenawaranBarangCtrl", ctrl);

	ctrl.$inject = ['$uibModal', 'GoodOfferEntryService', '$state', 'UIControlService', 'UploaderService', 'GlobalConstantService', '$stateParams', '$translatePartialLoader', '$filter'];

	function ctrl($uibModal, GOEService, $state, UIControlService, UploaderService, GlobalConstantService, $stateParams, $translatePartialLoader, $filter) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.TenderID = Number($stateParams.TenderID);
		vm.IDDoc = Number($stateParams.DocID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.habisTanggal = false;
		vm.showterendah = true;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.TenderName = null;
		vm.StartDate = null;
		vm.EndDate = null;
		vm.isCancelled = false;

		vm.init = init;
		function init() {
			//console.info("AA");
			loadDataPenawaran();
			$translatePartialLoader.addPart('pemasukkan-penawaran-barang');
			var test = $translatePartialLoader.getRegisteredParts();
		}

		function loadDataPenawaran() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			GOEService.getAll({ column: vm.IDStepTender, }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var jumlah = reply.data;
					vm.detail = reply.data;
					vm.totalItems = vm.detail.length;
					vm.GOEVendor = vm.detail;
					if (vm.detail.length === 0) {
						console.info("Masuk If");
						GOEService.GetStep({
							Status: vm.IDTender,
							FilterType: vm.ProcPackType,
							IntParam1: vm.TenderID
						}, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								//console.info("re:" + JSON.stringify(reply));
								var data = reply.data;
								vm.StartDate = data.StartDate;
								vm.EndDate = data.EndDate;
								vm.TenderName = data.tender.TenderName;
								vm.isCancelled = data.tender.IsCancelled;
							}
						}, function (err) {
							UIControlService.msg_growl("error", "MESSAGE.API");
							UIControlService.unloadLoading();
						});
					} else {
						console.info("Masuk else");
						vm.StartDate = vm.detail[0].StartDateTen;
						vm.EndDate = vm.detail[0].EndDateTen;
						vm.TenderName = vm.detail[0].TenderName;
					}

					GOEService.isNeedTenderStepApproval({ TenderStepID: vm.IDStepTender }, function (result) {
						vm.isNeedApproval = result.data;
						if (!vm.isNeedApproval) {
							GOEService.isApprovalSent({ TenderStepID: vm.IDStepTender }, function (result2) {
								vm.isApprovalSent = result2.data;
							});
						}
						//console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.prompt({
				title: $filter('translate')('MESSAGE.CONFIRM_APPROVAL'),
				buttons: {
					confirm: { label: "OK" },
					cancel: { label: "Cancel" }
				},
				callback: function (res) {
					if (res != null) {
						UIControlService.loadLoading('MESSAGE.SENDING');
						GOEService.sendToApproval({
							Summary: res,
							ID: vm.IDStepTender
						}, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
								init();
								//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							} else {
								$.growl.error({ message: "Send Approval Failed." });
								UIControlService.unloadLoading();
							}
						}, function (err) {
							$.growl.error({ message: "Gagal Akses API >" + err });
							UIControlService.unloadLoading();
						});
					}
				}
			});


			//bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVAL'), function (res) {
			//	if (res) {
			//		UIControlService.loadLoading('MESSAGE.SENDING');
			//		GOEService.sendToApproval({ ID: vm.IDStepTender }, function (reply) {
			//			UIControlService.unloadLoading();
			//			if (reply.status === 200) {
			//				UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
			//				init();
			//				//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
			//			} else {
			//				$.growl.error({ message: "Send Approval Failed." });
			//				UIControlService.unloadLoading();
			//			}
			//		}, function (err) {
			//			$.growl.error({ message: "Gagal Akses API >" + err });
			//			UIControlService.unloadLoading();
			//		});
			//	}
			//});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.IDStepTender;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pemasukkan-penawaran-barang/detailApproval.html',
				controller: "GOEApprvCtrl",
				controllerAs: "GOEApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('pemasukan-penawaran-barang-print', {
				TenderRefID: vm.IDTender,
				StepID: vm.IDStepTender,
				ProcPackType: vm.ProcPackType,
				TenderID: vm.TenderID
			});
		}

		vm.simpan = simpan;
		function simpan() {

		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', {
				TenderRefID: vm.IDTender,
				ProcPackType: vm.ProcPackType,
				TenderID: vm.TenderID
			});
		}

	}
})();
