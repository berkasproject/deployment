﻿(function () {
    'use strict';

    angular.module("app")
    .controller("evaluasiPenawaranBarangController", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranBarangService', 'DataPengadaanService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranBarangService, DataPengadaanService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.stepID = Number($stateParams.StepID);
        vm.tenderRefID = Number($stateParams.TenderRefID);
        vm.procPackType = Number($stateParams.ProcPackType);

        vm.evaluasi = {};
        vm.tenderStepData = {};

        vm.isProcess = false;
        vm.noEvaluation = true;
        vm.isItemize = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-penawaran-barang');

            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.tenderRefID,
                ProcPackageType: vm.procPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            EvaluasiPenawaranBarangService.isItemize({
                ID: vm.stepID
            }, function (reply) {
                vm.isItemize = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_TENDER_OPTION');
            });

            loadData();
        };

        function loadData() {
            UIControlService.loadLoading(loadmsg);
            DataPengadaanService.GetStepByID({
                ID: vm.stepID
            }, function (reply) {
                vm.tenderStepData = reply.data;
                vm.tenderID = vm.tenderStepData.TenderID;
                vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
                EvaluasiPenawaranBarangService.getByTenderStepData({
                    ID: vm.stepID,
                    TenderID: vm.tenderStepData.TenderID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    vm.evaluasi = reply.data;
                    vm.noEvaluation = !vm.evaluasi.GoodsOfferEvaluationVendors || vm.evaluasi.GoodsOfferEvaluationVendors.length === 0;
                    if (!vm.noEvaluation) {
                        vm.evaluasi.GoodsOfferEvaluationVendors.sort(function (a, b) {
                            return b.Score - a.Score;
                        });

                        /*
                        vm.evaluasi.GoodsOfferEvaluationVendors.forEach(function (oe) {
                            if (!oe.IsNeedDeliveryDateApproval) {
                                oe.ddApprovalStatus = "STATUS.NOT_NEEDED";
                            } else if (!oe.IsDeliveryDateApprovalSent) {
                                oe.ddApprovalStatus = "STATUS.NOT_SENT";
                                oe.allowSendDDApproval = true;
                            } else if (oe.DeliveryDateApprovalStatus === null) {
                                oe.ddApprovalStatus = "STATUS.ON_PROCESS";
                            } else if (oe.DeliveryDateApprovalStatus === true) {
                                oe.ddApprovalStatus = "STATUS.APPROVED";
                            } else if (oe.DeliveryDateApprovalStatus === false) {
                                oe.ddApprovalStatus = "STATUS.REJECT";
                                oe.allowSendDDApproval = true;
                            }
                        });
                        */
                    }
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.buatSummary = buatSummary;
        function buatSummary() {

            var evaluationVendor = [];
            if (vm.evaluasi.GoodsOfferEvaluationVendors) {
                vm.evaluasi.GoodsOfferEvaluationVendors.forEach(function (det) {
                    evaluationVendor.push({
                        GoodsOfferEntryID: det.GoodsOfferEntryID,
                        VendorName: det.VendorName,
                        Score: det.Score,
                    });
                });
            }
            var item = {
                evaluasiID: vm.evaluasi.ID,
                tenderRefID: vm.tenderRefID,
                procPackageType: vm.procPackType,
                tenderID: vm.tenderStepData.TenderID,
                evaluationVendor: evaluationVendor,
                tenderStepData: {
                    ID : vm.tenderStepData.ID,
                    Summary: vm.tenderStepData.Summary,
                    DocumentUrl: vm.tenderStepData.DocumentUrl,
                    DocumentDate: vm.tenderStepData.DocumentDate
                }
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang/evaluasiPenawaranBarang.modal.html',
                controller: 'evaluasiPenawaranBarangModalController',
                controllerAs: 'epbmCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                loadData();
            });
        }

        vm.lihatSummary = lihatSummary;
        function lihatSummary() {
            var item = {
                summary: vm.tenderStepData.Summary,
                documentUrl: vm.tenderStepData.DocumentUrl,
                documentDate: vm.tenderStepData.DocumentDate
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang/viewSummary.modal.html?v=1.000002',
                controller: 'summaryPenawaranController',
                controllerAs: 'summaryCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () { });
        };

        vm.lakukanEvaluasi = lakukanEvaluasi;
        function lakukanEvaluasi() {
            $state.transitionTo('detail-evaluasi-penawaran-barang',
                { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
        };

        vm.viewItemPRAward = viewItemPRAward;
        function viewItemPRAward(evaluationVendor) {
            var item = {
                rfqGoodsID: vm.tenderRefID,
                evaluationVendor: evaluationVendor,
                tenderName: vm.tenderStepData.tender.TenderName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang/viewItemPRAward.modal.html',
                controller: 'viewItemPRAwardCtrl',
                controllerAs: 'prAwardCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {});
        }

        vm.viewItemDeliveryDate = viewItemDeliveryDate;
        function viewItemDeliveryDate(evaluationVendor) {
            var item = {
                tenderID: vm.tenderID,
                vendorID:evaluationVendor.VendorID,
                vendorName: evaluationVendor.VendorName,
                tenderName: vm.tenderStepData.tender.TenderName,
                awardedOnly: true
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang/viewItemDeliveryDate.modal.html',
                controller: 'viewItemDeliveryDayeController',
                controllerAs: 'prDDCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () { });
        }


        vm.print = print;
        function print() {
            $state.transitionTo('evaluasi-penawaran-barang-print', { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
        }


        vm.detailApprovalDeliveryDate = detailApprovalDeliveryDate;
        function detailApprovalDeliveryDate(evaluationVendor) {
            var item = {
                TenderStepDataID: vm.stepID,
                isAllowedEdit: vm.isAllowedEdit,
                vendorId: evaluationVendor.VendorID,
                vendorName: evaluationVendor.VendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang/detailApprovalDeliveryDate.modal.html',
                controller: 'detailApprovalDeliveryDateCtrl',
                controllerAs: 'detAppGOEDDCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                init();
            });
        };

        vm.sendApproval = sendApproval;
        function sendApproval(evaluationVendor) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_DELIVERY_DATE_APPROVAL'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    EvaluasiPenawaranBarangService.insertDeliveryDateApproval({
                        TenderStepDataID: vm.stepID,
                        VendorID: evaluationVendor.VendorID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SEND_APPROVAL'));
                        init();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_APPROVAL'));
                    });
                }
            });
        }
    }
})();