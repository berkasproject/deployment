﻿(function () {
    'use strict';

    angular.module("app").controller("EvalVPCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$uibModal', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCriteriaSettings', 'VPEvaluationMthodService', 'UIControlService', 'VPCPRDataService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $uibModal, $filter, $translate, $translatePartialLoader, $location, SocketService, VPCriteriaSettings, VPEvaluationMthodService, UIControlService, VPCPRDataService) {
        var vm = this;
        var lang = $translate.use();

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.menuhome = 0;
        $scope.my_tree = {};
        vm.page_id = 135;
        vm.srcText = '';
        vm.Type = '';
        vm.isVhsCpr = 0;
        vm.VPEval = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('vpevaluation-method');
            vm.loadAwal();
            getRole();
        }

        vm.loadAwal = loadAwal
        function loadAwal() {
            vm.loadVPEvaluasi();
        };

        function getRole() {
            /*
            VPCPRDataService.cekRole(
            function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info("role: " + vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
            */

            vm.isProcSupp = false;
            VPEvaluationMthodService.isprocsupp(function (reply) {
                vm.isProcSupp = reply.data;
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            vm.isPM = false;
            VPEvaluationMthodService.ispm(function (reply) {
                vm.isPM = reply.data
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            vm.isAllowAdd = false;
            VPEvaluationMthodService.isallowadd(function (reply) {
                vm.isAllowAdd = reply.data
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
        }
        vm.lihatDetail = lihatDetail;
        function lihatDetail(metode_id) {
            var kirim = {
                metode_evaluasi_id: metode_id
            };
            $uibModal.open({
                templateUrl: 'app/modules/panitia/vpevaluation-method/ViewDetailEvaluasiVP.html',
                controller: 'veiwDetailEVP',
                controllerAs: 'veiwDetailEVP',
                resolve: {
                    item: function () {
                        return kirim;
                    }
                }
            });
        };

        vm.loadVPEvaluasi = loadVPEvaluasi
        function loadVPEvaluasi() {
            var offset = (vm.currentPage - 1) * vm.maxSize;
            VPEvaluationMthodService.selectVPEvaluasi({
                Keyword: vm.srcText,
                Offset: offset,
                Limit: vm.maxSize
            },
            function (reply) {
                if (reply.status === 200) {
                    vm.VPEval = reply.data.List;
                    vm.totalItems = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        function getType() {
            VPCriteriaSettings.getType(
                function (reply) {
                    if (reply.status === 200) {
                        vm.type = reply.data;
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
        };

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(metode) {
            var pesan = "";

            switch (lang) {
                case 'id': pesan = 'Anda yakin untuk ' + (metode.IsActive ? 'mengaktifkan' : 'menonaktifkan') + ' Metode Evaluasi : "' + metode.VPEvaluationMethodName + '"?'; break;
                default: pesan = 'Are you sure to ' + (metode.IsActive ? 'deactivate' : 'activate') + ' : "' + metode.VPEvaluationMethodName + '" Evaluation Method?'; break;
            }

            bootbox.confirm(pesan, function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    VPEvaluationMthodService.switchActive({
                        VPEvaluationMethodId: metode.VPEvaluationMethodId,
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "MESSAGE.SUCC_TOGGLE_ACTIVATION");
                            vm.loadVPEvaluasi();
                        } else {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_TOGGLE_ACTIVATION");
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_TOGGLE_ACTIVATION'));
                    });
                }
            })
        };

        vm.publish = publish;
        function publish(metode) {

            UIControlService.loadLoading("");
            VPEvaluationMthodService.isLevel3Complete({
                VPEvaluationMethodId: metode.VPEvaluationMethodId,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.data === true) {

                    var pesan = "";
                    switch (lang) {
                        case 'id': pesan = 'Apakah Anda yakin untuk menggunakan Metode Evaluasi "' + metode.VPEvaluationMethodName + '" ?'; break;
                        default: pesan = 'Are you sure to publish "' + metode.VPEvaluationMethodName + '" Evaluation Method ?'; break;
                    }

                    bootbox.confirm(pesan, function (yes) {
                        if (yes) {
                            UIControlService.loadLoading("");
                            VPEvaluationMthodService.publish({
                                VPEvaluationMethodId: metode.VPEvaluationMethodId,
                            }, function (reply) {
                                UIControlService.unloadLoading();
                                if (reply.status === 200) {
                                    UIControlService.msg_growl("notice", "MESSAGE.SUCC_PUBLISH");
                                    vm.loadVPEvaluasi();
                                } else {
                                    UIControlService.msg_growl("error", "MESSAGE.ERR_PUBLISH");
                                }
                            }, function (err) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_PUBLISH'));
                            });
                        }
                    })
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LEVEL3_INCOMPLETE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.ERR_CHECK_LEVEL3");
            });
        }


        vm.addEvaluasiVP = addEvaluasiVP;
        function addEvaluasiVP() {
            $state.transitionTo('add-evaluasi-vp', { id: 0, type: 0 });
        };

        vm.updateEvaluasiVP = updateEvaluasiVP;
        function updateEvaluasiVP(VPEvaluationMethodId, isVhsCpr) {
            $state.transitionTo('add-evaluasi-vp', {
                id: VPEvaluationMethodId,
                type: isVhsCpr
            });
        };

        vm.copyLevel2 = copyLevel2;
        function copyLevel2(metode) {
            var item = {
                originalMethodName : metode.VPEvaluationMethodName,
                originalMethodID: metode.VPEvaluationMethodId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpevaluation-method/CopyLevel2.modal.html',
                controller: 'CopyLevel2Ctrl',
                controllerAs: 'copyLv2Ctrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadVPEvaluasi();
            });
        }

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            vm.loadVPEvaluasi();
        };
    }
})();

