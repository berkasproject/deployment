(function () {
	'use strict';

	angular.module("app").controller("AdminLoginController", ctrl);

	ctrl.$inject = ['$window', '$location', '$stateParams', 'Idle', '$state', 'AuthService', 'UIControlService', '$scope', '$translatePartialLoader', 'SocketService','$rootScope'];

	/* @ngInject */
	function ctrl($window, $location, $stateParams, Idle, $state, AuthService, UIControlService, $scope, $translatePartialLoader, SocketService,$rootScope) {
		var vm = this;
		vm.returnToUrl = $stateParams.returnToURL;
		vm.username = "";
		vm.password = "";
		vm.passCaptcha = "";
		vm.tampung = [];
		vm.init = init;
		vm.captcha = captcha;
		vm.validCaptcha = validCaptcha;
		vm.login = login;
		// $event.keyCode == 13 && loginAdmCtrl.login()
		vm.submitCaptcha = submitCaptcha;

		function init() {
			vm.captcha();
			$translatePartialLoader.addPart('home');
		}

		function captcha() {
			var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				'1', '2', '3', '4', '5', '6', '7', '8', '9', '0');

			var i;
			var tCtx = document.getElementById('textCanvas').getContext('2d');
			var imageElem = document.getElementById('image');

			for (i = 0; i < 4; i++) {
				var a = alpha[Math.floor(Math.random() * alpha.length)];
				var b = alpha[Math.floor(Math.random() * alpha.length)];
				var c = alpha[Math.floor(Math.random() * alpha.length)];
				var d = alpha[Math.floor(Math.random() * alpha.length)];
			}

			var code = a + ' ' + b + ' ' + c + ' ' + d;

			tCtx.canvas.width = tCtx.measureText(code).width;
			tCtx.fillText(code, 0, 10);
			imageElem.src = tCtx.canvas.toDataURL();
			vm.passCaptcha = $.md5(removeSpaces(code));
		}

		function validCaptcha() {
			var string1 = vm.passCaptcha;
			var string2 = $.md5(removeSpaces(document.getElementById('txtInput').value));

			if (string1 === string2) {
				return true;
			} else {
			    UIControlService.msg_growl("error", "Ups, wrong captcha!");

				document.getElementById("txtInput").value = "";
				vm.captcha();
				return false;
			}
		}

		function removeSpaces(string) {
			return string.split(' ').join('');
		}

		function submitCaptcha(evt){
			let charCode = (evt.keyCode ? evt.keyCode : evt.which);
			if (charCode == 13) {
				login();
			}
		}

		function login() {
			if ($.trim(vm.username) == "" || $.trim(vm.password) == "") {
			    UIControlService.msg_growl("warning", "Masukkan username dan password!!");
				return;
			}

			if (validCaptcha()) {
				UIControlService.loadLoading('MESSAGE.LOADING');
				AuthService.login({ username: vm.username, password: vm.password }, function (reply) {
					if (reply.status === 200) {
						localStorage.removeItem('eProcValeToken');
						localStorage.removeItem('eProcValeRefreshToken');
						localStorage.setItem('eProcValeToken', reply.data.access_token);

						AuthService.getMenus(function (response) {
							var menuTemp = response.data;

							for (var i = 0; i < menuTemp.length; i++) {
								if (localStorage.getItem("currLang") == 'id') {
									menuTemp[i].Label = menuTemp[i].locale_id
									for (var j = 0; j < menuTemp[i].Children.length; j++) {
										menuTemp[i].Children[j].Label = menuTemp[i].Children[j].locale_id
										for (var k = 0; k < menuTemp[i].Children[j].Children.length; k++) {
											menuTemp[i].Children[j].Children[k].Label = menuTemp[i].Children[j].Children[k].locale_id
										}
									}
								} else {
									menuTemp[i].Label = menuTemp[i].locale_en
									for (var j = 0; j < menuTemp[i].Children.length; j++) {
										menuTemp[i].Children[j].Label = menuTemp[i].Children[j].locale_en
										for (var k = 0; k < menuTemp[i].Children[j].Children.length; k++) {
											menuTemp[i].Children[j].Children[k].Label = menuTemp[i].Children[j].Children[k].locale_en
										}
									}
								}
							}

							$scope.main.menus = $scope.main.mapMenu(menuTemp);
							$rootScope.menus = $scope.main.menus[1].submenu;

							localStorage.removeItem('eProcValeToken');
							localStorage.removeItem('eProcValeRefreshToken');
							localStorage.setItem('eProcValeToken', reply.data.access_token);
							localStorage.setItem('eProcValeRefreshToken', reply.data.refresh_token);
							localStorage.setItem('login', true);
							localStorage.setItem('sessEnd', new Date().setSeconds(new Date().getSeconds() + reply.data.expires_in));
							localStorage.setItem('roles', JSON.parse(reply.data.roles));
							localStorage.setItem('moduleLayer', 1);
							localStorage.setItem('username', JSON.parse(reply.data.username));
							
							AuthService.getUserLogin(function (reply2) {
							    UIControlService.unloadLoading();
							    if (reply2.status === 200) {
									AuthService.getRoleUserLogin({ Keyword: reply2.data.CurrentUsername }, function (reply3) {
										if (reply3.status === 200 && reply3.data.List.length > 0) {
										    var role = reply3.data.List[0].RoleName;
										    var roleData = reply3.data.List;
											$scope.main.currUser = $scope.main.getCurrUser();
											for (var i = 0; i < roleData.length; i++) {
											    if (roleData[i].RoleID == 1 || roleData[i].RoleID == 15 || roleData[i].RoleID == 16) {
											        $rootScope.RoleID = roleData[i].RoleID;
											        localStorage.setItem('RoleIDOnly', roleData[i].RoleID);

											    }
											}
											UIControlService.msg_growl("notice", 'NOTIFICATION.LOGIN.SUCCESS.MESSAGE', "NOTIFICATION.LOGIN.SUCCESS.TITLE");
											if (vm.returnToUrl != null && vm.returnToUrl != '') {
												vm.returnToUrl = vm.returnToUrl.replace(/_/g, '/');
												$location.path(vm.returnToUrl);
											} else if (role === 'APPLICATION.ROLE_VENDOR') {
												//$state.go('dashboard-vendor');
												$location.path('/dashboard-vendor');
											} else if (role === 'APPLICATION.ROLE_VENDOR_INTERNATIONAL') {
												$location.path('/dashboard-vendor');
											} else if (role === 'APPLICATION.ROLE_VENDOR_PREQUAL') {
											    $location.path('/dashboard-vendor-prakualifikasi');
											} else {
												$location.path('/homeadmin');
											}

											Idle.watch(); // ng-idle run
											SocketService.emit("KickClient", reply2.data.CurrentUsername);
										} else {
											UIControlService.msg_growl("error", "User Tidak Berhak Login");
											$state.go('home');
										}
									}, function (err1) {
										UIControlService.msg_growl("error", "MESSAGE.API");
									});
								}
							}, function (err) {
								UIControlService.msg_growl("error", "MESSAGE.API");
							});
						}, function (response) {
							if (!hideSideMenu()) {
								UIControlService.handleRequestError(response.Message);
								$location.path('/login');
							}
						});

						setTimeout(function () {
							$scope.main.jumlahRequestAktivasi = 0;
							$scope.main.jumlahRequestVerifikasi = 0;
							$scope.main.jumlahRequestUbahData = 0;
							$scope.main.jumlahSubmitUbahData = 0;

							if ($scope.main.isLoggedIn() === 'true' && ($scope.main.currRoles.indexOf('APPLICATION.ROLE_ADMINPROC') !== -1 || $scope.main.currRoles.indexOf('APPLICATION.ROLE_MGRPROC') !== -1)) {
								SocketService.emit("daftarRekanan");
								SocketService.emit("PermintaanUbahData");
								SocketService.emit("SubmitUbahData");
							}
						}, 2000);

						//$state.go('homeadmin');
					} else {
						UIControlService.msg_growl("error", "error");
						//alert('error');
					}
					//$rootScope.unloadLoading();
				}, function (error) {
				    //alert(error.data.error);
				    UIControlService.msg_growl("error", error.data.error);

					document.getElementById("txtInput").value = "";
					vm.captcha();
					UIControlService.unloadLoading();
					return false;
					//$.growl.error({ message: "Gagal Akses API >" + err });
					//$rootScope.unloadLoading();
				});

				//    console.log("session id: " + $cookieStore.get("sessId"));
				//    if ($cookieStore.get("sessId") != undefined && $cookieStore.get("sessId") != null) {
				//        $rootScope.getSession().then(function (result) {
				//            //console.log("get session: " + JSON.stringify(result));
				//            $rootScope.userSession = result.data.data;
				//            $rootScope.userLogin = $rootScope.userSession.username;
				//            $.growl.notice({ message: 'Login berhasil! Selamat datang, ' + $rootScope.userSession.session_data.nama_pegawai });
				//            $state.go('homeadmin');
				//            $rootScope.isLogged = true;
				//        });
				//    }
				//    $http.post($rootScope.url_api + "auth/login/admin", {
				//        username: $scope.username,
				//        password: $scope.password,
				//        api_key: $rootScope.api_key
				//    })
				//    .success(function (result) {
				//        //console.log("result: " + JSON.stringify(result));
				//        if (result.status === 200) {
				//            $rootScope.unloadLoading();
				//            var data = result.data;
				//            //console.log('data : ' + JSON.stringify(data));
				//            if (data.success) {
				//                $cookieStore.put("sessId", data.session_id);
				//                $http.post($rootScope.url_api + "auth/get_session", {
				//                    session_id: $cookieStore.get("sessId")
				//                })
				//                .success(function (result2) {
				//                    if (result2.status === 200) {
				//                        $rootScope.userSession = result2.data;
				//                        $.growl.notice({ message: 'Login berhasil! Selamat datang, ' + $rootScope.userSession.session_data.nama_pegawai });
				//                        $state.go('homeadmin');
				//                        $rootScope.isLogged = true;
				//                    }
				//                })
				//                .error(function (err) {
				//                    $.growl.error({ message: "Gagal Akses API >" + err });

				//                });
				//            } else {
				//                $.growl.error({ message: data.message });

				//            }
				//        } else {
				//            $rootScope.unloadLoading();
				//            $.growl.error({ message: result.message });
				//        }
				//    })
				//    .error(function (error) {
				//        $rootScope.unloadLoading();
				//        $.growl.error({ message: "Gagal Akses API >" + error });
				//    });
			} // end: if $scope.validCaptcha()
		}
	}
})();