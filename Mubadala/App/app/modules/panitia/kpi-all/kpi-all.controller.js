(function () {
	'use strict';

	angular.module("app").controller("KPIAllController", ctrl);

	ctrl.$inject = ['$http', '$translate', '$filter', '$translatePartialLoader', '$location', '$state', 'UIControlService', 'KPIAllService'];
	/* @ngInject */
	function ctrl($http, $translate, $filter, $translatePartialLoader, $location, $state, UIControlService, KPIAllService) {

		var vm = this;
		vm.init = init;
		vm.pageNumber = 1;
		vm.pageSize = 10;
		vm.count = 0;

		vm.pageNumberGoods = 1;
		vm.pageNumberVhs = 1;
		vm.pageSize = 10;
		vm.count = 0;

		vm.tenderTypes = [
            { Name: "Goods", Value: 0 },
            { Name: "Service", Value: 1 },
		    { Name: "VHS/FPA", Value:2}
		]
		vm.filterStatus = [
            { Name: "Cancel", Value: 0 },
            { Name: "In Progress", Value: 1 },
            { Name: "AR Approved", Value: 2 },
            { Name: "Finish", Value: 3 },
            { Name: $filter('translate')('All'), Value: 4 }
		]
		vm.tenderType = vm.tenderTypes[1];
		vm.status = vm.filterStatus[4];

		vm.costSaving = [];
		vm.costSavingGoods = [];
		vm.costSavingVhs = [];

		function init() {
		    $translatePartialLoader.addPart('kpi-contract');
		    if (vm.tenderType.Value == 1) {
		        loadCostSavingService();
		    }
		    else if (vm.tenderType.Value == 0) {
		        loadCostSavingGoods();
		    }
		    else if (vm.tenderType.Value == 2) {
		        loadCostSavingVhs();
		    }
		}

		vm.loadCostSavingService = loadCostSavingService;
		function loadCostSavingService() {
		    UIControlService.loadLoading("");
		    KPIAllService.CostSavingService({
		        Offset: (vm.pageNumber - 1) * vm.pageSize,
		        Limit: vm.pageSize,
                Status:vm.status.Value
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        vm.costSaving = reply.data.List;
		        vm.count = reply.data.Count;
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_COST_SAVING");
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadCostSavingGoods = loadCostSavingGoods;
		function loadCostSavingGoods() {
		    UIControlService.loadLoading("");
		    KPIAllService.CostSavingGoods({
		        Offset: (vm.pageNumberGoods - 1) * vm.pageSize,
		        Limit: vm.pageSize,
		        Status: vm.status.Value
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        vm.costSavingGoods = reply.data.List;
		        vm.count = reply.data.Count;
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_COST_SAVING");
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadCostSavingVhs = loadCostSavingVhs;
		function loadCostSavingVhs() {
		    UIControlService.loadLoading("");
		    KPIAllService.CostSavingVhs({
		        Offset: (vm.pageNumberVhs - 1) * vm.pageSize,
		        Limit: vm.pageSize,
		        Status: vm.status.Value
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        vm.costSavingVhs = reply.data.List;
		        vm.count = reply.data.Count;
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_COST_SAVING");
		        UIControlService.unloadLoading();
		    });
		}

		vm.exportCostSavingService = exportCostSavingService;
		function exportCostSavingService() {
		    UIControlService.loadLoading("");
		    KPIAllService.CostSavingService({
		        Offset: -1,
		        Limit: -1
		    }, function (reply) {
		        if (reply.status === 200) {
		            var dataCS = reply.data.List;
		            var toExport = [];
		            var index = 1;

		            dataCS.forEach(function (cs) {
		                toExport.push({
		                    No: index++,
		                    Tender_Type: cs.TenderType,
		                    SAP_No: cs.ContractNo,
		                    Tender_Code: cs.TenderCode,
		                    Tender_Name: cs.ContractDetailsInfo,
		                    Contractor: cs.VendorName,
		                    Budget_Request_USD: $filter("currency")(cs.BudgetedContractValueToUSD, ''),
		                    Historical_Price_USD: $filter("currency")(cs.HistoricalValueToUSD, ''),
		                    EPV_USD: $filter("currency")(cs.EPVValueToUSD, ''),
		                    Initial_Proposal_USD: $filter("currency")(cs.OEPriceToUSD, ''),
		                    Negotiation_1_USD: $filter("currency")(cs.Negotiation1ToUSD, ''),
		                    Negotiation_2_USD: $filter("currency")(cs.Negotiation2ToUSD, ''),
		                    Negotiation_3_USD: $filter("currency")(cs.Negotiation3ToUSD, ''),
		                    Awarded_USD: $filter("currency")(cs.FinalValueToUSD, ''),
		                    Contract_Issued: cs.EECCompletedDate ? $filter("date")(cs.EECCompletedDate, 'dd/MM/yyyy') : '',
		                    Contract_Start_Date: cs.ContractStartDate ? $filter("date")(cs.ContractStartDate, 'dd/MM/yyyy') : '',
		                    Contract_End_Date: cs.ContractEndDate ? $filter("date")(cs.ContractEndDate, 'dd/MM/yyyy') : '',
		                    Duration_Days: cs.ContractDurrationDays,
		                    Duration_Years: cs.ContractDurrationYear,
		                    Cost_Reduction_USD: $filter("currency")(cs.CostReduction, ''),
		                    Cost_Avoidance_USD: $filter("currency")(cs.CostAvoidance, ''),
		                    Cost_Saving_USD: $filter("currency")(cs.CostSaving, ''),
		                    Annualized_Saving_USD: $filter("currency")(cs.AnnualizedSaving, ''),
		                    Contract_Engineer: cs.CEName,
		                });
		                UIControlService.loadLoading("");
		            });
		            JSONToCSVConvertor(toExport, true, "KPI Cost Savings");
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_COST_SAVING");
		        UIControlService.unloadLoading();
		    });
		}

		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ShowLabel, fileName) {
		    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		    var arrData = JSONData;
		    //console.info(arrData[0]);
		    var CSV = '';

		    //This condition will generate the Label/Header
		    if (ShowLabel) {
		        var row = "sep=," + '\n';

		        //This loop will extract the label from 1st index of on array
		        for (var index in arrData[0]) {

		            //Now convert each value to string and comma-seprated
		            row += index + ',';
		        }

		        row = row.slice(0, -1);
		        //console.info(row);
		        //append Label row with line break
		        CSV += row + '\r\n';
		        //console.info(CSV);
		    }

		    //1st loop is to extract each row
		    for (var i = 0; i < arrData.length; i++) {
		        var row = "";

		        //2nd loop will extract each column and convert it in string comma-seprated
		        for (var index in arrData[i]) {
		            row += '"' + arrData[i][index] + '",';
		        }

		        row.slice(0, row.length - 1);

		        //add a line break after each row
		        CSV += row + '\r\n';
		    }

		    if (CSV == '') {
		        alert("Invalid data");
		        return;
		    }

		    //Generate a file name
		    //var fileName = "KPI Contract Service";
		    var fileName = fileName;

		    //Initialize file format you want csv or xls
		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

		    // Now the little tricky part.
		    // you can use either>> window.open(uri);
		    // but this will not work in some browsers
		    // or you will not get the correct file extension    

		    //this trick will generate a temp <a /> tag
		    var link = document.createElement("a");
		    link.href = uri;

		    //set the visibility hidden so it will not effect on your web-layout
		    link.style = "visibility:hidden";
		    link.download = fileName + ".csv";

		    //this part will append the anchor tag and remove it after automatic click
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
		}
	}
})();
