﻿(function () {
    'use strict';

    angular.module("app").controller("VHSNegotiationAppCtrl", ctrl);

    ctrl.$inject = ['$filter', 'Excel', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiVHSService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, Excel, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiVHSService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        var page_id = 141;
        vm.evalsafety = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.kata = new Kata("");
        vm.init = init;
        vm.exportHref;
        vm.detail = [];
        vm.jLoad = jLoad;
        vm.isCalendarOpened = [false, false, false, false];

        function init() {
            $translatePartialLoader.addPart('negosiasi');
            UIControlService.loadLoading("MESSAGE.LOADING");
            jLoad(1);
        }
        vm.jLoad = jLoad;
        function jLoad(current) {
            NegosiasiVHSService.GetNegoToApproval({}, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.detail = reply.data;
                    vm.count = reply.data.Count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.detailApproval = detailApproval;
        function detailApproval(data) {
            var item = {
                ID: data
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-vhs-approval/detailApproval.modal.html',
                controller: 'detailApprovalNegoVHSCtrl',
                controllerAs: 'detailApprovalNegoVHSCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.save = save;
        function save(flag, data) {
            vm.dtnego = data;
            if (flag == true) {
                bootbox.confirm($filter('translate')('MESSAGE.YAKIN_APP'), function (yes) {
                    if (yes) {
                        data.ApprovalStatus = flag;
                        UIControlService.loadLoadingModal("MESSAGE.LOADING");
                        NegosiasiVHSService.updateApproval(data,
                        function (reply) {
                            if (reply.status === 200) {
                                UIControlService.unloadLoading();
                                if (vm.dtnego.nego.IsNego == false) {
                                    sendMailNego();
                                    init();
                                }
                                else {
                                    sendMailToCE();
                                    init();
                                }
                            }
                        },
                        function (err) {
                            
                        }
                    );
                    }
                    else data.ApprovalStatus = null;
                });
            }
        }

        vm.sendMailNego = sendMailNego;
        function sendMailNego() {
            console.info(vm.dtnego);
            var datainsert = {
                ID: vm.dtnego.VHSNegoId
            }
            NegosiasiVHSService.sendMail(datainsert,
            function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.MAIL_SENT");
                    sendMailToCE();

                }
            },
            function (err) {
                sendMailToCE();
            }
        );

        }

        vm.sendMailToCE = sendMailToCE;
        function sendMailToCE() {
            var datainsert = {
                VHSNegoId: vm.dtnego.VHSNegoId,
                ApprovalStatus: true
            }
            NegosiasiVHSService.sendMailToCE(datainsert,
            function (reply) {},function (err) {}
        );

        }

        vm.reject = reject;
        function reject(dt, flag) {
            console.info(dt);
            var item = {
                ID: dt,
                Status: 1,
                flagNego: flag
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-vhs-approval/detailApproval.modal.html',
                controller: 'detailApprovalNegoVHSCtrl',
                controllerAs: 'detailApprovalNegoVHSCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function () {
                init();
            });
        };

    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

