﻿(function () {
	'use strict';

	angular.module("app").controller("TenderStepController", ctrl);

	ctrl.$inject = ['UIControlService', '$uibModal', '$translatePartialLoader', 'TenderStepService'];

	function ctrl(UIControlService, $uibModal, $translatePartialLoader, TenderStepService) {
		var vm = this;

		vm.findby = null;
		vm.findValue = null;
		vm.allowAdd = true;
		vm.allowEdit = true;
		vm.allowDelete = true;
		vm.tahapans = [];

		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.searchBy = 0;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('tahapan-tender');
			loadSteps(1);
		}

		vm.loadSteps = loadSteps;
		function loadSteps() {
			UIControlService.loadLoading('MESSAGE.LOADING');
			TenderStepService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword,
				column: vm.searchBy
			}, function (reply) {
				if (reply.status === 200) {
					vm.tahapans = reply.data.List;
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.STEPS.ERROR', "NOTIFICATION.GET.STEPS.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.STEPS.ERROR', "NOTIFICATION.GET.STEPS.TITLE");
			});
		}

		vm.addstep = addStep;
		function addStep() {
			var modalInstance = $uibModal.open({
				templateUrl: 'addModalStep.html',
				controller: addStepCtrl,
				controllerAs: 'addStepCtrl'
			});

			modalInstance.result.then(function () {
				loadSteps();
			});
		}

		vm.editStep = editStep;
		function editStep(id) {
			UIControlService.loadLoading('MESSAGE.LOADING');
			TenderStepService.isInUse({ TenderStepID: id }, function (reply) {
				if (reply.status === 200) {
					if (reply.data === true) {
						UIControlService.msg_growl("error", 'MESSAGE.DATA_IS_IN_USE', "");
						return false;
					} else {
						var post = id;
						var modalInstance = $uibModal.open({
							templateUrl: 'editModalStep.html',
							controller: editStepCtrl,
							controllerAs: 'editStepCtrl',
							resolve: {
								item: function () {
									return post;
								}
							}
						});

						modalInstance.result.then(function () {
							loadSteps();
						});
					}
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", err, '');
			});
			UIControlService.unloadLoading();
		}

		vm.inactivate = inactivate;
		function inactivate(id) {
			UIControlService.loadLoading('MESSAGE.LOADING');
			TenderStepService.isInUse({ TenderStepID: id }, function (reply) {
				if (reply.status === 200) {
					if (reply.data === true) {
						UIControlService.msg_growl("error", 'MESSAGE.DATA_IS_IN_USE', "");
						return false;
					} else {
						var post = id;
						var modalInstance = $uibModal.open({
							templateUrl: 'inactivateTenderStep.html',
							controller: inactivateStepCtrl,
							controllerAs: 'inactivateStepCtrl',
							resolve: { item: function () { return post; } },
						});

						modalInstance.result.then(function () {
							loadSteps();
						});
					}
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", err, '');
			});
			UIControlService.unloadLoading();
		}

		vm.activate = activate;
		function activate(id) {
			UIControlService.loadLoading('MESSAGE.LOADING');
			TenderStepService.isInUse({ TenderStepID: id }, function (reply) {
				if (reply.status === 200) {
					var post = id;
					var modalInstance = $uibModal.open({
						templateUrl: 'activateTenderStep.html',
						controller: activateStepCtrl,
						controllerAs: 'activateStepCtrl',
						resolve: { item: function () { return post; } }
					});

					modalInstance.result.then(function () {
						loadSteps();
					});
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", err, '');
			});
			UIControlService.unloadLoading();
		}

		vm.viewDetail = viewDetail;
		function viewDetail(id) {
			var post = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'viewDetail.html',
				controller: viewDetailCtrl,
				controllerAs: 'viewDetailCtrl',
				resolve: { item: function () { return post; } }
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			loadSteps();
		}

		vm.viewStep = viewStep;
		function viewStep() { }
	}
})();

var addStepCtrl = function (UIControlService, $uibModalInstance, TenderStepService) {
	var vm = this;

	vm.goodsOrService;
	vm.selectedFormType;
	vm.stepName = '';
	vm.formTypes = [];
	vm.remark = '';

	vm.getFormTypes = getFormTypes;
	function getFormTypes() {
	    TenderStepService.getFormTypes({
	        GoodsOrService: vm.goodsOrService
	    }, function (reply) {
	        UIControlService.loadLoadingModal('MESSAGE.LOADING');
	        if (reply.status === 200) {
	            vm.formTypes = reply.data;
	            UIControlService.unloadLoadingModal();
	        } else {
	            UIControlService.unloadLoadingModal();
	            UIControlService.msg_growl("error", 'NOTIFICATION.GET.FORMTYPE.ERROR', "NOTIFICATION.GET.LOCATION.TITLE");
	        }
	    }, function (err) {
	        UIControlService.unloadLoadingModal();
	        UIControlService.msg_growl("error", 'NOTIFICATION.GET.LOCATION.ERROR', "NOTIFICATION.GET.LOCATION.TITLE");
	    });
	}

	vm.createTenderStep = createTenderStep;
	function createTenderStep() {
		if (vm.goodsOrService === null || vm.selectedFormType === null || vm.stepName.trim() === '') {
			return false;
		}
		UIControlService.loadLoadingModal('MESSAGE.LOADING');
		TenderStepService.create({
			FormType: vm.selectedFormType,
			GoodsOrService: vm.goodsOrService.toString(),
			TenderStepName: vm.stepName,
			Remark: vm.remark
		}, function (reply) {
			if (reply.status === 200) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", 'MESSAGE.SUC_SAVE', "MESSAGE.SUC_SAVE_TITLE");
				$uibModalInstance.close();
			} else {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE', "MESSAGE.ERR_SAVE_TITLE");
			}
		}, function (err) {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", err, '');
		});

		//$uibModalInstance.close();
	}

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.close();
	}
}

var editStepCtrl = function (UIControlService, item, $uibModalInstance, TenderStepService) {
	var vm = this;

	vm.goodsOrService;
	vm.selectedFormType;
	vm.stepName = '';
	vm.remark = '';
	vm.formTypes = [];

	vm.getFormTypes = getFormTypes;
	function getFormTypes() {
	    TenderStepService.getFormTypes({
	        GoodsOrService: vm.goodsOrService
	    }, function (reply) {
	        UIControlService.loadLoadingModal('LOADING.GET.PROCMETHOD');
	        if (reply.status === 200) {
	            vm.formTypes = reply.data;
	            UIControlService.unloadLoadingModal();
	        } else {
	            UIControlService.unloadLoadingModal();
	            UIControlService.msg_growl("error", 'NOTIFICATION.GET.PROCMETHOD.ERROR', "NOTIFICATION.GET.PROCMETHOD.TITLE");
	        }
	    }, function (err) {
	        UIControlService.unloadLoadingModal();
	        UIControlService.msg_growl("error", 'NOTIFICATION.GET.PROCMETHOD.ERROR', "NOTIFICATION.GET.PROCMETHOD.TITLE");
	    });
	}

	TenderStepService.getByID({ TenderStepID: item }, function (reply) {
		UIControlService.loadLoadingModal('LOADING.GET.PROCMETHOD');
		if (reply.status === 200) {
			vm.goodsOrService = reply.data.GoodsOrService.toString();
			vm.selectedFormType = reply.data.FormType;
			vm.stepName = reply.data.TenderStepName;
			vm.remark = reply.data.Remark;
			UIControlService.unloadLoadingModal();
			getFormTypes();
		} else {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", 'NOTIFICATION.GET.PROCMETHOD.ERROR', "NOTIFICATION.GET.PROCMETHOD.TITLE");
		}
	}, function (err) {
		UIControlService.unloadLoadingModal();
		UIControlService.msg_growl("error", 'NOTIFICATION.GET.PROCMETHOD.ERROR', "NOTIFICATION.GET.PROCMETHOD.TITLE");
	});

	vm.updateTenderStep = updateTenderStep;
	function updateTenderStep() {
		if (vm.goodsOrService === null || vm.selectedFormType === null || vm.stepName.trim() === '') {
			return false;
		}
		UIControlService.loadLoadingModal('MESSAGE.LOADING');
		TenderStepService.update({
			FormType: vm.selectedFormType,
			GoodsOrService: vm.goodsOrService,
			TenderStepName: vm.stepName,
			TenderStepID: item,
			Remark: vm.remark,
		}, function (reply) {
			if (reply.status === 200) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", 'MESSAGE.SUC_UPDATE', "MESSAGE.SUC_UPDATE_TITLE");
				$uibModalInstance.close();
			} else {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_EDIT', "MESSAGE.ERR_EDIT_TITLE");
			}
		}, function (err) {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", 'MESSAGE.ERR_EDIT', "MESSAGE.ERR_EDIT_TITLE");
		});
	}

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.close();
	}
}

var inactivateStepCtrl = function (UIControlService, item, $uibModalInstance, TenderStepService) {
	var vm = this;

	vm.inactivateTenderStep = inactivateTenderStep;
	function inactivateTenderStep() {
		UIControlService.loadLoadingModal('MESSAGE.INACTIVATE');
		TenderStepService.inactivate({ TenderStepID: item }, function (reply) {
			if (reply.status === 200) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", 'MESSAGE.SUC_INACTIVATE', "MESSAGE.SUC_INACTIVATE_TITLE");
				$uibModalInstance.close();
			} else {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_INACTIVATE', "MESSAGE.ERR_INACTIVATE_TITLE");
			}
		}, function (err) {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", 'MESSAGE.ERR_INACTIVATE', "MESSAGE.ERR_INACTIVATE_TITLE");
		});
	}

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.close();
	}
}

var activateStepCtrl = function (UIControlService, item, $uibModalInstance, TenderStepService) {
	var vm = this;

	vm.activateTenderStep = activateTenderStep;
	function activateTenderStep() {
		UIControlService.loadLoadingModal('MESSAGE.ACTIVATE');
		TenderStepService.activate({ TenderStepID: item }, function (reply) {
			if (reply.status === 200) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", 'MESSAGE.SUC_ACTIVATE', "MESSAGE.SUC_ACTIVATE_TITLE");
				$uibModalInstance.close();
			} else {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_ACTIVATE', "MESSAGE.ERR_ACTIVATE_TITLE");
			}
		}, function (err) {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", 'MESSAGE.ERR_ACTIVATE', "MESSAGE.ERR_ACTIVATE_TITLE");
		});
	}

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.close();
	}
}

var viewDetailCtrl = function (UIControlService, item, $uibModalInstance, TenderStepService) {
	var vm = this;

	vm.tenderStepName = '';
	vm.remark = '';

	UIControlService.loadLoadingModal('MESSAGE.LOADING');
	TenderStepService.getByID({ TenderStepID: item }, function (reply) {
		if (reply.status === 200) {
			vm.formTypeName = reply.data.FormTypeName;
			vm.tenderStepName = reply.data.TenderStepName;
			vm.remark = reply.data.Remark;
			UIControlService.unloadLoadingModal();
		} else {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", 'MESSAGE.VIEW_ERROR_MESSAGE', "MESSAGE.VIEW_ERROR_TITLE");
		}
	}, function (err) {
		UIControlService.unloadLoadingModal();
		UIControlService.msg_growl("error", 'MESSAGE.VIEW_ERROR_MESSAGE', "MESSAGE.VIEW_ERROR_TITLE");
	});

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.close();
	}
}