﻿(function () {
    'use strict';

    angular.module("app").controller("TenderStepDateApprovalController", ctrl);

    ctrl.$inject = ['$filter', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'TenderStepDateApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService,
        TenderStepDateApprovalService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.keyword = "";
        vm.list = [];
        vm.count = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-pengadaan');
            jLoad();
        }

        vm.onSearch = onSearch;
        function onSearch(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            jLoad();
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("");
            TenderStepDateApprovalService.select({
                Keyword: vm.keyword,
                Limit: vm.pageSize,
                Offset: (vm.currentPage - 1) * vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.list = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_GET_APP");
                UIControlService.unloadLoading();
            });
        }

        vm.approve = approve;
        function approve(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVE_DATE_APPROVAL'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    TenderStepDateApprovalService.approve({
                        Id: data.Id,
                        //Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APPROVE_DATE_APPROVAL'));
                        jLoad();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APPROVE_DATE_APPROVAL'));
                    });
                }
            });
        };

        vm.detailTahapan = detailTahapan;
        function detailTahapan(data) {
            var item = {
                Id : data.Id,
                TenderId : data.TenderId,
                TenderCode : data.TenderCode,
                TenderName : data.TenderName,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-tgl-tahapan/detailTahapan.modal.html',
                controller: 'viewDetailTahapanStepDateAppCtrl',
                controllerAs: 'viewDAppCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.reject = reject;
        function reject(data) {
            var item = {
                Id : data.Id,
                tenderCode : data.TenderCode,
                tenderName : data.TenderName,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-tgl-tahapan/confirmApproval.modal.html',
                controller: 'confirmStepDateApprovalCtrl',
                controllerAs: 'confSDAppCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function () {
                init();
            });
        };

    }
})();


