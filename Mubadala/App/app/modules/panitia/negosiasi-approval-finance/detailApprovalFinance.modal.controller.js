(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalFinanceNegoCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'NegotiationFinanceApprovalService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, NegotiationFinanceApprovalService) {

        var vm = this;
        var Id = item.Id;
        vm.tenderCode = item.tenderCode;
        vm.tenderName = item.tenderName;
        vm.vendorName = item.vendorName;
        vm.remark = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi');

        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reject = reject;
        function reject() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REJ'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    NegotiationFinanceApprovalService.reject({
                        Id: Id,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REJ_RES'));
                        $uibModalInstance.close();
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJ_RES'));
                    });
                }
            });
        }
    }
})();