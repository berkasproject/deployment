﻿(function () {
    'use strict';

    angular.module("app").controller("NegotiationApprovalFinanceController", ctrl);

    ctrl.$inject = ['$filter', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegotiationFinanceApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService,
        NegotiationFinanceApprovalService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.keyword = "";
        vm.list = [];
        vm.count = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi');
            jLoad();
        }

        vm.onSearch = onSearch;
        function onSearch(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            jLoad();
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("");
            NegotiationFinanceApprovalService.select({
                Keyword: vm.keyword,
                Limit: vm.pageSize,
                Offset: (vm.currentPage - 1) * vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.list = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_GET_APP");
                UIControlService.unloadLoading();
            });
        }

        vm.approve = approve;
        function approve(data) {
            bootbox.confirm($filter('translate')('MESSAGE.APP_RESULT'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    NegotiationFinanceApprovalService.approve({
                        Id: data.Id,
                        //Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APP_RES'));
                        jLoad();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APP_RES'));
                    });
                }
            });
        };

        vm.detailDraftApproval = detailDraftApproval;
        function detailDraftApproval(data) {
            var item = { contractRequisitionId: data.ContractRequisitionId };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-approval-finance/detailDraftApproval.modal.html',
                controller: 'viewDraftApprovalFinanceCtrl',
                controllerAs: 'viewDAppCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.reject = reject;
        function reject(data) {
            var item = {
                Id : data.Id,
                tenderCode : data.TenderCode,
                tenderName : data.TenderName,
                vendorName : data.VendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-approval-finance/detailApprovalFinance.modal.html',
                controller: 'detailApprovalFinanceNegoCtrl',
                controllerAs: 'detAppNegoFCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function () {
                init();
            });
        };

    }
})();


