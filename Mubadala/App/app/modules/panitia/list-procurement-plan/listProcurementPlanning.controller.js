﻿(function () {
    'use strict';

    angular.module("app").controller("listProcurementPlanningCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'ListProcurementPlanningService', '$filter', '$rootScope'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, ListProcurementPlanningService, $filter, $rootScope) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.materialService = "";
        vm.status = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('list-procurement-plan');
            UIControlService.loadLoading("Loading...");
            vm.jLoad(1);
        }


        vm.ubahMaterialService = ubahMaterialService;
        function ubahMaterialService(materialService) {
            vm.materialService = materialService;
            vm.jLoad(1);
        }

        vm.ubahStatus = ubahStatus;
        function ubahStatus(status) {
            console.info(status);
            vm.status = status;
            vm.jLoad(1);
        }

        vm.act = act;
        function act(action) {
            vm.materialService = "";
            vm.status = "";
            vm.keyword = "";
            vm.action = action;
            //if (vm.action == "") {
            vm.jLoad(1);
            //}
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            ListProcurementPlanningService.select({
                Keyword: vm.action,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword2: vm.keyword,
                IntParam1: Number(vm.materialService),
                IntParam2: Number(vm.status)
            }, function (reply) {
                vm.plans = reply.data.List;
                vm.totalItems = reply.data.Count;
                UIControlService.unloadLoading();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                //$rootScope.unloadLoading();
            });
        }

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            vm.jLoad(1);
        }

        vm.tambah = tambah;
        function tambah() {
            var post = {
                isAdd: 1
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/list-procurement-plan/formListProcurementPlanning.html',
                controller: 'formListProcurementPlanCtrl',
                controllerAs: 'formListProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.publish = publish;
        function publish(data) {
            var post = {
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/list-procurement-plan/publishModal.html',
                controller: 'publishListProcurementPlanCtrl',
                controllerAs: 'publishListProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.ubah = ubah;
        function ubah(data) {
            var post = {
                ubah: 1,
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/list-procurement-plan/formListProcurementPlanningUbah.html',
                controller: 'formUbahListProcurementPlanCtrl',
                controllerAs: 'formUbahListProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.lihat = lihat;
        function lihat(data) {
            var post = {
                ubah: 0,
                data: data,
                scm: 0
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/list-procurement-plan/viewListProcurementPlanning.html',
                controller: 'viewListProcurementPlanCtrl',
                controllerAs: 'viewListProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.hapus = hapus;
        function hapus(id) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    ListProcurementPlanningService.hapus({
                        ProcurementPlanID: id
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_DELETE');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
                    });
                }
            });
        }



    }
})();