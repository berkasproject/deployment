﻿(function () {
    'use strict';

    angular.module("app").controller("viewListProcurementPlanCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'ListProcurementPlanningService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, ListProcurementPlanningService, UIControlService, GlobalConstantService) {
        var vm = this;
        vm.detail = item.data;
        vm.ubah = item.ubah;
        vm.scm = item.scm;
        vm.isDept = 0;
        vm.isDataSCM = 0;
        vm.isDataMarket = 0;
        vm.isDataSub = 0;
        vm.isDataCat = 0;
        vm.isDataSourcing = 0;
        vm.newContractDuration = 0;
        vm.statName = vm.detail.StatName;
        vm.statusProc = vm.detail.StatusPlan != null ? vm.detail.StatusPlan.toString() : "";
        vm.skkMigasNumber = vm.detail.SKKMigasNumber != null ? vm.detail.SKKMigasNumber : "";
        vm.globalNumber = vm.detail.AbuDhabiNumber != null ? vm.detail.AbuDhabiNumber : "";
        vm.newContractExtensionsDuration = vm.detail.NewContractExtensionsOption != null ? vm.detail.NewContractExtensionsOption : 0;
        vm.newContractIncludeExtensionsDuration = 0;
        vm.deptName = vm.detail.DepartmentName;
        vm.deptID = vm.detail.DepartmentID;
        vm.marketSector = vm.detail.MarketSectorName != null ? vm.detail.MarketSectorName : "";
        vm.subSector = vm.detail.SubSectorName != null ? vm.detail.SubSectorName : "";
        vm.categoryName = vm.detail.CategoryName != null ? vm.detail.CategoryName : "";
        vm.techFocalPointName = vm.detail.TechFocalPointName != null ? vm.detail.TechFocalPointName : "";
        vm.techFocalPointID = vm.detail.TechFocalPoint != null ? vm.detail.TechFocalPoint : "";
        vm.description = vm.detail.Description != null ? vm.detail.Description : "";
        vm.localNumber = vm.detail.LocalNumber;
        vm.scmFocalPoint = vm.detail.SCMFocalPointName != null ? vm.detail.SCMFocalPointName : "";
        vm.scmFocalPointID = vm.detail.SCMFocalPoint != null ? vm.detail.SCMFocalPoint : "";
        vm.sourcingActivity = vm.detail.SourcingActivityName != null ? vm.detail.SourcingActivityName : "";
        vm.entity = vm.detail.EntityID != null ? vm.detail.EntityID.toString() : "";
        vm.entityName = vm.detail.EntityName != "" ? vm.detail.EntityName : "";
        vm.materialServiceName = vm.detail.MaterialServiceName != "" ? vm.detail.MaterialServiceName : "";
        vm.existingContractAward = vm.detail.ExistingContractAward ? vm.detail.ExistingContractAward.toString() : "";
        vm.existingContract = vm.detail.ExistingContract ? vm.detail.ExistingContract.toString() : "";
        vm.materialService = vm.detail.MaterialOrService ? vm.detail.MaterialOrService.toString() : "";
        vm.prNumber = vm.detail.PRNumber != null ? vm.detail.PRNumber : "";
        vm.CCStartDate = vm.detail.CurrentContractStartDate != null ? new Date(vm.detail.CurrentContractStartDate) : "-";
        vm.CCEndDate = vm.detail.CurrentContractEndDate != null ? new Date(vm.detail.CurrentContractEndDate) : "-";
        vm.newContractStartDate = vm.detail.NewContractStartDate != null ? new Date(vm.detail.NewContractStartDate) : "-";
        vm.newContractEndDate = vm.detail.NewContractEndDate != null ? new Date(vm.detail.NewContractEndDate) : "-";
        vm.sourcingDateStart = vm.detail.SourcingDateStart != null ? new Date(vm.detail.SourcingDateStart) : "-";
        vm.cgcApprovalDate = vm.detail.CGCApprovalDate != null ? new Date(vm.detail.CGCApprovalDate) : "-";
        vm.bidIssueDate = vm.detail.BidIssueDate != null ? new Date(vm.detail.BidIssueDate) : "-";
        vm.bidDuedDate = vm.detail.BidDueDate != null ? new Date(vm.detail.BidDueDate) : "-";
        vm.bidEvalCompletionDate = vm.detail.BidEvaluationCompletionDate != null ? new Date(vm.detail.BidEvaluationCompletionDate) : "-";
        vm.negotiationDate = vm.detail.NegotiationDate != null ? new Date(vm.detail.NegotiationDate) : "-";
        vm.awardRecomDate = vm.detail.AwardRecommendation != null ? new Date(vm.detail.AwardRecommendation) : "-";
        vm.excomBoardApproval = vm.detail.ExcomBoardApproval != null ? new Date(vm.detail.ExcomBoardApproval) : "-";
        vm.consessPartApproval = vm.detail.PartnersApproval != null ? new Date(vm.detail.PartnersApproval) : "-";
        vm.contractExecutionDate = vm.detail.ContractExecutionDate != null ? new Date(vm.detail.ContractExecutionDate) : "-";
        vm.comments = vm.detail.Comments != null ? vm.detail.Comments : "";
        vm.procPlanStatusName = vm.detail.ProcPlanStatusName != null ? vm.detail.ProcPlanStatusName : "";
        vm.procPlanStatus = vm.detail.ProcPlanStatus != null ? vm.detail.ProcPlanStatus : "";
        vm.scmStatusName = vm.detail.SCMStatusName != null ? vm.detail.SCMStatusName : "";
        vm.scmStatus = vm.detail.SCMStatus != null ? vm.detail.SCMStatus : "";
        vm.procurementPlanID = vm.detail.ProcurementPlanID;
        vm.contractTypeName = vm.detail.ContractTypeName != "" ? vm.detail.ContractTypeName : "";
        vm.existingContractName = vm.detail.ExistingContractName != "" ? vm.detail.ExistingContractName : "";
        vm.existingContractAwardName = vm.detail.ExistingContractAwardName != "" ? vm.detail.ExistingContractAwardName : "";

        vm.isCalendarOpened = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('scm-procurement-plan');
            //UIControlService.loadLoadingModal("Loading...");
            //vm.loadDept();
            var day = getDays(vm.newContractEndDate, vm.newContractStartDate);
            var data = day / 30;
            var toMonth = Math.round(data);
            vm.newContractDuration = toMonth;
            vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
        }

        vm.ubahVendor = ubahVendor;
        function ubahVendor(data) {
            vm.vendorID = data.VendorID;
        }

        vm.ubahJumlahSaham = ubahJumlahSaham;
        function ubahJumlahSaham(jumlah) {
            if (jumlah < 0 || jumlah > 100) {
                vm.jumlahSaham = 0;
            }
        }

        vm.UbahNewStartDate = UbahNewStartDate;
        function UbahNewStartDate(date) {

            if (date && vm.newContractEndDate) {
                if (date > vm.newContractEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWSTARTTOEND");
                    vm.newContractStartDate = "";
                    return;
                }
                var day = getDays(vm.newContractEndDate, date);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.UbahNewEndDate = UbahNewEndDate;
        function UbahNewEndDate(date) {
            if (date && vm.newContractStartDate) {
                if (date < vm.newContractStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWENDTOSTART");
                    vm.newContractEndDate = "";
                    return;
                }
                var day = getDays(date, vm.newContractStartDate);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.ubahCCStart = ubahCCStart;
        function ubahCCStart(date) {
            if (date && vm.CCEndDate) {
                if (date > vm.CCEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRSTARTTOEND");
                    vm.CCStartDate = "";
                    return;
                }
            }
        }

        vm.ubahCCEnd = ubahCCEnd;
        function ubahCCEnd(date) {
            if (date && vm.CCStartDate) {
                if (date < vm.CCStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRENDTOSTART");
                    vm.CCEndDate = "";
                    return;
                }
            }
        }

        function getDays(endDt, startDt) {

            var end = endDt;
            var start = startDt;

            var ONE_DAY = 1000 * 60 * 60 * 24;
            var differenceMs = Math.abs(end - start);
            return Math.round(differenceMs / ONE_DAY);
        }

        vm.ubahNewContractExt = ubahNewContractExt;
        function ubahNewContractExt(number) {
            vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(number);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();