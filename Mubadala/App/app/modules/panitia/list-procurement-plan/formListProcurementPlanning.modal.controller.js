﻿(function () {
    'use strict';

    angular.module("app").controller("formListProcurementPlanCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'ListProcurementPlanningService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, ListProcurementPlanningService, UIControlService, GlobalConstantService) {
        var vm = this;
        vm.detail = item.item;
        vm.isAdd = item.act;
        vm.isDept = false;
        vm.isDataSCM = 0;
        vm.isDataMarket = 0;
        vm.isDataSub = 0;
        vm.isDataCat = 0;
        vm.isDataSourcing = 0;
        vm.newContractDuration = 0;
        vm.newContractExtensionsDuration = 0;
        vm.newContractIncludeExtensionsDuration = 0;
        vm.deptName = "";
        vm.deptID = 0;
        vm.marketSector = "";
        vm.subSector = "";
        vm.categoryName = "";
        vm.techFocalPointName = "";
        vm.description = "";
        vm.localNumber = "";
        vm.scmFocalPoint = "";
        vm.scmFocalPointID = 0;
        vm.sourcingActivity = "";
        vm.entity = "";
        vm.existingContractAward = "";
        vm.existingContract = "";
        vm.materialService = "";
        vm.prNumber = "";
        vm.CCStartDate = "";
        vm.CCEndDate = "";
        vm.newContractStartDate = "";
        vm.newContractEndDate = "";
        vm.sourcingDateStart = "";
        vm.cgcApprovalDate = "";
        vm.bidIssueDate = "";
        vm.bidDuedDate = "";
        vm.bidEvalCompletionDate = "";
        vm.negotiationDate = "";
        vm.awardRecomDate = "";
        vm.excomBoardApproval = "";
        vm.consessPartApproval = "";
        vm.contractExecutionDate = "";
        vm.comments = "";
        vm.isCalendarOpened = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('list-procurement-plan');
            if (vm.isAdd === 1) {
                vm.action = "Tambah";
            } else {
                vm.action = "Ubah";
            }
            UIControlService.loadLoadingModal("Loading...");
            vm.loadDept();
        }
        vm.myConfig = {
            maxItems: 1,
            optgroupField: "DepartmentName",
            labelField: "DepartmentName",
            valueField: "DepartmentName",
            searchField: "DepartmentName",
            render: {
                optgroup_header: function (item, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.myConfig2 = {
            maxItems: 1,
            optgroupField: "FullName",
            labelField: "FullName",
            valueField: "FullName",
            searchField: "FullName",
            render: {
                optgroup_header: function (data, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(data.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.myConfig3 = {
            maxItems: 1,
            optgroupField: "CategoryName",
            labelField: "CategoryName",
            valueField: "CategoryName",
            searchField: "CategoryName",
            render: {
                optgroup_header: function (data, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(data.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.myConfig4 = {
            maxItems: 1,
            optgroupField: "MethodName",
            labelField: "MethodName",
            valueField: "MethodName",
            searchField: "MethodName",
            render: {
                optgroup_header: function (data, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(data.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        //vm.ubahDept = ubahDept;
        //function ubahDept($item, $model, $label) {
        //    console.info($item);
        //    if ($item) {
        //        vm.deptID = $item.DepartmentID;
        //        vm.deptName = $item.DepartmentName;
        //        vm.loadTechFocalPoint();
        //    } else {
        //        vm.isDept = false;
        //    }
        //}
        vm.ubahManualDept = ubahManualDept;
        function ubahManualDept(deptName) {
            vm.datadept.forEach(function (item) {
                if (deptName == item.DepartmentName) {
                    vm.deptID = item.DepartmentID;
                    vm.deptName = item.DepartmentName;
                } else if (deptName != item.DepartmentName) {
                    vm.isDept = false;
                    vm.techFocalPointName = "";
                }
            });
            if (deptName == "") {
                vm.isDept = false;
                vm.techFocalPointName = "";
            } else {
                vm.loadTechFocalPoint();
            }
        }

        vm.ubahSCM = ubahSCM;
        function ubahSCM(scmName) {
            vm.dataSCMFocalPoint.forEach(function (item) {
                if (scmName == item.FullName) {
                    vm.scmFocalPoint = item.FullName;
                    vm.scmFocalPointID = item.employee_id;
                }
            });
        }

        vm.ubahTech = ubahTech;
        function ubahTech(techName) {
            vm.dataTFP.forEach(function (item) {
                if (techName == item.FullName) {
                    vm.scmFocalPoint = item.FullName;
                    vm.techFocalPointID = item.employee_id;
                }
            });
        }

        vm.ubahMarket = ubahMarket;
        function ubahMarket(marketName) {
            vm.dataMarketSec.forEach(function (item) {
                if (marketName == item.CategoryName) {
                    vm.marketSector = item.CategoryName;
                    vm.marketSectorID = item.ID;
                }
            });
        }

        vm.ubahSub = ubahSub;
        function ubahSub(subName) {
            vm.dataSubSec.forEach(function (item) {
                if (subName == item.CategoryName) {
                    vm.subSector = item.CategoryName;
                    vm.subSectorID = item.ID;
                }
            });
        }

        vm.ubahCategory = ubahCategory;
        function ubahCategory(catName) {
            vm.dataCategory.forEach(function (item) {
                if (catName == item.CategoryName) {
                    vm.categoryName = item.CategoryName;
                    vm.categoryID = item.ID;
                }
            });
        }

        vm.ubahSourcing = ubahSourcing;
        function ubahSourcing(sourcingName) {
            vm.dataSourcingActivity.forEach(function (item) {
                if (sourcingName == item.MethodName) {
                    vm.sourcingActivity = item.MethodName;
                    vm.sourcingActivityID = item.MethodID;
                }
            });
        }

        //vm.ubahTech = ubahTech;
        //function ubahTech($item, $model, $label) {
        //    if ($item) {
        //        vm.techFocalPointName = $item.FullName;
        //        vm.techFocalPointID = $item.employee_id;
        //    }
        //}

        //vm.ubahSCM = ubahSCM;
        //function ubahSCM($item, $model, $label) {
        //    if ($item) {
        //        vm.scmFocalPoint = $item.FullName;
        //        vm.scmFocalPointID = $item.employee_id;
        //    }
        //}

        //vm.ubahMarket = ubahMarket;
        //function ubahMarket($item, $model, $label) {
        //    if ($item) {
        //        vm.marketSector = $item.CategoryName;
        //        vm.marketSectorID = $item.ID;
        //    }
        //}

        //vm.ubahSub = ubahSub;
        //function ubahSub($item, $model, $label) {
        //    if ($item) {
        //        vm.subSector = $item.CategoryName;
        //        vm.subSectorID = $item.ID;
        //    }
        //}

        //vm.ubahCategory = ubahCategory;
        //function ubahCategory($item, $model, $label) {
        //    if ($item) {
        //        vm.categoryName = $item.CategoryName;
        //        vm.categoryID = $item.ID;
        //    }
        //}

        //vm.ubahSourcing = ubahSourcing;
        //function ubahSourcing($item, $model, $label) {
        //    if ($item) {
        //        vm.sourcingActivity = $item.MethodName;
        //        vm.sourcingActivityID = $item.MethodID;
        //    }
        //}

        vm.loadDept = loadDept;
        function loadDept() {
            ListProcurementPlanningService.getDept(function (reply) {
                if (reply.status === 200) {
                    vm.datadept = reply.data;
                    if (vm.datadept) {
                        loadEntity();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadTechFocalPoint = loadTechFocalPoint;
        function loadTechFocalPoint() {
            ListProcurementPlanningService.getTFP({ Keyword: vm.deptName }, function (reply) {
                if (reply.status === 200) {
                    vm.dataTFP = reply.data.List;
                    if (vm.dataTFP.length > 0) {
                        vm.isDept = true;
                    } else {
                        vm.isDept = false;
                        vm.techFocalPointName = "";
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.ubahVendor = ubahVendor;
        function ubahVendor(data) {
            vm.vendorID = data.VendorID;
        }

        vm.ubahJumlahSaham = ubahJumlahSaham;
        function ubahJumlahSaham(jumlah) {
            if (jumlah < 0 || jumlah > 100) {
                vm.jumlahSaham = 0;
            }
        }

        vm.loadEntity = loadEntity;
        function loadEntity() {
            ListProcurementPlanningService.getEntity(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataEntity = reply.data;
                    if (vm.dataEntity) {
                        loadSubSector();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSubSector = loadSubSector;
        function loadSubSector() {
            ListProcurementPlanningService.getSubSector(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSubSec = reply.data;
                    if (vm.dataSubSec.length > 0) {
                        vm.isDataSub = 1;
                    }
                    if (vm.dataSubSec) {
                        loadProcPlanStatus();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadProcPlanStatus = loadProcPlanStatus;
        function loadProcPlanStatus() {
            ListProcurementPlanningService.getProcPlanStatus(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataProcPlanStatus = reply.data;
                    vm.ProcPlanStatusData = [];
                    vm.dataProcPlanStatus.forEach(function (item) {
                        if (item.Value == "Draft") {
                            vm.status = item.RefID;
                            var param = {
                                RefID: item.RefID,
                                Type: item.Type,
                                Value: item.Value
                            }
                            vm.ProcPlanStatusData.push(param);
                        }
                    });
                    if (vm.dataProcPlanStatus) {
                        loadMarketSector();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadMarketSector = loadMarketSector;
        function loadMarketSector() {
            ListProcurementPlanningService.getMarketSector(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataMarketSec = reply.data;
                    if (vm.dataMarketSec.length > 0) {
                        vm.isDataMarket = 1;
                    }
                    if (vm.dataMarketSec) {
                        loadCategory();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadCategory = loadCategory;
        function loadCategory() {
            ListProcurementPlanningService.getCategory(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataCategory = reply.data;
                    if (vm.dataCategory.length > 0) {
                        vm.isDataCat = 1;
                    }
                    if (vm.dataCategory) {
                        loadExistingContract();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadExistingContract = loadExistingContract;
        function loadExistingContract() {
            ListProcurementPlanningService.getExistingContract(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataExistingContract = reply.data;
                    if (vm.dataExistingContract) {
                        loadExistingContractAward();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadExistingContractAward = loadExistingContractAward;
        function loadExistingContractAward() {
            ListProcurementPlanningService.getExistingContractAward(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataExistingContractAward = reply.data;
                    if (vm.dataExistingContractAward) {
                        loadSCMFocalPoint();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSCMFocalPoint = loadSCMFocalPoint;
        function loadSCMFocalPoint() {
            ListProcurementPlanningService.getSCMFocalPoint(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSCMFocalPoint = reply.data.List;
                    if (vm.dataSCMFocalPoint.length > 0) {
                        vm.isDataSCM = 1;
                    }
                    if (vm.dataSCMFocalPoint) {
                        loadSourcingActivity();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSourcingActivity = loadSourcingActivity;
        function loadSourcingActivity() {
            ListProcurementPlanningService.getSourcingActivity(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSourcingActivity = reply.data;
                    if (vm.dataSourcingActivity.length > 0) {
                        vm.isDataSourcing = 1;
                    }
                    if (vm.dataSourcingActivity) {
                        loadContractType();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadContractType = loadContractType;
        function loadContractType() {
            ListProcurementPlanningService.getContractType(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataContractType = reply.data;
                    if (vm.dataContractType) {
                        loadMaterialService();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadMaterialService = loadMaterialService;
        function loadMaterialService() {
            ListProcurementPlanningService.getMaterialService(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataMaterialService = reply.data;
                    if (vm.dataMaterialService) {
                        UIControlService.unloadLoadingModal();
                    }

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.UbahNewStartDate = UbahNewStartDate;
        function UbahNewStartDate(date) {

            if (date && vm.newContractEndDate) {
                if (date > vm.newContractEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWSTARTTOEND");
                    vm.newContractStartDate = "";
                    return;
                }
                var day = getDays(vm.newContractEndDate, date);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.UbahNewEndDate = UbahNewEndDate;
        function UbahNewEndDate(date) {
            if (date && vm.newContractStartDate) {
                if (date < vm.newContractStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWENDTOSTART");
                    vm.newContractEndDate = "";
                    return;
                }
                var day = getDays(date, vm.newContractStartDate);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                console.info(toMonth);
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.ubahCCStart = ubahCCStart;
        function ubahCCStart(date) {
            if (date && vm.CCEndDate) {
                if (date > vm.CCEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRSTARTTOEND");
                    vm.CCStartDate = "";
                    return;
                }
            }
        }

        vm.ubahCCEnd = ubahCCEnd;
        function ubahCCEnd(date) {
            if (date && vm.CCStartDate) {
                if (date < vm.CCStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRENDTOSTART");
                    vm.CCEndDate = "";
                    return;
                }
            }
        }

        function getDays(endDt, startDt) {

            var end = endDt;
            var start = startDt;

            var ONE_DAY = 1000 * 60 * 60 * 24;
            var differenceMs = Math.abs(end - start);
            return Math.round(differenceMs / ONE_DAY);

        }

        vm.ubahNewContractExt = ubahNewContractExt;
        function ubahNewContractExt(number) {
            vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(number);
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal("Loading...");
            console.info(vm.scmFocalPoint);
            var noDept = 0, noSCM = 0, noTech = 0, noMarket = 0, noSub = 0, noCategory = 0, noSourcing = 0;
            vm.datadept.forEach(function (item) {
                if (vm.deptName == item.DepartmentName) {
                    noDept++;
                }
            });
            if (noDept == 0 && vm.deptName != "") {
                vm.deptName = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NODEPT");
                return;
            }

            vm.dataSCMFocalPoint.forEach(function (item) {
                if (vm.scmFocalPoint == item.FullName) {
                    noSCM++;
                }
            });
            if (noSCM == 0 && vm.scmFocalPoint != "") {
                vm.scmFocalPoint = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOSCM");
                return;
            }

            if (vm.dataTFP) {
                vm.dataTFP.forEach(function (item) {
                    if (vm.techFocalPointName == item.FullName) {
                        noTech++;
                    }
                });
                if (noTech == 0 && vm.techFocalPointName != "") {
                    vm.techFocalPointName = "";
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.NOTECH");
                    return;
                }
            }

            vm.dataMarketSec.forEach(function (item) {
                if (vm.marketSector == item.CategoryName) {
                    noMarket++;
                }
            });
            if (noMarket == 0 && vm.marketSector != "") {
                vm.marketSector = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOMARKET");
                return;
            }

            vm.dataSubSec.forEach(function (item) {
                if (vm.subSector == item.CategoryName) {
                    noSub++;
                }
            });
            if (noSub == 0 && vm.subSector != "") {
                vm.subSector = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOSUB");
                return;
            }

            vm.dataCategory.forEach(function (item) {
                if (vm.categoryName == item.CategoryName) {
                    noCategory++;
                }
            });
            if (noCategory == 0 && vm.categoryName != "") {
                vm.categoryName = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOCATEGORY");
                return;
            }

            vm.dataSourcingActivity.forEach(function (item) {
                if (vm.sourcingActivity == item.MethodName) {
                    noSourcing++;
                }
            });
            if (noSourcing == 0 && vm.sourcingActivity != "") {
                vm.sourcingActivity = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOSOURCING");
                return;
            }

            if (!(vm.deptID)) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.DEPT");
                return;
            }

            if (vm.description == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.DESC");
                return;
            }

            if (vm.entity == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ENTITY");
                return;
            }

            if (vm.materialService == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.MATERIALSERVICE");
                return;
            }

            if (vm.techFocalPointName == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.TECH");
                return;
            }

            if (vm.newContractStartDate == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NEWSTART");
                return;
            }

            if (vm.newContractEndDate == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NEWEND");
                return;
            }



            ListProcurementPlanningService.insert({
                DepartmentID: vm.deptID,
                Description: vm.description,
                EntityID: Number(vm.entity),
                MaterialOrService: Number(vm.materialService),
                Reference: vm.ppReference,
                CurrentSupplier: vm.currentSupplier,
                SCMFocalPoint: vm.scmFocalPointID,
                TechFocalPoint: vm.techFocalPointID,
                MarketSector: vm.marketSectorID,
                SubSector: vm.subSectorID,
                Category: vm.categoryID,
                SourcingActivity: vm.sourcingActivityID,
                ContractType: vm.contractType,
                ExistingContract: vm.existingContract,
                ExistingContractAward: vm.existingContractAward,
                PRNumber: vm.prNumber,
                CurrentContractStartDate: vm.CCStartDate,
                CurrentContractEndDate: vm.CCEndDate,
                NewContractStartDate: vm.newContractStartDate,
                NewContractEndDate: vm.newContractEndDate,
                NewContractExtensionsOption: vm.newContractExtensionsDuration,
                ProcPlanStatus: vm.status,
                SourcingDateStart: vm.sourcingDateStart,
                CGCApprovalDate: vm.cgcApprovalDate,
                BidIssueDate: vm.bidIssueDate,
                BidDueDate: vm.bidDuedDate,
                BidEvaluationCompletionDate: vm.bidEvalCompletionDate,
                NegotiationDate: vm.negotiationDate,
                AwardRecommendation: vm.awardRecomDate,
                ExcomBoardApproval: vm.excomBoardApproval,
                PartnersApproval: vm.consessPartApproval,
                ContractExecutionDate: vm.contractExecutionDate,
                Comments: vm.comments

            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS_SAVED");
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();