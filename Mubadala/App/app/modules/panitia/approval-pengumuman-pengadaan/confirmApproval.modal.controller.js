(function () {
    'use strict';

    angular.module("app")
    .controller("confirmAnnouncementApprovalCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'TenderAnnouncementApprovalService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, TenderAnnouncementApprovalService) {

        var vm = this;
        var Id = item.Id;
        vm.tenderCode = item.tenderCode;
        vm.tenderName = item.tenderName;
        vm.vendorName = item.vendorName;
        vm.remark = "";

        vm.init = init;
        function init() {

        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reject = reject;
        function reject() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REJECT_ANNOUNCEMENT'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    TenderAnnouncementApprovalService.reject({
                        Id: Id,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REJECT_ANNOUNCEMENT'));
                        $uibModalInstance.close();
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REJECT_ANNOUNCEMENT'));
                    });
                }
            });
        }
    }
})();