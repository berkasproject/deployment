(function () {
    'use strict';

    angular.module("app")
    .controller("viewDetailAnnouncementAppCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'TenderAnnouncementApprovalService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, TenderAnnouncementApprovalService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var tenderAnnouncementId = item.TenderAnnouncementId;

        vm.ann = {};
        vm.TenderName = item.TenderName;
        vm.TenderCode = item.TenderCode;

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal("");
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("");
            TenderAnnouncementApprovalService.getdetails({
                TenderAnnouncementId: tenderAnnouncementId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.ann = reply.data;

                    vm.ann.EmailsView = vm.ann.Emails.replace(/,/g, ', ');
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ANNOUNCEMENT'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ANNOUNCEMENT'));
            });
        }
               
        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();