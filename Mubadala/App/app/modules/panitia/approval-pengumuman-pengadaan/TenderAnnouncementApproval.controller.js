﻿(function () {
    'use strict';

    angular.module("app").controller("TenderAnnouncementApprovalController", ctrl);

    ctrl.$inject = ['$filter', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'TenderAnnouncementApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService,
        TenderAnnouncementApprovalService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.keyword = "";
        vm.list = [];
        vm.count = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-pengadaan');
            jLoad();
        }

        vm.onSearch = onSearch;
        function onSearch(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            jLoad();
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("");
            TenderAnnouncementApprovalService.select({
                Keyword: vm.keyword,
                Limit: vm.pageSize,
                Offset: (vm.currentPage - 1) * vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.list = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_GET_APP");
                UIControlService.unloadLoading();
            });
        }

        vm.approve = approve;
        function approve(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVE_ANNOUNCEMENT'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    TenderAnnouncementApprovalService.approve({
                        Id: data.Id,
                        //Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APPROVE_ANNOUNCEMENT'));
                        jLoad();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APPROVE_ANNOUNCEMENT'));
                    });
                }
            });
        };

        vm.detailPengumuman = detailPengumuman;
        function detailPengumuman(data) {
            var item = {
                Id : data.Id,
                TenderAnnouncementId: data.TenderAnnouncementId,
                TenderCode : data.TenderCode,
                TenderName : data.TenderName,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-pengumuman-pengadaan/detailAnnouncement.modal.html',
                controller: 'viewDetailAnnouncementAppCtrl',
                controllerAs: 'viewDAppCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.reject = reject;
        function reject(data) {
            var item = {
                Id : data.Id,
                tenderCode : data.TenderCode,
                tenderName : data.TenderName,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/approval-pengumuman-pengadaan/confirmApproval.modal.html',
                controller: 'confirmAnnouncementApprovalCtrl',
                controllerAs: 'confAnnAppCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function () {
                init();
            });
        };

    }
})();


