﻿(function () {
    'use strict';

    angular.module("app").controller("ContractAwardFormCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ContractSignOffService','UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, ContractSignOffService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.tender = {};

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.isCalendarOpened = [false, false, false, false];

        vm.isNeedContractAwardForm = true;

        vm.summaryNegotiation = "";

        function init() {
            $translatePartialLoader.addPart('contract-variation');
            cekContractVariation();
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            if (!vm.isTenderVerification) {
                if (index === 0 || index === 1 || vm.enableDate) {
                    vm.isCalendarOpened[index] = true;
                }
            }
        };

        vm.cekContractVariation = cekContractVariation;
        function cekContractVariation() {
            UIControlService.loadLoading("");
            var param = {
                TenderStepID: item.data.TenderStepID,
                VendorID: item.data.VendorID
            }
            ContractSignOffService.isContractVariation(param, function (reply) {
                if (reply.status === 200) {
                    vm.isContractVariation = reply.data;
                    //console.info("variasi?" + vm.isContractVariation);
                    if (vm.isContractVariation != true) {
                        contractAward();
                    }
                    else if (vm.isContractVariation == true) {
                        contractVariation();
                        isInternalOnly();
                    }
                    UIControlService.unloadLoading();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data pengecekan kontrak variasi" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        
        vm.contractAward = contractAward;
        function contractAward() {
            UIControlService.loadLoading("");
            var param = {
                TenderStepID: item.data.TenderStepID,
                VendorID : item.data.VendorID
            }
            ContractSignOffService.contractAwardForm(param, function (reply) {
                if (reply.status === 200) {
                    vm.caf = reply.data;
                    //console.info("exchangerate" + JSON.stringify(vm.caf));
                    if (vm.caf.TenderAnnouncement[0] != null) {
                        if (vm.caf.TenderAnnouncement[0].TenderAnnouncement.Emails == '') {
                            vm.caf.TenderAnnouncement[0].TenderAnnouncement.Emails = 'Tidak ada';
                        }
                    }
                    /*
                    if (vm.caf.Negotiation != null) {
                        for (var i = 0; i <= vm.caf.Negotiation.length - 1; i++) {
                            var isdeal = "";
                            if (vm.caf.Negotiation[i].IsDeal == 1) {
                                if (localStorage.getItem("currLang") == "ID" || localStorage.getItem("currLang") == "id") {
                                    isdeal = "Sepakat";
                                }
                                else {
                                    isdeal = "Dealed";
                                }
                            }
                            else if (vm.caf.Negotiation[i].IsDeal == 0 || vm.caf.Negotiation[i].IsDeal == null) {
                                if (localStorage.getItem("currLang") == "ID" || localStorage.getItem("currLang") == "id") {
                                    isdeal = "Tidak Sepakat";
                                }
                                else {
                                    isdeal = "Not Dealed";
                                }
                            }
                            vm.summaryNegotiation = vm.summaryNegotiation + vm.caf.Negotiation[i].VendorName + ": " + isdeal + "\n";
                        }
                    }*/
                    UIControlService.unloadLoading();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.contractVariation = contractVariation;
        function contractVariation() {
            UIControlService.loadLoading("");
            var param = {
                TenderStepID: item.data.TenderStepID,
                VendorID: item.data.VendorID
            }
            ContractSignOffService.contractVariationForm(param, function (reply) {
                if (reply.status === 200) {
                    vm.contractVariationForm = reply.data;
                    vm.contractVariationForm.DateRequested = UIControlService.getStrDate(vm.contractVariationForm.DateRequested);
                    vm.contractVariationForm.DateRequired = UIControlService.getStrDate(vm.contractVariationForm.DateRequired);
                    vm.contractVariationForm.OriginalStartDate = UIControlService.getStrDate(vm.contractVariationForm.OriginalStartDate);
                    vm.contractVariationForm.OriginalEndDate = UIControlService.getStrDate(vm.contractVariationForm.OriginalEndDate);
                    vm.contractVariationForm.CurrentEndDate = UIControlService.getStrDate(vm.contractVariationForm.CurrentEndDate);
                    vm.contractVariationForm.NewPlannedEndDate = UIControlService.getStrDate(vm.contractVariationForm.NewPlannedEndDate);
                    console.info("contractVariation" + JSON.stringify(vm.contractVariationForm));
                    UIControlService.unloadLoading();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.isInternalOnly = isInternalOnly;
        function isInternalOnly() {
            UIControlService.loadLoading("");
            var param = {
                TenderStepID: item.data.TenderStepID,
                VendorID: item.data.VendorID
            }
            ContractSignOffService.isInternalOnly(param, function (reply) {
                if (reply.status === 200) {
                    vm.isInternalOnly = reply.data;
                    console.info("isinternal" + vm.isInternalOnly);
                    if (vm.isInternalOnly == true) {
                        contractAward();
                    }
                    else {
                        vm.isNeedContractAwardForm = false;
                    }
                    UIControlService.unloadLoading();
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }
        

    }
})();
//TODO


