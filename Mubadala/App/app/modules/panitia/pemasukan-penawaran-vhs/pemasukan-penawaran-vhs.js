﻿(function () {
	'use strict';

	angular.module("app").controller("PPVHSCtrl", ctrl);

	ctrl.$inject = ['SocketService', 'PPVHSAdminService', 'UIControlService', '$state', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader'];

	function ctrl(SocketService, PPVHSAdminService, UIControlService, $state, $stateParams, $uibModal, $translate, $translatePartialLoader) {
		var vm = this;
		vm.data = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.jLoad = jLoad;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.VOFVendor = null;

		function init() {
			$translatePartialLoader.addPart('pemasukkan-penawaran-vhs');
			//UIControlService.loadLoading("Silahkan Tunggu...");
			//loadTemplate();
			//if (vm.ProcPackType === 3168) {
			//    loadDataTender();
			//    jLoad(1);
			// }
			jLoad(1);
		}
		/*
        function loadDataTender() {
            PPVHSAdminService.selectAll({
                ID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    console.info("data:" + JSON.stringify(reply));
                    vm.TenderName = data.tender.TenderName;
                    vm.StartDate = UIControlService.getStrDate(data.StartDate);
                    vm.EndDate = UIControlService.getStrDate(data.EndDate);
                    console.info("tender::" + JSON.stringify(vm.dataTenderReal));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }*/

		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.data = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			PPVHSAdminService.GetAll({
			    column: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.data = reply.data;
				    vm.VOFVendor = vm.data
					PPVHSAdminService.GetStep({
					    Status: vm.IDTender,
					    FilterType: vm.ProcPackType
					}, function (reply) {
					    UIControlService.unloadLoading();
					    if (reply.status === 200) {
					        var data = reply.data;
					        vm.StartDate = data.StartDate;
					        vm.EndDate = data.EndDate;
					        vm.TenderName = data.tender.TenderName;
					        vm.TenderID = data.TenderID;
					    }
					}, function (err) {
					    UIControlService.msg_growl("error", "MESSAGE.API");
					    UIControlService.unloadLoading();
					});

					PPVHSAdminService.isNeedTenderStepApproval({ ID: vm.IDStepTender }, function (result) {
						vm.isNeedApproval = result.data;
						if (!vm.isNeedApproval) {
							PPVHSAdminService.isApprovalSent({ TenderStepID: vm.IDStepTender }, function (result2) {
								vm.isApprovalSent = result2.data;
							});
						}
					    //console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.IDStepTender;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pemasukan-penawaran-vhs/detailApproval.html',
				controller: "VHSOEApprvCtrl",
				controllerAs: "VHSOEApprvCtrl",
				resolve: { item: function () { vm.StepID = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('pemasukan-penawaran-vhs-print', {
				TenderRefID: vm.IDTender,
				StepID: vm.IDStepTender,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			UIControlService.loadLoading('MESSAGE.SENDING');
			PPVHSAdminService.sendToApproval({ ID: vm.IDStepTender }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'MESSAGE.SEND_SUCCESS', "MESSAGE.SEND_SUCCESS_TITLE");
					init();
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
				} else {
					$.growl.error({ message: "Send Approval Failed." });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
		    $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, TenderID: vm.TenderID });
		}
	}
})();


