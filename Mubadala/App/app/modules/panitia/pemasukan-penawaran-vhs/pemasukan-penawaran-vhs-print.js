﻿(function () {
	'use strict';

	angular.module("app").controller("PPVHSPrintCtrl", ctrl);

	ctrl.$inject = ['$stateParams', '$translatePartialLoader', 'PPVHSAdminService', 'UIControlService'];
	/* @ngInject */
	function ctrl($stateParams, $translatePartialLoader, PPVHSAdminService, UIControlService) {
		var vm = this;
		vm.data = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.jLoad = jLoad;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.VOFVendor = null;
		vm.jumlahlembar = 1;

		function init() {
			$translatePartialLoader.addPart('pemasukkan-penawaran-vhs');
			jLoad(1);
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.data = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			PPVHSAdminService.GetAll({
				column: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.data = reply.data;
					vm.VOFVendor = vm.data
					PPVHSAdminService.GetStep({
						Status: vm.IDTender,
						FilterType: vm.ProcPackType
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							var data = reply.data;
							vm.StartDate = data.StartDate;
							vm.EndDate = data.EndDate;
							vm.TenderName = data.tender.TenderName;
							vm.TenderID = data.TenderID;
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API" + err });
				UIControlService.unloadLoading();
			});
		}
	}
})()