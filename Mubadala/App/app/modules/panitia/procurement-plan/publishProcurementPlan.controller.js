﻿(function () {
    'use strict';

    angular.module("app").controller("publishProcurementPlanCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'ProcurementPlanningService', '$filter', '$rootScope', '$stateParams', '$state', '$uibModalInstance', 'item'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, ProcurementPlanningService, $filter, $rootScope, $stateParams, $state, $uibModalInstance, item) {
        var vm = this;
        vm.catatan = "";
        vm.init = init;
        function init() {
            getContentEmail();
            UIControlService.loadLoadingModal("Loading...");
        }

        function getContentEmail() {
            ProcurementPlanningService.getContentEmail(function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.konten = reply.data[0];
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_PUBLISH");
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.ya = ya;
        function ya() {
            if (vm.catatan == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOTES");
                return;
            }
            UIControlService.loadLoadingModal("Loading...");
            ProcurementPlanningService.publish({
                ProcurementPlanID: item.data.ProcurementPlanID,
                Remarks: vm.catatan
            }, function (reply) {
                if (reply.status === 200) {
                    if (reply.data == false) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("warning", "MESSAGE.PUBLISHADMINLAIN");
                        $uibModalInstance.dismiss('dismiss');
                    } else {
                        sendEmail();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_PUBLISH");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        function sendEmail() {
            var content = vm.konten.EmailContent1.split('#split');
            if (localStorage.getItem("currLang").toLowerCase() == 'id') {
                content = content[0];
            }
            if (localStorage.getItem("currLang").toLowerCase() == 'en') {
                content = content[1];

            }
            ProcurementPlanningService.sendEmail({
                EmailContent: content,
                EmailSubject: vm.konten.EmailSubject,
                Description: item.data.Description,
                TechFocalPoint: item.data.TechFocalPoint
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_PUBLISH");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.tidak = tidak;
        function tidak() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();