﻿(function () {
    'use strict';

    angular.module("app").controller("tolakSetujuProcPlanCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$translate', '$translatePartialLoader', '$location',
        'ProcurementPlanningService', 'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$sce', '$stateParams', '$q'];

    function ctrl($state, $http, $translate, $translatePartialLoader, $location, ProcurementPlanningService, UIControlService, item, $uibModalInstance, GlobalConstantService, $sce, $stateParams, $q) {

        var vm = this;
        vm.setuju = item.setuju;
        vm.remark = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("scm-procurement-plan");
            getContentEmail();
        }

        function getContentEmail() {
            ProcurementPlanningService.getContentEmailSCM(function (reply) {
                if (reply.status === 200) {
                    vm.konten = reply.data[0];
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_PUBLISH");
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('dismiss');
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal('LOADING');
            if (vm.setuju == 1) {
                ProcurementPlanningService.setuju({
                    ProcurementPlanID: item.data.ProcurementPlanID,
                    Remarks: vm.remark
                }, function (reply2) {
                    if (reply2.status === 200) {
                        if (reply2.data == false) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("warning", "MESSAGE.PUBLISHADMINLAIN");
                            $uibModalInstance.dismiss('dismiss');
                        } else {
                            sendEmail();
                        }
                    } else {
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_AGREE');
                    }
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl('error', 'MESSAGE.FAIL_AGREE');
                });
            } else {
                ProcurementPlanningService.tolak({
                    ProcurementPlanID: item.data.ProcurementPlanID
                }, function (reply2) {
                    if (reply2.status === 200) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_DISAGREE');
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_DISAGREE');
                    }
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl('error', 'MESSAGE.FAIL_DISAGREE');
                });
            }
        }

        function sendEmail() {
            var content = vm.konten.EmailContent1.split('#split');
            if (localStorage.getItem("currLang").toLowerCase() == 'id') {
                content = content[0];
            }
            if (localStorage.getItem("currLang").toLowerCase() == 'en') {
                content = content[1];

            }
            ProcurementPlanningService.sendEmailSCM({
                EmailContent: content,
                EmailSubject: vm.konten.EmailSubject,
                Description: item.data.Description
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_PUBLISH");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

    }
})();