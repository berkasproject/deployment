﻿(function () {
    'use strict';

    angular.module("app").controller("procurementPlanningSCMCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'ProcurementPlanningService', '$filter', '$rootScope'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, ProcurementPlanningService, $filter, $rootScope) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.materialService = "";
        vm.status = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('scm-procurement-plan');
            UIControlService.loadLoading("Loading...");
            vm.jLoad(1);
        }


        vm.ubahMaterialService = ubahMaterialService;
        function ubahMaterialService(materialService) {
            vm.materialService = materialService;
            vm.jLoad(1);
        }

        vm.ubahStatus = ubahStatus;
        function ubahStatus(status) {
            vm.status = status;
            vm.jLoad(1);
        }

        vm.act = act;
        function act(action) {
            vm.materialService = "";
            vm.status = "";
            vm.keyword = "";
            vm.action = action;
            //if (vm.action == "") {
                vm.jLoad(1);
            //}
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            ProcurementPlanningService.selectSCM({
                Keyword: vm.action,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword2: vm.keyword,
                IntParam1: Number(vm.materialService),
                IntParam2: Number(vm.status)
            }, function (reply) {
                vm.plans = reply.data.List;
                vm.totalItems = reply.data.Count;
                if (vm.plans.length > 0) {
                    for (var s = 0; s < vm.plans.length; s++) {
                        if (vm.plans[s].SCMStatus == null && vm.plans[s].ProcPlanStatus == 4497) {
                            vm.plans[s].StatName = "Completed";
                        } else if (vm.plans[s].SCMStatus == 4498 && vm.plans[s].ProcPlanStatus == 4497) {
                            vm.plans[s].StatName = "Reviewed";
                        } else if (vm.plans[s].SCMStatus == 4523 && vm.plans[s].ProcPlanStatus == 4497) {
                            vm.plans[s].StatName = "Approval Review";
                        } else if (vm.plans[s].SCMStatus == 4524 && vm.plans[s].ProcPlanStatus == 4497) {
                            vm.plans[s].StatName = "Cancelled";
                        } else if (vm.plans[s].SCMStatus == 4499 && vm.plans[s].ProcPlanStatus == 4497) {
                            vm.plans[s].StatName = "Done";
                        }
                     }
                }
                UIControlService.unloadLoading();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                //$rootScope.unloadLoading();
            });
        }

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            vm.jLoad(1);
        }

        vm.tambah = tambah;
        function tambah() {
            var post = {
                isAdd: 1
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/formProcurementPlan.html',
                controller: 'formProcurementPlanCtrl',
                controllerAs: 'formProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.detailApproval = detailApproval;
        function detailApproval(data) {
            var post = {
                item: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/detailApprovalProcPlan.html',
                controller: 'DetailApprovalProcurementPlanCtrl',
                controllerAs: 'DetailApprovalProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.publish = publish;
        function publish(data) {
            var post = {
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/publishProcurementPlan.html',
                controller: 'publishProcurementPlanCtrl',
                controllerAs: 'publishProcurementPlanCtrl',
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.ubah = ubah;
        function ubah(data) {
            var post = {
                ubah: 1,
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/formUbahProcurementPlanSCM.html',
                controller: 'formUbahProcurementPlanSCMCtrl',
                controllerAs: 'formUbahProcurementPlanSCMCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.lihat = lihat;
        function lihat(data) {
            var post = {
                ubah: 0,
                data: data,
                scm: 1
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/viewProcurementPlan.html',
                controller: 'viewProcurementPlanCtrl',
                controllerAs: 'viewProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.setuju = setuju;
        function setuju(data) {
            var post = {
                setuju: 1,
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/tolakSetujuProcPlan.html',
                controller: 'tolakSetujuProcPlanCtrl',
                controllerAs: 'tolakSetujuProcPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.tolak = tolak;
        function tolak(data) {
            var post = {
                setuju: 0,
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/tolakSetujuProcPlan.html',
                controller: 'tolakSetujuProcPlanCtrl',
                controllerAs: 'tolakSetujuProcPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.aktif = aktif;
        function aktif(id) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_ACTIVE') + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    ProcurementPlanningService.aktif({
                        ProcurementPlanID: id
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_ACTIVE');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_ACTIVE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_ACTIVE');
                    });
                }
            });
        }

        vm.nonAktif = nonAktif;
        function nonAktif(id) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_INACTIVE') + '<h3>', function (reply) {
                if (reply) {
                    ProcurementPlanningService.nonAktif({
                        ProcurementPlanID: id
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_INACTIVE');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_INACTIVE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_INACTIVE');
                    });
                }
            });
        }


    }
})();