﻿(function () {
    'use strict';

    angular.module("app").controller("formUbahProcurementPlanSCMCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'ProcurementPlanningService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, ProcurementPlanningService, UIControlService, GlobalConstantService) {
        var vm = this;
        vm.detail = item.data;
        vm.ubah = item.ubah;
        vm.isDept = 0;
        vm.isDataSCM = 0;
        vm.isDataMarket = 0;
        vm.isDataSub = 0;
        vm.isDataCat = 0;
        vm.isDataSourcing = 0;
        vm.newContractDuration = 0;
        vm.statName = vm.detail.StatName;
        vm.statusProc = vm.detail.StatusPlan != null ? vm.detail.StatusPlan.toString() : "";
        vm.skkMigasNumber = vm.detail.SKKMigasNumber != null ? vm.detail.SKKMigasNumber : "";
        vm.globalNumber = vm.detail.AbuDhabiNumber != null ? vm.detail.AbuDhabiNumber : "";
        vm.newContractExtensionsDuration = vm.detail.NewContractExtensionsOption != null ? vm.detail.NewContractExtensionsOption : 0;
        vm.newContractIncludeExtensionsDuration = 0;
        vm.deptName = vm.detail.DepartmentName;
        vm.deptID = vm.detail.DepartmentID;
        vm.marketSector = vm.detail.MarketSectorName != null ? vm.detail.MarketSectorName : "";
        vm.subSector = vm.detail.SubSectorName != null ? vm.detail.SubSectorName : "";
        vm.categoryName = vm.detail.CategoryName != null ? vm.detail.CategoryName : "";
        vm.techFocalPointName = vm.detail.TechFocalPointName != null ? vm.detail.TechFocalPointName : "";
        vm.techFocalPointID = vm.detail.TechFocalPoint != null ? vm.detail.TechFocalPoint : "";
        vm.description = vm.detail.Description != null ? vm.detail.Description : "";
        vm.localNumber = vm.detail.LocalNumber;
        vm.scmFocalPoint = vm.detail.SCMFocalPointName != null ? vm.detail.SCMFocalPointName : "";
        vm.scmFocalPointID = vm.detail.SCMFocalPoint != null ? vm.detail.SCMFocalPoint : "";
        vm.sourcingActivity = vm.detail.SourcingActivityName != null ? vm.detail.SourcingActivityName : "";
        vm.entity = vm.detail.EntityID != null ? vm.detail.EntityID.toString() : "";
        vm.existingContractAward = vm.detail.ExistingContractAward ? vm.detail.ExistingContractAward.toString() : "";
        vm.existingContract = vm.detail.ExistingContract ? vm.detail.ExistingContract.toString() : "";
        vm.materialService = vm.detail.MaterialOrService ? vm.detail.MaterialOrService.toString() : "";
        vm.prNumber = vm.detail.PRNumber != null ? vm.detail.PRNumber : "";
        vm.CCStartDate = vm.detail.CurrentContractStartDate != null ? new Date(vm.detail.CurrentContractStartDate) : "";
        vm.CCEndDate = vm.detail.CurrentContractEndDate != null ? new Date(vm.detail.CurrentContractEndDate) : "";
        vm.newContractStartDate = vm.detail.NewContractStartDate != null ? new Date(vm.detail.NewContractStartDate) : "";
        vm.newContractEndDate = vm.detail.NewContractEndDate != null ? new Date(vm.detail.NewContractEndDate) : "";
        vm.sourcingDateStart = vm.detail.SourcingDateStart != null ? new Date(vm.detail.SourcingDateStart) : "";
        vm.cgcApprovalDate = vm.detail.CGCApprovalDate != null ? new Date(vm.detail.CGCApprovalDate) : "";
        vm.bidIssueDate = vm.detail.BidIssueDate != null ? new Date(vm.detail.BidIssueDate) : "";
        vm.bidDuedDate = vm.detail.BidDueDate != null ? new Date(vm.detail.BidDueDate) : "";
        vm.bidEvalCompletionDate = vm.detail.BidEvaluationCompletionDate != null ? new Date(vm.detail.BidEvaluationCompletionDate) : "";
        vm.negotiationDate = vm.detail.NegotiationDate != null ? new Date(vm.detail.NegotiationDate) : "";
        vm.awardRecomDate = vm.detail.AwardRecommendation != null ? new Date(vm.detail.AwardRecommendation) : "";
        vm.excomBoardApproval = vm.detail.ExcomBoardApproval != null ? new Date(vm.detail.ExcomBoardApproval) : "";
        vm.consessPartApproval = vm.detail.PartnersApproval != null ?  new Date(vm.detail.PartnersApproval) : "";
        vm.contractExecutionDate = vm.detail.ContractExecutionDate != null ? new Date(vm.detail.ContractExecutionDate) : "";
        vm.comments = vm.detail.Comments != null ? vm.detail.Comments : "";
        vm.procPlanStatusName = vm.detail.ProcPlanStatusName != null ? vm.detail.ProcPlanStatusName : "";
        vm.procPlanStatus = vm.detail.ProcPlanStatus != null ? vm.detail.ProcPlanStatus : "";
        vm.scmStatusName = vm.detail.SCMStatusName != null ? vm.detail.SCMStatusName : "";
        vm.scmStatus = vm.detail.SCMStatus != null ? vm.detail.SCMStatus : null;
        vm.procurementPlanID = vm.detail.ProcurementPlanID;;
        vm.RCValue = vm.detail.RemainingContractValue != null ? vm.detail.RemainingContractValue : "";
        vm.ECValue = vm.detail.ExistingContractValue != null ? vm.detail.ExistingContractValue : "";
        vm.ECNumber = vm.detail.ExistingContractNumber != null ? vm.detail.ExistingContractNumber : "";
        vm.isCalendarOpened = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('scm-procurement-plan');
            UIControlService.loadLoadingModal("Loading...");
            if (vm.ECValue && vm.RCValue) {
                vm.RCValueP = (Number(vm.RCValue) / Number(vm.ECValue)) * 100;
            }
                vm.loadDept();
                var day = getDays(vm.newContractEndDate, vm.newContractStartDate);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
        }
        vm.myConfig2 = {
            maxItems: 1,
            optgroupField: "FullName",
            labelField: "FullName",
            valueField: "FullName",
            searchField: "FullName",
            render: {
                optgroup_header: function (data, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(data.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.myConfig3 = {
            maxItems: 1,
            optgroupField: "CategoryName",
            labelField: "CategoryName",
            valueField: "CategoryName",
            searchField: "CategoryName",
            render: {
                optgroup_header: function (data, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(data.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.myConfig4 = {
            maxItems: 1,
            optgroupField: "MethodName",
            labelField: "MethodName",
            valueField: "MethodName",
            searchField: "MethodName",
            render: {
                optgroup_header: function (data, escape) {
                    return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(data.label_scientific) + '</strong></h5></div>';
                }
            },
            optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
            ]
        };

        vm.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        //vm.ubahDept = ubahDept;
        //function ubahDept($item, $model, $label) {
        //    if ($item) {
        //        vm.deptID = $item.DepartmentID;
        //        vm.deptName = $item.DepartmentName;
        //        vm.loadTechFocalPoint();
        //    } else {
        //        vm.isDept = 0;
        //    }
        //}
        vm.ubahManualDept = ubahManualDept;
        function ubahManualDept(deptName) {
            vm.datadept.forEach(function (item) {
                if (deptName == item.DepartmentName) {
                    vm.deptID = item.DepartmentID;
                    vm.deptName = item.DepartmentName;
                } else if (deptName != item.DepartmentName) {
                    vm.isDept = false;
                    vm.techFocalPointName = "";
                }
            });
            if (deptName == "") {
                vm.isDept = false;
                vm.techFocalPointName = "";
            } else {
                vm.loadTechFocalPoint();
            }
        }

        vm.ubahSCM = ubahSCM;
        function ubahSCM(scmName) {
            vm.dataSCMFocalPoint.forEach(function (item) {
                if (scmName == item.FullName) {
                    vm.scmFocalPoint = item.FullName;
                    vm.scmFocalPointID = item.employee_id;
                }
            });
        }

        vm.ubahTech = ubahTech;
        function ubahTech(techName) {
            vm.dataTFP.forEach(function (item) {
                if (techName == item.FullName) {
                    vm.techFocalPointName = item.FullName;
                    vm.techFocalPointID = item.employee_id;
                }
            });
        }

        vm.ubahMarket = ubahMarket;
        function ubahMarket(marketName) {
            vm.dataMarketSec.forEach(function (item) {
                if (marketName == item.CategoryName) {
                    vm.marketSector = item.CategoryName;
                    vm.marketSectorID = item.ID;
                }
            });
        }

        vm.ubahSub = ubahSub;
        function ubahSub(subName) {
            vm.dataSubSec.forEach(function (item) {
                if (subName == item.CategoryName) {
                    vm.subSector = item.CategoryName;
                    vm.subSectorID = item.ID;
                }
            });
        }

        vm.ubahCategory = ubahCategory;
        function ubahCategory(catName) {
            vm.dataCategory.forEach(function (item) {
                if (catName == item.CategoryName) {
                    vm.categoryName = item.CategoryName;
                    vm.categoryID = item.ID;
                }
            });
        }

        vm.ubahSourcing = ubahSourcing;
        function ubahSourcing(sourcingName) {
            vm.dataSourcingActivity.forEach(function (item) {
                if (sourcingName == item.MethodName) {
                    vm.sourcingActivity = item.MethodName;
                    vm.sourcingActivityID = item.MethodID;
                }
            });
        }


        //vm.ubahTech = ubahTech;
        //function ubahTech($item, $model, $label) {
        //    if ($item) {
        //        vm.techFocalPointName = $item.FullName;
        //        vm.techFocalPointID = $item.employee_id;
        //    }
        //}

        //vm.ubahSCM = ubahSCM;
        //function ubahSCM($item, $model, $label) {
        //    if ($item) {
        //        vm.scmFocalPoint = $item.FullName;
        //        vm.scmFocalPointID = $item.employee_id;
        //    }
        //}

        //vm.ubahMarket = ubahMarket;
        //function ubahMarket($item, $model, $label) {
        //    if ($item) {
        //        vm.marketSector = $item.CategoryName;
        //        vm.marketSectorID = $item.ID;
        //    }
        //}

        //vm.ubahSub = ubahSub;
        //function ubahSub($item, $model, $label) {
        //    if ($item) {
        //        vm.subSector = $item.CategoryName;
        //        vm.subSectorID = $item.ID;
        //    }
        //}

        //vm.ubahCategory = ubahCategory;
        //function ubahCategory($item, $model, $label) {
        //    if ($item) {
        //        vm.categoryName = $item.CategoryName;
        //        vm.categoryID = $item.ID;
        //    }
        //}

        //vm.ubahSourcing = ubahSourcing;
        //function ubahSourcing($item, $model, $label) {
        //    if ($item) {
        //        vm.sourcingActivity = $item.MethodName;
        //        vm.sourcingActivityID = $item.MethodID;
        //    }
        //}

        vm.loadDept = loadDept;
        function loadDept() {
            ProcurementPlanningService.getDeptSCM(function (reply) {
                if (reply.status === 200) {
                    vm.datadept = reply.data;
                    if (vm.datadept) {
                        loadEntity();
                    }

                    vm.datadept.forEach(function (item) {
                        if (vm.deptName == item.DepartmentName) {
                            console.info("Masuk");
                            vm.loadTechFocalPoint();
                        }
                    });
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadTechFocalPoint = loadTechFocalPoint;
        function loadTechFocalPoint() {
            ProcurementPlanningService.getTFPSCM({ Keyword: vm.deptName }, function (reply) {
                if (reply.status === 200) {
                    vm.dataTFP = reply.data.List;
                    if (vm.dataTFP.length > 0) {
                        vm.isDept = 1;
                    } else {
                        vm.isDept = 0;
                        vm.techFocalPointName = "";
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.ubahVendor = ubahVendor;
        function ubahVendor(data) {
            vm.vendorID = data.VendorID;
        }

        vm.ubahJumlahSaham = ubahJumlahSaham;
        function ubahJumlahSaham(jumlah) {
            if (jumlah < 0 || jumlah > 100) {
                vm.jumlahSaham = 0;
            }
        }

        vm.loadEntity = loadEntity;
        function loadEntity() {
            ProcurementPlanningService.getEntitySCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataEntity = reply.data;
                    if (vm.dataEntity) {
                        loadSubSector();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSubSector = loadSubSector;
        function loadSubSector() {
            ProcurementPlanningService.getSubSectorSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSubSec = reply.data;
                    if (vm.dataSubSec.length > 0) {
                        vm.isDataSub = 1;
                    }
                    if (vm.dataSubSec) {
                        loadProcPlanStatus();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadProcPlanStatus = loadProcPlanStatus;
        function loadProcPlanStatus() {
            ProcurementPlanningService.getProcPlanStatusSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataProcPlanStatus = reply.data;
                    vm.ProcPlanStatusData = [];
                    vm.dataProcPlanStatus.forEach(function (item) {
                        if (item.Value == "Plan") {
                            vm.status = item.RefID;
                            var param = {
                                RefID: item.RefID,
                                Type: item.Type,
                                Value: item.Value
                            }
                            vm.ProcPlanStatusData.push(param);
                        }
                    });
                    if (vm.dataProcPlanStatus) {
                        loadMarketSector();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadMarketSector = loadMarketSector;
        function loadMarketSector() {
            ProcurementPlanningService.getMarketSectorSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataMarketSec = reply.data;
                    if (vm.dataMarketSec.length > 0) {
                        vm.isDataMarket = 1;
                    }
                    if (vm.dataMarketSec) {
                        loadCategory();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadCategory = loadCategory;
        function loadCategory() {
            ProcurementPlanningService.getCategorySCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataCategory = reply.data;
                    if (vm.dataCategory.length > 0) {
                        vm.isDataCat = 1;
                    }
                    if (vm.dataCategory) {
                        loadExistingContract();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadExistingContract = loadExistingContract;
        function loadExistingContract() {
            ProcurementPlanningService.getExistingContractSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataExistingContract = reply.data;
                    if (vm.dataExistingContract) {
                        loadExistingContractAward();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadExistingContractAward = loadExistingContractAward;
        function loadExistingContractAward() {
            ProcurementPlanningService.getExistingContractAwardSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataExistingContractAward = reply.data;
                    if (vm.dataExistingContractAward) {
                        loadSCMFocalPoint();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSCMFocalPoint = loadSCMFocalPoint;
        function loadSCMFocalPoint() {
            ProcurementPlanningService.getSCMFocalPointSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSCMFocalPoint = reply.data.List;
                    if (vm.dataSCMFocalPoint.length > 0) {
                        vm.isDataSCM = 1;
                    }
                    if (vm.dataSCMFocalPoint) {
                        loadSourcingActivity();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadSourcingActivity = loadSourcingActivity;
        function loadSourcingActivity() {
            ProcurementPlanningService.getSourcingActivitySCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataSourcingActivity = reply.data;
                    if (vm.dataSourcingActivity.length > 0) {
                        vm.isDataSourcing = 1;
                    }
                    if (vm.dataSourcingActivity) {
                        loadContractType();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadContractType = loadContractType;
        function loadContractType() {
            ProcurementPlanningService.getContractTypeSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataContractType = reply.data;
                    if (vm.dataContractType) {
                        loadMaterialService();
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.loadMaterialService = loadMaterialService;
        function loadMaterialService() {
            ProcurementPlanningService.getMaterialServiceSCM(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataMaterialService = reply.data;
                    if (vm.dataMaterialService) {
                        UIControlService.unloadLoadingModal();
                    }

                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.UbahNewStartDate = UbahNewStartDate;
        function UbahNewStartDate(date) {

            if (date && vm.newContractEndDate) {
                if (date > vm.newContractEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWSTARTTOEND");
                    vm.newContractStartDate = "";
                    return;
                }
                var day = getDays(vm.newContractEndDate, date);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.UbahNewEndDate = UbahNewEndDate;
        function UbahNewEndDate(date) {
            if (date && vm.newContractStartDate) {
                if (date < vm.newContractStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.NEWENDTOSTART");
                    vm.newContractEndDate = "";
                    return;
                }
                var day = getDays(date, vm.newContractStartDate);
                var data = day / 30;
                var toMonth = Math.round(data);
                vm.newContractDuration = toMonth;
                vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(vm.newContractExtensionsDuration);
            }
        }

        vm.ubahCCStart = ubahCCStart;
        function ubahCCStart(date) {
            if (date && vm.CCEndDate) {
                if (date > vm.CCEndDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRSTARTTOEND");
                    vm.CCStartDate = "";
                    return;
                }
            }
        }

        vm.ubahCCEnd = ubahCCEnd;
        function ubahCCEnd(date) {
            if (date && vm.CCStartDate) {
                if (date < vm.CCStartDate) {
                    UIControlService.msg_growl("error", "MESSAGE.CURRENDTOSTART");
                    vm.CCEndDate = "";
                    return;
                }
            }
        }

        function getDays(endDt, startDt) {

            var end = endDt;
            var start = startDt;

            var ONE_DAY = 1000 * 60 * 60 * 24;
            var differenceMs = Math.abs(end - start);
            return Math.round(differenceMs / ONE_DAY);

        }

        vm.ubahNewContractExt = ubahNewContractExt;
        function ubahNewContractExt(number) {
            vm.newContractIncludeExtensionsDuration = Number(vm.newContractDuration) + Number(number);
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal("Loading...");

            var noDept = 0, noSCM = 0, noTech = 0, noMarket = 0, noSub = 0, noCategory = 0, noSourcing = 0;
            vm.datadept.forEach(function (item) {
                if (vm.deptName == item.DepartmentName) {
                    noDept++;
                }
            });
            if (noDept == 0 && vm.deptName != "" && vm.deptName != null) {
                vm.deptName = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NODEPT");
                return;
            }

            vm.dataSCMFocalPoint.forEach(function (item) {
                if (vm.scmFocalPoint == item.FullName) {
                    noSCM++;
                }
            });
            if (noSCM == 0 && vm.scmFocalPoint != "" && vm.scmFocalPoint != null) {
                vm.scmFocalPoint = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOSCM");
                return;
            }

            if (vm.dataTFP) {
                vm.dataTFP.forEach(function (item) {
                    if (vm.techFocalPointName == item.FullName) {
                        noTech++;
                    }
                });
                if (noTech == 0 && vm.techFocalPointName != "" && vm.techFocalPointName != null) {
                    vm.techFocalPointName = "";
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.NOTECH");
                    return;
                }
            }

            vm.dataMarketSec.forEach(function (item) {
                if (vm.marketSector == item.CategoryName) {
                    noMarket++;
                }
            });
            if (noMarket == 0 && vm.marketSector != "" && vm.marketSector != null) {
                vm.marketSector = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOMARKET");
                return;
            }

            vm.dataSubSec.forEach(function (item) {
                if (vm.subSector == item.CategoryName) {
                    noSub++;
                }
            });
            if (noSub == 0 && vm.subSector != "" && vm.subSector != null) {
                vm.subSector = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOSUB");
                return;
            }

            vm.dataCategory.forEach(function (item) {
                if (vm.categoryName == item.CategoryName) {
                    noCategory++;
                }
            });
            if (noCategory == 0 && vm.categoryName != "" && vm.categoryName != null) {
                vm.categoryName = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOCATEGORY");
                return;
            }

            vm.dataSourcingActivity.forEach(function (item) {
                if (vm.sourcingActivity == item.MethodName) {
                    noSourcing++;
                }
            });
            if (noSourcing == 0 && vm.sourcingActivity != "" && vm.sourcingActivity != null) {
                vm.sourcingActivity = "";
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NOSOURCING");
                return;
            }

            if (!(vm.deptID)) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.DEPT");
                return;
            }

            if (vm.description == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.DESC");
                return;
            }

            if (vm.entity == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ENTITY");
                return;
            }

            if (vm.materialService == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.MATERIALSERVICE");
                return;
            }

            if (!(vm.techFocalPointID)) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.TECH");
                return;
            }

            if (vm.newContractStartDate == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NEWSTART");
                return;
            }

            if (vm.newContractEndDate == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NEWEND");
                return;
            }
            vm.scmStatus = vm.scmStatus == null ? 0 : vm.scmStatus;


            ProcurementPlanningService.updateSCM({
                ProcurementPlanID: vm.procurementPlanID,
                DepartmentID: vm.deptID,
                Description: vm.description,
                EntityID: Number(vm.entity),
                MaterialOrService: Number(vm.materialService),
                Reference: vm.ppReference,
                CurrentSupplier: vm.currentSupplier,
                SCMFocalPoint: vm.scmFocalPointID,
                TechFocalPoint: vm.techFocalPointID,
                MarketSector: vm.marketSectorID,
                SubSector: vm.subSectorID,
                Category: vm.categoryID,
                SourcingActivity: vm.sourcingActivityID,
                ContractType: vm.contractType,
                ExistingContract: vm.existingContract,
                ExistingContractAward: vm.existingContractAward,
                PRNumber: vm.prNumber,
                CurrentContractStartDate: vm.CCStartDate,
                CurrentContractEndDate: vm.CCEndDate,
                NewContractStartDate: vm.newContractStartDate,
                NewContractEndDate: vm.newContractEndDate,
                NewContractExtensionsOption: vm.newContractExtensionsDuration,
                ProcPlanStatus: Number(vm.procPlanStatus),
                SourcingDateStart: vm.sourcingDateStart,
                CGCApprovalDate: vm.cgcApprovalDate,
                BidIssueDate: vm.bidIssueDate,
                BidDueDate: vm.bidDuedDate,
                BidEvaluationCompletionDate: vm.bidEvalCompletionDate,
                NegotiationDate: vm.negotiationDate,
                AwardRecommendation: vm.awardRecomDate,
                ExcomBoardApproval: vm.excomBoardApproval,
                PartnersApproval: vm.consessPartApproval,
                ContractExecutionDate: vm.contractExecutionDate,
                Comments: vm.comments,
                AbuDhabiNumber: vm.globalNumber,
                StatusPlan: Number(vm.statusProc),
                SKKMigasNumber: vm.skkMigasNumber,
                SCMStatus: vm.scmStatus,
                ExistingContractValue: vm.ECValue,
                RemainingContractValue: vm.RCValue,
                ExistingContractNumber: vm.ECNumber
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS_SAVED");
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERROR");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.ECValueChange = ECValueChange;
        function ECValueChange(input) {
            if (input && vm.RCValue) {
                vm.RCValueP = (Number(vm.RCValue) / Number(input)) * 100;
            }
        }

        vm.RCValueChange = RCValueChange;
        function RCValueChange(input) {
            if (input && vm.ECValue) {
                vm.RCValueP = (Number(input) / Number(vm.ECValue)) * 100;
            }
        }
    }
})();