﻿(function () {
    'use strict';

    angular.module("app").controller("DetailApprovalProcurementPlanCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', 'ProcurementPlanningService', '$stateParams','$q'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, ProcurementPlanningService, $stateParams,$q) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.ProcurementPlanID = item.item.ProcurementPlanID;
        vm.SCMFocalPoint = item.item.SCMFocalPoint;
        vm.UIControl = UIControlService;
        vm.currentPage = 1;
        vm.totalItems = 0;
        vm.currentPageSCM = 1;
        vm.totalItemsSCM = 0;
        vm.dataApproverSCM = [];

        function init() {
            $translatePartialLoader.addPart('approval-procurement-plan');
            jLoad(1).then(function () {
                if (vm.SCMFocalPoint != 0) {
                    getDataApproverSCM(1);

                } else {
                    UIControlService.unloadLoadingModal();
                }
            })
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            var defer = $q.defer();
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            ProcurementPlanningService.selectApproverProcurementPlanNonScm({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.searchBy,
                IntParam1: vm.ProcurementPlanID
            }, function (reply) {
                //UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApprover = data.List;
                    vm.totalItems = data.Count;
                    defer.resolve();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    UIControlService.unloadLoadingModal();
                    defer.reject();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
                defer.reject();
            })

            return defer.promise;
        }

        vm.getDataApproverSCM = getDataApproverSCM;
        function getDataApproverSCM(current) {
            vm.currentPageSCM = current;
            var offset = (current * 10) - 10;

            ProcurementPlanningService.selectApproverProcurementPlanSCM({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.SCMFocalPoint
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApproverSCM = data.List;
                    vm.totalItemsSCM = data.Count;
                    defer.resolve();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    //UIControlService.unloadLoadingModal();
                    defer.reject()
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
                defer.reject();
            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();