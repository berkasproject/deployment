﻿(function () {
    'use strict';

    angular.module("app").controller("procurementPlanningCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'ProcurementPlanningService', '$filter', '$rootScope'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, ProcurementPlanningService, $filter, $rootScope) {
        var vm = this;
        vm.action = "";
        vm.keyword = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.materialService = "";
        vm.keyPlaceHolder = "";
        vm.status = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('procurement-plan');
            UIControlService.loadLoading("Loading...");
            vm.jLoad(1);
        }


        vm.ubahMaterialService = ubahMaterialService;
        function ubahMaterialService(materialService) {
            vm.materialService = materialService;
            vm.jLoad(1);
        }

        vm.ubahStatus = ubahStatus;
        function ubahStatus(status) {
            console.info(status);
            vm.status = status;
            vm.jLoad(1);
        }

        vm.act = act;
        function act(action) {
            vm.materialService = "";
            vm.status = "";
            vm.keyword = "";
       
            if(action == 1){
                vm.keyPlaceHolder = 'Local Number';
            }

            if(action == 2){
                vm.keyPlaceHolder = $filter('translate')('LABEL.DESC');
            }

            if(action == 3){
                vm.keyPlaceHolder = 'Material/Service';
            }

            if(action == 5){
                vm.keyPlaceHolder = 'Technical Focal POint';
            }

            if(action == 6){
                vm.keyPlaceHolder = 'Status';
            }

            if (action == 7) {
                vm.keyPlaceHolder = 'SCM Focal Point';
            }
            vm.action = action;
            //if (vm.action == "") {
                vm.jLoad(1);
            //}
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            ProcurementPlanningService.select({
                Keyword: vm.action,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword2: vm.keyword,
                IntParam1: Number(vm.materialService),
                IntParam2: Number(vm.status)
            }, function (reply) {
                vm.plans = reply.data.List;
                vm.totalItems = reply.data.Count;
                UIControlService.unloadLoading();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                //$rootScope.unloadLoading();
            });
        }

        vm.cari = cari;
        function cari(keyword) {
            vm.keyword = keyword;
            vm.jLoad(1);
        }

        vm.tambah = tambah;
        function tambah() {
            var post = {
                isAdd: 1
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/formProcurementPlan.html',
                controller: 'formProcurementPlanCtrl',
                controllerAs: 'formProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.publish = publish;
        function publish(data) {
            var post = {
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/publishProcurementPlan.html',
                controller: 'publishProcurementPlanCtrl',
                controllerAs: 'publishProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.ubah = ubah;
        function ubah(data) {
            var post = {
                ubah: 1,
                data: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/formUbahProcurementPlan.html',
                controller: 'formUbahProcurementPlanCtrl',
                controllerAs: 'formUbahProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.lihat = lihat;
        function lihat(data) {
            var post = {
                ubah: 0,
                data: data,
                scm: 0
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/viewProcurementPlan.html',
                controller: 'viewProcurementPlanCtrl',
                controllerAs: 'viewProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.hapus = hapus;
        function hapus(id) {
            bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
                if (reply) {
                    //UIControlService.loadLoading(loadmsg);
                    ProcurementPlanningService.hapus({
                        ProcurementPlanID: id
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_DELETE');
                            vm.init();
                        } else {
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
                    });
                }
            });
        }

        vm.detailApproval = detailApproval;
        function detailApproval(data) {
            var post = {
                item: data
            };

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/procurement-plan/detailApprovalProcPlan.html',
                controller: 'DetailApprovalProcurementPlanCtrl',
                controllerAs: 'DetailApprovalProcurementPlanCtrl',
                backdrop: false,
                resolve: {
                    item: function () {
                        return post;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        

    }
})();