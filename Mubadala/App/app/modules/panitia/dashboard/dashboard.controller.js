(function () {
	'use strict';

	angular.module("app").controller("DashboardCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'ApprovalService', 'DashboardAdminService', 'UIControlService', 'SocketService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, ApprovalService, DashboardAdminService, UIControlService, SocketService) {

		var vm = this;
		vm.init = init;
		vm.onSearchClick = onSearchClick;
		vm.loadApproval = loadApproval;
		vm.onMenujuAppClick = onMenujuAppClick;

		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.maxSize = 10;

		vm.appSrcText = '';
		vm.appPageSize = 10;
		vm.appCurrPage = 1;
		vm.approval = [];
		vm.appCount = 0;

		vm.adminVM = false;
		vm.adminProc = false;
		vm.adminContract = false;

		vm.isCalendarOpened = [false, false, false, false];

		vm.labels =['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13'];
		vm.series =['Annee 2016', 'Annee 2017'];
	    vm.data =[
        [650, 590, 800, 810, 560, 550, 400, 554, 423, 536, 675, 990, 567],
        [280, 480, 400, 190, 860, 270, 900, 123, 564, 789, 875, 998, 221]
		];
	    vm.values =[
        [1000, 1500, 324, 5555, 76544, 654456, 7746, 6456, 63436, 6356, 64563, 63656, 643564],
        [62456, 245645, 2546245, 65544, 444, 456245, 321321, 13243, 14351, 45431, 453, 134534, 5435]
		];
	    vm.options = {
		    legend: {
		        display: true
		    },
		    tooltips: {
		        enabled: true,
		        titleFontFamily: "'Roboto'",
		        titleFontStyle: "normal",
		        titleFontColor: "#888888",
		        backgroundColor: "rgba(241, 242, 242, 0.9)",
		        titleFontSize: 15,
		        titleSpacing: 3,
		        titleMarginBottom: 10,
		        yPadding: 5,
		        bodyFontFamily: "'Roboto'",
		        bodyFontColor: "#888888",
		        bodyFontSize: 15,
		        bodySpacing: 5,
		        callbacks: {
		            title: function (items, data) {
		                return 'Periode ' + items[0].xLabel;
		            },
		            label: function (item, data) {
		                return data.datasets[item.datasetIndex].data[item.index] + ' (' + $scope.values[item.datasetIndex][item.index] + '$)';
		            }
		        }
		    },
		    scales: {
		        yAxes: [{
		            scaleLabel: {
		                display: true,
		                labelString: 'Quantite'
		            }
		        }],
		        xAxes: [{
		            scaleLabel: {
		                display: true,
		                labelString: 'Periodes'
		            }
		        }]
		    }
		};

		function init() {
		    $translatePartialLoader.addPart('dashboard-panitia');
		    getUsername();
		    //loadApproval(1);
		}

		function getDaysInMonth(m, y) {
		    return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
		}

		function getStartEndDate() {
		    vm.StartDate = new Date();
		    vm.StartDate.setDate(1);
		    vm.currentMonth = new Date().getMonth() + 1;
		    vm.currentYear = new Date().getFullYear();
		    vm.lastDay = getDaysInMonth(vm.currentMonth, vm.currentYear);
		    vm.EndDate = new Date();
		    vm.EndDate.setDate(vm.lastDay);
		    //console.info("startdate" + vm.StartDate);
		    //console.info("endate" + vm.EndDate);
		}

		function onSearchClick(appSrcText) {
			vm.appSrcText = appSrcText;
			vm.loadApproval(1);
		}

		vm.goToActivatedVendor = goToActivatedVendor;
		function goToActivatedVendor() {
		    $state.transitionTo('verifikasi-data', {
                status: 2
		    });
		}

		vm.goToVerifiedVendor = goToVerifiedVendor;
		function goToVerifiedVendor() {
		    $state.transitionTo('verifikasi-data', {
		        status: 4
		    });
		}

		vm.goToNewVendor = goToNewVendor;
		function goToNewVendor() {
		    $state.transitionTo('verifikasi-data', {
		        status: 1
		    });
		}

		vm.clearUserCache = clearUserCache
		function clearUserCache() {
			SocketService.emit("clearUserCache");
		}

		vm.goToVerificationReq = goToVerificationReq;
		function goToVerificationReq() {
		    $state.transitionTo('verifikasi-data', {
		        status: 3
		    });
		}

		function loadApproval(page) {
			//$rootScope.loadLoading('Silahkan Tunggu...');
			vm.appCurrPage = page;
			ApprovalService.selectByUser({
				pegawai_id: $rootScope.userSession.session_data.pegawai_id,
				offset: (page - 1) * vm.appPageSize,
				limit: vm.appPageSize,
				search: vm.appSrcText
			}, function (reply) {
				if (reply.status === 200) {
					vm.approval = reply.result.data.result;
					vm.approval.forEach(function (a) {
						a.tgl_mulai = a.tgl_mulai ? $rootScope.convertTanggalWaktu(a.tgl_mulai) : '';
						a.tgl_selesai = a.tgl_selesai ? $rootScope.convertTanggalWaktu(a.tgl_selesai) : '';
					});
					vm.appCount = reply.result.data.count;
				}
				UIControlService.unloadLoading();
			}, function (error) {
			    UIControlService.msg_growl("error", "Gagal mendapatkan data approval");
				UIControlService.unloadLoading();
			});
		}

		function timeconfiguration() {
		    vm.datepickeroptions = {
		      minMode : 'month'
		    }
		}

        vm.loadCSMS_maindate=loadCSMS_maindate;
		function loadCSMS_maindate() {
		    console.info("mainDate:" + vm.CSMS_maindate);
		}

        vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index]= true;
		}

		function getEmpPos() {
		    DashboardAdminService.getEmpPos({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.empPosCode = reply.data;
		            console.info("emposcode" + vm.empPosCode);
		            if (vm.empPosCode == 'L1 Procurement Support') { //vendor management
		                vm.adminVM = true;
		                getStartEndDate();
		                getNewVendorCount();
		                getVendorActivatedCount();
		                getVerificationReqCount();
		                getVerifiedVendorCount();

		                //tambahan
                        timeconfiguration();
		            }
		            else if (vm.empPosCode == 'L1 Stock Purchasing' || vm.empPosCode == 'L1 Direct Purchasing') { //procurement
		                vm.adminProc = true;
		                getStartEndDate();
		                procurementPerformanceValue();
		                costSavingValue();
		                POValueLocal();
		                POValueNational();
		                POValueInternational();
		            }
		            else if (vm.empPosCode == 'L1 CONTRACT ADMIN') { //contract service
		                vm.adminContract = true;
		                contractPerformanceValue();
		                contractCSValue();
		                ContractValueLocal();
		                ContractValueNational();
		                ContractValueInternational();
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		       // UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}


		function contractPerformanceValue() {
		    DashboardAdminService.contractPerformanceValue({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.contractPerfValue = reply.data;
		            //console.info("contractPerformance" + vm.contractPerfValue);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data Contract Peformance");
		        UIControlService.unloadLoading();
		    });
		}

		function contractCSValue() {
		    DashboardAdminService.contractCSValue({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.contractCSValue = reply.data;
		            //console.info("contact cs:" + vm.contractCSValue);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function ContractValueLocal() {
		    DashboardAdminService.contractValueByArea({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate),
		        column: 1
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.cvlocal = reply.data;
		            //console.info("cvlocal:" + vm.cvlocal);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function ContractValueNational() {
		    DashboardAdminService.contractValueByArea({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate),
		        column: 2
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.cvnational = reply.data;
		            //console.info("cvnat:" + vm.cvnational);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function ContractValueInternational() {
		    DashboardAdminService.contractValueByArea({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate),
		        column: 3
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.cvint = reply.data;
		            //console.info("cvint:" + vm.cvint);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}


		function procurementPerformanceValue() {
		    DashboardAdminService.procurementPerformanceValue({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.procPerformanceValue = reply.data;
		            //console.info("procPerformance" + vm.procPerformanceValue);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function costSavingValue() {
		    DashboardAdminService.costSavingValue({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.csValue = reply.data;
		            //console.info("cs:" + vm.csValue);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function POValueLocal() {
		    DashboardAdminService.POValueByArea({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate),
                column: 1
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.polocal = reply.data;
		            //console.info("local:" + vm.polocal);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function POValueNational() {
		    DashboardAdminService.POValueByArea({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate),
		        column: 2
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.ponat = reply.data;
		            //console.info("nat:" + vm.ponat);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function POValueInternational() {
		    DashboardAdminService.POValueByArea({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate),
		        column: 3
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.point = reply.data;
		            //console.info("point:" + vm.point);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function getVendorActivatedCount() {
		    DashboardAdminService.getVendorActivatedCount({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.vendorActivatedCount = reply.data;
		            //console.info("activatedVendorCount" + vm.vendorActivatedCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}


		function getUsername() {
		    DashboardAdminService.getUsername({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.username = reply.data;
		            if (vm.username != 'admin' || vm.username != 'admin_proc') {
		                getEmpPos();
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function getNewVendorCount() {
		    DashboardAdminService.getNewVendorCount({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.newVendorCount = reply.data;
		            //console.info("newVendorCount" + vm.newVendorCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function getVerifiedVendorCount() {
		    DashboardAdminService.getVerifiedVendorCount({
		        Date1: UIControlService.getStrDate(vm.StartDate),
		        Date2: UIControlService.getStrDate(vm.EndDate)
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.verifiedVendorCount = reply.data;
		            //console.info("verifiedVendorCount" + vm.verifiedVendorCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function getVerificationReqCount() {
		    DashboardAdminService.getVerificationReqCount({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.verificationReqCount = reply.data;
		            //console.info("verificationReqCount" + vm.verificationReqCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data employee position");
		        UIControlService.unloadLoading();
		    });
		}

		function onMenujuAppClick(app) {
			$state.transitionTo('approval-master', {
				flowpaket_id: app.flow_paket_id,
				paket_lelang_id: app.paket_id
			});
		};
	}
})();

/*
.controller('dashboardCtrl', function( $scope, $rootScope, $state, $cookieStore, $http){ // alert("Tekan Tombol Refresh (F5)");
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    
    $scope.appSrcText = '';
    $scope.appPageSize = 10;
    $scope.appCurrPage = 1;
    $scope.approval = [];
    $scope.appCount = 0;

    $scope.init = function(){
        $rootScope.getSession().then(function(result){
            $rootScope.userSession = result.data.data;
            $rootScope.userLogin = $rootScope.userSession.session_data.username;
            $rootScope.authorize(bacaNotif());
            $scope.loadApproval(1);
        });
        //bacaNotif(); // AWN | old: $rootScope.readNotif();
    };   
    
    $scope.onSearchClick = function(appSrcText){
        $scope.appSrcText = appSrcText;
        $scope.loadApproval(1);
    };
    
    $scope.loadApproval = function(page){
        $rootScope.loadLoading('Silahkan Tunggu...');
        $scope.appCurrPage = page;
        $rootScope.authorize(
            $http.post($rootScope.url_api + 'approval/select/byuser', {
                pegawai_id: $rootScope.userSession.session_data.pegawai_id,
                offset: (page - 1) * $scope.appPageSize,
                limit: $scope.appPageSize,
                search: $scope.appSrcText
            }).success(function(reply) {
                if (reply.status === 200) {
                    $scope.approval = reply.result.data.result;
                    $scope.approval.forEach(function(a){
                        a.tgl_mulai = a.tgl_mulai ? $rootScope.convertTanggalWaktu(a.tgl_mulai) : '';
                        a.tgl_selesai = a.tgl_selesai ? $rootScope.convertTanggalWaktu(a.tgl_selesai) : '';
                    });
                    $scope.appCount = reply.result.data.count;
                }
                $rootScope.unloadLoading();
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                $rootScope.unloadLoading();
                return;
            })
        );
    };
    
    $scope.onMenujuAppClick = function(app){
        $state.transitionTo('approval-master', {
            flowpaket_id: app.flow_paket_id,
            paket_lelang_id: app.paket_id
        });
    };
    
    function bacaNotif(){ 
        
//        eb.send( auth, {sessionID: sess}, function( authReply ){ // AWN-Auth-Step4
//            if( authReply.status === 'ok' ){
//                $rootScope.userlogged = authReply.username; // AWN-Auth-Step5
//                
//                $rootScope.readNotif(); // AWN
//                
//            } else { // AWN-Auth-Step6
//                $rootScope.isLogged = false;
//                $rootScope.userLogged = "";
//                $state.transitionTo('login');                                                
//            }
//        }); // end: AWN-Auth-Step7            
    } // end bacaNotif     
});
*/