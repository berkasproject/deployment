﻿(function () {
	'use strict';

	angular.module("app").controller("TCCtrl", ctrl);

	ctrl.$inject = ['item','GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService', '$uibModalInstance'];
	function ctrl(item, GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService, $uibModalInstance) {

		var vm = this;
		vm.init = init;
		vm.item = item;

		function init() {
		    $translatePartialLoader.addPart('detail-po');
		}

		//CompanyPerson
		vm.loadCompanyPerson = loadCompanyPerson;
		function loadCompanyPerson(current) {
			vm.currentPage = current;
			var offset = 0;
			PUbahDataService.selectCompanyPerson({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data.List;
					vm.totalData = reply.data.Count;
					vm.changedData = [];
					for (var i = 0; i < vm.totalData; i++) {
						if (vm.verified[i].ID === vm.item.item.RefVendorId) {
							vm.changedData.push(vm.verified[i]);
						}
					}
					console.info("changedata compers:" + JSON.stringify(vm.changedData));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoading();
			});
		}


		//BusinessField
		vm.loadBusinessField = loadBusinessField;
		function loadBusinessField(current) {
			vm.currentPage = current;
			var offset = 0;
			PUbahDataService.selectBusinessField({
				Parameter: vm.VendorID,
				Offset: offset,
				Limit: vm.totalData
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data.List;
					vm.changedData = [];
					for (var i = 0; i < vm.verified.length; i++) {
						if (vm.verified[i].ID === vm.item.item.RefVendorId) {
							vm.changedData.push(vm.verified[i]);
						}
					}
					console.info("changedata bField:" + JSON.stringify(vm.changedData));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoading();
			});
		}


		//Experience
		vm.loadVendorExperience = loadVendorExperience;
		function loadVendorExperience(current) {
			console.info("expType:" + vm.expType);
			var offset = 0;
			if (vm.expType == 3154) {
				UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
				PUbahDataService.selectVendorExperience({
					Parameter: vm.VendorID,
					Offset: offset,
					Limit: vm.totalData,
					column: 1
				}, function (reply) {
					console.info("expe: " + JSON.stringify(reply));
					if (reply.status === 200) {
						vm.listCurrExp = reply.data.List;
						vm.changedData = [];
						for (var i = 0; i < vm.listCurrExp.length; i++) {
							if (vm.listCurrExp[i].ID === vm.item.item.RefVendorId) {
								vm.changedData.push(vm.listCurrExp[i]);
							}
						}
						console.info("changedata currExp:" + JSON.stringify(vm.changedData));
						if (vm.changedData[0].StateLocationRef.CountryID === 360) {
							vm.lokasidetail = vm.changedData[0].CityLocation.Name + "," + vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						} else {
							vm.lokasidetail = vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						}
						vm.changedData[0].StartDate = UIControlService.getStrDate(vm.changedData[0].StartDate);
						UIControlService.unloadLoading();
					} else {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
					}
				}, function (err) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				});
			} else if (vm.expType == 3153) {
				UIControlService.loadLoading('LOADING.VENDOREXPERIENCE.MESSAGE');
				PUbahDataService.selectVendorExperience({
					Parameter: vm.VendorID,
					Offset: offset,
					Limit: 10,
					column: 2
				}, function (reply) {
					//console.info("current?:"+JSON.stringify(reply));
					if (reply.status === 200) {
						UIControlService.unloadLoading();
						vm.listFinExp = reply.data.List;
						vm.changedData = [];
						for (var i = 0; i < vm.listFinExp.length; i++) {
							if (vm.listFinExp[i].ID === vm.item.item.RefVendorId) {
								vm.changedData.push(vm.listFinExp[i]);
							}
						}
						console.info("changedata finExp:" + JSON.stringify(vm.changedData));
						if (vm.changedData[0].StateLocationRef.CountryID === 360) {
							vm.lokasidetail = vm.changedData[0].CityLocation.Name + "," + vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						} else {
							vm.lokasidetail = vm.changedData[0].StateLocationRef.Name + "," + vm.changedData[0].StateLocationRef.Country.Name + "," + vm.changedData[0].StateLocationRef.Country.Continent.Name;
						}
						vm.changedData[0].StartDate = UIControlService.getStrDate(vm.changedData[0].StartDate);
					} else {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
					}
				}, function (err) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
				});
			}
		}

	    //Equipment
	    //building
		vm.loadDataBuilding = loadDataBuilding;
		vm.listBuilding = [];
		vm.maxSizeBuild = vm.totalData;
		function loadDataBuilding(current) {
		    var offset = (current * vm.maxSizeBuild) - vm.maxSizeBuild;
		    UIControlService.loadLoading(vm.msgLoading);
		    PUbahDataService.selectBuilding({
		        Parameter: vm.VendorID,
		        Offset: offset,
		        Limit: vm.totalData
		    }, function (reply) {
		        console.info("bangunanCR:" + JSON.stringify(reply));
		        UIControlService.unloadLoading();
		        vm.listBuilding = reply.data.List;
		        vm.totalData = reply.data.Count;
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		//VendorBalance
		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}
	}
})();
//TODO


