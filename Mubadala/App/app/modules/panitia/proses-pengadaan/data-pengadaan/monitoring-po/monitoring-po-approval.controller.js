﻿(function () {
    'use strict';

    angular.module("app").controller("monitoringPOApproval", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.paket = [];
        vm.listCheck = [];
        vm.dataChecked = [];
        vm.searchBy = 0;
        vm.init = init;
        function init() {
            localStorage.removeItem('checked');
            $translatePartialLoader.addPart('monitoring-po');
            loadPaket(1);
        };

        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataMonitoringService.SelectApproval({
                Keyword: vm.keyword,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.searchBy,
                FilterType: vm.FilterType
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.paket = reply.data.List;
                    vm.dataChecked = JSON.parse(localStorage.getItem('checked'));
                    for (var i = 0; i < vm.paket.length; i++) {
                        if (vm.dataChecked !== null) {
                            for (var j = 0; j < vm.dataChecked.length; j++) {
                                if (vm.dataChecked[j].ID == vm.paket[i].ID) vm.paket[i].IsCheck = true;
                            }
                        }
                        if (vm.paket[i].POSent !== null) vm.paket[i].IsCheck = true;
                    }
                    vm.totalItems = Number(data.Count);

                }


                else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            },
                        function (error) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                        });
        };

        vm.Checked = Checked;
        function Checked(dt) {
            vm.dataChecked = JSON.parse(localStorage.getItem('checked'));
            vm.check = { ID: dt.ID };
            if (dt.IsCheck == true) {
                if (vm.dataChecked == null) vm.dataChecked = [];
                vm.dataChecked.push(vm.check);
            }
            else {
                for (var i = 0; i < vm.dataChecked.length; i++) {
                    if (vm.dataChecked[i].ID == dt.ID) {
                        vm.dataChecked.splice(i, 1);
                    }
                }
            }
            localStorage.removeItem('checked');
            localStorage.setItem('checked', JSON.stringify(vm.dataChecked));
            console.info(JSON.parse(localStorage.getItem('checked')));
        }

        vm.cariPaket = cariPaket;
        function cariPaket(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            loadPaket();
        };

        vm.viewPO = viewPO;
        function viewPO(id) {
            $state.transitionTo('detail-po', { id : id });
        };


        vm.upload = upload;
        function upload() {
            $state.transitionTo('data-upload-po');
        };


        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
        vm.sent = sent;
        function sent() {
            vm.dataChecked = JSON.parse(localStorage.getItem('checked'));
            if (vm.dataChecked === null) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
            }
            else {
                if (vm.dataChecked.length == 0) {

                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                }
                else {

                    DataMonitoringService.SentPO(vm.dataChecked,
                                      function (reply) {
                                          if (reply.status === 200) {
                                              UIControlService.msg_growl("success", "MESSAGE.SUC_SEND_PO");
                                              vm.init();

                                          }
                                          else {
                                              UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
                                              return;
                                          }
                                      },
                                      function (err) {
                                          UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                                          UIControlService.unloadLoadingModal();
                                      }
                                  );
                }
            }
            
        }

        vm.detailApproval = detailApproval;
        function detailApproval(dt) {
            var data = {
                data: dt
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/detailApproval.modal.html',
                controller: 'detailApprovalCtrl',
                controllerAs: 'detailApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.sendApprove = sendApprove;
        function sendApprove(dt) {
            var data = {
                data: dt
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/SendApproval.html',
                controller: 'detailApproveCtrl',
                controllerAs: 'detailApproveCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

    }
})();
