﻿(function () {
	'use strict';

	angular.module("app").controller("monitoringPO", ctrl);

	ctrl.$inject = ['$timeout', 'Excel', '$state', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
	/* @ngInject */
	function ctrl($timeout, Excel, $state, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";

		vm.totalItems = 0;
		vm.column = 1;
		vm.maxSize = 10;
		vm.paket = [];
		vm.listCheck = [];
		vm.dataChecked = [];
		vm.searchBy = 0;
		vm.keyword = "";
		vm.pageSize = 10;
		vm.currentPage = 1;
		vm.exportHref;
		vm.exportHref2;
		vm.isReadOnly = localStorage.getItem('roles').toString().split(',').indexOf("APPLICATION.ROLE_VIEWMONITORINGPO");
		console.log(vm.isReadOnly);
		vm.isCalendarOpened = [false, false, false, false];

		vm.init = init;
		function init() {
			localStorage.removeItem('checked');
			localStorage.removeItem('checked-all');
			$translatePartialLoader.addPart('monitoring-po');

			loadPaket(1);
			loadGoodsAwardAll();
		};

		vm.Export = Export;
		function Export(tableId) {
			//UIControlService.loadLoading(loadmsg);
			//DataMonitoringService.loadAwardAll({
			//	DateNull1: vm.StartDate,
			//	DateNull2: vm.EndDate
			//}, function (reply) {
			//	UIControlService.unloadLoading();
			//	if (reply.status === 200) {
			//		vm.goodsAwardAll = reply.data;
			vm.exportHref = Excel.tableToExcel(tableId, 'sheet name');
			$timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
			//	} else {
			//		UIControlService.unloadLoading();
			//	}
			//}, function (error) {
			//	UIControlService.unloadLoading();
			//});
		}

		vm.ExportDataPO = ExportDataPO;
		function ExportDataPO(tableId) {
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.ExportDataPO({
				DateNull1: vm.StartDate,
				DateNull2: vm.EndDate
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.dataAllPO = reply.data;

					setTimeout(function () {
						vm.exportHref2 = Excel.tableToExcel(tableId, 'sheet name');
						location.href = vm.exportHref2;
					}, 3000); // trigger download
				} else {
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadGoodsAwardAll = loadGoodsAwardAll;
		function loadGoodsAwardAll() {
			DataMonitoringService.loadAwardAll({}, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.goodsAwardAll = reply.data;
				} else {
					//UIControlService.unloadLoading();
				}
			}, function (error) {
				//UIControlService.unloadLoading();
			});
		};

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		vm.verifyEndDate = verifyEndDate;
		function verifyEndDate(selectedEndDate, selectedStartDate) {
			var convertedEndDate = UIControlService.getStrDate(selectedEndDate);
			var convertedStartDate = UIControlService.getStrDate(selectedStartDate);

			if (selectedStartDate == null) {
				return;
			}

			if (convertedEndDate < convertedStartDate) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_DEADLINE");
				vm.EndDate = " ";
			}
		}

		vm.loadPaket = loadPaket;
		function loadPaket() {
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.Select({
				Keyword: vm.keyword,
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
				Column: vm.searchBy,
				FilterType: vm.FilterType
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.paket = reply.data.List;
					vm.dataChecked = JSON.parse(localStorage.getItem('checked'));
					vm.IsCheck = JSON.parse(localStorage.getItem('checked-all'));
					for (var i = 0; i < vm.paket.length; i++) {
						vm.paket[i].POSent = UIControlService.convertDateTime(vm.paket[i].POSent);
						if (vm.dataChecked !== null) {
							for (var j = 0; j < vm.dataChecked.length; j++) {
								if (vm.dataChecked[j].ID == vm.paket[i].ID && vm.dataChecked[j].VendorId == vm.paket[i].VendorId)
									vm.paket[i].IsCheck = true;
							}
						}
						if (vm.paket[i].POSent !== null && vm.paket[i].reff.Name != "RFQG_CANCELLED")
							vm.paket[i].IsCheck = true;
					}
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.loadPaketAll = loadPaketAll;
		function loadPaketAll() {
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.Select({
				Keyword: vm.keyword,
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
				Column: 5,
				FilterType: vm.FilterType
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.all = reply.data.List;
					for (var i = 0; i < vm.all.length; i++) {
						if (vm.all[i].flagApproval == true) {
							console.info(vm.all[i]);
							if (vm.all[i].reff.Name == "RFQ_DRAFT") vm.dataChecked.push({ ID: vm.all[i].ID, VendorId: vm.all[i].VendorId, type: vm.all[i].POmodel.POType });
							vm.all[i].IsCheck = true;
							if ((vm.all.length - 1) == i) {
								localStorage.removeItem('checked');
								localStorage.setItem('checked', JSON.stringify(vm.dataChecked));
							}

						}
					}
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.Checked = Checked;
		function Checked(dt) {
			vm.dataChecked = JSON.parse(localStorage.getItem('checked'));
			vm.check = { ID: dt.ID, VendorId: dt.VendorId, type: dt.POmodel.POType };
			if (dt.IsCheck == true) {
				if (vm.dataChecked == null) vm.dataChecked = [];
				vm.dataChecked.push(vm.check);
			} else {
				for (var i = 0; i < vm.dataChecked.length; i++) {
					if (vm.dataChecked[i].ID == dt.ID && vm.dataChecked[i].VendorId == dt.VendorId && vm.dataChecked[i].type == dt.POmodel.POType) {
						vm.dataChecked.splice(i, 1);
					}
				}
			}
			localStorage.removeItem('checked');
			localStorage.setItem('checked', JSON.stringify(vm.dataChecked));
			console.info(JSON.parse(localStorage.getItem('checked')));
		}

		vm.CheckedAll = CheckedAll;
		function CheckedAll() {
			vm.dataChecked = [];
			if (vm.IsCheck == true) {
				localStorage.setItem('checked-all', JSON.stringify(true));
				for (var j = 0; j < vm.paket.length; j++) {
					if (vm.paket[j].reff.Name == "RFQ_DRAFT")
						vm.paket[j].IsCheck = true;
				}
				loadPaketAll(1);
			} else {
				localStorage.removeItem('checked');
				localStorage.removeItem('checked-all');
				loadPaket(1);
			}
		}

		vm.cariPaket = cariPaket;
		function cariPaket(keyword) {
			vm.keyword = keyword;
			vm.currentPage = 1;
			loadPaket();
		};

		vm.viewPO = viewPO;
		function viewPO(id) {
			$state.transitionTo('detail-po', { id: id });
		};

		vm.upload = upload;
		function upload() {
			$state.transitionTo('data-upload-po');
		};

		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.sent = sent;
		function sent() {
			vm.dataChecked = JSON.parse(localStorage.getItem('checked'));
			if (vm.dataChecked === null) {
				UIControlService.msg_growl("error", "MESSAGE.NO_DATA");
			} else {
				if (vm.dataChecked.length == 0) {

					UIControlService.msg_growl("error", "MESSAGE.NO_DATA");
				} else {
					bootbox.confirm($filter('translate')('MESSAGE.SEND_PO'), function (yes) {
						if (yes) {
							UIControlService.loadLoading("MESSAGE.LOADING");
							DataMonitoringService.cekShipping(vm.dataChecked, function (reply) {
								if (reply.status === 200) {
									if (reply.data == true) {
										//UIControlService.msg_growl("success", "MESSAGE.SUC_SEND_PO");
										DataMonitoringService.SentPO(vm.dataChecked, function (reply) {
											if (reply.status === 200) {
											    UIControlService.msg_growl("success", "MESSAGE.SUC_SEND_PO");
                                                /*
												vm.listAward = [];
												vm.listAmanded = [];
												for (var i = 0; i < vm.dataChecked.length; i++) {
													if (vm.dataChecked[i].type == "Original")
														vm.listAward.push(vm.dataChecked[i]);
													else if (vm.dataChecked[i].type == "Amanded")
														vm.listAmanded.push(vm.dataChecked[i]);
													if (i === (vm.dataChecked.length - 1)) {
														sendMail(vm.listAward);
														sendMailAmanded(vm.listAmanded);
													}
												}
                                                */
												vm.init();
											} else {
											    UIControlService.unloadLoading();
												UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
												return;
											}
										}, function (err) {
										    UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
											UIControlService.unloadLoading();
										});
									} else {
										localStorage.removeItem('checked');
										localStorage.removeItem('checked-all');
										UIControlService.msg_growl("error", "MESSAGE.ERR_SHIPPING");
										UIControlService.unloadLoading();
										return;
									}
								} else {
								    UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
								    UIControlService.unloadLoading();
									return;
								}
							}, function (err) {
							    UIControlService.msg_growl("error", "MESSAGE.FAIL_SAVE");
								UIControlService.unloadLoading();
							});
						}
					});
				}
			}
		}

		vm.sendMail = sendMail;
		function sendMail(data) {
			DataMonitoringService.sendMail(data,
            function (reply) {
            	if (reply.status === 200) {
            		UIControlService.msg_growl("success", "MESSAGE.SEND_EMAIL");
            	}
            	else {
            		//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
            		return;
            	}
            },
            function (err) {
            	//UIControlService.msg_growl("error", "Gagal Akses Api!!");
            }
        );

		}

		vm.sendMailAmanded = sendMailAmanded;
		function sendMailAmanded(data) {
			DataMonitoringService.sendMailAmanded(data, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SEND_EMAIL");
				} else {
					//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
					return;
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "Gagal Akses Api!!");
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval(dt) {
			var data = {
				data: dt
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/detailApproval.modal.html',
				controller: 'detailApprovalCtrl',
				controllerAs: 'detailApprovalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.detailMonitoringPO = detailMonitoringPO;
		function detailMonitoringPO(data) {
			$state.go('monitoring-fpa', { id: data.ID });
		}

		vm.cancelPO = cancelPO;
		function cancelPO(id) {
			var data = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/cancelPO.modal.html',
				controller: 'CancelPOCtrl',
				controllerAs: 'CancelPOCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.negoUlang = negoUlang;
		function negoUlang() {
			bootbox.confirm($filter('translate')('Apakah anda yakin untuk negosiasi ulang ?'), function (yes) {
				if (yes) { }
			});
		}
	}
})();
