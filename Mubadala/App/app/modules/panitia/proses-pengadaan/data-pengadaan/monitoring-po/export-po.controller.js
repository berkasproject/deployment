﻿(function () {
	'use strict';

	angular.module("app").controller("ExportPOCtrl", ctrl);

	ctrl.$inject = ['GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
	/* @ngInject */
	function ctrl(GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.vendor_name = "";
		vm.list_detail = "";
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.dataPOId = String($stateParams.id);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.contactCP = [];
		vm.contactAddress = [];
		vm.paket = [];
		vm.init = init;
		function init() {
			vm.flagVAT = false;
			$translatePartialLoader.addPart('detail-po');
			loadPaket(1);
		};

		vm.loadPaket = loadPaket;
		function loadPaket() {
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.ExportPurchase({
				Status: vm.dataPOId
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.export = reply.data;
					if (vm.export.buyer.Phone == null) vm.export.buyer.Phone = "";
					if (vm.export.buyer.Email == null) vm.export.buyer.Email = "";
					vm.nasional = true;
					vm.hascity = true;
					vm.hasdistrict = true;
					vm.internasional = false;
					for (var i = 0; i < vm.export.vendorcontact.length; i++) {
						if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
							vm.contactCompany = vm.export.vendorcontact[i];
							if (vm.export.vendorcontact[i].Contact.Address.State.Country.Code == "IDN") vm.flagVAT = true;
							else vm.flagVAT = false;
						} else if (vm.export.vendorcontact[i].VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
							vm.contactCP.push(vm.export.vendorcontact[i]);
						} else if (vm.export.vendorcontact[i].VendorContactType.Type == "VENDOR_OFFICE_TYPE" && vm.export.vendorcontact[i].IsPrimary == null) {
							vm.contactAddress.push(vm.export.vendorcontact[i]);
							if (vm.contactAddress.length == 1) {
								if (vm.export.vendorcontact[i].Contact.Address.State.Country.Name === "Indonesia") {
									if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
										if (vm.export.vendorcontact[i].Contact.Address.DistrictID != null) {
											vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.Distric.Name + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
											vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
											vm.District = vm.export.vendorcontact[i].Contact.Address.Distric.Name;
											vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
											vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
											vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
										} else {
											vm.hasdistrict = false;
											vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
											vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
											vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
											vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
											vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
										}
									} else {
										vm.hascity = false;
										vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
										vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
										vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
										vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
									}
								} else {
									vm.internasional = true;
									vm.nasional = false;
									if (vm.export.vendorcontact[i].Contact.Address.CityID != null) {
										vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.City.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
										vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
										vm.City = vm.export.vendorcontact[i].Contact.Address.City.Name;
										vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
										vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
									} else {
										vm.hascity = false;
										vm.address = vm.export.vendorcontact[i].Contact.Address.AddressInfo + ", " + vm.export.vendorcontact[i].Contact.Address.State.Name + ", " + vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
										vm.AddressInfo = vm.export.vendorcontact[i].Contact.Address.AddressInfo;
										vm.State = vm.export.vendorcontact[i].Contact.Address.State.Name;
										vm.Country = vm.export.vendorcontact[i].Contact.Address.State.Country.Name;
									}
								}
							}
						}
					}
					showPO(1);
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.showPO = showPO;
		function showPO(current) {
			vm.currentPage = current;
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.loadItem({
				Keyword: "",
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
				Status: vm.dataPOId,
				FilterType: vm.export.VendorId
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.alldata = reply.data.List;
					vm.totalItems = Number(reply.data.Count);
					vm.Remark = "";
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.alldata = reply.data.List;
						vm.totalItems = Number(reply.data.Count);
						for (var i = 0; i < vm.alldata.length; i++) {
							if (vm.alldata[i].NettPricePOOri.includes("*")) {
								vm.alldata[i].flagPrice = true;
							} else
								vm.alldata[i].flagPrice = false;

							if (vm.alldata[i].BuyerRemark != null) {
								if (vm.Remark == "") vm.Remark = vm.alldata[i].BuyerRemark;
								else vm.Remark = vm.Remark + ', ' + vm.alldata[i].BuyerRemark;
							}
						}
					}
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.printPO = printPO;
		function printPO(exportPO) {

			var innerContents = document.getElementById(exportPO).innerHTML;
			var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
			popupWindow.document.open();
			popupWindow.document.write('<html><head><title>Purchase Order-' + vm.export.vendorcontact[0].Vendor.business.Name + ' ' + vm.export.vendorcontact[0].Vendor.VendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
			popupWindow.document.close();

			//$uibModalInstance.close();
		}

		vm.cariPaket = cariPaket;
		function cariPaket(keyword) {
			vm.keyword = keyword;
			vm.currentPage = 1;
			loadPaket();
		};

		vm.open = open;
		function open(param) {
			console.info("param" + param);
			if (param === 'Purchase Order') {
				var id = 'exportPO';
			} else if (param === 'Terms Of Condition') {
				tc(param);
			} else if (param === 'Acknowledgement Sheet') {
				Aknowsheet(vm.dataPOId);
			}
			console.info("id" + id);
			var innerContents = document.getElementById(id).innerHTML;
			/*
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Form Export PO</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();*/

			//$uibModalInstance.close();
		}

		vm.tc = tc;
		function tc(data) {
			var modalInstance = $uibModal.open({
				templateUrl: 'tc.html',
				controller: 'detailPO',
				controllerAs: 'detailPOCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.PO = PO;
		function PO(id) {
			$state.transitionTo('export-po', { id: id });
		};
		vm.Aknowsheet = Aknowsheet;
		function Aknowsheet(id) {
			$state.transitionTo('acknowledgementsheet', { id: id });
		};
		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.UploadSI = UploadSI;
		function UploadSI(dt) {
			var data = {
				data: dt
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/formUpload.html',
				controller: 'frmUploadCtrl',
				controllerAs: 'frmUploadCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}
	}
})();
