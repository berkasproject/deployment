﻿(function () {
	'use strict';

	angular.module("app").controller("detailPO", ctrl);

	ctrl.$inject = ['GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataMonitoringService', 'UIControlService'];
	/* @ngInject */
	function ctrl(GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataMonitoringService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.vendor_name = "";
		vm.list_detail = "";
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.dataPOId = String($stateParams.id);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isReadOnly = localStorage.getItem('roles').toString().split(',').includes("APPLICATION.ROLE_VIEWMONITORINGPO");
		vm.paket = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('detail-po');
			loadPaket();
		};

		vm.loadPaket = loadPaket;
		function loadPaket() {
			UIControlService.loadLoading(loadmsg);
			DataMonitoringService.Detail({
				Status: vm.dataPOId,
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.paket = reply.data;
					console.info("paket" + JSON.stringify(vm.paket));
				} else {
					$.growl.error({ message: "MESSAGE.ERR_LOAD" });
					UIControlService.unloadLoading();

				}
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};

		vm.cariPaket = cariPaket;
		function cariPaket(keyword) {
			vm.keyword = keyword;
			vm.currentPage = 1;
			loadPaket();
		};

		vm.open = open;
		function open(param) {
			console.info("param" + param);
			if (param === "DETAIL_PURCHASE") {
				PO(vm.dataPOId);
			} else if (param === "DETAIL_TOC") {
				tc(param);
			} else if (param === "DETAIL_ACKN") {
				Aknowsheet(vm.dataPOId);
			}
			console.info("id" + vm.dataPOId);
			//var innerContents = document.getElementById(vm.dataPOId).innerHTML;
			/*
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Form Export PO</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();*/

			//$uibModalInstance.close();
		}

		vm.tc = tc;
		function tc(data) {
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/toc.html',
				controller: 'TCCtrl',
				controllerAs: 'TCCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}

		vm.PO = PO;
		function PO(id) {
			$state.transitionTo('export-po', { id: id });
		};
		vm.Aknowsheet = Aknowsheet;
		function Aknowsheet(id) {
			$state.transitionTo('acknowledgementsheet', { id: id });
		};
		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.UploadSIandOtherDoc = UploadSIandOtherDoc;
		function UploadSIandOtherDoc(dt, flag) {
			var data = {
				data: dt,
				act: flag,
				Id: vm.dataPOId
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-po/formUpload.html',
				controller: 'frmUploadCtrl',
				controllerAs: 'frmUploadCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();
			});
		}
	}
})();
