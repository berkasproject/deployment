﻿(function () {
    'use strict';

    angular.module("app").controller("DetailItemCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService) {

        var vm = this;
        vm.ID = item.ID;
        vm.flag = item.flag;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.DocName = "";
        vm.DocumentUrl = "",
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('detail-dataupload-po');
            $translatePartialLoader.addPart('add-adendum');
            if (vm.flag == 0) loadPaket(1);
            else loadFPA();
        };

        vm.loadPaket = loadPaket;
        function loadPaket(current) {
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.pageSize) - vm.pageSize;
            UIControlService.loadLoading(loadmsg);
            DataFPAService.DetailDocUpload({
                Status: vm.ID,
                Offset: offset,
                Limit: vm.pageSize
            },
                   function (response) {
                       UIControlService.unloadLoading();
                       if (response.status == 200) {
                           vm.DataDocList = response.data.List;
                           vm.totalItems = Number(response.data.Count);
                       }
                   },
                   function (response) {
                       UIControlService.unloadLoading();
                   });
        }

        vm.loadFPA = loadFPA;
        function loadFPA(current) {
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.pageSize) - vm.pageSize;
            UIControlService.loadLoading(loadmsg);
            DataFPAService.DetailDocUploadFPA({
                Status: vm.ID,
                Offset: offset,
                Limit: vm.pageSize
            },
                   function (response) {
                       UIControlService.unloadLoading();
                       if (response.status == 200) {
                           vm.DataDocListFPA = response.data.List;
                           vm.totalItems = Number(response.data.Count);
                       }
                   },
                   function (response) {
                       UIControlService.unloadLoading();
                   });
        }

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.back = back;
        function back() {
            $uibModalInstance.close();
        }
    }
})();
