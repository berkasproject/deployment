﻿(function () {
	'use strict';

	angular.module("app").controller("AddendumCtrl", ctrl);

	ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.maxSize = 10;
		vm.VHSdata;
		vm.listFPA = [];
		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('detail-data-adendum');
			GetApproval(1, 1);
		};

		vm.initVHS = initVHS;
		function initVHS() {
		    $translatePartialLoader.addPart('detail-data-adendum');
		    GetApproval(1,2);
		};

		vm.detailApproval = detailApproval;
		function detailApproval(dt) {
			var data = {
				data: dt
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detailApproval.modal.html',
				controller: 'detailApprovalCtrl',
				controllerAs: 'detailApprovalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
			    if (vm.filterType == 1) vm.init();
			    else vm.initVHS();
			});
		}

		vm.detailAddendum = detailAddendum;
		function detailAddendum(dt) {
		    var data = {
		        data: dt
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/addendum-approval/edit-adendum-fpa.html',
		        controller: 'EditFPACtrl',
		        controllerAs: 'EditFPACtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        if (vm.filterType == 1) vm.init();
		        else vm.initVHS();
		    });
		}

		vm.GetApproval = GetApproval;
		function GetApproval(current, filter) {
		    vm.filterType = filter;
			vm.currentPage = current;
			UIControlService.loadLoading(loadmsg);
			DataFPAService.GetApproval({
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
                FilterType: filter
			},
            function (reply) {
            	UIControlService.unloadLoading();
            	if (reply.status === 200) {
            		vm.listFPA = reply.data.List;
            		vm.totalItems = Number(vm.listFPA.length);
            		if (vm.listFPA.length == 1 && vm.listFPA[0].AddendumId == 0) {
            			vm.listFPA = [];
            			vm.totalItems = 0;
            		}


            	}
            	else {
            		$.growl.error({ message: "MESSAGE.ERR_LOAD" });
            		UIControlService.unloadLoading();
            	}
            },
            function (error) {
            	UIControlService.unloadLoading();
            	UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
		};

		vm.cariPaket = cariPaket;
		function cariPaket(keyword) {
			vm.keyword = keyword;
			vm.currentPage = 1;
			loadPaket();
		};

		vm.addAdendum = addAdendum;
		function addAdendum(id, negoid) {
			$state.transitionTo('add-adendum-fpa', { id: id, negoid: negoid, flag: vm.flag });
		};

		vm.editAddendum = editAddendum;
		function editAddendum(id, negoid) {
			$state.transitionTo('edit-adendum-fpa', { id: id, negoid: negoid, flag: 1, flagstep: vm.flag });
		};
		vm.viewEditAddendum = viewEditAddendum;
		function viewEditAddendum(id, negoid) {
			$state.transitionTo('edit-adendum-fpa', { id: id, negoid: negoid, flag: 0, flagstep: vm.flag });
		};


		vm.Reject = Reject;
		function Reject(id) {
			UIControlService.loadLoading(loadmsg);
			DataFPAService.Reject({
				AddendumId: id,
			},
            function (reply) {
            	UIControlService.unloadLoading();
            	if (reply.status === 200) {
            		UIControlService.unloadLoading();
            		loadPaket();
            	}
            	else {
            		$.growl.error({ message: "MESSAGE.ERR_LOAD" });
            		UIControlService.unloadLoading();
            	}
            },
            function (error) {
            	UIControlService.unloadLoading();
            	UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
		};
		vm.viewAddendum = viewAddendum;
		function viewAddendum(AddendumId) {
			AddendumId: AddendumId
			var lempar = {
				datalempar: {
					AddendumId: AddendumId,
					AddendumCode: vm.listFPA[0].AddendumCode,
					VendorName: vm.listFPA[0].VendorName,
					TitleDoc: vm.listFPA[0].TitleDoc,
					TypeAddendum: vm.listFPA[0].TypeAddendum,
					BudgetContract: vm.listFPA[0].Budget_Val,
					SpendingVal: vm.listFPA[0].SpendingVal,

					AdditionalValue: vm.listFPA[0].AdditionalValue,
					RequestDate: vm.listFPA[0].RequestDate,
					RFQCode: vm.listFPA[0].RFQCode,
					StartDate: vm.listFPA[0].StartDate,
					EndDate: vm.listFPA[0].EndDate,
					StartContractDate: vm.listFPA[0].StartContractDate,
					FinishContractDate: vm.listFPA[0].FinishContractDate,
					Requestor: vm.listFPA[0].Requestor,
					VendorID: vm.listFPA[0].VendorID,
					TenderStepID: vm.listFPA[0].TenderStepID,
					VHSAwardId: vm.id,
					Remask: vm.listFPA[0].Remask,
					Duration: vm.listFPA[0].Duration,

				}
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detail-fpa.modal.html',
				controller: 'DetailAddendumFPACtrl',
				controllerAs: 'DetailAddendumFPACtrl',
				resolve: {
					item: function () {
						return lempar;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.loadData();

			});

		};
		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.back = back;
		function back() {
			if (vm.flag == 0) $state.go('monitoring-vhs');
			else $state.go('monitoring-fpa');
		}
        
		vm.approve = approve;
		function approve(data) {
			vm.id = data;
			bootbox.confirm($filter('translate')('MESSAGE.CONF_APP'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					DataFPAService.UpdateAddendumApproval({ ID: data.ID, AddendumId: data.AddendumId, ApprovalStatus: true },
                              function (reply) {
                              	if (reply.status === 200) {
                              		UIControlService.unloadLoading();
                              		UIControlService.msg_growl("success", "MESSAGE.SUCC_APP");
                              		if (vm.filterType == 1) init();
                              		else initVHS();
                              		    sendMailToApproval();
                              	}
                              },
                              function (err) {
                              	UIControlService.unloadLoading();
                              }
                          );
				}
			});
		}

		vm.sendMailToApproval = sendMailToApproval;
		function sendMailToApproval() {
			DataFPAService.sendMailToApproval({
				AddendumId: vm.id.AddendumId,
				ApprovalStatus: true,

			},
            function (reply) {
            	UIControlService.msg_growl("success", 'MESSAGE.EMAIL_SENT');
            	UIControlService.unloadLoading();
            },
            function (error) {
            	UIControlService.unloadLoading();
            });
		};

		vm.reject = reject;
		function reject(dt) {
			var data = {
				data: dt,
                flagReject: true
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detailApproval.modal.html',
				controller: 'detailApprovalCtrl',
				controllerAs: 'detailApprovalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
			    if (vm.filterType == 1) vm.init();
			    else vm.initVHS();
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval(dt) {
			var data = {
				data: dt
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-fpa/detailApproval.modal.html',
				controller: 'detailApprovalCtrl',
				controllerAs: 'detailApprovalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
			    if (vm.filterType == 1) vm.init();
			    else vm.initVHS();
			});
		}

	}
})();
