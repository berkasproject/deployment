﻿(function () {
	'use strict';

	angular.module("app").controller("DetEmailMonCtrl", ctrl);

	ctrl.$inject = ['$uibModalInstance', 'item', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService'];
	/* @ngInject */
	function ctrl($uibModalInstance, item, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		vm.list = item.item;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.maxSize = 10;
		vm.searchBy = 0;
		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('add-adendum');
			console.info(item);
			//loadPaket();
		};
		vm.sentMail = sentMail;
		function sentMail() {
			$uibModalInstance.close();
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss();
		}
	}
})();
