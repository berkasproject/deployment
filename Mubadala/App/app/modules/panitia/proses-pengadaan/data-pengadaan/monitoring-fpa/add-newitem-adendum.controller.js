﻿(function () {
    'use strict';

    angular.module("app").controller("AddFPANewItemCtrl", ctrl);

    ctrl.$inject = ['$uibModalInstance','$state', '$stateParams', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataFPAService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($uibModalInstance, $state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataFPAService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadCurrencies(1);
            loadCountry(1);

        };
        vm.save = save;
        function save() {
            if (vm.data.material_code == undefined || vm.data.material_code == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_CODE');
                return;
            }
            else if (vm.data.item_des == undefined || vm.data.item_des == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_DESC');
                return;
            }
            else if (vm.data.manufacture == undefined || vm.data.manufacture  == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_MANUFAC');
                return;
            }
            else if (vm.data.part_no == undefined || vm.data.part_no == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_PART');
                return;
            }
            else if (vm.data.estimation == undefined || vm.data.estimation == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_EST');
                return;
            }
            else if (vm.data.uop == undefined || vm.data.uop == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_UOP');
                return;
            }
            else if (vm.data.currency == undefined || vm.data.currency == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_CURR');
                return;
            }
            else if (vm.data.PriceIDR == undefined || vm.data.PriceIDR == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_PRICE');
                return;
            }
            else if (vm.data.eadtim == undefined || vm.data.eadtim == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_LEAD');
                return;
            }
            else if (vm.data.CountryOfOrigin == undefined || vm.data.CountryOfOrigin == null) {
                UIControlService.msg_growl("error", 'MESSAGE.NO_CO');
                return;
            }
            $uibModalInstance.close(vm.data);
        }

        vm.loadCurrencies = loadCurrencies;
        function loadCurrencies() {
            DataFPAService.Currency(
                function (response) {
                    if (response.status == 200) {
                        vm.currencyList = response.data;
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }
        function loadCountry() {
            DataFPAService.Country(
                function (response) {
                    if (response.status == 200) {
                        vm.NegaraList = response.data;
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }



    }
})();
