﻿(function () {
    'use strict';

    angular.module("app")
    .controller("konfBatalCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService, UIControlService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        var tenderRefID = item.TenderRefID;
        var procPackageType = item.ProcPackageType;
        
        vm.isRfqGoods = item.ProcPackTypeName === "TYPE_RFQGOODS";
        vm.isJasa = item.ProcPackTypeName === "TYPE_CR";
        vm.reTenderItems = true;
        vm.cancellationRemark = "";

        vm.init = init;
        function init() {
            vm.message = vm.isJasa ? "MESSAGE.CONFIRM_CANCEL_JASA" : "MESSAGE.CONFIRM_CANCEL";
        };
        
        vm.save = save;
        function save() {

            if (!vm.cancellationRemark)
            {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_REMARK');
                return;
            }

            UIControlService.loadLoadingModal("");
            DataPengadaanService.CancelTender({
                TenderRefID: tenderRefID,
                ProcPackageType: procPackageType,
                ReTenderItems: vm.reTenderItems,
                CancellationRemark: vm.cancellationRemark
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", 'MESSAGE.SUCC_CANCEL_TENDER');
                $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CANCEL_TENDER');
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();