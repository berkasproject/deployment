(function () {
    'use strict';

    angular.module("app")
    .controller("dataPengadaanTahapanController", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.TenderID = Number($stateParams.TenderID);

        vm.step = [];
        vm.paket;
        vm.namaLelang;
        vm.userBisaMengatur = false;
        //vm.page_id = 37;
        vm.selectedOption;
        //vm.menuhome = 0;
        vm.ditolak = false;
        vm.tahapanDitolak = {};
        vm.isCancelled = false;
        vm.IsAllowedEdit = false;
        vm.IsOERevision = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-pengadaan');
            UIControlService.loadLoading(loadmsg);

            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                vm.IsAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            DataPengadaanService.IsAllowed({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.data === true) {
                    loadSteps();
                } else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_NOT_ALLOWED');
                    kembali();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });
        };

        function loadSteps() {

            DataPengadaanService.IsOERevision({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                vm.IsOERevision = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_OE_STATUS');
            });

            vm.ditolak = false;
            /*cek tolak
            $http.post($rootScope.url_api + 'approval/cektolak', {
                paket_lelang_id: vm.idPaket
            }).success(function (reply) {
                console.info("tolak: " + JSON.stringify(reply));
                if (reply.status === 200) {
                    if (reply.result.data.length > 0) {
                        vm.ditolak = true;
                        vm.tahapanDitolak = reply.result.data[0];
                    }
                }
                else {
                    $.growl.error({ message: "Gagal mendapatkan status!" });
                    return;
                }
            }).error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                return;
            });
            */

            vm.userBisaMengatur = true;
            /* cek bisa mengatur 
            var arr2 = [];
            arr2.push(vm.idPaket);
            arr2.push($rootScope.userLogin);
            arr2.push(vm.page_id);
            //itp.paket.cekBisaMengatur
            $http.post($rootScope.url_api + 'paket/cekmengatur', {
                param: arr2,
                page_id: vm.page_id
            }).success(function (reply) {
                if (reply.status === 200) {
                    if (reply.result.data.length > 0) {
                        var data = reply.result.data[0].bisa_mengatur;
                        vm.userBisaMengatur = data;
                    } else {
                        vm.userBisaMengatur = false;
                    }
                }
                else {
                    $.growl.error({ message: "Gagal mendapatkan data hak akses!" });
                    return;
                }
            }).error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                return;
            });
            */

            //getStatusLelang();
            
            UIControlService.loadLoading(loadmsg);
            DataPengadaanService.GetStep({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType,
				ID:vm.TenderID
            }, function (reply) {
                vm.step = reply.data;
                if (vm.step.length > 0) {
                    vm.namaLelang = vm.step[0].tender.TenderCode + ' - ' + vm.step[0].tender.TenderName;
                    vm.isCancelled = vm.step[0].tender.IsCancelled;
                    vm.procPackTypeName = vm.step[0].tender.ProcPackTypeName;
                }
                vm.step.forEach(function (fl) {
                    fl.StartDateConverted = convertTanggal(fl.StartDate);
                    fl.EndDateConverted = convertTanggal(fl.EndDate);
                });
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
            });
        };

        /*
        function getStatusLelang() {
            var arr = [];
            arr.push(vm.idPaket);
            //itp.paket.selectById
            $http.post($rootScope.url_api + 'paket/byid', {
                param: arr
            }).success(function (reply) {
                if (reply.status === 200) {
                    vm.paket = reply.result.data[0];
                    vm.namaLelang = vm.paket.nama_paket;
                    vm.selectedOption = vm.paket.status_kelangsungan;
                }
            }).error(function (err) {
                $.growl.error({ message: "Gagal Akses API > " + err });
                return;
            });
        }
        */

        function convertTanggal(input) {
            return UIControlService.convertDateTime(input);
        };

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('data-pengadaan');
        };
                
        vm.aturTahapan = aturTahapan;
        function aturTahapan(step) {
            var stepItem = {
                ID: step.ID,
                TenderStepName: step.step.TenderStepName,
                StartDate: new Date(Date.parse(step.StartDate)),
                EndDate: new Date(Date.parse(step.EndDate)),
                Status: step.Status,
                IsRequireDateChangeApproval: step.IsRequireDateChangeApproval
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/atur-tahapan.modal.html?v=1.000004',
                controller: 'aturTahapanModalController',
                controllerAs: 'atModCtrl',
                resolve: {
                    item: function () {
                        return stepItem;
                    }
                }
            });
            modalInstance.result.then(function() {
                loadSteps();
            });
        };

        vm.aturTahapanAll = aturTahapanAll;
        function aturTahapanAll() {
            var stepItem = {
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType,
                TenderID: vm.TenderID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/atur-tahapan-all.modal.html?v=1.000002',
                controller: 'aturTahapanAllModalController',
                controllerAs: 'atallModCtrl',
                resolve: {
                    item: function () {
                        return stepItem;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadSteps();
            });
        };

        vm.detailApproval = detailApproval;
        function detailApproval(step) {
            var stepItem = {
                ID: step.ID,
                TenderStepName: step.step.TenderStepName,
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/detail-stepdateapproval.modal.html',
                controller: 'detailStepDateApprovalCtrl',
                controllerAs: 'detStepDateAppCtrl',
                resolve: { item: function () { return stepItem; } }
            });
        };

        vm.batalTender = batalTender;
        function batalTender() {
            var item = {
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType,
                ProcPackTypeName: vm.procPackTypeName
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/konfirmasi-batal.modal.html?v=1.000002',
                controller: 'konfBatalCtrl',
                controllerAs: 'konfBatalCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadSteps();
            });
            /*
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_CANCEL'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading(loadmsg);
                    DataPengadaanService.CancelTender({
                        TenderRefID: vm.TenderRefID,
                        ProcPackageType: vm.ProcPackType,
                        ReTenderItems: reTenderItems
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", 'MESSAGE.SUCC_CANCEL_TENDER');
                        loadSteps();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_CANCEL_TENDER');
                    });
                }
            });
            */
        }

        vm.revisiOE = revisiOE;
        function revisiOE() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REVISE_OE'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading(loadmsg);
                    DataPengadaanService.ReviseOE({
                        TenderRefID: vm.TenderRefID,
                        ProcPackageType: vm.ProcPackType,
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", 'MESSAGE.SUCC_REVISE_OE');
                        loadSteps();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_REVISE_OE');
                        if (error.substring(0, 4) === 'ERR_') {
                            UIControlService.msg_growl("error", 'MESSAGE.' + error);
                        }
                    });
                }
            });
        }

        vm.menujuTahapan = menujuTahapan;
        function menujuTahapan(step) {
        	//TenderRefID/:StepID/:ProcPackType
        	$state.transitionTo(step.step.FormTypeURL, { TenderRefID: vm.TenderRefID, StepID: step.ID, ProcPackType: vm.ProcPackType, TenderID: step.TenderID });
        }
    }
})();

