﻿(function () {
    'use strict';

    angular.module("app").controller("dataMRKOCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.id = 0;
        vm.maxSize = 10;
        vm.listVHS = [];
        vm.searchBy = 0;
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('data-mrko');
            loadPaket(1);
        };

        vm.loadPaket = loadPaket;
        function loadPaket(current) {
            vm.currentPage = current;
            UIControlService.loadLoading(loadmsg);
            DataVHSService.selectMonitoringMRKO({
                Keyword: vm.keyword,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                FilterType:vm.searchBy
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data.List;
                    vm.listVHS = reply.data.List;
                    vm.VHSdata = data;
                    vm.totalItems = Number(reply.data.Count);
                    vm.searchBy = 0;
                    vm.keyword = "";
                }
            },

            function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
                        
        };

        vm.cariPaket = cariPaket;
        function cariPaket(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            loadPaket();
        };

        vm.addMRKO = addMRKO;
        function addMRKO() {
            $state.transitionTo('add-mrko', {flag:0});
        };

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            var item = {
            
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko/upload-mrko.modal.html',
                controller: 'UploadMRKO',
                controllerAs: 'UploadMRKO',
                resolve: {
                item: function () {
                    return item;
                }
        }
            });
            modalInstance.result.then(function (data) {
                console.info(data.listItem);
                vm.DataDocList = data.listItem;

            });
        }

        vm.cancelMRKO = cancelMRKO;
        function cancelMRKO(ID) {
            vm.ID = ID;
            bootbox.confirm($filter('translate')('Apakah anda yakin ingin membatalkan MRKO?'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading(loadmsg);
                    DataVHSService.cancelMRKO({
                        ID: ID
                    },
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status == 200) {
                            UIControlService.msg_growl("success", "Berhasil membatalkan MRKO");
                            sendMailToVendor();
                            init();
                        }
                    },
                    function (error) {
                        UIControlService.unloadLoading();
                    });
                }
            });
        }

        vm.sent = sent;
        function sent(ID) {
            vm.ID = ID;
            bootbox.confirm($filter('translate')('Apakah anda yakin ingin mengirim detail MRKO ke vendor ?'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading(loadmsg);
                    DataVHSService.sentMRKO({
                        ID: ID
                    },
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "Success Sent ");
                            sendMailToVendor();
                            init();
                        }
                    },
                    function (error) {
                        UIControlService.unloadLoading();
                    });
                }
            });           
        };

        vm.sendMailToVendor = sendMailToVendor;
        function sendMailToVendor() {
            DataVHSService.sendMailToVendor({
                ID: vm.ID
            },
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "Email Sent !!! ");
                        }
                    },
                    function (error) {
                        UIControlService.unloadLoading();
                    });
        }




        vm.toDetail = toDetail;
        function toDetail(data) {
            var item = {
                item: data,
                ID : data.UploadDataDetailMRKO[0].IdUpload,
                Periode1: UIControlService.getStrDate(data.Periode1),
                Periode2: UIControlService.getStrDate(data.Periode2)
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko/detail-itemmonitoring.html',
                controller: 'DetailItemMRKOCtrl',
                controllerAs: 'DetailItemMRKOCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();

            });
        }

        vm.DetailApproval = DetailApproval;
        function DetailApproval(data) {
            var item = {
                MonitoringVHSMRKOId: data.ID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/data-pengadaan/monitoring-vhs/data-mrko/DetailApproval.html',
                controller: 'detailApprovalMRKOCtrl',
                controllerAs: 'detailApprovalMRKOCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                vm.init();

            });
        }

    }
})();
