﻿(function () {
    'use strict';

    angular.module("app").controller("DetailItemMRKOModalCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        vm.ID = item.ID;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.DocName = "";
        vm.DocumentUrl = "",
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadPaket(1);
        };

        vm.loadPaket = loadPaket;
        function loadPaket(current) {
            vm.currentPage = current;
            var offset = (vm.currentPage * vm.pageSize) - vm.pageSize;
            UIControlService.loadLoading(loadmsg);
            DataVHSService.DetailDocUpload({
                Status: vm.ID,
                Offset: offset,
                Limit: vm.pageSize
            },
                   function (response) {
                       UIControlService.unloadLoading();
                       if (response.status == 200) {
                           vm.DataDocList = response.data.List;
                           vm.totalItems = Number(response.data.Count);
                       }
                   },
                   function (response) {
                       UIControlService.unloadLoading();
                   });
        }
        
        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }

        vm.back = back;
        function back() {
            $uibModalInstance.close();
        }
    }
})();
