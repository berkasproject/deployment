﻿(function () {
    'use strict';

    angular.module("app").controller("UploadPriceCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'UploadFileConfigService', 'ExcelReaderService', 'GlobalConstantService', 'UploaderService'];
    /* @ngInject */
    function ctrl($state, $stateParams, item, $http, $filter, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, UploadFileConfigService, ExcelReaderService, GlobalConstantService, UploaderService) {

        var vm = this;
        var loadmsg = "LOADING";
        vm.currentPage = 1;
        vm.datalempar = item;
        console.info(item);
        vm.fileUpload;
        vm.maxSize = 10;
        vm.DocName = "";
        vm.currentPage = 1;
        vm.listItemVHS = [];
        vm.totalItems = 0;
        vm.keyword = "";
        vm.ID = Number($stateParams.id);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.NegoId = vm.datalempar.NegoId;

        var id = vm.datalempar.id;
        vm.id = vm.datalempar.id;
        var negoid = vm.datalempar.negoid;
        vm.negoid = vm.datalempar.negoid;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadTypeSizeFile();
            loadData();
        }
        function loadData(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            DataVHSService.DetailDoc({
                Status: vm.negoid,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.column

            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.VHSdata = data;
                    vm.listItemVHS = data.List;
                    console.info("negoid:" + JSON.stringify(vm.negoid));

                    vm.AdditionalValue = vm.datalempar.AdditionalValue;
                    vm.BudgetContract = vm.datalempar.BudgetContract;
                    vm.AddendumCode = vm.datalempar.AddendumCode;
                    vm.TypeAddendum = vm.datalempar.TypeAddendum;
                    vm.VHSAwardId = vm.datalempar.VHSAwardId;
                    vm.StartDate = vm.datalempar.StartDate;
                    vm.EndDate = vm.datalempar.EndDate;
                    vm.Duration = vm.datalempar.Duration;
                    vm.Requestor = vm.datalempar.Requestor;
                    vm.RequestDate = vm.datalempar.RequestDate;
                    vm.DocUrl = vm.datalempar.DocUrl;
                    vm.DocName = vm.datalempar.DocName;
                    vm.VendorID = vm.datalempar.VendorID;
                    vm.TenderStepID = vm.datalempar.TenderStepID;
                    vm.Remask = vm.datalempar.Remask;
                    vm.NegoId = vm.datalempar.NegoId;

                    vm.totalItems = Number(data.Count);
                    console.info("StartDate Uplaod:" + JSON.stringify(vm.StartDate));

                } else {
                    $.growl.error({ message: "MESSAGE.ERR_LOAD" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        function loadTypeSizeFile() {
            UIControlService.loadLoading("LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.ADMIN.MASTER.ITEMPR", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoading();
                return;
            });
        }
        /*count of property object*/
        function numAttrs(obj) {
            var count = 0;
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    ++count;
                }
            }
            return count;
        }
        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }



        //start upload
        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                UIControlService.msg_growl("error", "MSG_NOFILE");
                return;
            }

            if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
            }
        }

        function upload(file, config, filters, dates, callback) {
            //console.info(file);
            var size = config.Size;

            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UIControlService.loadLoading("LOADING");
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.pathFile = vm.folderFile + url;
                        saveExcelContent(fileName, url);
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

        }

        function saveExcelContent(fileName, url) {
            UIControlService.loadLoading(vm.message);
            ExcelReaderService.readExcel(vm.fileUpload,
                function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        var excelContents = reply.data;
                        var Sheet1 = excelContents[Object.keys(excelContents)[0]]; //untuk baca nama sheet
                        var countproperty = numAttrs(Sheet1[0]);
                        //console.info("excel:" + JSON.stringify(Sheet1[0]) + numAttrs(Sheet1[0]));

                        vm.MaterialCode;
                        vm.Refrigeran;
                        vm.ItemDescrip;
                        vm.Manufacture;
                        vm.PartNo;
                        vm.Estimate;
                        vm.Unit;
                        vm.Currency;
                        vm.CountryOfOrigin;
                        vm.PriceIDR;
                        vm.Remark;
                        vm.Code;
                        //   vm.ColPOType;

                        for (var i = 1; i <= countproperty ; i++) {
                            if (Sheet1[0]['Column' + i] === 'MaterialCode') { vm.MaterialCode = i; }
                            if (Sheet1[0]['Column' + i] === 'Refrigeran') { vm.Refrigeran = i; }
                            if (Sheet1[0]['Column' + i] === 'ItemDescrip') { vm.ItemDescrip = i; }
                            if (Sheet1[0]['Column' + i] === 'Manufacture') { vm.Manufacture = i; }
                            if (Sheet1[0]['Column' + i] === 'PartNo') { vm.PartNo = i; }
                            if (Sheet1[0]['Column' + i] === 'Estimate') { vm.Estimate = i; }
                            if (Sheet1[0]['Column' + i] === 'Unit') { vm.Unit = i; }
                            if (Sheet1[0]['Column' + i] === 'Currency') { vm.Currency = i; }
                            if (Sheet1[0]['Column' + i] === 'CountryOfOrigin') { vm.CountryOfOrigin = i; }
                            if (Sheet1[0]['Column' + i] === 'PriceIDR') { vm.PriceIDR = i; }
                            if (Sheet1[0]['Column' + i] === 'Remark') { vm.Remark = i; }
                            if (Sheet1[0]['Column' + i] === 'Code') { vm.Code = i; }

                            if (!(vm.MaterialCode === undefined) &&
                                !(vm.Refrigeran === undefined) &&
                                !(vm.ItemDescrip === undefined) &&
                                !(vm.Manufacture === undefined) &&
                                !(vm.PartNo === undefined) &&
                                !(vm.Estimate === undefined) &&
                                !(vm.Unit === undefined) &&
                                !(vm.Currency === undefined) &&
                                !(vm.CountryOfOrigin === undefined) &&
                                !(vm.PriceIDR === undefined) &&
                                !(vm.Remark === undefined) &&
                                !(vm.Code === undefined)
                                ) {
                                break;
                            }
                        }
                        //cek jika ada kolom excel tidak sesuai format
                        if ((vm.MaterialCode === undefined) || (vm.Refrigeran === undefined) ||
                                (vm.ItemDescrip === undefined) || (vm.Manufacture === undefined) || (vm.PartNo === undefined) ||
                               (vm.Estimate === undefined) || (vm.Unit === undefined) ||
                               (vm.Currency === undefined) || (vm.CountryOfOrigin === undefined) ||
                               (vm.PriceIDR === undefined) || (vm.Remark === undefined) ||
                               (vm.Code === undefined)
                            ) {
                            UIControlService.msg_growl("warning", "Gagal");
                            return;
                        }
                        vm.newExcel = []
                        for (var a = 1; a < Sheet1.length; a++) {
                            var objExcel = {
                                MaterialCode: Sheet1[a]['Column' + vm.MaterialCode],
                                Refrigeran: Sheet1[a]['Column' + vm.Refrigeran],
                                ItemDescrip: Sheet1[a]['Column' + vm.ItemDescrip],
                                Manufacture: Sheet1[a]['Column' + vm.Manufacture],
                                PartNo: Sheet1[a]['Column' + vm.PartNo],
                                Estimate: Sheet1[a]['Column' + vm.Estimate],
                                Unit: Sheet1[a]['Column' + vm.Unit],
                                Currency: Sheet1[a]['Column' + vm.Currency],
                                CountryOfOrigin: Sheet1[a]['Column' + vm.CountryOfOrigin],
                                PriceIDR: Sheet1[a]['Column' + vm.PriceIDR],
                                Remark: Sheet1[a]['Column' + vm.Remark],
                                Code: Sheet1[a]['Column' + vm.Code]

                            };

                            vm.newExcel.push(objExcel);
                        }
                        uploadSave(fileName, url, vm.newExcel);
                    }
                });
        }

        vm.tglSekarang = UIControlService.getDateNow("");
        function uploadSave(filename, url, detail) {

            if (!vm.RequestDate) {
                UIControlService.msg_growl("error", "Lengkapi Request Date Yang Kosong");
                UIControlService.unloadLoading();
                return;
            }
            var data = {
                AddendumCode: vm.AddendumCode,
                TypeAddendum: vm.TypeAddendum,
                AdditionalValue: vm.AdditionalValue,
                RequestDate: vm.RequestDate,
                DocUrl: url,
                DocName: filename,
                StartDate: vm.StartDate,
                EndDate: vm.EndDate,
                Requestor: vm.Requestor,
                VendorID: vm.VendorID,
                TenderStepID: vm.TenderStepID,
                VHSAwardId: vm.VHSAwardId,
                Remask: vm.Remask,
                Duration: vm.Duration,
                NegoId: vm.NegoId,
                DetailAddendum: detail,
            }
            console.info("data:" + JSON.stringify(data));

            console.info("dataUplode:" + JSON.stringify(detail));
            DataVHSService.CreateAddendumDetail(data, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    //console.info("hasil:" + JSON.stringify(reply));
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPLOAD");
                    $uibModalInstance.close();
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_API");
                UIControlService.unloadLoadingModal();
            });
        }



    }
})();
