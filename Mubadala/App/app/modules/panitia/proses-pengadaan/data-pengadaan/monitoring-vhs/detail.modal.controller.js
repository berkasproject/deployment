﻿(function () {
    'use strict';

    angular.module("app").controller("DetailAddendumCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService) {

        var vm = this;
        vm.fileUpload;
        vm.datalempar = item.datalempar;
        var loadmsg = "MESSAGE.LOADING";
        vm.currentPage = 1;
        vm.DocName = "";
        vm.DocumentUrl = "",
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;
        vm.maxSize = 10;
        vm.DetailDocList = [];
        vm.init = init;

        vm.AddendumId = vm.datalempar.AddendumId;
        vm.VendorName = vm.datalempar.VendorName;
        vm.AdditionalValue = vm.datalempar.AdditionalValue;
        vm.RFQCode = vm.datalempar.RFQCode;
        vm.Budget_Val = vm.datalempar.BudgetContract;
        vm.StartDate = vm.datalempar.StartDate;
        vm.EndDate = vm.datalempar.EndDate;
        vm.SpendingVal = vm.datalempar.SpendingVal;
        vm.Duration = vm.datalempar.Duration;
        vm.Requestor = vm.datalempar.Requestor;
        vm.RequestDate = vm.datalempar.RequestDate;
        vm.TitleDoc = vm.datalempar.TitleDoc;
        vm.StartContractDate = vm.datalempar.StartContractDate;
        vm.FinishContractDate = vm.datalempar.FinishContractDate;
        vm.DocUrl = vm.datalempar.DocUrl;
        vm.DocName = vm.datalempar.DocName;
        vm.VendorID = vm.datalempar.VendorID;
        vm.TenderStepID = vm.datalempar.TenderStepID;
        vm.Remask = vm.datalempar.Remask;
        var id = vm.datalempar.id;
        vm.id = vm.datalempar.id;
        var negoid = vm.datalempar.negoid;
        vm.negoid = vm.datalempar.negoid;

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadPaket(1);
        };

        vm.loadPaket = loadPaket;
        function loadPaket() {
            UIControlService.loadLoading(loadmsg);
            DataVHSService.DetailAddendum({
                Status: vm.AddendumId,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.column
            },
                   function (response) {
                       if (response.status == 200) {
                           var DataDocList = response.data;
                           vm.DataDocList = DataDocList;
                           vm.DetailDocList = response.data.List;
                           UIControlService.unloadLoading();

                           vm.totalItems = Number(vm.DataDocList.Count);
                           UIControlService.unloadLoading();
                           console.info("AdditionalValue :" + JSON.stringify(vm.AdditionalValue));
                       } else {
                           UIControlService.handleRequestError(response.data);
                       }
                   },
                   function (response) {
                       UIControlService.handleRequestError(response.data);
                       UIControlService.unloadLoading();
                   });
        }

        vm.save = save;
        function save() {

        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss();
        };

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
    }
})();
