﻿(function () {
    'use strict';

    angular.module("app").controller("AddVHSNewItemCtrl", ctrl);

    ctrl.$inject = ['$uibModalInstance','$state', '$stateParams', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataVHSService', 'UIControlService', 'GlobalConstantService', 'UploaderService', 'UploadFileConfigService'];
    /* @ngInject */
    function ctrl($uibModalInstance, $state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataVHSService, UIControlService, GlobalConstantService, UploaderService, UploadFileConfigService) {

        var vm = this;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('add-adendum');
            loadCurrencies(1);
            loadCountry(1);

        };
        vm.save = save;
        function save() {
            $uibModalInstance.close(vm.data);
        }

        vm.loadCurrencies = loadCurrencies;
        function loadCurrencies() {
            DataVHSService.Currency(
                function (response) {
                    if (response.status == 200) {
                        vm.currencyList = response.data;
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }
        function loadCountry() {
            DataVHSService.Country(
                function (response) {
                    if (response.status == 200) {
                        vm.NegaraList = response.data;
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');s
        }



    }
})();
