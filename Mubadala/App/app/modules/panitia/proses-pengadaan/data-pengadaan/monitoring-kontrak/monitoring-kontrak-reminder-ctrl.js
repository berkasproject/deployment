﻿(function () {
	'use strict'

	angular.module("app").controller("contractMonitoringReminderCtrl", ctrl)

	ctrl.$inject = ['UIControlService', 'CtrReminderService', '$translatePartialLoader']
	function ctrl(UIControlService, CtrReminderService, $translatePartialLoader) {
		var vm = this
		vm.currentPage = 1
		vm.keyword = ""
		vm.pageSize = 10
		vm.selectedColumn = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('contract-reminder')
			jLoad(1)
		}
		
		vm.initVHS = initVHS
		function initVHS() {
			$translatePartialLoader.addPart('contract-reminder')
			jLoadVHS(1)
		}

		vm.jLoad = jLoad
		function jLoad(currPage) {
			var offset = (currPage * 10) - 10
			UIControlService.loadLoading("MESSAGE.LOADING")
			CtrReminderService.getReminder({
				Keyword: vm.keyword,
				Offset: offset,
				Limit: vm.pageSize,
				FilterType: vm.selectedColumn
			}, function (response) {
				UIControlService.unloadLoading()
				if (response.status === 200) {
					vm.reminderList = response.data.List;
					vm.totalItems = Number(response.data.Count)
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
				UIControlService.unloadLoading()
			});
		}

		vm.jLoadVHS = jLoadVHS
		function jLoadVHS(currPage) {
			var offset = (currPage * 10) - 10
			UIControlService.loadLoading("MESSAGE.LOADING")
			CtrReminderService.getReminderVHS({
				Keyword: vm.keyword,
				Offset: offset,
				Limit: vm.pageSize,
				FilterType: vm.selectedColumn
			}, function (response) {
				UIControlService.unloadLoading()
				if (response.status === 200) {
					vm.reminderList = response.data.List;
					vm.totalItems = Number(response.data.Count)
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.GETREMINDER_FAILED")
				UIControlService.unloadLoading()
			});
		}

	}
})()