﻿(function () {
    'use strict';

    angular.module("app")
    .controller("aturTahapanAllModalController", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService, UIControlService, GlobalConstantService) {

        var vm = this;

        vm.stepDatas = [];
        vm.isCalendarOpened = [];

        var dateNow = new Date();
        var timezone = dateNow.getTimezoneOffset();
        var timezoneClient = timezone / 60;

        vm.init = init;
        function init() {

            UIControlService.loadLoadingModal("");
            DataPengadaanService.GetStep({
                TenderRefID: item.TenderRefID,
                ProcPackageType: item.ProcPackType,
                ID: item.TenderID
            }, function (reply) {

                vm.stepDatas = reply.data;

                if (vm.stepDatas.length > 0) {
                    vm.namaLelang = vm.stepDatas[0].tender.TenderCode + ' - ' + vm.stepDatas[0].tender.TenderName;
                }

                vm.stepDatas.forEach(function (stepItem) {
                    stepItem.StartDate = new Date(Date.parse(stepItem.StartDate));
                    stepItem.EndDate = new Date(Date.parse(stepItem.EndDate));
                    vm.isCalendarOpened.push(false); //calendar startdate
                    vm.isCalendarOpened.push(false); //calendar enddate
                });

                //angular detectBrowser
                var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
                var isChrome = !!window.chrome && !isOpera; // Chrome 1+

                if (isChrome === true) {
                    var objappVersion = navigator.appVersion; var objAgent = navigator.userAgent; var objbrowserName = navigator.appName; var objfullVersion = '' + parseFloat(navigator.appVersion); var objBrMajorVersion = parseInt(navigator.appVersion, 10); var objOffsetName, objOffsetVersion, ix;
                    // In Chrome 
                    if ((objOffsetVersion = objAgent.indexOf("Chrome")) != -1) { objbrowserName = "Chrome"; objfullVersion = objAgent.substring(objOffsetVersion + 7); }
                    // trimming the fullVersion string at semicolon/space if present 
                    if ((ix = objfullVersion.indexOf(";")) != -1) objfullVersion = objfullVersion.substring(0, ix);
                    if ((ix = objfullVersion.indexOf(" ")) != -1) objfullVersion = objfullVersion.substring(0, ix); objBrMajorVersion = parseInt('' + objfullVersion, 10);
                    if (isNaN(objBrMajorVersion)) { objfullVersion = '' + parseFloat(navigator.appVersion); objBrMajorVersion = parseInt(navigator.appVersion, 10); };
                    //console.info("versi full:" + objfullVersion);
                    //console.info("versi major:" + objBrMajorVersion);

                    vm.stepDatas.forEach(function (stepItem) {

                        if (objBrMajorVersion >= 58) {
                            stepItem.StartDate = new Date(stepItem.StartDate.setHours(stepItem.StartDate.getHours()));
                            stepItem.EndDate = new Date(stepItem.EndDate.setHours(stepItem.EndDate.getHours()));

                            stepItem.StartDate.setSeconds(0);
                            stepItem.StartDate.setMilliseconds(0);
                            stepItem.EndDate.setSeconds(0);
                            stepItem.EndDate.setMilliseconds(0);
                        }
                        else if (objBrMajorVersion <= 57) {
                            stepItem.StartDate = new Date(stepItem.StartDate.setHours(stepItem.StartDate.getHours() + timezoneClient));
                            stepItem.EndDate = new Date(stepItem.EndDate.setHours(stepItem.EndDate.getHours() + timezoneClient));
                            
                            stepItem.StartDate.setSeconds(0);
                            stepItem.StartDate.setMilliseconds(0);
                            stepItem.EndDate.setSeconds(0);
                            stepItem.EndDate.setMilliseconds(0);
                        }
                    });
                }

                UIControlService.unloadLoadingModal();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
            });
        };

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }

        vm.checkStartDate = checkStartDate;
        function checkStartDate(startdate, index) {
            var today = moment().format("YYYY-MM-DD");
            var convertedStartDate = moment(startdate).format("YYYY-MM-DD");
            if (today > convertedStartDate) {
                vm.stepDatas[index].StartDate = new Date();
                vm.stepDatas[index].StartDate.setHours(0);
                vm.stepDatas[index].StartDate.setMinutes(0);
                vm.stepDatas[index].StartDate.setSeconds(0);
                vm.stepDatas[index].StartDate.setMilliseconds(0);
            }
        }

        vm.save = save;
        function save() {

            var param = [];

            for(var i = 0; i < vm.stepDatas.length; i++) {
                
                var step = vm.stepDatas[i];

                if (!step.StartDate || !step.EndDate) {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_DATE');
                    return;
                }
                else if (step.EndDate < step.StartDate) {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_RANGE_DATE');
                    return;
                }

                var startDate = new Date(step.StartDate.getTime());
                var endDate = new Date(step.EndDate.getTime());
                startDate.setHours(startDate.getHours() - timezoneClient);
                endDate.setHours(endDate.getHours() - timezoneClient);

                param.push({
                    ID: step.ID,
                    StartDate: startDate,
                    EndDate: endDate,
                    StepOrder: step.StepOrder
                });
            }

            UIControlService.loadLoadingModal("");
            DataPengadaanService.AturStepAll(param, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", 'MESSAGE.SUCC_SAVE_STEP');
                $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE_STEP');
                if( error.Message === "ERR_STARTDATE_BF_NOW" ||
                    error.Message === "ERR_STARTDATE_NOT_IN_ORDER" ||
                    error.Message === "ERR_ENDDATE_NOT_IN_ORDER") {
                    UIControlService.msg_growl("error", "MESSAGE." + error.Message);
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();