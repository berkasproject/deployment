(function () {
    'use strict';

    angular.module("app")
    .controller("reTenderPilihVendorController", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'PaketPengadaanCEService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, PaketPengadaanCEService) {

        var vm = this;

        var tenderPackID = item.TenderPackID;
        vm.editable = item.editable;

        vm.retenderVendors = [];

        vm.init = init;
        function init() {
            if (vm.editable) {
                UIControlService.loadLoadingModal("");
                PaketPengadaanCEService.GetRegisteredVendors({
                    TenderPackID: tenderPackID
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    vm.retenderVendors = reply.data;
                }, function (error) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                });
            } else {
                UIControlService.loadLoadingModal("");
                PaketPengadaanCEService.GetRetenderVendors({
                    TenderPackID: tenderPackID
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    vm.retenderVendors = reply.data;
                }, function (error) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
                });
            }
        };

        vm.save = save;
        function save() {

            var noSelectedVendor = true;
            var incompleteRemark = false;

            vm.retenderVendors.forEach(function (ven) {
                if (ven.IsRegistered) {
                    noSelectedVendor = false;
                }

                if (!ven.IsRegistered && !ven.Remark) {
                    incompleteRemark = true;
                }
            });

            if (noSelectedVendor) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_SELECTED_VENDOR');
                return;
            }
            if (incompleteRemark) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_INCOMPLETE_REMARKS');
                return;
            }

            $uibModalInstance.close(vm.retenderVendors);
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();