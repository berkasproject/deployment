(function () {
    'use strict';

    angular.module("app")
    .controller("aturPaketPengadaanCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$stateParams', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PaketPengadaanCEService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $stateParams, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, PaketPengadaanCEService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        var contractRequisitionId = Number($stateParams.contractRequisitionId);
        var tenderPackId = Number($stateParams.tenderPackId);
            
        vm.tenderPackageCR = {};

        vm.typeOptions = [];
        vm.schemaOptions = [];
        vm.evalOptions = [];
        vm.mstDocs = [];

        vm.isCalendarStartOpened = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('paket-pengadaan-ce');

            PaketPengadaanCEService.IsAllowEdit({
                ContractRequisitionID: contractRequisitionId
            }, function (reply) {
                vm.isAllowEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });

            PaketPengadaanCEService.IsOERevision({
                ID: tenderPackId
            }, function (reply) {
                vm.isOERevision = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_OE_STATUS');
            });

            loadOptions();
            loadContract();
        };

        function loadContract() {
            UIControlService.loadLoading(loadmsg);
            PaketPengadaanCEService.SelectByID({
                ID: tenderPackId
            }, function (reply) {
                if (reply.data) {
                    UIControlService.unloadLoading();
                    vm.tenderPackageCR = reply.data;
                    if (vm.tenderPackageCR.CELineHasNoOpenQuotation === true) {
                        vm.tenderPackageCR.IsInternalOnly = true;
                    }
                    if (vm.tenderPackageCR.TenderPackageCRSteps) {
                        for (var i = 0; i < vm.tenderPackageCR.TenderPackageCRSteps.length; i++) {
                            var step = vm.tenderPackageCR.TenderPackageCRSteps[i];
                            step.StartDateForm = step.StartDate ? new Date(Date.parse(step.StartDate)) : null;
                            step.EndDateForm = step.EndDate ? new Date(Date.parse(step.EndDate)) : null;
                        }
                    }
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        function loadOptions() {
            PaketPengadaanCEService.GetAllOptions({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                vm.typeOptions = reply.data[0];
                vm.schemaOptions = reply.data[1];
                vm.evalOptions = reply.data[2];
                vm.tenderDocTypes = reply.data[3];
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_OPTIONS');
            });
        }

        vm.loadTahapan = loadTahapan;
        function loadTahapan() {
            UIControlService.loadLoading(loadmsg);
            PaketPengadaanCEService.GetStepsByMethod({
                MethodID: vm.tenderPackageCR.ProcurementMethodID
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.tenderPackageCR.TenderPackageCRSteps = reply.data;
                for (var i = 0; i < vm.tenderPackageCR.TenderPackageCRSteps.length; i++) {
                    var step = vm.tenderPackageCR.TenderPackageCRSteps[i];
                    step.Duration = 0;
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEPS');
            });
        }

        vm.openCalendarStart = openCalendarStart;
        function openCalendarStart() {
            if (vm.tenderPackageCR.VariationIsCancelled || vm.tenderPackageCR.IsPublished || !vm.isAllowEdit || vm.isOERevision) {
                return;
            }
            vm.isCalendarStartOpened = true;
        }

        vm.getEndDate = getEndDate;
        function getEndDate(index) {            

            for (var i = index; i < vm.tenderPackageCR.TenderPackageCRSteps.length; i++) {
                var step = vm.tenderPackageCR.TenderPackageCRSteps[i];
                var nextStep = vm.tenderPackageCR.TenderPackageCRSteps[i + 1];

                var dat = new Date(step.StartDateForm);
                if (step.Duration === null || step.Duration == 0) {
                    dat.setDate(dat.getDate() + 0);
                }
                else {
                    for (var j = 1; j <= step.Duration; j++) {

                        dat.setDate(dat.getDate() + parseInt(1));
                        var dateTemp = dat;
                        var day_dateTemp = dateTemp.getDay();
                        if (day_dateTemp === 6) {
                            dat.setDate(dat.getDate() + 2);
                        }
                        else if (day_dateTemp === 0) {
                            dat.setDate(dat.getDate() + 1);
                        }
                        else {
                            dat.setDate(dat.getDate() + 0);
                        }
                    }
                }
                step.EndDateForm = UIControlService.getStrDate(dat);
                if (nextStep) {
                    //atur startdate
                    dat.setDate(dat.getDate() + 0); //jika startdate tahapan selanjutnya +1hari setelah endate tahapan sebelumnya

                    //set startdate, pengecekan hari kerja
                    var dateTemp = dat;
                    var day_dateTemp = dateTemp.getDay();
                    if (day_dateTemp === 6) {
                        dat.setDate(dat.getDate() + 2);
                    }
                    else if (day_dateTemp === 0) {
                        dat.setDate(dat.getDate() + 1);
                    }

                    nextStep.StartDateForm = UIControlService.getStrDate(dat);
                }
            }
        }

        function saveTenderPackCR(callback) {

            if (vm.tenderPackageCR.IsInternalOnly) {
                if (!vm.tenderPackageCR.TenderType) {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_INCOMPLETE_FIELD');
                    return;
                }
                vm.tenderPackageCR.ProcurementMethodID = null;
                vm.tenderPackageCR.EvalMethodID = null;
                vm.tenderPackageCR.TenderPackageCRSteps = [];
            }
            else {
                if (!vm.tenderPackageCR.TenderType || !vm.tenderPackageCR.ProcurementMethodID || !vm.tenderPackageCR.EvalMethodID) {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_INCOMPLETE_FIELD');
                    return;
                }
                if (vm.tenderPackageCR.TenderPackageCRSteps.length == 0) {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_NO_STEPS');
                    return;
                }
                for (var i = 0; i < vm.tenderPackageCR.TenderPackageCRSteps.length; i++) {
                    var step = vm.tenderPackageCR.TenderPackageCRSteps[i];
                    if (step.IsNeedDocument && (!step.TenderPackageCRStepDocuments || step.TenderPackageCRStepDocuments.length === 0)) {
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_KELENGKAPAN_DOKUMEN');
                        return;
                    }

                    step.StartDate = step.StartDateForm ? UIControlService.getStrDate(step.StartDateForm) : null;
                    step.EndDate = step.EndDateForm ? UIControlService.getStrDate(step.EndDateForm) : null;
                }
            }

            UIControlService.loadLoading(loadmsg);
            PaketPengadaanCEService.SaveTP(vm.tenderPackageCR, function (reply) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("notice", 'MESSAGE.SUCC_SAVE');
                callback();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE." + (error[0] ? error[0] : 'ERR_SAVE'));
            });
        }

        vm.save = save;
        function save() {
            saveTenderPackCR(loadContract);
        }

        function confirmTenderPackCR() {
            UIControlService.loadLoading(loadmsg);
            PaketPengadaanCEService.ConfirmTP(vm.tenderPackageCR, function (reply) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("notice", 'MESSAGE.SUCC_CONFIRM');
                loadContract();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE." + (error[0] ? error[0] : 'ERR_CONFIRM'));
            });
        }

        vm.confirm = confirm;
        function confirm() {
            bootbox.confirm($filter('translate')("CONF_PUBLISH"), function (yes) {
                if (yes) {
                    saveTenderPackCR(confirmTenderPackCR);
                }
            })
        }

        vm.aturDokumen = aturDokumen;
        function aturDokumen(dt) {
            var item = {
                tenderPackageCRStepDocuments: dt.TenderPackageCRStepDocuments,
                tenderDocTypes: vm.tenderDocTypes,
                editable: vm.tenderPackageCR.IsPublished !== true
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/proses-pengadaan/paket-pengadaan-ce/detailKelengkapanTender.modal.html',
                controller: 'detailKelengkapanTenderController',
                controllerAs: 'detKelTenderCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function (result) {
                dt.TenderPackageCRStepDocuments = result
            });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('paket-pengadaan-ce');
        }
    }
})();