(function () {
    'use strict';

    angular.module("app")
    .controller("approveRequisitionListCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'TenderVerificationService', 'UIControlService', 'RequisitionListService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, TenderVerificationService, UIControlService, RequisitionListService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var contractRequisitionId = item.contractRequisitionId;
        var loadmsg = "MESSAGE.LOADING";

        vm.item = item;
        vm.approvalStatus = item.approvalStatus;
        vm.information = "";


        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');
        };

        vm.sendApproval = sendApproval;
        function sendApproval() {
            var confirmMessage = vm.approvalStatus ? "CONFIRM_APPROVE" : "CONFIRM_REJECT";
            bootbox.confirm(($filter('translate')(confirmMessage) + "<br/><br/>" + item.projectTitle), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal(loadmsg);
                    RequisitionListService.SetApprovalStatus({
                        ContractRequisitionId: contractRequisitionId,
                        ApprovalStatus: vm.approvalStatus,
                        Information: vm.information
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            $uibModalInstance.close();
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
                    });
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();