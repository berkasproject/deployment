﻿(function () {
	'use strict';

	angular.module("app").controller("verKDPrintCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'verKDService', '$stateParams', 'UIControlService'];
	/* @ngInject */
	function ctrl($translatePartialLoader, verKDService, $stateParams, UIControlService) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.jumlahlembar = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('verifikasi-kelengkapan-dokumen');
			loadStep();
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.step.tender.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		function loadData() {
			vm.data = [];
			UIControlService.loadLoading("");
			verKDService.select({
				Status: vm.IDTender,
				FilterType: vm.ProcPackType,
				column: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					reply.data.forEach(function (dt) {
						dt.OEDate = UIControlService.convertDateTime(dt.OEDate);
						dt.VerifiedDate = UIControlService.convertDateTime(dt.VerifiedDate);
						vm.data.push(dt);
					});
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function loadStep() {
			UIControlService.loadLoading("");
			verKDService.Step({
				ID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.step = data;
					loadData();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
	}
})()