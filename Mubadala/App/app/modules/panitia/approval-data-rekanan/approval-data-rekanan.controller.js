﻿(function () {
    'use strict';

    angular.module("app")
            .controller("approvalDataRekananController", ctrl);

    ctrl.$inject = ['$timeout', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', '$rootScope', 'ApprovalRekananService', '$stateParams', '$state', 'VerifikasiDataService'];
            /* @ngInject */
    function ctrl($timeout, $http, $uibModal, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, $rootScope, ApprovalRekananService, $stateParams, $state, VerifikasiDataService) {
            var vm = this;

            vm.init = init;
            vm.searchBy = '';
            vm.maxSize = 10;
            //vm.vendorID = $stateParams.vendorID;
            vm.statusName = 'ALL';
            vm.keyword = '';
            vm.currentPage = 1;
            vm.totalItems = 0;
            vm.startDate = new Date();
            vm.endDate = new Date();
            vm.isCalendarOpened = [false, false];
            vm.UIControl = UIControlService;

            vm.openCalendar = openCalendar;
            function openCalendar(index) {
                vm.isCalendarOpened[index] = true;
            };

            function init(){
                $translatePartialLoader.addPart('approval-data-rekanan');
                UIControlService.loadLoading("MESSAGE.LOADING");

                jLoad(1);
            }
            vm.jLoad = jLoad;
            function jLoad(current) {
                vm.currentPage = current;
                var offset = (current * 10) - 10;

                ApprovalRekananService.selectVendorVerified({
                    Offset: offset,
                    Limit: vm.maxSize,
                    Keyword: vm.searchBy,
                    Keyword2: vm.keyword,
                    Keyword3: vm.statusName,
                    Date1: vm.startDate,
                    Date2: vm.endDate
                }, function (reply) {
                    //console.info("datane:" + JSON.stringify(reply));
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        var data = reply.data;
                        vm.approvalData = data.List;
                        vm.totalItems = data.Count;
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    //console.info("error:" + JSON.stringify(err));
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    UIControlService.unloadLoading();
                });
            }

            vm.prosesVerifikasi = prosesVerifikasi;
            function prosesVerifikasi(VendorID) {
                $state.transitionTo('proses-verifikasi2', { id: VendorID, stats: 1 });
            }

            vm.approvedOrNot = approvedOrNot;
            function approvedOrNot(dataApproval, stats) {
                //var swal = {
                //    titleNotification: $filter('translate')('SWAL.NOTIFICATION'),
                //    textNotActivated: $filter('translate')('SWAL.NOTACTIVATED'),
                //    textCivd: $filter('translate')('SWAL.TEXTCIVD')
                //}

                var data = {
                    dataApproval: dataApproval,
                    stats:stats
                }

                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/approval-data-rekanan/approval-data-modal.html',
                    controller: 'ApprovalDataModalController',
                    controllerAs: 'approvalDataModalCtrl',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    init();
                });
                    
            }

            vm.listApprover = listApprover;
            function listApprover(data) {
                var data = {
                    item: data
                }
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/approval-data-rekanan/approval-detail-modal.html',
                    controller: 'ApprovalDetailModalCtrl',
                    controllerAs: 'approvalDetailModalCtrl',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                //console.info("okeee");
                modalInstance.result.then(function () {
                    jLoad(1)
                });
            }

    }
})();