﻿(function () {
    'use strict';

    angular.module("app").controller("ApprovalDetailModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', 'ApprovalRekananService','$stateParams'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, ApprovalRekananService,$stateParams) {

        var vm = this;
        
        vm.init = init;
        vm.maxSize = 10;
        vm.vendorID = item.item.VendorID;
        vm.UIControl = UIControlService;
        vm.currentPage = 1;
        vm.totalItems = 0;

        function init() {
            console.log('Init')
            jLoad(1)
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            ApprovalRekananService.selectDataApprover({
                Offset: offset,
                Limit: vm.maxSize,
                Keyword: vm.searchBy,
                IntParam1: vm.vendorID
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataApprover = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();