
/*(function () {
    'use strict';

    angular.module("app")
    .controller("approvalDDViewEvaluationResultCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DeliveryDateApprovalService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, DeliveryDateApprovalService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;

        vm.evalRes = [];
        vm.tenderName = item.tenderName;

        vm.init = init;
        function init() {
            loadItem();
        };       

        vm.loadItem = loadItem;
        function loadItem(){
            UIControlService.loadLoadingModal("");
            DeliveryDateApprovalService.getResults({
                GoodsOfferEvaluationId: item.goodsOfferEvaluationId,
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.evalRes = reply.data;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_EVAL_RES'));
            });
        }

        vm.awardedItem = awardedItem;
        function awardedItem(evalVendor) {
            var item = {
                tenderName: vm.tenderName,
                vendorName: evalVendor.VendorName,
                goodsOfferEvaluationVendorId: evalVendor.ID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang-approval-delivery-date/viewItemPRAward.modal.html',
                controller: 'approvalDDViewAwardedItemPRCtrl',
                controllerAs: 'prAwardCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
*/