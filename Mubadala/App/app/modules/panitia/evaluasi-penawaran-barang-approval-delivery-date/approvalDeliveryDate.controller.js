﻿(function () {
    'use strict';

    angular.module("app").controller("ApprovalDeliveryDateController", ctrl);

    ctrl.$inject = ['$filter', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'DeliveryDateApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService,
        DeliveryDateApprovalService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.keyword = "";
        vm.list = [];
        vm.count = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-penawaran-barang');
            jLoad();
        }

        vm.onSearch = onSearch;
        function onSearch(keyword) {
            vm.keyword = keyword;
            vm.currentPage = 1;
            jLoad();
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("");
            DeliveryDateApprovalService.select({
                Keyword: vm.keyword,
                Limit: vm.pageSize,
                Offset: (vm.currentPage - 1) * vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.list = reply.data.List;
                vm.count = reply.data.Count;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DD_APPROVAL");
                UIControlService.unloadLoading();
            });
        }

        vm.approve = approve;
        function approve(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVE_DD'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    DeliveryDateApprovalService.approve({
                        Id: data.Id,
                        //Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APP_DD'));
                        jLoad();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_APP_DD'));
                    });
                }
            });
        };

        /*
        vm.evalResult = evalResult;
        function evalResult(data) {
            var item = {
                tenderName: data.TenderName,
                goodsOfferEvaluationId: data.GoodsOfferEvaluationId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang-approval-delivery-date/viewEvaluationResult.modal.html',
                controller: 'approvalDDViewEvaluationResultCtrl',
                controllerAs: 'evalResCtrl',
                resolve: { item: function () { return item; } }
            });
        };
        */

        vm.awardedItem = awardedItem;
        function awardedItem(data) {
            var item = {
                approvalId: data.Id,
                tenderName: data.TenderName,
                vendorName: data.VendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang-approval-delivery-date/viewItemPRAward.modal.html',
                controller: 'approvalDDViewAwardedItemPRCtrl',
                controllerAs: 'prAwardCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.reject = reject;
        function reject(data) {
            var item = {
                Id : data.Id,
                tenderCode : data.TenderCode,
                tenderName : data.TenderName,
                vendorName : data.VendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-penawaran-barang-approval-delivery-date/detailApprovalDeliveryDate.modal.html',
                controller: 'detailApprovalDeliveryDateModalCtrl',
                controllerAs: 'detAppDDCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function () {
                init();
            });
        };

    }
})();


