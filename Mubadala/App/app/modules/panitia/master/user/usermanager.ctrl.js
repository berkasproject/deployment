﻿(function () {
	angular.module("app").controller("UserManagerCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'UIControlService', 'AuthService', '$stateParams', '$state'];

	function ctrl($translatePartialLoader, UIControlService, AuthService, $stateParams, $state) {
		var vm = this;
		vm.EmpID = $stateParams.EmpID;
		vm.EmpName = '';
		vm.AvailRoles = [];
		vm.Roles = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('user-manager');

			UIControlService.loadLoading("MESSAGE.LOADING");
			AuthService.getUserData({
				EmployeeID: vm.EmpID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.EmpName = reply.data.EmployeeName;
					vm.AvailRoles = reply.data.AvailRoles;
					vm.Roles = reply.data.Roles;
					vm.Username = reply.data.Username;
					vm.UserID = reply.data.ID;
					vm.Password = reply.data.Password;
					vm.confirmPasswd = reply.data.Password;
					vm.ValidUntil = new Date(reply.data.ValidUntil);
				} else {
					UIControlService.msg_growl("error", 'MESSAGE.GETUSER_ERROR', "MESSAGE.GETUSER_ERROR");
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.addRole = addRole;
		function addRole() {
			if (vm.selectedAvailRole === null)
				return false;

			vm.Roles.push(vm.selectedAvailRole);
			vm.AvailRoles = AuthService.remove(vm.AvailRoles, vm.selectedAvailRole);
		}

		vm.removeRole = removeRole;
		function removeRole() {
			if (vm.selectedRole === null) return false;

			vm.AvailRoles.push(vm.selectedRole);
			vm.Roles = AuthService.remove(vm.Roles, vm.selectedRole);
		}

		vm.openCalendar = openCalendar;
		function openCalendar() {
			vm.isCalendarOpened = true;
		};

		vm.toMasterEmployee = toMasterEmployee;
		function toMasterEmployee() {
			$state.transitionTo('pegawai', {});
		}

		vm.saveUser = saveUser;
		function saveUser() {
			if (vm.Password === null || vm.Password === '') {
				return false;
			}
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			AuthService.saveUser({
				Username: vm.Username,
				Password: vm.Password,
				Roles: vm.Roles,
				EmployeeID: vm.EmpID,
				ID: vm.UserID,
				ValidUntil: vm.ValidUntil
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'MESSAGE.SUCC_SAVE', 'MESSAGE.NOTIF_SUCC');
					toMyAccount();
				} else {
					UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE', "MESSAGE.NOTIF_ERR");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", err.Message);
			});
		}
	}
})();