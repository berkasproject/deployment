﻿(function () {
	'use strict';

	angular.module("app").controller("FormBeritaCtrl", ctrl);

	ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location',
        'NewsService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService'];
	function ctrl(item, $http, $translate, $translatePartialLoader, $location, NewsService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance, UploadFileConfigService, UploaderService) {

		var vm = this;
		vm.init = init;
	    //tanggal
		var currentDate = new Date();
		var bln = currentDate.getMonth() + 1;
		var tgl = currentDate.getDate();
		vm.tglSekarang = UIControlService.getDateNow("");
		vm.act = item.act;

		if (bln < 10) {
		    bln = "0" + bln;
		} else {
		    bln = bln;
		}

		if (tgl < 10) {
		    tgl = "0" + tgl;
		} else {
		    tgl = tgl;
		}

		vm.newsDate = currentDate.getFullYear() + "-" + bln + "-" + tgl;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		function init() {
		    //console.info("item" + JSON.stringify(item));
		    if (item.act == 2) {
		        vm.newsid = item.newsID;
		        vm.newsTitle = item.newsTitle;
		        vm.newsContent = item.newsContent;
		        vm.IsActive = item.IsActive;
		        vm.IsLocal = item.isLocal;
		        vm.IsNational = item.isNational;
		        vm.IsInternational = item.isInternational;
		        vm.IsOpen = item.isOpen;
		        vm.newsDate = item.newsDate;
		        vm.docUrl = item.DocUrl;
		        vm.docSize = item.DocSize;
            }
			UIControlService.loadLoadingModal("Silahkan Tunggu...");
			//loadOpsiViewer();
			cfgFile();

		}


		function cfgFile() {
		    UIControlService.loadLoadingModal("MESSAGE.LOADING");
		    //get tipe dan max.size file - 1
		    UploadFileConfigService.getByPageName("PAGE.ADMIN.NEWS", function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            vm.list = response.data;
		            vm.idUploadConfigs = vm.list;
		            //console.info("cfgfile" + JSON.stringify(vm.list));
		            //for (var i = 0; i < vm.list.length; i++) {
		            //    if (vm.list[i].Name == "xls" || vm.list[i].Name == "xlsx" || vm.list[i].Name == "pdf" || vm.list[i].Name == "docx" || vm.list[i].Name == "doc") vm.idUploadConfigs.push(vm.list[i]);
                    //
		            //}
		            vm.idFileTypes = UIControlService.generateFilterStrings(vm.idUploadConfigs);
		            //console.info("idfiletypes" + JSON.stringify(vm.idFileTypes));
		            vm.idFileSize = vm.idUploadConfigs[0];

		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
		        UIControlService.unloadLoadingModal();
		        return;
		    });
		}

		function loadOpsiViewer() {
		    //alert("load");
		    vm.listviewer = [];
		    NewsService.getOpsiViewer(function (reply) {
		        UIControlService.unloadLoadingModal();
		        vm.listviewer = reply.data.List;
		        //console.info("listOpsi2" + JSON.stringify(vm.listviewer));
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.deleteNews = deleteNews;
		function deleteNews() {
		    vm.docUrl = null;
		    vm.docSize = null;
        }

		vm.save = save;
		function save() {
		    UIControlService.loadLoadingModal("");
		    if (vm.fileUpload) {
		        savedocs();
		    } else {
		        saveNews(vm.docUrl, vm.docSize);
		    }
		}
        
		function savedocs() {
		    if (vm.fileUpload === undefined) {
		        return;
		    }
		    else {
		        if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);

		        }
		    }
		}

		function upload(file, config, filters, dates) {
		    //console.info("file" + JSON.stringify(file));
		    var size = config.Size;

		    var unit = config.SizeUnitName;
		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		    }

		    UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        var size = response.data.FileLength;
                        saveNews(url, size);
                    } else {
                        UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MSG_ERR_UPLOAD")
                    UIControlService.unloadLoadingModal();
                });

		}
        
		function saveNews(docUrl, docSize) {
		    if (vm.IsLocal == null) {
		        vm.IsLocal = false;
		    }
		    if (vm.IsNational == null) {
		        vm.IsNational = false;
		    }
		    if (vm.IsInternational == null) {
		        vm.IsInternational = false;
		    }
		    if (vm.IsOpen == null) {
		        vm.IsOpen = false;
		    }
		    if (vm.act == 1) {
		        NewsService.create({
		            NewsContent: vm.newsContent,
		            NewsTitle: vm.newsTitle,
		            IsActive: true,
		            IsLocal: vm.IsLocal,
		            IsNational: vm.IsNational,
		            IsInternational: vm.IsInternational,
		            IsOpen: vm.IsOpen,
		            Viewer: 4315,
		            DocUrl: docUrl,
                    DocSize: docSize
		        }, function (reply) {
                    $uibModalInstance.close();
		            UIControlService.unloadLoadingModal();
		        }, function (err) {
		            UIControlService.msg_growl("error", "MSG_ERR_SAVE");
		            UIControlService.unloadLoadingModal();
		        });
		    }
		    else if (vm.act == 2) {
		        //console.info("edit");
		        NewsService.update({
		            NewsID: vm.newsid,
		            NewsContent: vm.newsContent,
		            NewsTitle: vm.newsTitle,
		            IsActive: true,
		            IsLocal: vm.IsLocal,
		            IsNational: vm.IsNational,
		            IsInternational: vm.IsInternational,
		            IsOpen: vm.IsOpen,
		            Viewer: 4315,
		            DocUrl: docUrl,
		            DocSize: docSize
		        }, function (reply) {
		            //console.info("reply: "+JSON.stringify(reply));
                    var newsid = reply.data;
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
		        }, function (err) {
		            UIControlService.msg_growl("error", "MSG_ERR_SAVE");
		            UIControlService.unloadLoadingModal();
		        });
		    }
        }

        /*
		function savenewsdocs(url, filename, newsid) {
		    //console.info("url" + url);
		    //console.info("fn" + filename);
		    //console.info("nid" + newsid);
		    NewsService.savedocs({
		        FileName: filename,
		        DocUrl: url,
		        NewsID: newsid
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            UIControlService.msg_growl("success", "MSG_SUC_SAVE");
		        }
		        else {
		            UIControlService.msg_growl("error", "MSG_ERR_SAVE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		        UIControlService.unloadLoadingModal();
		    });
		}
		*/

		vm.batal = batal;
		function batal() {
			$uibModalInstance.close();
		}

	}
})();
//TODO


