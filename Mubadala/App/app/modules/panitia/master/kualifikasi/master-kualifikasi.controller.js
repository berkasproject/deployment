﻿(function() {
    'use strict';

    angular.module("app").controller("MasterQualificationController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MasterQualificationService',
         'GlobalConstantService', 'UIControlService', '$uibModal', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, MasterQualificationService,
         GlobalConstantService, UIControlService, $uibModal, $filter) {

        var vm = this;
        vm.listAllData = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-kualifikasi');
            jLoad();
        };

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("");
            MasterQualificationService.GetQualifications(function (reply) {
                UIControlService.unloadLoading();
                vm.listAllData = reply.data;
            }, function (err) {
                UIControlService.msg_growl("error", "ERR_LOAD");
                UIControlService.unloadLoading();
            });
        }

        vm.formEdit = formEdit;
        function formEdit(data) {
            var item = {
                ID: data.ID,
                MinAmountInti: data.MinAmountInti,
                MinAmountNonInti: data.MinAmountNonInti,
                QualificationName: data.QualificationName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/kualifikasi/master-kualifikasi.modal.html',
                controller: 'MasterQualificationModalCtrl',
                controllerAs: 'mstQualMCtrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                jLoad();
            });
        };
    }
})();
