﻿(function () {
    'use strict';

    angular.module("app").controller("masterEntityCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'EntityService', '$filter', '$rootScope'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, EntityService, $filter, $rootScope) {
        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 0;
        //vm.maxSize = 10;
        vm.pageSize = 10;
        vm.page_id = 35;
        vm.jLoad = jLoad;
        vm.IsApprovedCR = false;

        //vm.Kata = "";
        vm.VendorID;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-entity');
            jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            EntityService.selectMaster({
                Keyword: vm.keyword,
                Offset: offset,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataEntity = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.tambah = tambah;
        function tambah(data) {
            var data = {
                act: 1,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/entity/tambah-ubah.modal.html',
                controller: "MasterEntityModalCtrl",
                controllerAs: "MasterEntityModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                // window.location.reload();
                vm.init();
            });
        };

        vm.edit = edit;
        function edit(data) {
            var data = {
                act: 0,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/entity/tambah-ubah.modal.html',
                controller: "MasterEntityModalCtrl",
                controllerAs: "MasterEntityModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        };

        vm.aktif = aktif;
        function aktif(data) {
            UIControlService.loadLoading("Loading...");
                    EntityService.AktifkanMaster({
                        EntityID: data.EntityID
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_ACTIVATE');
                            vm.init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_ACTIVATE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_GET_DATA');
                    });
        };

        vm.nonAktif = nonAktif;
        function nonAktif(data) {
            UIControlService.loadLoading("Loading...");
                     EntityService.nonAktifkanMaster({
                         EntityID: data.EntityID
                    }, function (reply2) {
                        if (reply2.status === 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_INACTIVATE');
                            vm.init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_INACTIVATE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_GET_DATA');
                    });
        };
    }
})();