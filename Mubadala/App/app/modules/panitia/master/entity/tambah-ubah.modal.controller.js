﻿(function () {
    'use strict';

    angular.module("app").controller("MasterEntityModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'EntityService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, EntityService, UIControlService, GlobalConstantService) {
        var vm = this;
        vm.detail = item.item;
        vm.isAdd = item.act;
        vm.action = "";
        vm.entityName = "";
        vm.entityCode = "";
        vm.entityID = 0;
        vm.klik = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-entity');
            if (vm.isAdd === 1) {
                vm.action = "Tambah";
            } else {
                vm.action = "Ubah";
                vm.entityName = vm.detail.EntityName;
                vm.entityCode = vm.detail.EntityCode;
                vm.entityID = vm.detail.EntityID;
            }
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal("Loading...");
            if (vm.klik > 0) {
                return;
            }
            if (vm.entityName == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NAME");
                return;
            }

            if (vm.entityCode == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.CODE");
                return;
            }

            vm.klik++;
            EntityService.saveMaster({
                create: vm.isAdd,
                EntityID: vm.entityID,
                EntityName: vm.entityName,
                EntityCode: vm.entityCode
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS");
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
                } else {
                    vm.klik = 0;
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                vm.klik = 0;
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();