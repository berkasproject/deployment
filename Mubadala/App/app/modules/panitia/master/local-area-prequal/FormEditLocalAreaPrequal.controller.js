﻿(function () {
    'use strict';

    angular.module("app").controller("EditLocalAreaCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'LocalAreaPrequalService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, LocalAreaPrequalService,
		UIControlService, item, $uibModalInstance) {
        var vm = this;
        //console.info("masuuk modal : " + JSON.stringify(item));
        vm.isAdd = item.act;
        vm.action = "";
        vm.regionID = 0;
        vm.countryID = "";
        vm.provinceID = "";
        vm.listDistrict = [];
        vm.listLocalArea = [];
        vm.selectedDistrict = null;
        vm.name = "";

        vm.init = init;
        function init() {
            if (vm.isAdd == 0) {
                vm.name = item.item.Name;
                vm.listLocalArea = item.item.listDistrict;
            }
            else if (vm.isAdd == 2) {
                vm.name = item.item.Name;
                vm.listLocalArea = item.item.listDistrict;
            }

            selectAllDistrict();
        }

        vm.selectAllDistrict = selectAllDistrict;
        function selectAllDistrict() {
            LocalAreaPrequalService.selectAllDistrict(function (reply) {
                if (reply.status === 200) {
                    vm.listDistrict = reply.data;
                }
                else {
                    UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses Api!!");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.changeDistrict = changeDistrict;
        function changeDistrict() {
            if (vm.selectedDistrict == null) {
                UIControlService.msg_growl("error", "Kecamatan belum dipilih");
                return;
            }
            else {
                console.info(vm.selectedDistrict);
                for(var i=0; i< vm.listLocalArea.length; i++){
                    if (vm.selectedDistrict.DistrictID == vm.listLocalArea[i].DistrictID) {
                        UIControlService.msg_growl("error", "Kecamatan tersebut sudah pernah dipilih");
                        return;
                    }
                    else if ((vm.listLocalArea.length - 1) == i) {
                        vm.listLocalArea.push(vm.selectedDistrict);
                        vm.selectedDistrict = null;
                    }
                };
                if (vm.listLocalArea.length == 0) {
                    vm.listLocalArea.push(vm.selectedDistrict);
                    vm.selectedDistrict = null;
                }
            }
        }

        vm.deleteRow = deleteRow;
        function deleteRow(index) {
            var idx = index;
            var _length = vm.listLocalArea.length; // panjangSemula
            vm.listLocalArea.splice(idx, 1);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        };

        vm.simpan = simpan;
        function simpan() {
            if (vm.name == "")
            {
                UIControlService.msg_growl("error", "Nama area tidak boleh kosong");
                return;
            }
            else if(vm.listLocalArea.length == 0) {
                UIControlService.msg_growl("error", "Daftar kecamatan tidak boleh kosong");
                return;
            }
            else {
                UIControlService.loadLoadingModal("Silahkan Tunggu...");
                LocalAreaPrequalService.insert({
                    ID: vm.isAdd == 1 ? 0 : item.item.ID,
                    Name: vm.name,
                    listDistrict: vm.listLocalArea
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "Berhasil Menambahkan data!!");
                        $uibModalInstance.close();
                    }
                    else {
                        return;
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });

            }
        }


    }
})();