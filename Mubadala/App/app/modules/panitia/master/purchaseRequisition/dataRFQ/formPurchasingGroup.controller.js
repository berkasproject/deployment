﻿(function () {
    'use strict';

    angular.module("app").controller("PRPGRCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PurchaseRequisitionService', '$state', 'UIControlService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PurchReqService,
        $state, UIControlService, item, $uibModalInstance) {
        var vm = this;
        vm.textSearch = "";
        vm.colSearch = 1;
        vm.maxSize = 10;
        //console.info("item:" + JSON.stringify(item));
        vm.dataPR = item;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("purchase-requisition");
            vm.loadPGR(1);
        }

        vm.onSearchSubmit = onSearchSubmit;
        function onSearchSubmit(textSearch) {
            vm.textSearch = textSearch;
            vm.loadPGR(1);
        };

        vm.listPGR = [];
        vm.totalItems = 0;
        vm.loadPGR = loadPGR;
        function loadPGR(current) {
            UIControlService.loadLoadingModal('LOADING');
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            PurchReqService.selectPurchasingGroups({
                Keyword: vm.textSearch, Column: vm.colSearch, Limit: vm.maxSize, Offset: offset
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                //console.info("material:" + JSON.stringify(reply));
                if (reply.status === 200) {
                    vm.listPGR = reply.data.List;
                    vm.totalItems = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", "ERR_LOAD_PGR_CODE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_LOAD_PGR_CODE");
            });
        }

        vm.pilihPGR = pilihPGR;
        vm.senddata = [];
        function pilihPGR(data) {
            var data = {
                ID: vm.dataPR.ID,
                PurchGroup: data.PgrCode
            };
            vm.senddata.push(data);
            $uibModalInstance.close(vm.senddata);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();