﻿(function() {
    'use strict';

    angular.module("app").controller("EditEmailCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', 'item', '$uibModalInstance', 'SocketService', 'UIControlService', 'KontenEmailService'];
    function ctrl($http, $translate, $translatePartialLoader, item, $uibModalInstance, SocketService, UIControlService, KontenEmailService) {

        var vm = this;

        vm.konten_email = {};
        vm.variabel_email_globals = {};
        vm.variabel_email_lokals = {};

        vm.init = init;
        function init() {
            vm.konten_email = item;
            loadVariables();
        };

        vm.onCancelClick = onCancelClick;
        function onCancelClick() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.onSimpanClick = onSimpanClick;
        function onSimpanClick() {
            if (!vm.konten_email.EmailSubject || !vm.konten_email.EmailContent1) {
                UIControlService.msg_growl("error", "ERR_INCOMPLETE");
                return;
            }

            UIControlService.loadLoadingModal("LOADING");
            KontenEmailService.update(vm.konten_email, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", "SUCC_SAVE_EMAIL");
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", "ERR_SAVE_EMAIL");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_SAVE_EMAIL");
            });
        };

        /*
        vm.customTinymce = {
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor"
            ],
            toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            toolbar2: "forecolor backcolor",
            image_advtab: true,
            height: "200px",
            width: "auto"
        };
        */

        function loadVariables() {
            UIControlService.loadLoadingModal("LOADING");
            KontenEmailService.selectLocalVariable({
                ID: item.ID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.variabel_email_lokals = reply.data;
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_LOAD_VARIABLES");
            });
        };
    }
})();