﻿(function () {
	'use strict'

	angular.module("app").controller("delegationCtrl", ctrl)

	ctrl.$inject = ['$uibModal', 'UIControlService', 'delegationService', '$translatePartialLoader']
	function ctrl($uibModal, UIControlService, delegationService, $translatePartialLoader) {
		var vm = this
		vm.keyword = ""
		vm.pageSize = 10
		vm.currentPage = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('master-delegation')
			getDelegation(1)
		}

		vm.getDelegation = getDelegation
		function getDelegation(current) {
			var offset = (current * 10) - 10
			UIControlService.loadLoading("MESSAGE.LOADING")
			delegationService.getDelegation({
				Keyword: vm.keyword,
				Offset: offset,
				Limit: vm.pageSize
			}, function (response) {
				UIControlService.unloadLoading()
				if (response.status === 200) {
					vm.doaList = response.data.List;
					vm.totalItems = Number(response.data.Count)
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETDELEGATION_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.GETDELEGATION_FAILED")
				UIControlService.unloadLoading()
			});
		}

		vm.addDelegation = addDelegation;
		function addDelegation() {
			var item = {}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/delegation/addDelegation.html',
				controller: 'addDelegationCtrl',
				controllerAs: 'addDelegationCtrl',
				resolve: { item: function () { return item; } }
			})

			modalInstance.result.then(function () {
				getDelegation(vm.currentPage);
			})
		}
	}
})()