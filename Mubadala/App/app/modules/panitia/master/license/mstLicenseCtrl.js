﻿(function () {
	'use strict'

	angular.module("app").controller("mstLicenseCtrl", ctrl)

	ctrl.$inject = ['$uibModal', 'UIControlService', '$translatePartialLoader', 'mstLicenseService']
	function ctrl($uibModal, UIControlService, $translatePartialLoader, mstLicenseService) {
		var vm = this
		vm.keyword = ""
		vm.pageSize = 10
		vm.currentPage = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('master-license')
			getLicense(1)
		}

		vm.inactivate = inactivate;
		function inactivate(id) {
			UIControlService.loadLoading('MESSAGE.LOADING');
			mstLicenseService.inactivate({ ID: id }, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", "MESSAGE.SUC_INACTIVATE");
					getLicense(vm.currentPage);
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.FAIL_INACTIVATE");
					getLicense(vm.currentPage);
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.FAIL_INACTIVATE");
				getLicense(vm.currentPage);
			});
		}

		vm.activate = activate;
		function activate(id) {
			UIControlService.loadLoading('MESSAGE.LOADING');
			mstLicenseService.inactivate({ ID: id }, function (reply) {
				if (reply.status === 200) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("success", "MESSAGE.SUC_ACTIVATE");
					getLicense(vm.currentPage);
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.FAIL_ACTIVATE");
					getLicense(vm.currentPage);
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "MSG_FAIL_ACTIVATE");
				getLicense(vm.currentPage);
			});
		}

		vm.getLicense = getLicense
		function getLicense(current) {
			var offset = (current * 10) - 10
			UIControlService.loadLoading("MESSAGE.LOADING")
			mstLicenseService.getLicense({
				Keyword: vm.keyword,
				Offset: offset,
				Limit: vm.pageSize
			}, function (response) {
				UIControlService.unloadLoading()
				if (response.status === 200) {
					vm.licenseList = response.data.List;
					vm.totalItems = Number(response.data.Count)
				} else {
					UIControlService.msg_growl("error", "MESSAGE.GETLICENSE_FAILED")
					UIControlService.unloadLoading()
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.GETLICENSE_FAILED")
				UIControlService.unloadLoading()
			});
		}

		vm.addLicense = addLicense;
		function addLicense() {
			var item = {
				ID: 0,
				Name: ''
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/license/addLicense.html',
				controller: 'addLicenseCtrl',
				controllerAs: 'addLicenseCtrl',
				resolve: { item: function () { return item; } }
			})

			modalInstance.result.then(function () {
				getLicense(vm.currentPage);
			})
		}

		vm.editLicense = editLicense;
		function editLicense(id, name) {
			var item = {
				ID: id,
				Name: name
			}

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/master/license/addLicense.html',
				controller: 'addLicenseCtrl',
				controllerAs: 'addLicenseCtrl',
				resolve: { item: function () { return item; } }
			})

			modalInstance.result.then(function () {
				getLicense(vm.currentPage);
			})
		}
	}
})()