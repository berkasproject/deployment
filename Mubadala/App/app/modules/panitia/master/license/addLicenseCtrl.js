﻿(function () {
	'use strict'

	angular.module("app").controller("addLicenseCtrl", ctrl)

	ctrl.$inject = ['$uibModal', 'UIControlService', 'mstLicenseService', '$uibModalInstance', 'item']
	function ctrl($uibModal, UIControlService, mstLicenseService, $uibModalInstance, item) {
		var vm = this
		vm.Name = item.Name
		vm.ID = item.ID

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.close();
		}

		vm.init = init
		function init() {
		}

		vm.saveLicense = saveLicense
		function saveLicense() {
			if (vm.Name == null || vm.Name == null) {
				UIControlService.msg_growl("warning", "MESSAGE.WARN_LICENSENAME")
				return
			}

			if (item.ID !== 0) {
				mstLicenseService.editLicense({
					Name: vm.Name,
					ID: vm.ID
				}, function (reply) {
					UIControlService.unloadLoading()
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SAVE_SUCCESS")
						$uibModalInstance.close()
					} else {
						UIControlService.msg_growl("error", "MESSAGE.SAVE_FAILED")
						UIControlService.unloadLoading()
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.SAVE_FAILED")
					UIControlService.unloadLoading()
				})
			}
			else {
				mstLicenseService.saveLicense({
					Name: vm.Name,
				}, function (reply) {
					UIControlService.unloadLoading()
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SAVE_SUCCESS")
						$uibModalInstance.close()
					} else {
						UIControlService.msg_growl("error", "MESSAGE.SAVE_FAILED")
						UIControlService.unloadLoading()
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.SAVE_FAILED")
					UIControlService.unloadLoading()
				})
			}
		}
	}
})()