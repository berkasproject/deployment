﻿(function () {
	'use strict';

	angular.module("app").controller("departemenModalCtrl", ctrl);

	ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'DepartemenService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance'];

	function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, DepartemenService,
		RoleService, UIControlService, item, $uibModalInstance) {
		var vm = this;
		//console.info("masuuk modal : " + JSON.stringify(item));
		vm.isAdd = item.act;
		vm.action = "";
		vm.regionID = 0;
		vm.countryID = "";
		vm.provinceID = "";
		
		vm.init = init;
		function init() {
			//console.info("isAdd"+vm.isAdd);
		    if (vm.isAdd === 1) {
		        vm.action = "Tambah";
		    }
		    else {
		        vm.action = "Ubah ";
		        vm.kode_depart = item.item.DepartmentCode;
		        vm.nama_depart = item.item.DepartmentName;
		        vm.id_depart = item.item.DepartmentID;
		    }
		}

		vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
		};

		vm.simpan = simpan;
		function simpan() {
		    //console.info(vm.kode_depart + "-" + vm.nama_depart + "-" + vm.ket_depart);
		    if (vm.kode_depart === "" || vm.kode_depart === null) {
		        alert($filter('translate')('MESSAGE.NO_CODE'));
		        return;
		    }

		    if (vm.nama_depart === "" || vm.nama_depart === null) {
		        alert($filter('translate')('MESSAGE.NO_NAME'));
		        return;
		    }
		    cekKodeNama();
		}

	    //proses simpan
		vm.prosesSimpan = prosesSimpan;
        function prosesSimpan(){
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
		    if (vm.isAdd === 1) {
		        DepartemenService.insert({
		            DepartmentCode: vm.kode_depart, DepartmentName: vm.nama_depart
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
		                $uibModalInstance.close();
		            }
		            else {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            UIControlService.unloadLoadingModal();
		        });
		    }
		    else {
		        DepartemenService.update({
		            DepartmentCode: vm.kode_depart, DepartmentName: vm.nama_depart, DepartmentID: vm.id_depart
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                UIControlService.msg_growl("success", "MESSAGE.SUCC_UPDATE");
		                $uibModalInstance.close();
		            }
		            else {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_UPDATE");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            UIControlService.unloadLoadingModal();
		        });
		    }
		}

		vm.cekKodeNama = cekKodeNama;
		function cekKodeNama() {
		    //pengecekkan kode atau nama sudah ada belum?
		    DepartemenService.cekData({
		        column: 1, Keyword: vm.kode_depart
		    }, function (reply) {
		        //console.info("cek1:" + JSON.stringify(reply));
		        if (reply.status === 200 && reply.data.length > 0) {
		            UIControlService.msg_growl("warning", "MESSAGE.ERR_CODE");
		            return;
		        }
		        else if (reply.status === 200 && reply.data.length <= 0) {
		            DepartemenService.cekData({
		                column: 2, Keyword: vm.nama_depart
		            }, function (reply2) {
		                //console.info("cek2:" + JSON.stringify(reply2));
		                if (reply2.status === 200 && reply2.data.length > 0) {
		                    UIControlService.msg_growl("warning", "MESSAGE.ERR_NAME");
		                    return;
		                }
		                else if (reply2.status === 200 && reply2.data.length <= 0) {
		                    prosesSimpan();
		                }
		                else {
		                    UIControlService.msg_growl("error", "MESSAGE.ERR_CHECK");
		                    return;
		                }
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.API");
		                UIControlService.unloadLoading();
		            });
		        }
		        else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_CHECK");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

	}
})();