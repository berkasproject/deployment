﻿(function () {
    'use strict';

    angular.module("app").controller("CatalogueEquipmentModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'CatalogueEquipmentService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
		CatalogueEquipmentService, UIControlService, item, $uibModalInstance) {
        //console.info("masuk modal");
        var vm = this;

        vm.Costs = [];

        vm.init = init;
        function init() {

            if (item.ID) {
                vm.isEdit = true;
                vm.action = "UBAH";
                vm.ID = item.ID;
                vm.Name = item.Name;
		    } else {
                vm.action = "TAMBAH";
            }
        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

		vm.tambahCost = tambahCost;
		function tambahCost() {
		    var newYear = true;
		    for (var i = 0; i < vm.Costs.length; i++) {
		        if (vm.Year === vm.Costs[i].Year) {
		            vm.Costs[i].Cost = vm.Cost;
		            newYear = false;
		            break;
		        }
		    }
		    if (newYear) {
		        vm.Costs.push({
		            Year: vm.Year,
		            Cost: vm.Cost
		        });
		    }
		    vm.Year = null;
		    vm.Cost = null;
		    vm.Costs.sort(function (a, b) {
		        return b.Year - a.Year;
		    });
		};

		vm.hapusCost = hapusCost;
		function hapusCost(index) {
		    vm.Costs.splice(index, 1);
		};

        vm.simpan = simpan;
        function simpan() {

            if(!vm.Name){
                UIControlService.msg_growl("error", "ERR_INCOMPLETE");
                return;
            }

            var saveCatalogue = vm.isEdit ? CatalogueEquipmentService.EditCatalogue : CatalogueEquipmentService.InsertCatalogue;
            UIControlService.loadLoadingModal("");
            saveCatalogue({
                ID : vm.ID,
                Name: vm.Name,
                Costs: vm.Costs
            }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "SUCC_SAVE");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_SAVE");
		    });
        }
    }
})();