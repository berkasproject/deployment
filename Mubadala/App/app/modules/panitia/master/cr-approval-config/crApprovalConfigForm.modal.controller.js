﻿(function () {
    'use strict';

    angular.module("app").controller("CRAppConfigModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$uibModal',
        'CRApprovalConfigService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $uibModal,
		CRApprovalConfigService, UIControlService, item, $uibModalInstance) {
        //console.info("masuk modal");
        var vm = this;

        vm.init = init;
        function init() {

            if (item.ID || item.ID === 0 /*Id data pertama = 0*/) {
                vm.isEdit = true;
                vm.action = "UBAH";
                CRApprovalConfigService.GetById({
                    ID : item.ID
                }, function (reply) {
                    //console.info("data_asoc:" + JSON.stringify(reply));
                    UIControlService.unloadLoading();
                    var data = reply.data;
                    vm.Cfg = data;
                }, function (err) {
                    UIControlService.msg_growl("error", "ERR_LOAD");
                    UIControlService.unloadLoading();
                });
		    } else {
                vm.action = "TAMBAH";
                vm.Cfg = {};
            }
        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

		vm.openPosOrEmployeeSelect = openPosOrEmployeeSelect;
		function openPosOrEmployeeSelect(approvalType) {

		    var data = {
		        level: approvalType
		    };

		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/master/cr-approval-config/selectPosOrEmployee.modal.html',
		        controller: 'selectPosOrEmployeeModalController',
		        controllerAs: 'selPosOrEmpCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (selectedItem) {
		        setApprovalPositionValues(approvalType, selectedItem);
		    });
		}

		vm.clearApprovalPosition = clearApprovalPosition;
		function clearApprovalPosition(approvalType) {

		    var nullItem = {
		        PositionCode: null,
		        PositionName: null,
		        EmployeeID: null,
		        FullName: null,
		    };
		    setApprovalPositionValues(approvalType, nullItem);
		}

		function setApprovalPositionValues(approvalType, item) {
		    switch (approvalType) {
		        case "Finance":
		            vm.Cfg.ApprovalFinancePos = item.PositionCode;
		            vm.Cfg.ApprovalFinancePosName = item.PositionName;
		            vm.Cfg.ApprovalFinanceEmployeeID = item.EmployeeID;
		            vm.Cfg.ApprovalFinanceEmployeeName = item.FullName;
		            break;
		        case "L1":
		            vm.Cfg.ApprovalL1Pos = item.PositionCode;
		            vm.Cfg.ApprovalL1PosName = item.PositionName;
		            vm.Cfg.ApprovalL1EmployeeID = item.EmployeeID;
		            vm.Cfg.ApprovalL1EmployeeName = item.FullName;
		            break;
		        case "L2":
		            vm.Cfg.ApprovalL2Pos = item.PositionCode;
		            vm.Cfg.ApprovalL2PosName = item.PositionName;
		            vm.Cfg.ApprovalL2EmployeeID = item.EmployeeID;
		            vm.Cfg.ApprovalL2EmployeeName = item.FullName;
		            break;
		        case "L3":
		            vm.Cfg.ApprovalL3Pos = item.PositionCode;
		            vm.Cfg.ApprovalL3PosName = item.PositionName;
		            vm.Cfg.ApprovalL3EmployeeID = item.EmployeeID;
		            vm.Cfg.ApprovalL3EmployeeName = item.FullName;
		            break;
		        case "L4":
		            vm.Cfg.ApprovalL4Pos = item.PositionCode;
		            vm.Cfg.ApprovalL4PosName = item.PositionName;
		            vm.Cfg.ApprovalL4EmployeeID = item.EmployeeID;
		            vm.Cfg.ApprovalL4EmployeeName = item.FullName;
		            break;
		    }
		}

        vm.simpan = simpan;
        function simpan() {

            if ((vm.Cfg.ApprovalL4Pos && !vm.Cfg.ApprovalL3Pos) ||
                (vm.Cfg.ApprovalL3Pos && !vm.Cfg.ApprovalL2Pos) ||
                (vm.Cfg.ApprovalL2Pos && !vm.Cfg.ApprovalL1Pos)) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_INVALID_CONFIG");
                return;
            }

            if (!vm.Cfg.BudgetName || !vm.Cfg.ApprovalFinancePos) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_INCOMPLETE");
                return;
            }

            var saveConfig = vm.isEdit ? CRApprovalConfigService.UpdateCfg : CRApprovalConfigService.InsertCfg;
            UIControlService.loadLoadingModal("");
            saveConfig(vm.Cfg, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE_CFG");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_CFG");
		    });
        }
    }
})();