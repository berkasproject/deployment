﻿(function () {
    'use strict';

    angular.module("app")
    .controller("selectPosOrEmployeeModalController", ctrl);
    
    ctrl.$inject = ['$state', 'item', '$scope', '$http', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'CRApprovalConfigService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, item, $scope, $http, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, CRApprovalConfigService, UIControlService) {

        var vm = this;
        var loadmsg = "";
        
        vm.count;
        vm.pageNumber = 1;
        vm.pageSize = 10;
        vm.searchBy = "pos"
        vm.searchText = "";
        vm.list = [];
        vm.level = item.level;

        vm.onBatalClick = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.onSearchSubmit = function (searchText) {
            vm.searchText = searchText;
            vm.pageNumber = 1;
            vm.loadData();
        };

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            vm.loadData();
        };

        vm.loadData = loadData; 
        function loadData() {

            var select = vm.searchBy === 'pos' ? CRApprovalConfigService.SelectPosition : CRApprovalConfigService.SelectEmployee;
            
            UIControlService.loadLoadingModal("");
            select({
                Keyword: vm.searchText,
                Limit: vm.pageSize,
                Offset: (vm.pageNumber - 1) * vm.pageSize,
            }, function (reply) {
                vm.list = reply.data.List;
                vm.count = reply.data.Count;
                UIControlService.unloadLoadingModal();
            }, function (error) {
                UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_LOAD'));
                UIControlService.unloadLoadingModal();
            });
        };

        vm.onSelectClick = onSelectClick;
        function onSelectClick(selected) {
            if (vm.level !== 'Finance' && selected.LevelInfo !== vm.level) {
                UIControlService.msg_growl('error', $filter('translate')('MESSAGE.ERR_INVALID_LEVEL'));
            } else {
                $uibModalInstance.close(selected);
            }
        };
    }
})();