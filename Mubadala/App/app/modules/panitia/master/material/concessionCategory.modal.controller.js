﻿(function () {
    'use strict';

    angular.module("app").controller("mmmConcatCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'MasterMaterialService', '$state', 'UIControlService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        MasterMaterialService, $state, UIControlService, item, $uibModalInstance) {
        var vm = this;
        vm.textSearch = "";
        vm.colSearch = 1;
        vm.maxSize = 10;
        vm.dataPR = item;

        vm.init = init;
        function init() {
            vm.loadConcat(1);
        }

        vm.onSearchSubmit = onSearchSubmit;
        function onSearchSubmit(textSearch) {
            vm.textSearch = textSearch;
            vm.loadConcat(1);
        };

        vm.listConcat = [];
        vm.totalItems = 0;
        vm.loadConcat = loadConcat;
        function loadConcat(current) {
            UIControlService.loadLoadingModal('');
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            MasterMaterialService.SelectConcessions({
                Keyword: vm.textSearch, Column: vm.colSearch, Limit: vm.maxSize, Offset: offset
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.listConcat = reply.data.List;
                vm.totalItems = reply.data.Count;
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_LOAD_CONCESSION_CATEGORIES");
            });
        }

        vm.pilihConcat = pilihConcat;
        vm.senddata = [];
        function pilihConcat(data) {
            $uibModalInstance.close(data);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();