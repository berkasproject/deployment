﻿(function () {
    'use strict';

    angular.module("app").controller("MasterMaterialModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$uibModal',
        'MasterMaterialService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $uibModal,
		MasterMaterialService, UIControlService, item, $uibModalInstance) {
        //console.info("masuk modal");
        var vm = this;

        vm.commodities = [];

        vm.init = init;
        function init() {

            MasterMaterialService.GetAllCommodities(function(reply) {
                vm.commodities = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", "ERR_LOAD_COMMODITIES");
            });

            if (item.Code) {
                vm.isEdit = true;
                vm.action = "UBAH";
                vm.code = item.Code;
                vm.description = item.Description;
                vm.commodityId = item.CommodityID;
                vm.tariffCode = item.TariffCode;
                vm.concessionCategory = item.ConcessionCategory
		    } else {
                vm.action = "TAMBAH";
            }
        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

		vm.openConcessionCategory = openConcessionCategory;
		function openConcessionCategory(data) {
		    var modalInstance = $uibModal.open({
		        templateUrl: 'addConcessionCategory.html',
		        controller: 'mmmConcatCtrl',
		        controllerAs: 'mmmConcatCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (data) {
		        vm.concessionCategory = data.ConcessionCategory;
		    });
		}

		vm.openHarmonizedCode = openHarmonizedCode;
		function openHarmonizedCode(data) {
		    var modalInstance = $uibModal.open({
		        templateUrl: 'addHarmonizedCode.html',
		        controller: 'mmmHarcodCtrl',
		        controllerAs: 'mmmHarcodCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (data) {
		        vm.tariffCode = data.TariffCode
		    });
		}

        vm.simpan = simpan;
        function simpan() {

            if(!vm.code || !vm.description || !vm.commodityId || !vm.tariffCode || !vm.concessionCategory){
                UIControlService.msg_growl("error", "ERR_INCOMPLETE");
                return;
            }

            var saveMaterial = vm.isEdit ? MasterMaterialService.EditMaterial : MasterMaterialService.InsertMaterial;
            UIControlService.loadLoadingModal("");
            saveMaterial({
                Code : vm.code,
                Description: vm.description,
                CommodityID: vm.commodityId,
                TariffCode: vm.tariffCode,
                ConcessionCategory: vm.concessionCategory
            }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "SUCC_SAVE");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_SAVE");
                if (error.Message.substr(0, 4) === "ERR_") {
                    UIControlService.msg_growl("error", error.Message);
                }
		    });
        }
    }
})();