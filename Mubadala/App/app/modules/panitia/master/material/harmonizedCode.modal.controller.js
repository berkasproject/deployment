﻿(function () {
    'use strict';

    angular.module("app").controller("mmmHarcodCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'MasterMaterialService', '$state', 'UIControlService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        MasterMaterialService, $state, UIControlService, item, $uibModalInstance) {
        var vm = this;
        vm.textSearch = "";
        vm.colSearch = 1;
        vm.maxSize = 10;
        vm.dataPR = item;

        vm.init = init;
        function init() {
            vm.loadHarcod(1);
        }

        vm.onSearchSubmit = onSearchSubmit;
        function onSearchSubmit(textSearch) {
            vm.textSearch = textSearch;
            vm.loadHarcod(1);
        };

        vm.listHarcod = [];
        vm.totalItems = 0;
        vm.loadHarcod = loadHarcod;
        function loadHarcod(current) {
            UIControlService.loadLoadingModal('');
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            MasterMaterialService.SelectHarmonizedCodes({
                Keyword: vm.textSearch, Column: vm.colSearch, Limit: vm.maxSize, Offset: offset
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.listHarcod = reply.data.List;
                vm.totalItems = reply.data.Count;
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "ERR_LOAD_HARMONIZED_CODES");
            });
        }

        vm.pilihHarcod = pilihHarcod;
        function pilihHarcod(data) {
            $uibModalInstance.close(data);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();