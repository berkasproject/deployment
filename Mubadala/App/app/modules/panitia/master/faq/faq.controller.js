﻿(function() {
    'use strict';

    angular.module("app")
    .controller("FAQCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MstFAQService',
         'RoleService', 'UIControlService', '$uibModal', 'growl', '$filter'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, MstFAQService,
        RoleService, UIControlService, $uibModal, growl,$filter) {

        var vm = this;
        vm.listFaq = [];
        //vm.jumlahFaq = 0;
        vm.isProcSupport = false;

        vm.txtSearch = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('faq');
            cekRole();
            dataFaq();
        };

        vm.cekRole=cekRole;
        function cekRole(){

            MstFAQService.cekRole(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isProcSupport = reply.data;
                    
                } else {
                    UIControlService.msg_growl("error", "Gagal mendapatkan data role");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "Gagal akses api");
                UIControlService.unloadLoading();
            });
        };


        vm.dataFaq = dataFaq;
        function dataFaq() {

            MstFAQService.dataFaq(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listFaq = reply.data.List;
                    vm.jumlahFaq = reply.data.Count;

                } else {
                    UIControlService.msg_growl("error", "Gagal mendapatkan data FAQ");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "Gagal akses api");
                UIControlService.unloadLoading();
            });
        };


        vm.hapusFaq = hapusFaq;
        function hapusFaq(id) {

            bootbox.confirm($filter('translate')('Apakah anda yakin ingin menghapus data FAQ?'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("Loading..");
                    MstFAQService.deleteFaq({
                        ID: id
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("success", "FAQ berhasil dihapus");
                            UIControlService.unloadLoadingModal();
                            init();
                        }
                        else {
                            UIControlService.msg_growl("error", "Gagal hapus FAQ");
                            return;
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", "Gagal akses api");
                        UIControlService.unloadLoadingModal();
                    });
                }
            });
        }

        vm.formFaq = formFaq;
        function formFaq(data, isAdd) {
            //console.info("itemmodal:" + JSON.stringify(data));
            var data = {
                act: isAdd,
                data: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/faq/faqModal.html',
                controller: 'FormFAQCtrl',
                controllerAs: 'FormFAQCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            //console.info("okeee");
            modalInstance.result.then(function () {
                init();
            });
        };
    };
})();
