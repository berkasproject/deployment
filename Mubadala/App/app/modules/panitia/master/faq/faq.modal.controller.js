﻿(function () {
	'use strict';

	angular.module("app").controller("FormFAQCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'MstFAQService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance'];

	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, MstFAQService,
		RoleService, UIControlService, item, $uibModalInstance) {
		var vm = this;
		//console.info("masuuk modal : " + JSON.stringify(item));
		vm.isAdd = item.act;
		vm.data = item.data;
		vm.Question = '';
		vm.Answer = '';
		
		vm.init = init;
		function init() {
		    if (vm.isAdd === true) {
		        vm.action = "Tambah";
		    }
		    else {
		        vm.action = "Ubah";
		        vm.ID = item.data.ID;
		        vm.Question = item.data.Question;
		        vm.Answer = item.data.Answer;
		        //console.info("item:" + JSON.stringify(item));
		    }
		}


		vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
		};

		vm.simpan = simpan;
		function simpan() {
		    //console.info(vm.kode_depart + "-" + vm.nama_depart + "-" + vm.ket_depart);
		    if (vm.Question == '' || vm.Answer == '' || vm.Answer==" "||vm.Question==" ") {
		        UIControlService.msg_growl("warning", "Pertanyaan dan Jawaban harus diisi");
		        return;
		    }
		    if (vm.isAdd == true) {
		        tambahFaq();
		    }
		    else if (vm.isAdd == false) {
		        editFaq();
		    }
		}

		vm.tambahFaq = tambahFaq;
        function tambahFaq(){
            MstFAQService.insertFaq({
                Question:vm.Question,Answer:vm.Answer
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "FAQ berhasil ditambahkan");
                    $uibModalInstance.close();
                }
                else {
                    UIControlService.msg_growl("error", "Gagal simpan FAQ");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal akses api");
                UIControlService.unloadLoadingModal();
            });
		}

        vm.editFaq = tambahFaq;
        function editFaq() {
            MstFAQService.editFaq({
                ID:vm.ID,Question: vm.Question, Answer: vm.Answer
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "FAQ berhasil diubah");
                    $uibModalInstance.close();
                }
                else {
                    UIControlService.msg_growl("error", "Gagal simpan FAQ");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal akses api");
                UIControlService.unloadLoadingModal();
            });
        }

	}
})();