﻿(function () {
    'use strict';

    angular.module("app").controller("complainTypeModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ComplainTypeService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, ComplainTypeService,
		RoleService, UIControlService, item, $uibModalInstance) {
        var vm = this;
        
        vm.isAdd = item.act;
        vm.action = "";
        vm.regionID = 0;
        vm.countryID = "";
        vm.provinceID = "";

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-complain-type');
            if (vm.isAdd === true) {
                vm.action = "Tambah";
            }
            else {
                vm.action = "Ubah";                
                vm.nama_type = item.item.TypeName;
                vm.id_type = item.item.TypeID;
            }
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.simpan = simpan;
        function simpan() {                      
            if (vm.nama_type === "" || vm.nama_type === null) {
                alert("Jenis pengaduan belum diisi.");
                return;
            }
            cekJenis();
        }
        
        vm.prosesSimpan = prosesSimpan;
        function prosesSimpan() {
            UIControlService.loadLoadingModal($filter('translate')('MESSAGE.LOADING'));
            if (vm.isAdd === true) {
                ComplainTypeService.insert({
                    TypeName: vm.nama_type
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {                        
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SAVE_DATA'));
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DATA'));                        
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));                    
                    UIControlService.unloadLoadingModal();
                });
            }
            else {
                ComplainTypeService.update({
                    TypeName: vm.nama_type, TypeID: vm.id_type
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_UPDATE_DATA'));                        
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_UPDATE_DATA'));                        
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));                  
                    UIControlService.unloadLoadingModal();
                });
            }
        }

        vm.cekJenis = cekJenis;
        function cekJenis() {
                 
        ComplainTypeService.cekData({
            column: 1, Keyword: vm.nama_type
                }, function (reply) {
                    
                if (reply.status === 200 && reply.data.length > 0) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_EXIST_DATA'));                    
                    return;
                }
                else if (reply.status === 200 && reply.data.length <= 0) {
                    prosesSimpan();
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_DATA'));                    
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });            
        }

    }
})();