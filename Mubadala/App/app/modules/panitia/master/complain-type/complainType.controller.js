﻿(function () {
    'use strict';

    angular.module("app").controller("ComplainTypeCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ComplainTypeService', 'RoleService', 'UIControlService', '$uibModal'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, ComplainTypeService,
        RoleService, UIControlService, $uibModal) {

        var vm = this;
        var page_id = 141;

        vm.compType = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.kata = new Kata("");
        vm.init = init;

        
        vm.cariType = cariType;
        vm.jLoad = jLoad;
        
        vm.ubah_aktif = ubah_aktif;
        vm.tambah = tambah;
        vm.edit = edit;

        function init() {
            $translatePartialLoader.addPart('master-complain-type');
            UIControlService.loadLoadingModal($filter('translate')('MESSAGE.LOADING'));
         
            jLoad(1);
                 
        }
    
        vm.cariType = cariType;
        function cariType() {
            vm.jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            
            vm.compType = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            ComplainTypeService.select({
                Offset: offset,
                Limit: vm.pageSize,
                Keyword: vm.kata.srcText
            }, function (reply) {
                
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.compType = data.List;
                    vm.totalItems = Number(data.Count);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DATA'));
                    UIControlService.unloadLoading();
                }
            }, function (err) {                
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            UIControlService.loadLoadingModal($filter('translate')('MESSAGE.LOADING'));
        
            ComplainTypeService.editActive({
                TypeID: data.TypeID, TypeName: data.TypeName, IsActive: active
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var msg = "";
                    if (active === false) msg = "{{'NON_AKTIFKAN'|translate}}";
                    if (active === true) msg = "{{'AKTIFKAN_DATA'|translate}} ";
                    UIControlService.msg_growl("success", "{{'MESSAGE.NOTIF'|translate}} " + msg);
                    jLoad(1);
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHANGE_STATUS'));
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                UIControlService.unloadLoading();
            });

        }

        vm.tambah = tambah;
        function tambah(data, act) {
            console.info("masuk form add/edit");
            var data = {
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/complain-type/formComplainType.html',
                controller: 'complainTypeModalCtrl',
                controllerAs: 'formComplainTypeCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.edit = edit;
        function edit(data, act) {
            console.info("masuk form add/edit");
            var data = {
                act: act,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/complain-type/formComplainType.html',
                controller: 'complainTypeModalCtrl',
                controllerAs: 'formComplainTypeCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }
    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

