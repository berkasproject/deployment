﻿(function () {
    'use strict';

    angular.module("app")
    .controller("level2Ctrl", ctrl);
    
    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MetodeEvaluasiPrakuallService', 'KriteriaEvaluasiService', 'UIControlService', 'item', '$uibModal'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, MetodeEvaluasiPrakuallService, KriteriaEvaluasiService, UIControlService, item, $uibModal) {

        var vm = this;
        var data = item.data;
        var med_id = item.med_id;
        var page_id = item.page_id;
        var detailLama = [];
        var detailBaru = [];
        vm.kriteria = [];
        var loadingMessage = "";
        vm.Lvl1CriteriaName = item.data.CriteriaName;
        vm.sudahDipakai = item.sudahDipakai;
        vm.hasChild = true;
        vm.evaluationMethodTypeName = item.evaluationMethodTypeName;
        vm.isChooseMin1 = false;
        vm.allChecked = false;

        console.info(item);

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('metode-evaluasi');
            $translate.refresh().then(function () {
                loadingMessage = $filter('translate')('MESSAGE.LOADING');
            });

            addOrEdit();
        };

        function addOrEdit() {
            UIControlService.loadLoadingModal();
            MetodeEvaluasiPrakuallService.addOrEditlvl2({
                CriteriaId: item.data.CriteriaId,
                EvaluationMethodId: item.data.EvaluationMethodId,
                EMDId: item.data.EMDId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.isAdd = reply.data;
                    if (vm.isAdd == true) {
                        loadInitAdd();
                    } else {
                        loadInitEdit();
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD");
            }
            );
        }

        function loadInitAdd() {

            MetodeEvaluasiPrakuallService.selectCriteriaLevel2({
                CriteriaId: item.data.CriteriaId,
                EvaluationMethodId: item.data.EvaluationMethodId,
                EMDId: item.data.EMDId
            }, function (reply2) {
                if (reply2.status === 200) {
                    if (reply2.data.length > 0) {
                        var data = reply2.data[0];
                        var allKriteria = reply2.data[0].PrequalCriteriaDescriptions;
                        
                        allKriteria.forEach(function (krit) {

                            var detail = {
                                Id: null,
                                EMDId: data.EMDId,
                                EvaluationMethodId: data.EvaluationMethodId,
                                CriteriaId: krit.CriteriaId,
                                CriteriaName: krit.CriteriaName,
                                Level: krit.Level,
                                ParentId: data.Id,
                                IsActive: false
                            }
                            vm.kriteria.push(detail);
                        });
                    } else {
                        vm.hasChild = false;
                    }
                }
            });
        };

        function loadInitEdit() {

            MetodeEvaluasiPrakuallService.selectCriteriaLevel2Edit({
                CriteriaId: item.data.CriteriaId,
                EvaluationMethodId: item.data.EvaluationMethodId,
                EMDId: item.data.EMDId
            }, function (reply2) {
                if (reply2.status === 200) {
                    if (reply2.data.length > 0) {
                        var data = reply2.data;

                        data.forEach(function (krit) {

                            var detail = {
                                Id: krit.Id,
                                EMDId: krit.EMDId,
                                EvaluationMethodId: krit.EvaluationMethodId,
                                CriteriaId: krit.CriteriaId,
                                CriteriaName: krit.CriteriaName,
                                Level: krit.Level,
                                ParentId: krit.Id,
                                IsActive: krit.IsActive,
                                checked: krit.IsActive
                            }
                            vm.kriteria.push(detail);
                        });

                        //jika semua data checked / isActive = true, maka AllChecked = true
                        vm.angka = 0;
                        for (var i = 0; i < vm.kriteria.length; i++) {
                            if (vm.kriteria[i].IsActive) {
                                vm.angka += 1;
                            }
                        }
                        if (vm.angka == vm.kriteria.length) {
                            vm.allChecked = true;
                        }

                    } else {
                        vm.hasChild = false;
                    }
                }
            });
        };

        vm.detail = detail;
        function detail(data) {
            console.info(data);
            vm.loadLevel3(data);
        }

        vm.loadLevel3 = loadLevel3;
        function loadLevel3(data) {
            console.info(data);

            var lempar = {
                data: data
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/metode-evaluasi-prakualifikasi/metodeEvaluasiLevel3.html',
                controller: 'level3Ctrl',
                controllerAs: 'level3hCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
        }

        //fungsi untuk check semua kriteria
        vm.onCheckAll = onCheckAll;
        function onCheckAll() {
            for (var i = 0; i < vm.kriteria.length; i++) {
                vm.kriteria[i].checked = vm.allChecked;
            }
        }

        vm.simpan = simpan;
        function simpan() {
            
            var detail = [];
            for (var i = 0; i < vm.kriteria.length; i++) {
                console.info(vm.kriteria[i].ParentId)
                vm.kriteria[i].ParentId = vm.kriteria[i].ParentId;
                vm.kriteria[i].IsActive = vm.kriteria[i].checked ? vm.kriteria[i].checked:false;
                detail.push(vm.kriteria[i]);
            }

            //cek minim harus di centang 1 kriteria
            for (var i = 0; i < detail.length; i++) {
                if (detail[i].IsActive == true) {
                    vm.isChooseMin1 = true;
                    break;
                }
            }

            //jika tidak mencentang kriteria sama sekali, tampilkan error
            if (vm.isChooseMin1 == false) {
                UIControlService.msg_growl("error", "Harus memilih minimal 1 kriteria");
                return;
            }

            UIControlService.loadLoadingModal(loadingMessage);
            MetodeEvaluasiPrakuallService.saveDetailCriteriaAboveLevel1(detail,
                function (reply) {
                    if (reply.status === 200) {
                        vm.kriteria = [];
                        if (vm.isAdd == true) {
                            UIControlService.msg_growl('success', "MESSAGE.SUCC_SAVE_CRITERIA_LVL2");
                        } else {
                            UIControlService.msg_growl('success', "MESSAGE.SUCC_EDIT_CRITERIA_LVL2");
                        }
                        
                        UIControlService.unloadLoadingModal();
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl('error', "MESSAGE.FAIL_EDIT_CRITERIA");
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl('error', "MESSAGE.FAIL_EDIT_CRITERIA");
                }
            );
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();