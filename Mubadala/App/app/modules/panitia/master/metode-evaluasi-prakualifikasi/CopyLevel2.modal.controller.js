﻿(function () {
    'use strict';

    angular.module("app")
    .controller("CopyLevel2Ctrl", ctrl);

    ctrl.$inject = ['$http', '$uibModal', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'item', 'UIControlService', 'VPEvaluationMthodService'];
    /* @ngInject */
    function ctrl($http, $uibModal, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, item, UIControlService, VPEvaluationMthodService) {

        var vm = this;
        vm.nama = "";
        vm.originalMethodName = item.originalMethodName;
        vm.originalMethodID = item.originalMethodID;

        vm.init = init;
        function init() {
            
        };
        
        vm.save = save
        function save() {

            if (!vm.nama) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NO_NAME");
                return;
            }

            UIControlService.loadLoadingModal("");
            VPEvaluationMthodService.copyevaltolevel2({
                VPEvaluationMethodId: vm.originalMethodID,
                VPEvaluationMethodName: vm.nama
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_INSERT'));
                $uibModalInstance.close();
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT'));
                UIControlService.unloadLoadingModal();
            });
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();