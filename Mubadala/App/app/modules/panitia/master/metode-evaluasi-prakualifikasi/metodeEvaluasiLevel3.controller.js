﻿(function () {
    'use strict';

    angular.module("app")
    .controller("level3Ctrl", ctrl);
    
    ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MetodeEvaluasiPrakuallService', 'KriteriaEvaluasiService', 'UIControlService', 'item'];
    /* @ngInject */
    function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, MetodeEvaluasiPrakuallService, KriteriaEvaluasiService, UIControlService, item) {

        var vm = this;

        vm.kriteria = [];
        var loadingMessage = "";
        vm.Lvl2CriteriaName = item.data.CriteriaName;
        vm.hasChild = true;
        vm.isChooseMin1 = false;
        vm.allChecked = false;

        console.info(item);

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('metode-evaluasi');
            $translate.refresh().then(function () {
                loadingMessage = $filter('translate')('MESSAGE.LOADING');
            });

            addOrEdit();
            loadTipePengisian();
            
        };

        function addOrEdit() {
            UIControlService.loadLoadingModal();
            MetodeEvaluasiPrakuallService.addOrEditlvl3({
                Id : item.data.Id
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.isAdd = reply.data;
                    if (vm.isAdd == true) {
                        loadInitAdd();
                    } else {
                        loadInitEdit();
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "MESSAGE.ERR_LOAD");
            }
            );
        }

        function loadInitAdd() {

            MetodeEvaluasiPrakuallService.selectCriteriaLevel3({
                CriteriaId: item.data.CriteriaId,
                EvaluationMethodId: item.data.EvaluationMethodId,
                EMDId: item.data.EMDId
            }, function (reply2) {
                if (reply2.status === 200) {
                    if (reply2.data.length > 0) {
                        var data = reply2.data[0];
                        var allKriteria = reply2.data[0].PrequalCriteriaDescriptions;
                        
                        allKriteria.forEach(function (krit) {

                            var detail = {
                                Id: null,
                                EMDId: data.EMDId,
                                EvaluationMethodId: data.EvaluationMethodId,
                                CriteriaId: krit.CriteriaId,
                                CriteriaName: krit.CriteriaName,
                                Level: krit.Level,
                                ParentId: data.Id,
                                FillType: null,
                                DataSource: null,
                                IsActive: false,
                                checked: false
                            }
                            vm.kriteria.push(detail);
                        });

                        if (allKriteria.length == 0) {
                            vm.hasChild = false;
                        }
                    } else {
                        vm.hasChild = false;
                    }
                }
            });
        };

        function loadInitEdit() {

            MetodeEvaluasiPrakuallService.selectCriteriaLevel3Edit({
                Id: item.data.Id
            }, function (reply2) {
                if (reply2.status === 200) {
                    if (reply2.data.length > 0) {
                        var data = reply2.data;

                        data.forEach(function (krit) {

                            var detail = {
                                Id: krit.Id,
                                EMDId: krit.EMDId,
                                EvaluationMethodId: krit.EvaluationMethodId,
                                CriteriaId: krit.CriteriaId,
                                CriteriaName: krit.CriteriaName,
                                Level: krit.Level,
                                ParentId: krit.Id,
                                IsActive: krit.IsActive,
                                FillType: krit.FillType,
                                DataSource: krit.DataSource,
                                checked: krit.IsActive,
                                FillTypeRef: krit.FillTypeRef,
                                DataSourceRef: krit.DataSourceRef
                            }
                            vm.kriteria.push(detail);
                        });
                        console.info(vm.kriteria);
                        //jika semua data checked / isActive = true, maka AllChecked = true
                        vm.angka = 0;
                        for (var i = 0; i < vm.kriteria.length; i++) {
                            if (vm.kriteria[i].IsActive) {
                                vm.angka += 1;
                            }
                        }
                        if (vm.angka == vm.kriteria.length) {
                            vm.allChecked = true;
                        }

                    } else {
                        vm.hasChild = false;
                    }
                }
            });
        };

        function loadTipePengisian() {
            UIControlService.loadLoadingModal();
            MetodeEvaluasiPrakuallService.getTipePengisian(function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.tipePengisian = reply.data;
                    console.info(vm.tipePengisian)
                    loadSumberData();
                }
                else {
                    UIControlService.msg_growl('error', "MESSAGE.ERR_API");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "MESSAGE.ERR_API");
            })
        };
        
        function loadSumberData() {
            UIControlService.loadLoadingModal();
            MetodeEvaluasiPrakuallService.getSumberData(function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.sumberData = reply.data;
                    console.info(vm.sumberData)
                }
                else {
                    UIControlService.msg_growl('error', "MESSAGE.ERR_API");
                    
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "MESSAGE.ERR_API");
            }
            );
        };
        
        vm.change = change;
        function change(data) {
            console.info(data);
        }

        //fungsi untuk check semua kriteria
        vm.onCheckAll = onCheckAll;
        function onCheckAll() {
            for (var i = 0; i < vm.kriteria.length; i++) {
                vm.kriteria[i].checked = vm.allChecked;
            }
        }

        vm.simpan = simpan;
        function simpan() {
            
            var detail = [];
            for (var i = 0; i < vm.kriteria.length; i++) {
                console.info(vm.kriteria[i].ParentId)
                vm.kriteria[i].ParentId = vm.kriteria[i].ParentId;
                vm.kriteria[i].IsActive = vm.kriteria[i].checked ? vm.kriteria[i].checked:false;
                detail.push(vm.kriteria[i]);
            }

            //cek minim harus di centang 1 kriteria
            for (var i = 0; i < detail.length; i++) {
                if (detail[i].IsActive == true) {
                    vm.isChooseMin1 = true;
                    break;
                }
            }

            //jika tidak mencentang kriteria sama sekali, tampilkan error
            if (vm.isChooseMin1 == false) {
                UIControlService.msg_growl("error", "Harus memilih minimal 1 kriteria");
                return;
            }

            console.info(detail)

            UIControlService.loadLoadingModal(loadingMessage);
            MetodeEvaluasiPrakuallService.saveDetailCriteriaAboveLevel1(detail,
                function (reply) {
                    if (reply.status === 200) {
                        vm.kriteria = [];
                        if (vm.isAdd == true) {
                            UIControlService.msg_growl('success', "MESSAGE.SUCC_SAVE_CRITERIA_LVL3");
                        } else {
                            UIControlService.msg_growl('success', "MESSAGE.SUCC_EDIT_CRITERIA_LVL3");
                        }
                        
                        UIControlService.unloadLoadingModal();
                        $uibModalInstance.close();
                    }
                    else {
                        UIControlService.msg_growl('error', "MESSAGE.ERR_API");
                        UIControlService.unloadLoadingModal();
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl('error', "MESSAGE.ERR_API");
                }
            );
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();