﻿(function () {
    'use strict';

    angular.module("app").controller("evalMethodCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$uibModal', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MetodeEvalCriteriaSettingsService', 'MetodeEvaluasiPrakuallService', 'UIControlService', 'VPCPRDataService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $uibModal, $filter, $translate, $translatePartialLoader, $location, SocketService, MetodeEvalCriteriaSettingsService, MetodeEvaluasiPrakuallService, UIControlService, VPCPRDataService) {
        var vm = this;
        var lang = $translate.use();

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.menuhome = 0;
        $scope.my_tree = {};
        vm.page_id = 135;
        vm.keyword = "";
        vm.Type = '';
        vm.isVhsCpr = 0;
        vm.VPEval = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('vpevaluation-method');
            vm.jLoad(1);
            //vm.loadAwal();
            //getRole();
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading()
            vm.currentPage = current;
            MetodeEvaluasiPrakuallService.selectAll({
                Offset: (vm.currentPage - 1) * vm.maxSize,
                Limit: vm.maxSize,
                Keyword: vm.keyword
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.metode = reply.data.List;
                vm.totalItems = reply.data.Count;

            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                UIControlService.unloadLoading();
            });
        };

        vm.loadAwal = loadAwal
        function loadAwal() {
            vm.loadVPEvaluasi();
        };

        function getRole() {
            /*
            VPCPRDataService.cekRole(
            function (reply) {
                if (reply.status === 200) {
                    vm.flagrole = reply.data;
                    vm.FlagRole = vm.flagrole.flagRole;
                    console.info("role: " + vm.FlagRole);
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
            */

            vm.isProcSupp = false;
            MetodeEvaluasiPrakuallService.isprocsupp(function (reply) {
                vm.isProcSupp = reply.data;
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            vm.isPM = false;
            MetodeEvaluasiPrakuallService.ispm(function (reply) {
                vm.isPM = reply.data
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });

            vm.isAllowAdd = false;
            MetodeEvaluasiPrakuallService.isallowadd(function (reply) {
                vm.isAllowAdd = reply.data
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
        }
        vm.lihatDetail = lihatDetail;
        function lihatDetail(EvaluationMethodId) {
            var kirim = {
                EvaluationMethodId: EvaluationMethodId
            };
            $uibModal.open({
                templateUrl: 'app/modules/panitia/master/metode-evaluasi-prakualifikasi/ViewDetailEvaluasi.html',
                controller: 'veiwDetailEVP',
                controllerAs: 'veiwDetailEVP',
                resolve: {
                    item: function () {
                        return kirim;
                    }
                }
            });
        };

        vm.loadVPEvaluasi = loadVPEvaluasi
        function loadVPEvaluasi() {
            var offset = (vm.currentPage - 1) * vm.maxSize;
            MetodeEvaluasiPrakuallService.selectVPEvaluasi({
                Keyword: vm.srcText,
                Offset: offset,
                Limit: vm.maxSize
            },
            function (reply) {
                if (reply.status === 200) {
                    vm.VPEval = reply.data.List;
                    vm.totalItems = reply.data.Count;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        function getType() {
            MetodeEvalCriteriaSettingsService.getType(
                function (reply) {
                    if (reply.status === 200) {
                        vm.type = reply.data;
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
        };

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(metode, isactive) {
            var pesan = "";

            switch (lang) {
                case 'id': pesan = 'Anda yakin untuk ' + (metode.IsActive ? 'menonaktifkan' : 'mengaktifkan') + ' Metode Evaluasi : "' + metode.EvaluationMethodName + '"?'; break;
                default: pesan = 'Are you sure to ' + (metode.IsActive ? 'deactivate' : 'activate') + ' : "' + metode.EvaluationMethodName + '" Evaluation Method?'; break;
            }

            bootbox.confirm(pesan, function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    MetodeEvaluasiPrakuallService.switchActive({
                        EvaluationMethodId: metode.EvaluationMethodId,
                        IsActive: isactive
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            if (isactive == 1) {
                                //berhasil aktif
                                UIControlService.msg_growl("notice", "MESSAGE.SUCC_ACTIVATION");
                            } else {
                                //berhasil non aktif
                                UIControlService.msg_growl("notice", "MESSAGE.SUCC_DEACTIVATION");
                            }
                            
                            init();
                        } else {
                            UIControlService.msg_growl("error", "MESSAGE.ERR_TOGGLE_ACTIVATION");
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_TOGGLE_ACTIVATION'));
                    });
                }
            })
        };

        vm.publish = publish;
        function publish(metode) {

            UIControlService.loadLoading("");
            MetodeEvaluasiPrakuallService.isLevel3Complete({
                EvaluationMethodId: metode.EvaluationMethodId,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.data === true) {

                    var pesan = "";
                    switch (lang) {
                        case 'id': pesan = 'Apakah Anda yakin untuk menggunakan Metode Evaluasi "' + metode.EvaluationMethodName + '" ?'; break;
                        default: pesan = 'Are you sure to publish "' + metode.EvaluationMethodName + '" Evaluation Method ?'; break;
                    }

                    bootbox.confirm(pesan, function (yes) {
                        if (yes) {
                            UIControlService.loadLoading("");
                            MetodeEvaluasiPrakuallService.publish({
                                EvaluationMethodId: metode.EvaluationMethodId,
                            }, function (reply) {
                                UIControlService.unloadLoading();
                                if (reply.status === 200) {
                                    UIControlService.msg_growl("notice", "MESSAGE.SUCC_PUBLISH");
                                    vm.loadVPEvaluasi();
                                } else {
                                    UIControlService.msg_growl("error", "MESSAGE.ERR_PUBLISH");
                                }
                            }, function (err) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_PUBLISH'));
                            });
                        }
                    })
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_LEVEL3_INCOMPLETE");
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.ERR_CHECK_LEVEL3");
            });
        }


        vm.addEvaluasiVP = addEvaluasiVP;
        function addEvaluasiVP() {
            $state.transitionTo('add-metode-evaluasi-prakual', { id: 0, type: 0 });
        };

        vm.updateEvaluasiVP = updateEvaluasiVP;
        function updateEvaluasiVP(VPEvaluationMethodId, isVhsCpr) {
            $state.transitionTo('add-metode-evaluasi-prakual', {
                id: VPEvaluationMethodId,
                type: isVhsCpr
            });
        };

        vm.copyLevel2 = copyLevel2;
        function copyLevel2(metode) {
            var item = {
                originalMethodName : metode.VPEvaluationMethodName,
                originalMethodID: metode.VPEvaluationMethodId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/metode-evaluasi-prakualifikasi/CopyLevel2.modal.html',
                controller: 'CopyLevel2Ctrl',
                controllerAs: 'copyLv2Ctrl',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadVPEvaluasi();
            });
        }

        vm.cari = cari;
        function cari(srcText) {
            vm.keyword = srcText;
            vm.currentPage = 1;
            vm.jLoad();
        };
    }
})();

