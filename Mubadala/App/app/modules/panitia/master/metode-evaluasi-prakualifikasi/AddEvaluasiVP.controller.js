﻿(function () {
    'use strict';

    angular.module("app").controller("AddEvalMethodPrakCtrl", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$scope', '$http', '$uibModal', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'MetodeEvalCriteriaSettingsService', 'UIControlService', 'MetodeEvaluasiPrakuallService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $scope, $http, $uibModal, $filter, $translate, $translatePartialLoader, $location, SocketService, MetodeEvalCriteriaSettingsService, UIControlService, MetodeEvaluasiPrakuallService) {
        var vm = this;
        var lang;
        var EvaluationMthodId = Number($stateParams.id);
        var vhsorcpr = Number($stateParams.type);

        vm.isVhsCpr = vhsorcpr;
        vm.isEdit = EvaluationMthodId > 0;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.menuhome = 0;
        $scope.my_tree = {};
        vm.page_id = 135;
        vm.level = 1;
        vm.srcText = "";
        vm.Type = '';
        vm.evalById = [];
        vm.allChecked = false;
        vm.isChooseMin1 = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('vpevaluation-method');
            if (vm.isEdit) {
                vm.kriteriaList = [];
                
                MetodeEvalCriteriaSettingsService.getbyid({
                    EvaluationMethodId: Number($stateParams.id)
                },
                function (reply) {
                    if (reply.status === 200) {
                        vm.evalById = reply.data;
                        console.info(vm.evalById);
                        vm.namaMetode = vm.evalById.EvaluationMethodName;
                        vm.ambiltipe = String(vm.evalById.TypeRef.Name);
                        if (vm.evalById.PrequalEvaluationMethodDetails.length > 0) {
                            vm.allChecked = true;
                            for (var i = 0; i < vm.evalById.PrequalEvaluationMethodDetails.length; i++) {
                                console.info(vm.evalById.PrequalEvaluationMethodDetails[i].PrequalEvaluationMDCriterias[0])
                                console.info(vm.evalById.PrequalEvaluationMethodDetails[i].PrequalEvaluationMDCriterias[0].CriteriaId)
                                vm.kriteriaList.push({
                                    EMDId: vm.evalById.PrequalEvaluationMethodDetails[i].EMDId,
                                    CriteriaId: vm.evalById.PrequalEvaluationMethodDetails[i].PrequalEvaluationMDCriterias[0].CriteriaId,
                                    CriteriaName: vm.evalById.PrequalEvaluationMethodDetails[i].EvalMethodDetail,
                                    checked: vm.evalById.PrequalEvaluationMethodDetails[i].IsActive,
                                    EvaluationMethodId: vm.evalById.PrequalEvaluationMethodDetails[i].EvaluationMethodId
                                    //bobot: vm.evalById.PrequalEvaluationMethodDetails[i].Weight
                                });
                                if (!vm.evalById.PrequalEvaluationMethodDetails[i].IsActive) {
                                    vm.allChecked = false;
                                };
                            }
                            getType();
                            filterKriteriaList();
                        }
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
            }
            else {
                getType();
            }
        };

        function loadType(data) {
            console.info(data);
            var tipe = data.Name;
            var id = data.IdTypeReff;


            vm.srcText = "";
            vm.currentPage = 1;
            
            vm.kriteriaList = [];
            MetodeEvaluasiPrakuallService.selectCriteriaLevel1({
                Keyword: "",
                Level: 1,
                ParentId: null,
                IdTypeRef: id
            }, function (reply) {
                if (reply.status === 200) {
                    var allKriteria = reply.data;
                    for (var i = 0; i < allKriteria.length; i++) {
                        vm.kriteriaList.push({
                            CriteriaId: allKriteria[i].CriteriaId,
                            CriteriaName: allKriteria[i].CriteriaName,
                            checked: false,
                            ParentId: allKriteria[i].ParentId,
                            Level: allKriteria[i].Level,
                            IdTypeRef: allKriteria[i].IdTypeRef,
                            IsOptionStandard: allKriteria[i].IsOptionStandard
                        });
                    }

                    vm.kriteriaList.forEach(function (dt) {
                        dt.checked = true;
                    });

                    filterKriteriaList();
                }
            })
        }

        vm.onTypeChange = onTypeChange;
        function onTypeChange() {
            if (vm.Type.Name == 'Nilai' && !vm.isEdit) {
                vm.allChecked = true;
            }
            loadType(vm.Type);
        };

        function getType() {
            MetodeEvalCriteriaSettingsService.getType(
                function (reply) {
                    if (reply.status === 200) {
                        vm.type = reply.data;
                        if (vm.isEdit == true) {
                            for (var i = 0; i < vm.type.length; i++) {
                                if (vm.type[i].IdTypeReff == vm.evalById.TypeRef.IdTypeReff) {
                                    vm.Type = vm.type[i];
                                    break;
                                }
                            }
                        }
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
        };

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText.toLowerCase();
            vm.currentPage = 1;

            filterKriteriaList();
        };

        function filterKriteriaList() {
            vm.kriteriaListPaged = [];
            for (var i = 0; i < vm.kriteriaList.length; i++) {
                if (vm.kriteriaList[i].CriteriaName.toLowerCase().includes(vm.srcText)) {
                    vm.kriteriaListPaged.push(vm.kriteriaList[i]);
                }
            }
            onCheck();
        }

        vm.back = back;
        function back() {
            $state.transitionTo('prequal-master/eval-method');
        };

        vm.onCheck = onCheck;
        function onCheck() {
            vm.allChecked = true;
            for (var i = 0; i < vm.kriteriaListPaged.length; i++) {
                if (!vm.kriteriaListPaged[i].checked) {
                    vm.allChecked = false;
                    break;
                }
            }

            //getType();
        }

        vm.onCheckAll = onCheckAll;
        function onCheckAll() {
            for (var i = 0; i < vm.kriteriaListPaged.length; i++) {
                vm.kriteriaListPaged[i].checked = vm.allChecked;
            }
        }

        vm.detail = detail;
        function detail(data) {
            vm.loadLevel2(data);
        };

        vm.loadLevel2 = loadLevel2;
        function loadLevel2(data) {
            
            var lempar = {
                data: data
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/metode-evaluasi-prakualifikasi/metodeEvaluasiLevel2.html',
                controller: 'level2Ctrl',
                controllerAs: 'level2Ctrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        };

        vm.save = save;
        function save() {

            if (!vm.namaMetode) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_NAME'));
                return;
            }
            if (!vm.Type) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_TYPE'));
                return;
            }

            var noCheckedCrit = true;
            for (var i = 0; i < vm.kriteriaList.length; i++) {
                if(vm.kriteriaList[i].checked === true){
                    noCheckedCrit = false;
                    break
                }
            }
            if (noCheckedCrit) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_SELECTED_CRIT'));
                return;
            }

            var VPMetodeEvaluasi = {
                EvaluationMethodName: vm.namaMetode,
                IdTypeRef: vm.Type.IdTypeReff,
                PrequalEvaluationMethodDetails: []
            }

            if (vm.isEdit) {
                VPMetodeEvaluasi.EvaluationMethodId = vm.evalById.EvaluationMethodId;
            }

            var details = [];
            var detailMDCriteria = [];
            vm.kriteriaList.forEach(function (k) {

                var detail = {
                    Id: k.Id,
                    EMDId: k.EMDId,
                    EvalMethodDetail: k.CriteriaName,
                    Weight: k.bobot,
                    IsActive: k.checked,
                    CriteriaId: k.CriteriaId,
                    CriteriaName: k.CriteriaName,
                    Level: k.Level,
                    ParentId: k.ParentId,
                    IdTypeRef: k.IdTypeRef
                }
                if (k.id > 0) {
                    detail.Id = k.id;
                }
                details.push(detail);
            });

            //cek minim harus di centang 1 kriteria
            for (var i = 0; i < details.length; i++) {
                if (details[i].IsActive == true) {
                    vm.isChooseMin1 = true;
                    break;
                }
            }

            //jika tidak mencentang kriteria sama sekali, tampilkan error
            if (vm.isChooseMin1 == false) {
                UIControlService.msg_growl("error", "Harus memilih minimal 1 kriteria");
                return;
            }

            VPMetodeEvaluasi.PrequalEvaluationMethodDetails = details;

            console.info(VPMetodeEvaluasi);
            UIControlService.loadLoading();
            if (vm.isEdit) {
                //update
                MetodeEvalCriteriaSettingsService.updateEval(VPMetodeEvaluasi,
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "MESSAGE.SUCC_UPDATE");
                            $state.transitionTo('prequal-master/eval-method');
                        }
                        else {
                            UIControlService.msg_growl("error", $filter('translate')("MESSAGE.ERR_UPDATE"));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_UPDATE'));
                    }
                );
            } else {
                //insert
                MetodeEvalCriteriaSettingsService.insertEval(VPMetodeEvaluasi,
                    function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", "MESSAGE.SUCC_INSERT");
                            $state.transitionTo('prequal-master/eval-method');
                        }
                        else {
                            UIControlService.msg_growl("error", $filter('translate')("MESSAGE.ERR_INSERT"));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INSERT'));
                    }
                );
            }
        };
    }
})();

