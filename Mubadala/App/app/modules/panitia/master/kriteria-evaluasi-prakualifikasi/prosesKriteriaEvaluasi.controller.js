(function () {
    'use strict';

    angular.module("app")
    .controller("formKriteriaEvaluasiCtrl", ctrl);
    
    ctrl.$inject = ['$http', '$uibModal', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'KriteriaEvaluasiPrakualifikasiService', 'item', 'UIControlService'];
    /* @ngInject */
    function ctrl($http, $uibModal, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, KriteriaEvaluasiPrakualifikasiService, item, UIControlService) {
        var vm = this;
        vm.placeHolder = item.level === 1 ? 'MODAL.KRITERIA' : 'MODAL.SUB_KRITERIA';
        vm.isEdit = false;
        vm.title = "MODAL.TAMBAH";
        vm.nama = "";
        vm.options = [];
        vm.optionFails = [];
        vm.Criteria = {};
        vm.opsiPenilaian = false;
        var loadingMessage = '';
        vm.Criteria.CriteriaId = item.criteriaId;
        vm.Criteria.ParentId = item.parentId;
        vm.Criteria.Level = item.level;
        vm.Criteria.IdTypeRef = item.IdTypeRef;
        vm.Criteria.IsOptionStandard = item.IsOptionStandard;
        console.info(item);
        console.info(vm.Criteria);
        
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-kriteria-evaluasi');
            $translate.refresh().then(function () {
                loadingMessage = $filter('translate')('MESSAGE.LOADING');
            });
            
            if (vm.Criteria.CriteriaId) {
                vm.isEdit = true;
                vm.title = "MODAL.UBAH";
                vm.Criteria.CriteriaName = item.criteriaName;

                loadDataByID(vm.Criteria.CriteriaId);

            } else {
                if (vm.Criteria.Level == 3) {
                    loadTipeEvaluasi();
                }
            }    
        };

        function loadDataByID() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            KriteriaEvaluasiPrakualifikasiService.selectDataByID({
                CriteriaId: vm.Criteria.CriteriaId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.criteriaById = reply.data[0];
                    vm.Criteria.IsOptionStandard = vm.criteriaById.IsOptionStandard;
                    vm.Criteria.TypeRefName = vm.criteriaById.PrequalEvaluationTypeRef[0].Name;
                    if (vm.Criteria.Level == 3) {
                        loadTipeEvaluasi();
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_TYPE_EVALUASI'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_TYPE_EVALUASI'));
                UIControlService.unloadLoadingModal();
            });
        }

        function loadTipeEvaluasi()
        {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            KriteriaEvaluasiPrakualifikasiService.selectTypeEvaluation(function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.typeEvaluasi = reply.data;
                    if (vm.Criteria.CriteriaId) {
                        for (var i = 0; i < vm.typeEvaluasi.length; i++) {
                            if (vm.typeEvaluasi[i].IdTypeReff == vm.criteriaById.IdTypeRef) {
                                vm.chooseType = vm.typeEvaluasi[i];
                            }
                        }
                    }

                    if (vm.Criteria.Level == 3) {
                        if (vm.Criteria.CriteriaId) {
                            if (vm.Criteria.TypeRefName == 'Nilai') {
                                loadOption();
                            }
                            if (vm.Criteria.TypeRefName == 'Pass Fail') {
                                loadOptionFail();
                            }
                        } else {
                            loadOption();
                        }
                    }
                    
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_TYPE_EVALUASI'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_TYPE_EVALUASI'));
                UIControlService.unloadLoadingModal();
            });
        }

        function loadOption() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            KriteriaEvaluasiPrakualifikasiService.getOptions({
                criteriaId: item.criteriaId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.options = reply.data;
                    //vm.options.forEach(function (data) {
                    //    if (data.IsActive == true) {
                    //        vm.Criteria.IsOptionStandard = true;
                    //    }
                    //})
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        }

        function loadOptionFail() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            KriteriaEvaluasiPrakualifikasiService.getOptions({
                criteriaId: item.criteriaId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.optionFails = reply.data;
                    //vm.options.forEach(function (data) {
                    //    if (data.IsActive == true) {
                    //        vm.Criteria.IsOptionStandard = true;
                    //    }
                    //})
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        }

        vm.changeTipeEvaluasi = changeTipeEvaluasi;
        function changeTipeEvaluasi() {
            vm.Criteria.IdTypeRef = vm.chooseType.IdTypeReff;
            if (vm.chooseType.Name == 'Pass Fail' && vm.optionFails.length <= 2) {
                vm.optionFails = [];
                vm.optionFails.push(
                    {
                        DescriptionScore: "Memenuhi"
                    },
                    {
                        DescriptionScore: "Tidak Memenuhi"
                    }
                )
            }
            console.info(vm.Criteria)
            console.info(vm.optionFails)
            console.info(vm.optionFails.length)
        }

        vm.addOption = addOption;
        function addOption() {
            var prevMaxScore = 0;
            console.info(vm.options)
            if (vm.options.length > 0) {
                prevMaxScore = vm.options[vm.options.length - 1].MaxScore;
            }
            vm.options.push({
                DescriptionScore: null,
                Score: null
            });
        }

        vm.deleteOption = deleteOption;
        function deleteOption(index) {
            if (index != vm.options.length - 1) {
                vm.options[index + 1].MinScore = vm.options[index].MinScore;
            }
            vm.options.splice(index, 1);
        }

        vm.deleteOptionFail = deleteOptionFail;
        function deleteOptionFail(index) {
            vm.optionFails.splice(index, 1);
        }

        vm.maxScoreChange = maxScoreChange;
        function maxScoreChange(index) {
            if (!vm.options[index].MaxScore) {
                vm.options[index].MaxScore = 0;
            }
            var prevMaxScore = 0; //memastikan nilai maksimum tidak kurang dari nilai maksimum opsi sebelumnya
            if (index > 0) {
                prevMaxScore = vm.options[index - 1].MaxScore;
            }
            if (vm.options[index].MaxScore < prevMaxScore) {
                vm.options[index].MaxScore = prevMaxScore;
            }

            var nextMaxScore = 100; //memastikan nilai maksimum tidak melebihi nilai maksimum opsi selanjutnya
            if (index != vm.options.length - 1) {
                nextMaxScore = vm.options[index + 1].MaxScore;
            }
            if (vm.options[index].MaxScore > nextMaxScore) {
                vm.options[index].MaxScore = nextMaxScore;
            }

            if (index != vm.options.length - 1) { //menjadikan nilai maksimum sebagai nilai minimum opsi selanjutnya
                vm.options[index + 1].MinScore = vm.options[index].MaxScore;
            }
        }

        vm.cancel = cancel;
        function cancel () {
            $uibModalInstance.dismiss('cancel');
        };
        
        vm.save = save;
        function save() {
            console.info(vm.Criteria)
            if (!vm.Criteria.CriteriaName) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_NAME'));
                return;
            }
            if (vm.Criteria.Level === 3) {
                if (!vm.chooseType) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHOOSETYPE'));
                    return;
                }

                if (vm.chooseType.Name == 'Pass Fail' && vm.optionFails.length == 0) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_OPTIONFAIL'));
                    return;
                }

                if ((vm.chooseType.Name == 'Nilai' && vm.Criteria.IsOptionStandard == true)) {
                    vm.Criteria.PrequalCriteriaStandards = vm.options;
                }

                if (vm.chooseType.Name == 'Pass Fail') {
                    vm.Criteria.PrequalCriteriaStandards = vm.optionFails;
                }
            }
            for (var i = 0; i < vm.options.length; i++) {
                if (!vm.options[i].DescriptionScore) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_DESCRIPTION'));
                    return;
                }
                if (vm.options[i].Score < 0 && vm.chooseType.Name == 'Nilai') {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_SCORE'));
                    return;
                }
            }

            //console.info(vm.chooseType);
            //console.info(vm.chooseType.IdTypeReff);

            UIControlService.loadLoadingModal(loadingMessage);
            if (vm.isEdit) {
                console.info("masuk if");
                KriteriaEvaluasiPrakualifikasiService.updateCriteria(vm.Criteria, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    UIControlService.unloadLoadingModal();
                });
            } else {
                console.info("else");
                KriteriaEvaluasiPrakualifikasiService.insertCriteria(vm.Criteria, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    UIControlService.unloadLoadingModal();
                });
            }
        }
    }
})();