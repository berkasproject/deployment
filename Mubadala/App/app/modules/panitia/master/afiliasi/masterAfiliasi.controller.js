﻿(function () {
    'use strict';

    angular.module("app").controller("masterAfiliasiCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'AfiliasiService', '$filter', '$rootScope'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, AfiliasiService, $filter, $rootScope) {
        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 0;
        //vm.maxSize = 10;
        vm.pageSize = 10;
        vm.page_id = 35;
        vm.menuhome = 0;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.userId = 0;
        vm.jLoad = jLoad;
        vm.bank = [];
        vm.ambilUrl;
        vm.ambilUrl2;
        vm.IsApprovedCR = false;
        vm.menuIndex = 8;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        //vm.Kata = "";
        vm.VendorID;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-afiliasi');
            jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            AfiliasiService.selectMaster({
                Keyword: vm.keyword,
                Offset: offset,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataAfiliasi = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.tambah = tambah;
        function tambah(data) {
            var data = {
                act: 1,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/afiliasi/tambah-ubah.modal.html',
                controller: "MasterAfiliasiModalCtrl",
                controllerAs: "MasterAfiliasiModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                // window.location.reload();
                vm.init();
            });
        };

        vm.edit = edit;
        function edit(data) {
            var data = {
                act: 0,
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/master/afiliasi/tambah-ubah.modal.html',
                controller: "MasterAfiliasiModalCtrl",
                controllerAs: "MasterAfiliasiModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        };

        vm.aktif = aktif;
        function aktif(data) {
            UIControlService.loadLoading("Loading...");
                    AfiliasiService.AktifkanMaster({
                        AfiliasiID: data.AfiliasiID
                    }, function (reply2) {
                        UIControlService.unloadLoading();
                        if (reply2.status === 200) {
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_ACTIVATE');
                            vm.init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_ACTIVATE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_GET_DATA');
                    });
        };

        vm.nonAktif = nonAktif;
        function nonAktif(data) {
            UIControlService.loadLoading("Loading...");
                    AfiliasiService.nonAktifkanMaster({
                        AfiliasiID: data.AfiliasiID
                    }, function (reply2) {
                        if (reply2.status === 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_INACTIVATE');
                            vm.init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_INACTIVATE');
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_GET_DATA');
                    });
        };
    }
})();