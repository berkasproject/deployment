﻿(function () {
    'use strict';

    angular.module("app").controller("MasterAfiliasiModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'AfiliasiService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, AfiliasiService, UIControlService, GlobalConstantService) {
        var vm = this;
        vm.detail = item.item;
        vm.isAdd = item.act;
        vm.action = "";
        vm.afiliasiName = "";
        vm.afiliasiID = 0;
        vm.klik = 0;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('master-afiliasi');
            if (vm.isAdd === 1) {
                vm.action = "Tambah";
            } else {
                vm.action = "Ubah";
                vm.afiliasiName = vm.detail.AfiliasiName;
                vm.afiliasiID = vm.detail.AfiliasiID;
            }
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal("Loading...");
            if (vm.klik > 0) {
                return;
            }
            if (vm.afiliasiName == "") {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.NAME");
                return;
            }

            vm.klik++;
            AfiliasiService.saveMaster({
                create: vm.isAdd,
                AfiliasiID: vm.afiliasiID,
                AfiliasiName: vm.afiliasiName
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS");
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
                } else {
                    vm.klik = 0;
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                vm.klik = 0;
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();