﻿(function () {
	'use strict';

	angular.module("app").controller("BadanUsahaModalCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BadanUsahaService', 'RoleService', 'UIControlService', 'item', '$uibModalInstance'];

	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, BadanUsahaService,
		RoleService, UIControlService, item, $uibModalInstance) {
		var vm = this;
		//console.info("masuuk modal : " + JSON.stringify(item));
		vm.isAdd = item.act;
		vm.action = "";
		vm.id_depart = 0;
		vm.Name = "";
		vm.Description = "";
		vm.BusinessID = 0;

		vm.listReq = [];

		vm.licenses = [];

		vm.IsNeedNpwp = false;
		
		vm.init = init;
		function init() {
		    if (vm.isAdd === true) {
		        vm.action = "Tambah ";
		    }
		    else {
		        vm.action = "Ubah ";
		        //console.info("item:" + JSON.stringify(item));
		        vm.Name = item.item.Name;
		        vm.Description = item.item.Description;
		        vm.BusinessID = item.item.BusinessID;
		        vm.IsNeedNpwp = item.item.IsNeedNpwp;
		        if (item.item.BusinessEntityRequirement != null) {
		            vm.listReq = item.item.BusinessEntityRequirement;
		            if (vm.listReq.length > 0 || vm.listReq != null) {
		                for (var i = 0; i <= vm.listReq.length - 1; i++) {
		                    vm.licenses.push(vm.listReq[i].License);
		                }
		            }
		            console.info("licenses1:" + JSON.stringify(vm.licenses));
		        }
		    }
		    loadLicense();
		}



		vm.addLicense = addLicense;
		function addLicense() {
		    if (vm.selectedAvailLicense === null)
		        return false;

		    vm.licenses.push(vm.selectedAvailLicense);
		    vm.availLicenses = remove(vm.availLicenses, vm.selectedAvailLicense);
		    console.info("license" + JSON.stringify(vm.licenses));
		}

		vm.removeLicense = removeLicense;
		function removeLicense() {
		    if (vm.selectedLicense === null) return false;

		    vm.availLicenses.push(vm.selectedLicense);
		    vm.licenses = remove(vm.licenses, vm.selectedLicense);
		}


		function remove(list, data) {
		    var i = 0;

		    while (i < (list.length)) {
		        if (list[i] === data) {
		            list.splice(i, 1);
		            return list;
		        }
		        i++;
		    }
		}


		function loadLicense() {
		    UIControlService.loadLoading("MESSAGE.LOADING");


		    BadanUsahaService.selectLicense({BusinessID:vm.BusinessID}, function (reply) {
		        
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.availLicenses = reply.data;
		            //console.info("datane:" + JSON.stringify(vm.licenses));
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		};


		vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
		};

		vm.simpan = simpan;
		function simpan() {
		    //console.info(vm.kode_depart + "-" + vm.nama_depart + "-" + vm.ket_depart);
		    if (vm.Name === "" || vm.Name === null) {
		        UIControlService.msg_growl("warning", "MESSAGE.NO_CODE");
		        return;
		    }
		    if (vm.isAdd == true) {
		        cekKodeNama();
		    }
		    else if (vm.isAdd == false) {
		        prosesSimpan();
		    }
		}

	    //proses simpan
		vm.prosesSimpan = prosesSimpan;
        function prosesSimpan(){
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            if (vm.IsNeedNpwp == null) {
                vm.IsNeedNpwp = false;
            }
		    if (vm.isAdd === true) {
		        BadanUsahaService.insert({
		            Name: vm.Name, Description: vm.Description, License:vm.licenses,IsNeedNpwp:vm.IsNeedNpwp
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
		                $uibModalInstance.close();
		            }
		            else {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            UIControlService.unloadLoadingModal();
		        });
		    }
		    else {
		        //console.info("license" + JSON.stringify(vm.licenses));
                
		        BadanUsahaService.update({
		            BusinessID: vm.BusinessID, Name: vm.Name, Description: vm.Description, License: vm.licenses, IsNeedNpwp: vm.IsNeedNpwp
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                UIControlService.msg_growl("success", "MESSAGE.SUCC_UPDATE");
		                $uibModalInstance.close();
		            }
		            else {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_UPDATE");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            UIControlService.unloadLoadingModal();
		        });
                
		    }
		}

		vm.cekKodeNama = cekKodeNama;
		function cekKodeNama() {
		    //pengecekkan kode atau nama sudah ada belum?
		    BadanUsahaService.cekData({
		        column: 1, Keyword: vm.Name
		    }, function (reply) {
		        //console.info("cek1:" + JSON.stringify(reply));
		        if (reply.status === 200 && reply.data.length > 0) {
		            UIControlService.msg_growl("warning", "MESSAGE.ERR_NAME");
		            return;
		        }
		        else if (reply.status === 200 && reply.data.length <= 0) {
		            prosesSimpan();
		        }
		        else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_CHECK");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

	}
})();