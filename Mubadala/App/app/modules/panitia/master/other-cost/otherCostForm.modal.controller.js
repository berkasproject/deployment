﻿(function () {
    'use strict';

    angular.module("app").controller("OtherCostModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$uibModal',
        'OtherCostService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $uibModal,
		OtherCostService, UIControlService, item, $uibModalInstance) {

        var vm = this;

        vm.init = init;
        function init() {

            if (item.ID) {
                vm.isEdit = true;
                vm.action = "UBAH";
                OtherCostService.GetById({
                    ID : item.ID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    var data = reply.data;
                    vm.OtherCost = data;
                }, function (err) {
                    UIControlService.msg_growl("error", "ERR_LOAD");
                    UIControlService.unloadLoading();
                });
		    } else {
                vm.action = "TAMBAH";
                vm.OtherCost = {};
            }
        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

        vm.simpan = simpan;
        function simpan() {

            if (!vm.OtherCost.OtherCostName || (!vm.OtherCost.ForGoods && !vm.OtherCost.ForVHS && !vm.OtherCost.ForFPA)) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_INCOMPLETE");
                return;
            }

            var saveOtherCost = vm.isEdit ? OtherCostService.UpdateOtherCost : OtherCostService.InsertOtherCost;
            UIControlService.loadLoadingModal("");
            saveOtherCost(vm.OtherCost, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE_OTHER_COST");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_OTHER_COST");
		    });
        }
    }
})();