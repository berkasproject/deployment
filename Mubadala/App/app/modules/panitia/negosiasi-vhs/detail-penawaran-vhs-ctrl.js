﻿(function () {
    'use strict';

    angular.module("app").controller("DPVHSCtrl", ctrl);

    ctrl.$inject = ['$filter','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiVHSService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiVHSService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.VendorID = Number($stateParams.VendorID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.StepID = Number($stateParams.StepID);
        vm.IdNego = Number($stateParams.Id);

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        //vm.NegoId = 0;
        vm.jLoad = jLoad;
        vm.nego = [];
        vm.allitem = [];
        vm.dataChecked = [];
        function init() {
            $translatePartialLoader.addPart('negosiasi');
            vm.dataChecked = [];
            localStorage.removeItem('checked-negosiasi-vhs');
            localStorage.removeItem('checked-negosiasi-vhs-all');
            loadOvertime();
            showAll();
            jLoad(1);
        }

        vm.loadOvertime = loadOvertime;
        function loadOvertime() {
            NegosiasiVHSService.isOvertime({ ID: vm.StepID }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.overtime = reply.data;
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    //UIControlService.unloadLoading();
                }
            }, function (err) {
                //$.growl.error({ message: "Gagal Akses API >" + err });
                //UIControlService.unloadLoading();
            });
        }

        //tampil detail penawaran
        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.nego = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
           var tender = {
              VendorID: vm.VendorID,
              TenderStepDataID: vm.StepID,
              Offset: offset,
              Limit: vm.pageSize
           }
            NegosiasiVHSService.itemall(tender, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var jum = reply.data;
                    vm.nego = reply.data.List;
                    var checkAll = JSON.parse(localStorage.getItem('checked-negosiasi-vhs-all'));
                    var checked = JSON.parse(localStorage.getItem('checked-negosiasi-vhs'));
                    //console.info(checkAll);
                    //console.info(checked);
                    if (checkAll == true) {
                     //   vm.IsOpenAll = true;
                        for (var i = 0; i < vm.nego.length; i++) {
                            vm.nego[i].IsOpen = true;
                        }
                    }
                    if (checked != null) {
                        for (var j = 0; j < checked.length; j++) {
                            for (var x = 0; x < vm.nego.length; x++) {
                                if (checked[j].ID == vm.nego[x].ID && checked[j].VendorID == vm.nego[x].VendorID && checked[j].VOEId == vm.nego[x].VOEId) {
                                    vm.nego[x].IsOpen = true;
                                }
                            }
                        }
                    }

                   // else vm.IsOpenAll = false;
                    vm.id = vm.nego[0].VHSNegoId;
                    vm.generet = vm.nego[0].IsGenerate;
                    vm.totalItems = Number(jum.Count);
                    vm.IsNego = vm.nego[0].IsNego;
                    vm.IsActive = vm.nego[0].IsActive;
                    vm.maxSize = vm.totalItems;
                } else {
                    //UIControlService.unloadLoading();
                }
            }, function (err) {
                //UIControlService.unloadLoading();
            });
        }


        vm.saveIsOpen = saveIsOpen;
        function saveIsOpen(data) {
            var param = {
                ID: data.ID,
                VOEId: data.VOEId,
                IsOpen: data.IsOpen,
                VHSNegoId: data.VHSNegoId
            };
            vm.savedata = [];
            vm.savedata.push(param);   
          //  NegosiasiVHSService.updatedetail(vm.savedata,
          //     function (reply) {
          //         UIControlService.unloadLoadingModal();
          //         if (reply.status === 200) {
          //         }
          //         else {
          //             UIControlService.msg_growl("error", "Gagal menyimpan data!!");
          //             return;
          //         }
          //     },
          //     function (err) {
          //         UIControlService.msg_growl("error", "Gagal Akses Api!!");
          //         // UIControlService.unloadLoadingModal();
          //     }
          //);


        }

        vm.Checked = Checked;
        function Checked(dt) {
            vm.dataChecked = JSON.parse(localStorage.getItem('checked-negosiasi-vhs'));
            if (dt.IsOpen == true) {
                if (vm.dataChecked == null) vm.dataChecked = [];
                vm.dataChecked.push(dt);
            }
            else {
                for (var i = 0; i < vm.dataChecked.length; i++) {
                    if (vm.dataChecked[i].ID == dt.ID && vm.dataChecked[i].VendorID == dt.VendorID && vm.dataChecked[i].VOEId == dt.VOEId) {
                        vm.dataChecked.splice(i, 1);
                    }
                }
            }
            localStorage.removeItem('checked-negosiasi-vhs');
            //console.info("Checked");
           localStorage.setItem('checked-negosiasi-vhs', JSON.stringify(vm.dataChecked));
            //console.info(JSON.parse(localStorage.getItem('checked-negosiasi-vhs')));
        }

        vm.CheckedAll = CheckedAll;
        function CheckedAll() {
            localStorage.removeItem('checked-negosiasi-vhs');
            vm.dataChecked = [];
            if (vm.IsOpenAll == true) {
                localStorage.setItem('checked-negosiasi-vhs-all', JSON.stringify(true));
                for (var j = 0; j < vm.nego.length; j++) {
                    vm.nego[j].IsOpen = true;
                }
                showAll(1);
            }
            else {
                localStorage.removeItem('checked-negosiasi-vhs');
                localStorage.removeItem('checked-negosiasi-vhs-all');
                jLoad(1);
            }
        }

        vm.showAll = showAll;
        function showAll() {
            var parameter = {
                VendorID: vm.VendorID,
                TenderStepDataID: vm.StepID,
                Offset: 0,
                Limit: 0
            }
            UIControlService.loadLoading("");
            NegosiasiVHSService.itemall(parameter, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.allitem = reply.data.List;
                    vm.cost = 0; vm.cosEstimate = 0; vm.totalNego = 0; vm.totalHistorical = 0; vm.totalEPV = 0;
                    var checkAll = JSON.parse(localStorage.getItem('checked-negosiasi-vhs-all'));
                    var sameCurrenciesHistorical = true;
                    var sameCurrenciesEPV = true;
                    for (var i = 0; i < vm.allitem.length; i++) {
                        if (i === 0) {
                            vm.Id = vm.allitem[i].VHSNegoId;
                            vm.cost = vm.allitem[i].TotalPriceVOE;
                            vm.totalNego = vm.allitem[i].TotalPrice;

                        }
                        else {
                            vm.cost = +vm.cost + +vm.allitem[i].TotalPriceVOE;
                            vm.totalNego = +vm.totalNego + +vm.allitem[i].TotalPrice;
                        }
                        if (checkAll == true) {
                            vm.allitem[i].IsOpen = true;
                            vm.dataChecked.push(vm.allitem[i]);
                        }
                        else {
                            if (vm.allitem[i].IsOpen == true) vm.dataChecked.push(vm.allitem[i]);
                        }
                        if ((vm.allitem.length - 1) == i) {
                            localStorage.removeItem('checked-negosiasi-vhs');
                            //console.info("Show all");
                            localStorage.setItem('checked-negosiasi-vhs', JSON.stringify(vm.dataChecked));
                        }
                        
                        if (i !== 0) {
                            if (vm.allitem[i].HistoricalCurrency !== vm.allitem[i - 1].HistoricalCurrency){
                                sameCurrenciesHistorical = false;
                            }
                            if (vm.allitem[i].EPVCurrency !== vm.allitem[i - 1].EPVCurrency) {
                                sameCurrenciesEPV = false;
                            }
                        }
                        if (sameCurrenciesHistorical) {
                            vm.totalHistorical += vm.allitem[i].HistoricalTotalPrice;
                        } else {
                            vm.totalHistorical = 0;
                        }
                        if (sameCurrenciesEPV) {
                            vm.totalEPV += vm.allitem[i].EPVTotalPrice;
                        } else {
                            vm.totalEPV = 0;
                        }
                    }
                    vm.selisih = vm.cost - vm.totalNego;
                    vm.presentase = ((vm.selisih / vm.cost) * 100).toFixed(2);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        function checkIsOpenData() {
            //vm.IsOpenAll = false;
            var isOpenTrue = 0;
            for (var i = 0; i < vm.allitem.length; i++) {
                if (vm.allitem[i].IsOpen === true) {
                    isOpenTrue = isOpenTrue + 1;
                }
            }
            if (isOpenTrue === vm.allitem.length) {
              //  vm.IsOpenAll = true;
            }
        }

        vm.loadOpenAll = loadOpenAll;
        function loadOpenAll() {
            var countPaging = vm.nego.length;
            var countItemInPage = vm.totalItems - countPaging;
            var startIndex = (vm.currentPage - 1) * 10;
            if (vm.IsOpenAll === true) {
                for (var i = 0; i < vm.totalItems; i++) {
                    if (startIndex >= 10) {
                        if (i >= startIndex) {
                            vm.nego[i - startIndex].IsOpen = true;
                        }
                    }
                    else {
                        if (i < countPaging) {
                            vm.nego[i].IsOpen = true;
                        }
                    }
                }
            }
            else if (vm.IsOpenAll === false) {
                for (var i = 0; i < vm.totalItems; i++) {
                    if (startIndex >= 10) {
                        if (i >= startIndex) {
                            vm.nego[i - startIndex].IsOpen = false;
                        }
                    }
                    else {
                        if (i < countPaging) {
                            vm.nego[i].IsOpen = false;
                        }
                    }
                }
            }
            /*
            var parameter = {
                VendorID: vm.VendorID,
                TenderStepDataID: vm.StepID,
                Offset: 0,
                Limit: vm.totalItems
            }
            //showAll(parameter);
            //console.info("loadAll:" + JSON.stringify(vm.allitem));*/
        }
        
        vm.loadv = loadv;
        function loadv() {
            //console.info("curr "+current)
            vm.lv = [];
            //vm.currentPage = current;
           // var offset = (current * 10) - 10;
            NegosiasiVHSService.select({
                column: vm.StepID,
                status: vm.TenderRefID,
                FilterType: vm.ProcPackType
            }, function (reply) {
                //console.info("data:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.lv = reply.data;
                    //console.info("data:" + JSON.stringify(vm.nb));
                    //ambil data isgenerate
                    for (var i = 0; i < vm.lv.length; i++) {
                        if (vm.lv[i].ID === vm.Id && vm.lv[i].VendorID === vm.VendorID) {
                            vm.generet = vm.lv[i].IsGenerate;
                            vm.harga = vm.lv[i].NegotiationPrice;
                        }
                    }
                    //console.info("ini:" + JSON.stringify(vm.vendor));
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
        
        vm.save = save;
        function save() {
            vm.dataChecked = [];
            vm.dataChecked = JSON.parse(localStorage.getItem('checked-negosiasi-vhs'));
            //console.info(vm.dataChecked);
            if (vm.dataChecked == null) {
                vm.dataChecked.push({
                    tender: {
                        tender: {
                            TenderRefID: vm.TenderRefID,
                            ProcPackageType: vm.ProcPackType

                        }
                    },
                    VendorID: vm.VendorID,
                    IsOpen: null
                });
            }
            else if (vm.dataChecked.length == 0) {
                vm.dataChecked.push({
                    tender: {
                        tender: {
                            TenderRefID: vm.TenderRefID,
                            ProcPackageType: vm.ProcPackType

                        }
                    },
                    VendorID: vm.VendorID,
                    IsOpen: null
                });
            }
            NegosiasiVHSService.updatedetail({ IdNego : vm.IdNego, detailList: vm.dataChecked },
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", 'MESSAGE.SUCC_SAVE');
                       //   $uibModalInstance.close();
                       init();
                   }
                   else {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE');
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", 'MESSAGE.API');
                   // UIControlService.unloadLoadingModal();
               }
          );
            



            //vm.list = [];
            //console.info(vm.allitem);
            //if (vm.IsOpenAll === true) {
            //    for (var i = 0; i < vm.totalItems; i++) {
            //        var param = {
            //            ID: vm.allitem[i].ID,
            //            VOEId: vm.allitem[i].VOEId,
            //            IsOpen: true,
            //            VHSNegoId: vm.allitem[i].VHSNegoId
            //        };
            //        vm.list.push(param);
            //    }
            //}
            //else if (vm.IsOpenAll === false) {
            //    for (var i = 0; i < vm.totalItems; i++) {
            //        var param = {
            //            ID: vm.allitem[i].ID,
            //            VOEId: vm.allitem[i].VOEId,
            //            IsOpen: false,
            //            VHSNegoId: vm.allitem[i].VHSNegoId
            //        };
            //        vm.list.push(param);
            //    }
            //}
            
            /*
            for (var i = 0; i < vm.nego.length; i++) {
                if (vm.nego[i].ID !== 0) {
                    var dta = {
                        ID: vm.nego[i].ID,
                        VOEId: vm.nego[i].VOEId,
                        IsOpen: vm.nego[i].IsOpen,
                        VHSNegoId: vm.nego[i].VHSNegoId
                    };
                }
                else {
                    var dta = {
                        ID: 0,
                        VOEId: vm.nego[i].VOEId,
                        IsOpen: vm.nego[i].IsOpen,
                        VHSNegoId: vm.nego[i].VHSNegoId
                    };
                }
                vm.list.push(dta);
            }
            NegosiasiVHSService.updatedetail(vm.list,
               function (reply) {
                   UIControlService.unloadLoadingModal();
                   if (reply.status === 200) {
                       UIControlService.msg_growl("success", "Berhasil Simpan data");
                       //   $uibModalInstance.close();
                       init();
                   }
                   else {
                       UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "Gagal Akses Api!!");
                  // UIControlService.unloadLoadingModal();
               }
          );*/
        }
        vm.back = back;
        function back() {
            if (vm.nego[0].IsNego == true)
                $state.transitionTo('negosiasi-vhs', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, StepID: vm.StepID });
            else {
                $state.transitionTo('negosiasi-vhs-chat', { VHSNegoId: vm.Id, StepID: vm.StepID, VendorID: vm.VendorID, Id: vm.Id, ProcPackType: vm.ProcPackType, TenderRefID: vm.TenderRefID });
            }
        }


        vm.generate = generate;
        function generate() {
            /*
            var isg = {
                ID: vm.Id,
                IsGenerate: vm.generet
            };*/
            NegosiasiVHSService.update({
                ID: vm.Id,
                IsGenerate: vm.generet
            },
            function (reply) {
                //UIControlService.loadLoading("Silahkan Tunggu...");
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", 'MESSAGE.SUCC_SAVE');
                    // vm.generet = isg.IsGenerate;
                    //console.info("gnrt:" + JSON.stringify(vm.generet));
                    init();
                    //jLoad(1);
                }
                else {
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_SAVE');
                    return;
                }
            },
            function (err) {
                UIControlService.msg_growl("error", 'MESSAGE.API');
            }
            );
            
            }
        
    }
})();
//TODO


