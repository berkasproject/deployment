﻿(function () {
	'use strict';

	angular.module("app").controller("NegosiasiVHSPrintCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'UIControlService', 'DataPengadaanService', '$stateParams', 'NegosiasiVHSService'];
	function ctrl($translatePartialLoader, UIControlService, DataPengadaanService, $stateParams, NegosiasiVHSService) {
		var vm = this
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.jumlahlembar = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('negosiasi');
			UIControlService.loadLoading("MESSAGE.LOADING");

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			loadOvertime();
			loadStep();
			jLoad(1);
		}

		vm.loadOvertime = loadOvertime;
		function loadOvertime() {
			NegosiasiVHSService.isOvertime({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.overtime = reply.data;
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.listTenderStep.tender.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		vm.loadStep = loadStep;
		function loadStep() {
			NegosiasiVHSService.loadStep({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTenderStep = reply.data;
					vm.listTenderStep.StartDate = UIControlService.convertDateTime(vm.listTenderStep.StartDate);
					vm.listTenderStep.EndDate = UIControlService.convertDateTime(vm.listTenderStep.EndDate);
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data Chatting" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.nb = [];
			vm.list = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			NegosiasiVHSService.select({
				column: vm.StepID,
				status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.nb = reply.data;
					//console.info("data:" + JSON.stringify(vm.nb));
					vm.flagSum = true;
					for (var i = 0; i < vm.nb.length; i++) {
						if (vm.nb[i].ID !== 0) {
							if (vm.nb[i].IsDeal !== null) {
								//console.info(vm.nb[i].IsDeal);
								vm.flagSum = false;
							}
							else {
								//console.info(vm.nb[i].IsDeal);
								vm.flagSum = true;
							}
							vm.list.push(vm.nb[i]);
						}
					}
					vm.TenderStepDataID = vm.nb[0].TenderStepDataID;
					// cek();
					for (var i = 0; i < vm.nb.length; i++) {
						if (vm.nb[i].ID === 0) {
							vm.cek_summary = true;
							i = vm.nb.length;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
	}
})()