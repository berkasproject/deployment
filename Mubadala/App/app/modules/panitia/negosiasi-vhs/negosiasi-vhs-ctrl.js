﻿(function () {
	'use strict';

	angular.module("app").controller("NegosiasiVHSCtrl", ctrl);

	ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService',
        'NegosiasiVHSService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService,
        NegosiasiVHSService, RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.ProcPackType = Number($stateParams.ProcPackType);

		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.jLoad = jLoad;
		vm.nb = [];
		vm.GoodsNegoId = 0;
		vm.generate = 7;
		vm.pesan = "Apakah Anda setuju dengan  dengan nego ini..?"

		function init() {
			$translatePartialLoader.addPart('negosiasi');
			UIControlService.loadLoading("MESSAGE.LOADING");

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.TenderRefID,
				ProcPackageType: vm.ProcPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			loadOvertime();
			loadStep();
			jLoad(1);

		}

		vm.loadStep = loadStep;
		function loadStep() {
			NegosiasiVHSService.loadStep({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTenderStep = reply.data;
					vm.listTenderStep.StartDate = UIControlService.convertDateTime(vm.listTenderStep.StartDate);
					vm.listTenderStep.EndDate = UIControlService.convertDateTime(vm.listTenderStep.EndDate);
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data Chatting" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('negosiasi-vhs-print', {
				TenderRefID: vm.TenderRefID,
				StepID: vm.StepID,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.loadOvertime = loadOvertime;
		function loadOvertime() {
			NegosiasiVHSService.isOvertime({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.overtime = reply.data;
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data Chatting" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//tampil data forum
		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.nb = [];
			vm.list = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			NegosiasiVHSService.select({
				column: vm.StepID,
				status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.nb = reply.data;
					//console.info("data:" + JSON.stringify(vm.nb));
					vm.flagSum = true;
					for (var i = 0; i < vm.nb.length; i++) {
						if (vm.nb[i].ID !== 0) {
							if (vm.nb[i].IsDeal !== null) {
								//console.info(vm.nb[i].IsDeal);
								vm.flagSum = false;
							}
							else {
								//console.info(vm.nb[i].IsDeal);
								vm.flagSum = true;
							}
							vm.list.push(vm.nb[i]);
						}
					}
					vm.TenderStepDataID = vm.nb[0].TenderStepDataID;
					// cek();
					for (var i = 0; i < vm.nb.length; i++) {
						if (vm.nb[i].ID === 0) {
							vm.cek_summary = true;
							i = vm.nb.length;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//btn kembali
		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.listTenderStep.TenderID });
		}

		//detail chat
		vm.viewDetail = viewDetail;
		function viewDetail(index, data, flag, flagNego) {
			if (flag === true) {
				if (vm.overtime == false) {
					UIControlService.msg_growl("error", "MESSAGE.NEGO_TIDAK_BISA");
					return;
				} else {
					if (flagNego === true) {
						if (index != 0 && data.TenderItemize == false) {
							if (localStorage.getItem("currLang") === 'id') {
								var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Alasan negosiasi selain best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*jika klik OK, maka secara otomatis kirim approval ke L1 untuk negosiasi selain best bidder</div></div></form>');
							}
							else if (localStorage.getItem("currLang") === 'en') {
								var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Reasons for Negotiation with non-best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*If click OK, then it will automatically send approval to L1 for negotiation with non-best bidder</div></div></form>');
							}
							bootbox.confirm(form, /*$filter('translate')('MESSAGE.APPROVAL'),*/ function (yes) {
								if (yes) {
									vm.Remark = form.find('textarea[name=usernameInput]').val();
									if (vm.Remark == "") {
										UIControlService.msg_growl('error', "MESSAGE.REASON_CANT_EMPTY");
										return false
									} else {
										UIControlService.loadLoadingModal("MESSAGE.LOADING");
										var datainsert = {
											VendorID: data.VendorID,
											TenderStepDataID: vm.StepID,
											IsNego: true,
											IsNeedApproval: true,
											Remark: vm.Remark
										}
										NegosiasiVHSService.insert(datainsert, function (reply) {
											if (reply.status === 200) {
												UIControlService.unloadLoading();
												vm.VHSNegoId = reply.data.ID;
												sendApprovalNego();
											}
										}, function (err) { })
									}
								}
							})
						} else {
							bootbox.confirm($filter('translate')('MESSAGE.YAKIN_SEPAKAT'), function (yes) {
								if (yes) {
									var datainsert = {
										VendorID: data.VendorID,
										TenderStepDataID: data.TenderStepDataID,
										IsNego: true,
										IsNeedApproval: false
									}
									NegosiasiVHSService.insert(datainsert, function (reply) {
										if (reply.status === 200) {
											vm.VHSNegoId = reply.data.ID;
											UIControlService.unloadLoading();
											init();
										} else {
											UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
											return;
										}
									}, function (err) {
										UIControlService.msg_growl("error", "MESSAGE.API");
									});
								}
							});
						}
					} else {
						if (index != 0 && data.TenderItemize == false) {
							if (localStorage.getItem("currLang") === 'id') {
								var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Alasan negosiasi selain best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*jika klik OK, maka secara otomatis kirim approval ke L1 untuk negosiasi selain best bidder</div></div></form>');
							}
							else if (localStorage.getItem("currLang") === 'en') {
								var form = $('<form><div class="row"><div class="col-md-12"> <label class="control-label"> Reasons for Negotiation with non-best bidder   : </label></div><div class="col-md-12"><textarea class="form-control" type="text" name="usernameInput"/></div><br /> <div class="col-md-12">*If click OK, then it will automatically send approval to L1 for negotiation with non-best bidder</div></div></form>');
							}
							bootbox.confirm(form, /*$filter('translate')('MESSAGE.APPROVAL'),*/ function (yes) {
								if (yes) {
									vm.Remark = form.find('textarea[name=usernameInput]').val();
									if (vm.Remark == "") {
										UIControlService.msg_growl('error', "MESSAGE.REASON_CANT_EMPTY");
										return false;
									} else {
										UIControlService.loadLoadingModal("MESSAGE.LOADING");
										var datainsert = {
											VendorID: data.VendorID,
											TenderStepDataID: vm.StepID,
											IsNego: false,
											IsNeedApproval: true,
											Remark: vm.Remark
										}
										NegosiasiVHSService.insert(datainsert, function (reply) {
											if (reply.status === 200) {
												UIControlService.unloadLoading();
												vm.VHSNegoId = reply.data.ID;
												sendApprovalNego();
											}
										}, function (err) { });
									}
								}
							});
						} else {
							bootbox.confirm($filter('translate')('MESSAGE.YAKIN_NEGO'), function (yes) {
								if (yes) {
									var datainsert = {
										VendorID: data.VendorID,
										TenderStepDataID: data.TenderStepDataID,
										IsNego: false
									}
									NegosiasiVHSService.insert(datainsert, function (reply) {
										if (reply.status === 200) {
											vm.VHSNegoId = reply.data.ID;
											sendMailNego(reply.data.ID);
											$state.transitionTo('negosiasi-vhs-chat', { VHSNegoId: vm.VHSNegoId, StepID: data.TenderStepDataID, VendorID: data.VendorID, Id: vm.VHSNegoId, ProcPackType: vm.ProcPackType, TenderRefID: vm.TenderRefID });
										} else {
											UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
											return;
										}
									}, function (err) {
										UIControlService.msg_growl("error", "MESSAGE.API");
									});
								}
							});
						}
					}

				}
			} else {
				if (data.IsNego == true) {
					$state.transitionTo('detail-penawaran-vhs', {
						VendorID: data.VendorID,
						StepID: vm.StepID,
						TenderRefID: vm.TenderRefID,
						ProcPackType: vm.ProcPackType
					});
				} else {
					$state.transitionTo('negosiasi-vhs-chat', {
						VHSNegoId: data.ID,
						StepID: vm.StepID,
						VendorID: data.VendorID,
						Id: data.ID,
						ProcPackType: vm.ProcPackType,
						TenderRefID: vm.TenderRefID
					});
				}
			}
		}

		vm.sendApprovalNego = sendApprovalNego;
		function sendApprovalNego() {
			var datainsert = {
				VHSNegoId: vm.VHSNegoId
			}
			NegosiasiVHSService.InsertApproval(datainsert,
            function (reply) {
            	if (reply.status === 200) {
            		UIControlService.msg_growl("success", "MESSAGE.APP_SENT");
            		sendMailToApprover();
            		init();
            	}
            	else {
            		return;
            	}
            },
            function (err) {
            }
        );

		}

		vm.sendMailToApprover = sendMailToApprover;
		function sendMailToApprover() {
			var datainsert = {
				ID: vm.VHSNegoId
			}
			NegosiasiVHSService.sendMailToApprover(datainsert,
            function (reply) {
            	if (reply.status === 200) {
            		UIControlService.msg_growl("success", "MESSAGE.MAIL_SENT");
            	}
            	else {
            		//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
            		return;
            	}
            },
            function (err) {
            	//UIControlService.msg_growl("error", "Gagal Akses Api!!");
            }
        );

		}

		vm.cek = cek;
		function cek() {
			NegosiasiVHSService.cek({
				TenderStepDataID: vm.TenderStepDataID
			}, function (reply) {
				//console.info("data:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.hasil_cek = reply.data;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				// //console.info("error:" + JSON.stringify(err));
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//modal summary
		vm.do_summary = do_summary;
		function do_summary() {
			var data = {
				item: vm.list
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/negosiasi-vhs/summary.html',
				controller: 'SummaryVHSCtrl',
				controllerAs: 'SummaryVHSCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.endnego = endnego;
		function endnego(data) {
			bootbox.confirm($filter('translate')('MESSAGE.AKHIR_NEGO'), function (yes) {
				if (yes) {
					NegosiasiVHSService.editactive({
						ID: data.ID,
						IsActive: false
					},
                      function (reply) {
                      	if (reply.status === 200) {
                      		UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                      		init();
                      	}
                      	else {
                      		UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                      		return;
                      	}
                      },
                      function (err) {
                      	UIControlService.msg_growl("error", "MESSAGE.API");
                      }
                 );
				}
			});
		}

		vm.sendMailNego = sendMailNego;
		function sendMailNego(negoId) {
			var datainsert = {
				ID: negoId
			}
			NegosiasiVHSService.sendMail(datainsert,
            function (reply) {
            	if (reply.status === 200) {
            		UIControlService.msg_growl("success", "MESSAGE.MAIL_SENT");
            	}
            	else {
            		//UIControlService.msg_growl("error", "Gagal menyimpan data!!");
            		return;
            	}
            },
            function (err) {
            	//UIControlService.msg_growl("error", "Gagal Akses Api!!");
            }
        );

		}
		vm.detailApproval = detailApproval;
		function detailApproval(dt) {
			var item = {
				ID: dt.ID
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/negosiasi-vhs/detailApproval.modal.html?v=1.000002',
				controller: 'detailApprovalVHSNegoCtrl',
				controllerAs: 'detailApprovalVHSNegoCtrl',
				resolve: { item: function () { return item; } }
			});
		};

		vm.reOpen = reOpen;
		function reOpen(data) {
		    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REOPEN_NEGO'), function (yes) {
		        if (yes) {
		            UIControlService.loadLoading("");
		            NegosiasiVHSService.CanReOpenNego({
		                ID: data.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.data === true) {
		                    UIControlService.loadLoading("");
		                    NegosiasiVHSService.ReOpenNegotiation({
		                        ID: data.ID
		                    }, function (reply) {
		                        UIControlService.unloadLoading();
		                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REOPEN_NEGO'));
		                        sendMailNego(data.ID);
		                        init();
		                    }, function (error) {
		                        UIControlService.unloadLoading();
		                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
		                    });
		                } else {
		                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_MAX_NEGO'));
		                }
		            }, function (error) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
		            });
		        }
		    });
		}
	}
})();
//TODO


