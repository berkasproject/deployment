﻿(function () {
    'use strict';

    angular.module("app").controller("ViewVendorCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', 'UIControlService', '$uibModalInstance', 'anncPrequalService', 'item'];

    function ctrl($translatePartialLoader, UIControlService, $uibModalInstance, anncPrequalService, item) {
        var vm = this;

        vm.vendors = null;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.keyword = '';
        vm.ID = item.ID;
        vm.IsPublish = item.IsPublish;
        vm.IsLocal = item.IsLocal;
        vm.IsNational = item.IsNational;
        vm.IsInternational = item.IsInternational;
        vm.selectedVendor = [];
        vm.getData = item.getData;
        vm.init = init;
        vm.contactVendor = [];
        function init() {
            //if (vm.IsPublish != true) {
            //    if (vm.getData.length == 0) {
            //        if (vm.IsLocal == true) SaveSelectedLocal();
            //        else showVendor(1);
            //    }
            //    else {
            //        vm.contactVendor = vm.getData;
            //        vm.selectedVendor = vm.getData;
            //        showVendor(1);
            //    }
            //}
            //else {
            //    getVendor(1);
            //}
            vm.IsPublish = true;
            getVendor(1);
            
        }

        vm.getVendor = getVendor;
        function getVendor(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            anncPrequalService.getVendor({
                ID: item.ID,
                Keyword: vm.keyword,
                Offset: vm.maxSize * (vm.currentPage - 1),
                Limit: vm.maxSize
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vendors = reply.data.List;
                    vm.totalItems = Number(reply.data.Count);
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.SaveSelectedLocal = SaveSelectedLocal;
        function SaveSelectedLocal() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            anncPrequalService.viewVendor({
                IsLokal: true,
                IsNasional: false,
                IsInternasional: false,
                ID: 0,
                Keyword: vm.keyword,
                Offset: vm.maxSize * (vm.currentPage - 1),
                Limit: vm.maxSize,
                contactVendor: vm.contactVendor
            }, function (reply) {
                if (reply.status === 200) {
                    vm.selectedVendor = reply.data.List;
                    showVendor(1);
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.showVendor = showVendor;
        function showVendor(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            anncPrequalService.viewVendor({
                IsLokal: item.IsLocal,
                IsNasional: item.IsNational,
                IsInternasional: item.IsInternational,
                ID: vm.ID,
                Keyword: vm.keyword,
                contactVendor: vm.contactVendor
            }, function (reply) {
                if (reply.status === 200) {
                    vm.vendors = reply.data.List;
                    vm.totalItems = Number(reply.data.Count);

                    vm.selectedVendor.forEach(function (selectedVendor) {
                        vm.vendors.forEach(function (datavendor) {
                            if (selectedVendor.VendorID == datavendor.VendorID) {
                                datavendor.IsCheck = true;
                            }
                        })
                    });
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.save = save;
        function save() {
            $uibModalInstance.close(vm.selectedVendor);

        }

        vm.checkAll = checkAll;
        function checkAll() {
            if (vm.IsCheckAll == true) {
                UIControlService.loadLoadingModal("MESSAGE.LOADING");
                anncPrequalService.viewVendor({
                    IsLokal: vm.IsLocal,
                    IsNasional: vm.IsNational,
                    IsInternasional: vm.IsInternational,
                    Keyword: vm.keyword,
                    contactVendor: vm.contactVendor
                }, function (reply) {
                    if (reply.status === 200) {
                        vm.selectedVendor = reply.data.List;
                        showVendor(1);
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
            else {
                vm.selectedVendor = [];
                if (vm.IsLocal == true) {
                    UIControlService.loadLoadingModal("MESSAGE.LOADING");
                    anncPrequalService.viewVendor({
                        IsLokal: true,
                        IsNasional: false,
                        IsInternasional: false,
                        Keyword: vm.keyword,
                        contactVendor: vm.contactVendor
                    }, function (reply) {
                        if (reply.status === 200) {
                            vm.selectedVendor = reply.data.List;
                            showVendor(1);
                        }
                    }, function (err) {
                        UIControlService.unloadLoadingModal();
                    });
                }
            }
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.selectvendor = selectvendor;
        function selectvendor(data) {
            if (data.IsCheck == true) {
                vm.selectedVendor.push(data);
            }
            else {
                for (var i = 0; i < vm.selectedVendor.length; i++) {
                    if (vm.selectedVendor[i].VendorID == data.VendorID && data.IsCheck == false) {
                        vm.selectedVendor.splice(i, 1);
                    }
                }

            }
        }
    }
})();