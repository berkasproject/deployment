﻿(function () {
	'use strict';

	angular.module("app").controller("PengumumanHasilPQCtrl", ctrl);

	ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$state', 'UIControlService', '$uibModal', '$stateParams', 'PrequalResultAnnouncementService'];
	function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService, $state, UIControlService, $uibModal, $stateParams, PrequalResultAnnouncementService) {
		var vm = this;
		var lang;
		vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
		vm.PrequalSetupStepID = Number($stateParams.PrequalStepID);
		vm.disableAdd = false;
		vm.isPeriodOpen = false;

		vm.listPengumuman = [];
		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("pengumuman-hasil-prakualifikasi");
			loadPQStepDetail();
			getIsPeriodOpen();
			loadAnnouncement();
		}

		function getIsPeriodOpen() {
			PrequalResultAnnouncementService.getIsPeriodOpen({
				ID: vm.PrequalSetupStepID
			}, function (reply) {
				vm.isPeriodOpen = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", error);
				UIControlService.unloadLoadingModal();
			})
		}

		function loadPQStepDetail() {
			PrequalResultAnnouncementService.pqStep({
				PrequalSetupStepID: vm.PrequalSetupStepID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.stepDetail = reply.data;
					console.info("stepdetail:" + JSON.stringify(vm.stepDetail));
				}
			}, function (err) {
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
				UIControlService.unloadLoadingModal();
			});
		};

		function loadAnnouncement() {
			PrequalResultAnnouncementService.announc({
				PrequalSetupStepID: vm.PrequalSetupStepID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.announc = reply.data;
					console.info(reply.data);
				}
			}, function (err) {
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
				UIControlService.unloadLoadingModal();
			});
		};

		function cekOverTime() {
			anncPrequalService.isOvertime({
				SetupStepID: vm.SetupStepID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.isOvertime = reply.data;;
				}
			}, function (err) {
				UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
				UIControlService.unloadLoadingModal();
			});
		};

		vm.openForm = openForm;
		function openForm(act, data, update) {
			var senddata = {
				PrequalSetupStep: vm.stepDetail,
				data: data,
				Act: act,
				update: update
			};

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/pengumuman-hasil-prakualifikasi/formPengumumanPrakualifikasi.html',
				controller: 'formPengumumanPQCtrl',
				controllerAs: 'formPengumumanPQCtrl',
				resolve: {
					item: function () {
						return senddata;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.publish = publish;
		function publish(data) {
			console.info(data);
			bootbox.confirm($filter('translate')('MESSAGE.PUBLISH'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					PrequalResultAnnouncementService.publishAnnounc({
						//PrequalSetupStepID: data.PrequalSetupStepID,
						ID: data.ID,
						Description: '[eProc] Prequalification Result Announcement'
						//ForPassedVendor: data.ForPassedVendor,
						//ForFailedVendor: data.ForFailedVendor
					}, function (reply) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_PUBLISH_DATA'));
						init();
					}, function (err) {
						UIControlService.unloadLoading();
					});
				}
			});
		};

		vm.sendEmail = sendEmail;
		function sendEmail() {
			anncPrequalService.sendEmail({
				ID: vm.IDPrequalAnnoun,
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.EMAIL_SENT'));
			}, function (err) {
				UIControlService.unloadLoadingModal();
			});
		}

		vm.back = back;
		function back() {
			$state.transitionTo('data-prekualifikasi-tahapan', { id: vm.PrequalSetupID });
		}

	}
})();