﻿(function () {
  'use strict';

  angular.module("app")
  .controller("PrequalResultApprvCtrl", ctrl);

  ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
      'hasilPrakualApprvSvc', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams', '$filter'];
  function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, hasilPrakualApprvSvc,
      $state, UIControlService, $uibModal, GlobalConstantService, $stateParams, $filter) {

    var vm = this;

    vm.PrequalStepID = Number($stateParams.PrequalStepID);
    vm.PrequalSetupID = Number($stateParams.PrequalSetupID);

    vm.pageSize = 10;
    vm.migrasi = [];

    vm.init = init;
    function init() {
      $translatePartialLoader.addPart('hasil-prakualifikasi-approval');
      UIControlService.loadLoading("");
      loadMigrasi(1);
    }

    vm.loadMigrasi = loadMigrasi;
    function loadMigrasi(current) {
      vm.currentPage = current;
      UIControlService.loadLoading("");
      hasilPrakualApprvSvc.GetPrequal({
        Offset: vm.pageSize * (vm.currentPage - 1),
        Limit: vm.pageSize
      }, function (reply) {
        vm.migrasi = reply.data.List;
        vm.totalItems = Number(reply.data.Count);
        UIControlService.unloadLoading();
      }, function (err) {
        UIControlService.unloadLoading("");
      });
    }

    vm.loadMigrasiAll = loadMigrasiAll;
    function loadMigrasiAll() {
      UIControlService.loadLoading("");
      hasilPrakualApprvSvc.GetDataMigrasi({
        Status: vm.PrequalStepID,
        Offset: vm.pageSize * (vm.currentPage - 1),
        Limit: 0
      },
      function (reply) {
        UIControlService.unloadLoading();
        if (reply.status === 200) {
          var data = reply.data;
          vm.arrayCheckMigrasi = reply.data.List;
          loadMigrasi(vm.currentPage);

        }
      },
      function (error) {
        UIControlService.unloadLoading();
        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
      });

    };

    vm.Checked = Checked;
    function Checked(data) {
      if (data.IsCheck == true) vm.arrayCheckMigrasi.push(data);
      else {
        vm.arrayCheckMigrasi.forEach(function (dt) {
          if (dt.ID == data.ID) vm.arrayCheckMigrasi.splice(i, 1);
        });
      }
    }

    vm.CheckedAll = CheckedAll;
    function CheckedAll() {
      if (vm.IsCheck == true) loadMigrasiAll(1);
      else vm.arrayCheckMigrasi = [];
    }

    vm.sentApproval = sentApproval;
    function sentApproval() {
      if (vm.arrayCheckMigrasi.length == 0) {
        UIControlService.msg_growl('error', "MESSAGE.NO_VENDOR");
        return;
      }
      else {
        hasilPrakualApprvSvc.sentApproval(vm.arrayCheckMigrasi, function (reply) {
          if (reply.status == 200) {
            UIControlService.msg_growl('success', "MESSAGE.SUCC_SEND");
            init();
          }
        }, function (err) {
        });
      }
    }

    vm.detailVendor = detailVendor;
    function detailVendor(data) {
      var item = data;
      var modalInstance = $uibModal.open({
        templateUrl: 'app/modules/panitia/prakualifikasi/migrasi-prakualifikasi-approval/detailVendor.modal.html',
        controller: 'detailVendorCtrl',
        controllerAs: 'detailVendorCtrl',
        resolve: { item: function () { return item; } }
      });
    };

    vm.approve = approve;
    function approve(data) {
      vm.dataApproval = data;
      bootbox.confirm($filter('translate')('MESSAGE.APPROVECONFIRM'), function (yes) {
        if (yes) {
          UIControlService.loadLoadingModal("MESSAGE.LOADING");
          hasilPrakualApprvSvc.approve(data,
          function (reply) {
            if (reply.status === 200) {
              UIControlService.unloadLoading();
              UIControlService.msg_growl('success', 'NOTIFICATION.APPROVE.SUCCESS');
              sendMail();
              sendMigrasi();
            }
          },
          function (err) {
            // UIControlService.msg_growl("error", "Gagal Akses Api!!");
          }
      );
        }
        else data.ApprovalStatus = null;
      });
    }

    vm.reject = reject;
    function reject(data) {
      vm.dataApproval = data;
      var item = data;
      var modalInstance = $uibModal.open({
        templateUrl: 'app/modules/panitia/prakualifikasi/migrasi-prakualifikasi-approval/detailApproval.modal.html',
        controller: 'detailApprovalCtrl',
        controllerAs: 'detailApprovalCtrl',
        resolve: { item: function () { return item; } }
      });
      modalInstance.result.then(function () {
        sendMail();
        init();
      });
    };

    vm.sendMail = sendMail;
    function sendMail(ID) {
      hasilPrakualApprvSvc.sendMail({
        Status: vm.dataApproval.DetailApprovalPMId,
        FilterType: vm.dataApproval.PrequalSetupStepId
      }, function (reply) {
        UIControlService.msg_growl('success', "MESSAGE.EMAIL_SENT");
      }, function (err) {

      });
    }

    vm.sendMigrasi = sendMigrasi;
    function sendMigrasi(ID) {
      UIControlService.loadLoading("");
      hasilPrakualApprvSvc.sendMigrasi({
        Status: vm.dataApproval.DetailApprovalPMId,
        FilterType: vm.dataApproval.PrequalSetupStepId
      }, function (reply) {
        UIControlService.unloadLoading();
        UIControlService.msg_growl('success', "NOTIFICATION.APPROVE.SUCCESS");
        init();
      }, function (err) {

      });
    }

  }
})();;