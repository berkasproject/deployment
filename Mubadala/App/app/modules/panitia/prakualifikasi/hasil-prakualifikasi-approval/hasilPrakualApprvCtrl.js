﻿(function () {
  'use strict';

  angular.module("app").controller("hasilPrakualApprvCtrl", ctrlApproval);

  ctrlApproval.$inject = ['item', '$translatePartialLoader', 'UIControlService', '$uibModalInstance', 'hasilPrakualApprvSvc'];
  function ctrlApproval(item, $translatePartialLoader, UIControlService, $uibModalInstance, hasilPrakualApprvSvc) {
    var vm = this;
    vm.apprvs = [];
    vm.VendorName = "";
    vm.StepID = item.PrequalStepID;
    vm.VendorPrequalID = item.VendorPrequalID;

    vm.getDetailApproval = getDetailApproval;
    function getDetailApproval() {
      $translatePartialLoader.addPart('hasil-prakualifikasi-approval');
      UIControlService.loadLoadingModal('MESSAGE.LOADING');
      hasilPrakualApprvSvc.detailApproval({
        ID: vm.StepID,
        VendorPrequalID:vm.VendorPrequalID
      }, function (reply) {
        UIControlService.unloadLoadingModal();
        if (reply.status === 200) {
          vm.apprvs = reply.data.PrequalResultApprovalHeaderModel;
          vm.VendorName = reply.data.VendorName;
          //UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
        } else {
          UIControlService.msg_growl("error", 'MESSAGE.FAIL_GETAPPROVAL', "MESSAGE.FAIL_GETAPPROVALTITLE");
          UIControlService.unloadLoadingModal();
        }
      }, function (err) {
        UIControlService.msg_growl("error", 'MESSAGE.ERR_API', "MESSAGE.API_TITLE");
        UIControlService.unloadLoadingModal();
      });
    }

    vm.closeDetailApprv = closeDetailApprv;
    vm.cancel = closeDetailApprv;
    function closeDetailApprv() {
      $uibModalInstance.close();
    }

    vm.closeDetailApprv = function () {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();