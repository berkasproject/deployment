﻿(function () {
	'use strict';

	angular.module("app").controller("PrequalStepController", ctrl);

	ctrl.$inject = ['UIControlService', '$uibModal', '$filter', '$translatePartialLoader', 'PrequalStepService'];

	function ctrl(UIControlService, $uibModal, $filter, $translatePartialLoader, PrequalStepService) {
		var vm = this;

		vm.tahapans = [];

		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.searchBy = "1";

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('tahapan-prakualifikasi');
			loadSteps();
		}

		vm.loadSteps = loadSteps;
		function loadSteps() {
			UIControlService.loadLoading('LOADING');
			PrequalStepService.select({
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword,
				column: vm.searchBy
			}, function (reply) {
				if (reply.status === 200) {
					vm.tahapans = reply.data.List;
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "ERR_LOAD_PREQUAL_STEPS");
				}
			}, function (err) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", "ERR_LOAD_PREQUAL_STEPS");
			});
		}

		vm.addstep = addStep;
		function addStep() {
		    editStep(0);
		}

		vm.editStep = editStep;
		function editStep(id) {
			var post = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'editModalStep.html',
				controller: editStepCtrl,
				controllerAs: 'editStepCtrl',
				resolve: {
					item: function () {
						return post;
					}
				}
			});

			modalInstance.result.then(function () {
				loadSteps();
			});
		}

		vm.remove = remove;
		function remove(step) {
		    bootbox.confirm($filter('translate')('CONF_DELETE') + '<br/>' + step.PrequalStepName, function (yes) {
		        if (yes) {
		            UIControlService.loadLoading('LOADING');
		            PrequalStepService.remove({
		                PrequalStepID: step.PrequalStepID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("notice", "SUCC_REMOVE");
		                loadSteps();
		            }, function (err) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "ERR_REMOVE");
		            });
		        }
		    });
		}

		vm.viewDetail = viewDetail;
		function viewDetail(id) {
			var post = id;
			var modalInstance = $uibModal.open({
				templateUrl: 'viewDetail.html',
				controller: viewDetailCtrl,
				controllerAs: 'viewDetailCtrl',
				resolve: { item: function () { return post; } }
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			loadSteps();
		}

		vm.viewStep = viewStep;
		function viewStep(post) {
		    var post = id;
		    var modalInstance = $uibModal.open({
		        templateUrl: 'viewDetail.html',
		        controller: viewDetailCtrl,
		        controllerAs: 'viewDetailCtrl',
		        resolve: {
		            item: function () {
		                return post;
		            }
		        }
		    });
		}
	}
})();

var editStepCtrl = function (UIControlService, item, $uibModalInstance, PrequalStepService) {
	var vm = this;

	vm.goodsOrService;
	vm.selectedFormType;
	vm.stepName = '';
	vm.remark = '';
	vm.formTypes = [];

	vm.init = init;
	function init() {
	    PrequalStepService.getformtypes(function (reply) {
	        if (reply.status === 200) {
	            vm.formTypes = reply.data;
	            vm.selectedFormType = vm.formTypes[0].FormTypeID;
	        } else {
	            UIControlService.msg_growl("error", "ERR_LOAD_FORM_TYPES");
	        }
	    }, function (err) {
	        UIControlService.msg_growl("error", "ERR_LOAD_FORM_TYPES");
	    });

	    if (item > 0) {
	        PrequalStepService.getbyid({ PrequalStepID: item }, function (reply) {
	            UIControlService.loadLoadingModal('LOADING');
	            if (reply.status === 200) {
	                vm.selectedFormType = reply.data.PrequalFormType;
	                vm.stepName = reply.data.PrequalStepName;
	                vm.remark = reply.data.Remark;
	                UIControlService.unloadLoadingModal();
	            } else {
	                UIControlService.unloadLoadingModal();
	                UIControlService.msg_growl("error", "ERR_LOAD_DETAIL");
	            }
	        }, function (err) {
	            UIControlService.unloadLoadingModal();
	            UIControlService.msg_growl("error", "ERR_LOAD_DETAIL");
	        });
	    } else {
	        vm.stepName = "";
	        vm.remark = "";
	    }
	}

	vm.saveTenderStep = saveTenderStep;
	function saveTenderStep() {

	    if (!vm.stepName) {
	        UIControlService.msg_growl("error", "ERR_NO_NAME");
			return;
		}

	    var savePrequalStep = item > 0 ? PrequalStepService.update : PrequalStepService.insert;

		UIControlService.loadLoadingModal('LOADING');
		savePrequalStep({
		    PrequalFormType: vm.selectedFormType,
			PrequalStepName: vm.stepName,
			PrequalStepID: item,
			Remark: vm.remark,
		}, function (reply) {
			if (reply.status === 200) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", "SUCC_SAVE");
				$uibModalInstance.close();
			} else {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "ERR_SAVE");
			}
		}, function (err) {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", "ERR_SAVE");
		});
	}

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.dismiss();
	}
}

var viewDetailCtrl = function (UIControlService, item, $uibModalInstance, PrequalStepService) {
	var vm = this;

	vm.prequalStepName = '';
	vm.remark = '';

	UIControlService.loadLoadingModal('LOADING');
	PrequalStepService.getbyid({ PrequalStepID: item }, function (reply) {
		if (reply.status === 200) {
			vm.formTypeName = reply.data.FormTypeName;
			vm.prequalStepName = reply.data.PrequalStepName;
			vm.remark = reply.data.Remark;
			UIControlService.unloadLoadingModal();
		} else {
			UIControlService.unloadLoadingModal();
			UIControlService.msg_growl("error", 'ERR_LOAD_DETAIL');
		}
	}, function (err) {
		UIControlService.unloadLoadingModal();
		UIControlService.msg_growl("error", 'ERR_LOAD_DETAIL');
	});

	vm.closeModal = closeModal;
	function closeModal() {
		$uibModalInstance.dismiss();
	}
}