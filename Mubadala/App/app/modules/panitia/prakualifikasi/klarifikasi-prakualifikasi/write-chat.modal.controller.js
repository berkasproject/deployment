﻿(function () {
	'use strict';

	angular.module("app").controller("WriteClarificationChatCtrl", ctrl);

	ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PrequalificationClarificationChatAdminService',
		'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
	function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, PrequalificationClarificationChatAdminService,
		UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

		var vm = this;
		vm.message = "";

		function init() {

		}

		vm.simpan = simpan;
		function simpan(IsSent) {
			UIControlService.loadLoadingModal("");
			if (vm.message == "" || vm.message == " ") {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "ERR_BLANK_MESSAGE");
			}
			else {
				PrequalificationClarificationChatAdminService.AddChat({
					PrequalSetupStepID: item.PrequalStepID,
					PrequalVendorID: item.PrequalVendorID,
					Message: vm.message,
					IsSent: IsSent
				}, function (reply) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("success", "SUCC_SEND_MESSAGE");
					if (IsSent) {
						$uibModalInstance.close();
					} else {
						$uibModalInstance.dismiss();
					}
				}, function (err) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "ERR_SEND_MESSAGE");
				});
			}
		}
		function loadConfig() {
			PrequalificationClarificationChatAdminService.getByPageName("PAGE.ADMIN.PREQANNC", function (response) {
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
			});
		}
		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss();
		}
	}
})();


