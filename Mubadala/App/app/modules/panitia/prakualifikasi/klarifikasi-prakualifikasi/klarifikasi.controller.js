(function () {
	'use strict';

	angular.module("app").controller("PrequalificationClarificationController", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'PrequalificationClarificationChatAdminService', 'UIControlService', '$state', '$stateParams', '$filter'];
	function ctrl($translatePartialLoader, PrequalificationClarificationChatAdminService, UIControlService, $state, $stateParams, $filter) {
		var vm = this;

		vm.StepID = Number($stateParams.PrequalStepID);
		vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
		//vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.vendors = [];
		vm.prequalData = {};

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('klarifikasi-prakualifikasi');
			loadData(1);
		}

		vm.loadData = loadData
		function loadData(page) {
			UIControlService.loadLoading("");
			PrequalificationClarificationChatAdminService.GetStepInfo({
				PrequalStepID: vm.StepID
			}, function (reply) {
				vm.prequalData = reply.data;
				PrequalificationClarificationChatAdminService.GetVendors({
					IntParam1: vm.PrequalSetupID,
					IntParam2: vm.StepID,
					Offset: (vm.currentPage - 1) * vm.maxSize,
					Limit: vm.maxSize,
					Keyword: vm.keyword
				}, function (reply) {
					vm.vendors = reply.data.List;
					vm.totalItems = reply.data.Count
					UIControlService.unloadLoading();
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'ERR_LOAD_VENDORS');
				});
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'ERR_LOAD_PREQUAL_DATA');
			});

		};

		vm.reviseVendor = reviseVendor;
		function reviseVendor(vendor) {
			bootbox.confirm($filter('translate')('MESSAGE.SENDCONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.REVISING');
					PrequalificationClarificationChatAdminService.reviseVendor({
						PrequalVendorID: vendor.VendorID,
						PrequalSetupStepID: vm.StepID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'NOTIFICATION.REVISE.SUCCESS.MESSAGE');
							init();
						} else {
							$.growl.error({ message: "Process Failed." });
							UIControlService.unloadLoading();
						}
					}, function () {
						$.growl.error({ message: "Process Failed." });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.detail = detail;
		function detail(vendor) {
			$state.transitionTo('klarifikasi-prakualifikasi-chat', {
				PrequalSetupID: vm.PrequalSetupID,
				PrequalStepID: vm.StepID,
				PrequalVendorID: vendor.VendorID
			});
		}

		vm.kembali = kembali;
		function kembali() {
			$state.transitionTo('data-prekualifikasi-tahapan', {
				id: vm.PrequalSetupID
			});
		}
	}
})();

