(function () {
	'use strict';

	angular.module("app").controller("PrequalReportDetailCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'PrequalReportService', 'UIControlService', '$stateParams','Excel','$timeout'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, PrequalReportService, UIControlService, $stateParams,Excel,$timeout) {

		var vm = this;
		vm.init = init;
		vm.PrequalSetupID = Number($stateParams.PrequalSetupID);

		vm.arrKPI = [];
		vm.arrOntime = [];
		vm.arrSavingCost = [];

		vm.jenistender = null;
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.currentPageCP = 1;
		vm.currentPageCS = 1;

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    prequalReport();
		    prequalReportByVendor();
           
		    $translatePartialLoader.addPart('prequal-report');
           
		}
		vm.exportToExcel = function (tableId) { // ex: '#my-table'
		    var exportHref = Excel.tableToExcel('#print', 'Overall BL');
		    $timeout(function () { location.href = exportHref; }, 100); // trigger download
		}



        vm.prequalReport=prequalReport;
		function prequalReport() {
		    PrequalReportService.prequalReport({PrequalSetupID:vm.PrequalSetupID},function (reply) {
		        if (reply.status === 200) {
		            vm.pqReport = reply.data;
		            console.info("pqreport:" + JSON.stringify(vm.pqReport));

		            vm.labelSemua = [];
		            vm.lolosSemua = [];
		            vm.baranglolos = [];
		            vm.jasalolos = [];
		            vm.barangjasalolos = [];
		            vm.barangtidaklolos = [];
		            vm.jasatidaklolos = [];
		            vm.barangjasatidaklolos = [];
		            for (var i = 0; i <= vm.pqReport.PassedByArea.length-1; i++) {
		                if (vm.pqReport.PassedByArea[i].DistrictArea != null) {
		                    vm.labelSemua[i] = vm.pqReport.PassedByArea[i].DistrictArea.Name;
		                    vm.lolosSemua[i] = vm.pqReport.PassedByArea[i].Total;
		                    vm.baranglolos[i] = vm.pqReport.PassedByArea[i].Goods;
		                    vm.jasalolos[i] = vm.pqReport.PassedByArea[i].Service;
		                    vm.barangjasalolos[i] = vm.pqReport.PassedByArea[i].GoodsService;
		                }
		                else {
		                    if (vm.pqReport.PassedByArea[i].CountryArea != null) {
		                        if (localStorage.getItem("currLang") === 'id') {
                                    vm.labelSemua[i] = "Nasional";
                                }
                                else if (localStorage.getItem("currLang") === 'en') {
                                    vm.labelSemua[i] = "National";
                                }
		                        vm.lolosSemua[i] = vm.pqReport.PassedByArea[i].Total;
		                        vm.baranglolos[i] = vm.pqReport.PassedByArea[i].Goods;
		                        vm.jasalolos[i] = vm.pqReport.PassedByArea[i].Service;
		                        vm.barangjasalolos[i] = vm.pqReport.PassedByArea[i].GoodsService;
		                    }
		                    else {
		                        if (localStorage.getItem("currLang") === 'id') {
                                    vm.labelSemua[i] = "Internasional";
                                }
                                else if (localStorage.getItem("currLang") === 'en') {
                                    vm.labelSemua[i] = "International";
                                }
		                        vm.lolosSemua[i] = vm.pqReport.PassedByArea[i].Total;
		                        vm.baranglolos[i] = vm.pqReport.PassedByArea[i].Goods;
		                        vm.jasalolos[i] = vm.pqReport.PassedByArea[i].Service;
		                        vm.barangjasalolos[i] = vm.pqReport.PassedByArea[i].GoodsService;
		                    }
		                }
		            }
                    for (var i = 0; i <= vm.pqReport.NotPassedByArea.length-1; i++) {
                        if (vm.pqReport.NotPassedByArea[i].DistrictArea != null) {
                            vm.labelSemua[i] = vm.pqReport.NotPassedByArea[i].DistrictArea.Name;
                            //vm.lolosSemua[i] = vm.pqReport.NotPassedByArea[i].Total;
                            vm.barangtidaklolos[i] = vm.pqReport.NotPassedByArea[i].Goods;
                            vm.jasatidaklolos[i] = vm.pqReport.NotPassedByArea[i].Service;
                            vm.barangjasatidaklolos[i] = vm.pqReport.NotPassedByArea[i].GoodsService;
		                }
		                else {
                            if (vm.pqReport.NotPassedByArea[i].CountryArea != null) {
		                        if (localStorage.getItem("currLang") === 'id') {
                                    vm.labelSemua[i] = "Nasional";
                                }
                                else if (localStorage.getItem("currLang") === 'en') {
                                    vm.labelSemua[i] = "National";
                                }
		                        //vm.lolosSemua[i] = vm.pqReport.NotPassedByArea[i].Total;
		                        vm.barangtidaklolos[i] = vm.pqReport.NotPassedByArea[i].Goods;
		                        vm.jasatidaklolos[i] = vm.pqReport.NotPassedByArea[i].Service;
		                        vm.barangjasatidaklolos[i] = vm.pqReport.NotPassedByArea[i].GoodsService;
		                    }
		                    else {
		                        if (localStorage.getItem("currLang") === 'id') {
                                    vm.labelSemua[i] = "Internasional";
                                }
                                else if (localStorage.getItem("currLang") === 'en') {
                                    vm.labelSemua[i] = "International";
                                }
		                        //vm.lolosSemua[i] = vm.pqReport.NotPassedByArea[i].Total;
		                        vm.barangtidaklolos[i] = vm.pqReport.NotPassedByArea[i].Goods;
		                        vm.jasatidaklolos[i] = vm.pqReport.NotPassedByArea[i].Service;
		                        vm.barangjasatidaklolos[i] = vm.pqReport.NotPassedByArea[i].GoodsService;
		                    }
		                }
		            }
		            //console.info("label:" + JSON.stringify(vm.labelSemua));
		            grafikSemua(vm.labelSemua, vm.lolosSemua);
		            grafikBarangLolos(vm.labelSemua, vm.baranglolos);
		            grafikJasaLolos(vm.labelSemua, vm.jasalolos);
		            grafikBarangJasaLolos(vm.labelSemua, vm.barangjasalolos);
		            grafikBarangTidakLolos(vm.labelSemua, vm.barangtidaklolos);
		            grafikJasaTidakLolos(vm.labelSemua, vm.jasatidaklolos);
		            grafikBarangJasaTidakLolos(vm.labelSemua, vm.barangjasatidaklolos);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data modify vendor");
		        UIControlService.unloadLoading();
		    });
		}

		function grafikSemua(label, arr) {
		    vm.dataSemua = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.data);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelSemua = label;
		        vm.datasetSemua = [{
		            label: "Semua"
		        }];

		        vm.optionSemua = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelSemua = label;
		        vm.datasetSemua = [{
		            label: "Semua"
		        }];

		        vm.optionSemua = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}

		function grafikBarangLolos(label, arr) {
		    vm.dataBarang = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.dataBarang);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelBarang = label;
		        vm.datasetBarang = [{
		            label: "Barang"
		        }];

		        vm.optionBarang = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelBarang = label;
		        vm.datasetBarang = [{
		            label: "Goods"
		        }];

		        vm.optionBarang = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}

		function grafikJasaLolos(label, arr) {
		    vm.dataJasa = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.dataJasa);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelJasa = label;
		        vm.datasetJasa= [{
		            label: "Jasa"
		        }];

		        vm.optionJasa = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelJasa = label;
		        vm.datasetJasa = [{
		            label: "Service"
		        }];

		        vm.optionJasa = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}


		function grafikBarangJasaLolos(label, arr) {
		    vm.dataBarangJasa = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.dataBarangJasa);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelBarangJasa = label;
		        vm.datasetBarangJasa = [{
		            label: "Barang dan Jasa"
		        }];

		        vm.optionBarangJasa = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelBarangJasa = label;
		        vm.datasetBarangJasa = [{
		            label: "Goods and Service"
		        }];

		        vm.optionBarangJasa = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}

		function grafikBarangTidakLolos(label, arr) {
		    vm.dataBarangTidakLolos = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.dataBarangTidakLolos);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelBarangTidakLolos = label;
		        vm.datasetBarangTidakLolos = [{
		            label: "Barang"
		        }];

		        vm.optionBarangTidakLolos = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelBarangTidakLolos = label;
		        vm.datasetBarangTidakLolos = [{
		            label: "Goods"
		        }];

		        vm.optionBarangTidakLolos = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}

		function grafikJasaTidakLolos(label, arr) {
		    vm.dataJasaTidakLolos = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.dataJasaTidakLolos);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelJasaTidakLolos = label;
		        vm.datasetJasaTidakLolos = [{
		            label: "Jasa"
		        }];

		        vm.optionJasaTidakLolos = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelJasaTidakLolos = label;
		        vm.datasetJasaTidakLolos = [{
		            label: "Service"
		        }];

		        vm.optionJasaTidakLolos = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}


		function grafikBarangJasaTidakLolos(label, arr) {
		    vm.dataBarangJasaTidakLolos = arr;
		    //console.info("data semua:" + JSON.stringify(vm.dataSemua));
		    var maxvalue = Math.max.apply(Math, vm.dataBarangJasaTidakLolos);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelBarangJasaTidakLolos = label;
		        vm.datasetBarangJasaTidakLolos = [{
		            label: "Barang dan Jasa"
		        }];

		        vm.optionBarangJasaTidakLolos = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Vendor'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    } else {
		        vm.labelBarangJasaTidakLolos = label;
		        vm.datasetBarangJasaTidakLolos = [{
		            label: "Goods and Service"
		        }];

		        vm.optionBarangJasaTidakLolos = {

		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Count of Vendor(s)'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Area'
		                    }
		                }]
		            }
		        };
		    }
		}


		vm.prequalReportByVendor = prequalReportByVendor;
		function prequalReportByVendor() {
		    PrequalReportService.prequalReportByVendor({ PrequalSetupID: vm.PrequalSetupID }, function (reply) {
		        if (reply.status === 200) {
		            vm.pqReportVendor = reply.data;
		            console.info("pqreportVendor:" + JSON.stringify(vm.pqReportVendor));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data modify vendor");
		        UIControlService.unloadLoading();
		    });
		}


	}
})();
