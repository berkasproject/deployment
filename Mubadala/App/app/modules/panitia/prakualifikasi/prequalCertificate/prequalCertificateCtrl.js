﻿(function () {
	'use strict';

	angular.module("app").controller("prequalCertificateCtrl", ctrl);

	ctrl.$inject = ['$stateParams', 'PrequalCertificateService', 'UIControlService', '$filter', '$translatePartialLoader', 'GlobalConstantService', '$http', 'FileSaver', '$uibModal'];

	function ctrl($stateParams, PrequalCertificateService, UIControlService, $filter, $translatePartialLoader, GlobalConstantService, $http, FileSaver, $uibModal) {
		var vm = this;
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");

		vm.prequalId = Number($stateParams.PrequalSetupID);
		vm.stepId = Number($stateParams.SetupStepID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('prequal-certificate');
			getStepData();
		}

		vm.changeAll = changeAll;
		function changeAll() {
			for (var i = 0; i < vm.Vendors.length; i++) {
				if ((vm.Vendors[i].CertificateUrl !== null || vm.Vendors[i].CertificateUrl !== '') && vm.Vendors[i].IsCertSent === false) {
					if (vm.IsPublishAll)
						vm.Vendors[i].IsSendCert = true;
					else
						vm.Vendors[i].IsSendCert = false;
				}
			}
		}

		function getStepData() {
			PrequalCertificateService.GetStepData({
				PrequalSetupID: vm.prequalId
			}, function (reply) {
				var result = reply.data;
				vm.StartDate = result.StartDate;
				vm.EndDate = result.EndDate;
				vm.PrequalSetupName = result.PrequalSetupName;
				vm.Vendors = result.Vendors;
				vm.IsAllowPublish = result.IsAllowPublish;
				vm.PrequalCode = result.PrequalCode;

				UIControlService.unloadLoading();
			}, function (error) {
				UIControlService.unloadLoading();
			});
		}

		vm.uploadCert = uploadCert;
		function uploadCert(dataVendor) {
			var item = {
				PrequalSetupStepID: vm.stepId,
				PrequalSetupID: vm.prequalId,
				VendorID: dataVendor.VendorID,
				PrequalCode: vm.PrequalCode,
				Vendors: vm.Vendors
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/prequalCertificate/uploadVendorCertificate.html',
				controller: 'prequalCertificateModalCtrl',
				controllerAs: 'prequalCertificateModalCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function (detail) {
				vm.Detail = detail;
				getStepData();
			});
		}

		vm.downloadCert = downloadCert;
		function downloadCert(dataVendor) {
			var headers = {};
			headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

			$http({
				method: 'POST',
				url: vendorpoint + '/PrequalCertificate/GenerateCertificate',
				headers: headers,
				data: { PrequalSetupStepID: vm.stepId, PrequalSetupID: vm.prequalId, VendorID: dataVendor.VendorID },
				responseType: 'arraybuffer'
			}).success(function (data, status, headers) {
				headers = headers();

				var contentType = headers['content-type'];
				var linkElement = document.createElement('a');
				var fileName = "";

				try {
					var blob = new Blob([data], { type: contentType });
					FileSaver.saveAs(blob, 'Prequal_Certificate_' + dataVendor.VendorName.trim().replace(' ', '_') + '.pdf');
				} catch (e) {
					console.log(e);
				}
			});
		}

		vm.publish = publish;
		function publish() {
			bootbox.confirm($filter('translate')('MESSAGE.SENDCONFIRM'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.PUBLISHING');
					if (vm.IsPublishAll) {
						PrequalCertificateService.publishAll({
							PrequalSetupStepID: vm.stepId
						}, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								UIControlService.msg_growl("success", 'NOTIFICATION.SEND.SUCCESS.MESSAGE');
								init();
								vm.IsPublishAll = false;
								//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							} else {
								$.growl.error({ message: "Publish Failed." });
								UIControlService.unloadLoading();
							}
						}, function (err) {
							$.growl.error({ message: "Gagal Akses API >" + err });
							UIControlService.unloadLoading();
						});
						return;
					}

					PrequalCertificateService.publish({
						Vendors: vm.Vendors,
						PrequalSetupStepID: vm.stepId
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'NOTIFICATION.SEND.SUCCESS.MESSAGE');
							init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Publish Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}
	}
})();