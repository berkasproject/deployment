(function () {
    'use strict';

    angular.module("app")
    .controller("detailVendorCtrl", ctrl);

    ctrl.$inject = ['$uibModal', '$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'MigrasiPrakualService'];
    /* @ngInject */
    function ctrl($uibModal, $state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, MigrasiPrakualService) {

        var vm = this;

        vm.init = init;
        function init() {
            vm.listVendor = item.listDetailVendor;
            $translatePartialLoader.addPart('verifikasi-tender');
        };

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.detailKelengkapan = detailKelengkapan;
        function detailKelengkapan(data) {
            var item = data;
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/migrasi-prakualifikasi-approval/detailKelengkapanVendor.modal.html',
                controller: 'detailKelengkapanCtrl',
                controllerAs: 'detailKelengkapanCtrl',
                resolve: { item: function () { return item; } }
            });
        };
    }
})();