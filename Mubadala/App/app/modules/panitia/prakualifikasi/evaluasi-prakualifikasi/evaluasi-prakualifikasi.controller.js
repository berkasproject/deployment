﻿(function () {
    'use strict';

    angular.module("app")
    .controller("PrequalEvaluationController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PrequalEvaluationService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PrequalEvaluationService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams) {

        var vm = this;

        vm.PrequalStepID = Number($stateParams.PrequalStepID);
        vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
        
        vm.evaluations = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-prakualifikasi');
            loadStep();
            loadEvals();
        }

        function loadStep() {
            PrequalEvaluationService.GetStepData({
                ID: vm.PrequalStepID
            }, function (reply) {
                vm.step = reply.data;
                vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
            }, function (err) {
                
            });
        }

        function loadEvals() {
            UIControlService.loadLoading("");
            PrequalEvaluationService.GetEvaluations({
                ID: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading("");
                vm.evaluations = reply.data;
            }, function (err) {
                UIControlService.unloadLoading("");
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
            });
        }
    }
})();;