(function () {
	'use strict';
	angular.module("app")
    .controller("PrequalEvaluationBalanceController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'PrequalEvaluationService', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, PrequalEvaluationService, $http, $uibModal, $translate, $translatePartialLoader, $location, UIControlService) {
	    var vm = this;

	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
	    vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
	    vm.VendorID = Number($stateParams.VendorID);        

	    vm.assets = [];
	    vm.debts = [];

	    vm.isNotSavedYet = false;

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('evaluasi-prakualifikasi');
		    loadStep();
        }

		function loadStep() {

		    PrequalEvaluationService.GetStepData({
		        ID: vm.PrequalStepID
		    }, function (reply) {
		        vm.step = reply.data;
		        vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
		        vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
		    }, function (err) {

		    });
		    
		    PrequalEvaluationService.GetVendorName({
		        VendorID: vm.VendorID
		    }, function (reply) {
		        vm.VendorName = reply.data;
		    }, function (err) {

		    });

		    loadAssets();
		}

		function loadAssets() {
		    UIControlService.loadLoading("");
		    PrequalEvaluationService.GetAssets({
		        PrequalStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		    },
            function (reply) {
                UIControlService.unloadLoading();
                vm.assets = reply.data;
                vm.totalAssets = 0;
                vm.assets.forEach(function (a) {
                    vm.totalAssets += a.Nominal;
                });
                loadDebts();
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETBALANCE");
                UIControlService.unloadLoading();
            });
		}

		function loadDebts() {
		    UIControlService.loadLoading("");
		    PrequalEvaluationService.GetDebts({
		        PrequalStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		    },
            function (reply) {
                UIControlService.unloadLoading();
                vm.debts = reply.data;
                vm.totalDebts = 0;
                vm.debts.forEach(function (a) {
                    vm.totalDebts += a.Nominal;
                });

                vm.totalModal = vm.totalAssets - vm.totalDebts;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETBALANCE");
                UIControlService.unloadLoading();
            });
		}
	}
})();