(function () {
	'use strict';
	angular.module("app")
    .controller("PrequalEvaluationExperienceController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'PrequalEvaluationService', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, PrequalEvaluationService, $http, $uibModal, $translate, $translatePartialLoader, $location, UIControlService) {
	    var vm = this;

	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
	    vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
	    vm.VendorID = Number($stateParams.VendorID);        

	    vm.fields = [];

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('evaluasi-prakualifikasi');
		    loadStep();
        }

		function loadStep() {

		    PrequalEvaluationService.GetStepData({
		        ID: vm.PrequalStepID
		    }, function (reply) {
		        vm.step = reply.data;
		        vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
		        vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
		    }, function (err) {

		    });

		    PrequalEvaluationService.GetVendorName({
		        VendorID: vm.VendorID
		    }, function (reply) {
		        vm.VendorName = reply.data;
		    }, function (err) {

		    });

		    loadFields();
		}

		function loadFields() {
		    UIControlService.loadLoading("");
		    PrequalEvaluationService.GetBusinessFields({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		    },
            function (reply) {
                vm.fields = reply.data;
                loadExperience(0);
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETEXP");
                UIControlService.unloadLoading();
            });
		}

		function loadExperience(index) {
		    if (index < vm.fields.length) {
		        vm.fields[index].experiences = [];
		        PrequalEvaluationService.GetExpsByField({
		            PrequalSetupStepID: vm.PrequalStepID,
		            VendorID: vm.VendorID,
		            BusinessField: vm.fields[index].ID
		        },
                function (reply) {
                    vm.fields[index].experiences = reply.data;
                    calculateTotalMonths(vm.fields[index]);
                    loadExperience(index + 1);
                }, function (err) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETEXP");
                });
		    } else {
		        UIControlService.unloadLoading();
		    }
        }

		function calculateTotalMonths(field) {
		    PrequalEvaluationService.CalculateTotalMonths({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		        BusinessField: field.ID
		    },
            function (reply) {
                field.totalMonths = reply.data;
                field.avgMonths = field.totalMonths / 12;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_MONTH");
            });
        }
    }
})();