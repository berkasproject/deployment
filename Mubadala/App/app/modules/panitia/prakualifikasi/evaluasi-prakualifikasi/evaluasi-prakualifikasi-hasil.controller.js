(function () {
	'use strict';
	angular.module("app")
    .controller("PrequalEvaluationResultController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'PrequalEvaluationService', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'UIControlService'];
	/* @ngInject */
	function ctrl($state, $stateParams, PrequalEvaluationService, $http, $uibModal, $translate, $translatePartialLoader, $location, UIControlService) {
	    var vm = this;

	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
	    vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
	    vm.VendorID = Number($stateParams.VendorID);        

	    vm.fields = [];

	    vm.isTime = false;
	    vm.isNotSavedYet = false;

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('evaluasi-prakualifikasi');
		    PrequalEvaluationService.CheckTime({
		        ID: vm.PrequalStepID
		    }, function (reply) {
		        vm.isTime = reply.data;
                loadStep();
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_CHECKTIME");
		    });
        }

		function loadStep() {

		    PrequalEvaluationService.GetStepData({
		        ID: vm.PrequalStepID
		    }, function (reply) {
		        vm.step = reply.data;
		        vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
		        vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
		    }, function (err) {

		    });
		    
		    PrequalEvaluationService.GetVendorName({
		        VendorID: vm.VendorID
		    }, function (reply) {
		        vm.VendorName = reply.data;
		    }, function (err) {

		    });
		    loadSavedEvaluations();
		}

		function loadSavedEvaluations() {
		    UIControlService.loadLoading("");
		    PrequalEvaluationService.GetSavedEvaluation({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		    },
            function (reply) {
                UIControlService.unloadLoading();
                vm.fields = reply.data;
                if (vm.fields.length > 0) {
                    vm.isNotSavedYet = false;
                    vm.questionnaireCriterias = [];
                    vm.fields[0].QuestionnaireScores.forEach(function (q) {
                        vm.questionnaireCriterias.push(q.CriteriaName);
                    });
                } else {
                    reloadEvaluations();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETBF");
                UIControlService.unloadLoading();
            });
		}

		vm.reloadEvaluations = reloadEvaluations;
		function reloadEvaluations() {
		    UIControlService.loadLoading("");
		    PrequalEvaluationService.GetCommodityPrequals({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		    },
            function (reply) {
                UIControlService.unloadLoading();
                vm.fields = reply.data;
                if (vm.fields.length > 0) {
                    vm.isNotSavedYet = true;
                    vm.questionnaireCriterias = [];
                    vm.fields[0].QuestionnaireScores.forEach(function (q) {
                        vm.questionnaireCriterias.push(q.CriteriaName);
                    });
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETBF");
                UIControlService.unloadLoading();
            });
		}

		vm.saveEvaluations = saveEvaluations;
		function saveEvaluations() {

		    var evaluation = {
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorID: vm.VendorID,
		        Details: vm.fields
		    }

		    UIControlService.loadLoading("");
		    PrequalEvaluationService.SaveEvaluation(evaluation, function (reply) {
                UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE");
                UIControlService.unloadLoading();
                loadSavedEvaluations();
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                UIControlService.unloadLoading();
            });
		}
	}
})();