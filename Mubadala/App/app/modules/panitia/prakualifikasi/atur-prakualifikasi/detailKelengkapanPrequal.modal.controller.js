(function () {
    'use strict';

    angular.module("app")
    .controller("detKelPrequalCtrl", ctrl);

    ctrl.$inject = ['item', 'AturPrakualService', '$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance',  '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl(item, AturPrakualService, $state, $scope, $http, $filter, $stateParams, $uibModalInstance,  $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.listKelengkapan = [];
        vm.listSaveKelengkapan = [];
        vm.IsOpenAll = false;


        vm.init = init;
        function init() {
            vm.IsPublish = item.IsPublish;
            vm.listClassification = item.listClassification;
            vm.IsPublish = vm.IsPublish == undefined ? false : vm.IsPublish;
            $translatePartialLoader.addPart("permintaan-ubah-data");
            loadkelengkapan();
        };

        vm.loadkelengkapan = loadkelengkapan;
        function loadkelengkapan() {
            AturPrakualService.loadAllKelengkapan(vm.listClassification, function (response) {
                if (response.status == 200) {
                    vm.listKelengkapan = response.data;
                    if (item.listKelengkapan.length != 0) {
                        vm.listKelengkapan.forEach(function (data) {
                            item.listKelengkapan.forEach(function (dataitem) {
                                if (dataitem.DetailReffId == data.RefID)
                                {
                                    data.checked = true;
                                    vm.listSaveKelengkapan.push({
                                        DetailReffId: data.RefID
                                    });
                                }
                                if (data.Name == "K3L_POINT") {
                                    data.checked = true;
                                    vm.listSaveKelengkapan.push({
                                        DetailReffId: data.RefID
                                    });
                                }
                            });
                        });
                        if (vm.listSaveKelengkapan.length == vm.listKelengkapan.length) vm.IsOpenAll = true;

                    }
                    else {
                        vm.listKelengkapan.forEach(function (data) {
                            if (data.Name == "K3L_POINT") {
                                data.checked = true;
                                vm.listSaveKelengkapan.push({
                                    DetailReffId: data.RefID
                                });
                            }
                        });
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }
        
        vm.change = change;
        function change(data) {
            if (data.checked == true)
                vm.listSaveKelengkapan.push({
                    DetailReffId : data.RefID
                });
            else {
                for (var i = 0; i < vm.listSaveKelengkapan.length; i++) {
                    if (vm.listSaveKelengkapan[i].DetailReffId == data.RefID) {
                        vm.listSaveKelengkapan.splice(i, 1);
                    }
                }
                
            }
        }

        vm.changeAll = changeAll;
        function changeAll(flag) {
            for (var i = 0; i < vm.listKelengkapan.length; i++) {
                if (vm.listKelengkapan[i].Value !== "K3L_POINT") {
                    vm.listKelengkapan[i].checked = flag;
                    if (flag == true) vm.listSaveKelengkapan.push({
                        DetailReffId: vm.listKelengkapan[i].RefID
                    });
                    else {
                        vm.listSaveKelengkapan = [];
                    }
                }
            }
        }


        vm.save = save;
        function save() {
            $uibModalInstance.close(vm.listSaveKelengkapan);
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();