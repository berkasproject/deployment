﻿(function () {
	'use strict';

	angular.module("app").controller('addSetupCtrl', ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'AturPrakualService', 'UIControlService', '$uibModalInstance', 'item', '$uibModal'];
	function ctrl($translatePartialLoader, AturPrakualService, UIControlService, $uibModalInstance, item, $uibModal) {
		var vm = this;
		vm.prequalSteps = {};
		vm.isEdit = false;
		vm.allowEdit = true;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('prequal-setup');

			getPrequalMethod();
			getVPEvalMethod();
			console.info(item);
			if (item != null) {
				vm.setupName = item.Name;
				vm.selectedPrequalMethod = item.PrequalMethodID;
				vm.selectedVPEvalMethod = item.VPEvalMethodID;
				vm.IsPublish = item.IsPublish;
				vm.ID = item.ID;
				vm.isEdit = true;
				vm.prequalCode = item.PrequalCode;
				vm.Commitees = item.Commitees;
				change();
			} else {
				getPrequalCode();
			}
		}

		function getPrequalCode() {
			AturPrakualService.getPrequalCode(function (reply) {
				UIControlService.loadLoadingModal('LOADING.PREQUALMETHOD');
				if (reply.status === 200) {
					vm.prequalCode = reply.data;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALCODE.ERROR', "NOTIFICATION.GET.PREQUALCODE.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALCODE.ERROR', "NOTIFICATION.GET.PREQUALCODE.TITLE");
			});
		}

		function getPrequalMethod() {
			AturPrakualService.getPrequalMethod(function (reply) {
				UIControlService.loadLoadingModal('LOADING.PREQUALMETHOD');
				if (reply.status === 200) {
					vm.prequalMethods = reply.data;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
			});
		}

		function getVPEvalMethod() {
			AturPrakualService.getVPEvalMethod(function (reply) {
				UIControlService.loadLoadingModal('LOADING.VP_EVAL_METHOD');
				if (reply.status === 200) {
					vm.vpEvalMethods = reply.data;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.VP_EVAL_METHOD.ERROR', "NOTIFICATION.GET.VP_EVAL_METHOD.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.VP_EVAL_METHOD.ERROR', "NOTIFICATION.GET.VP_EVAL_METHOD.TITLE");
			});
		}

		vm.change = change;
		function change() {
			AturPrakualService.getPrequalStep({ PrequalMethodID: vm.selectedPrequalMethod }, function (reply) {
				UIControlService.loadLoadingModal('LOADING.PREQUALMETHOD');
				if (reply.status === 200) {
					vm.prequalSteps = reply.data;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALMETHOD.ERROR', "NOTIFICATION.GET.PREQUALMETHOD.TITLE");
			});
		}

		vm.setReviewer = setReviewer;
		function setReviewer() {
			if (vm.Commitees == null)
				vm.Commitees = [];

			var item = {
				//item: data
				item: vm,
				IsReadOnly: !vm.allowEdit
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/prakualifikasi/atur-prakualifikasi/setPrequalReviewer.html',
				controller: 'CommitteeModalCtrl',
				controllerAs: 'CommitteeModalCtrl',
				resolve: { item: function () { return item; } }
			});

			modalInstance.result.then(function (detail) {
				vm.Commitees = detail;
				//loadRFQVHS();
			});
		}

		vm.createPrequalMethod = createPrequalMethod;
		function createPrequalMethod() {
			if (vm.setupName.trim() == '' || vm.prequalSteps.length == 0 || vm.selectedPrequalMethod == null || vm.selectedPrequalMethod == 0 || vm.selectedVPEvalMethod == null || vm.selectedVPEvalMethod == 0) {
				return false;
			}

			if (vm.isEdit == true) {
				AturPrakualService.editPrequalMethod({
					ID: vm.ID,
					Name: vm.setupName,
					PrequalMethodID: vm.selectedPrequalMethod,
					VPEvalMethodID: vm.selectedVPEvalMethod,
					Commitees: vm.Commitees
				}, function (reply) {
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCC_EDIT");
						$uibModalInstance.close();
					} else {
					}
				}, function (err) {
					$.growl.error({ message: "Gagal Akses API >" + err });
					//$rootScope.unloadLoading();
				});
				return;
			}

			AturPrakualService.createPrequalMethod({
				PrequalCode: vm.prequalCode,
				Name: vm.setupName,
				PrequalMethodID: vm.selectedPrequalMethod,
				VPEvalMethodID: vm.selectedVPEvalMethod,
				Commitees: vm.Commitees
			}, function (reply) {
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_CREATE");
					$uibModalInstance.close();
				} else {
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				//$rootScope.unloadLoading();
			});
		}


		vm.closeModal = closeModal;
		function closeModal() {
			$uibModalInstance.close();
		};
	}
})();