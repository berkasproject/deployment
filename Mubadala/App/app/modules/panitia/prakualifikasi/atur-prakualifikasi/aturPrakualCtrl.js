﻿(function () {
	'use strict';

	angular.module("app").controller("AturPrequalCtrl", ctrl);

	ctrl.$inject = ['$state', '$translatePartialLoader', '$stateParams', 'UIControlService', 'AturPrakualService', '$uibModal', '$filter'];
	function ctrl($state, $translatePartialLoader, $stateParams, UIControlService, AturPrequalService, $uibModal, $filter) {
		var vm = this;
		vm.totalItems = 0;
		vm.action = '';
		vm.maxSize = 10;
		vm.currPage = 1;
		vm.allowAdd = true;
		var lang;
 

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('prequal-setup');
			vm.srchText = $stateParams.keyword;

			jLoad();
		}

		vm.act = act;
		function act(action) {
		    vm.status = "";
		    vm.keyword = "";

		    if (action == 1) {
		        vm.keyPlaceHolder = $filter('translate')('KODE_PREQUAL');
		    }

		    if (action == 2) {
		        vm.keyPlaceHolder = $filter('translate')('STATUS');
		    }

		    vm.action = action;
		    //if (vm.action == "") {
		    //vm.jLoad(1);
		    //}
		}

		//vm.addSetup = addSetup;
		//function addSetup() {
		//	var modalInstance = $uibModal.open({
		//		templateUrl: 'app/modules/panitia/prakualifikasi/atur-prakualifikasi/addModalSetup.html',
		//		controller: 'addSetupCtrl',
		//		controllerAs: 'addSetupCtrl',
		//		resolve: {
		//			item: function () {
		//				return null;
		//			}
		//		}
		//	});

		//	modalInstance.result.then(function () {
		//		jLoad();
		//	});
	    //}

		vm.addSetup = addSetup;
		function addSetup() {
		    $state.go('form-prequal', { Id: 0 });
		}

		vm.edit = edit;
		function edit(data) {
		    $state.go('form-prequal', { Id: data.ID });

		}

		vm.cariBerdasarStatus = cariBerdasarStatus;
		function cariBerdasarStatus(status) {
		    vm.status = status;
		    vm.jLoad(1);
		}

		vm.cari = cari;
		function cari(keyword) {
		    vm.keyword2 = keyword;
		    vm.jLoad(1);
		}

		vm.jLoad = jLoad;
		function jLoad() {
			UIControlService.loadLoading("LOADING.LOAD");
			var offset = (vm.currPage * 10) - 10;
			AturPrequalService.Select({
				Offset: offset,
				Limit: vm.maxSize,
                Keyword: vm.action,
                Keyword2: vm.keyword2,
			    IntParam1: vm.status
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.setups = data.List;
					vm.totalItems = Number(data.Count);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Prequal Setup" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.publish = publish;
		function publish(setup) {
		    vm.setupStep = [];
		    AturPrequalService.getDetailMethod({
		        PrequalMethodID: setup.PrequalMethodID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            var step = reply.data;
		            for (var i = 0; i < step.length; i++) {
		                vm.setupStep.push({
		                    PrequalSetupID: setup.ID,
		                    PrequalStepID: step[i].PrequalStepID,
		                    Sequence: step[i].Sequence
		                });
		            }
		            //console.info(vm.setupStep);
		            var pesan = "";

		            switch (lang) {
		                case 'id': pesan = 'Anda yakin untuk publish' + setup.Name; break;
		                default: pesan = 'Are you sure to publish ' + setup.Name; break;
		            }

		            bootbox.confirm(pesan, function (yes) {
		                if (yes) {
		                    simpan(setup.ID, vm.setupStep);
		                }
		            })
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        UIControlService.unloadLoading();
		    });
		};

		function simpan(ID, data) {
		    AturPrequalService.publish({
		        ID: ID,
		        PrequalSetupSteps: data
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
		            UIControlService.unloadLoading();
		            jLoad();
		        }else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_PUBLISH");
					UIControlService.unloadLoading();
				}
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        UIControlService.unloadLoading();
		    });
		};
	}
})();