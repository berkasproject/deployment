(function () {
	'use strict';

	angular.module("app").controller("PendaftaranPrakualifikasiCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$state', 'SocketService', 'PendaftaranPrakualifikasiService',
        'UIControlService', '$uibModal', '$stateParams'];
	function ctrl($http, $translate, $translatePartialLoader, $state, SocketService, PendaftaranPrakualifikasiService,
        UIControlService, $uibModal, $stateParams) {
	    var vm = this;

	    vm.prequalId = Number($stateParams.PrequalSetupID);
		vm.stepId = Number($stateParams.SetupStepID);		

		vm.namaPrakualifikasi;
		vm.docUrl;
	    //vm.isCancelled;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('pendaftaran-prakualifikasi');
			loadDataPrakual();
		}

		function loadDataPrakual() {
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    PendaftaranPrakualifikasiService.GetPrequalRegDoc({
		        PrequalSetupStepID: vm.stepId
		    }, function (reply) {
		        var data = reply.data;
		        vm.namaPrakualifikasi = data.PrequalSetupName;
		        vm.startDate = data.PrequalSetupStepStartDate;
		        vm.endDate = data.PrequalSetupStepEndDate;
		        vm.docUrl = data.DocUrl;
		        UIControlService.unloadLoading();
		        loadPeserta();
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DOC");
				UIControlService.unloadLoading();
			});
		}

		function loadPeserta(current) {
		    vm.register = [];
		    UIControlService.loadLoading("LOADING");
		    PendaftaranPrakualifikasiService.GetByPrequal({
		        ID: vm.prequalId
			}, function (reply) {
			    vm.register = reply.data;
			    UIControlService.unloadLoading();
			}, function (err) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_REGISTERED_VENDORS");
				UIControlService.unloadLoading();
			});
		}

		vm.lihatPendaftaranPrakualifikasi = lihatPendaftaranPrakualifikasi;
		function lihatPendaftaranPrakualifikasi() {
			var data = {
			    namaPrakual: vm.namaPrakualifikasi,
			    docUrl: vm.docUrl,
			    startDate: vm.startDate,
                endDate: vm.endDate
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/prakualifikasi/pendaftaran-prakualifikasi/detailPendaftaranPrakualifikasi.modal.html',
				controller: 'DetailPendaftaranPrakualCtrl',
				controllerAs: 'DetailPendaftaranPrakualCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.uploadDokumen = uploadDokumen;
		function uploadDokumen(tenderID, tenderStepID) {
			var data = {
			    prequalId: vm.prequalId,
			    stepId: vm.stepId
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/prakualifikasi/pendaftaran-prakualifikasi/uploadDokumen.modal.html',
				controller: 'FormUploadPendaftaranPrakualCtrl',
				controllerAs: 'FormUploadPendaftaranPrakualCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.back = back;
		function back() {
		    $state.transitionTo('data-prekualifikasi-tahapan', { id: vm.prequalId });
		}
	}
})();