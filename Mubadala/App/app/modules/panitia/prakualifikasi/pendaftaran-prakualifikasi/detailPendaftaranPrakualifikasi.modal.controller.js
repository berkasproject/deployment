﻿(function () {
    'use strict';

    angular.module("app").controller("DetailPendaftaranPrakualCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'UIControlService', '$uibModal', 'GlobalConstantService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        UIControlService, $uibModal,  GlobalConstantService, item, $uibModalInstance) {
        var vm = this;
        vm.data = item;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
   
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();