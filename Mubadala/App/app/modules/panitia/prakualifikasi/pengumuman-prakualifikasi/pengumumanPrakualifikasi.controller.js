﻿(function () {
    'use strict';

    angular.module("app")
    .controller("pengumumanPrakualCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        '$state', 'UIControlService', '$uibModal', '$stateParams','anncPrequalService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService,
        $state, UIControlService, $uibModal, $stateParams, anncPrequalService) {
        var vm = this;
        var lang;
        vm.SetupID = Number($stateParams.PrequalSetupID);
        vm.SetupStepID = Number($stateParams.SetupStepID);
        vm.disableAdd = false;

        vm.listPengumuman = [];
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart("pengumuman-prakualifikasi");
            jLoad();
        }

        function jLoad() {
            getAnnouncement();
            loadSetup();
            cekOverTime();
        };
        
        function getAnnouncement(){
            anncPrequalService.select({
                SetupStepID: vm.SetupStepID,
                SetupID:vm.SetupID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.anncdata = reply.data;
                    console.info("anncdata:" + JSON.stringify(vm.anncdata));
                    vm.TotalData = vm.anncdata.length;
                    //console.info(vm.TotalData)
                    if (vm.TotalData > 0)
                        vm.disableAdd = true;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        };
        vm.ListBusinessFields = [];
        vm.TechnicalRefID = 0;
        vm.CompanyScale = 0;
        function loadSetup() {
            anncPrequalService.getSetup({
                ID: vm.SetupID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.data = reply.data;
                    console.info("data" + JSON.stringify(vm.data));
                    if (vm.data.TechnicalRefID != undefined) {
                        vm.TechnicalRefID = vm.data.TechnicalRefID;
                    }
                    if (vm.data.CompanyScale != undefined) {
                        vm.CompanyScale = vm.data.CompanyScale;
                    }
                    vm.nama = vm.data.Name;
                    vm.ListBusinessFields = vm.data.ListBusinessFields;
                    //console.info(vm.nama);
                } 
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        };

        function cekOverTime() {
            anncPrequalService.isOvertime({
                SetupStepID: vm.SetupStepID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.isOvertime = reply.data;;
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_OPTS'));
                UIControlService.unloadLoadingModal();
            });
        };

        vm.openForm = openForm;
        function openForm(act, data) {
            //console.info("data:" + JSON.stringify(data));
            var senddata = {
                SetupID: vm.SetupID,
                SetupStepID: vm.SetupStepID,
                SetupName: vm.nama,
                Act: act,
                Data: data,
                ListBusinessFields: vm.ListBusinessFields,
                TechnicalRefID: vm.TechnicalRefID,
                CompanyScale:vm.CompanyScale
            };

            if (data == null) senddata.ID = 0;
            else senddata.ID = data.ID;

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/pengumuman-prakualifikasi/formPengumumanPrakualifikasi.html',
                controller: 'formPrakualCtrl',
                controllerAs: 'formPrakualCtrl',
                resolve: {
                    item: function () {
                        return senddata;
                    }
                }
            });
            modalInstance.result.then(function () {
                jLoad();
            });
        }

        vm.publish = publish;
        function publish(data) {
            vm.IDPrequalAnnoun = data.ID;
            var pesan = "";
            if (vm.isOvertime == true) {
                bootbox.confirm($filter('translate')('MESSAGE.PUBLISH'), function (yes) {
                    if (yes) {
                        UIControlService.loadLoading("Silahkan tunggu . . .");
                        anncPrequalService.publish({
                            ID: data.ID,
                        }, function (reply) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_PUBLISH_DATA'));
                            sendEmail(data.Description);
                            jLoad();
                        }, function (err) {
                            UIControlService.unloadLoading();
                        });
                    }
                });
            }
            else {
                UIControlService.msg_growl("error", $filter('translate')('Publish Pengumuman tidak bisa dilakukan. Periksa terlebih dahulu tanggal tahapan pengumuman prakualifikasi'));
                return;
            }
        };

        vm.sendEmail = sendEmail;
        function sendEmail(description) {
            anncPrequalService.sendEmail({
                ID: vm.IDPrequalAnnoun,
                Description: description
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", $filter('translate')('Email Sent'));
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('data-prekualifikasi-tahapan', { id: vm.SetupID });
        }

    }
})();