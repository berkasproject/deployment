﻿(function () {
    'use strict';

    angular.module("app")
    .controller("formPrakualCtrl", ctrl);

    ctrl.$inject = ['$http', '$filter','$translate', '$translatePartialLoader', '$location', 'SocketService',
        '$state', 'UIControlService', 'item', '$uibModalInstance',
        'UploadFileConfigService', 'UploaderService', '$uibModal', 'GlobalConstantService', 'anncPrequalService'];
    function ctrl($http, $filter, $translate, $translatePartialLoader, $location, SocketService,
        $state, UIControlService, item, $uibModalInstance, UploadFileConfigService,
        UploaderService, $uibModal, GlobalConstantService, anncPrequalService) {
        
        var vm = this;
        vm.listClassification = [];
        vm.DataTender = item.DataTender;
        vm.Description = '';
        vm.fileUpload;
        vm.SetupID = item.SetupID;
        vm.SetupStepID = item.SetupStepID;
        vm.SetupName = item.SetupName;
        vm.data = item.Data;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.Vendors = [];
        vm.isAdd = item.isAdd;
        vm.viewvendor = [];
        vm.IsPublish = false;
        //ng-model
        vm.TenderCode = ''; vm.TenderName = ''; vm.IsLocal = false; vm.IsNational = false; vm.IsInternational = false;
        vm.IsVendorEmails = false; vm.IsOpen = false; vm.Emails = ''; vm.CompScale = null; vm.CommodityID = null; vm.DocUrl = '';
        vm.DocUrl = '';
        vm.disabeledForm = false;
        vm.dnButton = false;
        vm.init = init;
        function init() {
            //console.info("data:" + JSON.stringify(item));
            if (vm.data != null) {
                vm.IsPublish = item.Data.IsPublish;
                vm.ID = item.Data.ID;
                vm.IsOpen = item.Data.IsOpen;
                vm.EmailContent = item.Data.AnnouncementText;
                console.info("emailcontent:" + vm.EmailContent);
                /*
                vm.BusinessField = item.Data.ListBusinessFields[0];
                vm.TechnicalRefID = item.Data.TechnicalRefID;
                vm.CompanyScale = item.Data.CompanyScale;
                */
            }
            if (vm.ID != undefined) vm.viewvendor = item.Data.Vendor;
            loadDetailPrequal();
            //loadEmailContent();
            getCompanyScale();
            getBusinessField();
            getTechnicalClassification();
            jLoad();
        }

        vm.selectedTypePrequal = {};
        vm.listTypePrequal = [];
        vm.loadPrequal = loadPrequal;
        function loadPrequal() {
            anncPrequalService.loadPrequal(function (response) {
                if (response.status == 200) {
                    vm.listTypePrequal = response.data.List;
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.selectedDetailPrequal = {};
        vm.listDetailPrequal = [];
        vm.loadDetailPrequal = loadDetailPrequal;
        function loadDetailPrequal() {
            anncPrequalService.loadDetailPrequal(function (response) {
                if (response.status == 200) {
                    vm.listDetailPrequal = response.data.List;
                    console.info("listdetailpq" + JSON.stringify(vm.listDetailPrequal));
                    if (vm.data != undefined) {
                        vm.listDetailPrequal.forEach(function (list) {
                            vm.data.Classification.forEach(function (data) {
                                if (data.RefID == list.RefID) {
                                    list.IsCheck = true;
                                    checkClassification(list);
                                }
                            })
                        })
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.getCompanyScale = getCompanyScale;
        function getCompanyScale() {
            anncPrequalService.getCompanyScale(function (response) {
                if (response.status == 200) {
                    vm.listcompanyscale = response.data;
                    if (vm.ID != 0) {
                        for (var i = 0; i <= vm.listcompanyscale.length - 1; i++) {
                            if (item.CompanyScale == vm.listcompanyscale[i].RefID) {
                                vm.CompanyScale = vm.listcompanyscale[i];
                                i = vm.listcompanyscale.length;
                            }
                        }
                    }
                    //console.info("companyscale:" + JSON.stringify(vm.listcompanyscale));
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.getBusinessField = getBusinessField;
        function getBusinessField() {
            anncPrequalService.getBusinessField(function (response) {
                if (response.status == 200) {
                    vm.listbfield = response.data;
                    //console.info("bfield:" + JSON.stringify(vm.listbfield));
                    if (vm.ID != 0) {
                        for (var i = 0; i <= vm.listbfield.length - 1; i++) {
                            if (item.ListBusinessFields[0].BusinessFieldID == vm.listbfield[i].ID) {
                                vm.BusinessField = vm.listbfield[i];
                                i = vm.listbfield.length;
                                //addBField(vm.BusinessField);
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };
        vm.getTechnicalClassification = getTechnicalClassification;
        function getTechnicalClassification() {
            anncPrequalService.getTechnicalClassification(function (response) {
                if (response.status == 200) {
                    vm.listtechnicalclassification = response.data;
                    if (vm.Id != 0) {
                        for (var i = 0; i <= vm.listtechnicalclassification.length - 1; i++) {
                            if (item.TechnicalRefID == vm.listtechnicalclassification[i].RefID) {
                                vm.TechnicalRefID = vm.listtechnicalclassification[i];
                                i = vm.listtechnicalclassification.length;
                            }
                        }
                    }
                    // console.info("tech Classf:" + JSON.stringify(vm.listtechnicalclassification));
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        vm.loadEmailContent = loadEmailContent;
        function loadEmailContent() {
            anncPrequalService.emailContent(function (response) {
                if (response.status == 200) {
                    vm.EmailContent = response.data;
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });

        };

        function jLoad() {
            UploadFileConfigService.getByPageName("PAGE.ADMIN.PREQANNC", function (response) {
                if (response.status == 200) {
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
            });
            vm.PrequalName = item.SetupName;
            if (item.Act == false) {
                vm.EmailContent = item.Data.AnnouncementText;
                vm.IsLocal = item.Data.IsLokal;
                vm.IsNational = item.Data.IsNasional;
                vm.IsInternational = item.Data.IsInternasional;
                vm.IsOpen = item.Data.IsOpen;
                if (item.Data.IsPublish == true) {
                    vm.disabeledForm = true;
                }
                if (item.Data.DocUrl !== null) {
                    vm.DocUrl = item.Data.DocUrl;
                    vm.dnButton = true;
                }
                vm.EmailContent = item.Data.AnnouncementText;
            }
        };

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
        };

        function validateFileType(file, allowedFileTypes) {
            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return false;
            }
            return true;
        }
        
        vm.checkClassification = checkClassification;
        function checkClassification(data) {
            if (data.IsCheck == true) {
                data.ClassificationRefId = data.RefID;
                vm.listClassification.push(data);
            }
            else {
                for (var i = 0; i < vm.listClassification.length; i++) {
                    if (vm.listClassification[i].ClassificationRefId == data.RefID) {
                        vm.listClassification.splice(i, 1);
                    }
                }

            }
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            vm.prefix = "Pengumuman Prakualifikasi_" + vm.SetupStepID;
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UploaderService.uploadSinglePrequalAnnc(vm.SetupStepID, vm.prefix, file, size, filters,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        vm.pathFile = url;
                        vm.name = response.data.FileName;
                        var s = response.data.FileLength;
                        if (vm.flag == 0) vm.size = Math.floor(s);
                        else if (vm.flag == 1) vm.size = Math.floor(s / (1024));

                        update();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });
        }

        vm.openDataCommVendor = openDataCommVendor;
        function openDataCommVendor() {
            var data = {
                IsLocal: vm.IsLocal,
                IsNational: vm.IsNational,
                IsInternational: vm.IsInternational,
            };
            data.getData = vm.viewvendor;
            data.IsPublish = vm.IsPublish;
            data.ID = vm.ID;

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/prakualifikasi/pengumuman-prakualifikasi/viewVendor.html',
                controller: 'ViewVendorCtrl',
                controllerAs: 'ViewVendorCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (dataVendor) {
                vm.viewvendor = dataVendor;
            });
        }

        vm.saveData = saveData;
        function saveData() {
            if (vm.ID == undefined) {
                if (vm.fileUpload == null) {
                    UIControlService.msg_growl("error", "Berkas Prakualifikasi belum ada");
                    return;
                }
                else {
                    if (vm.IsLocal == false && vm.IsNational == false && vm.IsInternational == false) {
                        UIControlService.msg_growl("error", "Area belum diisi");
                        return;
                    }
                    else {
                        if (vm.viewvendor.length == 0) {
                            if (vm.IsLocal == true) {
                                anncPrequalService.viewVendor({
                                    IsLokal: true,
                                    IsNasional: false,
                                    IsInternasional: false,
                                    Keyword: '',
                                    Offset: 0,
                                    Limit: 0
                                }, function (reply) {
                                    if (reply.status === 200) {
                                        vm.viewvendor = reply.data.List;
                                        if (vm.listClassification.length == 0) {
                                            UIControlService.msg_growl("error", "Klasifikasi Prakualifikasi belum diisi");
                                            return;
                                        }
                                        else {
                                            uploadFile();
                                        }
                                    }
                                }, function (err) {
                                    UIControlService.unloadLoadingModal();
                                });
                            }
                            else {
                                UIControlService.msg_growl("error", "Daftar Vendor belum diisi");
                                return;
                            }
                        }
                        else {
                            if (vm.listClassification.length == 0) {
                                UIControlService.msg_growl("error", "Klasifikasi Prakualifikasi belum diisi");
                                return;
                            }
                            else {
                                uploadFile();
                            }
                        }
                    }
                }
            }
            else {
                if (vm.IsLocal == false && vm.IsNational == false && vm.IsInternational == false) {
                    UIControlService.msg_growl("error", "Area belum diisi");
                    return;
                }
                else {
                    if (vm.viewvendor.length == 0) {
                        if (vm.IsLocal == true) {
                            anncPrequalService.viewVendor({
                                IsLokal: true,
                                IsNasional: false,
                                IsInternasional: false,
                                Keyword: '',
                                Offset: 0,
                                Limit: 0
                            }, function (reply) {
                                if (reply.status === 200) {
                                    vm.viewvendor = reply.data.List;
                                    if (vm.listClassification.length == 0) {
                                        UIControlService.msg_growl("error", "Klasifikasi Prakualifikasi belum diisi");
                                        return;
                                    }
                                    else {
                                        if (vm.fileUpload) uploadFile();
                                        else update();
                                    }
                                }
                            }, function (err) {
                                UIControlService.unloadLoadingModal();
                            });
                        }
                        else {
                            UIControlService.msg_growl("error", "Daftar Vendor belum diisi");
                            return;
                        }
                    }
                    else {
                        if (vm.listClassification.length == 0) {
                            UIControlService.msg_growl("error", "Klasifikasi Prakualifikasi belum diisi");
                            return;
                        }
                        else {
                            if (vm.fileUpload) uploadFile();
                            else {
                                vm.pathFile = vm.DocUrl;
                                update();
                            }
                        }
                    }
                }
            }
        };

        vm.update = update;
        function update() {
            anncPrequalService.insert({
                ID: vm.ID,
                IsOpen: vm.IsOpen,
                SetupStepID: vm.SetupStepID,
                Description: vm.EmailContent,
                IsLokal: vm.IsLocal,
                IsNasional: vm.IsNational,
                IsInternasional: vm.IsInternational,
                DocUrl: vm.pathFile,
                prequalVendor: vm.viewvendor,
                prequalClassification: vm.listClassification
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_INSERT_DATA'));
                $uibModalInstance.close();
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();