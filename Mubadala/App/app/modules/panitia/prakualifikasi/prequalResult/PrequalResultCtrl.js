﻿(function () {
	'use strict';

	angular.module("app").controller("PrequalResultCtrl", ctrl);

	ctrl.$inject = ['PrequalResultService', 'UIControlService'];
	function ctrl(PrequalResultService, UIControlService) {
		var vm = this;
		vm.pageSize = 10;
		vm.currentPageEquip = 1;
		vm.keywordEquip = '';

		vm.init = init;
		function init() {
			getDataVehicle(1);
			getDataExperience(1);
		}

		function getDataExperience(currPage) {
			vm.currPageEquip = currPage;
			PrequalResultService.getDataExperience({
				Keyword: vm.keywordExp,
				Offset: vm.pageSize * (vm.currentPageExp - 1),
				Limit: vm.pageSize,
				Keyword4: vm.vendorId
			}, function (reply) {
				UIControlService.loadLoadingModal('LOADING.PREQUALRESULTEXP');
				if (reply.status === 200) {
					vm.prequalResultExp = reply.data;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALRESULTEXP.ERROR', "NOTIFICATION.GET.PREQUALRESULTEXP.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALRESULTEXP.ERROR', "NOTIFICATION.GET.PREQUALRESULTEXP.TITLE");
			});
		}

		function getDataVehicle(currPage) {
			vm.currPageEquip = currPage;
			PrequalResultService.getVendorVehicleAndEquipment({
				Keyword: vm.keywordEquip,
				Offset: vm.pageSize * (vm.currentPageEquip - 1),
				Limit: vm.pageSize,
				Keyword4: vm.vendorId
			}, function (reply) {
				UIControlService.loadLoadingModal('LOADING.PREQUALRESULTEQUIP');
				if (reply.status === 200) {
					vm.prequalResultEquip = reply.data;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALRESULTEQUIP.ERROR', "NOTIFICATION.GET.PREQUALRESULTEQUIP.TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALRESULTEQUIP.ERROR', "NOTIFICATION.GET.PREQUALRESULTEQUIP.TITLE");
			});
		}
	}
})();