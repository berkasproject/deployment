﻿(function () {
	'use strict';

	angular.module("app").controller("PrequalResultIndexCtrl", ctrl);

	ctrl.$inject = ['PrequalResultService', 'UIControlService'];

	function ctrl(PrequalResultService, UIControlService) {
		var vm = this;
		vm.maxSize = 10;
		vm.srchText = '';
		vm.vendors = [];
		vm.totalItems = 0;
		vm.currPage = 1;

		vm.init = init;
		function init() {
			getVendorList();
		}

		vm.getVendorList = getVendorList;
		function getVendorList() {
			PrequalResultService.getVendorList({
				Offset: (vm.currPage * 10) - 10,
				Limit: vm.maxSize,
				Keyword: vm.srchText
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.vendors = reply.data.List;
					vm.totalItems = reply.data.Count;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				UIControlService.unloadLoading();
			});
		}

		vm.ViewPrequal = ViewPrequal
		function ViewPrequal() {

		}
	}
})();