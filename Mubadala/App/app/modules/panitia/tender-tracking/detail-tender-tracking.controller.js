﻿(function () {
    'use strict';

    angular.module("app").controller("DetailTenderTrackingController", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'TenderTrackingService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, TenderTrackingService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.item = item.data;

        function init() {
            detailItemPr();
        }


        vm.detailItemPr = detailItemPr;
        function detailItemPr() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            TenderTrackingService.GetStep({
                ID:vm.item.ID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    vm.data = reply.data;
                    //console.info("itempr" + JSON.stringify(vm.listItemPR));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }
    }
})();
//TODO


