﻿(function () {
	'use strict';

	angular.module("app").controller("TenderTrackingController", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'TenderTrackingService', 'UIControlService', '$filter', '$uibModal'];

	function ctrl($http, $translate, $translatePartialLoader, $location, $state, TenderTrackingService, UIControlService, $filter, $uibModal) {
		var vm = this;
		vm.init = init;
		vm.pageSize = 10;
		vm.currentPage = 1;
		vm.column = "1";

		function init() {
			$translatePartialLoader.addPart("data-pengadaan");
			//UIControlService.loadLoading("MESSAGE.LOADING");
			loadData(1);
		}
		vm.Keyword = '';

		vm.cari = cari;
		function cari() {
            loadData(1);
		}

		vm.loadData = loadData;
		function loadData(current) {
		    vm.listTender = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			//console.info("masuk");
			UIControlService.loadLoading("MESSAGE.LOADING");
			TenderTrackingService.Select({
			    column: vm.column,
			    Keyword: vm.Keyword,
				Offset: offset,
				Limit: vm.pageSize
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listTender = reply.data.List;
					vm.totalItems = reply.data.Count;
					vm.maxSize = vm.totalItems;
				}
			}, function (err) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD");
				UIControlService.unloadLoading();
			});
		}


		vm.detail = detail;
		function detail(data) {
		    var item = {
		        data: data
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/tender-tracking/detail-tender-tracking.html',
		        controller: 'DetailTenderTrackingController',
		        controllerAs: 'detTTCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () { });
		}
	}
})()