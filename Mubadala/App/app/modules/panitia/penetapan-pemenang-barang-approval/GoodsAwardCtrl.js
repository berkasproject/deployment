(function () {
	'use strict';

	angular.module("app").controller("GoodsAwardAppCtrl", ctrl);

	ctrl.$inject = ['$filter', 'Excel', '$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'GoodsAwardService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($filter, Excel, $timeout, $http, $translate, $translatePartialLoader, $location, SocketService, GoodsAwardService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		var page_id = 141;
		vm.evalsafety = [];
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.userBisaMengatur = false;
		vm.allowAdd = true;
		vm.allowEdit = true;
		vm.allowDelete = true;
		vm.kata = new Kata("");
		vm.init = init;
		vm.exportHref;
		vm.detail = [];
		vm.jLoad = jLoad;
		vm.isCalendarOpened = [false, false, false, false];
		vm.Status = 0;
		vm.StatusApprv = 3
		vm.Keyword = "";
		function init() {
			$translatePartialLoader.addPart('goods-award');
			vm.listDropdown =
            [
                { Value: 1, Name: "Kode Pengadaan" },
                { Value: 2, Name: "Nama Pengadaan" }
                //{ Value: 3, Name: "Approve" },
                //{ Value: 4, Name: "Reject" },
                //{ Value: 5, Name: "Approval Process" },
                //{ Value: 0, Name: "Semua" }
            ];
            if (localStorage.getItem("currLang") === 'id') {
				vm.listDropdownStatus =
	            [
	                { Value: 1, Name: "Approved" },
	                { Value: 2, Name: "Rejected" },
	                { Value: 3, Name: "Approval Process" },
	                { Value: 4, Name: "Semua" }
	            ];
            } else {
            	vm.listDropdownStatus =
	            [
	                { Value: 1, Name: "Approved" },
	                { Value: 2, Name: "Rejected" },
	                { Value: 3, Name: "Approval Process" },
	                { Value: 4, Name: "All" }
	            ];
        	}
			UIControlService.loadLoading("MESSAGE.LOADING");
			jLoad(1);
		}

		vm.awardReport = awardReport;
		function awardReport(tenderStepID, procPackType, vendorId) {
		    $state.transitionTo('award-report', { TenderStepDataID: tenderStepID, ProcPackType: procPackType, VendorID: vendorId });
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.currentPage = current;
			UIControlService.loadLoading("MESSAGE.LOADING");
			GoodsAwardService.selectApproval({
				Offset: vm.maxSize * (vm.currentPage - 1),
				Limit: vm.maxSize,
				Column: vm.Status,
				Status: vm.StatusApprv,
				Keyword: vm.Keyword
			}, function (reply) {
				if (reply.status === 200) {
				    vm.detail = reply.data.List;
				    console.info("detail" + JSON.stringify(vm.detail));
					vm.totalItems = Number(reply.data.Count);
					for (var i = 0; i < vm.detail.length; i++) {
						loadTaxCode(vm.detail[i]);
						loadStorageLocation(vm.detail[i]);
						vm.detail[i].PODate = new Date(Date.parse(vm.detail[i].PODate));
						if (vm.detail[i].ApprovalStatusReff == null) {
							vm.detail[i].Status = 'Draft';
							vm.detail[i].flagTemp = 0;
						} else if (vm.detail[i].ApprovalStatusReff != null) {
							cekEmployee(vm.detail[i].ID, vm.detail[i]);
							vm.detail[i].Status = vm.detail[i].ApprovalStatusReff.Value;
							vm.detail[i].flagTemp = 1;
						}

						if (vm.detail[i].Status === 'Draft' || vm.detail[i].Status === 'REJECT') {
							vm.detail[i].flagApprove = true;
						} else {
							vm.detail[i].flagApprove = false;
						}
						vm.detail[i].StartContractDate = new Date(Date.parse(vm.detail[i].StartContractDate));
					}
					vm.count = reply.data.Count;
					UIControlService.unloadLoading();
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.show = show;
		function show() {
			//vm.Keyword = "";
			//if (!(vm.StatusApprv == 1 || vm.StatusApprv == 2)) {
			jLoad(1);
			//}
		}

		vm.loadTaxCode = loadTaxCode;
		function loadTaxCode(data) {
			GoodsAwardService.selectTaxCode(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listTaxCode = reply.data;
					for (var x = 0; x < reply.data.length; x++) {
						if (data.TaxCode == reply.data[x].ID) {
							data.selectTaxCode = reply.data[x];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadStorageLocation = loadStorageLocation;
		function loadStorageLocation(data) {
			GoodsAwardService.selectStorageLocation(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					data.listStorageLocation = reply.data;
					for (var y = 0; y < reply.data.length; y++) {
						if (data.StorageLocation == reply.data[y].ID) {
							data.selectStorageLocation = reply.data[y];
							break;
						}
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.cekEmployee = cekEmployee;
		function cekEmployee(Id, reff) {
			GoodsAwardService.CekEmployee({
				ID: Id
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					reff.flagEmp = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString(data) { // TIMEZONE (-)
			if (data) {
				data = UIControlService.getStrDate(data);
			}
		};

		//supaya muncul di date picker saat awal load
		function convertToDate(data) {
			if (data) {
				data = new Date(Date.parse(data));
			}
		}

		vm.loadExcelVendor = loadExcelVendor;
		function loadExcelVendor() {
			vm.vendor = [];
			var tender = {
				column: vm.StepID,
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}
			GoodsAwardService.selectExcelVendor(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.vendor = reply.data;
					console.info("data:" + JSON.stringify(vm.vendor));
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.save = save;
		function save(data) {
			UIControlService.loadLoadingModal("Silahkan Tunggu...");
			GoodsAwardService.updateApproval(data, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_APPRV");
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.simpan = simpan;
		vm.List = [];
		function simpan() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			for (var i = 0; i < vm.detail.length; i++) {
				if (vm.detail[i].selectTaxCode === null) {
					UIControlService.msg_growl("warning", "Tax code belum diisi !!");
					return;
				} else if (vm.detail[i].selectStorageLocation === null) {
					UIControlService.msg_growl("warning", "MESSAGE.STOR_LOC_FILL");
					return;
				} else if (vm.detail[i].PODate === null) {
					UIControlService.msg_growl("warning", "MESSAGE.DELIV_DATE_FILL");
					return;
				} else {
					var dataGoods = {
						ID: vm.detail[i].ID,
						TenderStepID: vm.detail[i].TenderStepID,
						VendorID: vm.detail[i].VendorID,
						TotalNego: vm.detail[i].TotalNego,
						TaxCode: vm.detail[i].selectTaxCode.ID,
						StorageLocation: vm.detail[i].selectStorageLocation.ID,
						PODate: UIControlService.getStrDate(vm.detail[i].PODate)
					}
					vm.List.push(dataGoods);
					if (i == vm.detail.length - 1 && vm.List.length === vm.detail.length) {
						GoodsAwardService.update(vm.List, function (reply) {
							UIControlService.unloadLoading();
							if (reply.status === 200) {
								vm.ListDeal = [];
								vm.flag = false;
								UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_DATA_PO");
								window.location.reload();

							} else {
								UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
								return;
							}
						}, function (err) {
							UIControlService.msg_growl("error", "MESSAGE.API");
							UIControlService.unloadLoadingModal();
						});
					}
				}
			}
		}

		vm.edit = edit;
		function edit(dataTabel) {
			var data = {
				item: dataTabel
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/Detail.html',
				controller: 'DetailGoodsAwardCtrl',
				controllerAs: 'DetailGoodsAwardCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.Export = Export;
		function Export(tableId) {
			vm.exportHref = Excel.tableToExcel(tableId, 'sheet name');
			$timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
		}

		function sendMail(data) {
			var email = {
				subject: 'Notifikasi Pemenang ',
				mailContent: 'Selamat Anda adalah pemendang ',
				isHtml: false,
				addresses: data
			};

			UIControlService.loadLoading("MESSAGE.LOADING_SEND_EMAIL");
			GoodsAwardService.sendMail(email, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					loadNotDeal();
					UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT")
				} else {
					UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
			});
		}

		vm.loadNotDeal = loadNotDeal;
		function loadNotDeal() {
			var model = {
				column: vm.StepID,
				Status: vm.TenderRefID,
				FilterType: vm.ProcPackType
			}
			GoodsAwardService.selectVendorNotDeal(model, function (reply) {
				console.info("data:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.detailNotDeal = reply.data;
					vm.ListNotDeal = [];
					for (var j = 0; j < vm.detailNotDeal.length; j++) {
						vm.ListNotDeal.push(vm.detailNotDeal[j].Email);
					}
					var email = {
						subject: 'Notifikasi Pemenang Tender' + vm.TenderName,
						mailContent: 'Maaf anda belum berhasil memenangkan Tender' + vm.TenderName,
						isHtml: false,
						addresses: vm.ListNotDeal
					};

					UIControlService.loadLoading("MESSAGE.LOADING_SEND_EMAIL");
					GoodsAwardService.sendMail(email,
                        function (response) {
                        	UIControlService.unloadLoading();
                        	if (response.status == 200) {
                        		UIControlService.msg_growl("notice", "EMAIL_SENT_LOSE");
                        		init();
                        	} else {
                        		UIControlService.handleRequestError(response.data);
                        	}
                        }, function (response) {
                        	UIControlService.handleRequestError(response.data);
                        	UIControlService.unloadLoading();
                        });
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.getUserLogin = getUserLogin;
		function getUserLogin() {
			GoodsAwardService.CekRequestor({
				TenderRefID: vm.TenderRefID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.flagSave = reply.data;
					console.info(vm.flagSave);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.sendToApprove = sendToApprove;
		function sendToApprove(data) {
			bootbox.confirm($filter('translate')('MESSAGE.SEND_APPRV'), function (yes) {
				if (yes) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					var dt = {
						ID: data.ID,
						TenderStepID: vm.StepID,
						flagEmp: 1
					};
					GoodsAwardService.SendApproval(dt, function (reply) {
						if (reply.status === 200) {
							console.info(reply.data);
							UIControlService.unloadLoading();
							UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
					});
				}
			});
		}
		vm.detailApproval = detailApproval;
		function detailApproval(dt, data) {
			var item = {
				IDApproval: data.IDApproval,
				ID: data.ID,
				flag: data.flagEmp,
				Status: dt
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/detailApproval.modal.html',
				controller: 'detailApprovalSignOffCtrl',
				controllerAs: 'detailApprovalSignOffCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function (ID, flag) {
				console.info(flag);
				GoodsAwardService.sendMailRFQGoods({
					Status: ID, FilterType: 1, IsApprove: flag
				}, function (reply) {
					//UIControlService.unloadLoading();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.EMAILSENT");
					} else {
						//UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
						return;
					}
				}, function (err) {
					//UIControlService.msg_growl("error", "MESSAGE.ERR_API");
					//UIControlService.unloadLoadingModal();
				});
				init();
			});
		};

		vm.createExcel = createExcel;
		function createExcel() {
			var data = {
				StepID: vm.StepID,
				TenderRefID: vm.TenderRefID,
				ProcPackType: vm.ProcPackType
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/SaveExcelNotif.html',
				controller: "SaveNotifExcel",
				controllerAs: "SaveNotifExcel",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				init();
			});
		}

		vm.GotoTenderInfo = GotoTenderInfo;
		function GotoTenderInfo(data) {
			$state.go('data-pengadaan-tahapan', { TenderRefID: data.TenderRefID, ProcPackType: data.ProcPackType });
		}
	}
})();
//TODO

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}

