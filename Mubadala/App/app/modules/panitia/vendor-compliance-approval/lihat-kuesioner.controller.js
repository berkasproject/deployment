﻿(function () {
    'use strict';

    angular.module("app").controller("LihatKuesionerReviewerCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'VendorComplianceApprovalService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, VendorComplianceApprovalService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('verifikasi-data');
            vm.data = item.data;


        }


        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }


    }
})();
//TODO


