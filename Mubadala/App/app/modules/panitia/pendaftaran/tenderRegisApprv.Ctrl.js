﻿(function () {
	'use strict';

	angular.module("app").controller("tenderRegisCtrl", ctrlApproval);

	ctrlApproval.$inject = ['item', 'UIControlService', 'PendaftaranLelangService', '$uibModalInstance', '$translatePartialLoader'];
	function ctrlApproval(item, UIControlService, PendaftaranLelangService, $uibModalInstance, $translatePartialLoader) {
		var vm = this;
		vm.apprvs = [];
		vm.StepID = item;

		vm.getDetailApproval = getDetailApproval;
		function getDetailApproval() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			$translatePartialLoader.addPart('pemasukkan-penawaran-vhs');

			PendaftaranLelangService.detailApproval({ ID: vm.StepID }, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.apprvs = reply.data;
					//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
				} else {
					$.growl.error({ message: "Get Approval Failed." });
					UIControlService.unloadLoadingModal();
				}
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoadingModal();
			});
		}

		vm.closeDetailApprv = closeDetailApprv;
		vm.cancel = closeDetailApprv;
		function closeDetailApprv() {
			$uibModalInstance.close();
		}
	}
})();