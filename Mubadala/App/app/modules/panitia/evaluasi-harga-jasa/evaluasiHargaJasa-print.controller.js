﻿(function () {
    'use strict';

    angular.module("app")
    .controller("evaluasiHargaJasaPrintController", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiHargaJasaService', 'DataPengadaanService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, EvaluasiHargaJasaService, DataPengadaanService, UIControlService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.stepID = Number($stateParams.StepID);
        vm.tenderRefID = Number($stateParams.TenderRefID);
        vm.procPackType = Number($stateParams.ProcPackType);

        vm.evaluasi = {};
        vm.tenderStepData = {};
        vm.currency = "";

        vm.isProcess = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-harga-jasa');

            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.tenderRefID,
                ProcPackageType: vm.procPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            EvaluasiHargaJasaService.getCurrency({
                TenderRefID: vm.tenderRefID,
            }, function (reply) {
                vm.currency = reply.data;
            }, function (error) {

            });

            loadStep();
        };

        function loadStep() {
            UIControlService.loadLoading(loadmsg);
            DataPengadaanService.GetStepByID({
                ID: vm.stepID
            }, function (reply) {
                vm.tenderStepData = reply.data;
                vm.tenderID = vm.tenderStepData.TenderID;
                vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
                cekApproval();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        };

        vm.cetak = cetak;
        function cetak() {
            html2canvas(document.getElementById('print'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download(vm.tenderStepData.tender.TenderName + " (Evaluasi Harga Jasa).pdf");
                }
            });
        }


        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('evaluasi-harga-jasa', { TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
        }

        function cekApproval() {
            UIControlService.loadLoading(loadmsg);
            EvaluasiHargaJasaService.isLess3Approved({
                ID: vm.stepID
            }, function (reply) {
                if (reply.data === true) {
                    EvaluasiHargaJasaService.isTechEvalApproved({
                        ID: vm.stepID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.data === true) {
                            loadData();
                        } else {
                            UIControlService.msg_growl("error", 'EVAL_TECH_NOT_APPROVED');
                            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.tenderRefID, ProcPackType: vm.procPackType, TenderID: vm.tenderID });
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_STEP_APPROVAL');
                    });
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'LESS_3_NOT_APPROVED');
                    $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.tenderRefID, ProcPackType: vm.procPackType, TenderID: vm.tenderID });
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_STEP_APPROVAL');
            });
        }

        function loadData() {
            UIControlService.loadLoading(loadmsg);
            EvaluasiHargaJasaService.getByTenderStepData({
                ID: vm.stepID
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.evaluasi = reply.data;
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
            });
        }

        vm.lakukanEvaluasi = lakukanEvaluasi;
        function lakukanEvaluasi() {

            var evaluationVendor = [];
            if (vm.evaluasi.ServiceOfferEntryEvaluationVendors) {
                vm.evaluasi.ServiceOfferEntryEvaluationVendors.forEach(function (det) {
                    evaluationVendor.push({
                        OfferEntryVendorID: det.OfferEntryVendorID,
                        VendorName: det.VendorName,
                        Score: det.Score,
                        Rank: det.Rank,
                        IsPassed: det.IsPassed,
                        OfferTotalCost: det.OfferTotalCost,
                        PriceBreakdownURL: det.PriceBreakdownURL,
                        TenderLetterURL: det.TenderLetterURL
                    });
                });
            }
            var item = {
                evaluasiID: vm.evaluasi.ID,
                noPriceLimit: vm.evaluasi.IsNoPriceLimit,
                tenderRefID: vm.tenderRefID,
                procPackageType: vm.procPackType,
                stepID: vm.stepID,
                tenderID: vm.tenderStepData.TenderID,
                evaluationVendor: evaluationVendor,
                currency: vm.currency,
                tenderStepData: {
                    ID: vm.tenderStepData.ID,
                    Summary: vm.tenderStepData.Summary,
                    DocumentUrl: vm.tenderStepData.DocumentUrl,
                    DocumentDate: vm.tenderStepData.DocumentDate
                },
                isAllowedEdit: vm.isAllowedEdit
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-harga-jasa/evaluasiHargaJasa.modal.html?v=1.000011',
                controller: 'evaluasiHargaJasaModalController',
                controllerAs: 'ehjmCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                loadData();
            });
        }

        vm.lihatSummary = lihatSummary;
        function lihatSummary() {
            var item = {
                summary: vm.tenderStepData.Summary,
                documentUrl: vm.tenderStepData.DocumentUrl,
                documentDate: vm.tenderStepData.DocumentDate
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-harga-jasa/viewSummary.modal.html',
                controller: 'summaryPenawaranController',
                controllerAs: 'summaryCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () { });
        };

        vm.lihatKuisioner = lihatKuisioner;
        function lihatKuisioner(ev) {
            var item = {
                SOEVID: ev.OfferEntryVendorID,
                vendorName: ev.VendorName
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/evaluasi-harga-jasa/detailKuisioner.modal.html',
                controller: 'detailKuisionerPenawaranController',
                controllerAs: 'dkpCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () { });
        };

        vm.detail = detail;
        function detail(id) {
            $state.transitionTo('detail-penawaran-harga-jasa',
                { SOEEVID: id, TenderRefID: vm.tenderRefID, StepID: vm.stepID, ProcPackType: vm.procPackType });
        };

        vm.viewSummary = viewSummary;
        function viewSummary() {
        };
    }
})();