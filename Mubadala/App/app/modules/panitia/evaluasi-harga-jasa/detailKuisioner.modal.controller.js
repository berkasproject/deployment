﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailKuisionerPenawaranController", ctrl);

    ctrl.$inject = ['$state', 'item', '$http', '$filter', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiHargaJasaService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, item, $http, $filter, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, EvaluasiHargaJasaService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        var soevID = item.SOEVID;
        vm.vendorName = item.vendorName;
        vm.questionnaires = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-harga-jasa');
            loadData();
        };

        function loadData() {
            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiHargaJasaService.getQuestionnaires({
                ID: soevID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.questionnaires = reply.data;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_QUESTIONNAIRES');
            });
        };

        vm.keluar = keluar;
        function keluar() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();