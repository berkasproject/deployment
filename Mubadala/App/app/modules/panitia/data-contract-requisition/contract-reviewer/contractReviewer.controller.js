(function () {
    'use strict';

    angular.module("app")
    .controller("contractReviewerCtrl", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'ContractRequisitionReviewService', 'DataContractRequisitionService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, ContractRequisitionReviewService, DataContractRequisitionService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.totalItems = 0;
        vm.keyword = "";
        vm.column = 1;

        vm.contractRequisition = [];

        vm.statusLabels = [];
        vm.statusLabels["CR_PROCESS_2"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_PROCESS_3"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_REJECT_1"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_REJECT_2"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_REJECT_3"] = 'STATUS.APPROVED';
        vm.statusLabels["CR_APPROVED"] = 'STATUS.APPROVED';

        vm.statusLabels["CR_REJECT_1"] = 'STATUS.REJECTED';
        vm.statusLabels["CR_PROCESS_1"] = 'STATUS.ON_PROCESS';

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            vm.loadContracts(evaluateApprovalStatus);
        };

        function evaluateApprovalStatus() {
            //DataContractRequisitionService.EvaluateApprovalStatuses(function (reply) {
            //}, function (error) {
            //    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_EVALUATE'));
            //});
        }

        vm.onSearchClick = onSearchClick;
        function onSearchClick(keyword) {
            vm.keyword = keyword;
            vm.loadContracts();
            vm.currentPage = 1;
        }

        vm.onFilterTypeChange = onFilterTypeChange;
        function onFilterTypeChange(column) {
            vm.column = column;
        }

        vm.loadContracts = loadContracts;
        function loadContracts(callback) {
            UIControlService.loadLoading(loadmsg);
            ContractRequisitionReviewService.SelectCR({
                Keyword: vm.keyword,
                Offset: vm.pageSize * (vm.currentPage - 1),
                Limit: vm.pageSize,
                Column: vm.column
            }, function (reply) {
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                    vm.contractRequisition = reply.data.List;
                    vm.totalItems = reply.data.Count;
                    if (callback) {
                        callback();
                    }
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        vm.detailReviewer = detailReviewer;
        function detailReviewer(dt) {
            var item = {
                contractRequisitionId: dt.ContractRequisitionId,
                tenderCode: dt.TenderCode,
                projectTitle: dt.ProjectTitle,
                status: vm.statusLabels[dt.StatusName]
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-contract-requisition/contract-reviewer/detailReviewer.modal.html',
                controller: 'detailReviewerCtrl',
                controllerAs: 'detRevCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                loadContracts();
            });
        };

        vm.menujuDokumen = menujuDokumen;
        function menujuDokumen(dt) {
            $state.transitionTo('contract-requisition-docs-rev', { contractRequisitionId: dt.ContractRequisitionId });
        };

        vm.detailContract = detailContract;
        function detailContract(dt) {
            $state.transitionTo('detail-contract-requisition-rev', { contractRequisitionId: dt.ContractRequisitionId });
        };

        vm.convertDate = convertDate;
        function convertDate(date) {
            return UIControlService.convertDate(date);
        }
    }
})();