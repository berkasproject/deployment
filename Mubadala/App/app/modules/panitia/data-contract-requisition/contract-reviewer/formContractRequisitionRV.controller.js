(function () {
    'use strict';

    angular.module("app")
    .controller("formContractReqRVCtrl", ctrl);
    
    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataContractRequisitionService', 'UIControlService', 'ContractRequisitionReviewService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataContractRequisitionService, UIControlService, ContractRequisitionReviewService) {

        var vm = this;
        var contractRequisitionId = Number($stateParams.contractRequisitionId);
        vm.contractRequisitionId = Number($stateParams.contractRequisitionId);
        var loadmsg = "";
        vm.isTenderVerification = true;

        vm.contractRequisition = {};
        vm.isCalendarOpened = [false, false, false, false];
        vm.budgetDistValue = null;
        vm.budgetDistYear = null;
        vm.duration = 0;

        vm.breadcrumbs = [
            { title: "BREADCRUMB.MASTER_REQUISITION", href: "" },
            { title: "BREADCRUMB.DATA_CONTRACT_REQUISITION", href: "#/data-contract-requisition" },
            { title: "BREADCRUMB.REVIEW", href: "#/data-contract-requisition/review" },
            { title: "BREADCRUMB.DETAIL_CONTRACT_REQUISITION", href: "#/data-contract-requisition/review/detail-contract-requisition/" + $stateParams.contractRequisitionId },
            { title: "BREADCRUMB.CREATE_CONTRACT_REQUISITION", href: "" }
        ];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            $translate.refresh().then(function () {
                loadmsg = $filter('translate')('MESSAGE.LOADING');
            });
            UIControlService.unloadLoading(loadmsg);
            ContractRequisitionReviewService.IsReviewer({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    if (reply.data === true) {
                        vm.loadData();
                    } else {
                        UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.ERR_NOT_REVIEWER'));
                        $state.transitionTo('contract-requisition-review');
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REVIEWER'));
                    $state.transitionTo('contract-requisition-review');
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REVIEWER'));
                $sta$state.transitionTo('contract-requisition-review');
            });
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.SelectById2({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.contractRequisition = reply.data;
                    vm.contractRequisition.DirectAward = vm.contractRequisition.DirectAward ? "1" : "0";
                    vm.contractRequisition.BudgetStatus = vm.contractRequisition.BudgetStatus ? "1" : "0";
                    //vm.contractRequisition.OperatingOrCapitalText = vm.contractRequisition.OperatingOrCapital ? 'CAPITAL' : 'OPERATING';
                    //vm.contractRequisition.OperatingOrCapitalText = $filter('translate')(vm.contractRequisition.OperatingOrCapitalText);
                    vm.contractRequisition.OperatingOrCapital = vm.contractRequisition.OperatingOrCapital + "";
                    vm.contractRequisition.MstCurrency = {
                        Symbol: vm.contractRequisition.CurrencySymbol ? vm.contractRequisition.CurrencySymbol : "USD"
                    };
                    convertAll();
                    convertToDate();
                    vm.getDuration();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        vm.convertAll = convertAll;
        function convertAll() {
            convertAppBudget();
            convertOutBudget();
            convertTotalValue();
        }

        vm.convertAppBudget = convertAppBudget;
        function convertAppBudget() {
            if (vm.contractRequisition.MstCurrency.Symbol === "USD") {
                vm.contractRequisition.ApprovedBudgetInIDR = Number(vm.contractRequisition.ApprovedBudget) * vm.contractRequisition.RateUSDToIDR;
            }
            if (vm.contractRequisition.MstCurrency.Symbol === "IDR") {
                vm.contractRequisition.ApprovedBudgetInUSD = Number(vm.contractRequisition.ApprovedBudget) * vm.contractRequisition.RateIDRToUSD;
            }
        }

        vm.convertOutBudget = convertOutBudget;
        function convertOutBudget() {
            if (vm.contractRequisition.MstCurrency.Symbol === "USD") {
                vm.contractRequisition.OutstandingBudgetInIDR = Number(vm.contractRequisition.OutstandingBudget) * vm.contractRequisition.RateUSDToIDR;
            }
            if (vm.contractRequisition.MstCurrency.Symbol === "IDR") {
                vm.contractRequisition.OutstandingBudgetInUSD = Number(vm.contractRequisition.OutstandingBudget) * vm.contractRequisition.RateIDRToUSD;
            }
        }

        vm.convertTotalValue = convertTotalValue;
        function convertTotalValue() {
            if (vm.contractRequisition.MstCurrency.Symbol === "USD") {
                vm.contractRequisition.TotalValueInIDR = Number(vm.contractRequisition.TotalValue) * vm.contractRequisition.RateUSDToIDR;
            }
            if (vm.contractRequisition.MstCurrency.Symbol === "IDR") {
                vm.contractRequisition.TotalValueInUSD = Number(vm.contractRequisition.TotalValue) * vm.contractRequisition.RateIDRToUSD;
            }
        }

        vm.getDuration = getDuration;
        function getDuration() {
            vm.duration = (vm.contractRequisition.FinishDate - vm.contractRequisition.StartDate) / 1000 / 60 / 60 / 24;
            if (vm.duration < 0) {
                vm.duration = 0;
            }
        }

        vm.directAward = directAward;
        function directAward() {
            var item = {
                contractRequisitionId: contractRequisitionId,
                ProjectManager: vm.contractRequisition.ProjectManager,
                ProjectManagerFullName: vm.contractRequisition.ProjectManagerName
            };
            var modalInstance = $uibModal.open({
                templateUrl: "app/modules/panitia/data-contract-requisition/directAwardForm.html?v=1.000003",
                controller: "directAwardFormCPCtrl",
                controllerAs: "daFormCtrl",
                resolve: { item: function () { return item; } }
            });
        };        

        function convertToDate(){
            if (vm.contractRequisition.RequestedDate) {
                vm.contractRequisition.RequestedDate = new Date(Date.parse(vm.contractRequisition.RequestedDate));
            }
            if (vm.contractRequisition.RequiredDate) {
                vm.contractRequisition.RequiredDate = new Date(Date.parse(vm.contractRequisition.RequiredDate));
            }
            if (vm.contractRequisition.StartDate) {
                vm.contractRequisition.StartDate = new Date(Date.parse(vm.contractRequisition.StartDate));
            }
            if (vm.contractRequisition.FinishDate) {
                vm.contractRequisition.FinishDate = new Date(Date.parse(vm.contractRequisition.FinishDate));
            }
        }

        vm.print = print;
        function print(toPrint) {
            var innerContents = document.getElementById(toPrint).innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Contract Requisition Form</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();
        }

        vm.back = back;
        function back() {
            $state.transitionTo('detail-contract-requisition-rev', { contractRequisitionId: contractRequisitionId });
        };
    }
})();