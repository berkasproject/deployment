(function () {
    'use strict';

    angular.module("app")
    .controller("detailReviewerCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'ContractRequisitionReviewService', 'GlobalConstantService', 'CommonEngineService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, ContractRequisitionReviewService, GlobalConstantService, CommonEngineService) {

        var vm = this;
        var contractRequisitionId = item.contractRequisitionId;
        var loadmsg = "MESSAGE.LOADING";

        vm.item = item;
        vm.approverIDs = [];
        vm.crRevs = [];
        vm.contractReq = {};
        vm.hasApproved = true;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            UIControlService.loadLoadingModal(loadmsg);
            CommonEngineService.GetLoggedEmployee(function (reply) {
                if (reply.status === 200) {
                    //vm.employeeFullName = reply.data.FullName + ' ' + reply.data.SurName;
                    vm.employeeID = reply.data.EmployeeID;
                    vm.approverIDs.push(vm.employeeID);
                    CommonEngineService.GetDelegationEmployeeIds(function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            var delegationEmployeeIds = reply.data;
                            delegationEmployeeIds.forEach(function (deId) {
                                vm.approverIDs.push(deId);
                            });
                            vm.loadData();
                        } else {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_USER'));
            });
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            ContractRequisitionReviewService.GetCRReviewers({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.tenderCode = reply.data.TenderCode;
                vm.projectTitle = reply.data.ProjectTitle;
                vm.statusName = reply.data.StatusName;
                vm.isProcess1 = vm.statusName === "CR_PROCESS_1";
                vm.crRevs = reply.data.ContractRequisitionReviewers;
                vm.crRevs.forEach(function (crr) {
                    crr.isToApprove = isCRToApprove(crr);
                    if (crr.isToApprove === true) {
                        vm.hasApproved = false;
                    }
                    if (crr.ReviewStatus !== null) {
                        crr.ReviewStatus = crr.ReviewStatus === true ? 'APPROVED' : 'REJECTED';
                    }
                });
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_REVIEWER'));
            });
        }

        function isCRToApprove(crr) {
            for (var i = 0; i < vm.approverIDs.length; i++) {
                if (crr.EmployeeId === vm.approverIDs[i] && crr.ReviewStatus === null) {
                    return true;
                }
            }
            return false;
        }

        vm.approve = approve;
        function approve() {
            sendApproval(true);
        }

        vm.reject = reject;
        function reject() {
            sendApproval(false);
        }

        function sendApproval(reviewStatus) {
            var confirmMessage = reviewStatus ? "CONFIRM.APPROVE_CRDRAFT" : "CONFIRM.REJECT_CRDRAFT";
            bootbox.confirm(($filter('translate')(confirmMessage) + "<br/><br/>" + item.projectTitle), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal(loadmsg);
                    ContractRequisitionReviewService.SetReviewStatus({
                        ContractRequisitionId: contractRequisitionId,
                        ReviewStatus: reviewStatus,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.status === 200) {
                            $uibModalInstance.close();
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
                    });
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();