(function () {
    'use strict';

    angular.module("app")
    .controller("formContractReqCtrl", ctrl);
    
    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataContractRequisitionService', 'UIControlService','$q'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataContractRequisitionService, UIControlService,$q) {

        var vm = this;
        var contractRequisitionId = Number($stateParams.contractRequisitionId);
        vm.contractRequisitionId = Number($stateParams.contractRequisitionId);
        var loadmsg = "";

        vm.contractRequisition = {};
        vm.isCalendarOpened = [false, false, false, false];
        vm.budgetDistValue = null;
        vm.budgetDistYear = null;
        vm.duration = 0;
        vm.isTenderVerification = false;
        vm.budgetDistYearOptions = [];
        vm.requestTypeOptions = [];
        vm.commodityData = [];
        vm.entityData = [];
        vm.isPreparedPR = false;

        vm.crform;

        vm.breadcrumbs = [
            { title: "BREADCRUMB.MASTER_REQUISITION", href: "" },
            { title: "BREADCRUMB.DATA_CONTRACT_REQUISITION", href: "#/data-contract-requisition" },
            { title: "BREADCRUMB.DETAIL_CONTRACT_REQUISITION", href: "#/detail-contract-requisition/" + $stateParams.contractRequisitionId},
            { title: "BREADCRUMB.CREATE_CONTRACT_REQUISITION", href: "" }
        ];

        vm.startDateOptions = {
            minDate: addDays(vm.contractRequisition.RequiredDate, 1),
            maxDate: vm.contractRequisition.FinishDate
        };

        vm.finishDateOptions = {
            minDate: vm.contractRequisition.StartDate
        };

        vm.requestedDateOptions = {
            minDate: new Date(),
        };

        vm.requiredDateOptions = {
            minDate: new Date(),
        };

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            $translate.refresh().then(function () {
                loadmsg = $filter('translate')('MESSAGE.LOADING');
            });

            DataContractRequisitionService.GetRequestTypeOptions(function (reply) {
                vm.requestTypeOptions = reply.data;
            }, function (err){
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.ERR_LOAD_REQ_TYPE_OPTS'));
            });

            DataContractRequisitionService.GetBudgetOptions({
                ID: contractRequisitionId
            }, function (reply) {
                vm.budgetOptions = reply.data;
            }, function (err) {
                UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.ERR_LOAD_BUDGET_OPTS'));
            });

            getIsPreparePR().then(function (reply) {
                getRequestorEtc();
            })
        };

        function getRequestorEtc() {
            DataContractRequisitionService.IsRequestor({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.isRequestor = reply.data;
                DataContractRequisitionService.isCompliance(function (reply) {
                    vm.isCompliance = reply.data;
                    if (vm.isCompliance || vm.isRequestor || vm.isPreparedPR) {
                        vm.loadData();
                    } else {
                        UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.ERR_NOT_REQUESTOR'));
                        $state.transitionTo('data-contract-requisition');
                    }
                }, function () {
                    UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                $state.transitionTo('data-contract-requisition');
            });
        }

        function getIsPreparePR() {
            UIControlService.loadLoading(loadmsg);

            var defer = $q.defer();
            DataContractRequisitionService.isPreparePR({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                if (reply.status == 200) {
                    vm.isPreparedPR = reply.data;
                    defer.resolve(true)
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REQUESTOR'));
                    defer.reject()
                }
            }, function (error) {
                UIControlService.unloadLoading();
                defer.reject()
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REQUESTOR'));
                //$state.transitionTo('data-contract-requisition');
            });
            return defer.promise;
        }

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.SelectById2({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.contractRequisition = reply.data;
                    if (!vm.contractRequisition.ContractDetailsInfo) {
                        vm.contractRequisition.ContractDetailsInfo = vm.contractRequisition.ProjectTitle;
                    }
                    if (!vm.contractRequisition.RequestedDate) {
                        vm.contractRequisition.RequestedDate = new Date();
                        //vm.contractRequisition.RequiredDate = new Date();
                    }
                    vm.contractRequisition.DirectAward = 0;
                    vm.contractRequisition.BudgetStatus = vm.contractRequisition.BudgetStatus ? "1" : "0";
                    //vm.contractRequisition.OperatingOrCapitalText = vm.contractRequisition.OperatingOrCapital ? 'CAPITAL' : 'OPERATING';
                    //vm.contractRequisition.OperatingOrCapitalText = budgetTypeText[vm.contractRequisition.OperatingOrCapital];
                    //vm.contractRequisition.OperatingOrCapitalText = $filter('translate')(vm.contractRequisition.OperatingOrCapitalText);
                    //vm.contractRequisition.OperatingOrCapital = vm.contractRequisition.OperatingOrCapital + "";
                    vm.isTenderVerification = vm.contractRequisition.StatusName !== 'CR_DRAFT' && vm.contractRequisition.StatusName.lastIndexOf('CR_REJECT_', 0) !== 0 && !vm.contractRequisition.IsOERevision;
                    vm.isTenderVerification = vm.isTenderVerification || !vm.isRequestor;
                    vm.contractRequisition.MstCurrency = {
                        Symbol : vm.contractRequisition.CurrencySymbol ? vm.contractRequisition.CurrencySymbol : "USD"
                    };
                    //convertAll();
                    if (!vm.contractRequisition.ContractRequisitionBudgetDists) {
                        vm.contractRequisition.ContractRequisitionBudgetDists = [];
                    }
                    if (!vm.contractRequisition.ContractRequisitionVendorSuggestions) {
                        vm.contractRequisition.ContractRequisitionVendorSuggestions = [];
                    }
                    convertToDate();
                    vm.getDuration();
                    getCommodity();
                    getMstEntity();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        };

        function getCommodity() {
            DataContractRequisitionService.getCommodity(function (reply) {
                if (reply.status === 200) {
                    vm.commodityData = reply.data.List;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        }

        function getMstEntity() {
            DataContractRequisitionService.getMstEntity(function (reply) {
                if (reply.status === 200) {
                    vm.entityData = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
            });
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            if (!vm.isTenderVerification) {
                vm.isCalendarOpened[index] = true;
            }
        };

        vm.getDuration = getDuration;
        function getDuration() {

            var minReqDate;
            //if (vm.contractRequisition.DirectAward == 1) {
            //    minReqDate = addDays(vm.contractRequisition.RequestedDate, 3 * 7);
            //} else {
                minReqDate = addDays(vm.contractRequisition.RequestedDate, 12 * 7);
            //}

            if (vm.contractRequisition.RequiredDate < minReqDate) {
                vm.contractRequisition.RequiredDate = minReqDate
            }

            vm.requiredDateOptions = {
                minDate: minReqDate,
            };

            if (vm.contractRequisition.StartDate && vm.contractRequisition.RequiredDate >= vm.contractRequisition.StartDate) {
                vm.contractRequisition.StartDate = addDays(vm.contractRequisition.RequiredDate, 1);
            }

            if (vm.contractRequisition.FinishDate && vm.contractRequisition.StartDate >= vm.contractRequisition.FinishDate) {
                vm.contractRequisition.FinishDate = vm.contractRequisition.StartDate;
            }

            vm.duration = (vm.contractRequisition.FinishDate - vm.contractRequisition.StartDate) / 1000 / 60 / 60 / 24;
            if (vm.duration < 0) {
                vm.duration = 0;
            }

            vm.startDateOptions = {
                minDate: addDays(vm.contractRequisition.RequiredDate, 1),
                maxDate: vm.contractRequisition.FinishDate
            };

            vm.finishDateOptions = {
                minDate: vm.contractRequisition.StartDate
            };

            vm.budgetDistYearOptions = [];
            var startDateYear =  vm.contractRequisition.StartDate != null ? vm.contractRequisition.StartDate.getFullYear() : null;
            var finishDateYear = vm.contractRequisition.FinishDate != null ? vm.contractRequisition.FinishDate.getFullYear() : null;
            for (var i = startDateYear; i <= finishDateYear; i++) {
                vm.budgetDistYearOptions.push(i);
            }
        }

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }

        vm.selectOwner = selectOwner;
        function selectOwner() {
            if (!vm.isTenderVerification) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/data-contract-requisition/formContractRequisition.selectEmployeeModal.html',
                    controller: 'selectEmployeeModal',
                    controllerAs: 'selectEmployeeCtrl',
                });
                modalInstance.result.then(function (selectedOwner) {
                    vm.contractRequisition.ProjectOwner = selectedOwner.EmployeeID;
                    vm.contractRequisition.ProjectOwnerName = selectedOwner.FullName + ' ' + selectedOwner.SurName;
                });
            }
        };

        vm.convertAll = convertAll;
        function convertAll() {
            convertAppBudget();
            convertOutBudget();
            convertTotalValue();
        }

        vm.convertAppBudget = convertAppBudget;
        function convertAppBudget() {
            if (vm.contractRequisition.MstCurrency.Symbol === "USD") {
                vm.contractRequisition.ApprovedBudgetInIDR = Number(vm.contractRequisition.ApprovedBudget) * vm.contractRequisition.RateUSDToIDR;
            }
            if (vm.contractRequisition.MstCurrency.Symbol === "IDR") {
                vm.contractRequisition.ApprovedBudgetInUSD = Number(vm.contractRequisition.ApprovedBudget) * vm.contractRequisition.RateIDRToUSD;
            }
        }

        vm.convertOutBudget = convertOutBudget;
        function convertOutBudget() {
            if (vm.contractRequisition.MstCurrency.Symbol === "USD") {
                vm.contractRequisition.OutstandingBudgetInIDR = Number(vm.contractRequisition.OutstandingBudget) * vm.contractRequisition.RateUSDToIDR;
            }
            if (vm.contractRequisition.MstCurrency.Symbol === "IDR") {
                vm.contractRequisition.OutstandingBudgetInUSD = Number(vm.contractRequisition.OutstandingBudget) * vm.contractRequisition.RateIDRToUSD;
            }
        }

        vm.convertTotalValue = convertTotalValue;
        function convertTotalValue() {
            if (vm.contractRequisition.MstCurrency.Symbol === "USD") {
                vm.contractRequisition.TotalValueInIDR = Number(vm.contractRequisition.TotalValue) * vm.contractRequisition.RateUSDToIDR;
            }
            if (vm.contractRequisition.MstCurrency.Symbol === "IDR") {
                vm.contractRequisition.TotalValueInUSD = Number(vm.contractRequisition.TotalValue) * vm.contractRequisition.RateIDRToUSD;
            }
        }

        vm.addVendorSugg = addVendorSugg;
        function addVendorSugg() {
            var item = {
                currentData : vm.contractRequisition.ContractRequisitionVendorSuggestions
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/data-contract-requisition/formContractRequisition.selectVendorModal.html',
                controller: 'selectVendorModal',
                controllerAs: 'selectVendorCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function (selectedVendor) {
                vm.contractRequisition.ContractRequisitionVendorSuggestions.push({
                    VendorID: selectedVendor.VendorID,
                    VendorName: selectedVendor.VendorName,
                    ContractRequisitionId : contractRequisitionId,
                });
            });
        };

        vm.removeVendorSugg = removeVendorSugg;
        function removeVendorSugg(index) {
            vm.contractRequisition.ContractRequisitionVendorSuggestions.splice(index, 1);
        }

        vm.directAward = directAward;
        function directAward() {
            var item = {
                contractRequisitionId: contractRequisitionId,
                ProjectManager: vm.contractRequisition.ProjectManager,
                ProjectManagerFullName: vm.contractRequisition.ProjectManagerName,
                IsTenderVerification: vm.isTenderVerification
            };
            var modalInstance = $uibModal.open({
                templateUrl: "app/modules/panitia/data-contract-requisition/directAwardForm.html?v=1.000003",
                controller: "directAwardFormCtrl",
                controllerAs: "daFormCtrl",
                resolve: { item: function () { return item; } }
            });
        };

        vm.addBudgetDist = addBudgetDist;
        function addBudgetDist() {
            var newYear = true;
            for (var i = 0; i < vm.contractRequisition.ContractRequisitionBudgetDists.length; i++){
                if (vm.budgetDistYear === vm.contractRequisition.ContractRequisitionBudgetDists[i].Year) {
                    //UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DUPLICATE_YEAR'));
                    vm.contractRequisition.ContractRequisitionBudgetDists[i].Value = vm.budgetDistValue;
                    newYear = false;
                    break;
                }
            }
            if (newYear) {
                vm.contractRequisition.ContractRequisitionBudgetDists.push({
                    ContractRequisitionId: contractRequisitionId,
                    Value: vm.budgetDistValue,
                    Year: vm.budgetDistYear
                });
            }
            vm.budgetDistValue = null;
            vm.budgetDistYear = null;
            vm.contractRequisition.ContractRequisitionBudgetDists.sort(function (a, b) {
                return a.Year - b.Year;
            });
        };

        vm.removeBudgetDist = removeBudgetDist;
        function removeBudgetDist(index) {
            vm.contractRequisition.ContractRequisitionBudgetDists.splice(index, 1);
        };

        vm.save = save;
        function save() {

            angular.forEach(vm.crform, function (value, key) {
                if (typeof value === 'object' && value.hasOwnProperty('$modelValue'))
                    value.$setTouched();
            });

            var checkField = checkRequiredField();
            if (!checkField) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INCOMPLETE_FIELD'));
                return;
            }
            if (vm.contractRequisition.RequiredDate < vm.contractRequisition.RequestedDate) {
                vm.crform.requiredDate.$setValidity("required", false);
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REQUIRED_DATE'));
                return;
            }
            if (vm.contractRequisition.StartDate < vm.contractRequisition.RequiredDate) {
                vm.crform.startDate.$setValidity("required", false);
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_START_DATE'));
                return;
            }
            if (Number(vm.contractRequisition.OutstandingBudget) > Number(vm.contractRequisition.ApprovedBudget)) {
                vm.crform.outstandingBudget.$setValidity("required", false);
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_OUTSTANDING'));
                return;
            }
            if (vm.contractRequisition.CommodityID == null || vm.contractRequisition.CommodityID == "") {
                vm.crform.commodityType.$setValidity("required", false);
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_COMMODITY'));
                return;
            }
            if (Number(vm.contractRequisition.TotalValue) > Number(vm.contractRequisition.OutstandingBudget)) {
                vm.crform.totalValue.$setValidity("required", false);
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_TOTAL_VALUE'));
                return;
            }

            var totalBudgetDistribution = 0;
            vm.contractRequisition.ContractRequisitionBudgetDists.forEach(function (budget) {
                totalBudgetDistribution += Number(budget.Value);
            });
            if (Number(totalBudgetDistribution.toFixed(2)) !== Number(vm.contractRequisition.TotalValue)) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_BUDGET_DIST'));
                return;
            }

            UIControlService.loadLoading(loadmsg);
            //vm.contractRequisition.DirectAward = vm.contractRequisition.DirectAward == "1";
            vm.contractRequisition.BudgetStatus = vm.contractRequisition.BudgetStatus == "1";
            //vm.contractRequisition.OperatingOrCapital = vm.contractRequisition.OperatingOrCapital == "1";
            convertAllDateToString();
            DataContractRequisitionService.Update2(vm.contractRequisition, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                    vm.back();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
            });
        };

        function checkRequiredField() {
            return vm.contractRequisition.ContractDetailsInfo && vm.contractRequisition.RequestedDate && vm.contractRequisition.RequiredDate
                && vm.contractRequisition.StartDate && vm.contractRequisition.FinishDate && vm.contractRequisition.ProjectOwner
                /*&& vm.contractRequisition.OutsourcingRequest && vm.contractRequisition.Project */
                && vm.contractRequisition.WBS && vm.contractRequisition.TotalValue && vm.contractRequisition.RequestType
                && vm.contractRequisition.JustForRequest && vm.contractRequisition.CostAnalysis;
        }

        function convertAllDateToString() {
            if (vm.contractRequisition.RequestedDate) {
                vm.contractRequisition.RequestedDate = UIControlService.getStrDate(vm.contractRequisition.RequestedDate);
            }
            if (vm.contractRequisition.RequiredDate) {
                vm.contractRequisition.RequiredDate = UIControlService.getStrDate(vm.contractRequisition.RequiredDate);
            }
            if (vm.contractRequisition.StartDate) {
                vm.contractRequisition.StartDate = UIControlService.getStrDate(vm.contractRequisition.StartDate);
            }
            if (vm.contractRequisition.FinishDate) {
                vm.contractRequisition.FinishDate = UIControlService.getStrDate(vm.contractRequisition.FinishDate);
            }
        };

        //Supaya muncul di date picker saat awal load
        function convertToDate(){
            if (vm.contractRequisition.RequestedDate) {
                vm.contractRequisition.RequestedDate = new Date(Date.parse(vm.contractRequisition.RequestedDate));
            }
            if (vm.contractRequisition.RequiredDate) {
                vm.contractRequisition.RequiredDate = new Date(Date.parse(vm.contractRequisition.RequiredDate));
            }
            if (vm.contractRequisition.StartDate) {
                vm.contractRequisition.StartDate = new Date(Date.parse(vm.contractRequisition.StartDate));
            }
            if (vm.contractRequisition.FinishDate) {
                vm.contractRequisition.FinishDate = new Date(Date.parse(vm.contractRequisition.FinishDate));
            }
        }

        vm.print = print;
        function print(toPrint) {
            var innerContents = document.getElementById(toPrint).innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Contract Requisition Form</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();
        }

        vm.back = back;
        function back() {
            $state.transitionTo('detail-contract-requisition', { contractRequisitionId: contractRequisitionId });
        };
    }
})();