﻿(function () {
    'use strict';

    angular.module("app").controller("FormCELineModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'DataContractRequisitionService', 'UIControlService', 'item', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
		DataContractRequisitionService, UIControlService, item, $uibModalInstance) {
        //console.info("masuk modal");
        var vm = this;

        vm.commodities = [];
        vm.ceLine = item;

        vm.init = init;
        function init() {

            if (item.ContractRequisitionCELineID) {
                vm.isEdit = true;
                vm.action = "UBAH";
		    } else {
                vm.action = "TAMBAH";
            }
        }

        vm.onUnitCostChange = onUnitCostChange;
        function onUnitCostChange() {
            vm.ceLine.LineCost = (vm.ceLine.Quantity * vm.ceLine.UnitCost).toFixed(2);
        }

        vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
        };

        vm.simpan = simpan;
        function simpan() {

            if (!vm.ceLine.Name || !vm.ceLine.Quantity || !vm.ceLine.OrderUnit || !vm.ceLine.UnitCost) {
                UIControlService.msg_growl("error", "MESSAGE.ERR_INCOMPLETE_FIELD");
                return;
            }

            var saveCELine = vm.isEdit ? DataContractRequisitionService.EditCELine : DataContractRequisitionService.InsertCELine;
            UIControlService.loadLoadingModal("");
            saveCELine(vm.ceLine, function (reply) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE_CE_LINE");
		        $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_CE_LINE");
		    });
        }
    }
})();