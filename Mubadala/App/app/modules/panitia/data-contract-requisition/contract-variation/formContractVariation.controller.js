(function () {
    'use strict';

    angular.module("app")
    .controller("formContractVarCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'DataContractRequisitionService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, DataContractRequisitionService, UIControlService) {

        var vm = this;
        var contractRequisitionId = Number($stateParams.contractRequisitionId);
        var contractRequisitionVariationId = Number($stateParams.contractRequisitionVariationId);
        vm.contractRequisitionVariationId = contractRequisitionVariationId;
        var action = $stateParams.action;

        var loadmsg = "";

        vm.contractRecVar = {};
        vm.isCalendarOpened = [false, false, false, false];
        vm.isTenderVerification = false;
        vm.contractReqVar = {};
        vm.reasonRefs = [];
        vm.contractValueVariationPercent = 0;
        vm.contractDateVariationPercent = 0;
        vm.contracts = [];
        vm.selectedContract = {};

        vm.crvform;

        vm.breadcrumbs = []
        vm.breadcrumbs.push({ title: "BREADCRUMB.MASTER_REQUISITION", href: "" });
        vm.breadcrumbs.push({ title: "BREADCRUMB.DATA_CONTRACT_REQUISITION", href: "#/data-contract-requisition" });
        if (contractRequisitionVariationId > 0) {
            vm.breadcrumbs.push({ title: "BREADCRUMB.DETAIL_CONTRACT_REQUISITION", href: "#/detail-contract-requisition/" + contractRequisitionId });
        }
        vm.breadcrumbs.push({ title: "BREADCRUMB.DATA_CONTRACT_VARIATION", href: "" });

        vm.dateRequestedOptions = {
            minDate: new Date()
        };

        vm.dateRequiredOptions = {
            minDate: new Date()
        };

        vm.planEndDateOptions = {
            minDate: new Date()
        };

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('contract-variation');
            $translate.refresh().then(function () {
                loadmsg = $filter('translate')('MESSAGE.LOADING');
            });

            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.IsRequestor({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.isRequestor = reply.data;
                DataContractRequisitionService.isCompliance(function (reply) {
                    vm.isCompliance = reply.data;
                    if (vm.isCompliance || vm.isRequestor) {
                        vm.loadData();
                    } else {
                        UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.ERR_NOT_REQUESTOR'));
                        $state.transitionTo('data-contract-requisition');
                    }
                }, function () {
                    UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REQUESTOR'));
                $state.transitionTo('data-contract-requisition');
            });
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.GetCRVariationReasons(function (reply) {
                UIControlService.unloadLoading();
                vm.reasonRefs = reply.data;
                if (contractRequisitionVariationId > 0) {
                    loadDataForEdit();
                } else {
                    loadDataForCreate();
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_REASONS'));
            });
        };

        function loadDataForEdit() {
            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.GetVariationByID({
                ID: contractRequisitionVariationId
            }, function (reply) {
                vm.contractRecVar = reply.data;
                vm.contractRecVar.IsAnnualPlan = vm.contractRecVar.IsAnnualPlan ? "1" : "0";
                vm.contractRecVar.OriginalStartDate = new Date(Date.parse(vm.contractRecVar.OriginalStartDate));
                vm.contractRecVar.OriginalEndDate = new Date(Date.parse(vm.contractRecVar.OriginalEndDate));
                convertToDate();
                if (vm.contractRecVar.OriginalStartDate > new Date()) {
                    vm.planEndDateOptions.minDate = vm.contractRecVar.OriginalStartDate;
                }
                vm.isTenderVerification = vm.contractRecVar.StatusName !== 'CR_DRAFT' && vm.contractRecVar.StatusName.lastIndexOf('CR_REJECT_', 0) !== 0 && !vm.contractRecVar.IsOERevision;
                vm.isTenderVerification = vm.isTenderVerification || !vm.isRequestor;
                if (vm.contractRecVar.ReasonRefs && vm.contractRecVar.ReasonRefs.length > 0) {
                    for (var i = 0; i < vm.reasonRefs.length; i++) {
                        vm.reasonRefs[i].isChecked = false;
                        for (var j = 0; j < vm.contractRecVar.ReasonRefs.length; j++) {
                            if (vm.reasonRefs[i].RefID === vm.contractRecVar.ReasonRefs[j].RefID) {
                                vm.reasonRefs[i].isChecked = true;
                                if (vm.reasonRefs[i].Name == 'CV_REASON_EXTENSION') {
                                    vm.enableDate = true;
                                } else if (vm.reasonRefs[i].Name == 'CV_REASON_RATE') {
                                    vm.enableValue = true;
                                }
                                break;
                            }
                        }
                    }
                }
                if (vm.contractRecVar.CurrencySymbol === 'IDR') {
                    vm.contractRecVar.OriginalValueInUSD = vm.contractRecVar.OriginalValue * vm.contractRecVar.RateIDRToUSD;
                    vm.contractRecVar.ContractCommitmentTotalValueInUSD = vm.contractRecVar.ContractCommitmentTotalValue * vm.contractRecVar.RateIDRToUSD;
                }

                vm.contracts.forEach(function (con) {
                    if (con.ID === vm.contractRecVar.ContractSignOffId) {
                        vm.selectedContract = con;
                    }
                });

                calculateVariationPercent();
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        }

        function loadDataForCreate() {
            vm.DateRequested = new Date();
            //vm.DateRequired = new Date();
            vm.setRequiredDate();
            vm.contractRecVar.ContractRequisitionOriginId = contractRequisitionId;
            vm.contractRecVar.IsAnnualPlan = "0";
            vm.contractRecVar.ContractCommitmentTotalValue = 0;
            vm.contractRecVar.FurtherCommitmentTotalValue = 0;
            vm.contractRecVar.ReasonRefs = [];
            DataContractRequisitionService.GetContractByCRID({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                vm.contracts = reply.data;
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CONTRACT'));
            });
        };

        vm.setRequiredDate = setRequiredDate;
        function setRequiredDate() {
            
            var minDateRequired = addDays(vm.DateRequested, 2 * 7);
            vm.dateRequiredOptions = {
                minDate: minDateRequired
            };

            if (vm.DateRequired < minDateRequired) {
                vm.DateRequired = minDateRequired;
            }
        }

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }

        vm.onSelectContract = onSelectContract;
        function onSelectContract() {
            DataContractRequisitionService.IsContractSuspended({
                ContractSignOffId: vm.selectedContract.ID
            }, function (reply) {
                if (reply.data === false) {
                    DataContractRequisitionService.CanCreateVariant({
                        ContractRequisitionOriginId: contractRequisitionId,
                        ContractSignOffId: vm.selectedContract.ID
                    }, function (reply) {
                        if (reply.data === true) {
                            processSelectContract();
                        }
                        else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.CANNOT_CREATE_VARIATION'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_VARIATION'));
                    });
                }
                else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.CONTRACT_IS_SUSPENDED'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHECK_VARIATION'));
            });
        }

        function processSelectContract() {
            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.GetCurrentVariantNumber({
                ContractRequisitionOriginId: contractRequisitionId,
                ContractSignOffId: vm.selectedContract.ID
            }, function (reply) {
                vm.contractRecVar.VariationNo = reply.data;
                DataContractRequisitionService.SelectById2({
                    ContractRequisitionId: contractRequisitionId
                }, function (reply) {
                    var contractRec = reply.data;
                    vm.contractRecVar.TenderCode = contractRec.TenderCode;
                    vm.contractRecVar.ProjectTitle = contractRec.ProjectTitle;
                    vm.contractRecVar.CurrencySymbol = contractRec.CurrencySymbol;
                    vm.contractRecVar.RateIDRToUSD = contractRec.RateIDRToUSD;

                    var constractSO = vm.selectedContract;
                    vm.contractRecVar.ContractSignOffId = constractSO.ID;
                    vm.contractRecVar.OriginalStartDate = new Date(Date.parse(constractSO.ContractStartDate));
                    vm.contractRecVar.OriginalEndDate = new Date(Date.parse(constractSO.ContractEndDate));
                    vm.contractRecVar.OriginalValue = constractSO.NegotiationPrice;
                    if (vm.contractRecVar.OriginalStartDate > new Date()) {
                        vm.planEndDateOptions.minDate = vm.contractRecVar.OriginalStartDate;
                    }
                    vm.contractRecVar.ContractNo = constractSO.ContractNo;
                    if (vm.contractRecVar.CurrencySymbol === 'IDR') {
                        vm.contractRecVar.OriginalValueInUSD = vm.contractRecVar.OriginalValue * vm.contractRecVar.RateIDRToUSD;
                    }

                    DataContractRequisitionService.GetCurrentEndateAndTotalValue({
                        ContractRequisitionOriginId: contractRequisitionId,
                        ContractSignOffId: vm.selectedContract.ID
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        var currentData = reply.data;
                        vm.CurrentEndDate = new Date(Date.parse(currentData.CurrentEndDate));
                        vm.NewPlannedEndDate = vm.CurrentEndDate;
                        vm.contractRecVar.ContractCommitmentTotalValue = currentData.ContractCommitmentTotalValue;
                        if (vm.contractRecVar.CurrencySymbol === 'IDR') {
                            vm.contractRecVar.ContractCommitmentTotalValueInUSD = vm.contractRecVar.ContractCommitmentTotalValue * vm.contractRecVar.RateIDRToUSD;
                        }
                        calculateVariationPercent();
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CR'));
                    });
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CR'));
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_CR'));
            });
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            if (!vm.isTenderVerification) {
                if (index === 0 || index === 1 || vm.enableDate) {
                    vm.isCalendarOpened[index] = true;
                }
            }
        };

        vm.calculateVariationPercent = calculateVariationPercent;
        function calculateVariationPercent() {

            //Variasi nilai
            vm.contractRecVar.NewTotalValue =
                Number(vm.contractRecVar.ContractCommitmentTotalValue) + Number(vm.contractRecVar.FurtherCommitmentTotalValue);
            var originalValue = vm.contractRecVar.OriginalValue;
            var newValue = vm.contractRecVar.NewTotalValue;
            var percentageValue = originalValue > 0 ? (newValue * 100 / originalValue) - 100 : 0;
            vm.contractValueVariationPercent = Number(percentageValue.toFixed(2));
            if (vm.contractValueVariationPercent < 0) {
                vm.contractValueVariationPercent = 0;
            }

            if (vm.contractRecVar.CurrencySymbol === 'IDR') {
                vm.contractRecVar.FurtherCommitmentTotalValueInUSD = Number(vm.contractRecVar.FurtherCommitmentTotalValue) * vm.contractRecVar.RateIDRToUSD;
                vm.contractRecVar.NewTotalValueInUSD = vm.contractRecVar.NewTotalValue * vm.contractRecVar.RateIDRToUSD;
            }

            //variasi tanggal
            vm.contractDateVariationPercent = 0;
            if (vm.NewPlannedEndDate) {
                var originalDuration = (vm.contractRecVar.OriginalEndDate - vm.contractRecVar.OriginalStartDate);
                var newDuration = (vm.NewPlannedEndDate - vm.contractRecVar.OriginalStartDate);
                var percentageDuration = originalDuration > 0 ? (newDuration * 100 / originalDuration) - 100 : 0;
                vm.contractDateVariationPercent = Number(percentageDuration.toFixed(2));
            }
            if (vm.contractDateVariationPercent < 0) {
                vm.contractDateVariationPercent = 0;
            }

            //Ambil nilai terbesar
            vm.contractRecVar.VariationPercent = vm.contractValueVariationPercent > vm.contractDateVariationPercent ?
                vm.contractValueVariationPercent : vm.contractDateVariationPercent;
        };

        vm.onReasonCheck = onReasonCheck;
        function onReasonCheck(reasonRef) {
            switch (reasonRef.Name) {
                case 'CV_REASON_EXTENSION':
                    if (reasonRef.isChecked === true) {
                        vm.enableDate = true;
                    } else {
                        vm.NewPlannedEndDate = vm.CurrentEndDate;
                        vm.enableDate = false;
                    }
                    break;
                case 'CV_REASON_RATE':
                    if (reasonRef.isChecked === true) {
                        vm.enableValue = true;
                    } else {
                        vm.contractRecVar.FurtherCommitmentTotalValue = 0;
                        vm.contractRecVar.NewTotalValue = Number(vm.contractRecVar.ContractCommitmentTotalValue);
                        vm.enableValue = false;
                    }
                    break;
                default:
                    break;
            };
            vm.calculateVariationPercent();
        };

        vm.save = save;
        function save() {

            angular.forEach(vm.crvform, function (value, key) {
                if (typeof value === 'object' && value.hasOwnProperty('$modelValue'))
                    value.$setTouched();
            });

            if (!vm.contractRecVar.ContractSignOffId) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_SELECTED_CONTRACT'));
            }

            var checkField = checkRequiredField();
            if (!checkField) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_INCOMPLETE_FIELD'));
                return;
            }
            if (vm.DateRequired < vm.DateRequested) {
                vm.crvform.dateRequired.$setValidity("required", false);
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DATE_REQUIRED'));
                return;
            }

            vm.contractRecVar.IsAnnualPlan = vm.contractRecVar.IsAnnualPlan === "1";
            vm.contractRecVar.ReasonRefs = [];
            vm.reasonRefs.forEach(function (ref) {
                if (ref.isChecked === true) {
                    vm.contractRecVar.ReasonRefs.push({
                        RefID: ref.RefID
                    });
                }
            });
            convertAllDateToString();
            if (contractRequisitionVariationId > 0) {
                UIControlService.loadLoading(loadmsg);
                DataContractRequisitionService.EditVariant(vm.contractRecVar, function (reply) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                    vm.back();
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                });
            } else {
                DataContractRequisitionService.CreateVariant(vm.contractRecVar, function (reply) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE'));
                    vm.back();
                }, function (error) {
                    UIControlService.unloadLoading();
                    if (error.data === 'ERR_DUPLICATE_VARIATION_NO') {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DUPLICATE_VARIATION_NO'));
                        vm.contractRecVar.VariationNo += 1;
                    }
                    else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE'));
                    }
                });
            }
        };

        function checkRequiredField() {
            return vm.DateRequested && vm.DateRequired && vm.CurrentEndDate && vm.NewPlannedEndDate;
        }

        function convertAllDateToString() {
            if (vm.DateRequested) {
                vm.contractRecVar.DateRequested = UIControlService.getStrDate(vm.DateRequested);
            }
            if (vm.DateRequired) {
                vm.contractRecVar.DateRequired = UIControlService.getStrDate(vm.DateRequired);
            }
            if (vm.CurrentEndDate) {
                vm.contractRecVar.CurrentEndDate = UIControlService.getStrDate(vm.CurrentEndDate);
            }
            if (vm.NewPlannedEndDate) {
                vm.contractRecVar.NewPlannedEndDate = UIControlService.getStrDate(vm.NewPlannedEndDate);
            }
        };

        //Supaya muncul di date picker saat awal load
        function convertToDate() {
            if (vm.contractRecVar.DateRequested) {
                vm.DateRequested = new Date(Date.parse(vm.contractRecVar.DateRequested));
            }
            if (vm.contractRecVar.DateRequired) {
                vm.DateRequired = new Date(Date.parse(vm.contractRecVar.DateRequired));
            }
            if (vm.contractRecVar.CurrentEndDate) {
                vm.CurrentEndDate = new Date(Date.parse(vm.contractRecVar.CurrentEndDate));
            }
            if (vm.contractRecVar.NewPlannedEndDate) {
                vm.NewPlannedEndDate = new Date(Date.parse(vm.contractRecVar.NewPlannedEndDate));
            }
        }

        vm.print = print;
        function print(toPrint) {
            var innerContents = document.getElementById(toPrint).innerHTML;
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Contract Variation Request Form</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();
        }

        vm.back = back;
        function back() {
            if (contractRequisitionVariationId > 0) {
                $state.transitionTo('detail-contract-requisition', { contractRequisitionId: contractRequisitionId });
            } else {
                $state.transitionTo('data-contract-requisition');
            }
        };
    }
})();