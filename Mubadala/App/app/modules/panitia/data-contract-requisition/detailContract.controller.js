(function () {
    'use strict';

    angular.module("app")
    .controller("detailContractReqCtrl", ctrl);
    
    ctrl.$inject = ['$state', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataContractRequisitionService', 'UIControlService','$q'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DataContractRequisitionService, UIControlService,$q) {

        var vm = this;
        var contractRequisitionId = Number($stateParams.contractRequisitionId);
        var loadmsg = "MESSAGE.LOADING";

        vm.breadcrumbs = [
            { title: "BREADCRUMB.MASTER_REQUISITION", href: "" },
            { title: "BREADCRUMB.DATA_CONTRACT_REQUISITION", href: "#/data-contract-requisition" },
            { title: "BREADCRUMB.DETAIL_CONTRACT_REQUISITION", href: "" }
        ];

        vm.tenderCode = "";
        vm.projectTitle = "";
        vm.contractSponsor = "";
        vm.projectManager = "";
        vm.isSubmitted = false;
        vm.isPreparedPR = false;

        vm.formList = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-contract-requisition');
            getIsPreparePR().then(function (reply) {
                getRequestorEtc();
            })
           
        };

        function getRequestorEtc() {

            DataContractRequisitionService.IsRequestor({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                //UIControlService.unloadLoading();
                vm.isRequestor = reply.data;
                DataContractRequisitionService.isCompliance(function (reply) {
                    vm.isCompliance = reply.data;
                    if (vm.isCompliance || vm.isRequestor || vm.isPreparedPR) {
                        loadDetails();
                    } else {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("warning", $filter('translate')('MESSAGE.ERR_NOT_REQUESTOR'));
                        $state.transitionTo('data-contract-requisition');
                    }
                }, function () {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "Gagal Mendapatkan Data login");
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REQUESTOR'));
                $state.transitionTo('data-contract-requisition');
            });
        }

        function getIsPreparePR() {
            UIControlService.loadLoading(loadmsg);

            var defer = $q.defer();
            DataContractRequisitionService.isPreparePR({
                ContractRequisitionId: contractRequisitionId
            }, function (reply) {
                if (reply.status == 200) {
                    vm.isPreparedPR = reply.data;
                    defer.resolve(true)
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REQUESTOR'));
                    defer.reject()
                }
            }, function (error) {
                UIControlService.unloadLoading();
                defer.reject()
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_CHK_REQUESTOR'));
                //$state.transitionTo('data-contract-requisition');
            });
            return defer.promise;
        }

        function loadDetails() {
            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.GetRefCRDet(
            {
                ContractRequisitionId: contractRequisitionId
            },
            function (reply) {
                if (reply.status === 200) {
                    vm.formList = reply.data;
                    for (var i = 0; i < vm.formList.length; i++) {
                        vm.formList[i].ContractRequisitionId = contractRequisitionId;
                        vm.formList[i].Information = "";
                        vm.formList[i].notNeeded = vm.formList[i].LinkState !== 'form-contract-requisition';
                        /*
                        vm.formList[i].mandatory = (
                            vm.formList[i].LinkState === 'form-contract-requisition' ||
                            vm.formList[i].LinkState === 'detail-cost-estimate' || 
                            vm.formList[i].LinkState === 'csms-decision')
                        vm.formList[i].Status = vm.formList[i].mandatory;
                        */
                    }
                    DataContractRequisitionService.SelectDetail({
                        ContractRequisitionId: contractRequisitionId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            var contractRequisition = reply.data;
                            vm.tenderCode = contractRequisition.TenderCode;
                            vm.projectTitle = contractRequisition.ProjectTitle;
                            vm.contractSponsor = contractRequisition.ContractSponsor;
                            vm.projectManager = contractRequisition.ProjectManager;
                            vm.projectManagerName = contractRequisition.ProjectManagerName;
                            vm.contractRequisitionVariationId = contractRequisition.ContractRequisitionVariationId;
                            vm.isSubmitted = contractRequisition.StatusName !== 'CR_DRAFT' && contractRequisition.StatusName.lastIndexOf('CR_REJECT_', 0) !== 0;
                            vm.isRejectVerification = contractRequisition.StatusName === 'CR_REJECT_2';
                           
                            for (var i = 0; i < vm.formList.length; i++) {
                                vm.formList[i].notNeeded = false;
                            }
                            
                            var details = contractRequisition.ContractRequisitionDetails;
                            for (var i = 0; i < details.length; i++) {
                                vm.formList[i].ContractRequisitionDetailId = details[i].contractRequisitionId;
                                vm.formList[i].Status = details[i].Status;
                                vm.formList[i].Information = details[i].Information;
                                vm.formList[i].VerificationStatus = details[i].VerificationStatus;
                            }
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DET'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DET'));
                    });
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DET'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_DET'));
            });
        };

        vm.gotoDetail = gotoDetail;
        function gotoDetail(dt) {
            if (dt.LinkState === 'direct-award-form') {
                var item = {
                    contractRequisitionId: contractRequisitionId,
                    ProjectManager: vm.projectManager,
                    ProjectManagerFullName: vm.projectManagerName,
                    IsTenderVerification: vm.isSubmitted || !vm.isRequestor
                };
                var modalInstance = $uibModal.open({
                    templateUrl: "app/modules/panitia/data-contract-requisition/directAwardForm.html?v=1.000003",
                    controller: "directAwardFormCtrl",
                    controllerAs: "daFormCtrl",
                    resolve: { item: function () { return item; } }
                });
                modalInstance.result.then(function (ret) {
                    vm.projectManager = ret.ProjectManager;
                    vm.projectManagerName = ret.ProjectManagerFullName;
                });
            } else {
                if (vm.contractRequisitionVariationId > 0 && dt.LinkState === 'form-contract-requisition') {
                    $state.transitionTo('contract-variation', { contractRequisitionId: contractRequisitionId, contractRequisitionVariationId: vm.contractRequisitionVariationId });
                }
                else {
                    $state.transitionTo(dt.LinkState, { contractRequisitionId: contractRequisitionId });
                }
            }
        };

        vm.save = save;
        function save() {

            for (var i = 0; i < vm.formList.length; i++) {
                if (vm.formList[i].notNeeded === false && vm.formList[i].Status !== true) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NOT_ALL_ITEM_CHECKED'));
                    return;
                }
            }

            UIControlService.loadLoading(loadmsg);
            DataContractRequisitionService.SaveDetail({
                ContractRequisitionId: contractRequisitionId,
                ProjectTitle: vm.projectTitle,
                ContractSponsor: vm.contractSponsor,
                ProjectManager: vm.projectManager,
                ContractRequisitionDetails: vm.formList
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SAVE_DET'));
                    loadDetails();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DET'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SAVE_DET'));
            });
        };

        vm.back = back;
        function back(dt) {
            $state.transitionTo('data-contract-requisition');
        };
    };
})();