﻿(function () {
    'use strict';

    angular.module("app").controller("PrintPDFRankingRiskCtrl", ctrl);

    ctrl.$inject = ['GlobalConstantService', '$state', '$stateParams', '$http', '$filter', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'RankingRiskService', 'UIControlService'];
    function ctrl(GlobalConstantService, $state, $stateParams, $http, $filter, $uibModal, $translate, $translatePartialLoader, $location, SocketService, RankingRiskService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.pageSize = 10;
        vm.init = init;
        vm.totalItems = String($stateParams.totalItems);
        vm.tahun = String($stateParams.tahun);
        vm.batasAtas = String($stateParams.batasAtas);
        vm.batasBawah = String($stateParams.batasBawah);
        vm.durasi = String($stateParams.durasi);
        vm.maxSize = 35;

        vm.hasilParsing = Math.ceil(vm.totalItems / 5);
        vm.alldata_ = [];

        function init() {
            //console.info("vm.coba" + JSON.stringify(vm.coba));
            //filter();
            $translatePartialLoader.addPart('ranking-risk');
            loadData();
        }

        vm.loadData = loadData;
        vm.generate = [];
        function loadData() {
            RankingRiskService.datavendor({
                column: vm.tahun,
                IntParam1: vm.batasAtas,
                IntParam2: vm.batasBawah,
                Status: vm.durasi,
                Offset: 0,
                Limit: vm.totalItems
            }, function (reply) {
                UIControlService.loadLoading("MESSAGE.LOADING");
                if (reply.status === 200) {
                    vm.dataAllVendor = reply.data;
                    vm.alldata = reply.data.List;
                  
                    vm.totalItems = Number(reply.data.Count);
                    console.info("alldata:" + JSON.stringify(vm.alldata));
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
                UIControlService.unloadLoading();
            });

        }



        //print
        vm.exportpdf = exportpdf;
        var pdf, page_section, HTML_Width, HTML_Height, top_left_margin, PDF_Width, PDF_Height, canvas_image_width, canvas_image_height;

        function exportpdf() {
            
            pdf = "";
            $("#downloadbtn").hide();
            $("#genmsg").show();

            for (var i = 1; i <= vm.jumlahlembar; i++) {
                if (i < vm.jumlahlembar) {
                    html2canvas($(".print-wrap:eq("+(i-1)+")")[i-1], { allowTaint: true }).then(function (canvas) {

                        calculatePDF_height_width(".print-wrap", i-1);
                        var imgData = canvas.toDataURL("image/png", 1.0);
                        pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
                        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);

                    });
                }
                else if (i == vm.jumlahlembar) {
                    html2canvas($(".print-wrap:eq(" + (i - 1) + ")")[i - 1], { allowTaint: true }).then(function (canvas) {

                        calculatePDF_height_width(".print-wrap", i-1);

                        var imgData = canvas.toDataURL("image/png", 1.0);
                        pdf.addPage(PDF_Width, PDF_Height);
                        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);



                        //console.log((page_section.length-1)+"==="+index);
                        setTimeout(function () {

                            //Save PDF Doc  
                            pdf.save("HTML-Document.pdf");

                            //Generate BLOB object
                            var blob = pdf.output("blob");

                            //Getting URL of blob object
                            var blobURL = URL.createObjectURL(blob);

                            //Showing PDF generated in iFrame element
                            var iframe = document.getElementById('sample-pdf');
                            //iframe.src = blobURL;

                            //Setting download link
                            var downloadLink = document.getElementById('pdf-download-link');
                            //downloadLink.href = blobURL;

                            $("#sample-pdf").slideDown();


                            $("#downloadbtn").show();
                            $("#genmsg").hide();
                        }, 0);
                    });
                }
            }
       
        }


        vm.makePDF = makePDF;
        var indeks = "";
        vm.generate2 = [];
        function makePDF() {


            for (var i = 0; i <= vm.alldata.length - 1; i++) {
                if (i == 0) {
                    var newmap1 = 'No';
                    var newmap2 = $filter('translate')("TABLE.NAMA");
                    var newmap3 = $filter('translate')("TABLE.RANKING_RISK");
                    var newmap4 = $filter('translate')("TABLE.ACTIVED_DATE");
                    var newmap5 = $filter('translate')("TABLE.APPROVALCOMPLIANCE_DATE");
                    var newmap6 = $filter('translate')("TABLE.AREA_TITLE");
                    
                }
                else {
                    var approvalCompliance = '';
                    if (vm.alldata[i].CategoryVendorData.Name == 'HIGH_RISK' || vm.alldata[i].CategoryVendorData.Name == 'GOVERNMENT_ENTITY') {
                        if (vm.alldata[i].ApprovalComplianceDate != null) {
                            approvalCompliance = UIControlService.convertDate(UIControlService.getStrDate(vm.alldata[i].ApprovalComplianceDate));
                        }
                    }
                    else if (vm.alldata[i].CategoryVendorData.Name == 'LOW_RISK' || vm.alldata[i].CategoryVendorData.Name == 'MEDIUM_RISK') {
                        approvalCompliance = 'N/A';
                    }

                    var newmap1 = i.toString();
                    var newmap2 = vm.alldata[i].VendorName;
                    var newmap3 = $filter('translate')(vm.alldata[i].CategoryVendorData.Name);
                    var newmap4 = UIControlService.convertDate(UIControlService.getStrDate(vm.alldata[i].ActivedDate));
                    var newmap5 = approvalCompliance;
                    var newmap6 = $filter('translate')(vm.alldata[i].Area);
                }
                vm.generate.push(newmap1,newmap2,newmap3,newmap4,newmap5,newmap6);
                vm.generate2[i] = vm.generate;
                vm.generate = [];
            }
            console.info("gen" + JSON.stringify(vm.generate2));
            
            var documentDefinition = {
                content:
                [
                    {
                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAyCAYAAACQyQOIAAANoklEQVR4nO1de5AcRRn/dmfvwsMcyWWXA8UUAgEVLVFBSIDyeJSISKkl6xPKKNYKyuZub3u2HzO70xcKpaBACUgBicgjDzhCILvJlbzEKrUUTSWlsXgEBBF5JbcbAwnJZe+u/WO6Z3pf98ru3uHdV9WV25nunn785vf9uuebCYBu0agB0ajh/U4kDoc0/XyI29xw7M0hbu8wHHuP4djDIW4Lw7GLBrd2hhzrTyFu3xzi9oUQj8+RpQPAO0PAIQhTZJxD8GkOoac5hAAgoJ0K1Dg+w41DsAQAafJpg7MVBrdeDnFbTDDtCGaYAzz1wap1N8H6+sDo64OKa265E1q23Akt5cdnAQHgsoC6a7m1xODWk/rEGtwaMbg1JNOIwa2RivOOPVx+3uDW3hC3bwBC5ldcp0HGOQR1ABRy8xcWcuGr8rnwukI2vLWQjbxSyIX/VchFthVy4dUD2QVXvLrmqPmqLMx4MPBk2ODWam9yHXvYcOyicgETSeVlDW69ZWSs77rXaRg7BCQAAgAA+Vz74nw28nA+F9lfyEVE9RQeKeQiopCNDOSz4d5dG8NzVV0NaN/0NyNjXWo49qty0oYMbg1Nwh1UAsJliqKmJx4BSiMAAMA7Q/Vqf18fGPJuhoENCz5WyIazpZMdLhZy4WIhGx4uSe6xIQWIfC78n0Ju/kIAjx1mlkmhJwzHHqwHAGoxhATHG6EMOw8AFBgmfffpbuCt++DIgWzk+kI2POje5XKi1V0/agoflEB4QnMrM5AVbPtEg1tvqLu4EWDQVhgjBrdGghmWBIDJ6oaALu4Kj0a+VMhGXpYAGHLv8rEmX6ZseFi6h1fe2vCBo1X99Rvc95kZGevryjU0CghV2OFujxXGpxsCT3MIKdrOP9x+XD4X6StzAeNggBK3MVLIhod2PRLuBHDdTONGebqb9Nchx/q5unMbCgaXFQ7K378DShfo7ahontwLAHmnvtYHhxeyC3A+F96jscDw+AHgAaFYyEVEPhtGAN4SckZbAAACEIu1hBzrj81ghhLAOdYLwOnJAODpBjX5umh7cQXMyW+M/EBzA8MTcgPVQJCLPAQw05lANzXgduoUw7F3N1ovlIPB4NYApOm5JW1RS8FH2z9cyC7AHgAknU/MDVTqgnwuvOO1vrZ2/VqzBuBRs5Gxvt0sVtCvYzj2oMHT3xICQv9cf+zpO7OReD4beSyfjbynTeIk3UCFLhjM59oXA8yyQXVTYODstmboBY0Zhlu5JSCdOeDccfHbQ/1tYncuInZlO3wqPyQAuCkvXcJAdkEcYFYXjGYuRcbjc0Lc/kuTmWGkxbGHIcPFlbdeNrJnU3tx36b5QzuzHZNzASUAiIh87ujink0LxK6NR68GmGWCsU356DQ+1eDWO83SCyq1cDYC9rViyY0/Es9u+IgY7p8rdmU7xECuY1Ig2JXtEIVsePjA5nniz32nPHv9nRceJXs6qwvGNF8vfK+ZrKDSYb1MQLpXzF9OxH33LhbF/jaxd3O72Jk9ZlyAyEsA7MweI97bPG/kYH+buGXVeQeO4YnPAQBEo7NsMH6TmzyGY93ZTL2g0pxeSwSdjID0chH9xeVi+8MnipH+uWLv5nYxkHMneVe2oySpY7tzEXFg8zwx1N8mtq0/qXjJzUsFsOt/AgDQWcfnHDPFXOrksSNC3N46FczQyi0xp9cSkF4ujuxlInH7V8XW9YvEe5vni+H+uaLY3yYG++eJwf55otjf5h3ble0Qj607TXx/RbTYyi0B7Lo1AADNjov4/zE1cGn8KRlf0FS9oLODwdMC7GtFK7fEhTddKfhdF4u1958pNq39jMiu+ay4/97F4mcrvyAuX/EdccrPlgnIOMOQXi5C3H72BByb1QWHbP6S8odTwQo+O7jaIcRtAeleAfa17r8Z7qb0cnlsuYB0ZqSVW6KVW4MtaXomAMyyQV3M1wu/ngq9UA6IOb2WOKyXiTm9lpATXnKsxbGKIW6LILeuAYC6xj/MdHMpNZWaG3Ks7RIME45eakbytq0de1YXNMTUgHLrdMOx90s30XS9MAYIXHA61nPAu+fJls/qgrqbpNhgxr56ql1EBQhU0KxjDwK3zgKACbNBNBo1Ojs7Q52dnaHoOMtOpMxk6geAgCrT2TkxF6dfb6w0kXpV7a5ekMGu0wUMqh3BjB0HgHroggCMzSbjyVMr70TKTktzG4/xUSHHeq6EkqcYBAa31gLApHUBShGKML3DTLGVyRS5qqS/lRYEAEim6BUI01UI07tQilyjnyvPaxKy1EyxlQjTVT0m+WaNvMoCAADxOI0kMV2BML0dYXpTPB5vG6Nd3jkTszjCdFUS0xVJTG9DhN6qkvqdxHQFIvTWUeoaxXy9cJYKep0qvaCB8PnJ6gJFjYiw603ChJtoobu7+9ga9QUAAJYuXXoYwuwlVSZJWEKvT897NSHzTUxf9+rH7PloNNo6SnuDAADLTHMRwnTIJEwgwt41TfOYcfQxCACAMH3KJEyo8lXSsNsW+vg4h6qKeXqBdU2Vi1DBsAa3DkKaLgaAybKBe8ea5okI070mYYMmYQKl6DKAiokF5d8Rxpf4A0rfisdluL42SV7eFL1S5t2PMD3gThC+VM9TrU2JBDnJz892d3WxjvJr1CqLCH1MAuGABMOwSvL3foTpmqvVS0iTNm9/wX5wKsDg6QJuLQOAQ9IF/uSy9dpd+9fR8pqErlZ5EaZ36ec0C8h6fy/zFmUSCLP1NcoA1IERTEyf9PtCe7swXmia5mkIsU8kMP7kNSn5WmIdtIp8HpFoNxzrxWbqBW2/YB24o3lI+wXld7k/+KRTP6/63NXFOkxCB9RAJ0w31K5sUoMAAD0YL/EAQ9i7inFMwvYlCDlBz1tetl5ASBISA6hkN5m3DqLVex5Bz631fmTDdIFjvQA+rR1qZwIA7kAhQrf7E0fvcbvp9tPTEz7VCxOzZ6oPjWSOFFupAWG5xg4CpQjV69WsvkBIkR8DAMRisSOi0WhrNBptjcViLVBbrE7ClF7gFmq0i6ijLqgwXzQS7FM+261RaBCqDLJaLVQTid3d3ccizPKKZbq6WIeJaUoD2natnD65zWKEOpuMbDK4taGRYPD3C1iXe926diwIANCF8UIT03cQpiMmYSKJ3WsplZ9M0o/79E4Hqk2OxhzdGqh+AwCQwPhUhOlBVb9pki/K+ivcSv00AnsGYXq3Sehak9DViND7TEI3Ioy/cqiDVm5SL6CjDcd6pRF6QdsveADckav7c4SqQpDQLQD65JK0JhLv1suVjAVAEGG2zadneoXKgAh9Qluqrq1SR92AUGP5WJRsZk16sGqa7EiI2+fXWys0SBdU6YLbh54Uu6CaaIzFYi2I0L/5IKkQk97fSYwv0tjg5VgsdgT4q4hvaIr+nS6MF8riQf3fOgFhxMT0vyamryNM30SYvmli+ppJ2KBiu/qbrxdYvVxEiS7g1hIAaMZTxQAidIs2kfcCACBCzh/v8hJh+qA22X9PEpZEKeKYmBKUYjeYhBW9uxVTBFDiw+urEeSEJxKJ9hjGR8Xj8bbu7u55cf9TRw0wXy9skhN5SMEs2n5Bt1t/YwWP5t+XaWp/z9KlSw9DhN5SPrhlAszbnDIJ26dYxdMDZTt73iRjtg18NgjA+1Ys6qZC4hk71uDWvw9FL2j7BQ8CQLPiCyoUvzvxlPlLS1pIJBIf0vMD+AOdxDSjfHENECiNMSJ3+0RPil3gdjFqQA0gqBWMlkdPnjYBqFg+XgUAEI/H50SjUUNP0AAX65vSCxl20WSBoJXZATzR1PcUy/cAEKZDiLB3te3ee/V8etsSicThJmY7tOVlOpkkZ/Sk2AWIkM6eFDuvJ8XOS2J8jonpP8rdz2hAmMgW89QzgjL/eYQzUb3g6QLHLkKang0ATY028oBg0rMVjWtJIELO1/PpfydT9DLNpbxxdRVxW2vPQrGMqqsECJjuRYgu7ibk+J4eevIy01y0zDQXJRLkpK5U6hTlNry2l2gEasfjNJJM0o/q5Xp66Mmau2mgKb3gWI9NRC9o+wUJt54piTt0FT6hf5AT8V4Nf65MPvVjmzQg/BKgkpLlrh4kCDkBYboXYXpQ35xSexaJBDnJJGxQ6oyDcv+iPO2Ty9z79LImpo/Ldh+Q+xYH9HKqPyahqxs/lN6n+1LHeZ/oGcNNaPsFfeD2bKriDvVnBfsUGySJ++mfaiIxmSRnaL5fICR3P6ts5Wqri4f11QVo/n6ZaS7yWKi21pAPseg6WW+rrHfUx9D+cfpAY4avsscyqol+eSwg+F9hs170vqYytZE87uNgjE81CX0AYbpVW/NXPm7G+FKE6YOI0HsQYT8dIyTNrduk5yJM15kpthIRek88lTpOZejqYh0y8OVuE7NfVU1usMu6ch2ACDFNQtciTFeNUm6NFoTTBPM/0XNdLb1Qogu4dQ4ATJco5PIndBMB5njyvq9D1iZjAQAIGI71VDW9oO0X9ADAdHsfIagttUYNE5tEgGpwlEDSkuDVMQJQS9zPeINXy8s13jy9QI43HPttxQJl+wUPyV5MByaYtYaZ/8r915Qm0D7X+9I00QWz1hTz9cKNig0Mbg15H9SaZYMZY66fjUYNg1u/lfsFjYgvmLVpb/5/DXC69z7CTPwY9jSx/wHaStM+GgZv+AAAAABJRU5ErkJggg==',
                        width: 70,
                        height:28
                    },
                    {
                        text: 'DATA RANKING RISK', style: 'header'
                    },
                    {
                        style: 'tableStyle',
                        table:
                        {
                            widths: ['auto', '*', '*', '*', '*', '*'],
                            body: vm.generate2
                        }
                    }
                ],


                styles:
                {
                    header: {
                        bold: true,
                        alignment: 'center',
                        margin:[0,0,0,5]
                    },
                    tableStyle: {
                        fontSize: 7,
                        margin: [0, 5, 0, 5]
                    }
                }
            };

            pdfMake.createPdf(documentDefinition).download('Data Ranking Risk ('+UIControlService.getStrDate(new Date())+').pdf');

        }





        //VendorBalance
        vm.batal = batal;
        function batal() {
            $uibModalInstance.close();
        }
    }
})();
//TODO