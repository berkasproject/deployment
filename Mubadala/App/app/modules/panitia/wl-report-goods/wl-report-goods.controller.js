(function () {
	'use strict';

	angular.module("app").controller("WLReportGoodsCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'WLReportService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, WLReportService, UIControlService) {

		var vm = this;
		vm.init = init;
		vm.datenow = new Date();

		vm.monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

		function init() {
		    $translatePartialLoader.addPart('kpi-contract');
		    barangLokal();
		    barangnasional();
		    lokalbyType();
		    nasionalbyType();
		}

		function grafiklokal(supplierwl, vendorperforma, persen, label) {
		    var maxs = [];
		    maxs[0] = Math.max.apply(Math, persen);
		    maxs[1] = Math.max.apply(Math, vendorperforma);
		    maxs[2] = Math.max.apply(Math, supplierwl);
		    var maxvalue = Math.max.apply(Math, maxs);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = parseInt((maxvalue / 9).toFixed(0));
		    }
		    else {
		        maxvalue = 10;
		    }
		    vm.color = ['#8FBC8F', '#DC143C', '#3498DB'];
		    vm.datalokal = [
                supplierwl, vendorperforma, persen
		    ]
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labellokal = label;
		        vm.datasetlokal = [{
		            label: "Supplier WL"
		        }, { label: "Vendor Performa" }, { label: "% Performa" }];
		        vm.serieslokal = ['Supplier WL', 'Vendor Performa', '% Performa'];

		        vm.optionlokal = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: ''
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Periode'
		                    }
		                }]
		            }
		        };
		    }
		}


		function grafiknasional(supplierwl, vendorperforma, persen, label) {
		    var maxs = [];
		    maxs[0] = Math.max.apply(Math, persen);
		    maxs[1] = Math.max.apply(Math, vendorperforma);
		    maxs[2] = Math.max.apply(Math, supplierwl);
		    var maxvalue = Math.max.apply(Math, maxs);
		    var step = 1;
		    if (maxvalue >= 1) {
		        step = parseInt((maxvalue / 9).toFixed(0));
		    }
		    else {
		        maxvalue = 10;
		    }
		    console.info("maxvalue" + maxvalue);
		    vm.color = ['#8FBC8F', '#DC143C', '#3498DB'];
		    vm.datanasional = [
                supplierwl, vendorperforma, persen
		    ]
		    if (localStorage.getItem("currLang") === 'id') {
		        vm.labelnasional = label;
		        vm.datasetnasional = [{
		            label: "Supplier WL"
		        }, { label: "Vendor Performa" }, { label: "% Performa" }];
		        vm.seriesnasional = ['Supplier WL', 'Vendor Performa', '% Performa'];

		        vm.optionnasional = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: ''
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Periode'
		                    }
		                }]
		            }
		        };
		    }
		}


		vm.barangLokal = barangLokal;
		function barangLokal() {
		    WLReportService.warningLetterReport({
		        column: 1,
                Status:3090
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.barangLokal = reply.data;
		            //console.info("nasional:" + JSON.stringify(vm.barangLokal));
		            vm.supplierWLlokal = [];
		            vm.vendorperformalokal = [];
		            vm.persenperformalokal = [];
		            vm.label = [];
		            for (var i = 0; i <= vm.barangLokal.length - 1; i++) {
		                vm.supplierWLlokal[i] = vm.barangLokal[i].PerformaSupplierData.SupplierWL;
		                vm.vendorperformalokal[i] = vm.barangLokal[i].PerformaSupplierData.VendorPerforma;
		                vm.persenperformalokal[i] = vm.barangLokal[i].PerformaSupplierData.PersentasePerforma;
		                var startdate= new Date(vm.barangLokal[i].StartDate);
		                var enddate= new Date(vm.barangLokal[i].EndDate);
		                vm.label[i] = vm.monthNames[startdate.getMonth()] + "-" + vm.monthNames[enddate.getMonth()];
		            }
		            grafiklokal(vm.supplierWLlokal, vm.vendorperformalokal, vm.persenperformalokal, vm.label);
                    /*
		            console.info("supplierWLlokal" + JSON.stringify(vm.supplierWLlokal));
		            console.info("vendorperformalokal" + JSON.stringify(vm.vendorperformalokal));
		            console.info("persenperformlokal" + JSON.stringify(vm.persenperformalokal));
		            console.info("label" + JSON.stringify(vm.label));*/
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data modify vendor");
		        UIControlService.unloadLoading();
		    });
		}

		vm.lokalbyType = lokalbyType;
		function lokalbyType() {
		    WLReportService.reportbyWLtype({
		        column: 1,
		        Status: 3090
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.listlokalbyType = reply.data;
		            console.info("lokal by type:" + JSON.stringify(vm.listlokalbyType));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data modify vendor");
		        UIControlService.unloadLoading();
		    });
		}

		vm.nasionalbyType = nasionalbyType;
		function nasionalbyType() {
		    WLReportService.reportbyWLtype({
		        column: 2,
		        Status: 3090
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.listnasionalbyType = reply.data;
		            console.info("nat by type:" + JSON.stringify(vm.listnasionalbyType));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data modify vendor");
		        UIControlService.unloadLoading();
		    });
		}

		vm.barangnasional = barangnasional;
		function barangnasional() {
		    WLReportService.warningLetterReport({
		        column: 2,
		        Status: 3090
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.barangnasional = reply.data;
		            //console.info("nasional:" + JSON.stringify(vm.barangnasional));
		            vm.supplierWLnasional = [];
		            vm.vendorperformanasional = [];
		            vm.persenperformanasional = [];
		            vm.label = [];
		            for (var i = 0; i <= vm.barangnasional.length - 1; i++) {
		                vm.supplierWLnasional[i] = vm.barangnasional[i].PerformaSupplierData.SupplierWL;
		                vm.vendorperformanasional[i] = vm.barangnasional[i].PerformaSupplierData.VendorPerforma;
		                vm.persenperformanasional[i] = vm.barangnasional[i].PerformaSupplierData.PersentasePerforma;
		                var startdate = new Date(vm.barangnasional[i].StartDate);
		                var enddate = new Date(vm.barangnasional[i].EndDate);
		                vm.label[i] = vm.monthNames[startdate.getMonth()] + "-" + vm.monthNames[enddate.getMonth()];
		            }
		            grafiknasional(vm.supplierWLnasional, vm.vendorperformanasional, vm.persenperformanasional, vm.label);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data modify vendor");
		        UIControlService.unloadLoading();
		    });
		}

	}
})();
