(function () {
	'use strict';

	angular.module("app").controller("ProcSupportReportCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'ProcSupportReportService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, ProcSupportReportService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.procTypes = [];
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.isCalendarOpened = [false, false, false, false];

		vm.data = [];

		function init() {
		    $translatePartialLoader.addPart('proc-support-report');
		    konfigurasiWaktu();
		    //dataWorkloadProc();
		    //grafikNewVendor();
		}


		vm.filter = filter;
		function filter() {
		    var invalidFilter = false;
		    if (vm.durasi == null) {
		        invalidFilter = true;
		    }
		    else {
		        if (vm.durasi.Value == 0) { //minggu
		            if (vm.ma == null || vm.mb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ma.NumberOfWeek > vm.mb.NumberOfWeek) {
		                    invalidFilter = true;
		                }
		                else {
		                    vm.batasAtas = vm.ma.NumberOfWeek;
		                    vm.batasBawah = vm.mb.NumberOfWeek;
		                }
		            }
		        }
		        else if (vm.durasi.Value == 1) { //bulan         
		            if (vm.ba == null || vm.bb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ba > vm.bb) {
		                    invalidFilter = true;
		                }
		                else {
		                    var intBatasAtas = vm.ba.getMonth();
		                    var intBatasBawah = vm.bb.getMonth();
		                    vm.batasAtas = intBatasAtas + 1;
		                    vm.batasBawah = intBatasBawah + 1;
		                }
		            }

		        }
		    }
		    if (invalidFilter == true) {
		        UIControlService.msg_growl("warning", "MESSAGE.INVALID_FILTER");
		        return;
		    }
		    else if (invalidFilter == false) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        dataReport();
		    }
		}

		vm.cekFilterTahun = cekFilterTahun;
		function cekFilterTahun() {
		    vm.tahun = vm.filterTahun.getFullYear();
		    if (vm.durasi.Value == 0) {
		        weekByYear();
		    }
		    else if (vm.durasi.Value == 1) {
		        var mindateNow = new Date("January 01, " + vm.tahun + " 00:00:00");
		        var maxdateNow = new Date("December 31, " + vm.tahun + " 23:59:59");
		        vm.datepickeroptions = {
		            minMode: 'month',
		            maxDate: maxdateNow,
		            minDate: mindateNow
		        }
		        vm.ba = mindateNow;
		        vm.bb = maxdateNow;
		    }
		}


		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		vm.konfigurasiWaktu = konfigurasiWaktu;
		function konfigurasiWaktu() {
		    vm.datenow = new Date();
		    vm.getYearNow = vm.datenow.getFullYear();
		    var mindateNow = new Date("January 01, " + vm.getYearNow + " 00:00:00");
		    var maxdateNow = new Date("December 31, " + vm.getYearNow + " 23:59:59");
		    //mindateNow.setYear(vm.getYearNow - 1);
		    vm.datepickeroptions = {
		        minMode: 'month',
		        maxDate: maxdateNow,
		        minDate: mindateNow
		    }
		    vm.datepickeroptionsTahun = {
		        minMode: 'year'
		    }
		    if (localStorage.getItem("currLang") === 'id' || localStorage.getItem("currLang") === 'ID') {
			    vm.ddDurasi =
	            [
	                { Value: 0, Name: "Minggu" },
	                { Value: 1, Name: "Bulan" }

	            ]
	        }
		    else if (localStorage.getItem("currLang") === 'en' || localStorage.getItem("currLang") === 'EN') {
	        	vm.ddDurasi =
	            [
	                { Value: 0, Name: "Week" },
	                { Value: 1, Name: "Month" }

	            ]
	        }
		    //vm.jenistender = vm.ddTender[0];
		    vm.durasi = vm.ddDurasi[0];
		    vm.filterTahun = vm.datenow;
		    vm.tahun = vm.getYearNow;
		    weekByYear();
		}

		function weekByYear() {
		    ProcSupportReportService.weekByYear({
		        column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.weeks = reply.data;
		            //console.info("weeks" + JSON.stringify(vm.weeks));
		            for (var i = 0; i <= vm.weeks.length - 1; i++) {
		                vm.weeks[i].label = "Week #" + vm.weeks[i].NumberOfWeek + " (" + UIControlService.convertDate(vm.weeks[i].StartDate) + " s/d " + UIControlService.convertDate(vm.weeks[i].EndDate) + ")";
		            }
		            vm.ma = vm.weeks[0];
		            vm.mb = vm.weeks[vm.weeks.length - 1];
		            vm.batasAtas = vm.ma.NumberOfWeek;
		            vm.batasBawah = vm.mb.NumberOfWeek;
		            dataReport();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
		        UIControlService.unloadLoading();
		    });
		}

		function dataReport() {
		    ProcSupportReportService.procSupportReport({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Status: vm.durasi.Value
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.data = reply.data;
		            console.info("data" + JSON.stringify(vm.data));
		            //grafik(vm.data);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		function grafik(param) {
		    vm.aktivasi = []; vm.taktivasi = []; vm.verifikasi = []; vm.tverifikasi = [];
		    vm.apprv_cr = []; vm.reject_cr = []; vm.apprv_crdata = []; vm.reject_crdata = [];
		    vm.label = [];
		    for (var i = 0; i <= param.length-1; i++) {
		        vm.aktivasi[i] = param[i].Aktivasi;
		        vm.taktivasi[i] = param[i].TolakAktivasi;
		        vm.verifikasi[i] = param[i].Verifikasi;
		        vm.tverifikasi[i] = param[i].TolakVerifikasi;
		        vm.apprv_cr[i] = param[i].ApproveChangeReq;
		        vm.reject_cr[i] = param[i].RejectChangeReq;
		        vm.apprv_crdata[i] = param[i].ApproveChangeReqData;
		        vm.reject_crdata[i] = param[i].RejectChangeReqData;
		        vm.label = param[i].EmployeeName;
		    }
		    vm.arrMaxValue = [];
		    vm.arrMaxValue[0] = Math.max.apply(Math, vm.aktivasi);
		    vm.arrMaxValue[1] = Math.max.apply(Math, vm.taktivasi);
		    vm.arrMaxValue[2] = Math.max.apply(Math, vm.verifikasi);
		    vm.arrMaxValue[3] = Math.max.apply(Math, vm.tverifikasi);
		    vm.arrMaxValue[4] = Math.max.apply(Math, vm.apprv_cr);
		    vm.arrMaxValue[5] = Math.max.apply(Math, vm.reject_cr);
		    vm.arrMaxValue[6] = Math.max.apply(Math, vm.apprv_crdata);
		    vm.arrMaxValue[7] = Math.max.apply(Math, vm.reject_crdata);
		    var maxvalue = Math.max.apply(Math, vm.arrMaxValue);
		    var step = 1;
		    if (maxvalue > 10) {
		        step = (maxvalue / 10).toFixed(1);
		    }
		    else {
		        maxvalue = 10;
		    }
		    vm.colours = ['#8FBC8F', '#DC143C', '#3498DB'];
		    vm.datagrafik = [
                vm.aktivasi, vm.taktivasi, vm.verifikasi, vm.tverifikasi, vm.apprv_cr, vm.reject_cr, vm.apprv_crdata, vm.reject_crdata
		    ]
		    if (localStorage.getItem("currLang") === 'id') {
		        //vm.labelNewVendor = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		        vm.datasetNewVendor = [{
		            label: "Aktivasi"},
                { label: "Tolak Aktivasi" },
                { label: "Verifikasi" }
		        , { label: "Tolak Verifikasi" }
		        , { label: "Approve Permintaan Ubah Data" }
		        , { label: "Reject Permintaan Ubah Data" }
		        , { label: "Final Approve Permintaan Ubah Data" }
		        , { label: "Final Reject Permintaan Ubah Data" }];
		        vm.seriesNewVendor = ['Aktivasi'
                    , 'Tolak Aktivasi'
                    , 'Verifikasi'
		            , 'Tolak Verifikasi'
		            , 'Approve Permintaan Ubah Data'
		            , 'Reject Permintaan Ubah Data'
		            , 'Final Approve Permintaan Ubah Data'
		            , 'Final Reject Permintaan Ubah Data'];

		        vm.optionNewVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Nama Pegawai'
		                    }
		                }]
		            }
		        };
		    }
		    else {
		        //vm.labelNewVendor = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		        vm.datasetNewVendor = [{
		            label: "Activation"
		        },
                { label: "Rejection of Activation" },
                { label: "Verification" }
		        , { label: "Rejection of Verification" }
		        , { label: "Approved Change Request" }
		        , { label: "Rejection of Change Request" }
		        , { label: "Final Approved Change Request" }
		        , { label: "Final Rejection of Change Request" }];
		        vm.seriesNewVendor = ['Activation'
                    , 'Rejection of Activation'
                    , 'Verification'
		            , 'Rejection of Verification'
		            , 'Approved Change Request'
		            , 'Rejection of Change Request'
		            , 'Final Approved Change Request'
		            , 'Final Rejection of Change Request'];

		        vm.optionNewVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Vendor Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Employee Name'
		                    }
		                }]
		            }
		        };
		    }
		}

	}
})();
