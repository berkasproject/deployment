﻿(function () {
    'use strict';

    angular.module("app").controller("NegosiasiCtrl", ctrl);

    ctrl.$inject = ['$filter', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'NegosiasiService', 'DataPengadaanService',
        'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter,$http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiService, DataPengadaanService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.StepID = Number($stateParams.StepID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.NegoId = 0;
        vm.jLoad = jLoad;

        function init() {
            $translatePartialLoader.addPart('negosiasi');
            UIControlService.loadLoading("MESSAGE.LOADING");

            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            jLoad(1);
            loadStep();
            loadOvertime();

        }

        vm.loadStep = loadStep;
        function loadStep() {
            NegosiasiService.loadStep({ ID: vm.StepID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listTenderStep = reply.data;
                    vm.listTenderStep.StartDate = UIControlService.convertDateTime(vm.listTenderStep.StartDate);
                    vm.listTenderStep.EndDate = UIControlService.convertDateTime(vm.listTenderStep.EndDate);
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadOvertime = loadOvertime;
        function loadOvertime() {
            NegosiasiService.isOvertime({ ID: vm.StepID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.overtime = reply.data;
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.list = [];
            vm.nego = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            var tender = {
                Status: vm.TenderRefID,
                FilterType: vm.ProcPackType,
                column: vm.StepID
            }
            NegosiasiService.selectFirst(tender, function (reply) {
                console.info("data:"+JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.nego = reply.data;
                    //console.info("data:" + JSON.stringify(vm.nego));
                    vm.flagSum = true;
                    for (var i = 0; i < vm.nego.length; i++) {
                        if (vm.nego[i].ID !== 0) {
                            if (vm.nego[i].IsDeal !== null) {
                                console.info(vm.nego[i].IsDeal);
                                vm.flagSum = false;
                            }
                            else {
                                console.info(vm.nego[i].IsDeal);
                                vm.flagSum = true;
                            }
                            vm.list.push(vm.nego[i]);
                        }
                    }
                    vm.TenderStepDataID = vm.nego[0].TenderStepDataID;
                    cek();
                    for (var i = 0; i < vm.nego.length; i++) {
                        if (vm.nego[i].ID === 0) {
                            vm.cek_summary = true;
                            i = vm.nego.length;
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Negosiasi" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.cek = cek;
        function cek() {
            NegosiasiService.cek({
                TenderStepDataID: vm.TenderStepDataID
            }, function (reply) {
                console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.hasil_cek = reply.data;
                    //console.info("data:" + JSON.stringify(vm.hasil_cek));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                // console.info("error:" + JSON.stringify(err));
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        //modal summary
        vm.do_summary = do_summary;
        function do_summary() {
            var data = {
                item: vm.list
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi/summary.html?v=1.000002',
                controller: 'SummaryCtrl',
                controllerAs: 'SummaryCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        /*
        vm.detail = detail;
        function detail(flag) {
            if (flag === 1) {
                var data = {
                    item: vm.evaltechnical,
                    act: true
                }
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/evaluasi-teknis/FormEvaluator.html',
                    controller: 'FormEvaluator',
                    controllerAs: 'FormEvaluator',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    init();
                });
            }
            else if (flag === 2) {
                $state.transitionTo('evaluasi-teknis-tim', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
            }
            else if (flag === 3) {
                $state.transitionTo('data-evaluator', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType });
            }
            else if (flag === 4) {
                var data = {
                    item: vm.evaltechnical,
                    act: false
                }
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/evaluasi-teknis/FormEvaluator.html',
                    controller: 'FormEvaluator',
                    controllerAs: 'FormEvaluator',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    init();
                });
            }
        }
        */

        vm.viewDetail = viewDetail;
        function viewDetail(index, data, flag, flagNego) {
            if (flag === true) {
                if (vm.overtime == false) {
                    UIControlService.msg_growl("error", "MESSAGE.NEGO_TIDAK_BISA");
                    return;
                }
                else {



                    if (flagNego === true) {
                        if (index == 0) {
                            bootbox.confirm($filter('translate')('MESSAGE.YAKIN_SEPAKAT'), function (yes) {
                                if (yes) {
                                    UIControlService.loadLoadingModal("MESSAGE.LOADING");
                                    var dt = {
                                        SOEPDId: data.SOEPDId,
                                        VendorID: data.VendorID,
                                        TenderStepDataID: vm.StepID,
                                        IsNego: true,
                                        IsNeedApproval: false
                                    }
                                    NegosiasiService.Insert(dt,
                                   function (reply) {
                                       if (reply.status === 200) {
                                           vm.NegoId = reply.data.ID;
                                           init();
                                       }
                                       else {
                                           UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                                           return;
                                       }
                                   },
                                   function (err) {
                                       //UIControlService.msg_growl("error", "Gagal Akses Api!!");
                                   }
                               );
                                }
                            });
                        }
                        else {
                            bootbox.confirm($filter('translate')('MESSAGE.APPROVAL'), function (yes) {
                                if (yes) {
                                    UIControlService.loadLoadingModal("MESSAGE.LOADING");
                                    var datainsert = {
                                        VendorID: data.VendorID,
                                        TenderStepDataID: vm.StepID,
                                        IsNego: true,
                                        IsNeedApproval: true
                                    }
                                    NegosiasiService.Insert(datainsert,
                                    function (reply) {
                                        if (reply.status === 200) {
                                            UIControlService.unloadLoading();
                                            vm.NegoId = reply.data.ID;
                                            sendApprovalNego();
                                        }
                                    },
                                    function (err) {
                                        // UIControlService.msg_growl("error", "Gagal Akses Api!!");
                                    }
                                );
                                }
                            });
                        }

                    }
                    else {
                        if (index == 0) {
                            bootbox.confirm($filter('translate')('MESSAGE.YAKIN_NEGO'), function (yes) {
                                if (yes) {
                                    UIControlService.loadLoadingModal("MESSAGE.LOADING");
                                    var dt = {
                                        SOEPDId: data.SOEPDId,
                                        VendorID: data.VendorID,
                                        TenderStepDataID: vm.StepID,
                                        IsNego: false
                                    }
                                    NegosiasiService.Insert(dt,
                                   function (reply) {
                                       if (reply.status === 200) {
                                           vm.NegoId = reply.data.ID;
                                           sendMailNego(reply.data.ID);
                                           $state.transitionTo('negosiasi-chat', { NegoId: vm.NegoId, StepID: data.TenderStepDataID, VendorID: data.VendorID });
                                       }
                                       else {
                                           UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                                           return;
                                       }
                                   },
                                   function (err) {
                                       //UIControlService.msg_growl("error", "Gagal Akses Api!!");
                                   }
                               );
                                }
                            });
                        }
                        else {
                            bootbox.confirm($filter('translate')('MESSAGE.APPROVAL'), function (yes) {
                                if (yes) {
                                    UIControlService.loadLoadingModal("MESSAGE.LOADING");
                                    var dt = {
                                        SOEPDId: data.SOEPDId,
                                        VendorID: data.VendorID,
                                        TenderStepDataID: vm.StepID,
                                        IsNego: false,
                                        IsNeedApproval: true
                                    }
                                    NegosiasiService.Insert(dt,
                                   function (reply) {
                                       if (reply.status === 200) {
                                           vm.NegoId = reply.data.ID;
                                           sendApprovalNego();
                                       }
                                       else {
                                           UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                                           return;
                                       }
                                   },
                                   function (err) {
                                       //UIControlService.msg_growl("error", "Gagal Akses Api!!");
                                   }
                               );
                                }
                            });
                        }

                    }
                }
            }
            else {
                if (data.IsNego == true)
                    $state.transitionTo('list-sub-nego', { TenderRefID: vm.TenderRefID, VendorID: data.VendorID, ProcPackType: vm.ProcPackType });
                else
                    $state.transitionTo('negosiasi-chat', { NegoId: data.NegoId, StepID: data.TenderStepDataID, VendorID: data.VendorID });
            }          
        }


        vm.print = print;
        function print() {
            $state.transitionTo('negosiasi-print', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
        }


        vm.sendMailNego = sendMailNego;
        function sendMailNego(negoId) {
            var datainsert = {
                ID: negoId
            }
            NegosiasiService.sendMail(datainsert,
                function (reply) {
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.MAIL_SENT");
                    }
                    else {
                        //UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                        return;
                    }
                },
                function (err) {
                    //UIControlService.msg_growl("error", "Gagal Akses Api!!");
                }
            );
        }

        vm.backpengadaan = backpengadaan;
        function backpengadaan() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.listTenderStep.TenderID });
        }

        vm.EndNego = EndNego;
        function EndNego(data) {
            bootbox.confirm($filter('translate')('MESSAGE.AKHIR_NEGO'), function (yes) {
                if (yes) {
                    NegosiasiService.InsertActive({
                        ID: data.NegoId,
                        IsActive: false
                    }, function (reply) {
                          if (reply.status === 200) {
                              UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                              init();
                          }
                          else {
                              UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                              return;
                          }
                    }, function (err) {
                        UIControlService.msg_growl("error", "MESSAGE.API");
                    });
                }
            });
        }

        vm.reOpen = reOpen;
        function reOpen(data) {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REOPEN_NEGO'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("");
                    NegosiasiService.CanReOpenNego({
                        NegoId: data.NegoId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.data === true) {
                            UIControlService.loadLoading("");
                            NegosiasiService.ReOpenNegotiation({
                                NegoId: data.NegoId
                            }, function (reply) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REOPEN_NEGO'));
                                sendMailNego(data.NegoId);
                                init();
                            }, function (error) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
                            });
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_MAX_NEGO'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
                    });
                }
            });
        }

        vm.sendApprovalNego = sendApprovalNego;
        function sendApprovalNego() {
            var datainsert = {
                NegoId: vm.NegoId
            }
            NegosiasiService.InsertApproval(datainsert,
                function (reply) {
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.APP_SENT");
                        sendMailToApprover();
                        init();
                    }
                    else {
                        //UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                        return;
                    }
                },
                function (err) {
                    //UIControlService.msg_growl("error", "Gagal Akses Api!!");
                }
            );
        }

        vm.sendMailToApprover = sendMailToApprover;
        function sendMailToApprover() {
            var datainsert = {
                NegoId: vm.NegoId
            }
            NegosiasiService.sendMailToApprover(datainsert,
                function (reply) {
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.MAIL_SENT");
                    }
                    else {
                        //UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                        return;
                    }
                },
                function (err) {
                    //UIControlService.msg_growl("error", "Gagal Akses Api!!");
                }
            );
        }

        vm.detailApproval = detailApproval;
        function detailApproval(data) {
            var item = {
                ID: data.NegoId
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi/detailApproval.modal.html',
                controller: 'detailApprovalNegoCtrl',
                controllerAs: 'detailApprovalNegoCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.detailApprovalFinance = detailApprovalFinance;
        function detailApprovalFinance(data) {
            var item = {
                ID: data.NegoId,
                isAllowedEdit: vm.isAllowedEdit
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi/detailApprovalFinance.modal.html?v=1.000002',
                controller: 'detailApprovalFinanceNegoCtrl',
                controllerAs: 'detAppFinanceNegoCtrl',
                resolve: { item: function () { return item; } }
            });
            modalInstance.result.then(function () {
                init();
            });
        };
    }
})();
//TODO


