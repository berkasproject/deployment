﻿(function () {
    'use strict';

    angular.module("app").controller("ListSubNego", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService',
        'NegosiasiService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService,
        NegosiasiService, RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.VendorID = Number($stateParams.VendorID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.NegoId = 0;
        vm.jLoad = jLoad;

        function init() {
            $translatePartialLoader.addPart('negosiasi');
            UIControlService.loadLoading("");
            jLoad(1);

        }
        vm.loadOvertime = loadOvertime;
        function loadOvertime() {
            UIControlService.loadLoading("");
            NegosiasiService.isOvertime({ ID: vm.nego[0].TenderStepDataID}, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.overtime = reply.data;
                } else {
                    //$.growl.error({ message: "Gagal mendapatkan data Chatting" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            DataPengadaanService.IsAllowedEdit({
                TenderRefID: vm.TenderRefID,
                ProcPackageType: vm.ProcPackType
            }, function (reply) {
                vm.isAllowedEdit = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
            });

            vm.nego = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            var tender = {
                TenderRefID: vm.TenderRefID,
                VendorID: vm.VendorID,
                CRCESubId: 0,
                ProcPackType: vm.ProcPackType
            }
            NegosiasiService.selectVendor(tender, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.nego = reply.data;
                    loadOvertime();
                    vm.Generate = vm.nego[0].IsGenerate;
                    vm.cost = 0; vm.cosEstimate = 0; vm.totalNego = 0; vm.cosEstimateRev = 0;

                    for (var i = 0; i < vm.nego.length; i++) {
                        vm.cost += vm.nego[i].OfferTotalCost;
                        vm.cosEstimate += vm.nego[i].TotalLineCost;
                        vm.totalNego += vm.nego[i].NegoTotalCost;
                        vm.cosEstimateRev += vm.nego[i].TotalLineRevCost;

                        if (i === (vm.nego.length - 1)) {
                            vm.total = (((vm.cost - vm.cosEstimate) / vm.cost) * 100).toFixed(2);
                            if (vm.total < 0) vm.total *= -1;
                            
                        }
                    }
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Evaluasi Teknis" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.detail = detail;
        function detail(data) {
            if (data.HasChildren)
            {
                var data = {
                    item: data,
                    overtime: vm.overtime,
                    TenderRefID: vm.TenderRefID,
                    VendorID: vm.VendorID,
                    ProcPackType: vm.ProcPackType
                }
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/negosiasi/modal-sub-parent.html?v=1.000002',
                    controller: 'DetailSubParentCtrl',
                    controllerAs: 'DetailSubParentCtrl',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    init();
                });
            }
            else {
                var data = {
                    item: data,
                    overtime: vm.overtime,
                    TenderRefID: vm.TenderRefID,
                    VendorID: vm.VendorID,
                    ProcPackType: vm.ProcPackType
                }
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/modules/panitia/negosiasi/modal-sub-line.html?v=1.000006',
                    controller: 'DetailSubLineCtrl',
                    controllerAs: 'DetailSubLineCtrl',
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    init();
                });
            }
           
        }

        vm.viewDetail = viewDetail;
        function viewDetail(data, flag) {
            if (flag === true) {
                var data = {
                    VendorID: data.VendorID,
                    TenderStepDataID: data.TenderStepDataID
                }
                NegosiasiService.Insert(data,
               function (reply) {
                   if (reply.status === 200) {
                       vm.NegoId = reply.data.ID;
                       $state.transitionTo('negosiasi-chat', { NegoId: vm.NegoId, StepID: data.TenderStepDataID, VendorID: data.VendorID });
                   }
                   else {
                       UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                       return;
                   }
               },
               function (err) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
               }
           );
            }

        }

        vm.Approval = Approval;
        function Approval() {
            var data = {
                item: vm.TenderRefID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/temporary/safetyEvaluation/DetailApproval.html',
                controller: 'DetailApprovalCtrl',
                controllerAs: 'DetailApprovalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.back = back;
        function back() {
            if (vm.nego[0].IsNego == true)
                $state.transitionTo('negosiasi', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, StepID: vm.nego[0].TenderStepDataID });
            else
                $state.transitionTo('negosiasi-chat', { NegoId:vm.nego[0].NegoId, VendorID: vm.VendorID, StepID: vm.nego[0].TenderStepDataID });
        }

        vm.generate = generate;
        function generate() {
                vm.Insert = {
                    IsGenerate :vm.Generate,
                    SOEPDId: vm.nego[0].SOEPDId,
                    TenderStepDataID: vm.nego[0].TenderStepDataID,
                    VendorID: vm.nego[0].VendorID
                };
            NegosiasiService.InsertByPersen(vm.Insert,
                                       function (reply) {
                                           UIControlService.loadLoading("MESSAGE.LOADING");
                                           if (reply.status === 200) {
                                               UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
                                               init();
                                           }
                                           else {
                                               UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
                                               return;
                                           }
                                       },
                                       function (err) {
                                           UIControlService.msg_growl("error", "MESSAGE.API");
                                       }
                                       );
            
            }
        
    }
})();
//TODO


