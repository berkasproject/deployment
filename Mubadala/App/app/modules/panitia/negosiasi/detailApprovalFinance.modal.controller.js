(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalFinanceNegoCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'NegosiasiService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, NegosiasiService) {

        var vm = this;
        vm.ID = item.ID;
        vm.isAllowedEdit = item.isAllowedEdit;

        vm.init = init;
        function init() {
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            UIControlService.loadLoadingModal("");
            NegosiasiService.CurrentFinanceApproval({
                NegoId: vm.ID
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                vm.list = reply.data;
                vm.allowSendApprove = vm.list.length === 0 || vm.list[0].ApprovalStatus === false;
                vm.allowReOpen = vm.list.length > 0 && vm.list[0].ApprovalStatus === false;
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
            });
        }

        vm.sendApproval = sendApproval;
        function sendApproval() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_FINANCE_APPROVAL'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    NegosiasiService.InsertFinanceApproval({
                        NegoId: vm.ID
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_SEND_APPROVAL'));
                        $uibModalInstance.close();
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_APPROVAL'));
                    });
                }
            });
        }

        vm.reOpen = reOpen;
        function reOpen() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REOPEN_NEGO'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    NegosiasiService.CanReOpenNego({
                        NegoId: vm.ID
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        if (reply.data === true) {
                            UIControlService.loadLoadingModal("");
                            NegosiasiService.ReOpenNegotiation({
                                NegoId: vm.ID
                            }, function (reply) {
                                UIControlService.unloadLoadingModal();
                                UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_REOPEN_NEGO'));
                                $uibModalInstance.close();
                            }, function (error) {
                                UIControlService.unloadLoadingModal();
                                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
                            });
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_MAX_NEGO'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_REOPEN_NEGO'));
                    });
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();