(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalNegoCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'NegosiasiService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, NegosiasiService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.ID = item.ID;
        vm.flag = item.flag;
        vm.flagNego = item.flagNego;
        vm.Status = item.Status;
        vm.crApps = [];
        vm.employeeFullName = "";
        vm.employeeID = 0;
        vm.information = "";
        vm.flagEmp = item.flag;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('verifikasi-tender');
            $translatePartialLoader.addPart('negosiasi');
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            vm.crApps = [];
            UIControlService.loadLoading(loadmsg);
            NegosiasiService.GetApproval({
                NegoId: vm.ID.NegoId
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.list = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reject = reject;
        function reject() {
            sendApproval(0);
        }

        function sendApproval(approvalStatus) {
            vm.ID.ApprovalStatus = false;
            vm.ID.flagNego = vm.flagNego;
            UIControlService.loadLoadingModal(loadmsg);
            NegosiasiService.updateApproval(vm.ID, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", $filter('translate')('Berhasil Approval'));
                    console.info("nego");
                    sendMail();
                    $uibModalInstance.close();

                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
            });
        }
        function sendMail() {
            vm.ID.ApprovalStatus = false;
            UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            NegosiasiService.sendMailToCE(vm.ID,
                function (response) {
                    console.info(response);
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        UIControlService.msg_growl("notice", "MESSAGE.MAIL_SENT");
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }
    }
})();