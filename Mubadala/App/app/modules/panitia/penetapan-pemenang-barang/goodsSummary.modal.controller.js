﻿(function () {
	'use strict';

	angular.module("app").controller("goodsSummaryController", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', '$state', 'UIControlService', '$uibModalInstance', 'GlobalConstantService', 'item', 'GoodsAwardService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, $state, UIControlService, $uibModalInstance, GlobalConstantService, item, GoodsAwardService) {
		var vm = this

		vm.Summary = item.Summary
		vm.VendorName = item.VendorName
		vm.isReadOnly = item.isReadOnly
		vm.ID = item.ID

		function init() {

		}

		vm.save = save;
		function save() {
			//UIControlService.loadLoadingModal("MESSAGE.LOADING");
			//GoodsAwardService.saveGoodsSummary({ ID: vm.ID, Summary: vm.Summary }, function (reply) {
			//	UIControlService.unloadLoadingModal();
			//	if (reply.status === 200) {
			//		UIControlService.msg_growl("success", "SUCC_APPRV");
			//	} else {
			//		UIControlService.msg_growl("error", "ERR_SAVE_DATA");
			//		return;
			//	}
			//}, function (err) {
			//	UIControlService.msg_growl("error", "ERR_SAVE_DATA");
			//	UIControlService.unloadLoadingModal();
			//});

			$uibModalInstance.close(vm.Summary);
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
			//$uibModalInstance.close();
		};
	}
})();;