(function () {
	'use strict';

	angular.module("app").controller("CSMSReportCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'CSMSReportService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, CSMSReportService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.arrKPI = [];
		vm.arrOntime = [];
		vm.arrSavingCost = [];

		vm.jenistender = null;
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.currentPageCP = 1;
		vm.currentPageCS = 1;

		vm.isCalendarOpened = [false, false, false, false];

		function init() {
		    $translatePartialLoader.addPart('cpr-report');
		    var dateNow = new Date();
		    vm.CSMS_maindate = dateNow;
		    timeconfiguration();
		    getStartEndDate(dateNow);
		}

		function timeconfiguration() {
		    vm.datepickeroptions = {
		        minMode: 'month'
		    }
		}

		vm.loadCSMS_maindate = loadCSMS_maindate;
		function loadCSMS_maindate() {
		    //console.info("mainDate:" + vm.CSMS_maindate);
		    getStartEndDate(vm.CSMS_maindate);
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		function getDaysInMonth(m, y) {
		    return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
		}

		function getStartEndDate(initdate) {
		    var yearNow = initdate.getFullYear();
		    var monthnow = initdate.getMonth();
		    var lastDay = getDaysInMonth(monthnow+1, yearNow);

		    var startdate = new Date();
		    startdate.setDate(1);
		    startdate.setMonth(monthnow);
		    startdate.setYear(yearNow);

		    var enddate = new Date();
		    enddate.setDate(lastDay);
		    enddate.setMonth(monthnow);
		    enddate.setYear(yearNow);

		    //console.info("default:" + JSON.stringify("sd:" + startdate + ",ed:" + enddate));
		    CPRCSMSCount(startdate, enddate);
		    safetycompliance(startdate, enddate);
		    activeContract(startdate, enddate);
		    lowestRankSafetyPerformance(startdate, enddate);
		    highestRankSafetyPerformance(startdate, enddate);
		    cprReport(startdate,enddate);
		}

        vm.CPRCSMSCount=CPRCSMSCount;
		function CPRCSMSCount(startdate,enddate) {
		    CSMSReportService.CPRCSMSCount({
		        Date1: startdate,
                Date2: enddate
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.CPRCSMSCount = reply.data;
		            //console.info("cprCsms:" + vm.CPRCSMSCount);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data");
		        UIControlService.unloadLoading();
		    });
		}

		vm.safetycompliance = safetycompliance;
		function safetycompliance(startdate, enddate) {
		    CSMSReportService.safetyCompliance({
		        Date1: startdate,
		        Date2: enddate
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.sc = reply.data;
		            //console.info("safetycomp.:" + vm.sc);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data");
		        UIControlService.unloadLoading();
		    });
		}

		vm.activeContract = activeContract;
		function activeContract(startdate, enddate) {
		    CSMSReportService.activeContracts({
		        Date1: startdate,
		        Date2: enddate
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.ac = reply.data;
		            //console.info("active contr.:" + vm.ac);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data  ");
		        UIControlService.unloadLoading();
		    });
		}

		vm.cprReport = cprReport;
		function cprReport(startdate, enddate) {
		    CSMSReportService.cprDepartement({
		        Date1: startdate,
		        Date2: enddate
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.cr = reply.data;
		            //console.info("cr.:" + JSON.stringify(vm.cr));
                    grafik(vm.cr)
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data  ");
		        UIControlService.unloadLoading();
		    });
		}

		vm.lowestRankSafetyPerformance = lowestRankSafetyPerformance;
		function lowestRankSafetyPerformance(startdate, enddate) {
		    CSMSReportService.rankSafetyPerformance({
		        Date1: startdate,
		        Date2: enddate,
                column: 0
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.fromlowest = reply.data;
		            //console.info("lowest:" + JSON.stringify(vm.fromlowest));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data  ");
		        UIControlService.unloadLoading();
		    });
		}

		vm.highestRankSafetyPerformance = highestRankSafetyPerformance;
		function highestRankSafetyPerformance(startdate, enddate) {
		    CSMSReportService.rankSafetyPerformance({
		        Date1: startdate,
		        Date2: enddate,
		        column: 1
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.fromhighest = reply.data;
		            //console.info("highest:" + JSON.stringify(vm.fromhighest));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapatkan data  ");
		        UIControlService.unloadLoading();
		    });
		}


		function grafik(param) {
		    vm.active = []; vm.hasCPR = []; vm.label = [];
		    for (var i = 0; i <= param.length-1; i++) {
		        vm.active[i] = param[i].ContractCreatedCount;
		        vm.hasCPR[i] = param[i].ContractHasCPRCount;
		        vm.label[i] = param[i].DepartmentCode;
		    }
		    var maxvalue = Math.max.apply(Math, vm.active);
		    var step = 1;
		    if (maxvalue > 10) {
		        step = Math.ceil(maxvalue / 10);
		    }
		    else {
		        maxvalue = 10;
		    }
		    vm.colours = ['#8FBC8F', '#DC143C'];
		    vm.dataGrafik = [
                vm.active, vm.hasCPR
		    ]
		    if (localStorage.getItem("currLang") === 'ID') {
		        vm.labelNewVendor = vm.label;
		        vm.datasetNewVendor = [{
		            label: "Jumlah Kontrak"
		        }, { label: "Jumlah Kontrak yang Memiliki CPR" }];
		        vm.seriesNewVendor = ['Jumlah Kontrak', 'Jumlah Kontrak yang Memiliki CPR'];

		        vm.optionNewVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Jumlah Kontrak'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Kode Departemen'
		                    }
		                }]
		            }
		        };
		    }
		    else {
		        vm.labelNewVendor = vm.label;
		        vm.datasetNewVendor = [{
		            label: "Active Contract"
		        }, { label: "Contract Has CPR" }];
		        vm.seriesNewVendor = ['Active Contract', 'Contract Has CPR'];

		        vm.optionNewVendor = {
		            legend: {
		                display: true,
		                labels: {
		                    fontColor: 'rgb(255, 99, 132)'
		                }
		            },
		            scales: {
		                yAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Contract Count'
		                    },
		                    ticks: {
		                        min: 0,
		                        max: maxvalue,
		                        stepSize: step
		                    }
		                }],
		                xAxes: [{
		                    scaleLabel: {
		                        display: true,
		                        labelString: 'Department Code'
		                    }
		                }]
		            }
		        };
		    }
		}


	}
})();
