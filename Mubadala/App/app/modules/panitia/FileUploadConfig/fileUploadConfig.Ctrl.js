﻿(function () {
	'use strict';

	angular.module("app").controller("fileUploadConfigCtrl", ctrl);

	ctrl.$inject = [];
	function ctrl() {
		var vm = this;

		vm.init = init;
		function init() {
			vm.filterBy = Number($stateParams.searchBy);
			vm.srchText = $stateParams.keyword;

			if (vm.filterBy === 0 || vm.filterBy === undefined) {
				vm.filterBy = 1;
			}

			jLoad();
		}
	}

	function jLoad() {
		UIControlService.loadLoading("Querying configs...");
		var offset = (vm.currPage * 10) - 10;
		FileUploadConfigService.Select({
			Offset: offset,
			Limit: vm.maxSize,
			Keyword: vm.srchText,
			Status: 1,
			FilterType: vm.filterBy
		}, function (reply) {
			UIControlService.unloadLoading();
			if (reply.status === 200) {
				var data = reply.data;
				vm.configs = data.List;
				vm.totalItems = Number(data.Count);
			} else {
				$.growl.error({ message: "Gagal mendapatkan data hak akses." });
				UIControlService.unloadLoading();
			}
		}, function (err) {
			$.growl.error({ message: "Gagal mendapatkan data hak akses." });
			//console.info("error:" + JSON.stringify(err));
			//$.growl.error({ message: "Gagal Akses API >" + err });
			UIControlService.unloadLoading();
		});
	}
})();