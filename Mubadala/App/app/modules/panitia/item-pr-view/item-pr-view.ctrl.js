﻿(function () {
	'use strict';

	angular.module("app").controller("ItemPRViewCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'ItemPRViewService', 'UIControlService', '$filter', '$uibModal'];

	function ctrl($http, $translate, $translatePartialLoader, $location, $state, ItemPRViewService, UIControlService, $filter, $uibModal) {
		var vm = this;
		vm.init = init;
		vm.pageSize = 10;
		vm.currentPage = 1;
		vm.listDropdown =
        [
            { Value: 1, Name: "Purchase Req" },
            { Value: 4, Name: "Material" }
        ]
		vm.column = { Value: 0, Name: "undefined" };

		function init() {
			$translatePartialLoader.addPart("purchase-requisition");
			//UIControlService.loadLoading("MESSAGE.LOADING");
			loadDataItemPR(1);
		}
		vm.column = {Value:0,Name:"undefined"};
		vm.Keyword = '';
		vm.IntNull = null;

		vm.cari = cari;
		function cari() {
		    if (vm.column.Value == 0) {
		        UIControlService.msg_growl("warning", "MESSAGE.NEED_VARIABLEINPUT");
		        return;
		    }
		    else {
		        loadDataItemPR(1);
		    }
		}

		vm.loadDataItemPR = loadDataItemPR;
		function loadDataItemPR(current) {
			vm.dataItemPR = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			//console.info("masuk");
			UIControlService.loadLoading("MESSAGE.LOADING");
			ItemPRViewService.select({
			    column: vm.column.Value,
			    Keyword: vm.Keyword,
                IntNull:vm.IntNull,
				Offset: offset,
				Limit: vm.pageSize
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listItemPR = reply.data.List;
				    //console.info("itempr" + JSON.stringify(vm.listItemPR));
					vm.totalItems = reply.data.Count;
					vm.maxSize = vm.totalItems;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}


		vm.detail = detail;
		function detail(data) {
		    var item = {
		        data: data
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/item-pr-view/detailItemPR.html',
		        controller: 'DetailItemPRViewCtrl',
		        controllerAs: 'DetailItemPRViewCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}
	}
})()