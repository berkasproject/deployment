﻿(function () {
    'use strict';

    angular.module("app").controller("DetailItemPRViewCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ItemPRViewService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, ItemPRViewService,
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.item = item.data;

        function init() {
            $translatePartialLoader.addPart("purchase-requisition");
            //console.info("data:" + JSON.stringify(item));
            detailItemPr();
        }


        vm.detailItemPr=detailItemPr;
        function detailItemPr() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            ItemPRViewService.detailItemPr({
                ID:vm.item.ID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.data = reply.data;
                    //console.info("itempr" + JSON.stringify(vm.listItemPR));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }


        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }



    }
})();
//TODO


