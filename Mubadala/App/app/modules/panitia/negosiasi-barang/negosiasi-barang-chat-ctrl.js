﻿(function () {
    'use strict';

    angular.module("app").controller("NegosiasiBarangChatCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiBarangService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.StepID = Number($stateParams.StepID);
        //vm.GoodsNegoId = Number($stateParams.GoodsNegoId);
        vm.VendorID = Number($stateParams.VendorID);
        //vm.IsGenerate = Number($stateParams.IsGenerate);
        vm.Id = Number($stateParams.Id);
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.maxSize = 10;
        vm.init = init;
        //vm.TenderRefID = 70;
        //vm.ProcPackType = 0;
        vm.jLoad = jLoad;
        vm.nb = [];

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
              $translatePartialLoader.addPart('negosiasi-barang');
            //UIControlService.loadLoading("Silahkan Tunggu...");
            //loadTender();
            //jLoad(1);
            //loadOvertime();
            loadTender();
        }
        //tampil isi chat
       vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.nego = [];
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            var tender = {
                VendorID: vm.VendorID,
                TenderStepDataID: vm.StepID
            }
            NegosiasiBarangService.bychat(tender, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.nego = reply.data;
                    vm.nego.forEach(function (data) {
                        data.ChatDate = UIControlService.convertDateTime(data.ChatDate);
                    });
                    vm.VendorName = reply.data[0].VendorName;
                    vm.judul = reply.data[0].TenderName;
                    vm.idnb = vm.nego[0].GoodsNegoId
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadOvertime = loadOvertime;
        function loadOvertime() {
            NegosiasiBarangService.isOvertime({ ID: vm.StepID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.overtime = reply.data;
                } 
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadTender = loadTender;
        function loadTender() {
            UIControlService.loadLoading("");
            NegosiasiBarangService.selectStep({ ID: vm.StepID }, function (reply) {
                //console.info("dataTender:" + JSON.stringify(reply));
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    vm.TenderRefID = vm.step.tender.TenderRefID;
                    vm.ProcPackType = vm.step.tender.ProcPackageType;
                    jLoad(1);
                    loadOvertime();
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        //ambil data vendor
        vm.loadv = loadv;
        function loadv() {
            vm.lv = [];
            NegosiasiBarangService.select({
                column: vm.StepID,
                status: vm.TenderRefID,
                FilterType: vm.ProcPackType
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.lv = reply.data;
                    //ambil data isgenerate
                    for (var i = 0; i < vm.lv.length; i++) {
                        if (vm.lv[i].ID === vm.Id) {
                            vm.VendorName = vm.lv[i].VendorName;
                            vm.judul = vm.lv[i].TenderName;
                            vm.idnb = vm.lv[i].ID;
                        }
                    }
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
        //ke halaman detail penawaran
        vm.detailpenawaran = detailpenawaran;
        function detailpenawaran(flag) {
            $state.transitionTo('detail-penawaran', { VendorID: vm.VendorID, StepID: vm.StepID });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('negosiasi-barang', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, StepID: vm.StepID });
        }
        //tulis, insertchat
        vm.tulis = tulis;
        function tulis() {
            var data = {
                NegoId: vm.NegoId,
                VendorID: vm.VendorID,
                StepID: vm.StepID,
                Judul: vm.judul,
                GoodsNegoId: vm.idnb
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-barang/write-chat-barang.html?v=1.000002',
                controller: 'WriteChatBarangCtrl',
                controllerAs: 'WriteChatBarangCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

    }
})();
//TODO


