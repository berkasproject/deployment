(function () {
    'use strict';

    angular.module("app")
    .controller("detailApprovalGoodsNegoCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'GlobalConstantService', 'NegosiasiBarangService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, UIControlService, GlobalConstantService, NegosiasiBarangService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";
        vm.ID = item.ID;
        vm.flag = item.flag;
        vm.Status = item.Status;
        vm.crApps = [];
        vm.employeeFullName = "";
        vm.employeeID = 0;
        vm.information = "";
        vm.flagEmp = item.flag;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('negosiasi-barang');
            $translatePartialLoader.addPart('verifikasi-tender');
            loadData();
        };

        vm.loadData = loadData;
        function loadData() {
            vm.crApps = [];
            UIControlService.loadLoading(loadmsg);
            NegosiasiBarangService.GetApproval({
                GoodsNegoId: vm.ID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.list = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_APPROVERS'));
            });
        }

        vm.approve = approve;
        function approve() {
            sendApproval(1);
        }

        vm.reject = reject;
        function reject() {
            sendApproval(0);
        }

        function sendApproval(approvalStatus) {
            vm.flagApprove = approvalStatus;
            UIControlService.loadLoadingModal(loadmsg);
            GoodsAwardService.SendApproval({
                ID: vm.ID,
                Status: approvalStatus,
                Remark: vm.information,
                flagEmp: vm.flagEmp
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APPROVAL'));
                    console.info(vm.flagApprove);
                    $uibModalInstance.close(vm.ID, vm.flagApprove);
                    
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
                }
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
        function sendMail() {
            UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            GoodsAwardService.sendMail({ ID: vm.ID },
                function (response) {
                    console.info(response);
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        UIControlService.msg_growl("notice", "MESSAGE.SEND_TO_WIN");
                        $uibModalInstance.close();
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }
    }
})();