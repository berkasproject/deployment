﻿(function () {
    'use strict';

    angular.module("app").controller("detailPRLineModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', '$stateParams', 'PersetujuanPRService', 'GlobalConstantService'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, $stateParams, PersetujuanPRService, GlobalConstantService) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.data = item.data;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.currentPage = 1;
        vm.totalItems = 0;
        console.log(item)
        vm.listCommodity = [];
        vm.dataPRL = [];
        //vm.PRID = item.data.PRID;

        function init() {
            jLoad()
        }

        function jLoad() {
            UIControlService.loadLoadingModal();
            PersetujuanPRService.getDetailPRLineByPRID({
                PRID:vm.data.PRID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.dataPRL = reply.data;
                } else {
                    UIControlService.msg_growl('error', "ERROR_API");

                }
                UIControlService.unloadLoadingModal();

            }, function (err) {
                UIControlService.msg_growl('error', "ERROR_API");
                UIControlService.unloadLoadingModal();
            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();