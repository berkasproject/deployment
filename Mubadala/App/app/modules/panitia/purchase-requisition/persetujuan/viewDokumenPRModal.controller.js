﻿(function () {
    'use strict';

    angular.module("app").controller("viewDokumenPRModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', '$stateParams', 'PersetujuanPRService', 'GlobalConstantService'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, $stateParams, PersetujuanPRService, GlobalConstantService) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.dataDokumenPR = [];
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.currentPage = 1;
        vm.totalItems = 0;
        console.log(item)
        vm.PRID = item.data.PRID;

        function init() {
            console.log('Init')
            jLoad(1)
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            PersetujuanPRService.selectDokumenPR({
                Offset: offset,
                Limit: vm.maxSize,
                IntParam1: vm.PRID
            }, function (reply) {
                //console.info("datane:" + JSON.stringify(reply));
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.dataDokumenPR = data.List;
                    vm.totalItems = data.Count;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                    UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                UIControlService.unloadLoadingModal();
            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();