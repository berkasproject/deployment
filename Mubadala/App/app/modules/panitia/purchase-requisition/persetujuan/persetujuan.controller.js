﻿(function () {
    'use strict';

    angular.module("app").controller("persetujuanPurchaseRequisitionCtrl", ctrl);

    ctrl.$inject = ['$http', '$state', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService','PersetujuanPRService'];

    function ctrl($http, $state, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, PersetujuanPRService) {
        var vm = this;

        vm.totalPRsebenarnya = 0;
        vm.selectBy = "stats";
        vm.PR = [];
        vm.totalItems = 0;
        vm.currentPage = 0;
        vm.pageSize = 10;
        vm.selectByValue = "4539";


        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('persetujuan-purchase-requisition');
            vm.PRSelected = [];

            vm.jLoad(1);
            console.log('sini')
        }

        vm.setSelectByValue = setSelectByValue;
        function setSelectByValue() {
            if (vm.selectBy == "all") {
                jLoad(1);
            }
            vm.selectByValue = "";
        }
        
        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading();
            vm.currentPage = current;
            PersetujuanPRService.select({
                Keyword: vm.selectBy,
                Keyword2: vm.selectByValue,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
            }, function (reply) {
                if (reply.status == 200) {
                    vm.PR = reply.data.List;
                    vm.totalItems = reply.data.Count;

                    for (var i = 0; i < vm.PR.length; i++) {
                        // $('#check'+vm.PR[i].pr_id).prop('checked', false);
                        if (i == 0) {
                            var temp = {
                                'PRID': vm.PR[i].PRID,
                                'PRLID': vm.PR[i].PRLID,
                                'PRCode': vm.PR[i].PRCode,
                                'PRName': vm.PR[i].PRName,
                                'Name': vm.PR[i].Name,
                                'MaterialCode': vm.PR[i].MaterialCode,
                                'parent_pr_line': true,
                                'Quantity': vm.PR[i].Quantity,
                                'OrderUnitName': vm.PR[i].OrderUnitName,
                                'UnitCost': vm.PR[i].UnitCost,
                                'DepartmentName': vm.PR[i].DepartmentName,
                                'EntityName': vm.PR[i].EntityName,
                                'EmployeeName': vm.PR[i].EmployeeName,
                                'LastApprovedDate': vm.PR[i].LastApprovedDate,
                                'PreparePRName': vm.PR[i].PreparePRName,
                                'LocationName': vm.PR[i].LocationName,
                                'CommodityID': vm.PR[i].CommodityID,
                                'PRStatus': vm.PR[i].PRStatus,
                                'Notes': "",
                            }

                            vm.PR.unshift(temp);
                        }
                        else {
                            if (vm.PR[i].PRID != vm.PR[(i - 1)].PRID) {
                                var temp = {
                                    'PRID': vm.PR[i].PRID,
                                    'PRLID': vm.PR[i].PRLID,
                                    'PRCode': vm.PR[i].PRCode,
                                    'PRName': vm.PR[i].PRName,
                                    'Name': vm.PR[i].Name,
                                    'MaterialCode': vm.PR[i].MaterialCode,
                                    'parent_pr_line': true,
                                    'Quantity': vm.PR[i].Quantity,
                                    'OrderUnitName': vm.PR[i].OrderUnitName,
                                    'UnitCost': vm.PR[i].UnitCost,
                                    'DepartmentName': vm.PR[i].DepartmentName,
                                    'EntityName': vm.PR[i].EntityName,
                                    'EmployeeName': vm.PR[i].EmployeeName,
                                    'LastApprovedDate': vm.PR[i].LastApprovedDate,
                                    'PreparePRName': vm.PR[i].PreparePRName,
                                    'LocationName': vm.PR[i].LocationName,
                                    'CommodityID': vm.PR[i].CommodityID,
                                    'PRStatus': vm.PR[i].PRStatus,
                                    'Notes': ""
                                }

                                vm.PR.splice(i, 0, temp);
                            }
                        }
                    }

                    //Checkbox for pagination
                    for (var i = 0; i < vm.PRSelected.length; i++) {
                        var index = vm.PR.findIndex(x => x.PRID === vm.PRSelected[i]);
                        console.log(index);
                        if (index != -1) {
                            vm.PR[index].parentSelected = true;
                        }
                    }

                    console.log(vm.PR)
                    UIControlService.unloadLoading();


                } else {
                    UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');

                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            })
        }

        vm.CheckParent = CheckParent;
        function CheckParent(dataPR) {
            
            if (vm.PRSelected.length != 0) {
                var index = vm.PRSelected.findIndex(x => x.PRID === dataPR.PRID);
                console.log(index);
                if (index == -1) {
                    vm.PRSelected.push(dataPR);
                } else {
                    vm.PRSelected.splice(index, 1);
                }
            } else {
                vm.PRSelected.push(dataPR);
            }

            console.log(vm.PRSelected);
        }

        vm.ModalSetuju = ModalSetuju;
        function ModalSetuju() {
            if (vm.PRSelected.length == 0) {
                UIControlService.msg_growl('warning', 'MESSAGE.DATA_PR_BELUM_DIPILIH');
                return;
            }
            console.log('Modal setuju')
            var data = {
                data: vm.PRSelected
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/persetujuan/setujuPRModal.html',
                controller: 'setujuPRModalController',
                controllerAs: 'setujuPRModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.ModalTolak = ModalTolak;
        function ModalTolak() {
            if (vm.PRSelected.length == 0) {
                UIControlService.msg_growl('warning', 'MESSAGE.DATA_PR_BELUM_DIPILIH');
                return;
            }
            console.log('Modal tolak')
            var data = {
                data: vm.PRSelected
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/persetujuan/tolakPRModal.html',
                controller: 'tolakPRModalController',
                controllerAs: 'tolakPRModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.lihatDokumenPR = lihatDokumenPR;
        function lihatDokumenPR(dataPR) {
            var data = {
                data: dataPR
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/persetujuan/viewDokumenPRModal.html',
                controller: 'viewDokumenPRModalController',
                controllerAs: 'viewDokumenPRModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        vm.LihatPRHistory = LihatPRHistory;
        function LihatPRHistory(dataPR) {
            var data = {
                data: dataPR
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/persetujuan/viewPRHistoryModal.html',
                controller: 'viewPRHistoryModalController',
                controllerAs: 'viewPRHistoryModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

    }
})();