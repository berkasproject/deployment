﻿(function () {
    'use strict';

    angular.module("app").controller("viewPRHistoryModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', '$stateParams', 'PersetujuanPRService', 'GlobalConstantService'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, $stateParams, PersetujuanPRService, GlobalConstantService) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.data = item.data;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.currentPage = 1;
        vm.totalItems = 0;
        console.log(item)
        vm.listCommodity = [];
        vm.dataHistoryPR = [];
        //vm.PRID = item.data.PRID;

        function init() {
            jLoad(1)
        }

        function jLoad(current) {
            UIControlService.loadLoadingModal();
            vm.currentPage = current;
            var offset = (current * 10) - 10;

            PersetujuanPRService.getDataHistoryPR({
                IntParam1: vm.data.PRID,
                Offset: offset,
                Limit: vm.maxSize
            }, function (reply) {
                if (reply.status == 200) {
                    vm.dataHistoryPR = reply.data.List;
                    vm.totalItems = reply.data.Count;
                } else {
                    UIControlService.msg_growl('error', "ERROR_API");

                }
                UIControlService.unloadLoadingModal();

            }, function (err) {
                UIControlService.msg_growl('error', "ERROR_API");
                UIControlService.unloadLoadingModal();
            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

    }
})();