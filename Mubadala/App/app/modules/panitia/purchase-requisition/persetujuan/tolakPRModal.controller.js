﻿(function () {
    'use strict';

    angular.module("app").controller("tolakPRModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModalInstance', '$stateParams', 'PersetujuanPRService', 'GlobalConstantService','$uibModal'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModalInstance, $stateParams, PersetujuanPRService, GlobalConstantService,$uibModal) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.data = item.data;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.currentPage = 1;
        vm.totalItems = 0;
        console.log(item)
        //vm.PRID = item.data.PRID;

        function init() {
            //for (var i = 0; i < vm.data.length; i++) {
            //    vm.data[i].Notes = "";
            //}
        }

        vm.detailPRLine = detailPRLine;
        function detailPRLine(param) {
            var data = {
                data: param
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/persetujuan/detailPRLineModal.html',
                controller: 'detailPRLineModalController',
                controllerAs: 'detailPRLineModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
               
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

        vm.simpan = simpan;
        function simpan() {
            var bolehNotes = true;

            for (var i = 0; i < vm.data.length; i++) {
                if (vm.data[i].Notes == "") {
                    bolehNotes = false;
                }
            }

            if (!bolehNotes) {
                UIControlService.msg_growl('warning', 'MESSAGE.HARAP_ISI_SEMUA_CATATAN');
                return;
            }

            UIControlService.loadLoadingModal();

            PersetujuanPRService.updateTolak({
                dataPR: vm.data
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    $uibModalInstance.close();

                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            })

        }

    }
})();