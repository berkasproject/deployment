﻿(function () {
    'use strict';

    angular.module("app").controller("syncPurchaseRequisitionCtrl", ctrl);

    ctrl.$inject = ['$http', '$state', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'PurchaseRequisitionService'];

    function ctrl($http, $state, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, PurchaseRequisitionService) {
        var vm = this;

        vm.totalPRsebenarnya = 0;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;

        vm.init = init;
        function init() {
            vm.selectBy = "5";
            vm.keyword = "4557";
            vm.PR = [];
            vm.PRSelected = [];
            $translatePartialLoader.addPart('synchronize-purchase-requisition');
            vm.jLoad(1);
        }

        vm.setSelectByValue = setSelectByValue;
        function setSelectByValue() {
            vm.keyword = "";
            //vm.status = "";
            vm.jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.currentPage = current;
            PurchaseRequisitionService.getDataPR({
                Keyword: vm.selectBy,
                Offset: (vm.currentPage - 1) * vm.pageSize,
                Limit: vm.pageSize,
                Keyword2: vm.keyword
            }, function (reply) {
                vm.PR = reply.data.List;
                //vm.plans = reply.data.List;
                vm.totalItems = reply.data.Count;
                for (var i = 0; i < vm.PR.length; i++) {
                    // $('#check'+vm.PR[i].pr_id).prop('checked', false);
                    if (i == 0) {
                        var temp = {
                            'PRID': vm.PR[i].PRID,
                            'PRLID': vm.PR[i].PRLID,
                            'PRCode': vm.PR[i].PRCode,
                            'PRName': vm.PR[i].PRName,
                            'Name': vm.PR[i].Name,
                            'MaterialCode': vm.PR[i].MaterialCode,
                            'parent_pr_line': true,
                            'Quantity': vm.PR[i].Quantity,
                            'OrderUnitName': vm.PR[i].OrderUnitName,
                            'UnitCost': vm.PR[i].UnitCost,
                            'DepartmentName': vm.PR[i].DepartmentName,
                            'EntityName': vm.PR[i].EntityName,
                            'EmployeeName': vm.PR[i].EmployeeName,
                            'LastApprovedDate': vm.PR[i].LastApprovedDate,
                            'PreparePRName': vm.PR[i].PreparePRName,
                            'LocationName': vm.PR[i].LocationName,
                            'CommodityID': vm.PR[i].CommodityID,
                            'PRStatus': vm.PR[i].PRStatus,
                            'Notes': ""
                        }

                        vm.PR.unshift(temp);
                    }
                    else {
                        if (vm.PR[i].PRID != vm.PR[(i - 1)].PRID) {
                            var temp = {
                                'PRID': vm.PR[i].PRID,
                                'PRLID': vm.PR[i].PRLID,
                                'PRCode': vm.PR[i].PRCode,
                                'PRName': vm.PR[i].PRName,
                                'Name': vm.PR[i].Name,
                                'MaterialCode': vm.PR[i].MaterialCode,
                                'parent_pr_line': true,
                                'Quantity': vm.PR[i].Quantity,
                                'OrderUnitName': vm.PR[i].OrderUnitName,
                                'UnitCost': vm.PR[i].UnitCost,
                                'DepartmentName': vm.PR[i].DepartmentName,
                                'EntityName': vm.PR[i].EntityName,
                                'EmployeeName': vm.PR[i].EmployeeName,
                                'LastApprovedDate': vm.PR[i].LastApprovedDate,
                                'PreparePRName': vm.PR[i].PreparePRName,
                                'LocationName': vm.PR[i].LocationName,
                                'CommodityID': vm.PR[i].CommodityID,
                                'PRStatus': vm.PR[i].PRStatus,
                                'Notes': ""
                            }

                            vm.PR.splice(i, 0, temp);
                        }
                    }
                }

                //Checkbox for pagination
                for (var i = 0; i < vm.PRSelected.length; i++) {
                    var index = vm.PR.findIndex(x => x.PRID === vm.PRSelected[i]);
                    //console.log(index);
                    if (index != -1) {
                        vm.PR[index].parentSelected = true;
                    }
                }

                //console.log(vm.PR)
                UIControlService.unloadLoading();
            }, function (err) {
                //$.growl.error({ message: "Gagal mendapatkan data plant" });
                //$rootScope.unloadLoading();
            });
        }

        vm.CheckParent = CheckParent;
        function CheckParent(dataPR) {
            for (var i = 0; i < vm.PR.length; i++) {
                if (!vm.PR[i].parent_pr_line && vm.PR[i].PRID == dataPR.PRID && vm.PR[i].PRLStatusName == "PRL_STATUS_COMPLETE") {
                    if (dataPR.parentSelected) {
                        vm.PR[i].selected = true;
                        var temp2 = {
                            CommodityID: vm.PR[i].CommodityID,
                            CostCenterID: vm.PR[i].CostCenterID,
                            CostCenterName: vm.PR[i].CostCenterName,
                            CreatedBy: vm.PR[i].CreatedBy,
                            CreatedDate: vm.PR[i].CreatedDate,
                            CurrencyID: vm.PR[i].CurrencyID,
                            CurrencyName: vm.PR[i].CurrencyName,
                            DepartmentID: vm.PR[i].DepartmentID,
                            DepartmentName: vm.PR[i].DepartmentName,
                            Detail: vm.PR[i].Detail,
                            DocumentName: vm.PR[i].DocumentName,
                            DocumentURL: vm.PR[i].DocumentURL,
                            EmployeeName: vm.PR[i].EmployeeName,
                            EntityID: vm.PR[i].EntityID,
                            EntityName: vm.PR[i].EntityName,
                            IsActive: vm.PR[i].IsActive,
                            ItemID: vm.PR[i].ItemID,
                            ItemName: vm.PR[i].ItemName,
                            LabelCurr: vm.PR[i].LabelCurr,
                            LastApprovedDate: vm.PR[i].LastApprovedDate,
                            LineNumber: vm.PR[i].LineNumber,
                            LocationID: vm.PR[i].LocationID,
                            LocationName: vm.PR[i].LocationName,
                            MaterialCode: vm.PR[i].MaterialCode,
                            ModifiedBy: vm.PR[i].ModifiedBy,
                            ModifiedDate: vm.PR[i].ModifiedDate,
                            Name: vm.PR[i].Name,
                            Notes: vm.PR[i].Notes,
                            OrderUnit: vm.PR[i].OrderUnit,
                            OrderUnitName: vm.PR[i].OrderUnitName,
                            PRCode: vm.PR[i].PRCode,
                            PRID: vm.PR[i].PRID,
                            PRLID: vm.PR[i].PRLID,
                            PRLStatusName: vm.PR[i].PRLStatusName,
                            PRName: vm.PR[i].PRName,
                            PRStatus: vm.PR[i].PRStatus,
                            PRStatusName: vm.PR[i].PRStatusName,
                            PreparePR: vm.PR[i].PreparePR,
                            PreparePRName: vm.PR[i].PreparePRName,
                            PurchaseRequisitionDocumentName: vm.PR[i].PurchaseRequisitionDocumentName,
                            PurchaseRequisitionDocumentURL: vm.PR[i].PurchaseRequisitionDocumentURL,
                            Quantity: vm.PR[i].Quantity,
                            Status: vm.PR[i].Status,
                            UOMName: vm.PR[i].UOMName,
                            UnitCost: vm.PR[i].UnitCost,
                            dataPR: vm.PR[i].dataPR,
                            selected: true
                        }
                        vm.PRSelected.push(temp2);
                    } else {
                        vm.PR[i].selected = false;
                        for (var j = 0; j < vm.PRSelected.length; j++) {
                            if (vm.PRSelected[j].PRLID == vm.PR[i].PRLID) {
                                vm.PRSelected.splice(j, 1);
                                break;
                            }
                        }
                    }
                }
            }
        }

        vm.CheckChild = CheckChild;
        function CheckChild(prl) {
            var count = 0;
            var spliced = false;
            for (var i = 0; i < vm.PRSelected.length; i++) {
                if (vm.PRSelected[i].PRLID == prl.PRLID) {
                    vm.PRSelected.splice(i, 1);
                    count++;
                    spliced = true;
                    break;
                }
            }
            if (count == 0) {
                vm.PRSelected.push(prl);
            }

            var countPR = $.grep(vm.PR, function (n) { return n.PRID == prl.PRID; });
            var countPRSelected = $.grep(vm.PRSelected, function (n) { return n.PRID == prl.PRID; });;

            if ((countPR.length - 1) == countPRSelected.length) {
                vm.PR.forEach(function (item) {
                    if (item.parent_pr_line && item.PRID == prl.PRID) {
                        item.parentSelected = true;
                    }
                });
            } else if ((countPR.length - 1) != countPRSelected.length && spliced) {
                vm.PR.forEach(function (item) {
                    if (item.parent_pr_line && item.PRID == prl.PRID) {
                        item.parentSelected = false;
                    }
                });
            }
        }

        vm.batalkanPR = batalkanPR;
        function batalkanPR() {
            if (vm.PRSelected.length == 0) {
                UIControlService.msg_growl('warning', 'DATA_PR_BELUM_DIPILIH');
                return;
            }
            var data = {
                data: vm.PRSelected,
                modalType: 1
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/synchronize/batalkanPROrKembalikanReviewModal.html',
                controller: 'batalkanPROrKembalikanReviewModalCtrl',
                controllerAs: 'batalkanPROrKembalikanReviewModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.kembaliKeReview = kembaliKeReview;
        function kembaliKeReview() {
            if (vm.PRSelected.length == 0) {
                UIControlService.msg_growl('warning', 'DATA_PR_BELUM_DIPILIH');
                return;
            }
            var data = {
                data: vm.PRSelected,
                modalType: 2
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/synchronize/batalkanPROrKembalikanReviewModal.html',
                controller: 'batalkanPROrKembalikanReviewModalCtrl',
                controllerAs: 'batalkanPROrKembalikanReviewModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.ajukanPR = ajukanPR;
        function ajukanPR() {
            if (vm.PRSelected.length == 0) {
                UIControlService.msg_growl('warning', 'DATA_PR_BELUM_DIPILIH');
                return;
            }

            var isMultiPR = [...new Set(vm.PRSelected.map(a => a.PRID))];
            if (isMultiPR.length > 1) {
                var isDiffLocation = [...new Set(vm.PRSelected.map(a => a.LocationID))];
                var isDiffCommodity = [...new Set(vm.PRSelected.map(a => a.CommodityID))];
                var isDiffCostCenter = [...new Set(vm.PRSelected.map(a => a.CostCenterID))];
                var isDiffEntity = [...new Set(vm.PRSelected.map(a => a.EntityID))];
                var isDiffDepartment = [...new Set(vm.PRSelected.map(a => a.DepartmentID))];
                var isDiffBuyer = [...new Set(vm.PRSelected.map(a => a.EmployeeID))];
                var isDiffRequestor = [...new Set(vm.PRSelected.map(a => a.PreparePR))];
                var isDiffCurrency = [...new Set(vm.PRSelected.map(a => a.CurrencyID))];
                var isDiffMaterialCode = [...new Set(vm.PRSelected.map(a => a.MaterialCode))];

                var dataDistinctMaterialCode = [];
                isDiffMaterialCode.forEach(function(item){
                    var temp = {
                        "MaterialCode": item
                    }
                    dataDistinctMaterialCode.push(temp);
                });
                var isDiffMaterialCode2 = $.grep(dataDistinctMaterialCode, function (n) { return n.MaterialCode == null});

                var countMaterialCodeNullfromPR = $.grep(vm.PRSelected, function (n) { return n.MaterialCode == null});


                if (isDiffLocation.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_LOC');
                    return;
                }

                if (isDiffCommodity.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_COMM');
                    return;
                }

                if (isDiffCostCenter.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_COSTCENTER');
                    return;
                }

                if (isDiffEntity.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_ENTITY');
                    return;
                }

                if (isDiffDepartment.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_DEPT');
                    return;
                }

                if (isDiffBuyer.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_BUYER');
                    return;
                }

                if (isDiffRequestor.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_REQUESTOR');
                    return;
                }

                if (isDiffMaterialCode.length > 1 && isDiffMaterialCode2.length > 0) {
                    UIControlService.msg_growl('warning', 'DIFF_MATERIALCODE');
                    return;
                }

                if (countMaterialCodeNullfromPR.length == vm.PRSelected.length) {
                    UIControlService.msg_growl('warning', 'ALLNULL_MATERIALCODE');
                    return;
                }

                if (isDiffCurrency.length > 1) {
                    UIControlService.msg_growl('warning', 'DIFF_CURRENCY');
                    return;
                }

            }

            var data = {
                data: vm.PRSelected,
                isMultiPR: isMultiPR.length
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/synchronize/ajukanPRModal.html',
                controller: 'ajukanPRModalCtrl',
                controllerAs: 'ajukanPRModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.bantuan = bantuan;
        function bantuan() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/synchronize/bantuanModal.html',
                controller: 'bantuanModalCtrl',
                controllerAs: 'bantuanModalCtrl'
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }
    }
})();


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}