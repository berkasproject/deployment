﻿(function () {
    'use strict';

    angular.module("app").controller("bantuanModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', '$uibModal', '$stateParams', 'PurchaseRequisitionService', 'GlobalConstantService', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, $uibModal, $stateParams, PurchaseRequisitionService, GlobalConstantService, $uibModalInstance) {

        var vm = this;

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();