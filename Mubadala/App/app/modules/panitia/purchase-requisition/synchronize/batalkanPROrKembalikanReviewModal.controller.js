﻿(function () {
    'use strict';

    angular.module("app").controller("batalkanPROrKembalikanReviewModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModal', '$stateParams', 'PurchaseRequisitionService', 'GlobalConstantService', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModal, $stateParams, PurchaseRequisitionService, GlobalConstantService, $uibModalInstance) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.data = item.data;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.currentPage = 1;
        vm.totalItems = 0;
        vm.listCommodity = [];
        vm.modalType = item.modalType;
        //vm.PRID = item.data.PRID;

        function init() {
            //getJenisKomoditi()
            dataPR();
        }

        function dataPR() {
            vm.datas = [];
            var index = 0;
            vm.data.forEach(function (item) {
                if (vm.datas.length == 0) {
                    vm.datas.push(item);
                }

                if (vm.datas.length > 0) {
                    if (item.PRID != vm.data[index].PRID) {
                        vm.datas.push(item);
                        index++;
                    }
                }
            });
        }




        function getJenisKomoditi() {
            PurchaseRequisitionService.getJenisKomoditi(function (reply) {
                if (reply.status == 200) {
                    vm.listCommodity = reply.data.List;
                } else {
                    UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');

                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            })
        }

        vm.detailPRLine = detailPRLine;
        function detailPRLine(PRID) {
            vm.dataPRL = $.grep(vm.data, function (n) { return n.PRID == PRID; });
            var data = {
                dataPRL: vm.dataPRL 
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/synchronize/detailPRLModal.html',
                controller: 'detailPRLModalCtrl',
                controllerAs: 'detailPRLModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

        vm.simpan = simpan;
        function simpan() {
            var bolehCommodity = true;
            var bolehNotes = true;

            if (vm.modalType == 1) {
                for (var i = 0; i < vm.datas.length; i++) {
                    if (vm.datas[i].Notes == "" || vm.datas[i].Notes == undefined) {
                        bolehNotes = false;
                    }
                }

                if (!bolehNotes) {
                    UIControlService.msg_growl('warning', 'CATATAN');
                    return;
                }
            }

            UIControlService.loadLoadingModal();

            vm.modalType == 1 ? batalkanPR() : kembalikanReview();

            }

        function batalkanPR(){
            PurchaseRequisitionService.batalkanPR({
                dataPR: vm.datas
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl('success', 'SUCCESS_CANCEL');
                    $uibModalInstance.close();

                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            })
        }

        function kembalikanReview(){
            PurchaseRequisitionService.kembalikanReview({
                dataPR: vm.datas
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl('success', 'SUCCESS_KEMBALIKANKEREVIEW');
                    $uibModalInstance.close();

                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            })
        }

    }
})();