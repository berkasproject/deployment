﻿(function () {
    'use strict';

    angular.module("app").controller("ajukanPRModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'item', '$uibModal', '$stateParams', 'PurchaseRequisitionService', 'GlobalConstantService', '$uibModalInstance'];

    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, item, $uibModal, $stateParams, PurchaseRequisitionService, GlobalConstantService, $uibModalInstance) {

        var vm = this;

        vm.init = init;
        vm.maxSize = 10;
        vm.data = item.data;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.currentPage = 1;
        vm.totalItems = 0;
        vm.isMultiPR = item.isMultiPR;
        //vm.PRID = item.data.PRID;

        function init() {
            dataPR();
        }

        function dataPR() {
            vm.datas = [];
            var index = 0;
            vm.data.forEach(function (item) {
                if (vm.datas.length == 0) {
                    vm.datas.push(item);
                }
                else if (vm.datas.length > 0) {
                    if (item.PRID != vm.datas[index].PRID) {
                        vm.datas.push(item);
                        index++;
                    }
                }
            });
            vm.countMaterialCodeNullfromPR = $.grep(vm.datas, function (n) { return n.MaterialCode == null });
            if (vm.isMultiPR == 1 && vm.countMaterialCodeNullfromPR.length == vm.datas.length) {
                getProcPlan();
            }
        }




        function getProcPlan() {
            PurchaseRequisitionService.getProcPlan(function (reply) {
                if (reply.status == 200) {
                    vm.listProcPlan = reply.data;
                } else {
                    UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            })
        }

        vm.detailPRLine = detailPRLine;
        function detailPRLine(PRID) {
            vm.dataPRL = $.grep(vm.data, function (n) { return n.PRID == PRID; });
            var data = {
                dataPRL: vm.dataPRL
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/purchase-requisition/synchronize/detailPRLModal.html',
                controller: 'detailPRLModalCtrl',
                controllerAs: 'detailPRLModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }

        vm.simpan = simpan;
        function simpan() {
            UIControlService.loadLoadingModal("Loading...");
            //vm.datas.forEach(function (item) {
            //    var itemPRL = "";
            //    vm.dataPRL = $.grep(vm.data, function (n) { return n.PRID == item.PRID });
            //    for (var i = 0; i < vm.dataPRL.length; i++) {
            //        if (vm.dataPRL.length == 1) {
            //            itemPRL = vm.dataPRL[i].Name;
            //        }
            //        if (i < vm.dataPRL.length - 1) {
            //            itemPRL += vm.dataPRL.length == 2 ? vm.dataPRL[i].Name + " " : vm.dataPRL[i].Name + ", ";
            //        }
            //        else if (i == vm.dataPRL.length - 1) {
            //            itemPRL += "& " + vm.dataPRL[i].Name;
            //        }
            //    }
            //    var catatan = "Item name " + itemPRL + " has been choosed for tender process";
            //    item.notes = catatan;

            //});
            //return;

            PurchaseRequisitionService.ajukanPR({
                dataPR: vm.data,
                isMultiPR: vm.isMultiPR,
                countMaterialCodeNullfromPR: vm.countMaterialCodeNullfromPR.length,
                ProcPlanID: vm.ProcPlanID,
                NamaPRGabungan: vm.prGabunganName
            }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl('success', 'SUCCESS_SUBMISSION');
                    $uibModalInstance.close();

                }
            }, function (err) {
                UIControlService.msg_growl('error', 'MESSAGE.ERROR_API');
                return;
            });

        }

    }
})();