(function () {
	'use strict';

	angular.module("app").controller("pengumumanPengadaanCtrl", ctrl);

	ctrl.$inject = ['GlobalConstantService', '$translatePartialLoader', 'DataPengadaanService', 'PengumumanPengadaanService', 'GoodOfferEntryService', 'PurchaseRequisitionService', '$state', 'UIControlService', '$uibModal', '$stateParams', '$filter'];

	function ctrl(GlobalConstantService, $translatePartialLoader, DataPengadaanService, PengumumanPengadaanService, GoodOfferEntryService, PurchReqService, $state, UIControlService, $uibModal, $stateParams, $filter) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.TypeTender = "";
		vm.jumlahlembar = 1;
		vm.filteredEmails = [];
		vm.dataTenderReal = {};
		vm.isApproved = true;
		vm.dataTender = {
			TenderCode: '',
			TenderName: '',
			IsVendorEmails: false,
			IsInternational: false,
			IsLocal: false,
			IsNational: false,
			IsOpen: false,
			Vendors: [],
			Emails: '',
			CompScale: null,
			CommodityID: null,
			Description: ''
		};
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.listPengumuman = [];
		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart("pengumuman-pengadaaan-tender");

			if (vm.ProcPackType == '4189') { // service tender only
				vm.TypeTender = "CR"
				DataPengadaanService.IsAllowedEdit({
					TenderRefID: vm.IDTender,
					ProcPackageType: vm.ProcPackType
				}, function (reply) {
					vm.isAllowedEdit = reply.data;
					DataPengadaanService.isNeedTenderStepApproval({ ID: vm.IDStepTender }, function (result) {
						//UIControlService.unloadLoading();
						vm.isNeedApproval = result.data;
						if (vm.isNeedApproval) vm.isApproved = false;
						if (!vm.isNeedApproval) {
							DataPengadaanService.isApprovalSent({ ID: vm.IDStepTender }, function (result2) {
								vm.isApprovalSent = result2.data;
								if (vm.isApprovalSent) {
									DataPengadaanService.isApproved({ ID: vm.IDStepTender }, function (result) {
										vm.isApproved = result.data;
									});
								}
							});
						}
						//console.info("jumlahData:" + vm.countRegister);
					}, function (err) {
						$.growl.error({ message: "Gagal mendapatkan data Approval" });
						//UIControlService.unloadLoading();
					});

				}, function (error) {
					UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
				});

				PengumumanPengadaanService.getTenderRegAdmin({
					ID: vm.IDStepTender
				}, function (reply) {
					//UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.dataTender = reply.data;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					//UIControlService.unloadLoading();
				});

				loadDataPengumuman();
				loadOvertime();
			} else if (vm.ProcPackType == '4190') {
				PengumumanPengadaanService.getTenderRegAdmin({
					ID: vm.IDStepTender
				}, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						vm.dataTender = reply.data;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});

				loadGoodsOfferEntry(1);
				loadDokumenBarang();
			}
		}

		vm.printExport = printExport
		function printExport() {
			$state.transitionTo('pengumuman-pengadaan-print', {
				TenderRefID: vm.IDTender,
				StepID: vm.IDStepTender,
				ProcPackType: vm.ProcPackType
			});
		}

		vm.loadGoodsOfferEntry = loadGoodsOfferEntry;
		function loadGoodsOfferEntry(current) {
			vm.currentPage = current;
			var offset = (vm.currentPage * 10) - 10;
			var tender = {
				Status: vm.IDTender,
				FilterType: vm.ProcPackType,
				Offset: offset,
				Limit: 10,
				column: vm.StepID
			}
			PengumumanPengadaanService.getByAdmin(tender, function (reply) {
				vm.totalItems = reply.data.Count;
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.goods = reply.data.List;
					for (var i = 0; i < vm.goods.length; i++) {
						if (i == vm.goods.length - 1) {
							vm.offer = vm.goods[i].goods.OfferTotalCost;
							vm.Date = vm.goods[i].goods.CurrencyDate;
						}
					}
					if (vm.goods[0].ID !== 0) {
						loadOtherDoc(vm.goods[0].GoodsOEId);
						vm.QLTime = vm.goods[0].goods.SupplierQLTime;
						loadFreightCost2(vm.goods[0].goods.FreighCostId);
					} else {
						loadFreightCost2();
					}

					loadCurrencies();
					loadDeliveryTerms();
					loadFreightCost();
					loadRFQItem();
					loadRFQGoods();
					loadDeliveryTender();
					loadIncoTerms();
					loadDeliveryPoint();
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Penawaran Barang" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadDokumenBarang = loadDokumenBarang;
		function loadDokumenBarang() {
			PurchReqService.selectDoc({ ID: vm.IDTender }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					console.info("docGoods" + JSON.stringify(reply));
					vm.listDoc = reply.data;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.selectedFreightCost;
		vm.listFreightCost = [];
		function loadFreightCost() {
			GoodOfferEntryService.getTypeFreightCost(function (reply) {
				UIControlService.unloadLoading();
				vm.listFreightCost = reply.data.List;
				for (var i = 0; i < vm.listFreightCost.length; i++) {
					if (vm.goods[0].goods.FreightCostType === vm.listFreightCost[i].RefID) {
						vm.selectedFreightCost = vm.listFreightCost[i];
						changeFreight(vm.selectedFreightCost.RefID);
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadRFQItem = loadRFQItem;
		function loadRFQItem() {
			var status = {
				Status: vm.IDTender
			}
			GoodOfferEntryService.GetRFQ(status, function (reply) {
				UIControlService.unloadLoading();
				vm.RfqID = reply.data;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedIncoTerms;
		vm.loadIncoTerms = loadIncoTerms
		function loadIncoTerms() {
			PengumumanPengadaanService.getIncoTerms({ ID: vm.goods[0].goods.IncoId }, function (reply) {
				UIControlService.unloadLoading();
				vm.IncoTermsName = reply.data;
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.loadDeliveryPoint = loadDeliveryPoint
		function loadDeliveryPoint() {
			PengumumanPengadaanService.getDeliveryPoint({ FreightCostID: vm.goods[0].goods.FreighCostId }, function (reply) {
				UIControlService.unloadLoading();
				vm.DeliveryStateName = reply.data;
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + i), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download('Tender Announcement - '+vm.dataTender.TenderName + ".pdf");
					}
				});
				indeks = "";
			}
		}

		vm.selectedDeliveryTender;
		vm.loadDeliveryTender = loadDeliveryTender;
		function loadDeliveryTender() {
			GoodOfferEntryService.getDeliveryTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listDeliveryTender = reply.data.List;
				for (var i = 0; i < vm.listDeliveryTender.length; i++) {
					if (vm.goods[0].goods.DeliveryTerms === vm.listDeliveryTender[i].RefID) {
						vm.selectedDeliveryTender = vm.listDeliveryTender[i];
						break;
					}
				}
			}, function (err) {
				// UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadReference = loadReference;
		function loadReference(data) {
			GoodOfferEntryService.DeliveryTerm({}, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.DeliveryTerm = reply.data.List;
					for (var i = 0; i < vm.DeliveryTerm.length; i++) {
						if (data == vm.DeliveryTerm[i].RefID) vm.selectedDeliveryTerms = vm.DeliveryTerm[i];
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data penawaran" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.selectedPaymentTerm;
		vm.listPaymentTerm = [];
		function loadPaymentTerm(data) {
			GoodOfferEntryService.getPaymentTerm(function (reply) {
				UIControlService.unloadLoading();
				vm.listPaymentTerm = reply.data;
				for (var i = 0; i < vm.listPaymentTerm.length; i++) {
					if (data === vm.listPaymentTerm[i].Id) {
						vm.selectedPaymentTerm = vm.listPaymentTerm[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedTypeTender;
		vm.loadTypeTender = loadTypeTender;
		function loadTypeTender() {
			GoodOfferEntryService.getTypeTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listTypeTender = reply.data.List;
				for (var i = 0; i < vm.listTypeTender.length; i++) {
					if (vm.RfqGoods[0].TenderType === vm.listTypeTender[i].RefID) {
						//console.info(vm.listTypeTender[i]);
						vm.selectedTypeTender = vm.listTypeTender[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.selectedOptionsTender;
		vm.loadOptionsTender = loadOptionsTender;
		function loadOptionsTender() {
			GoodOfferEntryService.getOptionsTender(function (reply) {
				UIControlService.unloadLoading();
				vm.listOptionsTender = reply.data.List;
				for (var i = 0; i < vm.listOptionsTender.length; i++) {
					if (vm.RfqGoods[0].TenderOption === vm.listOptionsTender[i].RefID) {
						vm.selectedOptionsTender = vm.listOptionsTender[i];
						break;
					}
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		//vm.selectedTechnical;
		//vm.listTechnical = [];
		//function loadTechnical() {
		//	PengumumanPengadaanService.getTechnical(function (reply) {
		//		UIControlService.unloadLoading();
		//		vm.listTechnical = reply.data.List;
		//		if ((vm.TechnicalID > 0 || vm.TechnicalID != null)) {
		//			for (var i = 0; i < vm.listTechnical.length; i++) {
		//				if (vm.TechnicalID === vm.listTechnical[i].RefID) {
		//					vm.selectedTechnical = vm.listTechnical[i]
		//					break;
		//				}
		//			}
		//		}
		//	}, function (err) {
		//		UIControlService.msg_growl("error", "MESSAGE.API");
		//		UIControlService.unloadLoading();
		//	});
		//}

		vm.selectedFreightCost2;
		vm.listFreightCost2 = [];
		function loadFreightCost2(data) {
			GoodOfferEntryService.GetFreightCost(function (reply) {
				UIControlService.unloadLoading();
				vm.listFreightCost2 = reply.data;
				if (data !== undefined) {
					for (var i = 0; i < vm.listFreightCost2.length; i++) {
						if (vm.listFreightCost2[i].FreightCostID == data) {
							vm.selectedFreightCost2 = vm.listFreightCost2[i];
						}
					}
				}

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadCurrencies = loadCurrencies;
		vm.selectedCurrencies;
		vm.listCurrencies = [];
		var CurrencyID;
		function loadCurrencies() {
			GoodOfferEntryService.getCurrencies(
            function (response) {
            	if (response.status === 200) {
            		vm.listCurrencies = response.data;
            		for (var i = 0; i < vm.listCurrencies.length; i++) {
            			if (vm.goods[0].goods.RateID === vm.listCurrencies[i].CurrencyID) {
            				vm.selectedCurrencies = vm.listCurrencies[i];
            				changeCurrent();
            				break;
            			}
            		}
            	}
            	else {
            		UIControlService.msg_growl("error", "Gagal mendapatkan list Currency");
            		return;
            	}
            }, function (err) {
            	UIControlService.msg_growl("error", "Gagal Akses API");
            	return;
            });
		}

		vm.selectedDeliveryTerms;
		vm.listDeliveryTerms = [];
		function loadDeliveryTerms() {
			PurchReqService.getDeliveryTerms(function (reply) {
				UIControlService.unloadLoading();
				vm.listDeliveryTerms = reply.data.List;
				for (var i = 0; i < vm.listDeliveryTerms.length; i++) {
					if (vm.goods[0].goods.DeliveryTerms === vm.listDeliveryTerms[i].RefID) {
						vm.selectedDeliveryTerms = vm.listDeliveryTerms[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadRFQGoods = loadRFQGoods;
		function loadRFQGoods() {
			var status = {
				Status: vm.IDTender
			}
			GoodOfferEntryService.GetRFQGoods(status, function (reply) {
				UIControlService.unloadLoading();
				vm.RfqGoods = reply.data;
				vm.TenderType = reply.data[0].TenderType;
				loadReference(reply.data[0].DeliveryTerms);
				loadPaymentTerm(reply.data[0].PaymentTerm);
				loadTypeTender();
				loadOptionsTender();
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.sendToApproval = sendToApproval;
		function sendToApproval() {
			bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SENDAPPROVAL'), function (res) {
				if (res) {
					UIControlService.loadLoading('MESSAGE.LOADING');
					DataPengadaanService.sendToApproval({ ID: vm.IDStepTender }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND', "MESSAGE.SUCC_SENDTITLE");
							init();
							//UIControlService.msg_growl('notice', $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
						} else {
							$.growl.error({ message: "Send Approval Failed." });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.detailApproval = detailApproval;
		function detailApproval() {
			var item = vm.IDStepTender;

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pengumuman-pengadaan/detailApproval.html',
				controller: 'tenderAnnounceCtrl',
				controllerAs: 'tenderAnnounceCtrl',
				resolve: { item: function () { vm.IDStepTender = item; return item; } }
			});

			modalInstance.result.then(function () {
				init();
			});
		}

		vm.loadOvertime = loadOvertime;
		function loadOvertime() {
			PengumumanPengadaanService.isOvertime({ ID: vm.IDStepTender }, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.overtime = reply.data;
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data Chatting" });
					//UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				//UIControlService.unloadLoading();
			});
		}

		vm.SendAnnouncementApproval = SendAnnouncementApproval;
		function SendAnnouncementApproval(data) {
		    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_SEND_ANNOUNCEMENT_APPROVAL'), function (res) {
		        if (res) {
		            UIControlService.loadLoading("");
		            PengumumanPengadaanService.sendAnnouncementApproval({
		                ID: data.TenderAnnouncement.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", 'MESSAGE.SUCC_SEND');
		                init();
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.ERR_SEND");
		                UIControlService.unloadLoading();
		            });
		        }
		    });
		}

		vm.detailAnnouncementApproval = detailAnnouncementApproval;
		function detailAnnouncementApproval(data) {
		    var item = {
		        ID : data.TenderAnnouncement.ID
		    };

		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/pengumuman-pengadaan/detailAnnouncementApproval.html',
		        controller: 'detailAnnouncementApprovalController',
		        controllerAs: 'detAnnCtrl',
		        resolve: { item: function () { vm.IDStepTender = item; return item; } }
		    });

		    modalInstance.result.then(function () {
		        init();
		    });
		}

        /*
		vm.Publish = Publish;
		function Publish(data) {
		    //console.info("s");
		    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_PUBLISH_ANNOUNCEMENT'), function (res) {
		        if (res) {
		            UIControlService.loadLoading("");
		            PengumumanPengadaanService.Publish({
		                Status: data.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.status === 200) {
		                    init();
		                }
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.API");
		                UIControlService.unloadLoading();
		            });
		        }
		    });
		}
        */

		function loadDataTender() {
			PengumumanPengadaanService.getDataTender({
				ProcPackageType: vm.ProcPackType, TenderRefID: vm.IDTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.dataTenderReal = data;
					vm.TenderID = vm.dataTenderReal.TenderID;

					PengumumanPengadaanService.isNeedAnnouncement({
					    TenderRefID: vm.IDTender,
					}, function (reply) {
					    if (reply.data === false) {
					        UIControlService.msg_growl("warning", "MESSAGE.ANNOUNCEMENT_NOT_NEED");
					        backDetailTahapan();
					    }
					}, function (err) {
					    UIControlService.msg_growl("error", "MESSAGE.API");
					});

				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function loadDataPengumuman() {
			UIControlService.loadLoading("");
			PengumumanPengadaanService.selectPengumuman({
				StepID: vm.IDStepTender, tender: { TenderRefID: vm.IDTender, ProcPackageType: vm.ProcPackType }
			}, function (reply) {
				//console.info("*added: "+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					if (data.length > 0 && (data[0].TenderAnnouncement.ID != 0)) {
						vm.listPengumuman = data;
						vm.TenderID = data[0].TenderAnnouncement.TenderStepData.TenderID;
						loadKomoditi();
						//loadTechnical();
					} else {
						loadDataTender();
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.openForm = openForm;
		function openForm(isAdd, data) {
			//set data jika tender RFQ GOODS
			if (vm.ProcPackType === 4190) {
				vm.dataTender.TenderCode = vm.dataTenderReal.RFQCode;
				vm.dataTender.TenderName = vm.dataTenderReal.RFQName;
				vm.dataTender.IsLocal = vm.dataTenderReal.IsLocal;
				vm.dataTender.IsNational = vm.dataTenderReal.IsNational;
				vm.dataTender.IsInternational = vm.dataTenderReal.IsInternational;
				vm.dataTender.CommodityID = vm.dataTenderReal.CommodityID;
				vm.dataTender.CompScale = vm.dataTenderReal.CompScale;
				vm.dataTender.IsVendorEmails = vm.dataTenderReal.IsVendorEmails;
				vm.dataTender.Emails = vm.dataTenderReal.Emails;
				vm.dataTender.filteredEmails = vm.dataTenderReal.FilteredEmails;
				vm.dataTender.Vendors = vm.dataTenderReal.Vendors;
				vm.TypeTender = "RFQGOODS";
			} else if (vm.ProcPackType === 3168) {
				vm.dataTender.TenderCode = vm.dataTenderReal.RFQCode;
				vm.dataTender.TenderName = vm.dataTenderReal.RFQName;
				vm.dataTender.IsLocal = vm.dataTenderReal.IsLocal;
				vm.dataTender.IsNational = vm.dataTenderReal.IsNational;
				vm.dataTender.IsInternational = vm.dataTenderReal.IsInternational;
				vm.dataTender.CommodityID = vm.dataTenderReal.CommodityID;
				vm.dataTender.CompScale = vm.dataTenderReal.CompScale;
				vm.dataTender.IsVendorEmails = vm.dataTenderReal.IsVendorEmails;
				vm.dataTender.Emails = vm.dataTenderReal.Emails;
				vm.dataTender.filteredEmails = vm.dataTenderReal.FilteredEmails;
				vm.dataTender.Vendors = vm.dataTenderReal.Vendors;
				vm.TypeTender = "RFQVHS";
			}
				//set data jika tender CR
			else if (vm.ProcPackType === 4189) {
				vm.dataTender.TenderCode = vm.dataTenderReal.TenderCode;
				vm.dataTender.TenderName = vm.dataTenderReal.ProjectTitle;
				vm.TypeTender = "CR";
			}
			var senddata = {
				isAdd: isAdd,
				IDRefTender: vm.IDTender,
				IDStepTender: vm.IDStepTender,
				IDProcPackType: vm.ProcPackType,
				TypeTender: vm.TypeTender,
				DataTender: vm.dataTender,
				flag: 0
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/pengumuman-pengadaan/formPengumumanPengadaan.html?v=1.000002',
				controller: 'formPPController',
				controllerAs: 'formPPCtrl',
				resolve: {
					item: function () {
						return senddata;
					}
				}
			});
			modalInstance.result.then(function () {
				loadDataPengumuman();
			});
		}

		vm.editForm = editForm;
		function editForm(isAdd, data) {
			vm.detvendorcomm = [];
			vm.dataTender.Vendors = [];
			if (vm.ProcPackType === 4190) { vm.TypeTender = "RFQGOODS"; }
			if (vm.ProcPackType === 4189) { vm.TypeTender = "CR" }
			//console.info("edit:" + data.TenderAnnouncement.Description);
			vm.dataTender.TenderCode = data.TenderAnnouncement.TenderStepData.tender.TenderCode;
			vm.dataTender.TenderName = data.TenderAnnouncement.TenderStepData.tender.TenderName;
			vm.dataTender.IsLocal = data.TenderAnnouncement.IsLokal;
			vm.dataTender.IsNational = data.TenderAnnouncement.IsNational;
			vm.dataTender.IsInternational = data.TenderAnnouncement.IsInternational;
			vm.dataTender.CommodityID = data.TenderAnnouncement.CommodityID;
			vm.dataTender.TechnicalID = data.TenderAnnouncement.TechnicalID;
			vm.dataTender.CompScale = data.TenderAnnouncement.CompanyScaleID;
			vm.dataTender.IsVendorEmails = data.TenderAnnouncement.IsVendorEmail;
			vm.dataTender.Emails = data.TenderAnnouncement.Emails;
			vm.dataTender.Description = data.TenderAnnouncement.Description;
			vm.dataTender.DocUrl = data.TenderAnnouncement.DocUrl;
			vm.dataTender.IsOpen = data.TenderAnnouncement.IsOpen;
		    //console.info(JSON.stringify(;.DetailVendor));
			var detvendor = data.DetailVendor;
			vm.detvendorcomm = data.DetailVendorComm;
			if (detvendor !== null) {
				for (var i = 0; i < detvendor.length ; i++) {
					var data = {
						VendorID: detvendor[i].VendorID,
						Code: detvendor[i].Code,
						VendorName: detvendor[i].Name,
						Email: detvendor[i].Email
					}
					vm.dataTender.Vendors.push(data);
				}
			}
			else vm.dataTender.Vendors = [];
			var senddata = {
				isAdd: isAdd,
				IDRefTender: vm.IDTender,
				IDStepTender: vm.IDStepTender,
				IDProcPackType: vm.ProcPackType,
				TypeTender: vm.TypeTender,
				DataTender: vm.dataTender,
				DetailVendorComm: vm.detvendorcomm,
				flag: 0
			};
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/pengumuman-pengadaan/formPengumumanPengadaan.html?v=1.000002',
				controller: 'formPPController',
				controllerAs: 'formPPCtrl',
				resolve: {
					item: function () {
						return senddata;
					}
				}
			});
			modalInstance.result.then(function () {
				loadDataPengumuman();
			});
		}

		vm.selectedComodity;
		vm.listComodity = [];
		function loadKomoditi() {
			PengumumanPengadaanService.getCommodity({ type: vm.TypeTender }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listComodity = reply.data;
					if ((vm.listPengumuman[0].TenderAnnouncement.CommodityID > 0 || vm.listPengumuman[0].TenderAnnouncement.CommodityID != null)) {
						console.info(vm.CommodityID + "...");
						for (var i = 0; i < vm.listComodity.length; i++) {
							if (vm.listPengumuman[0].TenderAnnouncement.CommodityID === vm.listComodity[i].ID) {
								vm.selectedComodity = vm.listComodity[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.detailData = detailData;
		function detailData(isAdd, data) {
			vm.dataTender.Vendors = [];
			if (vm.ProcPackType === 4190) { vm.TypeTender = "RFQGOODS"; }
			if (vm.ProcPackType === 4189) { vm.TypeTender = "CR" }
			//console.info("edit:" + data.TenderAnnouncement.Description);
			vm.dataTender.TenderCode = data.TenderAnnouncement.TenderStepData.tender.TenderCode;
			vm.dataTender.TenderName = data.TenderAnnouncement.TenderStepData.tender.TenderName;
			vm.dataTender.IsLocal = data.TenderAnnouncement.IsLokal;
			vm.dataTender.IsNational = data.TenderAnnouncement.IsNational;
			vm.dataTender.IsInternational = data.TenderAnnouncement.IsInternational;
			vm.dataTender.CommodityID = data.TenderAnnouncement.CommodityID;
			vm.dataTender.TechnicalID = data.TenderAnnouncement.TechnicalID;
			vm.dataTender.CompScale = data.TenderAnnouncement.CompanyScaleID;
			vm.dataTender.IsVendorEmails = data.TenderAnnouncement.IsVendorEmail;
			vm.dataTender.Emails = data.TenderAnnouncement.Emails;
			vm.dataTender.Description = data.TenderAnnouncement.Description;
			console.info(data);
			var detvendor = data.DetailVendor;
			var detvendorcomm = data.DetailVendorComm;

			//PengumumanPengadaanService.viewVendor({
			//    CommodityID: data.TenderAnnouncement.CommodityID,
			//    IsLokal: data.TenderAnnouncement.IsLokal,
			//    IsNational: data.TenderAnnouncement.IsNational,
			//    IsInternational: data.TenderAnnouncement.IsInternational,
			//    CompanyScaleID: data.TenderAnnouncement.CompanyScaleID,
			//    TechnicalID: data.TenderAnnouncement.TechnicalID
			//}, function (reply) {
			//    if (reply.status === 200) {
			//        vm.vendors = reply.data;
			//        for (var i = 0; i < vm.vendors.length; i++) {
			//            var data = {
			//                VendorID: vm.vendors[i].VendorID,
			//                Code: vm.vendors[i].Code,
			//                VendorName: vm.vendors[i].Name,
			//                Email: vm.vendors[i].Email
			//            }
			//            vm.dataTender.Vendors.push(data);
			//        }
			//    } 

			//}, function (err) {
			//    UIControlService.unloadLoadingModal();
			//});









			if (detvendor != null) {
				for (var i = 0; i < detvendor.length ; i++) {
					var data = {
						VendorID: detvendor[i].VendorID,
						Code: detvendor[i].Code,
						VendorName: detvendor[i].Name,
						Email: detvendor[i].Email
					}
					vm.dataTender.Vendors.push(data);
				}

			}
			if (detvendorcomm != null) {
				for (var i = 0; i < detvendorcomm.length ; i++) {
					var data = {
						VendorID: detvendorcomm[i].VendorID,
						Code: detvendorcomm[i].Code,
						VendorName: detvendorcomm[i].Name,
						Email: detvendorcomm[i].Email
					}
					vm.dataTender.Vendors.push(data);
				}

			}
			var senddata = {
				isAdd: isAdd,
				IDRefTender: vm.IDTender,
				IDStepTender: vm.IDStepTender,
				IDProcPackType: vm.ProcPackType,
				TypeTender: vm.TypeTender,
				DataTender: vm.dataTender,
				flag: 1
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'detail-pengumuman-pengadaan.html',
				controller: 'formPPController',
				controllerAs: 'formPPCtrl',
				resolve: {
					item: function () {
						return senddata;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.dataTender.Vendors = [];
				loadDataPengumuman();
			});
		}

		vm.batal = batal;
		function batal() {
			//console.info("batal");
			$uibModalInstance.dismiss('cancel');
		};

		vm.backDetailTahapan = backDetailTahapan;
		function backDetailTahapan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, TenderID: vm.TenderID });
		}

		vm.backDetailTahapanPrint = backDetailTahapanPrint;
		function backDetailTahapanPrint() {
			$state.transitionTo('pengumuman-pengadaan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, StepID: vm.IDStepTender });
		}

		vm.PublishForm = PublishForm;
		function PublishForm(isAdd, data) {
			if (vm.overtime == true) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_PUBLISH_STEP_DATE");
				return;
			} else {
				bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_PUBLISH_ANNOUNCEMENT'), function (yes) {
				    if (yes) {

				        vm.dataPublish = data;
				        vm.Email = [];
				        vm.EmailVendor = [];

				        UIControlService.loadLoading("");
				        PengumumanPengadaanService.sendMail(vm.dataPublish, function (response) {
				            //UIControlService.unloadLoading();
				            //UIControlService.msg_growl("notice", "MESSAGE.EMAIL_SENT");
				            PengumumanPengadaanService.Publish({
				                Status: data.TenderAnnouncement.ID
				            }, function (reply) {
				                UIControlService.unloadLoading();
                                UIControlService.msg_growl("success", "MESSAGE.SUCC_PUBLISH");
				                init();
				            }, function (error) {
				                UIControlService.msg_growl("error", "MESSAGE.ERR_PUBLISH");
				                UIControlService.unloadLoading();
				            });
				        }, function (error) {
				            //UIControlService.handleRequestError(response.data);
				            UIControlService.unloadLoading();
				            UIControlService.msg_growl("error", "MESSAGE.ERR_PUBLISH");
				            //UIControlService.msg_growl("error", "MESSAGE.ERR_EMAIL_SENT");
				        });
				    }
				});
			}
		}

		vm.loadVendorEmail = loadVendorEmail;
		function loadVendorEmail() {
			PengumumanPengadaanService.getVendorEmail({
				ID: vm.IDTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data.List;
					vm.dataEmailAssosiasi = data;
					if (vm.dataEmailAssosiasi.length == 0) init();
					for (var i = 0; i < vm.dataEmailAssosiasi.length; i++) {
						vm.EmailAssosiasi.push(vm.dataEmailAssosiasi[i].Email);
						if (i == (vm.dataEmailAssosiasi.length - 1)) {
							sendEmailAssosiasi();
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
		vm.sendEmailAssosiasi = sendEmailAssosiasi;
		function sendEmailAssosiasi() {
			var email = {
				EmailContent: 'Pengumuman Tender Jasa',
				Id: vm.IDTender,
				isHtml: true,
				addresses: vm.EmailAssosiasi
			};

			UIControlService.loadLoading("MESSAGE.LOADING");
			PengumumanPengadaanService.sendMailAssosiasi(email, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					UIControlService.msg_growl("notice", "MESSAGE.EMAIL_ASSOCIATION");
					init();
					//GetApprovalGoods();
				} else {
					init();
					//UIControlService.handleRequestError(response.data);
				}
			}, function (response) {
				//UIControlService.handleRequestError(response.data);
				UIControlService.unloadLoading();
				init();
			});
		}

	}
})();