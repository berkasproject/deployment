﻿(function () {
	'use strict';

	angular.module("app").controller("DataVendorCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader','UIControlService', '$uibModalInstance', 'PengumumanPengadaanService','item'];

	function ctrl($translatePartialLoader,UIControlService, $uibModalInstance, PengumumanPengadaanService, items) {
		var vm = this;

		vm.vendors = null;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';
		vm.listCheckVendor = [];
		vm.listSaveVendor = [];
		vm.IsLocalOnly = items.IsLocal && !items.IsNational && !items.IsInternational;

		vm.getAllVendors = getAllVendors;
		function getAllVendors() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			PengumumanPengadaanService.getAllVendors({
				Vendors: items,
				Offset: (vm.currentPage - 1) * vm.maxSize,
				Limit: vm.maxSize,
				Keyword: vm.keyword
			}, function (reply) {
				if (reply.status === 200) {
					vm.vendors = reply.data.List;
					vm.totalItems = reply.data.Count;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_GETVENDOR', "MESSAGE.ERR_GETTITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_GETVENDOR', "MESSAGE.ERR_GETTITLE");
			});
		}

		vm.pageChanged = pageChanged;
		function pageChanged() {
			getAllVendors();
		}

		vm.selectVendor = selectVendor;
		function selectVendor(selectedVendor) {
		    //console.info(JSON.stringify(selectedVendor));
		    var getVendor = { Code: selectedVendor.Code, VendorName: selectedVendor.Name, 
		        Email: selectedVendor.Email, VendorID: selectedVendor.VendorID }
		    $uibModalInstance.close(getVendor);
		}

		vm.batal = batal;
		function batal() {
		    $uibModalInstance.dismiss('cancel');
		};

		vm.initComm = initComm;
		function initComm() {
		    if (items.CommodityID == null) items.CommodityID = 0;
		    vm.oriviewvendor = items.viewvendor;
		    $translatePartialLoader.addPart("purchase-requisition");
		    UIControlService.loadLoadingModal('MESSAGE.LOADING');
		    if (items.IsLocal == true) {
		        if (items.viewvendor.length == 0) {
		            vm.getData = [];
		            jloadvendor(1, []);
		        }
		        else {
		            vm.getData = items.viewvendor;
		            vm.listSaveVendor = items.viewvendor;
		            jloadvendor(1, items.viewvendor);
		        }
		       
		    }
		    else {
		        if (items.viewvendor.length == 0) {
		            vm.getData = [];
		            jloadvendor(1, []);
		        }
		        else {
		            vm.getData = items.viewvendor;
		            vm.listSaveVendor = items.viewvendor;
		            jloadvendor(1, items.viewvendor);
		        }
		    }
		    
		}

		vm.jloadvendor = jloadvendor;
		function jloadvendor(current, dataSave) {
		    UIControlService.loadLoadingModal("MESSAGE.LOADING");
		    vm.currentPage = current;
		    PengumumanPengadaanService.viewVendor({
		        CommodityID: items.CommodityID,
		        IsLokal: items.IsLocal,
		        IsNational: items.IsNational,
		        IsInternational: items.IsInternational,
		        CompanyScaleID: items.CompScale,
		        TechnicalID: items.TechnicalID,
		        Keyword: vm.keyword,
		        Offset: (vm.currentPage * 10) - 10,
		        contactVendor: dataSave
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.vendors = reply.data.List;
		            if (vm.listSaveVendor.length == 0) {
		                vm.vendors.forEach(function (data) {
		                    if (data.IsLocal == 1) vm.listSaveVendor.push(data);
		                });
		            }
		            else {
		                vm.listSaveVendor.forEach(function (save) {
		                    vm.vendors.forEach(function (data) {
		                        if (save.VendorID == data.VendorID)
		                            data.IsCheck = true;
		                    });
		                });
		            }
		            vm.totalItems = Number(reply.data.Count);
		            UIControlService.unloadLoadingModal();
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		    });
		}


		vm.checkAll = checkAll;
		function checkAll(data) {
		    if (data == true) {
		        UIControlService.loadLoadingModal("MESSAGE.LOADING");
		        PengumumanPengadaanService.viewVendor({
		            CommodityID: items.CommodityID,
		            IsLokal: items.IsLocal,
		            IsNational: items.IsNational,
		            IsInternational: items.IsInternational,
		            CompanyScaleID: items.CompScale,
		            TechnicalID: items.TechnicalID,
		            Keyword: vm.keyword,
		            Offset: (vm.currentPage * 10) - 10,
		            contactVendor: []
		        }, function (reply) {
		            if (reply.status === 200) {
		                vm.vendors = [];
		                vm.listSaveVendor = reply.data.List;
		                jloadvendor(1, []);
		            }
		        }, function (err) {
		            UIControlService.unloadLoadingModal();
		        });
		    }
		    else {
		        vm.listSaveVendor = [];
		        initComm();
		    }
		}

		vm.save = save;
		function save() {
		    $uibModalInstance.close(vm.listSaveVendor);
		};

		vm.change = change;
		function change(data) {
		    if (data.IsCheck == true) vm.listSaveVendor.push(data);
		    for (var i = 0; i < vm.listSaveVendor.length; i++) {
		        if (vm.listSaveVendor[i].VendorID == data.VendorID && data.IsCheck == false) {
		            vm.listSaveVendor.splice(i, 1);
		        }
		    }


		}
	}
})();