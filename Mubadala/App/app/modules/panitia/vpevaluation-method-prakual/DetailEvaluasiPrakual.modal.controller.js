﻿(function () {
	'use strict';

	angular.module("app")
    .controller("detailEvaluasiPrakualModal", ctrl);

	ctrl.$inject = ['$http', '$state', '$filter', '$stateParams', '$uibModalInstance', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPEvaluationMthodService', 'VPCriteriaSettings', 'UIControlService', 'item'];
	/* @ngInject */
	function ctrl($http, $state, $filter, $stateParams, $uibModalInstance, $translate, $translatePartialLoader, $location, SocketService, VPEvaluationMthodService, VPCriteriaSettings, UIControlService, item) {

		var vm = this;
		var data = item.data;
		var med_id = item.med_id;
		var page_id = item.page_id;
		var loadingMessage = "";
		vm.nama = item.nama;
		vm.tp = item.tipeMetode;
		vm.sudahDipakai = item.sudahDipakai;
		vm.hasChild = true;
		vm.evaluationMethodTypeName = item.evaluationMethodTypeName;
		var Bobot = 0;
		var bobot = [];
		var maxScore = 0;
		vm.show = false;
        vm.keyword = ""
        vm.allChecked = false;

		vm.init = init;
		function init() {
			//console.info(item.data);
		    $translatePartialLoader.addPart('vpevaluation-method');
			if (item.nama == "K3L" && item.level == 2)
			    vm.show = true;

			if (item.level === 1)
			    vm.keyword = item.nama;
			else
			    vm.keyword = '';

			//console.info(item.nama + " " + item.level);
			//console.info(vm.keyword);
			vm.initialize();
		};

		vm.initialize = initialize;
		function initialize() {

		    var selectedCriteriaId = [];
		    data.forEach(function (d) {
		        selectedCriteriaId.push(d.kriteria_id);
		    });

			if (item.tipeMetode === 3) {
				VPCriteriaSettings.select({
				    keyword: vm.keyword,
					isVhsCpr: item.tipeMetode,
					level: item.level,
					parentId: item.parent,
					offset: 0,
					limit: 0,
					selectedCriteriaId: selectedCriteriaId
				}, function (reply2) {
					if (reply2.status === 200) {
						if (reply2.data.length > 0) {
							var allKriteria = reply2.data;
							vm.kriteria = [];
							if (item.level === 1) {
								allKriteria.forEach(function (krit) {
									vm.kriteria.push(krit);
								});
							} else {
								vm.kriteria = allKriteria;
							}
							vm.allChecked = true;
							for (var i = 0; i < vm.kriteria.length; i++) {
								vm.kriteria[i].VPEvaluationMethodName = item.namaMetode;
								vm.kriteria[i].VPEvaluationMethodId = item.idMetode;
								vm.kriteria[i].VPEMDId = med_id
								vm.kriteria[i].checked = false;
								vm.kriteria[i].Weight = 0;
								for (var j = 0; j < data.length; j++) {
									if (vm.kriteria[i].CriteriaId === data[j].kriteria_id) {
										vm.kriteria[i].Id = data[j].med_kriteria_id;
										vm.kriteria[i].checked = true;
										vm.kriteria[i].Weight = data[j].bobot;
										break;
									}
								}
								if (!vm.kriteria[i].checked) {
								    vm.allChecked = false;
								};
							}
						} else {
							vm.hasChild = false;
						}
					}
				});
			} 
		};

		vm.onCheck = onCheck;
		function onCheck() {
		    vm.allChecked = true;
		    for (var i = 0; i < vm.kriteria.length; i++) {
		        if (!vm.kriteria[i].checked) {
		            vm.allChecked = false;
		            break;
		        }
		    }
		}

		vm.onCheckAll = onCheckAll;
		function onCheckAll() {
		    for (var i = 0; i < vm.kriteria.length; i++) {
		        vm.kriteria[i].checked = vm.allChecked;
		    }
		}

		vm.simpan = simpan;
		function simpan() {
			if (item.tipeMetode === 2) {
				for (var i = 0; i < vm.kriteria.length; i++) {
					if (vm.kriteria[i].checked && !(vm.kriteria[i].Weight || vm.kriteria[i].Weight === 0)) {
						UIControlService.msg_growl('error', "MESSAGE.ERR_WEIGHT");
						return;
					}
				}
				var totalPersentase = 0;
				for (var i = 0; i < vm.kriteria.length; i++) {
					if (vm.kriteria[i].checked) {
						totalPersentase = totalPersentase + Number(vm.kriteria[i].Weight);
					}
				}
				if (totalPersentase !== 100) {
					UIControlService.msg_growl('error', "MESSAGE.ERR_PERCENTAGE");
					return;
				}
			}

			var detail = [];
			for (var i = 0; i < vm.kriteria.length; i++) {
				vm.kriteria[i].VPEMDId = med_id;
				vm.kriteria[i].Parent = vm.kriteria[i].ParentId; //Beda Nama Kolom di DB
				vm.kriteria[i].IsActive = vm.kriteria[i].checked;
				vm.kriteria[i].Weight = vm.kriteria[i].checked ? vm.kriteria[i].Weight : 0;
				detail.push(vm.kriteria[i]);
			}

			VPEvaluationMthodService.saveDetailCriteria(detail,
                function (reply) {
                	if (reply.status === 200) {
                		vm.kriteria = [];
                		UIControlService.msg_growl('notice', "MESSAGE.SUCC_CHANGE_CRITERIA");
                		UIControlService.unloadLoadingModal();
                		$uibModalInstance.close();
                	}
                	else {
                		UIControlService.msg_growl('error', "MESSAGE.SUCC_CHANGE_CRITERIA");
                		UIControlService.unloadLoadingModal();
                	}
                }, function (err) {
                	UIControlService.msg_growl('error', "MESSAGE.ERR_CHANGE_CRITERIA");
                }
            );
		};

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();