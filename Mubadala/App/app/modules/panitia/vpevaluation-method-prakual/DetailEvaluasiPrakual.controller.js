﻿(function () {
    'use strict';

    angular.module("app").controller("DetailEvalPrakual", ctrl);

    ctrl.$inject = ['$state', '$stateParams', '$scope', '$http', '$uibModal', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCriteriaSettings', 'VPEvaluationMthodService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $stateParams, $scope, $http, $uibModal, $filter, $translate, $translatePartialLoader, $location, SocketService, VPCriteriaSettings, VPEvaluationMthodService, UIControlService) {
        var vm = this;
        var lang;
        var VPEMDId = Number($stateParams.id);
        var type = Number($stateParams.type);
        vm.tp = 3;
        vm.detailVPEval;
        vm.namaMetode = '';
        vm.idMetode = 0
        vm.kriteria = [];
        vm.sudahMengaturLevel1 = 0;
        vm.col_defsc = [];
        vm.allChecked = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('vpevaluation-method');

            if ($stateParams.name === 'Experience' || $stateParams.name === 'K3L')
                vm.flagShow = true;
            else
                vm.flagShow = false;
            loadAwal();
        };

        vm.onCheck = onCheck;
        function onCheck() {
            vm.allChecked = true;
            for (var i = 0; i < vm.kriteria.length; i++) {
                if (!vm.kriteria[i].checked) {
                    vm.allChecked = false;
                    break;
                }
            }
        }

        vm.onCheckAll = onCheckAll;
        function onCheckAll() {
            for (var i = 0; i < vm.kriteria.length; i++) {
                vm.kriteria[i].checked = vm.allChecked;
            }
        }

        function loadAwal() {
            vm.kriteria = [];
            VPEvaluationMthodService.selectDetailById({
                VPEMDId: VPEMDId,
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.detailVPEval = reply.data;
                    vm.namaMetode = vm.detailVPEval.VPEvaluationMethod.VPEvaluationMethodName;//nama metode
                    vm.idMetode = vm.detailVPEval.VPEvaluationMethodId;//VpEvaluation Method ID
                    vm.tipeMetode = vm.detailVPEval.VPEvaluationMethod.Type.Name;//tipe metode
                    if ($stateParams.name === 'Experience' || $stateParams.name === 'K3L') {
                        if (vm.col_defsv.length === 1) {
                            addAdditionalColumn();
                        }
                    } else  {
                        if (vm.col_defsc.length === 0) {
                            addAdditionalColumn();
                        }
                    }

                    VPEvaluationMthodService.selectdetailcriteria({
                        VPEMDId: VPEMDId,
                        Parent: null,
                        Level: 1
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            vm.sudahMengaturLevel1 = reply.data.length > 0;
                            //console.info(vm.sudahMengaturLevel1);
                            if (vm.sudahMengaturLevel1 == false) {
                                VPCriteriaSettings.select({
                                    keyword: $stateParams.name,
                                    isVhsCpr: type,
                                    level: 1,
                                    parentId: null,
                                    offset: 0,
                                    limit: 0,
                                }, function (reply) {
                                    if (reply.status === 200) {
                                        var allKriteria = reply.data;
                                        allKriteria.forEach(function (krit) {
                                            vm.kriteria.push(krit);
                                        });

                                        for (var i = 0; i < vm.kriteria.length; i++) {
                                            vm.kriteria[i].checked = false;
                                            vm.kriteria[i].VPEvaluationMethodName = vm.namaMetode;
                                            vm.kriteria[i].VPEvaluationMethodId = vm.idMetode;
                                            if (vm.tipeMetode == 'TYPE_VHS') {
                                                vm.kriteria[i].checked = true;
                                            }
                                        }
                                        //console.info(vm.kriteria);
                                    }
                                });
                            } else if (vm.sudahMengaturLevel1 === true) {
                                reply.data.forEach(function (obj) {
                                    obj.children = [];
                                    obj.bisaNgatur = vm.userBisaMengatur;
                                    vm.kriteria.push({
                                        "med_kriteria_id": obj.Id,
                                        "metode_evaluasi_id": obj.VPEvaluationMethodId,
                                        "metode_evaluasi_nm": obj.VPEvaluationMethodName,
                                        "med_id": obj.VPEMDId,
                                        "jenis_detail": obj.DetailType,
                                        "kriteria_id": obj.CriteriaId,
                                        "kriteria_nama": obj.CriteriaName,
                                        "bobot": obj.Weight,
                                        "level": obj.Level,
                                        "is_active": obj.IsActive,
                                        "parent": obj.Parent,
                                        "children": obj.children,
                                        "expanded": expandedCriteriaIds.indexOf(obj.Id) >= 0
                                        //"obj": obj,
                                    });
                                    vm.selectSubKriteria(obj);
                                });
                            }
                        }
                    });
                } else {
                    UIControlService.msg_growl("error", $filter('translate')("MESSAGE.ERR_GETDETAIL"));
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_GETDETAIL'));
            });
        };

        vm.selectSubKriteria = selectSubKriteria;
        function selectSubKriteria(object) {
            VPEvaluationMthodService.selectdetailcriteria({
                VPEMDId: VPEMDId,
                Level: object.Level + 1,
                Parent: object.CriteriaId
            }, function (reply) {
                if (reply.status === 200) {
                    if (reply.data.length > 0) {
                        reply.data.forEach(function (objChild) {
                            objChild.children = [];
                            objChild.bisaNgatur = vm.userBisaMengatur;
                            vm.kriteria.forEach(function (objParent) {
                                if (objChild.Parent === objParent.kriteria_id) {
                                    objChild.children = [];
                                    objChild.bisaNgatur = vm.userBisaMengatur;
                                    objParent.children.push({
                                        "med_kriteria_id": objChild.Id,
                                        "metode_evaluasi_id": objChild.VPEvaluationMethodId,
                                        "metode_evaluasi_nm": objChild.VPEvaluationMethodName,
                                        "med_id": objChild.VPEMDId,
                                        "jenis_detail": objChild.DetailType,
                                        "kriteria_id": objChild.CriteriaId,
                                        "kriteria_nama": objChild.CriteriaName,
                                        "bobot": objChild.Weight,
                                        "level": objChild.Level,
                                        "is_active": objChild.IsActive,
                                        "parent": objChild.Parent,
                                        "children": objChild.children,
                                        "expanded": expandedCriteriaIds.indexOf(objChild.Id) >= 0
                                        //"obj": obj,
                                    });
                                }
                            });
                            VPEvaluationMthodService.selectdetailcriteria({
                                VPEMDId: VPEMDId,
                                Level: objChild.Level + 1,
                                Parent: objChild.CriteriaId
                            }, function (reply2) {
                                if (reply2.status === 200) {
                                    if (reply2.data.length > 0) {
                                        reply2.data.forEach(function (objGrndChild) {
                                            objGrndChild.children = [];
                                            objGrndChild.bisaNgatur = vm.userBisaMengatur;
                                            for (var i = 0; i < vm.kriteria.length; i++) {
                                                if (vm.kriteria[i].children.length > 0) {
                                                    for (var j = 0; j < vm.kriteria[i].children.length; j++) {
                                                        if (objGrndChild.Parent === vm.kriteria[i].children[j].kriteria_id) {
                                                            vm.kriteria[i].children[j].children.push({
                                                                "med_kriteria_id": objGrndChild.Id,
                                                                "metode_evaluasi_id": objGrndChild.VPEvaluationMethodId,
                                                                "metode_evaluasi_nm": objGrndChild.VPEvaluationMethodName,
                                                                "med_id": objGrndChild.VPEMDId,
                                                                "jenis_detail": objGrndChild.DetailType,
                                                                "kriteria_id": objGrndChild.CriteriaId,
                                                                "kriteria_nama": objGrndChild.CriteriaName,
                                                                "bobot": objGrndChild.Weight,
                                                                "level": objGrndChild.Level,
                                                                "is_active": objGrndChild.IsActive,
                                                                "parent": objGrndChild.Parent,
                                                                "children": objGrndChild.children,
                                                            });
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                }
            });
        };


        vm.back = back;
        function back() {
            $state.transitionTo('add-evaluasi-vp', {
                id: vm.idMetode,
                type: 3
            });
        };

        vm.save = save;
        function save() {

            var noCheckedCrit = true;
            for (var i = 0; i < vm.kriteria.length; i++) {
                if (vm.kriteria[i].checked === true) {
                    noCheckedCrit = false;
                    break
                }
            }
            if (noCheckedCrit) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_NO_SELECTED_CRIT'));
                return;
            }

            if (type == 2) {
                var totalPersentase = 0;
                for (var i = 0; i < vm.kriteria.length; i++) {
                    if (vm.kriteria[i].checked) {
                        totalPersentase = totalPersentase + Number(vm.kriteria[i].Weight);
                    }
                }

                if (totalPersentase !== 100) {
                    UIControlService.msg_growl('error', "MESSAGE.ERR_PERCENTAGE");
                    return;
                }
            }

            var detail = [];
            for (var i = 0; i < vm.kriteria.length; i++) {
                vm.kriteria[i].VPEMDId = VPEMDId
                vm.kriteria[i].Parent = vm.kriteria[i].ParentId; //Beda Nama Kolom di DB
                vm.kriteria[i].IsActive = vm.kriteria[i].checked;
                vm.kriteria[i].Weight = vm.kriteria[i].checked ? vm.kriteria[i].Weight : 0;
                detail.push(vm.kriteria[i]);
            }

            VPEvaluationMthodService.saveDetailCriteria(detail,
               function (reply) {
                   if (reply.status === 200) {
                       vm.kriteria = [];
                       loadAwal();
                       UIControlService.msg_growl('notice', "MESSAGE.SUCC_LEV1");
                       UIControlService.unloadLoading();
                   }
                   else {
                       UIControlService.msg_growl('error', "MESSAGE.ERR_LEV1");
                       UIControlService.unloadLoading();
                   }
               }, function (err) {
                   UIControlService.msg_growl('error', "MESSAGE.ERR_LEV1");
               }
           );
        };

        vm.ubahDetailLevel1 = ubahDetailLevel1
        function ubahDetailLevel1() {
            var lempar = {
                namaMetode: vm.namaMetode,
                idMetode: vm.idMetode,
                data: vm.kriteria,
                med_id: VPEMDId,
                level: 1,
                parent: 0,
                nama: $stateParams.name,
                tipeMetode: 3
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpevaluation-method-prakual/DetailEvaluasiPrakual.modal.html?v=1.000002',
                controller: 'detailEvaluasiPrakualModal',
                controllerAs: 'detailEvaluasiPrakualModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.tambahDetailLevel1 = tambahDetailLevel1;
        function tambahDetailLevel1(obj) {
            var data = [];
            if (obj.level == 0) {
                data = vm.kriteria;
            } else if (obj.level == 1) {
                for (var i = 0; i < vm.kriteria.length; i++) {
                    if (vm.kriteria[i].kriteria_id == obj.kriteria_id) {
                        data = vm.kriteria[i].children;
                    }
                }
            } else {
                for (var i = 0; i < vm.kriteria.length; i++) {
                    for (var j = 0; j < vm.kriteria[i].children.length; j++) {
                        if (vm.kriteria[i].children[j].kriteria_id == obj.kriteria_id) {
                            data = vm.kriteria[i].children[j].children;
                        }
                    }
                }
            }
            var lempar = {
                data: data,
                namaMetode: vm.namaMetode,
                idMetode: vm.idMetode,
                med_id: VPEMDId,
                level: Number(obj.level) + 1,
                parent: obj.kriteria_id,
                nama: obj.kriteria_nama,
                tipeMetode: 3
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpevaluation-method-prakual/DetailEvaluasiPrakual.modal.html?v=1.000002',
                controller: 'detailEvaluasiPrakualModal',
                controllerAs: 'detailEvaluasiPrakualModal',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadAwal();
            });
        };

        vm.expanding_property = {
            field: "kriteria_nama",
            displayName: lang === 'id' ? 'Kriteria Evaluasi' : 'Evaluation Criteria',
            sortable: false,
            sortingType: "string",
            filterable: false
        };

        vm.col_defsv = [
        {
            field: "bobot",
            displayName: lang === 'id' ? 'Bobot' : 'Weight',
            cellTemplate: '<a style="width: 10%"> {{row.branch.bobot}}&nbsp; </a> ',
            sortable: false,
            sortingType: "number",
            filterable: false
        }];

        function addAdditionalColumn() {
            if ($stateParams.name === 'Experience' || $stateParams.name === 'K3L') {
                vm.col_defsv.push({
                    field: "obj",
                    displayName: "  ",
                    cellTemplate: '<a ng-if="row.branch.level < 3" class="btn btn-flat btn-xs btn-primary" ng-click="cellTemplateScope.click(row.branch)"><i class="fa fa-plus-circle"></i>&nbsp;' + (lang === 'id' ? 'Ubah Sub Kriteria' : 'Sub Criteria') + '</a>',
                    cellTemplateScope: {
                        click: function (data) {
                            vm.tambahDetailLevel1(data);
                        }
                    }
                });
            }
            else {
                vm.col_defsc.push({
                    field: "obj",
                    displayName: "  ",
                    cellTemplate: '<a ng-if="row.branch.level < 3" class="btn btn-flat btn-xs btn-primary" ng-click="cellTemplateScope.click(row.branch)"><i class="fa fa-plus-circle"></i>&nbsp;' + (lang === 'id' ? 'Ubah Sub Kriteria' : 'Sub Criteria') + '</a>',
                    cellTemplateScope: {
                        click: function (data) {
                            vm.tambahDetailLevel1(data);
                        }
                    }
                });
            } 
        };

        var expandedCriteriaIds = [];
        vm.onSelectTree = onSelectTree;
        function onSelectTree(crit) {
            if (crit.expanded === true) {
                expandedCriteriaIds.push(crit.med_kriteria_id);
            }
            else {
                for (var i = 0; i < expandedCriteriaIds.length; i++) {
                    if (expandedCriteriaIds[i] === crit.med_kriteria_id) {
                        expandedCriteriaIds.splice(i, 1);
                        break;
                    }
                }
            }
        }
    }
})();

