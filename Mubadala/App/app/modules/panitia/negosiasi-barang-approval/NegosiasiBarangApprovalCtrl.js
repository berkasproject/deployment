(function () {
    'use strict';

    angular.module("app").controller("GoodsNegotiationAppCtrl", ctrl);

    ctrl.$inject = ['$filter','Excel','$timeout','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'NegosiasiBarangService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter,Excel,$timeout, $http, $translate, $translatePartialLoader, $location, SocketService, NegosiasiBarangService,
        RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        var page_id = 141;
        vm.evalsafety = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.kata = new Kata("");
        vm.init = init;
        vm.exportHref;
        vm.detail = [];
        vm.jLoad = jLoad;
        vm.isCalendarOpened = [false, false, false, false];

        function init() {
            $translatePartialLoader.addPart('negosiasi-barang');
            UIControlService.loadLoading("MESSAGE.LOADING");
            jLoad(1);
        }
        vm.jLoad = jLoad;
        function jLoad(current) {
            NegosiasiBarangService.GetNegoToApproval({}, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.detail = reply.data;
                    vm.count = reply.data.Count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.save = save;
        function save(flag, data) {
            vm.dtnego = data;
            if (flag == true) {
                bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROV'), function (yes) {
                    if (yes) {
                        data.ApprovalStatus = flag;
                        UIControlService.loadLoadingModal("MESSAGE.LOADING");
                        NegosiasiBarangService.updateApproval(data,
                        function (reply) {
                            if (reply.status === 200) {
                                UIControlService.unloadLoading();
                                if (vm.dtnego.nego.IsNego == false) {
                                    sendMailNego();
                                    init();
                                }
                                else init();
                            }
                        },
                        function (err) {
                            // UIControlService.msg_growl("error", "Gagal Akses Api!!");
                        }
                    );
                    }
                    else data.ApprovalStatus = null;
                });
            }
            else {
                bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REJECT'), function (yes) {
                    if (yes) {
                        data.ApprovalStatus = flag;
                        UIControlService.loadLoadingModal("MESSAGE.LOADING");
                        NegosiasiBarangService.updateApproval(data,
                        function (reply) {
                            if (reply.status === 200) {
                                UIControlService.unloadLoading();
                                init();
                            }
                        },
                        function (err) {
                            // UIControlService.msg_growl("error", "Gagal Akses Api!!");
                        }
                    );
                    }
                    else data.ApprovalStatus = null;
                });
            }
        }

        function sendMail() {
            vm.dtnego.ApprovalStatus = true;
            UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            NegosiasiBarangService.sendMailToBuyer(vm.dtnego,
                function (response) {
                    console.info(response);
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        UIControlService.msg_growl("notice", "MESSAGE.SEND_EMAIL");
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }

        vm.sendMailNego = sendMailNego;
        function sendMailNego() {
            var datainsert = {
                ID: vm.dtnego.GoodsNegoId
            }
            NegosiasiBarangService.sendMail(datainsert,
            function (reply) {
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SEND_EMAIL");
                }
                else {
                    //UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                    return;
                }
            },
            function (err) {
                //UIControlService.msg_growl("error", "Gagal Akses Api!!");
            }
        );

        }

        vm.simpan = simpan;
        vm.List = [];
        function simpan() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            for (var i = 0; i < vm.detail.length; i++) {
                if (vm.detail[i].selectTaxCode === null) {
                    UIControlService.msg_growl("warning", "Tax code belum diisi !!");
                    return;
                }
                else if (vm.detail[i].selectStorageLocation === null) {
                    UIControlService.msg_growl("warning", "Storage Location belum diisi !!");
                    return;
                }
                else if (vm.detail[i].PODate === null) {
                    UIControlService.msg_growl("warning", "Tanggal kirim barang belum diisi !!");
                    return;
                }
                else {
                    var dataGoods = {
                        ID: vm.detail[i].ID,
                        TenderStepID: vm.detail[i].TenderStepID,
                        VendorID: vm.detail[i].VendorID,
                        TotalNego: vm.detail[i].TotalNego,
                        TaxCode: vm.detail[i].selectTaxCode.ID,
                        StorageLocation: vm.detail[i].selectStorageLocation.ID,
                        PODate: UIControlService.getStrDate(vm.detail[i].PODate)
                    }
                    vm.List.push(dataGoods);
                    if (i == vm.detail.length - 1 && vm.List.length === vm.detail.length) {
                        NegosiasiBarangService.update(vm.List, function (reply) {
                            UIControlService.unloadLoading();
                            if (reply.status === 200) {
                                vm.ListDeal = [];
                                vm.flag = false;
                                UIControlService.msg_growl("success", "Berhasil Simpan Data PO!!");
                                window.location.reload();

                            }
                            else {
                                UIControlService.msg_growl("error", "Gagal menyimpan data!!");
                                return;
                            }
                        }, function (err) {
                            UIControlService.msg_growl("error", "MESSAGE.API");
                            UIControlService.unloadLoadingModal();
                        });
                    }
                }
                
            }
            
        }

        vm.edit = edit;
        function edit(dataTabel) {
            var data = {
                item: dataTabel
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/Detail.html',
                controller: 'DetailGoodsAwardCtrl',
                controllerAs: 'DetailGoodsAwardCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.Export = Export;
        function Export(tableId) {
            vm.exportHref = Excel.tableToExcel(tableId, 'sheet name');
            $timeout(function () { location.href = vm.exportHref; }, 100); // trigger download
        }

        function sendMail(data) {
            var email = {
                subject: 'Notifikasi Pemenang ',
                mailContent: 'Selamat Anda adalah pemendang ',
                isHtml: false,
                addresses: data
            };

            UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            NegosiasiBarangService.sendMail(email,
                function (response) {
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                            loadNotDeal();
                            UIControlService.msg_growl("notice", "Email Telah dikirim ke vendor pemenang")
                    } else {
                        UIControlService.handleRequestError(response.data);
                    }
                },
                function (response) {
                    UIControlService.handleRequestError(response.data);
                    UIControlService.unloadLoading();
                });
        }

        vm.loadNotDeal = loadNotDeal;
        function loadNotDeal() {
            var model = {
                column: vm.StepID,
                Status: vm.TenderRefID,
                FilterType: vm.ProcPackType
            }
            NegosiasiBarangService.selectVendorNotDeal(model, function (reply) {
                console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.detailNotDeal = reply.data;
                    vm.ListNotDeal = [];
                    for (var j = 0; j < vm.detailNotDeal.length; j++) {
                        vm.ListNotDeal.push(vm.detailNotDeal[j].Email);
                    }
                    var email = {
                        subject: 'Notifikasi Pemenang Tender' + vm.TenderName,
                        mailContent: 'Maaf anda belum berhasil memenangkan Tender' + vm.TenderName,
                        isHtml: false,
                        addresses: vm.ListNotDeal
                    };

                    UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
                    NegosiasiBarangService.sendMail(email,
                        function (response) {
                            UIControlService.unloadLoading();
                            if (response.status == 200) {
                                UIControlService.msg_growl("notice", "Email Telah dikirim ke vendor kalah");
                                init();
                            } else {
                                UIControlService.handleRequestError(response.data);
                            }
                        },
                        function (response) {
                            UIControlService.handleRequestError(response.data);
                            UIControlService.unloadLoading();
                        });
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Peneapan Pemenang" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.getUserLogin = getUserLogin;
        function getUserLogin() {
            NegosiasiBarangService.CekRequestor({
                TenderRefID: vm.TenderRefID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.flagSave = reply.data;
                    console.info(vm.flagSave);
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.sendToApprove = sendToApprove;
        function sendToApprove(data) {
            bootbox.confirm($filter('translate')('Apakah anda ingin mengirim data approval ini untuk proses persetujuan?'), function (yes) {
                if (yes) {
                    UIControlService.loadLoading("MESSAGE.LOADING");
                    var dt = {
                        ID: data.ID,
                        TenderStepID: vm.StepID,
                        flagEmp: 1
                    };
                    NegosiasiBarangService.SendApproval(dt, function (reply) {
                        if (reply.status === 200) {
                            console.info(reply.data);
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SEND_TO_APPRV'));
                            init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                        }
                    }, function (error) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SEND_TO_APPRV'));
                    });
                }
            });
        }
        
        vm.detailApproval = detailApproval;
        function detailApproval(dt) {
            var item = {
                ID: dt.GoodsNegoId,
                Status: 0
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-barang-approval/detailApproval.modal.html',
                controller: 'detailApprovalGoodsAppCtrl',
                controllerAs: 'detailApprovalGoodsAppCtrl',
                resolve: { item: function () { return item; } }
            });
        };

        vm.reject = reject;
        function reject(dt, flag) {
            var item = {
                ID: dt,
                Status: 1,
                flagNego: flag
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/negosiasi-barang-approval/detailApproval.modal.html',
                controller: 'detailApprovalGoodsAppCtrl',
                controllerAs: 'detailApprovalGoodsAppCtrl',
                resolve: { item: function () { return item; } }
            });

            modalInstance.result.then(function () {
                init();
            });
        };

        vm.createExcel = createExcel;
        function createExcel() {
            var data = {
                StepID: vm.StepID,
                TenderRefID: vm.TenderRefID,
                ProcPackType: vm.ProcPackType
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/penetapan-pemenang-barang/SaveExcelNotif.html',
                controller: "SaveNotifExcel",
                controllerAs: "SaveNotifExcel",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        vm.GotoTenderInfo = GotoTenderInfo;
        function GotoTenderInfo(data) {
            $state.go('data-pengadaan-tahapan', { TenderRefID: data.TenderRefID, ProcPackType: data.ProcPackType });
        }
    }
})();
//TODO

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}

