﻿(function () {
	'use strict';

	angular.module('app').controller('viewVendorCtrl', ctrl);

	ctrl.$inject = ['item', 'ApprovalForMin3Service', 'UIControlService', '$translatePartialLoader', '$uibModalInstance'];

	function ctrl(item, ApprovalForMin3Service, UIControlService, $translatePartialLoader, $uibModalInstance) {
		var vm = this;

		vm.vendors = null;
		vm.currentPage = 1;
		vm.maxSize = 10;
		vm.keyword = '';

		vm.init = init;
		function init() {
			UIControlService.loadLoadingModal('MESSAGE.LOADING');
			$translatePartialLoader.addPart('tender-step-approval');
			ApprovalForMin3Service.viewVendor({
				TenderStepDataId: item.item.TenderStepDataId
			}, function (reply) {
				if (reply.status === 200) {
					vm.result = reply.data;
					vm.vendors = reply.data.Vendors;
					UIControlService.unloadLoadingModal();
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_GET_VENDOR', "MESSAGE.ERR_TITLE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_GET_VENDOR', "MESSAGE.ERR_TITLE");
			});
		}

		vm.cancel = cancel;
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();