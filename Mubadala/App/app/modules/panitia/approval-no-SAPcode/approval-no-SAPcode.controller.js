﻿(function () {
	'use strict';

	angular.module("app").controller("ApprovalNoSAPCtrl", ctrl);

	ctrl.$inject = ['$filter', '$translatePartialLoader', 'ApprovalNoSAPService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];

	function ctrl($filter, $translatePartialLoader, ApprovalNoSAPService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {
		var vm = this;
		vm.init = init;
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.textSearch = '';
		vm.columnApprovalStatus = '2'

		function init() {
			$translatePartialLoader.addPart('tender-step-approval');
			UIControlService.loadLoading("MESSAGE.LOADING");
			loadDataApproval(1);
		}

		vm.loadDataApproval = loadDataApproval;
		function loadDataApproval(current) {
			vm.dataApproval = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			ApprovalNoSAPService.select({
				Keyword: vm.textSearch,
				Offset: offset,
				Limit: vm.pageSize,
				Status: vm.columnApprovalStatus
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.dataApproval = reply.data.List;
					vm.totalItems = reply.data.Count;
					vm.maxSize = vm.totalItems;
					//console.info("dataApproval:" + JSON.stringify(vm.dataApproval));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changedFilter = changedFilter
		function changedFilter() {
			loadDataApproval(1);
		}

		vm.GotoTenderInfo = GotoTenderInfo;
		function GotoTenderInfo(data) {
			$state.go('data-pengadaan-tahapan', { TenderRefID: data.TenderRefID, ProcPackType: data.ProcPackageType });
		}

		vm.approve = approve;
		function approve(data) {
			//console.info("data:" + JSON.stringify(data));
			//bootbox.confirm($filter('translate')('CONFIRM_APPROVE'), function (res) {
			//	if (res) {
			//		ApprovalNoSAPService.approve({
			//			ID: data.ID,
			//			TenderStepDataId: data.TenderStepDataId,
			//			ApprovalComment: "approve",
			//			VendorID: data.VendorID
			//		}, function (reply) {
			//			UIControlService.unloadLoading();
			//			if (reply.status === 200) {
			//				UIControlService.msg_growl("success", "MESSAGE.SUCC_APPROVE");
			//				loadDataApproval(vm.currentPage);
			//			}
			//		}, function (err) {
			//			UIControlService.msg_growl("error", "MESSAGE.FAIL_APPROVE");
			//			UIControlService.unloadLoading();
			//		});
			//	}
			//});

			var data = { item: data };

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-no-SAPcode/approvalModalApprove.html',
				controller: 'ApprovalNoSAPModalCtrl',
				controllerAs: 'ApprovalNoSAPModalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//init();
				loadDataApproval(vm.currentPage);
			});
		}

		vm.reject = reject;
		function reject(data) {
			console.info("data:" + JSON.stringify(data));
			ApprovalForMin3Service.reject({
				Id: data.TenderStepDataId,
				ApprvType: data.ApprvType,
				ApprovalComment: "reject"
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "MESSAGE.SUCC_REJECT");
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.FAIL_REJECT");
				UIControlService.unloadLoading();
			});
		}

		vm.viewVendors = viewVendors;
		function viewVendors(list, flag) {
			var data = { item: list, act: flag };

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-min3/viewVendor.html',
				controller: 'viewVendorCtrl',
				controllerAs: 'viewVendorCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//init();
				loadDataApproval(vm.currentPage);
			});
		}

		vm.detailApprovalL1L2 = detailApprovalL1L2
		function detailApprovalL1L2(dt) {
			var item = {
				TenderStepDataId: dt.TenderStepDataId,
				VendorID: dt.VendorID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-no-SAPcode/approval-no-SAPcode.modal.html',
				controller: 'detailApprovalNoSAPCtrl',
				controllerAs: 'detailApprovalNoSAPCtrl',
				resolve: { item: function () { return item } }
			})
			modalInstance.result.then(function (ID, filter, flag) {
				init()
			})
		}

		vm.approvalModal = approvalModal;
		function approvalModal(list) {
			var data = { item: list };

			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/approval-no-SAPcode/approvalModal.html',
				controller: 'ApprovalNoSAPModalCtrl',
				controllerAs: 'ApprovalNoSAPModalCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//init();
				loadDataApproval(vm.currentPage);
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType });
		}
	}
})();


