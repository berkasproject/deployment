﻿(function () {
    'use strict';

    angular.module("app").controller("SummaryApprovalTenderInterestCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'TenderInterestApprovalService', 'RoleService', 'UIControlService', '$uibModal', 'GlobalConstantService', 'item', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, TenderInterestApprovalService,
        RoleService, UIControlService, $uibModal, GlobalConstantService, item, $uibModalInstance) {
        var vm = this;
        vm.data = item.data;
        vm.userBisaNgatur = false;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.init = init;
        function init() {
            console.info(JSON.stringify(vm.data));
            //$translatePartialLoader.addPart('pendaftaran-lelang');
            //loadTenderInterestReview();
            // UIControlService.loadLoading("Silahkan Tunggu...");
            //  getBlacklist();

        }


        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();