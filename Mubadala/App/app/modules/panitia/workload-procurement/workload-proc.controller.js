(function () {
	'use strict';

	angular.module("app").controller("WorkloadProcurementCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'WorkloadProcurementService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, WorkloadProcurementService, UIControlService) {

		var vm = this;
		vm.init = init;

		vm.procTypes = [];
		vm.durasi = null;

		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.isCalendarOpened = [false, false, false, false];

		vm.data = [];

		function init() {
		    $translatePartialLoader.addPart('workload-procurement');
		    procurementType();
		    //dataWorkloadProc();
		    //grafikNewVendor();
		}

		function procurementType() {
		    WorkloadProcurementService.procurementType({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.procTypes = reply.data;
		            //console.info("proctypes:" + JSON.stringify(vm.procTypes));
		            employeePositionName();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
		        UIControlService.unloadLoading();
		    });
		}

		vm.filter = filter;
		function filter() {
		    var invalidFilter = false;
		    if (vm.durasi == null) {
		        invalidFilter = true;
		    }
		    else {
		        if (vm.durasi.Value == 0) { //minggu
		            if (vm.ma == null || vm.mb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ma.NumberOfWeek > vm.mb.NumberOfWeek) {
		                    invalidFilter = true;
		                }
		                else {
		                    vm.batasAtas = vm.ma.NumberOfWeek;
		                    vm.batasBawah = vm.mb.NumberOfWeek;
		                }
		            }
		        }
		        else if (vm.durasi.Value == 1) { //bulan         
		            if (vm.ba == null || vm.bb == null) {
		                invalidFilter = true;
		            }
		            else {
		                if (vm.ba > vm.bb) {
		                    invalidFilter = true;
		                }
		                else {
		                    var intBatasAtas = vm.ba.getMonth();
		                    var intBatasBawah = vm.bb.getMonth();
		                    vm.batasAtas = intBatasAtas + 1;
		                    vm.batasBawah = intBatasBawah + 1;
		                }
		            }

		        }
		    }
		    if (invalidFilter == true) {
		        UIControlService.msg_growl("warning", "MESSAGE.INVALID_FILTER");
		        return;
		    }
		    else if (invalidFilter == false) {
		        UIControlService.loadLoading("MESSAGE.LOADING");
		        dataWorkloadProc();
		    }
		}

		vm.cekFilterTahun = cekFilterTahun;
		function cekFilterTahun() {
		    vm.tahun = vm.filterTahun.getFullYear();
		    if (vm.durasi.Value == 0) {
		        weekByYear();
		    }
		    else if (vm.durasi.Value == 1) {
		        var mindateNow = new Date("January 01, " + vm.tahun + " 00:00:00");
		        var maxdateNow = new Date("December 31, " + vm.tahun + " 23:59:59");
		        vm.datepickeroptions = {
		            minMode: 'month',
		            maxDate: maxdateNow,
		            minDate: mindateNow
		        }
		        vm.ba = mindateNow;
		        vm.bb = maxdateNow;
		    }
		}

		function employeePositionName() {
		    WorkloadProcurementService.employeePositionName({
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.employeePositionName = reply.data;
		            //console.info("employee" + vm.employeePositionName);
                    /*
		            console.info("emposname:" + JSON.stringify(vm.employeePositionName));
		            if (vm.employeePositionName == "L1 Stock Purchasing") {
		                vm.procType = vm.procTypes[1];
		                vm.isMgr = false;
		            }
		            else if (vm.employeePositionName == "L1 Direct Purchasing") {
		                vm.procType = vm.procTypes[0];
		                vm.isMgr = false;
		            }
		            else if (vm.employeePositionName == "MGR Procurement") {
		                vm.procType = vm.procTypes[0];
		            }
		            else if (vm.employeePositionName == "MGR Supply Chain Management") {
		                vm.procType = vm.procTypes[0];
		            }*/
		            vm.procType = vm.procTypes[0];
		            konfigurasiWaktu();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
		        UIControlService.unloadLoading();
		    });
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
		    vm.isCalendarOpened[index] = true;
		};

		vm.konfigurasiWaktu = konfigurasiWaktu;
		function konfigurasiWaktu() {
		    vm.datenow = new Date();
		    vm.getYearNow = vm.datenow.getFullYear();
		    var mindateNow = new Date("January 01, " + vm.getYearNow + " 00:00:00");
		    var maxdateNow = new Date("December 31, " + vm.getYearNow + " 23:59:59");
		    //mindateNow.setYear(vm.getYearNow - 1);
		    vm.datepickeroptions = {
		        minMode: 'month',
		        maxDate: maxdateNow,
		        minDate: mindateNow
		    }
		    vm.datepickeroptionsTahun = {
		        minMode: 'year'
		    }
		    if (localStorage.getItem("currLang") === 'id' || localStorage.getItem("currLang") === 'ID') {
			    vm.ddDurasi =
	            [
	                { Value: 0, Name: "Minggu" },
	                { Value: 1, Name: "Bulan" }

	            ]
	        }
		    else if (localStorage.getItem("currLang") === 'en' || localStorage.getItem("currLang") === 'EN') {
	        	vm.ddDurasi =
	            [
	                { Value: 0, Name: "Week" },
	                { Value: 1, Name: "Month" }

	            ]
	        }
		    //vm.jenistender = vm.ddTender[0];
		    vm.durasi = vm.ddDurasi[0];
		    vm.filterTahun = vm.datenow;
		    vm.tahun = vm.getYearNow;
		    weekByYear();
		}

		function weekByYear() {
		    WorkloadProcurementService.weekByYear({
		        column: vm.tahun
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.weeks = reply.data;
		            //console.info("weeks" + JSON.stringify(vm.weeks));
		            for (var i = 0; i <= vm.weeks.length - 1; i++) {
		                vm.weeks[i].label = "Week #" + vm.weeks[i].NumberOfWeek + " (" + UIControlService.convertDate(vm.weeks[i].StartDate) + " s/d " + UIControlService.convertDate(vm.weeks[i].EndDate) + ")";
		            }
		            vm.ma = vm.weeks[0];
		            vm.mb = vm.weeks[vm.weeks.length - 1];
		            vm.batasAtas = vm.ma.NumberOfWeek;
		            vm.batasBawah = vm.mb.NumberOfWeek;
		            dataWorkloadProc();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_POST");
		        UIControlService.unloadLoading();
		    });
		}

		function dataWorkloadProc() {
		    WorkloadProcurementService.dataWorkloadProc({
		        column: vm.tahun,
		        IntParam1: vm.batasAtas,
		        IntParam2: vm.batasBawah,
		        Offset: 0,
		        Limit: vm.maxSize,
		        Status: vm.durasi.Value,
		        FilterType: vm.procType.RefID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.data = reply.data;
		            console.info("data" + JSON.stringify(vm.data));
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

	}
})();
