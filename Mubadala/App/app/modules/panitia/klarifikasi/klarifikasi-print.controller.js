﻿(function () {
    'use strict';

    angular.module("app").controller("ClarificationPrintController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'ClarificationChatAdminService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService,
        ClarificationChatAdminService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;

        vm.StepID = Number($stateParams.StepID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.vendors = [];

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('klarifikasi');
            UIControlService.loadLoading("");
            loadData();
        }

        vm.cetak = cetak;
        function cetak() {
            html2canvas(document.getElementById('print'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download(vm.tenderStepData.tender.TenderName + " (" + vm.tenderStepData.step.TenderStepName + ").pdf");
                }
            });
        }

        function loadData() {
            UIControlService.loadLoading("");
            ClarificationChatAdminService.GetStepInfo({
                ID: vm.StepID
            }, function (reply) {
                vm.tenderStepData = reply.data;
                ClarificationChatAdminService.GetVendors({
                    ID: vm.StepID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    vm.vendors = reply.data;
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'ERR_LOAD_VENDORS');
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'ERR_LOAD_STEP');
            });
        };

        vm.detail = detail;
        function detail(vendor) {
            $state.transitionTo('klarifikasi-chat', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType, VendorID: vendor.VendorID });
        }

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.TenderRefID, ProcPackType: vm.ProcPackType, TenderID: vm.tenderStepData.TenderID });
        }
    }
})();

