﻿(function () {
	'use strict';

	angular.module("app").controller("WriteClarificationChatCtrl", ctrl);

	ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'ClarificationChatAdminService',
        'UIControlService', '$uibModal', '$state', '$stateParams', 'UploadFileConfigService', 'UploaderService', 'GlobalConstantService', '$uibModalInstance'];
	function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, ClarificationChatAdminService,
        UIControlService, $uibModal, $state, $stateParams, UploadFileConfigService, UploaderService, GlobalConstantService, $uibModalInstance) {

		var vm = this;
		vm.message = "";

		vm.init = init
		function init() {
		    vm.isInformByEmail = true;
		    getTypeSizeFile();
		}

		function getTypeSizeFile() {
		    UploadFileConfigService.getByPageName("PAGE.ADMIN.TENDER.AANWIJZING", function (response) {
		        UIControlService.unloadLoadingModal();
		        if (response.status == 200) {
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];
		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
		        UIControlService.unloadLoadingModal();
		        return;
		    });
		}
		vm.selectUpload = selectUpload;
		function selectUpload() {
		    console.info(vm.fileUpload);
		}

	    /*proses upload file*/
		function uploadFile() {
		    var folder = 'CLARIFICATION_';
		    if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		        upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, folder);
		    }
		}

		function upload(file, config, filters, dates, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;
		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		    }

		    UIControlService.loadLoadingModal("");
		    UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, dates,
                function (response) {
                    console.info()
                    UIControlService.unloadLoadingModal();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        processsave(url);
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD")
                    UIControlService.unloadLoadingModal();
                });

		}

		function validateFileType(file, allowedFileTypes) {
		    if (!file || file.length == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		        return false;
		    }

		    return true;
		}

		vm.simpan = simpan;
		function simpan() {
		    if (!vm.fileUpload && !vm.pathfile) {
		        processsave('');
		    }
		    else {
		        uploadFile();
		    }
		}

		function processsave(urlDoc) {
			UIControlService.loadLoadingModal("");
			ClarificationChatAdminService.AddChat({
				TenderStepDataID: item.StepID,
				VendorID: item.VendorID,
				Message: vm.message,
				IsInformByEmail: vm.isInformByEmail,
                UrlDoc: urlDoc
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("success", "SUCC_SEND_MESSAGE");
				$uibModalInstance.close();
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "ERR_SEND_MESSAGE");
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss();
		}
	}
})();


