﻿(function () {
    'use strict';

    angular.module("app").controller("CprCtrl", ctrl);

    ctrl.$inject = ['$scope', '$http', '$uibModal', '$filter', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VPCriteriaSettings', 'UIControlService', 'VPCPRDataService'];
    /* @ngInject */
    function ctrl($scope, $http, $uibModal, $filter, $translate, $translatePartialLoader, $location, SocketService, VPCriteriaSettings, UIControlService, VPCPRDataService) {
        var vm = this;
        var lang;

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.userBisaMengatur = true; //TODO
        vm.menuhome = 0;
        $scope.my_tree = {};
        vm.page_id = 135;
        vm.level = 1;
        vm.kriteria = [];
        vm.srcText = '';
        vm.Type = '';
        vm.isVhsCpr = 0;

        vm.loadKriteriaEvaluasi = loadKriteriaEvaluasi;
        function loadKriteriaEvaluasi() {
            $translatePartialLoader.addPart('vpsettings-criteria');
            lang = $translate.use();
            //getType();
            //loadAwal();
            getRole();
        }

        function getRole() {
            VPCPRDataService.cekRole(
            function (reply) {
                if (reply.status === 200) {
                    vm.roleName = reply.data.Role;
                    console.info("role: " + vm.roleName);
                    getType();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_ROLE'));
            });
        }

        function getType() {
            VPCriteriaSettings.getType(
                function (reply) {
                    if (reply.status === 200) {
                        vm.type = reply.data;
                        vm.Type = vm.type[0];
                        onChangeVhsCpr();
                    } else {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                });
        }

        /*
        vm.loadAwal = loadAwal;
        function loadAwal() {
            vm.loadKriteria();
        };
        */

        vm.cari = cari;
        function cari(srcText) {
            vm.srcText = srcText;
            vm.currentPage = 1;
            vm.loadKriteria();
        };

        vm.onChangeVhsCpr = onChangeVhsCpr;
        function onChangeVhsCpr() {
            if (vm.Type.Name == 'TYPE_CPR')
                vm.isVhsCpr = 1;
            else if (vm.Type.Name == 'TYPE_VHS')
                vm.isVhsCpr = 2;
            else if (vm.Type.Name == 'TYPE_PRAKUAL')
                vm.isVhsCpr = 3;
            else
                vm.isVhsCpr = 0;

            vm.loadKriteria();
        };

        vm.loadKriteria = loadKriteria;
        function loadKriteria() {

            vm.allowAddLevel3 = vm.Type.Name !== 'TYPE_CPR' || (vm.Type.Name === 'TYPE_CPR' && vm.roleName !== "PROCSUP");

            VPCriteriaSettings.count({
                keyword: vm.srcText,
                isVhsCpr: vm.isVhsCpr,
                level: 1,
                parentId: null
            }, function (reply) {
                if (reply.status === 200) {
                    vm.totalItems = reply.data;
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });

            vm.kriteria = [];
            //level 1
            VPCriteriaSettings.select({
                keyword: vm.srcText,
                isVhsCpr: vm.isVhsCpr,
                level: 1,
                parentId: null,
                offset: (vm.currentPage - 1) * vm.maxSize,
                limit: vm.maxSize
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    if (data.length > 0) {
                        data.forEach(function (obj) {
                            obj.children = [];
                            obj.bisaNgatur = vm.userBisaMengatur;
                            vm.kriteria.push({
                                "CriteriaId": obj.CriteriaId,
                                "CriteriaName": obj.CriteriaName,
                                "IsVhsCpr": obj.IsVhsCpr,
                                "isOptionScoreFixed": obj.isOptionScoreFixed,
                                "Level": obj.Level,
                                "IsActive": obj.IsActive,
                                "ParentId": obj.ParentId,
                                "children": obj.children,
                                "isShowAdd": true,
                                "isShowDelete": vm.roleName === "PROCSUP" || vm.roleName === "ADMIN",
                                "expanded": expandedCriteriaIds.indexOf(obj.CriteriaId) >= 0
                            });
                            vm.selectSubKriteria(obj);
                        });
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        }

        //level 2
        vm.selectSubKriteria = selectSubKriteria;
        function selectSubKriteria(object) {
            var level2 = object.Level + 1;
            var parent2 = object.CriteriaId;
            VPCriteriaSettings.select({
                keyword: "",
                level: level2,
                parentId: parent2,

                offset: 0,
                limit: 0 //no limit
            }, function (reply) {
                if (reply.status === 200) {
                    var data = reply.data;
                    if (data.length > 0) {
                        data.forEach(function (objChild) {
                            objChild.children = [];
                            objChild.bisaNgatur = vm.userBisaMengatur;
                            vm.kriteria.forEach(function (objParent) {
                                if (objChild.ParentId === objParent.CriteriaId) {
                                    objParent.children.push({
                                        "CriteriaId": objChild.CriteriaId,
                                        "CriteriaName": objChild.CriteriaName,
                                        "IsVhsCpr": objChild.IsVhsCpr,
                                        "isOptionScoreFixed": objChild.isOptionScoreFixed,
                                        "Level": objChild.Level,
                                        "IsActive": objChild.IsActive,
                                        "ParentId": objChild.ParentId,
                                        "children": objChild.children,
                                        "isShowAdd": vm.allowAddLevel3,
                                        "isShowDelete": vm.roleName === "PROCSUP" || vm.roleName === "ADMIN",
                                        "expanded": expandedCriteriaIds.indexOf(objChild.CriteriaId) >= 0
                                    });
                                    if (vm.allowAddLevel3) {
                                        var level3 = objChild.Level + 1;
                                        var parent3 = objChild.CriteriaId;
                                        VPCriteriaSettings.select({
                                            keyword: "",
                                            level: level3,
                                            parentId: parent3,
                                            offset: 0,
                                            limit: 0 //no limit
                                        }, function (reply1) {
                                            if (reply1.status === 200) {
                                                var data1 = reply1.data;
                                                if (data1.length > 0) {
                                                    data1.forEach(function (objGrndChild) {
                                                        objGrndChild.children = [];
                                                        objGrndChild.bisaNgatur = vm.userBisaMengatur;
                                                        for (var i = 0; i < vm.kriteria.length; i++) {
                                                            if (vm.kriteria[i].children.length > 0) {
                                                                for (var j = 0; j < vm.kriteria[i].children.length; j++) {
                                                                    if (objGrndChild.ParentId === vm.kriteria[i].children[j].CriteriaId) {
                                                                        vm.kriteria[i].children[j].children.push({
                                                                            "CriteriaId": objGrndChild.CriteriaId,
                                                                            "CriteriaName": objGrndChild.CriteriaName,
                                                                            "IsVhsCpr": objGrndChild.IsVhsCpr,
                                                                            "Level": objGrndChild.Level,
                                                                            "IsActive": objGrndChild.IsActive,
                                                                            "ParentId": objGrndChild.ParentId,
                                                                            "children": objGrndChild.children,
                                                                            "IsOptionScoreFixed": objGrndChild.IsOptionScoreFixed,
                                                                            "isShowDelete": vm.roleName === "PROCSUP" || vm.roleName === "ADMIN"
                                                                            //"obj": objGrndChild,
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
                }
            }, function (err) {
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD'));
            });
        };

        //tambah kriteria kpi
        vm.addKPIDescription = addKPIDescription;
        function addKPIDescription(level, id) {
            var lempar = {
                Level: level + 1,
                ParentId: id,
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpcriteria-settings/formAddKriteria.html',
                controller: 'AddKriteriaCtrl',
                controllerAs: 'AddKriteriaCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadKriteria();
            });

        };

        //tambah sub kriteria kpi
        vm.addSubKriteria = addSubKriteria;
        function addSubKriteria(data) {
            var lempar = {
                Level: data.Level + 1,
                ParentId: data.CriteriaId,
                IsVhsCpr: data.IsVhsCpr
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpcriteria-settings/formAddKriteria.html',
                controller: 'AddKriteriaCtrl',
                controllerAs: 'AddKriteriaCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadKriteria();
            });
        };

        vm.hapus = hapus
        function hapus(data, flag) {
            var pesan = "";

            if (flag == true) {
                switch (lang) {
                    case 'id': pesan = 'Anda yakin untuk menghapus Kriteria"' + data.CriteriaName + '"?'; break;
                    default: pesan = 'Are you sure want to remove "' + data.CriteriaName + '" from criteria list?'; break;
                }

            } else {
                switch (lang) {
                    case 'id': pesan = 'Anda yakin untuk menghapus Sub-Kriteria "' + data.CriteriaName + '"?'; break;
                    default: pesan = 'Are you sure want to remove "' + data.CriteriaName + '" from subcriteria list?'; break;
                }
            }
            bootbox.confirm(pesan, function (yes) {
                if (yes) {
                    //UIControlService.loadLoading(loadingMessage);
                    VPCriteriaSettings.hapus({
                        criteriaId: data.CriteriaId
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_DEL'));
                            vm.loadKriteria();
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL'));
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_DEL'));
                    });
                }
            })
        };

        vm.ubah = ubah
        function ubah(data) {
            var lempar = {
                CriteriaId: data.CriteriaId,
                IsVhsCpr: data.IsVhsCpr,
                Level: data.Level,
                CriteriaName: data.CriteriaName,
                ParentId: data.ParentId,
                isOptionScoreFixed: data.isOptionScoreFixed
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/vpcriteria-settings/formAddKriteria.html',
                controller: 'AddKriteriaCtrl',
                controllerAs: 'AddKriteriaCtrl',
                resolve: {
                    item: function () {
                        return lempar;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.loadKriteria();
            });

        };

        vm.col_defs = [
           {
               field: "obj",
               displayName: "  ",
               //cellTemplate: '<a ng-show="row.branch.Level < 3" class="btn btn-flat btn-xs btn-primary" ng-click="cellTemplateScope.click(row.branch)" title="' + (lang === 'id' ? 'Tambah nilai' : 'Add Score') + '"><i class="fa fa-plus-circle"></i>&nbsp; ' + '</a>',
               cellTemplate: '<a ng-show="row.branch.Level < 3 && row.branch.isShowAdd" class="btn btn-flat btn-xs btn-primary" ng-click="cellTemplateScope.click(row.branch)" title="' + ('row.branch.Level === 2' ? 'Add Sub Criteria' : 'Add Criteria') + '"><i class="fa fa-plus-circle"></i>&nbsp; ' + '</a>',
               cellTemplateScope: {
                   click: function (data) {
                       addSubKriteria(data);
                   }
               }
           },
           {
               field: "obj",
               displayName: "  ",
               cellTemplate: '<a ng-show="row.branch.Level <= 3" ng-click="cellTemplateScope.click(row.branch)" title="' + (lang === 'id' ? 'Ubah' : 'Edit') + '" class="btn btn-flat btn-xs btn-success"><i class="fa fa-edit"></i>&nbsp; ' + '</a>',
               cellTemplateScope: {
                   click: function (data) {         // this works too: $scope.someMethod;
                       vm.ubah(data);
                   }
               }
           },
           {
               field: "obj",
               displayName: "  ",
               cellTemplate: '<a ng-show="row.branch.Level <= 3 && row.branch.isShowDelete" class="btn btn-flat btn-xs btn-danger" ng-click="cellTemplateScope.click(row.branch)" title="' + (lang === 'id' ? 'Hapus' : 'Delete') + '"><i class="fa fa-trash-o"></i>&nbsp; ' + '</a>',
               cellTemplateScope: {
                   click: function (data) {
                       if (data.Level == 1) {
                           vm.hapus(data, true);
                       }
                       else {
                           vm.hapus(data, false);
                       }
                   }
               }
           },
        ];

        vm.expanding_property = {
            field: "CriteriaName",
            displayName: lang === 'id' ? 'KPI Description' : 'KPI Description',
            sortable: false,
            sortingType: "string",
            filterable: true
        };

        var expandedCriteriaIds = [];
        vm.onSelectTree = onSelectTree;
        function onSelectTree(crit) {
            if (crit.expanded === true) {
                expandedCriteriaIds.push(crit.CriteriaId);
            }
            else {
                for (var i = 0; i < expandedCriteriaIds.length; i++) {
                    if (expandedCriteriaIds[i] === crit.CriteriaId) {
                        expandedCriteriaIds.splice(i, 1);
                        break;
                    }
                }
            }
        }
    }
})();

