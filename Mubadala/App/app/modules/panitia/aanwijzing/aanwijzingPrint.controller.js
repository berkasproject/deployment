﻿(function () {
	'use strict';

	angular.module("app").controller("AanwijzingPrintCtrl", ctrl)

	ctrl.$inject = ['$translatePartialLoader', 'AanwijzingService', 'UIControlService', '$stateParams', '$state', '$filter']
	function ctrl($translatePartialLoader, AanwijzingService, UIControlService, $stateParams, $state, $filter) {
		var vm = this
		vm.IDTender = Number($stateParams.TenderRefID)
		vm.IDStepTender = Number($stateParams.StepID)
		vm.ProcPackType = Number($stateParams.ProcPackType)
		vm.jumlahlembar = 2
		vm.pagedData = []

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("aanwijzing")
			loadAdminPost()
			loadDataTender()
		}

		function loadAdminPost() {
			AanwijzingService.getAdminPostByStep({ ID: vm.IDStepTender }, function (reply) {
				if (reply.status === 200) {
					vm.adminPost = reply.data
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_DATA")
			});
		}

		vm.listPertanyaan = [];
		vm.jLoad = jLoad;
		function jLoad() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			AanwijzingService.getDataQuestionsAll({
				FilterType: vm.dataAturAanwijzing.ID,
			}, function (reply) {
				//console.info("dataQue:" + JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					//console.info("data" + JSON.stringify(data));
					if (data.Count > 0) {
						vm.aanwijzingStepId = data.List[0].AanwijzingStepID;
						vm.listPertanyaan = data.List;
						for (var i = 0; i < vm.listPertanyaan.length; i++) {
							vm.listPertanyaan[i].QuestionDate = UIControlService.getStrDate(vm.listPertanyaan[i].QuestionDate);
						}
						vm.totalItems = data.Count;
						vm.loadAllQuestion();
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
				UIControlService.unloadLoading();
			});
		}

		vm.daftarPertanyaan = [];
		vm.loadAllQuestion = loadAllQuestion;
		function loadAllQuestion() {
			AanwijzingService.getDataQuestionsAll({
				FilterType: vm.dataAturAanwijzing.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.daftarPertanyaan = data.List;
					for (var i = 0; i < vm.daftarPertanyaan.length; i++) {
						vm.daftarPertanyaan[i].QuestionDate = UIControlService.getStrDate(vm.daftarPertanyaan[i].QuestionDate);
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));		
				UIControlService.msg_growl("error", "MESSAGE.ERR_GETQUEST");
				UIControlService.unloadLoading();
			});
		}

		vm.backpengadaan = backpengadaan;
		function backpengadaan() {
			$state.transitionTo('aanwijzing', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, StepID: vm.IDStepTender });
		}

		function loadDataTender() {
			UIControlService.loadLoading("MESSAGE.LOADING");

			AanwijzingService.SelectVendorTender({
				StepID: vm.IDStepTender,
				ProcPackageType: vm.ProcPackType,
				TenderRefID: vm.IDTender
			}, function (reply) {
				UIControlService.unloadLoading()
				if (reply.status === 200) {
					vm.register = reply.data
					vm.jumlahlembar = 1 + Math.ceil(vm.register.length / 48)

					for (var i = 0; i < vm.jumlahlembar; i++) {
						vm.pagedData.push(i)
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});

			AanwijzingService.getDataStepTender({ ID: vm.IDStepTender }, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.TenderName = data.tender.TenderName;
					vm.isCancelled = data.tender.IsCancelled;
					vm.StartDate = UIControlService.getStrDate(data.StartDate);
					vm.EndDate = UIControlService.getStrDate(data.EndDate);
					vm.nama_tahapan = data.step.TenderStepName;
					vm.TenderID = data.TenderID;
					//console.info("tenderID:" + vm.TenderID);
					loadDataAanwijzing();

					if (!vm.isCancelled) {
						UIControlService.loadLoading("MESSAGE.LOADING");
					}

					//console.info("tender::" + JSON.stringify(data));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_TENDER");
				UIControlService.unloadLoading();
			});
		}

		vm.generateListvendor = [];
		vm.generateListvendor2 = [];
		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			//for (var i = 1; i <= vm.jumlahlembar; i++) {
			//	var indeks = i.toString()
			//	html2canvas(document.getElementById('print' + indeks), {
			//		onrendered: function (canvas) {
			//			var data = canvas.toDataURL();
			//			var docDefinition = {
			//				content: [{ image: data, width: 500 }]
			//			};

			//			pdfMake.createPdf(docDefinition).download('Pre-bid Meeting - ' + vm.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
			//		}
			//	});
			//}

			var batal = '';
			if (vm.isCancelled == true) {
				batal = $filter('translate')('KET_BATAL');
			}

			var remark = vm.dataAturAanwijzing.Remark;
			var remarkNew = '';
			if (remark != null) {
				remarkNew = remark.replace(/<br\s*[\/]?>/gi, "\n");
			}

			var adminPost = '';
			var adminPostDate = '';
			var adminPostTitle = '';
			if (vm.adminPost != null) {
				if (vm.adminPost.Title != null) {
					adminPostTitle = vm.adminPost.Title;
				}
				if (vm.adminPost.PostDate != null) {
					adminPostDate = UIControlService.convertDateTime(vm.adminPost.PostDate);
				}
				if (vm.adminPost.Post != null) {
					adminPostTitle = vm.adminPost.Post.replace(/<br\s*[\/]?>/gi, "\n");
				}
			}
			var newmap1 = {
				text: 'No.', bold: true, alignment: 'center'
			};
			var newmap2 = {
				text: $filter('translate')('FORM.VENDOR_NAME'), bold: true, alignment: 'center'

			};
			var newmap3 = {
				text: $filter('translate')('FORM.REG_DATE'), bold: true, alignment: 'center'

			};
			var newmap4 = {
				text: $filter('translate')('FORM.SET_VENDOR'), bold: true, alignment: 'center'

			};
			vm.generateListvendor.push(newmap1, newmap2, newmap3, newmap4);
			vm.generateListvendor2[0] = vm.generateListvendor;
			vm.generateListvendor = [];
			var indeks = 0;
			for (var i = 0; i < vm.register.length; i++) {
				indeks = indeks + 1;

				var newmap1 = {
					text: indeks.toString()
				};
				var newmap2 = {
					text: vm.register[i].VendorName

				};
				var newmap3 = {
					text: UIControlService.convertDateTime(vm.register[i].TenderRegistrationDate)

				};
				if (vm.register[i].IsSurvive == true) {
					var newmap4 = {
						text: 'V', alignment: 'center'

					};
				}
				else {
					var newmap4 = {
						text: ' '

					};
				}
				vm.generateListvendor.push(newmap1, newmap2, newmap3, newmap4);
				vm.generateListvendor2[indeks] = vm.generateListvendor;
				vm.generateListvendor = [];
			}
			console.info("he" + JSON.stringify(vm.generateListvendor2));
			/*
		    var reviewerPost = '';
		    var reviewerPostDate = '';
		    var reviewerPostTitle = '';
		    if (vm.reviewerPost != null) {
		        if (vm.reviewerPost.Title != null) {
		            reviewerPostTitle = vm.reviewerPost.Title;
		        }
		        if (vm.reviewerPost.PostDate != null) {
		            reviewerPostDate = UIControlService.convertDateTime(vm.reviewerPost.PostDate);
		        }
		        if (vm.reviewerPost.Post != null) {
		            reviewerPostTitle = vm.reviewerPost.Post.replace(/<br\s*[\/]?>/gi, "\n");
		        }
		    }*/
			//console.info("remarkNew" + remarkNew);
			var documentDefinition = {
				pageMargins: [25, 25, 25, 25],
				content: [
                    /* footer
                    {
                        style: 'marginAtas',
                        columns: [
                            {},
                            {
                                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAyCAYAAACQyQOIAAANoklEQVR4nO1de5AcRRn/dmfvwsMcyWWXA8UUAgEVLVFBSIDyeJSISKkl6xPKKNYKyuZub3u2HzO70xcKpaBACUgBicgjDzhCILvJlbzEKrUUTSWlsXgEBBF5JbcbAwnJZe+u/WO6Z3pf98ru3uHdV9WV25nunn785vf9uuebCYBu0agB0ajh/U4kDoc0/XyI29xw7M0hbu8wHHuP4djDIW4Lw7GLBrd2hhzrTyFu3xzi9oUQj8+RpQPAO0PAIQhTZJxD8GkOoac5hAAgoJ0K1Dg+w41DsAQAafJpg7MVBrdeDnFbTDDtCGaYAzz1wap1N8H6+sDo64OKa265E1q23Akt5cdnAQHgsoC6a7m1xODWk/rEGtwaMbg1JNOIwa2RivOOPVx+3uDW3hC3bwBC5ldcp0HGOQR1ABRy8xcWcuGr8rnwukI2vLWQjbxSyIX/VchFthVy4dUD2QVXvLrmqPmqLMx4MPBk2ODWam9yHXvYcOyicgETSeVlDW69ZWSs77rXaRg7BCQAAgAA+Vz74nw28nA+F9lfyEVE9RQeKeQiopCNDOSz4d5dG8NzVV0NaN/0NyNjXWo49qty0oYMbg1Nwh1UAsJliqKmJx4BSiMAAMA7Q/Vqf18fGPJuhoENCz5WyIazpZMdLhZy4WIhGx4uSe6xIQWIfC78n0Ju/kIAjx1mlkmhJwzHHqwHAGoxhATHG6EMOw8AFBgmfffpbuCt++DIgWzk+kI2POje5XKi1V0/agoflEB4QnMrM5AVbPtEg1tvqLu4EWDQVhgjBrdGghmWBIDJ6oaALu4Kj0a+VMhGXpYAGHLv8rEmX6ZseFi6h1fe2vCBo1X99Rvc95kZGevryjU0CghV2OFujxXGpxsCT3MIKdrOP9x+XD4X6StzAeNggBK3MVLIhod2PRLuBHDdTONGebqb9Nchx/q5unMbCgaXFQ7K378DShfo7ahontwLAHmnvtYHhxeyC3A+F96jscDw+AHgAaFYyEVEPhtGAN4SckZbAAACEIu1hBzrj81ghhLAOdYLwOnJAODpBjX5umh7cQXMyW+M/EBzA8MTcgPVQJCLPAQw05lANzXgduoUw7F3N1ovlIPB4NYApOm5JW1RS8FH2z9cyC7AHgAknU/MDVTqgnwuvOO1vrZ2/VqzBuBRs5Gxvt0sVtCvYzj2oMHT3xICQv9cf+zpO7OReD4beSyfjbynTeIk3UCFLhjM59oXA8yyQXVTYODstmboBY0Zhlu5JSCdOeDccfHbQ/1tYncuInZlO3wqPyQAuCkvXcJAdkEcYFYXjGYuRcbjc0Lc/kuTmWGkxbGHIcPFlbdeNrJnU3tx36b5QzuzHZNzASUAiIh87ujink0LxK6NR68GmGWCsU356DQ+1eDWO83SCyq1cDYC9rViyY0/Es9u+IgY7p8rdmU7xECuY1Ig2JXtEIVsePjA5nniz32nPHv9nRceJXs6qwvGNF8vfK+ZrKDSYb1MQLpXzF9OxH33LhbF/jaxd3O72Jk9ZlyAyEsA7MweI97bPG/kYH+buGXVeQeO4YnPAQBEo7NsMH6TmzyGY93ZTL2g0pxeSwSdjID0chH9xeVi+8MnipH+uWLv5nYxkHMneVe2oySpY7tzEXFg8zwx1N8mtq0/qXjJzUsFsOt/AgDQWcfnHDPFXOrksSNC3N46FczQyi0xp9cSkF4ujuxlInH7V8XW9YvEe5vni+H+uaLY3yYG++eJwf55otjf5h3ble0Qj607TXx/RbTYyi0B7Lo1AADNjov4/zE1cGn8KRlf0FS9oLODwdMC7GtFK7fEhTddKfhdF4u1958pNq39jMiu+ay4/97F4mcrvyAuX/EdccrPlgnIOMOQXi5C3H72BByb1QWHbP6S8odTwQo+O7jaIcRtAeleAfa17r8Z7qb0cnlsuYB0ZqSVW6KVW4MtaXomAMyyQV3M1wu/ngq9UA6IOb2WOKyXiTm9lpATXnKsxbGKIW6LILeuAYC6xj/MdHMpNZWaG3Ks7RIME45eakbytq0de1YXNMTUgHLrdMOx90s30XS9MAYIXHA61nPAu+fJls/qgrqbpNhgxr56ql1EBQhU0KxjDwK3zgKACbNBNBo1Ojs7Q52dnaHoOMtOpMxk6geAgCrT2TkxF6dfb6w0kXpV7a5ekMGu0wUMqh3BjB0HgHroggCMzSbjyVMr70TKTktzG4/xUSHHeq6EkqcYBAa31gLApHUBShGKML3DTLGVyRS5qqS/lRYEAEim6BUI01UI07tQilyjnyvPaxKy1EyxlQjTVT0m+WaNvMoCAADxOI0kMV2BML0dYXpTPB5vG6Nd3jkTszjCdFUS0xVJTG9DhN6qkvqdxHQFIvTWUeoaxXy9cJYKep0qvaCB8PnJ6gJFjYiw603ChJtoobu7+9ga9QUAAJYuXXoYwuwlVSZJWEKvT897NSHzTUxf9+rH7PloNNo6SnuDAADLTHMRwnTIJEwgwt41TfOYcfQxCACAMH3KJEyo8lXSsNsW+vg4h6qKeXqBdU2Vi1DBsAa3DkKaLgaAybKBe8ea5okI070mYYMmYQKl6DKAiokF5d8Rxpf4A0rfisdluL42SV7eFL1S5t2PMD3gThC+VM9TrU2JBDnJz892d3WxjvJr1CqLCH1MAuGABMOwSvL3foTpmqvVS0iTNm9/wX5wKsDg6QJuLQOAQ9IF/uSy9dpd+9fR8pqErlZ5EaZ36ec0C8h6fy/zFmUSCLP1NcoA1IERTEyf9PtCe7swXmia5mkIsU8kMP7kNSn5WmIdtIp8HpFoNxzrxWbqBW2/YB24o3lI+wXld7k/+KRTP6/63NXFOkxCB9RAJ0w31K5sUoMAAD0YL/EAQ9i7inFMwvYlCDlBz1tetl5ASBISA6hkN5m3DqLVex5Bz631fmTDdIFjvQA+rR1qZwIA7kAhQrf7E0fvcbvp9tPTEz7VCxOzZ6oPjWSOFFupAWG5xg4CpQjV69WsvkBIkR8DAMRisSOi0WhrNBptjcViLVBbrE7ClF7gFmq0i6ijLqgwXzQS7FM+261RaBCqDLJaLVQTid3d3ccizPKKZbq6WIeJaUoD2natnD65zWKEOpuMbDK4taGRYPD3C1iXe926diwIANCF8UIT03cQpiMmYSKJ3WsplZ9M0o/79E4Hqk2OxhzdGqh+AwCQwPhUhOlBVb9pki/K+ivcSv00AnsGYXq3Sehak9DViND7TEI3Ioy/cqiDVm5SL6CjDcd6pRF6QdsveADckav7c4SqQpDQLQD65JK0JhLv1suVjAVAEGG2zadneoXKgAh9Qluqrq1SR92AUGP5WJRsZk16sGqa7EiI2+fXWys0SBdU6YLbh54Uu6CaaIzFYi2I0L/5IKkQk97fSYwv0tjg5VgsdgT4q4hvaIr+nS6MF8riQf3fOgFhxMT0vyamryNM30SYvmli+ppJ2KBiu/qbrxdYvVxEiS7g1hIAaMZTxQAidIs2kfcCACBCzh/v8hJh+qA22X9PEpZEKeKYmBKUYjeYhBW9uxVTBFDiw+urEeSEJxKJ9hjGR8Xj8bbu7u55cf9TRw0wXy9skhN5SMEs2n5Bt1t/YwWP5t+XaWp/z9KlSw9DhN5SPrhlAszbnDIJ26dYxdMDZTt73iRjtg18NgjA+1Ys6qZC4hk71uDWvw9FL2j7BQ8CQLPiCyoUvzvxlPlLS1pIJBIf0vMD+AOdxDSjfHENECiNMSJ3+0RPil3gdjFqQA0gqBWMlkdPnjYBqFg+XgUAEI/H50SjUUNP0AAX65vSCxl20WSBoJXZATzR1PcUy/cAEKZDiLB3te3ee/V8etsSicThJmY7tOVlOpkkZ/Sk2AWIkM6eFDuvJ8XOS2J8jonpP8rdz2hAmMgW89QzgjL/eYQzUb3g6QLHLkKang0ATY028oBg0rMVjWtJIELO1/PpfydT9DLNpbxxdRVxW2vPQrGMqqsECJjuRYgu7ibk+J4eevIy01y0zDQXJRLkpK5U6hTlNry2l2gEasfjNJJM0o/q5Xp66Mmau2mgKb3gWI9NRC9o+wUJt54piTt0FT6hf5AT8V4Nf65MPvVjmzQg/BKgkpLlrh4kCDkBYboXYXpQ35xSexaJBDnJJGxQ6oyDcv+iPO2Ty9z79LImpo/Ldh+Q+xYH9HKqPyahqxs/lN6n+1LHeZ/oGcNNaPsFfeD2bKriDvVnBfsUGySJ++mfaiIxmSRnaL5fICR3P6ts5Wqri4f11QVo/n6ZaS7yWKi21pAPseg6WW+rrHfUx9D+cfpAY4avsscyqol+eSwg+F9hs170vqYytZE87uNgjE81CX0AYbpVW/NXPm7G+FKE6YOI0HsQYT8dIyTNrduk5yJM15kpthIRek88lTpOZejqYh0y8OVuE7NfVU1usMu6ch2ACDFNQtciTFeNUm6NFoTTBPM/0XNdLb1Qogu4dQ4ATJco5PIndBMB5njyvq9D1iZjAQAIGI71VDW9oO0X9ADAdHsfIagttUYNE5tEgGpwlEDSkuDVMQJQS9zPeINXy8s13jy9QI43HPttxQJl+wUPyV5MByaYtYaZ/8r915Qm0D7X+9I00QWz1hTz9cKNig0Mbg15H9SaZYMZY66fjUYNg1u/lfsFjYgvmLVpb/5/DXC69z7CTPwY9jSx/wHaStM+GgZv+AAAAABJRU5ErkJggg==',
                                width: 70,
                                height: 28,
                                alignment: 'right'
                            }
                        ]
                    },*/
					{
						style: 'marginAtas',
						text: vm.nama_tahapan + " ( " + vm.TypeAanwijzing + " )",
						fontSize: 14,
						bold: true,
						alignment: 'center'
					},
					{
						style: 'marginAtas',
						text: $filter('translate')('FORM.TENDERNAME') + ':',
						fontSize: 12,
						bold: false,
						alignment: 'center'
					},
					{
						text: vm.TenderName,
						fontSize: 12,
						bold: true,
						alignment: 'center'
					},
					{
						style: 'marginAtas',
						text: batal,
						fontSize: 12,
						bold: true,
						alignment: 'center'
					},
					{
						style: 'marginAtas',
						text: $filter('translate')('FORM.RANGE') + ' : ' + UIControlService.convertDateTime(vm.StartDateVendorEntry) + ' ' + $filter('translate')('FORM.TO') + ' ' + UIControlService.convertDateTime(vm.EndDateVendorEntry),
						fontSize: 10,
						bold: false,
						alignment: 'left'
					},
					{
						text: $filter('translate')('FORM.RANGE_ANSWER') + ' : ' + UIControlService.convertDateTime(vm.AnswerStartDate) + ' ' + $filter('translate')('FORM.TO') + ' ' + UIControlService.convertDateTime(vm.AnswerEndDate),
						fontSize: 10,
						bold: false,
						alignment: 'left'
					},
					{
						style: 'tableExample,marginAtas',
						table: {
							body: [
                                [{
                                	text: [{ text: $filter('translate')('BTN.SUMMARY') + '\n\n', fontSize: 12, bold: true },
										{ text: vm.dataAturAanwijzing.PostedDate + ' ', fontSize: 10, bold: false },
										{ text: vm.dataAturAanwijzing.TitleSummary + '\n', fontSize: 10, bold: true },
										{ text: remarkNew, fontSize: 8, bold: false }
                                	]
                                }],/*
                                [
                                    {
                                        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a pharetra odio.\n\nVestibulum erat mauris, sodales et consequat sit amet, ultricies vitae erat. Etiam feugiat orci justo, ultrices malesuada dui ornare ac.'
                                    }
                                ],
                                [
                                    {
                                        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a pharetra odio.\n\nVestibulum erat mauris, sodales et consequat sit amet, ultricies vitae erat. Etiam feugiat orci justo, ultrices malesuada dui ornare ac.'
                                    }
                                ],*/
							]
						},
					},
                    {
                    	style: 'tableExample', margin: [0, 10, 0, 0],
                    	table: {
                    		body: [[{
                    			text: [{ text: $filter('translate')('Informasi') + '\n\n', fontSize: 12, bold: true },
								   { text: adminPostDate + '\n ', fontSize: 8, bold: false },
								   { text: adminPostTitle + '\n', fontSize: 8, bold: false },
								   { text: adminPost, fontSize: 8, bold: false }
                    			]
                    		}, ]
                    		]
                    	},
                    },
                    {
                    	style: 'tableStyle',
                    	table:
                        {
                        	widths: ['auto', 'auto', 'auto', '*'],
                        	body: vm.generateListvendor2
                        }
                    },
				],



				styles: {
					tableHeader: {
						bold: true,
						alignment: 'center',
						fillColor: '#EAC2C2'
					},
					tFoot: {
						bold: true,
						alignment: 'right',
						fillColor: '#EAC2C2'
					},
					textRight: {
						alignment: 'right'
					},
					marginAtas: {
						margin: [0, 10, 0, 0],
					},
					marginAtas30: {
						margin: [0, 30, 0, 0],
					},
					tableStyleHeader: {
						fontSize: 10,
						margin: [0, 5, 0, 0],
						alignment: 'left'
					},
					tableStyle: {
						fontSize: 10,
						margin: [0, 10, 0, 0],
						alignment: 'left'
					},
					tableHeader2: {
						bold: true,
						alignment: 'center'
					},
					tableStyle2: {
						fontSize: 7,
						margin: [0, 10, 0, 0],
						alignment: 'left'
					},
					tableStyleMargin2: {
						fontSize: 7,
						margin: [0, 2, 0, 0],
						alignment: 'left'
					},
					tableStyleMargin100: {
						fontSize: 10,
						margin: [0, 100, 0, 0],

					},
					tableStyleFooter: {
						margin: [40, 10, 40, 'auto'],
						fontSize: 8,
						alignment: 'left'
					}
				}
			};



			pdfMake.createPdf(documentDefinition).download('Pre-bid Meeting - ' + vm.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
		}

		function loadDataAanwijzing() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			AanwijzingService.getDataAanwijzingByTender({
				TenderID: vm.TenderID,
				TenderStepID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.dataAturAanwijzing = data;

					//console.info("aan: " + JSON.stringify(data));
					if (!(data === null)) {
						vm.IsAtur = true;
						vm.StartDateVendorEntry = data.StartDateVendorEntry;
						vm.EndDateVendorEntry = data.EndDateVendorEntry;
						vm.AnswerStartDate = data.AnswerStartDate;
						vm.AnswerEndDate = data.AnswerEndDate;
						vm.TypeAanwijzing = data.TypeAaanwijzing.Value;
						//console.info(new Date(Date.parse(vm.AnswerStartDate)) + ">>" + "==" + UIControlService.getStrDate(new Date()));
						if (new Date(Date.parse(vm.AnswerStartDate)) == new Date()) {
							vm.TimeToAnswer === true;
						}
						if (!(vm.dataAturAanwijzing.PostedDate === null)) {
							vm.dataAturAanwijzing.PostedDate = UIControlService.getStrDate(vm.dataAturAanwijzing.PostedDate);
						}
						vm.jLoad(1);
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_GET_AANWIJZING");
				UIControlService.unloadLoading();
			});
		}
	}
})()