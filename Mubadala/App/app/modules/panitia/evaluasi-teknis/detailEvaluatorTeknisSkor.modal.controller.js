(function () {
    'use strict';

    angular.module("app")
    .controller("detailEvaluatorTeknisSkorCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluationTechnicalService', 'UIControlService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, EvaluationTechnicalService, UIControlService, GlobalConstantService) {

        var vm = this;
        var criteriaLv1s = [];

        vm.vendorName = item.VendorName;
        vm.evaluatorName = item.EvaluatorName;
        vm.criterias = [];
        vm.totalScores = 0;

        vm.init = init;
        function init() {
            loadData();
        };

        function loadData() {
            UIControlService.loadLoadingModal("");
            EvaluationTechnicalService.getTechnicalDetailsByEvaluatorAndVendor({
                VendorID: item.VendorID,
                TenderStepDataID: item.TenderStepDataID,
                EvaluatorID: item.EvaluatorID
            }, function (reply) {
                var techDetails = reply.data;
                EvaluationTechnicalService.selectCriterias({
                    ID: item.TenderStepDataID
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    var Teknis = reply.data;
                    var TeknisLevel1 = [];
                    var TeknisLevel2 = [];
                    var TeknisLevel3 = [];
                    for (var i = 0; i < Teknis.length; i++) {
                        for (var j = 0; j < techDetails.length; j++) {
                            if (techDetails[j].EvalMDCriteria == Teknis[i].Id) {
                                Teknis[i].EvalMDCriteria = techDetails[j].EvalMDCriteria;
                                Teknis[i].Score = techDetails[j].Score;
                                break;
                            }
                        }
                        if (Teknis[i].Level === 1) {
                            TeknisLevel1.push(Teknis[i]);
                        }
                        else if (Teknis[i].Level === 2) {
                            TeknisLevel2.push(Teknis[i]);
                        }
                        else if (Teknis[i].Level === 3) {
                            TeknisLevel3.push(Teknis[i]);
                        }
                    }
                    for (var i = 0; i < TeknisLevel2.length; i++) {
                        TeknisLevel2[i].sub = [];
                        for (var j = 0; j < TeknisLevel3.length; j++) {
                            if (TeknisLevel3[j].Parent === TeknisLevel2[i].CriteriaId) {
                                TeknisLevel2[i].sub.push(TeknisLevel3[j]);
                            }
                        }
                    }
                    for (var i = 0; i < TeknisLevel1.length; i++) {
                        TeknisLevel1[i].sub = [];
                        for (var j = 0; j < TeknisLevel2.length; j++) {
                            if (TeknisLevel2[j].Parent === TeknisLevel1[i].CriteriaId) {
                                TeknisLevel1[i].sub.push(TeknisLevel2[j]);
                            }
                        }
                    }

                    criteriaLv1s = TeknisLevel1;
                    vm.criterias = [];
                    criteriaLv1s.forEach(function (lv1) {
                        flattenCriteriaTree(lv1);
                    });
                    calculateAllScore();
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETLIST");
                });
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETSCORE");
            });
        }

        function flattenCriteriaTree(criteria) {
            vm.criterias.push(criteria);
            criteria.sub.forEach(function (sub) {
                flattenCriteriaTree(sub);
            });
        }

        vm.calculateAllScore = calculateAllScore;
        function calculateAllScore() {
            vm.totalScores = 0;
            criteriaLv1s.forEach(function (lv1) {
                vm.totalScores += calculateScore(lv1);
            });
        }
        function calculateScore(criteria) {
            if (criteria.sub && criteria.sub.length > 0) {
                var score = 0;
                criteria.sub.forEach(function (sub) {
                    score += calculateScore(sub);
                });
                criteria.Score = score;
            } else {
                if (!criteria.Score) {
                    criteria.Score = 0;
                }
            }
            criteria.WeightedScore = criteria.Score * criteria.Weight / 100;
            return criteria.WeightedScore;
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();