(function () {
    'use strict';

    angular.module("app").controller("ScoringTechnicalCtrl", ctrl);

    ctrl.$inject = ['$filter','$scope','$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DataPengadaanService',
        'EvaluationTechnicalService', 'RoleService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
    function ctrl($filter, $scope, $http, $translate, $translatePartialLoader, $location, SocketService, DataPengadaanService,
        EvaluationTechnicalService, RoleService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

        var vm = this;
        vm.ID = Number($stateParams.ID);
        vm.StepID = Number($stateParams.StepID);
        vm.TenderRefID = Number($stateParams.TenderRefID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        var criteriaLv1s = [];
        vm.criterias = [];
        vm.totalScores = 0;
        vm.scoreOptions = [1, 2, 3, 4, 5];
        vm.isTime = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('evaluasi-teknis');
            UIControlService.loadLoading("");
            //console.info("init");
            EvaluationTechnicalService.isEvaluator({
                TenderRefID: vm.TenderRefID
            }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.data === true) {
                    EvaluationTechnicalService.getEvaluatorName({
                        TenderRefID: vm.TenderRefID
                    }, function (reply) {
                        vm.evaluator = reply.data;
                        EvaluationTechnicalService.checkTime({
                            ID: vm.StepID
                        }, function (reply) {
                            vm.isTime = reply.data;
                            jLoad();
                        }, function (err) {
                            UIControlService.msg_growl('error', "MESSAGE.ERR_CHECKTIME");
                            UIControlService.unloadLoading();
                        });
                    }, function (err) {
                        UIControlService.msg_growl('error', "MESSAGE.NAME_EVAL");
                        UIControlService.unloadLoading();
                    });
                } else {
                    UIControlService.msg_growl('error', "MESSAGE.NOT_EVALUATOR" );
                    vm.back();
                }
            }, function (err) {
                UIControlService.msg_growl('error', "MESSAGE.NOT_EVALUATOR" );
                UIControlService.unloadLoading();
            });

            EvaluationTechnicalService.getVendorInfo({
                VendorID: vm.ID
            }, function (reply) {
                vm.vendorName = reply.data.VendorName;
            }, function (err) {
                UIControlService.msg_growl('error', "MESSAGE.FAIL_GETVENDOR");
            });

            DataPengadaanService.GetStepByID({ ID: vm.StepID }, function (reply) {
                vm.tenderStepData = reply.data;
            }, function (error) {
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
            });
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("");
            EvaluationTechnicalService.getTechnicalDetails({
                VendorID: vm.ID,
                TenderStepDataID: vm.StepID
            }, function (reply) {
                var techDetails = reply.data;
                EvaluationTechnicalService.selectCriterias({
                    ID: vm.StepID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        var Teknis = reply.data;
                        var TeknisLevel1 = [];
                        var TeknisLevel2 = [];
                        var TeknisLevel3 = [];
                        for (var i = 0; i < Teknis.length; i++) {
                            for (var j = 0; j < techDetails.length; j++) {
                                if (techDetails[j].EvalMDCriteria == Teknis[i].Id) {
                                    Teknis[i].EvalMDCriteria = techDetails[j].EvalMDCriteria;
                                    Teknis[i].Score = techDetails[j].Score;
                                    break;
                                }
                            }
                            if (Teknis[i].Level === 1) {
                                TeknisLevel1.push(Teknis[i]);
                            }
                            else if (Teknis[i].Level === 2) {
                                TeknisLevel2.push(Teknis[i]);
                            }
                            else if (Teknis[i].Level === 3) {
                                TeknisLevel3.push(Teknis[i]);
                            }
                        }
                        for (var i = 0; i < TeknisLevel2.length; i++) {
                            TeknisLevel2[i].sub = [];
                            for (var j = 0; j < TeknisLevel3.length; j++) {
                                if (TeknisLevel3[j].Parent === TeknisLevel2[i].CriteriaId) {
                                    TeknisLevel2[i].sub.push(TeknisLevel3[j]);
                                }
                            }
                        }
                        for (var i = 0; i < TeknisLevel1.length; i++) {
                            TeknisLevel1[i].sub = [];
                            for (var j = 0; j < TeknisLevel2.length; j++) {
                                if (TeknisLevel2[j].Parent === TeknisLevel1[i].CriteriaId) {
                                    TeknisLevel1[i].sub.push(TeknisLevel2[j]);
                                }
                            }
                        }

                        criteriaLv1s = TeknisLevel1;
                        vm.criterias = [];
                        criteriaLv1s.forEach(function (lv1) {
                            flattenCriteriaTree(lv1);
                        });
                        calculateAllScore();
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.FAIL_GETLIST");
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETLIST");
                });
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETSCORE");
                if (err.Message === "ERR_MULTIPLE_DELEGATING_EVALUATOR") {
                    UIControlService.msg_growl("error", "MESSAGE.MORETHANONE");
                    vm.back();
                }
            });

            vm.cprScore = 0;
            EvaluationTechnicalService.getCPRScore({
                VendorID: vm.ID,
                TenderStepDataID: vm.StepID
            }, function (reply) {
                vm.cprScore = reply.data;
            }, function (err) {
                UIControlService.msg_growl('error', "MESSAGE.FAIL_GETCPR_SCORE");
            });
        }
        
        function flattenCriteriaTree(criteria) {
            vm.criterias.push(criteria);
            criteria.sub.forEach(function (sub) {
                flattenCriteriaTree(sub);
            });
        }

        vm.calculateAllScore = calculateAllScore;
        function calculateAllScore() {
            vm.totalScores = 0;
            criteriaLv1s.forEach(function (lv1) {
                vm.totalScores += calculateScore(lv1);
            });
        }
        function calculateScore(criteria) {
            if (criteria.sub && criteria.sub.length > 0) {
                var score = 0;
                criteria.sub.forEach(function (sub) {
                    score += calculateScore(sub);
                });
                criteria.Score = score;
            } else {
                if (!criteria.Score) {
                    criteria.Score = 0;
                }
            }
            criteria.WeightedScore = criteria.Score * criteria.Weight / 100;
            return criteria.WeightedScore;
        }

        vm.saveScore = saveScore;
        function saveScore() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            var tech = {
                VendorID: vm.ID,
                TenderStepDataID: vm.StepID,
                Score: vm.totalScores
            }

            var detail = [];
            vm.criterias.forEach(function (crit) {
                if (!crit.sub || crit.sub.length === 0) {
                    detail.push({
                        EvalMDCriteria: crit.Id,
                        Score: crit.Score
                    });
                }
            });

            EvaluationTechnicalService.saveAllDetail({
                tech: tech,
                detail: detail
            }, function (reply) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('success', "MESSAGE.SUCC_SAVE_SCORE");
                jLoad();
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "MESSAGE.ERR_SAVE_SCORE");
            });
        }

        /*
        vm.eval = eval;
        function detail(flag) {
            if (flag === 1) {
                $state.transitionTo('detail-evaluasi-safety', { StepID: id, VendorID: vendorID });
            }
            else if (flag === 2) {
                $state.transitionTo('evaluasi-teknis-tim', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
            }
            else if (flag === 3) {
                $state.transitionTo('detail-evaluasi-safety', { StepID: id, VendorID: vendorID });
            }
            else if (flag === 4) {
                $state.transitionTo('detail-evaluasi-safety', { StepID: id, VendorID: vendorID });
            }
        }
        */
        vm.back = back;
        function back() {
            $state.transitionTo('evaluasi-teknis-tim', { TenderRefID: vm.TenderRefID, StepID: vm.StepID, ProcPackType: vm.ProcPackType });
        }
    }
})();

