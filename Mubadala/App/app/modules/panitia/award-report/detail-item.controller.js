﻿(function () {
    'use strict';

    angular.module("app").controller("DetailItemPARCtrl", ctrl);

    ctrl.$inject = ['item', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance'];
    function ctrl(item, $http, $translate, $translatePartialLoader, $location, SocketService, 
        UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance) {

        var vm = this;
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('verifikasi-data');
            vm.awardReport = item.data;
            //console.info("list" + JSON.stringify(vm.awardReport.ListItemPr));
            vm.ProcPackType = item.ProcPackType;

        }


        vm.close = close;
        function close() {
            $uibModalInstance.close();
        }


    }
})();
//TODO


