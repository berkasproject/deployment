(function () {
	'use strict';

	angular.module("app").controller("AwardReportCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', '$state', 'AwardReportService', 'UIControlService', '$filter', '$uibModal', '$stateParams', 'UploaderService', 'UploadFileConfigService', 'GlobalConstantService', '$window', 'ContractSignOffService'];
	/* @ngInject */
	function ctrl($http, $translate, $translatePartialLoader, $location, $state, AwardReportService, UIControlService, $filter, $uibModal, $stateParams, UploaderService, UploadFileConfigService, GlobalConstantService, $window,ContractSignOffService) {

		var vm = this;
		vm.init = init;
		vm.TenderStepDataID = Number($stateParams.TenderStepDataID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.VendorID = Number($stateParams.VendorID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.financialOpt = [
            { Name: "Low", Value: 4471 },
            { Name: "Medium", Value: 4472 },
            { Name: "High", Value: 4473 }
		]
		vm.FinancialRisk=null;


		vm.awardReport = [];
		vm.data = [];

		vm.paymentTermValue = 0;
		vm.negoPriceDec = 0;


		function init() {
		    $translatePartialLoader.addPart('contract-signoff');
		    $translatePartialLoader.addPart('verifikasi-data');
		    kuesioner();
		    fileConfig();
		    if (vm.ProcPackType == 4190) {
		        goodsAwardReport();
		    }
		    else if (vm.ProcPackType == 3168) {
		        vhsAwardReport();
		    }
		    else if (vm.ProcPackType == 4189) {
		        isContractApprover();
		        contractAwardReport();
		    }
		}

        
		vm.detailItem = detailItem;
		function detailItem(data,procPackType) {
		    var item = {
		        data: data,
                ProcPackType:procPackType
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/award-report/detail-item.html',
		        controller: 'DetailItemPARCtrl',
		        controllerAs: 'DetailItemPARCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		}

		vm.uploadDoc = uploadDoc;
		function uploadDoc(i) {
		    if (vm.singleDoc == undefined) {
		        UIControlService.msg_growl("err", "Tidak ada file yang dipilih");
		        return
		    }
		    else {
		        if (validateFileType(vm.singleDoc, vm.idUploadConfigs)) {
		            uploadSingleDoc(vm.singleDoc, vm.idFileSize, vm.idFileTypes, vm.tglSekarang,i);
		        }
		    }
		}

		function validateFileType(file, allowedFileTypes) {
		    if (!file || file.length == 0) {
		        UIControlService.handleRequestError("ERRORS.INVALID_FILETYPE!");
		        return false;
		    }

		    var selectedFileType = file[0].type;
		    selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('/') + 1);
		    var allowed = false;
		    for (var i = 0; i < allowedFileTypes.length; i++) {
		        if (allowedFileTypes[i].Name == selectedFileType) {
		            allowed = true;
		            return allowed;
		        }
		    }

		    if (!allowed) {
		        UIControlService.handleRequestError("ERRORS.INVALID_FILETYPE!");
		        return false;
		    }
		}

		function generateFilterStrings(allowedTypes) {
		    var filetypes = "";
		    for (var i = 0; i < allowedTypes.length; i++) {
		        filetypes += "." + allowedTypes[i].Name + ",";
		    }
		    return filetypes.substring(0, filetypes.length - 1);
		}

		function uploadSingleDoc(file, config, filters, dates,i) {
		    //console.info("doctype:" + doctype);
		    //console.info("file"+JSON.stringify(file));
		    var size = config.Size;

		    var unit = config.SizeUnitName;
		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		    }

		    //UIControlService.loadLoading("LOADING");
		    UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    UIControlService.unloadLoading();
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        vm.pathFile = url;
                        vm.kuesioner.TenderAwardQuestAnswer[i].DocUrl = vm.pathFile;
                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MESSAGE.API")
                    UIControlService.unloadLoading();
                });

		}

		function fileConfig() {
		    UploadFileConfigService.getByPageName('PAGE.VENDOR.REGISTRATION.ID', function (response) {
		        if (response.status == 200) {
		            vm.idUploadConfigs = response.data;
		            vm.idFileTypes = generateFilterStrings(response.data);
		            vm.idFileSize = vm.idUploadConfigs[0];
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.simpanKuesioner = simpanKuesioner;
		function simpanKuesioner() {
            /*
		    for (var i = 0; i <= vm.kuesioner.TenderAwardQuestAnswer.length - 1; i++) {
		        if (vm.kuesioner.TenderAwardQuestAnswer[i].Answer == null || vm.kuesioner.TenderAwardQuestAnswer[i].DocUrl == null) {
		            UIControlService.msg_growl('warning', "Semua ja");
		            return
		        }
		    }*/
		    for (var i = 0; i <= vm.kuesioner.TenderAwardVendor.length - 1; i++) {
		        if (vm.kuesioner.TenderAwardVendor[i].FinancialRisk != null) {
		            vm.kuesioner.TenderAwardVendor[i].FinancialRiskRefID = vm.kuesioner.TenderAwardVendor[i].FinancialRisk.Value;
		        }
		        if (vm.kuesioner.TenderAwardVendor[i].PaymentTermData != null) {
		            vm.kuesioner.TenderAwardVendor[i].PaymentTermID= vm.kuesioner.TenderAwardVendor[i].PaymentTermData.Id;
		        }
		        if (vm.kuesioner.TenderAwardVendor[i].IncoTerms != null) {
		            vm.kuesioner.TenderAwardVendor[i].IncoTermID = vm.kuesioner.TenderAwardVendor[i].IncoTerms.ID;
		        }
		    }
		    AwardReportService.saveQuestionnaire(vm.kuesioner, function (reply) {
		        if (reply.status == 200) {
		            UIControlService.msg_growl('success', "Data tersimpan.");
		            init();
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.kuesioner = kuesioner;
		function kuesioner() {
		    vm.kuesioner = [];
		    AwardReportService.awardQuestionnaire({
		        column: vm.TenderStepDataID,
                Status:vm.VendorID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.kuesioner = reply.data;
		            console.info("data:" + JSON.stringify(vm.kuesioner));
                    /*
		            if (vm.kuesioner.FinancialRiskRefID != null) {
		                for (var i = 0; i <= vm.financialOpt.length - 1; i++) {
		                    if (vm.kuesioner.FinancialRiskRefID == vm.financialOpt[i].Value) {
		                        vm.FinancialRisk = vm.financialOpt[i];
		                    }
		                }
		            }*/
		            for (var i = 0; i <= vm.kuesioner.TenderAwardVendor.length - 1; i++) {
		                if (vm.kuesioner.TenderAwardVendor[i].FinancialRisk != null) {
		                    vm.kuesioner.TenderAwardVendor[i].FinancialRiskRefID = vm.kuesioner.TenderAwardVendor[i].FinancialRisk.Value;
		                }
		                if (vm.kuesioner.TenderAwardVendor[i].PaymentTermData != null) {
		                    vm.kuesioner.TenderAwardVendor[i].PaymentTermID = vm.kuesioner.TenderAwardVendor[i].PaymentTermData.Id;
		                }
		                if (vm.kuesioner.TenderAwardVendor[i].IncoTerms != null) {
		                    vm.kuesioner.TenderAwardVendor[i].IncoTermID = vm.kuesioner.TenderAwardVendor[i].IncoTerms.ID;
		                }
		            }
		            if (vm.kuesioner.TenderAwardVendor != null) {
		                for (var i = 0; i <= vm.kuesioner.TenderAwardVendor.length - 1; i++) {
		                    if (vm.kuesioner.TenderAwardVendor[i].FinancialRiskRefID != null) {
		                        for (var j = 0; j <= vm.financialOpt.length - 1; j++) {
		                            if (vm.kuesioner.TenderAwardVendor[i].FinancialRiskRefID == vm.financialOpt[j].Value) {
		                                vm.kuesioner.TenderAwardVendor[i].FinancialRisk = vm.financialOpt[j];
		                            }
		                        }
		                    }
		                    if (vm.kuesioner.TenderAwardVendor[i].PaymentTermID != null) {
		                        for (var j = 0; j <= vm.kuesioner.TenderAwardVendor[i].PaymentTermList.length - 1; j++) {
		                            if (vm.kuesioner.TenderAwardVendor[i].PaymentTermID == vm.kuesioner.TenderAwardVendor[i].PaymentTermList[j].Id) {
		                                vm.kuesioner.TenderAwardVendor[i].PaymentTermData = vm.kuesioner.TenderAwardVendor[i].PaymentTermList[j];
		                            }
		                        }
		                    }
		                    if (vm.kuesioner.TenderAwardVendor[i].IncoTermID != null) {
		                        for (var j = 0; j <= vm.kuesioner.TenderAwardVendor[i].IncoTermList.length - 1; j++) {
		                            if (vm.kuesioner.TenderAwardVendor[i].IncoTermID == vm.kuesioner.TenderAwardVendor[i].IncoTermList[j].ID) {
		                                vm.kuesioner.TenderAwardVendor[i].IncoTerms = vm.kuesioner.TenderAwardVendor[i].IncoTermList[j];
		                            }
		                        }
		                    }
		                }
		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

        vm.deleteDoc=deleteDoc
		function deleteDoc(i) {
		    vm.kuesioner.TenderAwardQuestAnswer[i].DocUrl = null;
		}


		vm.goodsAwardReport = goodsAwardReport;
		function goodsAwardReport() {
		    AwardReportService.goodsAwardReport({
		        column: vm.TenderStepDataID,
		        Status: vm.ProcPackType,
                Offset:vm.VendorID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.awardReport = reply.data;
		            console.info("report:" + JSON.stringify(vm.awardReport));
		            if (vm.awardReport.RFQ.PaymentTermDetail != null) {
		                vm.paymentTermValue = (vm.awardReport.RFQ.PaymentTermDetail.Name).replace(/[^\d.]/g, '');
		            }
		            vm.negoPriceDec = (vm.awardReport.Negosiasi.NegotiationPriceUSD).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.vhsAwardReport = vhsAwardReport;
		function vhsAwardReport() {
		    AwardReportService.vhsAwardReport({
		        column: vm.TenderStepDataID,
		        Status: vm.ProcPackType,
		        Offset: vm.VendorID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.awardReport = reply.data;
		            //console.info("data:" + JSON.stringify(vm.awardReport));
		            if (vm.awardReport.RFQ.PaymentTermDetail != null) {
		                vm.paymentTermValue = (vm.awardReport.RFQ.PaymentTermDetail.Name).replace(/[^\d.]/g, '');
		            }
		            vm.negoPriceDec = (vm.awardReport.Negosiasi.NegotiationPriceUSD).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}


		vm.contractAwardReport = contractAwardReport;
		function contractAwardReport() {
		    AwardReportService.contractAwardReport({
		        column: vm.TenderStepDataID,
		        Status: vm.ProcPackType,
		        Offset: vm.VendorID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.awardReport = reply.data;
		            console.info("data:" + JSON.stringify(vm.awardReport));
                    /*
		            if (vm.awardReport.RFQ.PaymentTermDetail != null) {
		                vm.paymentTermValue = (vm.awardReport.RFQ.PaymentTermDetail.Name).replace(/[^\d.]/g, '');
		            }*/
		            vm.negoPriceDec = (vm.awardReport.Negosiasi.NegotiationPriceUSD).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.isContractApprover = isContractApprover;
		function isContractApprover() {
		    AwardReportService.isContractApprover({
		        column: vm.TenderStepDataID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.isApprover = reply.data;
		            console.info("data:" + JSON.stringify(vm.isApprover));
		            /*
		            if (vm.awardReport.RFQ.PaymentTermDetail != null) {
		                vm.paymentTermValue = (vm.awardReport.RFQ.PaymentTermDetail.Name).replace(/[^\d.]/g, '');
		            }*/
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.msg_growl("error", "MESSAGE.FAIL_MODIFY");
		        UIControlService.unloadLoading();
		    });
		}

		vm.reject = reject;
		function reject() {
		    console.info("id" + vm.isApprover.Limit);
		    console.info("flag" + vm.isApprover.column);
		    var item = {
		        ID: vm.isApprover.Limit,
		        flag: vm.isApprover.column,
		        Status: 1
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/panitia/contractSignOff/detailApproval.modal.html?v=1.000002',
		        controller: 'detailApprovalSignOffCtrl',
		        controllerAs: 'detailApprovalSignOffCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        init();
		    });
		};

		vm.approve = approve;
		function approve() {
		    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVE_CONTRACT_SIGNOFF'), function (yes) {
		        if (yes) {
		            ContractSignOffService.SendApproval({
		                ID: vm.isApprover.Limit,
		                Status: 1,
		                flagEmp: vm.isApprover.column
		            }, function (reply) {
		                if (reply.status === 200) {
		                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_APPROVAL'));
		                    sendMailApproval();
		                    init();
		                } else {
		                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
		                }
		            }, function (error) {
		                UIControlService.unloadLoadingModal();
		                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_APPROVAL'));
		            });
		        }
		    });
		}

		vm.sendMailApproval = sendMailApproval;
		function sendMailApproval() {
		    ContractSignOffService.SendEmail({
		        ID: vm.isApprover.Limit,
		        Status: 1
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) vm.init();
		    }, function (error) {
		        UIControlService.unloadLoadingModal();
		    });
		}

		vm.back=back;
		function back() {
            /*
		    if (vm.ProcPackType == 4190) {
		        $state.transitionTo('penetapan-pemenang-barang', { TenderRefID: vm.awardReport.Tender.TenderRefID, StepID: vm.TenderStepDataID, ProcPackType: vm.ProcPackType });
		    }
		    else if (vm.ProcPackType == 3168) {
		        $state.transitionTo('penetapan-pemenang-vhs-fpa', { TenderRefID: vm.awardReport.Tender.TenderRefID, StepID: vm.TenderStepDataID, ProcPackType: vm.ProcPackType });
		    }
		    else if (vm.ProcPackType == 4189) {
		        $state.transitionTo('penandatanganan-kontrak', { TenderRefID: vm.awardReport.Tender.TenderRefID, StepID: vm.TenderStepDataID, ProcPackType: vm.ProcPackType });
		    }*/
		    $window.history.back();
		}


	}
})();
