﻿(function () {
    'use strict';

    angular.module("app").controller("BusinessFieldAssPrequalCtrl", ctrl);

    ctrl.$inject = ['$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', 'AdministrasiPrequalService', 'DataAdministrasiService', 'VendorRegistrationService', 'UploadFileConfigService', 'UploaderService', 'AssessmentPrequalService'];
    function ctrl($timeout, $http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, AdministrasiPrequalService, DataAdministrasiService, VendorRegistrationService, UploadFileConfigService, UploaderService, AssessmentPrequalService) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
        vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
        vm.VEPID = Number($stateParams.VEPID);

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.init = init;
        vm.NegoId = 0;
        vm.IsSubmit = null;
        vm.listPersonal = [];
        vm.isCalendarOpened = [false, false, false, false];
        vm.administrasiDate = {};
        vm.listCurrFalse = [];
        vm.listPersFalse = [];
        vm.IsEdit = false;
        vm.IsEditAlter = false;
        vm.addressFlag = 0;
        vm.ContactName = "";
        vm.addressFlagAlternatif = 0;
        vm.listOfficeAddress = {};
        vm.listOfficeAddressAlternatif = {};
        vm.AddressAlterId = 0;
        vm.listCompanyContact = {};

        function init() {
            vm.flagSubmit = true;
            $translatePartialLoader.addPart("data-administrasi");
            $translatePartialLoader.addPart("pemasukkan-prakualifikasi");
            $translatePartialLoader.addPart("assessment-prakualifikasi");
            UIControlService.loadLoading("MESSAGE.LOADING");
            loadSetupStep();
        }

        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID,
                Offset: vm.VEPID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isEntry = reply.data.IsNotStarted;
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID;
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    vm.remark = reply.data.Remark;
                    console.info("isrev?" + vm.IsNeedRevision);
                    loadVendorContactPrequal();
                    load();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.load = load;
        function load() {
            AssessmentPrequalService.loadCommodityPrequal({
        VendorPrequalID: vm.VendorPrequalID,
                PrequalSetupStepID: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listBussinesDetailField = reply.data;
                    }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
        });
        }



        vm.loadBusinessField = loadBusinessField;
        vm.selectedBusinessField;
        vm.listBusinessField = [];
        function loadBusinessField() {
            AssessmentPrequalService.SelectBusinessFieldPrequal({
                Status: vm.VendorPrequalID
            },
			   function (response) {
			       if (response.status === 200) {
			           vm.listBusinessField = response.data;
			       }
			       else {
			           return;
			       }
			   }, function (err) {
			       return;
			   });
        }

        vm.loadVendorContactPrequal = loadVendorContactPrequal;
        function loadVendorContactPrequal() {
           AssessmentPrequalService.loadContactPrequal({
               PrequalSetupStepID: vm.PrequalStepID,
               VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listContact = reply.data;
                    vm.listContact.forEach(function (contact) {
                        if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.administrasi = contact.Vendor;
                            loadTypeVendor(vm.administrasi);
                        }
                        UIControlService.unloadLoading();
                    });
                }
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.selectedTypeVendor;
        vm.listTypeVendor;
        function loadTypeVendor(data) {
            DataAdministrasiService.getTypeVendor(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listTypeVendor = reply.data.List;
                    if (data !== undefined) {
                        for (var i = 0; i < vm.listTypeVendor.length; i++) {
                            if (data.VendorTypeID === vm.listTypeVendor[i].RefID) {
                                vm.selectedTypeVendor = vm.listTypeVendor[i];
                                changeTypeVendor(vm.administrasi);
                                break;
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.changeTypeVendor = changeTypeVendor;
        function changeTypeVendor(data) {
            if (vm.selectedTypeVendor !== undefined) {
                if (vm.selectedTypeVendor.Value === "VENDOR_TYPE_SERVICE") {
                    vm.disablePemasok = true;
                    vm.listSupplier = {};
                    //console.info("dpm:" + JSON.stringify(vm.disablePemasok));
                }
                vm.GoodsOrService = vm.selectedTypeVendor.RefID;
                loadBusinessField();
                vm.listComodity = [];
            }

        }

        vm.changeBussinesField = changeBussinesField;
        function changeBussinesField() {
            if (vm.selectedBusinessField != undefined) {
                if (vm.selectedBusinessField.Name != "Lain - lain")
                    vm.loadComodity();
            }

        }

        vm.loadComodity = loadComodity;
        vm.selectedComodity;
        vm.listComodity = [];
        function loadComodity() {
            //console.info("bidang usaha goodsorservice" + JSON.stringify(vm.selectedBusinessField.GoodsOrService));
            if (vm.selectedBusinessField.GoodsOrService === 3091) {
                UIControlService.msg_growl("success", "MESSAGE.COMMODITY");
                vm.listComodity = [];
            } else {
                DataAdministrasiService.SelectComodity({ ID: vm.selectedBusinessField.ID },
				   function (response) {
				       //console.info("xx"+JSON.stringify(response));
				       if (response.status === 200 && response.data.length > 0) {
				           vm.listComodity = response.data;
				       } else if (response.status === 200 && response.data.length < 1) {
				           UIControlService.msg_growl("success", "MESSAGE.COMMODITY");
				           vm.listComodity = [];
				       } else {
				           return;
				       }
				   }, function (err) {
				       return;
				   });
            }
        }

        vm.selectedSupplier;
        vm.listSupplier;
        function loadSupplier(data) {
            DataAdministrasiService.getSupplier(function (reply) {
                UIControlService.unloadLoading();
                //console.info("PMS:"+JSON.stringify(reply));
                if (reply.status === 200) {
                    vm.listSupplier = reply.data.List;
                    if (data) {
                        for (var i = 0; i < vm.listSupplier.length; i++) {
                            if (data.SupplierID === vm.listSupplier[i].RefID) {
                                vm.selectedSupplier = vm.listSupplier[i];
                                break;
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCurrency = loadCurrency;
        function loadCurrency() {
            UIControlService.loadLoading("MESSAGE.LOADING_CURRENCY");
            VendorRegistrationService.getCurrencies(
                function (response) {
                    vm.currencyList = response.data;
                    UIControlService.unloadLoading();
                },
                handleRequestError);
        }

        function handleRequestError(response) {
            UIControlService.log(response);
            //UIControlService.handleRequestError(response.data, response.status);
            UIControlService.unloadLoading();
        }

        vm.addCurrency = addCurrency;
        function addCurrency() {
            vm.flagCurr = false;
            for (var i = 0; i < vm.listCurrencies.length; i++) {
                if (vm.Currency.CurrencyID == vm.listCurrencies[i].MstCurrency.CurrencyID && vm.listCurrencies[i].IsActive == true) { vm.flagCurr = true; }
            }
            if (vm.flagCurr == false) {
                vm.listCurrencies.push({
                    ID: 0,
                    CurrencyID: vm.Currency.CurrencyID,
                    VendorID: vm.administrasi.VendorID,
                    MstCurrency: vm.Currency,
                    IsActive: true
                });
            }
            else {
                UIControlService.msg_growl("warning", "MESSAGE.ERR_ADD_CURR");
                return;

            }
        }

        vm.addDetailBussinesField = addDetailBussinesField;
        function addDetailBussinesField() {
            if (vm.selectedBusinessField === undefined) {
                UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD");
                return;
            }

            var comID;
            if (vm.selectedComodity === undefined) {
                //UIControlService.msg_growl("warning", "Komoditas Belum di Pilih");
                //return;
                comID = null;
            } else if (!(vm.selectedComodity === undefined)) {
                comID = vm.selectedComodity.ID;
            }
            countDetailBusinessField = [];
            for (var i = 0; i < vm.listBusinessField.length; i++) {
                if (vm.listBusinessField[i].Name != "Lain - lain")
                    countDetailBusinessField.push(vm.listBusinessField[i]);
            }
            var countDetailBusinessField = vm.listBussinesDetailField.length;
            var addPermission = false; var sameItem = true;
            //console.info("countDetail" + JSON.stringify(countDetailBusinessField));
            var dataDetail = {
                VendorID: vm.administrasi.VendorID,
                CommodityID: comID,
                BusinessFieldID: vm.selectedBusinessField.ID,
                Commodity: vm.selectedComodity,
                BusinessField: vm.selectedBusinessField,
                Remark: vm.RemarkBusinessField
            }
            if (vm.selectedBusinessField.Name == "Lain - lain") {
                vm.listBussinesDetailField.push(dataDetail);
                savedata();
            }
            else {

                if (countDetailBusinessField <= 6) {
                    var countGoodsDetail = 0; var countServiceDetail = 0;
                    for (var a = 0; a < vm.listBussinesDetailField.length; a++) {

                        if (vm.listBusinessField[a].Name != "Lain - lain") {
                            if (countDetailBusinessField > 0) {
                                if (dataDetail.BusinessField.GoodsOrService === 3091) {
                                    if (vm.listBussinesDetailField[a].BusinessField.GoodsOrService === 3091) {
                                        if (vm.listBussinesDetailField[a].BusinessFieldID !== dataDetail.BusinessFieldID) {
                                            countServiceDetail = +countServiceDetail + 1;
                                        }
                                        else {
                                            UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD_SERVICE");
                                            sameItem = false;
                                        }
                                    }
                                }
                                else if (dataDetail.BusinessField.GoodsOrService === 3090) {
                                    if (vm.listBussinesDetailField[a].BusinessField.GoodsOrService === 3090) {
                                        if (vm.listBussinesDetailField[a].CommodityID !== dataDetail.CommodityID) {
                                            countGoodsDetail = +countGoodsDetail + 1;
                                        }
                                        else {
                                            UIControlService.msg_growl("warning", "MESSAGE.COMMODITY2");
                                            sameItem = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //barang & jasa
                    if (vm.selectedBusinessField.Name != "Lain - lain") {
                        if (vm.GoodsOrService === 3092) {
                            if (dataDetail.BusinessField.GoodsOrService === 3090) {
                                if (countGoodsDetail < 3) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT1");
                                }
                            }
                            else if (dataDetail.BusinessField.GoodsOrService === 3091) {
                                if (countServiceDetail < 5) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT2");
                                }
                            }

                            if (countDetailBusinessField === 6) {
                                UIControlService.msg_growl("warning", "MESSAGE.LIMIT3");
                                addPermission = false;
                            }
                        }
                            //barang
                        else if (vm.GoodsOrService === 3090) {
                            if (dataDetail.BusinessField.GoodsOrService === 3090) {
                                if (countGoodsDetail < 3) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT4");
                                }
                            }
                        }

                            //jasa
                        else if (vm.GoodsOrService === 3091) {
                            if (dataDetail.BusinessField.GoodsOrService === 3091) {
                                if (countServiceDetail < 5) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "MESSAGE.LIMIT5");
                                }
                            }
                        }
                    }
                    else {
                        vm.listBussinesDetailField.push(dataDetail);
                        savedata();
                    }


                }
                if (addPermission === true && sameItem === true) {
                    vm.listBussinesDetailField.push(dataDetail);
                    savedata();
                }
            }

            vm.selectedComodity = undefined;

        }

        vm.deleteRow = deleteRow;
        function deleteRow(index) {
            var idx = index - 1;
            var _length = vm.listBussinesDetailField.length; // panjangSemula
            vm.listBussinesDetailField.splice(idx, 1);
        };

        vm.deleteRowCurr = deleteRowCurr;
        function deleteRowCurr(index, data) {
            if (data.ID != undefined) {
                data.IsActive = false;
                vm.listCurrFalse.push(data);
            }
            var idx = index;
            var _length = vm.listCurrencies.length; // panjangSemula
            vm.listCurrencies.splice(idx, 1);
        };

        vm.deleteRowPers = deleteRowPers;
        function deleteRowPers(index, data) {
            console.info(data);
            if (data.ContactID != 0) {
                data.IsActive = false;
                vm.listPersFalse.push(data);
            }
            var idx = index;
            var _length = vm.listPersonal.length; // panjangSemula
            vm.listPersonal.splice(idx, 1);
        };

        vm.CheckEmail = CheckEmail;
        function CheckEmail() {
            UIControlService.loadLoading("Check Email . . .");
            if (vm.EmailPers !== '') {
                var data = {
                    Keyword: vm.EmailPers
                };
                DataAdministrasiService.checkEmail(data,
                        function (response) {
                            vm.EmailAvailable = response.data;
                            if (vm.EmailAvailable) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');

                                vm.NamePers = undefined;
                                vm.PhonePers = undefined;
                                vm.EmailPers = undefined;
                            }
                            else {
                                vm.addContactPers();
                            }
                        }, handleRequestError);
            }
        }

        vm.addContactPers = addContactPers;
        function addContactPers() {
            vm.listPersonal.push({

                Contact: {
                    ContactID: 0,
                    Name: vm.NamePers,
                    Phone: vm.PhonePers,
                    Email: vm.EmailPers
                },
                IsActive: true
            });
            vm.NamePers = undefined;
            vm.PhonePers = undefined;
            vm.EmailPers = undefined;

            UIControlService.unloadLoading();
        }

        vm.editcontact = editcontact;
        function editcontact(data) {
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-administrasi/DetailContact.html',
                controller: 'DetailContactAdministrasiPrequalCtrl',
                controllerAs: 'DetailContactAdministrasiPrequalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                console.info(data);
                vm.listPers = [];
                vm.listPers = vm.listPersonal;
                vm.listPersonal = [];
                for (var i = 0; i < vm.listPers.length; i++) {
                    if (vm.listPers[i].ContactID === data.ContactID) {
                        var aish = {
                            VendorContactType: vm.listPers[i].VendorContactType,
                            ContactID: vm.listPers[i].ContactID,
                            VendorID: vm.listPers[i].VendorID,
                            Contact: {
                                ContactID: data.ContactID,
                                Name: data.Name,
                                Email: data.Email,
                                Phone: data.Phone
                            },
                            IsActive: true,
                            IsEdit: true
                        }
                        vm.listPersonal.push(aish);
                    }
                    else {
                        vm.listPersonal.push(vm.listPers[i]);
                    }
                }
            });
        }

        vm.CheckAddress = CheckAddress;
        function CheckAddress(flag) {
            if (flag == true) {
                if (vm.address1 !== vm.cekAddress) vm.IsEdit = true;
            }
            else {
                if (vm.addressinfo !== vm.cekAddress1) vm.IsEditAlter = true;
            }
        }

        vm.loadRegion = loadRegion;
        vm.selectedRegion;
        vm.listRegion = [];
        function loadRegion(countryID) {
            DataAdministrasiService.SelectRegion({
                CountryID: countryID
            }, function (response) {
                vm.listRegion = response.data;
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadCountry = loadCountry;
        vm.selectedCountry;
        vm.listCountry = [];
        function loadCountry(data) {
            DataAdministrasiService.SelectCountry(function (response) {
                vm.listCountry = response.data;
                for (var i = 0; i < vm.listCountry.length; i++) {
                    if (data.CountryID === vm.listCountry[i].CountryID) {
                        vm.selectedCountry = vm.listCountry[i];
                        loadState(data);
                        break;
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadState = loadState;
        vm.selectedState;
        vm.listState = [];
        function loadState(data) {
            if (!data) {
                data = vm.selectedCountry;
                vm.selectedState = "";
                vm.selectedCity = "";
                vm.selectedDistrict = "";
                vm.selectedState1 = "";
            }
            loadRegion(data.CountryID);

            DataAdministrasiService.SelectState(data.CountryID, function (response) {
                vm.listState = response.data;
                for (var i = 0; i < vm.listState.length; i++) {
                    if (vm.selectedState1 !== "" && vm.selectedState1.StateID === vm.listState[i].StateID) {
                        vm.selectedState = vm.listState[i];
                        if (vm.selectedState.Country.Code === 'IDN') {
                            loadCity(vm.selectedState);
                            break;
                        }
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadCity = loadCity;
        vm.selectedCity;
        vm.listCity = [];
        function loadCity(data) {
            if (!data) {

                data = vm.selectedState;
                vm.selectedCity = "";
                vm.selectedCity1 = "";
                vm.selectedDistrict = "";
            }
            DataAdministrasiService.SelectCity(data.StateID, function (response) {
                vm.listCity = response.data;
                for (var i = 0; i < vm.listCity.length; i++) {
                    if (vm.selectedCity1 !== "" && vm.selectedCity1.CityID === vm.listCity[i].CityID) {
                        vm.selectedCity = vm.listCity[i];
                        if (vm.selectedState.Country.Code === 'IDN') {
                            loadDistrict(vm.selectedCity);
                            break;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadDistrict = loadDistrict;
        vm.selectedDistrict;
        vm.listDistrict = [];
        function loadDistrict(city) {
            if (!city) {
                city = vm.selectedCity;
                vm.selectedDistrict = "";
                vm.selectedDistrict1 = "";

            }
            DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
                vm.listDistrict = response.data;
                for (var i = 0; i < vm.listDistrict.length; i++) {
                    if (vm.selectedDistrict1 !== "" && vm.selectedDistrict1.DistrictID === vm.listDistrict[i].DistrictID) {
                        vm.selectedDistrict = vm.listDistrict[i];
                        break;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadAssociation = loadAssociation;
        vm.selectedAssociation;
        vm.listAssociation = [];
        function loadAssociation(data) {
            DataAdministrasiService.getAssociation({
                Offset: 0,
                Limit: 0,
                Keyword: ""
            },
			function (response) {
			    if (response.status === 200) {
			        vm.listAssociation = response.data.List;
			        for (var i = 0; i < vm.listAssociation.length; i++) {
			            if (data.AssociationID === vm.listAssociation[i].AssosiationID) {
			                vm.selectedAssociation = vm.listAssociation[i];
			                break;
			            }
			        }
			    } else {
			        return;
			    }
			}, function (err) {
			    return;
			});
        }

        vm.loadRegionAlternatif = loadRegionAlternatif;
        vm.selectedRegionAlternatif;
        vm.listRegionAlternatif = [];
        function loadRegionAlternatif(countryID) {
            DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
                vm.listRegionAlternatif = response.data;
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadCountryAlternatif = loadCountryAlternatif;
        vm.selectedCountryAlternatif;
        vm.listCountryAlternatif = [];
        function loadCountryAlternatif(data) {
            DataAdministrasiService.SelectCountry(function (response) {
                vm.listCountryAlternatif = response.data;
                for (var i = 0; i < vm.listCountryAlternatif.length; i++) {
                    if (data !== undefined) {
                        if (data.CountryID === vm.listCountryAlternatif[i].CountryID) {
                            vm.selectedCountryAlternatif = vm.listCountryAlternatif[i];
                            loadStateAlternatif(data);
                            break;
                        }

                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadStateAlternatif = loadStateAlternatif;
        vm.selectedStateAlternatif;
        vm.listStateAlternatif = [];
        function loadStateAlternatif(data) {
            if (!data) {
                data = vm.selectedCountryAlternatif;
                vm.selectedStateAlternatif = "";
                vm.selectedCityAlternatif = "";
                vm.selectedDistrictAlternatif = "";
                vm.selectedStateAlternatif1 = "";
            }
            loadRegionAlternatif(data.CountryID);

            DataAdministrasiService.SelectState(data.CountryID, function (response) {
                vm.listStateAlternatif = response.data;
                for (var i = 0; i < vm.listStateAlternatif.length; i++) {
                    if (vm.selectedStateAlternatif1 !== "" && vm.selectedStateAlternatif1.StateID === vm.listStateAlternatif[i].StateID) {
                        vm.selectedStateAlternatif = vm.listStateAlternatif[i];
                        if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
                            loadCityAlternatif(vm.selectedStateAlternatif);
                            break;
                        }
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadCityAlternatif = loadCityAlternatif;
        vm.selectedCityAlternatif;
        vm.listCityAlternatif = [];
        function loadCityAlternatif(data) {
            if (!data) {

                data = vm.selectedStateAlternatif;
                vm.selectedCityAlternatif = "";
                vm.selectedCityAlternatif1 = "";
                vm.selectedDistrictAlternatif = "";
            }
            DataAdministrasiService.SelectCity(data.StateID, function (response) {
                vm.listCityAlternatif = response.data;
                for (var i = 0; i < vm.listCityAlternatif.length; i++) {
                    if (vm.selectedCityAlternatif1 !== "" && vm.selectedCityAlternatif1.CityID === vm.listCityAlternatif[i].CityID) {
                        vm.selectedCityAlternatif = vm.listCityAlternatif[i];
                        if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
                            loadDistrictAlternatif(vm.selectedCityAlternatif);
                            break;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadDistrictAlternatif = loadDistrictAlternatif;
        vm.selectedDistrictAlternatif;
        vm.listDistrictAlternatif = [];
        function loadDistrictAlternatif(city) {
            if (!city) {
                city = vm.selectedCityAlternatif;
                vm.selectedDistrictAlternatif = "";
                vm.selectedDistrictAlternatif1 = "";

            }
            DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
                vm.listDistrictAlternatif = response.data;
                for (var i = 0; i < vm.listDistrictAlternatif.length; i++) {
                    if (vm.selectedDistrictAlternatif1 !== "" && vm.selectedDistrictAlternatif1.DistrictID === vm.listDistrictAlternatif[i].DistrictID) {
                        vm.selectedDistrictAlternatif = vm.listDistrictAlternatif[i];
                        break;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });
        }

        vm.loadBusinessEntity = loadBusinessEntity;
        vm.selectedBusinessEntity;
        vm.listBusinessEntity = [];
        function loadBusinessEntity() {
            console.info(vm.administrasi);
            DataAdministrasiService.SelectBusinessEntity(function (response) {
                if (response.status === 200) {
                    vm.listBusinessEntity = response.data;
                    for (var i = 0; i < vm.listBusinessEntity.length; i++) {
                        if (vm.administrasi.BusinessID === vm.listBusinessEntity[i].BusinessID) {
                            vm.selectedBusinessEntity = vm.listBusinessEntity[i];
                            break;
                        }
                    }
                } else {
                    return;
                }
            }, function (err) {
                return;
            });
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                vm.PKPUrl = vm.administrasi.PKPUrl;
                if (vm.fileUploadNPWP === undefined) {
                    vm.NpwpUrl = vm.administrasi.NpwpUrl;
                    savedata();
                }
                else {
                    if (vm.fileUploadNPWP !== null) {
                        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
                    }
                    else {
                        vm.NpwpUrl = vm.administrasi.NpwpUrl;
                        savedata();
                    }
                }
            }
            else {
                if (vm.fileUpload !== null) {
                    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
                }
                else {
                    vm.PKPUrl = vm.administrasi.PKPUrl;
                    if (vm.fileUploadNPWP === undefined) {
                        vm.NpwpUrl = vm.administrasi.NpwpUrl;
                        savedata();
                    }
                    else {
                        if (vm.fileUploadNPWP !== null) {
                            upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
                        }
                        else {
                            vm.NpwpUrl = vm.administrasi.NpwpUrl;
                            savedata();
                        }
                    }
                }
            }

        }

        vm.validateFileType = validateFileType;
        function validateFileType(administrasi, flag, file, allowedFileTypes) {
            if (flag == true) {
                if (!file && administrasi.PKPUrl === "") {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                    return false;
                }
            }
            else {
                if (!file && administrasi.NpwpUrl === "") {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                    return false;
                }
            }
            return true;
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            //console.info("masuk"+JSON.stringify(vm.administrasi.PKPUrl));
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            //if (vm.administrasi.PKPUrl === null) {
            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFileSPPKP(vm.administrasi.VendorID, file, size, filters, function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    vm.pathFile = url;
                    vm.name = response.data.FileName;
                    var s = response.data.FileLength;
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s)
                    }
                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }
                    vm.PKPUrl = vm.pathFile;
                    if (vm.fileUploadNPWP === undefined) {
                        vm.NpwpUrl = vm.administrasi.NpwpUrl;
                        savedata();
                    }
                    else {
                        if (vm.fileUploadNPWP !== null) {
                            upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
                        }
                        else {
                            vm.NpwpUrl = vm.administrasi.NpwpUrl;
                            savedata();
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (response) {
                if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
                    UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
                    UIControlService.unloadLoading();
                }
            });
            //} end if
        }

        vm.upload1 = upload1;
        function upload1(file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            console.info(config);
            UIControlService.loadLoading("MESSAGE.LOADING_UPLOAD_FILE");
            UploaderService.uploadRegistration(file, vm.administrasi.Npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    vm.pathFile1 = url;
                    vm.name1 = response.data.FileName;
                    var s = response.data.FileLength;
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s)
                    }
                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }
                    vm.NpwpUrl = vm.pathFile1;
                    savedata();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (response) {
                if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
                    UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
                    UIControlService.unloadLoading();
                }
            });
        }
        vm.savedata = savedata;
        function savedata() {
            vm.cek = 0;
            if (vm.listBussinesDetailField.length === 0) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_BUSINESSFIELD");
                return;
            }
            if (vm.cek === 0) {
                addtolist(vm.VendorTypeID, vm.SupplierID);
            }
        }

        vm.addtolist = addtolist;
        vm.vendor = {};
        vm.listcontact = [];
        function addtolist(data1, data2) {
            vm.insertdata = {
                VendorCommodity: vm.listBussinesDetailField,
                VendorPrequal: {
                    VendorID: vm.administrasi.VendorID,
                },
                PrequalStepID: vm.PrequalStepID
            };
            AssessmentPrequalService.InsertBusinessField(vm.insertdata, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "SUCCESS_BUSINESSFIELD");
                    init();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });



        }

        vm.deleteBusinessField = deleteBusinessField;
        function deleteBusinessField(data) {
            AssessmentPrequalService.deleteBusinessField(data, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCC_DEL");
                    init();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });
        }

        vm.valid = true;
        vm.notvalid = function notvalid() {
            vm.valid = false;
        }

        vm.Submit = Submit;
        function Submit(flag) {
            vm.valid = flag;
            if (vm.remark == undefined) {
                vm.remark = "";
            }
            AssessmentPrequalService.InsertSubmit({
                PrequalStepID: vm.PrequalStepAssID,
                VendorPrequalID: vm.VendorPrequalID,
                IsAgree: flag,
                VEPID: vm.VEPID,
                Remark: vm.remark
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }



    }
})();
//TODO


