﻿(function () {
    'use strict';

    angular.module("app").controller("SuratPernyataanModalAssCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$stateParams', '$location', 'SocketService', 'VerifiedSendService', 'SrtPernyataanPrequalService',
        '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService', 'AssessmentPrequalService'];
    function ctrl($http, $translate, $translatePartialLoader, $stateParams, $location, SocketService, VerifiedSendService, SrtPernyataanPrequalService,
        $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService, AssessmentPrequalService) {
        var vm = this;

        vm.true = 1;
        vm.agree;
        vm.vendorName;
        vm.NamaDir;
        vm.NamaNotaris;
        vm.DocNo;
        vm.DocDate;
        vm.DocDateSub;
        vm.ContactID;
        vm.AddressID;
        vm.positionRef;
        vm.vendorName;
        vm.StateID;
        vm.CountryID;
        vm.Country;
        vm.StateName;
        vm.Position;
        vm.noID;
        vm.PersonAddress;
        vm.tglSekarang = UIControlService.getDateNow2("-");
        vm.ID;
        vm.DocType1 = item.DocType;
        vm.PrequalStepID = item.PrequalStepID;
        vm.VendorPrequalID = item.VendorPrequalID;
        vm.IsNeedRevision = item.IsNeedRevision;
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('surat-pernyataan');
            CekVendor();
            getdatenow();
            loadVendorContact();
            var dateNow = new Date();
            vm.convertedDateNow = UIControlService.getStrDate(dateNow);
        }

        vm.getdatenow = getdatenow;
        function getdatenow() {
            var datenow = new Date();
            vm.yearnow = datenow.getFullYear();
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            vm.monthnow = month[datenow.getMonth()];
            //return now;
        }

        vm.CekVendor = CekVendor;
        function CekVendor() {
            AssessmentPrequalService.CekAgree({
                DocType: vm.DocType1,
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.flag = reply.data;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.setuju = setuju;
        function setuju() {
            AssessmentPrequalService.insertDocST({
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID,
                DocType: vm.DocType1,
                UploadDate: vm.tglSekarang,
                DocumentUrl: "",
                IsAgree: 1,
                AgreementDate: vm.tglSekarang,
                IsActive: 1
            },
                                    function (reply) {
                                        UIControlService.unloadLoadingModal();
                                        if (reply.status === 200) {
                                            $uibModalInstance.close();

                                        }
                                        else {
                                            UIControlService.msg_growl("error", "NOTIF.FAIL_SAVE");
                                            return;
                                        }
                                    },
                                    function (err) {
                                        console.info(err);
                                        UIControlService.msg_growl("error", "NOTIF.API");
                                        UIControlService.unloadLoadingModal();
                                    }
                                );
        }

        vm.cek = cek;
        function cek() {
            VerifiedSendService.selectVerifikasi(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.verified = reply.data;
                    vm.cekTemporary = vm.verified.IsTemporary;
                    vm.VendorID = vm.verified.VendorID;
                    vm.vendorName = vm.verified.VendorName;
                    Cekcek(1);
                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.Cekcek = Cekcek;
        function Cekcek(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            SrtPernyataanPrequalService.selectCek({
                VendorId: vm.VendorID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200 && reply.data.length > 0) {
                    var data = reply.data;
                    vm.ID = data[0].ID;
                    vm.DocType = data[0].DocType;
                    vm.UploadDate = data[0].UploadDate;
                    vm.DocUrl = data[0].DocumentUrl;
                    SrtPernyataanPrequalService.updateAgree({
                        ID: vm.ID,
                        VendorId: vm.VendorID,
                        DocType: vm.DocType,
                        UploadDate: vm.UploadDate,
                        DocumentUrl: vm.DocUrl,
                        IsAgree: 1,
                        AgreementDate: vm.tglSekarang,
                        IsActive: 1
                    },
                                    function (reply) {
                                        UIControlService.unloadLoadingModal();
                                        if (reply.status === 200) {
                                            UIControlService.msg_growl("success", "NOTIF.SUC_AGREE");
                                            $uibModalInstance.close();

                                        }
                                        else {
                                            UIControlService.msg_growl("error", "NOTIF.FAIL_SAVE");
                                            return;
                                        }
                                    },
                                    function (err) {
                                        console.info(err);
                                        UIControlService.msg_growl("error", "NOTIF.API");
                                        UIControlService.unloadLoadingModal();
                                    }
                                );
                 
                }
                else if (reply.status === 200 && reply.data.length <= 0) {
                    agree();
                }
                else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadCompanyPerson = loadCompanyPerson;
        function loadCompanyPerson(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.loadCompPersPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.NamaDir = generateFilterStrings(reply.data);
                    vm.positionRef = data[0].PositionRef;
                    vm.noID = generateFilterStrings2(reply.data);
                    vm.PersonAddress = data[0].PersonAddress;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
        
        vm.loadVendorContact = loadVendorContact;
        function loadVendorContact(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.loadContactPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.ContactID = data[0].ContactID;
                    getAddressID(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.getAddressID = getAddressID;
        function getAddressID(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.selectAddressID({
                ContactID: vm.ContactID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.AddressID = data[0].AddressID;
                    loadAddress(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadAddress = loadAddress;
        function loadAddress(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.selectAddress({
                AddressID: vm.AddressID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Address = data[0].AddressDetail;
                    vm.StateID = data[0].StateID;
                    loadCountryID(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountryID = loadCountryID;
        function loadCountryID(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
                AssessmentPrequalService.selectCountryID({
                StateID: vm.StateID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.CountryID = data[0].CountryID;
                    vm.StateName = data[0].Name;
                    loadCountry(1);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountry = loadCountry;
        function loadCountry(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.selectCountry({
                CountryID: vm.CountryID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Country = data[0].Name;

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        function generateFilterStrings(nama) {
            var namaDir = "";
            for (var i = 0; i < nama.length; i++) {
                namaDir += nama[i].PersonName + ",";
            }
            return namaDir.substring(0, namaDir.length - 1);
        }

        function generateFilterStrings2(nama) {
            var namaDir = "";
            for (var i = 0; i < nama.length; i++) {
                namaDir += nama[i].NoID + ",";
            }
            return namaDir.substring(0, namaDir.length - 1);
        }

        //console.info(vm.tglSekarang);

        vm.agree = agree;
        function agree() {
            SrtPernyataanPrequalService.insertAgree({
                PrequalStepID: vm.PrequalStepID,
                VendorId: vm.VendorID,
                IsAgree: 1,
                AgreementDate: vm.tglSekarang,
                DocType: vm.DocType1,
                IsActive: 1
            },
                                    function (reply) {
                                        //console.info("reply" + JSON.stringify(reply))
                                        UIControlService.unloadLoadingModal();
                                        if (reply.status === 200) {
                                            UIControlService.msg_growl("success", "NOTIF.SUC_AGREE");
                                            $uibModalInstance.close();

                                        }
                                        else {
                                            UIControlService.msg_growl("error", "NOTIF.FAIL_SAVE");
                                            return;
                                        }
                                    },
                                    function (err) {
                                        console.info(err);
                                        UIControlService.msg_growl("error", "NOTIF.API");
                                        UIControlService.unloadLoadingModal();
                                    }
                                );

        }

        vm.update = update;
        function update() {
            SrtPernyataanPrequalService.updateAgree({
                ID : vm.ID,
                VendorId: vm.VendorID,
                IsAgree: 1,
                AgreementDate: vm.tglSekarang,
                IsActive: 1,
                DocType: vm.DocType1
            },
                                    function (reply) {
                                        UIControlService.unloadLoadingModal();
                                        if (reply.status === 200) {
                                            UIControlService.msg_growl("success", "NOTIF.SUC_AGREE");
                                            $uibModalInstance.close();

                                        }
                                        else {
                                            UIControlService.msg_growl("error", "NOTIF.FAIL_SAVE");
                                            return;
                                        }
                                    },
                                    function (err) {
                                        console.info(err);
                                        UIControlService.msg_growl("error", "NOTIF.API");
                                        UIControlService.unloadLoadingModal();
                                    }
                                );

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };


    }

    
})();