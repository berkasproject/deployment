﻿(function () {
    'use strict';

    angular.module("app").controller("SuratPernyataanModalBacaAssCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$stateParams', '$location', 'SocketService', 'VerifiedSendService', 'SrtPernyataanPrequalService',
        '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService', 'AssessmentPrequalService'];
    function ctrl($http, $translate, $translatePartialLoader, $stateParams, $location, SocketService, VerifiedSendService, SrtPernyataanPrequalService,
        $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService, AssessmentPrequalService) {
        var vm = this;

        vm.VendorID;
        //vm.Nama = [];
        //vm.Document = [];
        vm.NamaDir;
        vm.NamaNotaris;
        vm.DocNo;
        vm.DocDate;
        vm.DocDateSub;
        vm.ContactID;
        vm.AddressID;
        vm.positionRef;
        vm.vendorName;
        vm.StateID;
        vm.CountryID;
        vm.PrequalStepID = item.PrequalStepID;
        vm.VendorPrequalID = item.VendorPrequalID;
        vm.IsNeedRevision = item.IsNeedRevision;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('surat-pernyataan');
            loadCompanyPerson(1);
            loadLegalDoc(1);
            loadVendorContact(1);
        };

        vm.loadCompanyPerson = loadCompanyPerson;
        function loadCompanyPerson(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.loadCompPersPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.NamaDir = generateFilterStrings(reply.data);
                    vm.positionRef = data[0].PositionRef;

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadLegalDoc = loadLegalDoc;
        function loadLegalDoc(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.loadLegalPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.NamaNotaris = data[0].NotaryName;
                    vm.DocNo = data[0].DocumentNo;
                    vm.DocDate = data[0].DocumentDate;
                    vm.DocDateSub = vm.DocDate.substring(0,4);

                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.loadVendorContact = loadVendorContact;
        function loadVendorContact(current) {
            UIControlService.loadLoading("NOTIF.LOADING");
            AssessmentPrequalService.loadContactPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.ContactID = data[0].ContactID;
                    vm.businessName = data[0].Vendor.businessName;
                    vm.countrycode = data[0].Contact.Address.State.Country.Code;
                    getAddressID(1);
                
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.getAddressID = getAddressID;
        function getAddressID(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("NOTIF.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            AssessmentPrequalService.selectAddressID({
                ContactID: vm.ContactID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.AddressID = data[0].AddressID;
                    loadAddress(1);
                    //console.info(vm.AddressID);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        vm.loadAddress = loadAddress;
        function loadAddress(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("NOTIF.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            AssessmentPrequalService.selectAddress({
                AddressID: vm.AddressID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Address = data[0].AddressDetail;
                    vm.StateID = data[0].StateID;
                    loadCountryID(1);
                    //console.info(vm.Address);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountryID = loadCountryID;
        function loadCountryID(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("NOTIF.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            AssessmentPrequalService.selectCountryID({
                StateID: vm.StateID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.CountryID = data[0].CountryID;
                    loadCountry(1);
                    //console.info(vm.CountryID);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountry = loadCountry;
        function loadCountry(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("NOTIF.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            AssessmentPrequalService.selectCountry({
                CountryID: vm.CountryID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    //vm.Address = data[0].;
                    //console.info(reply.data);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        function generateFilterStrings(nama) {
            var namaDir = "";
            for (var i = 0; i < nama.length; i++) {
                namaDir += nama[i].PersonName + ",";
            }
            return namaDir.substring(0, namaDir.length - 1);
        }




        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };


    }
})();