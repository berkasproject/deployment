﻿(function () {
	'use strict';

	angular.module("app").controller("formTenagaAhliAssPQCtrl", ctrl);

	ctrl.$inject = ['AssessmentPrequalService', 'TenagaAhliPrequalService', 'UIControlService', 'item', '$uibModalInstance', '$translatePartialLoader'];

	function ctrl(AssessmentPrequalService, TenagaAhliService, UIControlService, item, $uibModalInstance, $translatePartialLoader) {
		var vm = this;
		vm.act = item.act;
		vm.item = item.item;
		vm.Gender = "";
		vm.tenagaahli = {};
		vm.isCalendarOpened = [false, false, false, false];
		vm.addresses = { AddressInfo: "" };
		vm.countrys = { Name: "" };
		vm.statuss = { Name: "" };
		vm.radio = {
			tipeM: "M",
			tipeF: "F",
			StatusK: "CONTRACT",
			StatusI: "INTERNSHIP",
			StatusP: "PERMANENT",
		}
		vm.nationalities = ["Indonesia"];
		vm.VEPID = item.VEPID;
		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('tenaga-ahli');
			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date(tomorrow);
			afterTomorrow.setDate(tomorrow.getDate() + 30);
			UIControlService.loadLoadingModal("MESSAGE.LOADING");

			AssessmentPrequalService.getallnationalities(function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    vm.nationalities = reply.data;
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_LIST_COUNTRY");
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_LIST_COUNTRY");
				UIControlService.unloadLoadingModal();
			});

			if (vm.act === false) {
				vm.Name = vm.item.Name;
				vm.tenagaahli.BirthDate = vm.item.DateOfBirth;
				if (vm.item.Gender === "M") {
					vm.Gender = "M";
				} else if (vm.item.Gender === "F") {
					vm.Gender = "F";
				}
				vm.address = vm.item.address.AddressInfo;
				vm.Education = vm.item.Education;
				vm.Nationality = vm.item.country.Name;
				vm.Position = vm.item.Position;
				vm.YearOfExperience = vm.item.YearOfExperience;
				vm.Email = vm.item.Email;

				if (vm.item.Statusperson.Name === "CONTRACT") {
					vm.Status = "CONTRACT";
				} else if (vm.item.Statusperson.Name === "INTERNSHIP") {
					vm.Status = "INTERNSHIP";
				} else if (vm.item.Statusperson.Name === "PERMANENT") {
					vm.Status = "PERMANENT";
				}

				vm.Expertise = vm.item.Expertise;
			}

			convertToDate();
		}


		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.tenagaahli.BirthDate) {
				vm.tenagaahli.BirthDate = UIControlService.getStrDate(vm.tenagaahli.BirthDate);
			}
		};

		//Supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.tenagaahli.BirthDate) {
				vm.tenagaahli.BirthDate = new Date(Date.parse(vm.tenagaahli.BirthDate));
			}
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.update = update;
		vm.vendor = {};
		function update() {
			convertAllDateToString();
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			if (vm.act === false) {
				vm.addresses.AddressInfo = vm.address;
				vm.countrys.Name = vm.Nationality;
				vm.statuss.Value = vm.Status;
				vm.vendor = {
					ID: vm.item.ID,
					Name: vm.Name,
					DateOfBirth: vm.tenagaahli.BirthDate,
					Gender: vm.Gender,
					address: vm.addresses,
					Education: vm.Education,
					country: vm.countrys,
					Position: vm.Position,
					YearOfExperience: vm.YearOfExperience,
					Email: '',
					Statusperson: vm.statuss,
					Expertise: vm.Expertise
				};
				AssessmentPrequalService.updateExpert(vm.vendor, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCCESS_UPDATE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.FAILED_UPDATE");
					UIControlService.unloadLoadingModal();
				});
			}
			else if (vm.act === true) {
				vm.addresses.AddressInfo = vm.address;
				vm.countrys.Name = vm.Nationality;
				vm.statuss.Value = vm.Status;
				vm.vendor = {
					Name: vm.Name,
					DateOfBirth: vm.tenagaahli.BirthDate,
					Gender: vm.Gender,
					address: {
					    AddressInfo: vm.address

					},
					Education: vm.Education,
					country: vm.countrys,
					Position: vm.Position,
					Email: '',
					Statusperson: vm.statuss,
					Expertise: vm.Expertise,
				    VendorEntryPrequalID: vm.VEPID
				};
			    AssessmentPrequalService.insertExpert(vm.vendor, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					UIControlService.unloadLoadingModal();
				});
			}
		}

		vm.validateAge = validateAge;
		function validateAge(inputDate) {
		    var inputDate = vm.tenagaahli.BirthDate;
		    //var convertedDate = moment(inputDate).format("DD-MM-YYYY");
		    var validatedAge = false;
		    var birthDate = moment(inputDate).format("DD-MM");;
		    var dateNow = moment().format("DD-MM");
		    var birthYear = moment(inputDate).format("YYYY");;
		    var yearNow = moment().format("YYYY");
		    var yearAge = yearNow - birthYear;
		    if (yearAge > 17) {
		        var validatedAge = true;
		    } else if (yearAge === 17) {
		        if (birthDate < dateNow || birthDate === dateNow) {
		            var validatedAge = true;
		        }
		    }

		    if (validatedAge === false) {
		        UIControlService.msg_growl('error', "ERRORS.AGE_UNDER17");
		        vm.tenagaahli.BirthDate = null;
		    }
		}

		vm.addToList = addToList;
		function addToList() {
			vm.addresses.AddressInfo = vm.address;
			vm.countrys.Name = vm.Nationality;
			vm.statuss.Name = vm.Status;
			vm.vendor = {
				Name: vm.Name,
				DateOfBirth: vm.tenagaahli.BirthDate,
				Gender: vm.Gender,
				address: vm.addresses,
				Education: vm.Education,
				country: vm.countrys,
				Position: vm.Position,
				YearOfExperience: vm.YearOfExperience,
				Email: '',
				Statusperson: vm.statuss,
				Expertise: vm.Expertise
			};
		}

		vm.add = add;
		function add(data) {
		}
	}
})();