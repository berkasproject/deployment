(function () {
	'use strict';

	angular.module("app").controller("TenagaAhliController", ctrl);

	ctrl.$inject = ['$state', '$filter', '$stateParams', 'AssessmentPrequalService', '$timeout', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'TenagaAhliPrequalService', 'UIControlService', 'GlobalConstantService'];
	/* @ngInject */
	function ctrl($state, $filter, $stateParams, AssessmentPrequalService, $timeout, $http, $uibModal, $translate, $translatePartialLoader, $location, TenagaAhliService, UIControlService, GlobalConstantService) {
	    var vm = this;
	    vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
	    vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
	    vm.VEPID = Number($stateParams.VEPID);
		vm.fullSize = 10;
		var offset = 0;
		vm.flag = 0;
		vm.flagOri = 0;
		vm.flagDetail = 0;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.vendorexpertsCertificate = [];
		vm.isApprovedCR = false;
		vm.flagCV = true;
		vm.initialize = initialize;
		function initialize() {
		    vm.flagCV = true;
		    $translatePartialLoader.addPart('tenaga-ahli');
		    $translatePartialLoader.addPart('assessment-prakualifikasi');
			loadSetupStep();
        };

        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID,
                Offset: vm.VEPID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isEntry = reply.data.IsNotStarted;
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID;
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    vm.remark = reply.data.Remark;
                    loadExpertPrequal(false);
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadExpert = loadExpert;
        function loadExpert() {
            AssessmentPrequalService.SelectExpert({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listExpert = reply.data;
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
        
        vm.loadExpertPrequal = loadExpertPrequal;
        function loadExpertPrequal(flag) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.flag = flag;
            AssessmentPrequalService.SelectExpertPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status === 200) {
                    vm.listExpertPrequal = reply.data;
                    vm.listExpertPrequal.forEach(function (data) {
                        if (data.YearOfExperience == null) vm.flagCV = false;
                    });
                    console.info("flagcv" + vm.flagCV);
                    if (vm.flag == true) {
                        vm.listExpertPrequal.forEach(function (data) {
                            if (vm.dataExpert.ID == data.ID) {
                                viewdetail(data, true);
                            }
                        });
                    }
                    else UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
		
		vm.loadCertificate = loadCertificate;
		function loadCertificate(data) {
			vm.listJob = [];
			vm.listEducation = [];
			vm.listCertificate = [];
			AssessmentPrequalService.selectCertificate({
				Offset: offset,
				Limit: vm.fullSize,
				Status: data.ID
			}, function (reply) {
				if (reply.status === 200) {
				    vm.vendorexpertsCertificate = reply.data;
					for (var i = 0; vm.vendorexpertsCertificate.length; i++) {
						vm.vendorexpertsCertificate[i].StartDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].StartDate));
						vm.vendorexpertsCertificate[i].EndDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].EndDate));
						if (vm.vendorexpertsCertificate[i].SysReference.Name == "JOB_EXPERIENCE" && vm.vendorexpertsCertificate[i].IsActive == true)
							vm.listJob.push(vm.vendorexpertsCertificate[i]);
						else if (vm.vendorexpertsCertificate[i].SysReference.Name == "DIPLOMA" && vm.vendorexpertsCertificate[i].IsActive == true)
							vm.listEducation.push(vm.vendorexpertsCertificate[i]);
						else if (vm.vendorexpertsCertificate[i].SysReference.Name == "CERTIFICATE" && vm.vendorexpertsCertificate[i].IsActive == true)
							vm.listCertificate.push(vm.vendorexpertsCertificate[i]);

					}
					UIControlService.unloadLoading();
				} else {
					$.growl.error({ message: $filter('translate')('MESSAGE.FAIL_GETDATA')});
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadCertificatePrequal = loadCertificatePrequal;
		function loadCertificatePrequal(data) {
		    vm.listJobPrequal = [];
		    vm.listEducationPrequal = [];
		    vm.listCertificatePrequal = [];
		    //console.info("curr "+current)
		    AssessmentPrequalService.selectCertificatePrequal({
		        Offset: offset,
		        Limit: vm.fullSize,
		        Status: data.ID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.vendorexpertsCertificatePrequal = reply.data;
		            console.info("certPQ" + JSON.stringify(vm.vendorexpertsCertificate));
		            for (var i = 0; vm.vendorexpertsCertificatePrequal.length; i++) {
		                vm.vendorexpertsCertificatePrequal[i].StartDate = new Date(Date.parse(vm.vendorexpertsCertificatePrequal[i].StartDate));
		                vm.vendorexpertsCertificatePrequal[i].EndDate = new Date(Date.parse(vm.vendorexpertsCertificate[i].EndDate));
		                if (vm.vendorexpertsCertificatePrequal[i].SysReference.Name == "JOB_EXPERIENCE" && vm.vendorexpertsCertificatePrequal[i].IsActive == true)
		                    vm.listJobPrequal.push(vm.vendorexpertsCertificatePrequal[i]);
		                else if (vm.vendorexpertsCertificatePrequal[i].SysReference.Name == "DIPLOMA" && vm.vendorexpertsCertificatePrequalPrequal[i].IsActive == true)
		                    vm.listEducationPrequal.push(vm.vendorexpertsCertificatePrequal[i]);
		                else if (vm.vendorexpertsCertificatePrequal[i].SysReference.Name == "CERTIFICATE" && vm.vendorexpertsCertificatePrequal[i].IsActive == true)
		                    vm.listCertificatePrequal.push(vm.vendorexpertsCertificatePrequal[i]);

		            }
		        } else {
		            $.growl.error({ message: $filter('translate')('MESSAGE.FAIL_GETDATA')});
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.UbahTenagaAhli = UbahTenagaAhli;
		function UbahTenagaAhli(data) {
		    vm.flag = 0;
		    vm.flagOri = 0;
		    vm.flagDetail = 0;
			console.info("masuk form add/edit");
			var data = {
				act: false,
				item: data
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/FormTenagaAhli.html',
			    controller: 'formTenagaAhliAssPQCtrl',
				controllerAs: 'formTenagaAhliAssPQCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				initialize();
				viewdetail(vm.dataExpert, true);
			});
		}

		vm.lihatDetail = lihatDetail;
		function lihatDetail(data) {
		    vm.flag = 0;
		    vm.flagOri = 0;
		    vm.flagDetail = 0;
			console.info("masuk form add/edit");
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/DetailTenagaAhli.html',
				controller: 'DetailTenagaAhliCtrl',
				controllerAs: 'DetailTenagaAhliCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});

			modalInstance.result.then(function () {
				initialize();
				viewdetail(vm.dataExpert, true);
			});
		}

		vm.HapusData = HapusData;
		function HapusData(obj, act) {
			vm.flag = 0;
			AssessmentPrequalService.editActiveExpert({
				ID: obj.ID,
				IsActive: act
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					if (act === false)
						UIControlService.msg_growl("success", "MESSAGE.SUCC_DELETE");
					if (act === true)
						UIControlService.msg_growl("success", "Data Berhasil di Aktifkan");
					$timeout(function () {
						window.location.reload();
					}, 1000);
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_DEL");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.TambahTenagaAhli = TambahTenagaAhli;
		function TambahTenagaAhli(data) {
			console.info("masuk form add/edit");
			var data = {
				act: true,
				item: data,
                VEPID: vm.VEPID
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/FormTenagaAhli.html',
			    controller: 'formTenagaAhliAssPQCtrl',
			    controllerAs: 'formTenagaAhliAssPQCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {

				initialize();
				//viewdetail(vm.dataExpert);
				window.location.reload();
			});
		}

		vm.hapusdetail = hapusdetail;
		function hapusdetail(data, act) {
			AssessmentPrequalService.editCertificateActive({
				ID: data.ID,
				IsActive: act
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    UIControlService.msg_growl("success", "MESSAGE.SUCC_DELDATA");
				    loadExpertPrequal(true);
				} else {
					return;
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.DetailCertificate = DetailCertificate;
		function DetailCertificate(data1, act, type) {
		    vm.data1 = data1;
		    vm.act = act;
		    vm.type = type;
			if (data1 == undefined) {
				vm.cc = {
				    VendorExpertsID: vm.IdExpert
				}
			} else {
				vm.cc = data1
			}

			var data = {
				type: type,
				act: act,
				item: vm.cc
			}

			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/FormDetailTenagaAhli.html',
				controller: 'formDetailTenagaAhliAssPQCtrl',
				controllerAs: 'formDetailTenagaAhliAssPQCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				loadExpertPrequal(true);
			});
		}

		vm.viewdetail = viewdetail;
		function viewdetail(data, flagPrequal) {
		    console.info(data);
		    vm.dataExpert = data;
		    vm.IdExpert = data.ID;
		    vm.flagDetail = 1;
		    vm.namavendor = data.Name;
		    if (flagPrequal == false) {
		        loadCertificate(data);
		        vm.flagOri = 1;
		    }
		    else {
		        vm.jobexp = [];
		        vm.diploma = [];
		        vm.certificate = [];
		        if (data.Certificates != null) {
		            for (var i = 0; i <= data.Certificates.length - 1; i++) {
		                if (data.Certificates[i].Type == 3128) {
		                    vm.jobexp.push(data.Certificates[i]);
		                }
		                else if (data.Certificates[i].Type == 3129) {
		                    vm.diploma.push(data.Certificates[i]);
		                }
		                else if (data.Certificates[i].Type == 3130) {
		                    vm.certificate.push(data.Certificates[i]);
		                }
		            }
		        }
		        vm.flag = 1;
		        UIControlService.unloadLoading();
		    }
		}


		vm.valid = true;
		vm.notvalid = function notvalid() {
		    vm.valid = false;
		}

		vm.Submit = Submit;
		function Submit(flag) {
		    vm.valid = flag;
		    if (vm.remark == undefined) {
		        vm.remark = "";
		    }
		    if (vm.flagCV == false) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_CV");
		        return;
		    }
		    AssessmentPrequalService.InsertSubmit({
		        PrequalStepID: vm.PrequalStepAssID,
		        VendorPrequalID: vm.VendorPrequalID,
		        IsAgree: flag,
		        VEPID: vm.VEPID,
		        Remark: vm.remark
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            $state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
		        } else {
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}

	}
})();
