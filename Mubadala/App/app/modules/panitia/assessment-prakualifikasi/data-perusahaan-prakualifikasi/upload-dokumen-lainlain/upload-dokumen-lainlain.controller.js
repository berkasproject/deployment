(function () {
	'use strict';

	angular.module("app").controller("UploadDokumenPrequalAssCtrl", ctrl);

	ctrl.$inject = ['$http', '$state', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', '$filter', 'UploadDokumenLainlainPrequalService', 'UIControlService', 'GlobalConstantService', 'AssessmentPrequalService'];
	/* @ngInject */
	function ctrl($http, $state, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, $filter, UploadDokumenLainlainPrequalService, UIControlService, GlobalConstantService, AssessmentPrequalService) {
		var vm = this;
		vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
		vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
		vm.VEPID = Number($stateParams.VEPID);
		vm.totalItems = 0;
		vm.currentPage = 0;
		vm.maxSize = 10;
		vm.page_id = 35;
		vm.menuhome = 0;
		vm.userId = 0;
		vm.jLoad = jLoad;
		vm.document = [];
		vm.Kata = "";
		vm.VendorID;
		vm.IsApprovedCR = false;

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('other-docs');
		    $translatePartialLoader.addPart('assessment-prakualifikasi');
		    loadSetupStep();
		    
		}

		vm.loadSetupStep = loadSetupStep;
		function loadSetupStep() {
		    AssessmentPrequalService.SelectSetupStep({
		        Status: vm.PrequalStepAssID,
		        column: vm.VendorPrequalID,
		        Offset: vm.VEPID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.isEntry = reply.data.IsNotStarted;
		            vm.PrequalStepID = reply.data.PrequalStepID;
		            vm.PrequalSetupID = reply.data.PrequalSetupID;
		            vm.IsNeedRevision = reply.data.IsNeedRevision;
		            vm.remark = reply.data.Remark;
		            load();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.load = load;
		function load() {
		    AssessmentPrequalService.loadDokumenLainLain({
		        VendorPrequalID: vm.VendorPrequalID,
		        PrequalSetupStepID: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.document = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}


		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("NOTIF.LOADING");
			UploadDokumenLainlainService.SelectVend({
				VendorID: vm.VendorID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.document = data;
					vm.document.forEach(function (cr) {
						cr.ValidDateConverted = convertDate(cr.ValidDate);
					});
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.tambah = tambah;
		function tambah(data) {
			var data = {
				act: 1,
				item: data,
				VEPID: vm.VEPID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/upload-dokumen-lainlain/upload-dokumen-lainlain.modal.html',
				controller: "UploadDokModalPrequalAssCtrl",
				controllerAs: "UploadDokModalPrequalAssCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				load();
			});
		}

		vm.edit = edit;
		function edit(data) {
			//console.info("console edit dokumen");
			var data = {
				act: 0,
				item: {
					ID: data.ID,
					DocumentName: data.DocumentName,
					DocumentNo: data.DocumentNo,
					ValidDate: new Date(Date.parse(data.ValidDate)),
					DocumentUrl: data.DocumentUrl,
					VendorEntryPrequalID: data.VendorEntryPrequalID
				}
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/upload-dokumen-lainlain/upload-dokumen-lainlain.modal.html',
				controller: "UploadDokModalPrequalAssCtrl",
				controllerAs: "UploadDokModalPrequalAssCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				load();
			});
		}

		vm.view = view;
		function view(doc) {
			var data = {
				item: {
					DocumentName: doc.DocumentName,
					DocumentNo: doc.DocumentNo,
					ValidDate: doc.ValidDate,
					DocumentUrl: doc.DocumentUrl,
					VendorEntryPrequalID: doc.VendorEntryPrequalID
				}
			};
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/upload-dokumen-lainlain/upload-dokumen-lainlain.viewModal.html',
			    controller: 'viewUploadDocPrequalAssCtrl',
			    controllerAs: 'viewUploadDocPrequalAssCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
		};

		vm.back = back;
		function back() {
		    $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		}

		vm.Submit = Submit;
		function Submit() {
		    UploadDokumenLainlainPrequalService.submit({ Status: vm.PrequalStepID }, function (reply) {
		        if (reply.status == 200) {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("success", "Success Submit");
		            $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		        } else {
		            UIControlService.unloadLoading();
		            return;
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		        return;
		    });

		}

		vm.valid = true;
		vm.notvalid = function notvalid() {
		    vm.valid = false;
		}


		vm.Submit = Submit;
		function Submit(flag) {
		    vm.valid = flag;
		    if (vm.remark == undefined) {
		        vm.remark = "";
		    }
		    AssessmentPrequalService.InsertSubmit({
		        PrequalStepID: vm.PrequalStepAssID,
		        VendorPrequalID: vm.VendorPrequalID,
		        IsAgree: flag,
		        VEPID: vm.VEPID,
		        Remark: vm.remark
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            $state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
		        } else {
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.remove = remove;
		function remove(doc) {
			bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('NOTIF.CONFIRM_DEL') + '<h3>', function (reply) {
				if (reply) {
					//UIControlService.loadLoading(loadmsg);
					AssessmentPrequalService.removeOtherDoc({
						ID: doc.ID
					}, function (reply2) {
						UIControlService.unloadLoading();
						if (reply2.status === 200) {
							UIControlService.msg_growl('notice', 'NOTIF.SUCCESS_DELETE');
							load();
						} else
							UIControlService.msg_growl('error', 'NOTIF.FAIL_DELETE');
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl('error', 'NOTIF.FAIL_DELETE');
					});
				}
			});
		};
	}
})();
