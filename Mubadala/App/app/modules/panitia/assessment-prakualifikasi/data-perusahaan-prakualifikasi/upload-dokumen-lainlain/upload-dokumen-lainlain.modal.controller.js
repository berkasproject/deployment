﻿(function () {
    'use strict';

    angular.module("app").controller("UploadDokModalPrequalAssCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$stateParams', '$location', 'UploadDokumenLainlainPrequalService',
        '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService', 'AssessmentPrequalService'];
    function ctrl($http, $translate, $translatePartialLoader, $stateParams, $location, UploadDokumenLainlainPrequalService,
        $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService, AssessmentPrequalService) {
        var vm = this;
        vm.detail = item.item;
        vm.VendorID;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.isAdd = item.act;
        vm.action = "";
        vm.pathFile;
        vm.Description;
        vm.fileUpload;
        vm.size;
        vm.name;
        vm.type;
        vm.flag;
        vm.selectedForm;
        vm.isCalendarOpened = [false, false, false];
        vm.Nama;
        vm.No;
        vm.ID;
        vm.idFileTypes;
        vm.idFileSize;
        vm.idUploadConfigs;
        vm.tgl = {};
        vm.DocUrl;
        vm.tglSekarang = UIControlService.getDateNow("");
        vm.VEPID = item.VEPID;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('other-docs');
            //console.info("item:" + JSON.stringify(item));
            UIControlService.loadLoading("MESSAGE.LOADING");
            //get tipe dan max.size file - 1
            UploadFileConfigService.getByPageName("PAGE.VENDOR.UPLOADDL", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", ".NOTIF.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "NOTIF.API");
                UIControlService.unloadLoading();
                return;
            });
            if (vm.isAdd == 1) {
                vm.action = "Tambah";
            } else {
                vm.action = "Ubah";
                vm.ID = item.item.ID;
                vm.Nama = item.item.DocumentName;
                vm.No = item.item.DocumentNo;
                vm.tgl.StartDate = item.item.ValidDate;
                vm.VendorEntryPrequalID = item.item.VendorEntryPrequalID;
                vm.DocUrl = item.item.DocumentUrl;
            }


        }

        vm.simpan = simpan;
        function simpan() {
            if (vm.Nama === undefined || vm.Nama === "" || vm.Nama == null) {
                UIControlService.msg_growl("error", "Document Name is still empty");
                return;
            } else if (vm.No === undefined || vm.No === "" || vm.No == null) {
                UIControlService.msg_growl("error", "Document Number is still empty");
                return;
            }
            else if (vm.fileUpload == null && vm.DocUrl != null) {
                saveData();
            }
            else if (vm.fileUpload == null && vm.DocUrl == null) {
                UIControlService.msg_growl("error", "ERROR.NO_FILE");
                return;
            } else {
                uploadFile();
            }
        }

        //get tipe dan max.size file - 2
        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }
        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        }

        vm.selectUpload = selectUpload;
        //vm.fileUpload;
        function selectUpload() {
        }
        /*start upload */
        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.VEPID, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
            }
        }

        function validateFileType(file, allowedFileTypes) {

            if (!file || file.length == 0) {
                UIControlService.msg_growl("error", "ERROR.NO_FILE");
                return false;
            }
            return true;
        }

        vm.upload = upload;
        function upload(id, file, config, filters, callback) {
            convertToDate();

            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }
            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            UIControlService.loadLoading("NOTIF.LOADING_UPLOAD");
            
            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_ADMIN", size, filters, vm.tglSekarang,
            function (response) {
                //console.info("upload:" + JSON.stringify(response.data));
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    var fileName = response.data.FileName;
                    vm.DocUrl = url;
                    saveData();
                    //console.info("url" + vm.pathFile);
                } else {
                    UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
                    return;
                }
            },
            function (response) {
                UIControlService.msg_growl("error", "MESSAGE.API")
                UIControlService.unloadLoading();
            });

                

        }

        vm.saveData = saveData;
        function saveData() {
            if (vm.isAdd == 1) {
                AssessmentPrequalService.insertOtherDoc({
                    DocumentName: vm.Nama,
                    DocumentNo: vm.No,
                    DocumentUrl: vm.DocUrl,
                    ValidDate:vm.tgl.StartDate,
                    VendorEntryPrequalID: vm.VEPID
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "NOTIF.SUCCESS_UPLOAD");
                        $uibModalInstance.close();

                    } else {
                        return;
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                }
                    );
            }
            else {
                AssessmentPrequalService.updateOtherDoc({
                    DocumentName: vm.Nama, DocumentUrl: vm.DocUrl, DocumentNo: vm.No, ValidDate: vm.tgl.StartDate, ID: vm.ID
                }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "NOTIF.SUCCESS_UPDATE");
                        $uibModalInstance.close();
                    } else {
                        UIControlService.msg_growl("error", "ERROR.FAIL_UPDATE");
                        return;
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
        }

        function convertAllDateToString() { // TIMEZONE (-)
            vm.tgl = UIControlService.getStrDate(vm.tgl);
        };

        function convertToDate() {
            vm.tgl.StartDate = UIControlService.getStrDate(vm.tgl.StartDate);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
