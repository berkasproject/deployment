(function () {
    'use strict';

    angular.module("app").controller("BalanceVendorPrequalCtrl", ctrl);

    ctrl.$inject = ['AssessmentPrequalService', '$stateParams', 'AuthService', '$scope', '$state', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BalanceVendorPrequalService', 'BalanceVendorService', 'RoleService', 'UIControlService', 'GlobalConstantService', '$uibModal'];
    function ctrl(AssessmentPrequalService, $stateParams, AuthService, $scope, $state, $http, $translate, $translatePartialLoader, $location, SocketService, BalanceVendorPrequalService, BalanceVendorService,
        RoleService, UIControlService, GlobalConstantService, $uibModal) {

        var vm = this;
        vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
        vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
        vm.VEPID = Number($stateParams.VEPID);
        var page_id = 141;
        vm.departemen = [];
        var asset = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        //vm.isApprovedCR;
        vm.balanceDocUrl = "";
        vm.valid = true;

        vm.IsApprovedCR = false;
        vm.initialize = initialize;
        vm.changeDataPermission = false;
        function initialize() {
            $translatePartialLoader.addPart('vendor-balance');
            $translatePartialLoader.addPart('assessment-prakualifikasi');
            
            loadSetupStep();
        }


        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID,
                Offset:vm.VEPID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isEntry = reply.data.IsNotStarted;
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID;
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    vm.remark = reply.data.Remark;
                    console.info("reply" + JSON.stringify(reply.data));
                    loadBalancePrequal();
                    loadBalanceDocUrlPrequal();

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.loadBalanceDocUrl = loadBalanceDocUrl;
        function loadBalanceDocUrl() {
            AssessmentPrequalService.balanceDocUrl({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status === 200) {
                    vm.balanceDocUrl = reply.data.DocUrl;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        vm.loadBalanceDocUrlPrequal = loadBalanceDocUrlPrequal;
        function loadBalanceDocUrlPrequal() {
            AssessmentPrequalService.balanceDocUrlPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status === 200) {
                    vm.balanceDocUrlPrequal = reply.data.DocUrl;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }


        vm.loadPrequalStep = loadPrequalStep;
        function loadPrequalStep(){
            BalanceVendorPrequalService.loadPrequalStep({ Status: vm.PrequalStepID }, function (reply) {
                if (reply.status === 200) {
                    vm.isEntry = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        vm.loadBalance = loadBalance;
        function loadBalance() {
            vm.asset = 0;
            vm.hutang = 0;
            vm.modal = 0;
            vm.vendorbalance = [];
            AssessmentPrequalService.loadBalance({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorbalance = reply.data;
                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        if (vm.vendorbalance[i].WealthType.Name == "WEALTH_TYPE_ASSET") {
                            vm.listAsset = vm.vendorbalance[i];
                        }
                        if (vm.vendorbalance[i].WealthType.Name == "WEALTH_TYPE_DEBTH") {
                            vm.listDebth = vm.vendorbalance[i];
                        }
                    }
                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        for (var j = 0; j < vm.vendorbalance[i].subWealth.length; j++) {
                            if (vm.vendorbalance[i].subWealth[j].subCategory.length === 0) {
                                if (vm.vendorbalance[i].WealthType.RefID === 3097 && vm.vendorbalance[i].subWealth[j].IsActive === true) {
                                    if (vm.asset === 0) {
                                        vm.asset = vm.vendorbalance[i].subWealth[j].nominal;
                                    }
                                    else
                                        vm.asset = +vm.asset + +vm.vendorbalance[i].subWealth[j].nominal;

                                }
                                else if (vm.vendorbalance[i].WealthType.RefID === 3099 && vm.vendorbalance[i].subWealth[j].IsActive === true) {
                                    if (vm.hutang === 0) {
                                        vm.hutang = vm.vendorbalance[i].subWealth[j].nominal;
                                    }
                                    else {
                                        vm.hutang = +vm.hutang + +vm.vendorbalance[i].subWealth[j].nominal;
                                    }


                                }
                            }
                            else {
                                for (var k = 0; k < vm.vendorbalance[i].subWealth[j].subCategory.length; k++) {
                                    if (vm.vendorbalance[i].subWealth[j].subCategory[k].Wealth.RefID === 3097 && vm.vendorbalance[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.asset === 0) {
                                            vm.asset = vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else
                                            vm.asset = +vm.asset + +vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;

                                    }
                                    else if (vm.vendorbalance[i].subWealth[j].subCategory[k].WealthType === 3099 && vm.vendorbalance[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.hutang === 0) {
                                            vm.hutang = vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else {
                                            vm.hutang = +vm.hutang + +vm.vendorbalance[i].subWealth[j].subCategory[k].Nominal;
                                        }


                                    }
                                }
                            }
                            
                        }
                    }
                    vm.modal = +vm.asset - +vm.hutang;
                                
                    console.info(JSON.stringify(vm.modal));
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadBalancePrequal = loadBalancePrequal;
        function loadBalancePrequal() {
            vm.asset = 0;
            vm.hutang = 0;
            vm.modal = 0;
            vm.vendorbalancePrequal = [];
            AssessmentPrequalService.loadBalancePrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorbalancePrequal = reply.data;
                    console.info("vendorBlance:" + JSON.stringify(vm.vendorbalancePrequal));
                    for (var i = 0; i < vm.vendorbalancePrequal.length; i++) {
                        if (vm.vendorbalancePrequal[i].WealthType.Name == "WEALTH_TYPE_ASSET") {
                            vm.listAsset = vm.vendorbalancePrequal[i];
                        }
                        if (vm.vendorbalancePrequal[i].WealthType.Name == "WEALTH_TYPE_DEBTH") {
                            vm.listDebth = vm.vendorbalancePrequal[i];
                        }
                    }
                    for (var i = 0; i < vm.vendorbalancePrequal.length; i++) {
                        for (var j = 0; j < vm.vendorbalancePrequal[i].subWealth.length; j++) {
                            if (vm.vendorbalancePrequal[i].subWealth[j].subCategory.length === 0) {
                                if (vm.vendorbalancePrequal[i].WealthType.RefID === 3097 && vm.vendorbalancePrequal[i].subWealth[j].IsActive === true) {
                                    if (vm.asset === 0) {
                                        vm.asset = vm.vendorbalancePrequal[i].subWealth[j].nominal;
                                    }
                                    else
                                        vm.asset = +vm.asset + +vm.vendorbalancePrequal[i].subWealth[j].nominal;

                                }
                                else if (vm.vendorbalancePrequal[i].WealthType.RefID === 3099 && vm.vendorbalancePrequal[i].subWealth[j].IsActive === true) {
                                    if (vm.hutang === 0) {
                                        vm.hutang = vm.vendorbalancePrequal[i].subWealth[j].nominal;
                                    }
                                    else {
                                        vm.hutang = +vm.hutang + +vm.vendorbalancePrequal[i].subWealth[j].nominal;
                                    }


                                }
                            }
                            else {
                                for (var k = 0; k < vm.vendorbalancePrequal[i].subWealth[j].subCategory.length; k++) {
                                    if (vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].Wealth.RefID === 3097 && vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.asset === 0) {
                                            vm.asset = vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else
                                            vm.asset = +vm.asset + +vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].Nominal;

                                    }
                                    else if (vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].WealthType === 3099 && vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].IsActive === true) {
                                        if (vm.hutang === 0) {
                                            vm.hutang = vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].Nominal;
                                        }
                                        else {
                                            vm.hutang = +vm.hutang + +vm.vendorbalancePrequal[i].subWealth[j].subCategory[k].Nominal;
                                        }


                                    }
                                }
                            }

                        }
                    }
                    vm.modal = +vm.asset - +vm.hutang;
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            AssessmentPrequalService.deleteBalance({
                ID: data.ID,
                IsActive: active
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var msg = "";
                    if (active === false) msg = "{{'Non-aktifkan'|translate}}";
                    if (active === true) msg = "{{'Aktifkan'|translate}}";
                    UIControlService.msg_growl("success", "SUC_DELETE");
                    loadBalancePrequal();
                }
                else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_DEL");
                    return;
                }
            }, function (err) {

                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });

        }

        vm.tambah = tambah;
        function tambah() {
            var data = {
                act: true,
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/neraca/formNeraca.html',
                controller: 'formNeracaCtrl',
                controllerAs: 'formNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        }

        vm.edit = edit;
        function edit(data, flag) {
            console.info("masuk form add/edit");
            if (flag == 1) {
                var data = {
                    act: false,
                    item: data,
                    PrequalStepID: vm.PrequalStepID
                }
            }
            if (flag != 1) {

                var data = {
                    act: false,
                    item:
                    {
                        ID: flag.ID,
                        Wealth: data,
                        COA: flag.COAType,
                        Unit: flag.Unit,
                        Amount: flag.Amount,
                        DocUrl: flag.DocUrl,
                        Nominal: flag.nominal
                    },
                    PrequalStepID: vm.PrequalStepID
                }
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/neraca/formNeraca.html',
                controller: 'formNeracaCtrl',
                controllerAs: 'formNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                loadBalancePrequal();
            });
        }

        vm.upload = upload;
        function upload() {
            var data = {
                act: true,
                PrequalStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/neraca/formUploadNeraca.html',
                controller: 'frmUploadNeracaCtrl',
                controllerAs: 'frmUploadNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        }


        vm.notvalid = function notvalid() {
            vm.valid = false;
        }

        vm.Submit = Submit;
        function Submit(flag) {
            vm.valid = flag;
            if (vm.remark == undefined) {
                vm.remark = "";
            }
            AssessmentPrequalService.InsertSubmit({
                PrequalStepID: vm.PrequalStepAssID,
                VendorPrequalID: vm.VendorPrequalID,
                IsAgree: flag,
                VEPID: vm.VEPID,
                Remark: vm.remark
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $state.go('assessment-prakualifikasi-detail', {SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
    }
})();
