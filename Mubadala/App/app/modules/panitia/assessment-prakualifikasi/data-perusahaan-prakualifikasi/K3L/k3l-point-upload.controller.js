﻿(function () {
	'use strict';

	angular.module("app")
            .controller("K3LAssPrequalCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'GlobalConstantService', '$state', '$stateParams', 'AssessmentPrequalService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, UploadFileConfigService,
        UIControlService, UploaderService, GlobalConstantService, $state, $stateParams, AssessmentPrequalService) {

	    var vm = this;
	    vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
	    vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
	    vm.VEPID = Number($stateParams.VEPID);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.action = "";
		vm.pathFile;
		vm.Description;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;


		vm.init = init;
		function init() {
		    vm.fileUpload = undefined;
		    $translatePartialLoader.addPart("assessment-prakualifikasi");
		    $translatePartialLoader.addPart("master-library");
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.CSMSPRAKUAL", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			loadSetupStep();
            
		}

		function loadSetupStep() {
		    UIControlService.loadLoading("MESSAGE.LOADING");
		    AssessmentPrequalService.SelectSetupStep({
		        Status: vm.PrequalStepAssID,
		        column: vm.VendorPrequalID,
		        Offset: vm.VEPID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.isEntry = reply.data.IsNotStarted;
		            vm.PrequalStepID = reply.data.PrequalStepID;
		            vm.PrequalSetupID = reply.data.PrequalSetupID;
		            vm.IsNeedRevision = reply.data.IsNeedRevision;
		            vm.remark = reply.data.Remark;
		            jLoad();
		            //jLoadPrequal(1);

		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			console.info(allowedTypes);
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		/*start upload */
		vm.uploadFile = uploadFile;
		function uploadFile() {

			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			}
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                	UIControlService.unloadLoading();
                	console.info("response:" + JSON.stringify(response));
                	if (response.status == 200) {
                		var url = response.data.Url;
                		vm.pathFile = url;
                		vm.name = response.data.FileName;
                		var s = response.data.FileLength;
                		if (vm.flag == 0) {

                			vm.size = Math.floor(s)
                		}

                		if (vm.flag == 1) {
                			vm.size = Math.floor(s / (1024));
                		}
                		save();

                	} else {
                		UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	UIControlService.msg_growl("error", "MESSAGE.API")
                	UIControlService.unloadLoading();
                });
		}

		vm.jLoad = jLoad;
		function jLoad() {
		    AssessmentPrequalService.loadDokumenCSMS({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorPrequalID: vm.VendorPrequalID
		    }, function (reply) {
		        if (reply.status === 200) {
		            vm.data = reply.data[0];
		            UIControlService.unloadLoading();
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan data" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.valid = true;
		vm.notvalid = function notvalid() {
		    vm.valid = false;
		}


		vm.Submit = Submit;
		function Submit(flag) {
		    vm.valid = flag;
		    if (vm.remark == undefined) {
		        vm.remark = "";
		    }
		    AssessmentPrequalService.InsertSubmit({
		        PrequalStepID: vm.PrequalStepAssID,
		        VendorPrequalID: vm.VendorPrequalID,
		        IsAgree: flag,
		        VEPID: vm.VEPID,
		        Remark: vm.remark
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            $state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
		        } else {
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}


		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.save = save;
		function save() {
		    AssessmentPrequalService.insertDocCSMS({
                Status: vm.PrequalStepID,
                Keyword: vm.pathFile,
                column: vm.VendorPrequalID
		    },
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "MESSAGE.SUCC_SAVE_DATA");
                        init();
                    }
                    else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_DATA");
                        return;
                    }
                },
                function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                });
		}
	}
})();