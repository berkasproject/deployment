﻿(function () {
    'use strict';

    angular.module("app")
    .controller("AdministrasiAssPrequalCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'AssessmentPrequalService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, AssessmentPrequalService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams) {
        var vm = this;
        vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
        vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
        vm.VEPID = Number($stateParams.VEPID);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.textSearch = '';
        vm.listPengumuman = [];
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.step = [];
        vm.init = init;
        vm.oriDate = new Date();
        vm.listPersonalPrequal = [];
        vm.listPersonal = [];
        vm.addressFlagPrequal = 0;
        function init() {
            $translatePartialLoader.addPart('assessment-prakualifikasi');
            $translatePartialLoader.addPart('data-administrasi');
            $translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
            $translatePartialLoader.addPart("permintaan-ubah-data");
            loadStep();
            loadSetupStep();
        }

        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            //console.info("stepass:" + vm.PrequalStepAssID);
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID,
                Offset: vm.VEPID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //console.info("reply" + JSON.stringify(reply.data));
                    vm.isEntry = reply.data.IsNotStarted;
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID;
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    vm.remark = reply.data.Remark;
                    loadVendorContactPrequal();
                    loadVendorCurrencyPrequal();
                    loadVendorCommodityPrequal();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadStep = loadStep;
        function loadStep() {
            AssessmentPrequalService.selectStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.list = [];
            AssessmentPrequalService.selectDetailEntry({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data;
                    vm.list.forEach(function (list) {
                        if (list.VendorEntryPrequal.CreatedDate !== null) {
                            list.VendorEntryPrequal.CreatedDate = UIControlService.convertDateTime(list.VendorEntryPrequal.CreatedDate);
                            console.info(list);
                        }

                    });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorContactPrequal = loadVendorContactPrequal;
        function loadVendorContactPrequal() {
            vm.iplusPrequal = 0;
            AssessmentPrequalService.loadContactPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listContactPrequal = reply.data;
                    console.info("listc:" + JSON.stringify(vm.listContactPrequal));
                    vm.listContactPrequal.forEach(function (contact) {
                        if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.VendorNamePrequal = contact.Vendor.VendorName;
                            if (contact.Vendor.SysReference1 != null) {
                                vm.PemasokPrequal = contact.Vendor.SysReference1.Value;
                            }
                            vm.TipePrequal = contact.Vendor.SysReference.Value;
                            vm.CountryPrequal = contact.Contact.Address.State.Country.Code;
                            vm.BusinessVendor =  contact.Vendor.Vendor.business != null ?  contact.Vendor.Vendor.business.Name : "";
                            vm.PKPNumberPrequal = contact.Vendor.PKPNumber;
                            vm.PKPUrlPrequal = contact.Vendor.PKPUrl;
                            vm.PhonePrequal = contact.Contact.Phone;
                            vm.FaxPrequal = contact.Contact.Fax;
                            vm.EmailCompanyPrequal = contact.Contact.Email;
                            vm.WebsitePrequal = contact.Contact.Website;
                            vm.NpwpUrlPrequal = contact.Vendor.NpwpUrl;
                            vm.Associate = contact.Vendor.AssociationID;
                            if (vm.Associate != null) {
                                vm.NamaAsosiasi = contact.Vendor.MstAssociation.AssosiationName;
                            }
                        }
                        else if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
                            if (vm.iplusPrequal == 0) vm.EmailPrequal = contact.Contact.Email;
                            vm.iplusPrequal++;
                            vm.listPersonalPrequal.push(contact);
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && contact.IsPrimary === null) {
                            vm.addressFlagPrequal = 1;
                            vm.ContactNamePrequal = "Kantor Pusat";
                            if (contact.Contact.Address.StateID != null) {
                                if (contact.Contact.Address.CityID != null) {
                                    if (contact.Contact.Address.DistrictID != null) vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.Distric.Name + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                    else vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                }
                                else vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                            }
                            else vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                            vm.addressInfoPrequal = contact.Contact.Address.AddressInfo;
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && contact.IsPrimary === null) {
                            if (vm.addressFlagPrequal == 0) {
                                vm.ContactNamePrequal = "Kantor Cabang";
                                vm.listOfficeAddress = contact.Contact;
                                vm.VendorContactType = contact.VendorContactType;
                                if (contact.Contact.Address.StateID != null) {
                                    if (contact.Contact.Address.CityID != null) {
                                        if (contact.Contact.Address.DistrictID != null) vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.Distric.Name + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                        else vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                    }
                                    else vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                                }
                                else vm.address1Prequal = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                                vm.addressInfoPrequal = contact.Contact.Address.AddressInfo;
                            }
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && contact.IsPrimary === 2) {
                            vm.addressFlagAlternatif = 1;
                            vm.listOfficeAddressAlternatif = contact.Contact;
                            vm.VendorContactTypeAlternatif = contact.VendorContactType;
                            vm.ContactOfficeAlterId = contact.Contact.ContactID;
                            vm.AddressAlterId = contact.Contact.AddressID;
                            if (contact.Contact.Address.StateID != null) {
                                if (contact.Contact.Address.CityID != null) {
                                    if (contact.Contact.Address.DistrictID != null) vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.Distric.Name + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                    else vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                }
                                else vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                            }
                            else vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                            vm.addressInfo = contact.Contact.Address.AddressInfo;
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && contact.IsPrimary === 2) {
                            if (vm.addressFlagAlternatif == 0) {
                                vm.listOfficeAddressAlternatif = contact.Contact;
                                vm.ContactOfficeAlterId = contact.Contact.ContactID;
                                vm.AddressAlterId = contact.Contact.AddressID;
                                vm.VendorContactTypeAlternatif = contact.VendorContactType;
                                if (contact.Contact.Address.StateID != null) {
                                    if (contact.Contact.Address.CityID != null) {
                                        if (contact.Contact.Address.DistrictID != null) vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.Distric.Name + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                        else vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.City.Name + ", " + contact.Contact.Address.State.Name;
                                    }
                                    else vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                                }
                                else vm.addressinfo = contact.Contact.Address.AddressInfo + ", " + contact.Contact.Address.State.Name;
                                vm.addressInfo = contact.Contact.Address.AddressInfo;
                            }
                        }
                    });
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendorCurrencyPrequal = loadVendorCurrencyPrequal;
        function loadVendorCurrencyPrequal() {
            AssessmentPrequalService.loadCurrencyPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listCurrencyPrequal = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendorCommodityPrequal = loadVendorCommodityPrequal;
        function loadVendorCommodityPrequal() {
            AssessmentPrequalService.loadCommodityPrequal({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listCommodityPrequal = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendorContact = loadVendorContact;
        function loadVendorContact() {
            vm.iplus = 0;
            AssessmentPrequalService.loadContact({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listContact = reply.data;
                    vm.listContact.forEach(function (contact) {
                        if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.VendorName = contact.Vendor.VendorName;
                            if (contact.Vendor.SupplierType !== null) vm.Pemasok = contact.Vendor.SupplierType.Value;
                            vm.Tipe = contact.Vendor.VendorType.Value;
                            vm.Country = contact.Contact.Address.State.Country.Code;
                            vm.BusinessVendor = contact.Vendor.business.Name;
                            //vm.Associate = contact.Vendor.MstAssociation.AssosiasionName;
                            vm.PKPNumber = contact.Vendor.PKPNumber;
                            vm.PKPUrl = contact.Vendor.PKPUrl;
                            vm.Phone = contact.Contact.Phone;
                            vm.Fax= contact.Contact.Fax;
                            vm.Email = contact.Contact.Email;
                            vm.Website = contact.Contact.Website;
                        }
                        else if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
                            if (vm.iplus == 0) vm.Email = contact.Contact.Email;
                            vm.iplus++;
                            vm.listPersonal.push(contact);
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && contact.IsPrimary === null) {
                            vm.addressFlag = 1;
                            vm.ContactName = "Kantor Pusat";
                            vm.addressInfo = contact.Contact.Address.AddressInfo.split(' ');
                            for (var y = 0; y < (vm.addressInfo.length - 1) ; y++) {
                                if (y == 0) vm.address1 = (vm.addressInfo[y] + ' ');
                                else vm.address1 += (vm.addressInfo[y] + " ");
                            }
                            vm.cekAddress = vm.address1;
                            vm.postcalcode = vm.addressInfo[vm.addressInfo.length - 1];
                            vm.cekPostCode = vm.postcalcode;
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && contact.IsPrimary === null) {
                            if (vm.addressFlag == 0) {
                                vm.ContactName = "Kantor Cabang";
                                vm.listOfficeAddress = contact.Contact;
                                vm.VendorContactType = contact.VendorContactType;
                                vm.addressInfo = contact.Contact.Address.AddressInfo.split(' ');
                                for (var y = 0; y < (vm.addressInfo.length - 1) ; y++) {
                                    if (y == 0) vm.address1 = (vm.addressInfo[y] + ' ');
                                    else vm.address1 += (vm.addressInfol[y] + " ");
                                }
                            }
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && contact.IsPrimary === 2) {
                            vm.addressFlagAlternatif = 1;
                            vm.listOfficeAddressAlternatif = contact.Contact;
                            vm.VendorContactTypeAlternatif = contact.VendorContactType;
                            vm.addressInfo = contact.Contact.Address.AddressInfo.split(' ');
                            vm.ContactOfficeAlterId = contact.Contact.ContactID;
                            vm.AddressAlterId = contact.Contact.AddressID;
                            for (var y = 0; y < (vm.addressInfo.length - 1) ; y++) {
                                if (y == 0) vm.addressinfo = vm.addressInfo[y] + ' ';
                                else vm.addressinfo += (vm.addressInfo[y] + " ");
                            }
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && contact.IsPrimary === 2) {
                            if (vm.addressFlagAlternatif == 0) {
                                vm.listOfficeAddressAlternatif = contact.Contact;
                                vm.ContactOfficeAlterId = contact.Contact.ContactID;
                                vm.AddressAlterId = contact.Contact.AddressID;
                                vm.VendorContactTypeAlternatif = contact.VendorContactType;
                                vm.addressInfo = contact.Contact.Address.AddressInfo.split(' ');
                                for (var y = 0; y < (vm.addressInfo.length - 1) ; y++) {
                                    if (y == 0) vm.addressinfo = vm.addressInfo[y] + ' ';
                                    else vm.addressinfo += (vm.addressInfo[y] + " ");
                                }
                            }
                        }
                    });
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendorCurrency = loadVendorCurrency;
        function loadVendorCurrency() {
            AssessmentPrequalService.loadCurrency({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listCurrency = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendorCommodity = loadVendorCommodity;
        function loadVendorCommodity() {
            AssessmentPrequalService.loadCommodity({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listCommodity = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        };

        vm.view = view;
        function view(data) {
            $state.transitionTo('assessment-prakualifikasi-detail', { PrequalStepID: vm.PrequalStepID, VendorPrequalID: data.VendorPrequalID });
        }

        vm.valid = true;
        vm.notvalid = function notvalid() {
            vm.valid = false;
        }


        vm.Submit = Submit;
        function Submit(flag) {
            vm.valid = flag;
            if (vm.remark == undefined) {
                vm.remark = "";
            }
            AssessmentPrequalService.InsertSubmit({
                PrequalStepID: vm.PrequalStepAssID,
                VendorPrequalID: vm.VendorPrequalID,
                IsAgree: flag,
                VEPID: vm.VEPID,
                Remark:vm.remark
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $state.go('assessment-prakualifikasi-detail', {SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
    }
})();;