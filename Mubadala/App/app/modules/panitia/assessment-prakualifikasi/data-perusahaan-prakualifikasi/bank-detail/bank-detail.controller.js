﻿(function () {
	'use strict';

	angular.module("app").controller("BankDetailCtrl", ctrl);

	ctrl.$inject = ['AssessmentPrequalService', '$state', '$stateParams', '$translatePartialLoader', '$uibModal', 'BankDetailPrequalService', 'BankDetailService', 'UIControlService', 'GlobalConstantService', 'VerifiedSendService'];
	/* @ngInject */
	function ctrl(AssessmentPrequalService, $state, $stateParams, $translatePartialLoader, $uibModal, BankDetailPrequalService, BankDetailService, UIControlService, GlobalConstantService, VerifiedSendService) {
	    var vm = this;
	    vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
	    vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
	    vm.VEPID = Number($stateParams.VEPID);
		vm.totalItems = 0;
		vm.currentPage = 0;
		vm.maxSize = 10;
		vm.page_id = 35;
		vm.menuhome = 0;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.userId = 0;
		vm.jLoad = jLoad;
		vm.bank = [];
		vm.ambilUrl;
		vm.ambilUrl2;

		//vm.Kata = "";
		vm.VendorID;

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('assessment-prakualifikasi');
		    $translatePartialLoader.addPart('bank-detail');
		    //loadPrequalStep();
		    loadSetupStep();
        }


        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID,
                Offset: vm.VEPID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isEntry = reply.data.IsNotStarted;
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID;
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    vm.remark = reply.data.Remark;
                    jLoad(1);
                    jLoadPrequal(1);

                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    BankDetailPrequalService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		            console.info("isentry:" + vm.isEntry);
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("Silahkan Tunggu...");
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			AssessmentPrequalService.loadBankDetail({
			    PrequalSetupStepID: vm.PrequalStepID,
			    VendorPrequalID: vm.VendorPrequalID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.bank = data;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.jLoadPrequal = jLoadPrequal;
		function jLoadPrequal(current) {
		    UIControlService.loadLoading("Silahkan Tunggu...");
		    vm.currentPage = current;
		    var offset = (current * 10) - 10;
		    AssessmentPrequalService.loadBankDetailPrequal({
		        PrequalSetupStepID: vm.PrequalStepID,
		        VendorPrequalID: vm.VendorPrequalID
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            var data = reply.data;
		            vm.bankPrequal = data;
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan data" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.tambah = tambah;
		function tambah(data) {
		    var data = {
				act: 1,
				item: {
				    PrequalStepID: vm.PrequalStepID,
                    VendorPrequalID: vm.VendorPrequalID
				}
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/bank-detail/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.edit = edit;
		function edit(data) {
		    data.PrequalStepID = vm.PrequalStepID;
			var data = {
				act: 0,
				item: data
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/panitia/assessment-prakualifikasi/data-perusahaan-prakualifikasi/bank-detail/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
			    window.location.reload();
			});
		}

		vm.remove = remove;
		function remove(doc) {
			bootbox.confirm('<h3 class="afta-font center-block">' + "Yakin ingin menghapus?" + '<h3>', function (reply) {
				if (reply) {
					//UIControlService.loadLoading(loadmsg);
				    AssessmentPrequalService.removeBankDetailPrequal({
						ID: doc.ID
					}, function (reply2) {
						UIControlService.unloadLoading();
						if (reply2.status === 200) {
							UIControlService.msg_growl('notice', 'data berhasil di hapus');
							window.location.reload();
						} else {
							UIControlService.msg_growl('error', 'gagal menghapus');
						}
					}, function (error) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl('error', 'MESSAGE.ERR_DEL');
					});
				}
			});
		};

		vm.valid = true;
		vm.notvalid = function notvalid() {
		    vm.valid = false;
		}

		vm.Submit = Submit;
		function Submit(flag) {
		    AssessmentPrequalService.InsertSubmit({
		        PrequalStepID: vm.PrequalStepAssID,
		        VendorPrequalID: vm.VendorPrequalID,
		        IsAgree: flag,
		        VEPID: vm.VEPID,
                Remark: vm.remark
		    }, function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            $state.go('assessment-prakualifikasi-detail', { SetupStepID: vm.PrequalSetupID, PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: vm.VendorPrequalID });
		        } else {
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		    });
		}
	}
})();