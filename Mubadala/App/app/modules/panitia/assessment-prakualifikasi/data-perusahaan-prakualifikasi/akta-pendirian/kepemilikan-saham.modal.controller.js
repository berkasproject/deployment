﻿(function () {
	'use strict';

	angular.module("app").controller("formKepemilikanSahamCtrl", ctrl);

	ctrl.$inject = ['AssessmentPrequalService', '$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'AktaPendirianService', 'AktaPendirianPrequalService', 'CommonEngineService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'VendorRegistrationService'];
	/* @ngInject */
	function ctrl(AssessmentPrequalService, $http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, AktaPendirianService, AktaPendirianPrequalService, CommonEngineService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService) {

		var loadmsg = 'MESSAGE.LOADING';
		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.currencyList = [];
		vm.stockUnits = [];
		vm.stockOption =[];
		vm.stock = {};
		vm.file;
		vm.isDobCalendarOpened = false;
		vm.nama = "NM_KOSONG";
		vm.tgl = "TGL_KOSONG";
		vm.identitas = "NO_KOSONG";
		vm.VendorPrequalID = item.VendorPrequalID;
		vm.PrequalStepID = item.PrequalStepID;
		vm.init = init;
		function init() {
		    console.info(item);
		    vm.StockTypeID = item.StockTypeID;
		    vm.TotalPercent = item.TotalPercent;
		    UIControlService.loadLoadingModal(loadmsg);

			//Konfigurasi upload disamakan dengan yang ada di halaman pendaftaran
			UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.ID", function (response) {
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
					
					AktaPendirianService.GetCurrencies(function (response) {
					    vm.currencyList = response.data;
					    AktaPendirianService.getStockOption(function (response){
					        vm.stockOption = response.data;
					        vm.stock = item.stock;
					        vm.PrequalStepID = item.PrequalStepID;
					        AktaPendirianService.GetStockTypes(function (response) {
					            vm.stockUnits = response.data;
					            vm.stock = item.stock;
					            if (vm.StockTypeID != 0) {
					                console.info(vm.StockTypeID);
					                vm.stockUnits.forEach(function (data) {
					                    if (vm.StockTypeID == data.RefID) vm.stock.UnitID = data.RefID;
					                })
					            }
					            vm.onUnitChange();
					            VendorRegistrationService.getUploadPrefix(function (response) {
					                var prefixes = response.data;
					                vm.prefixes = {};
					                for (var i = 0; i < prefixes.length; i++) {
					                    vm.prefixes[prefixes[i].Name] = prefixes[i];
					                }
					                UIControlService.unloadLoadingModal();
					            }, function (error) {
					                UIControlService.unloadLoadingModal();
					                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_PREFIXES')
					            }
                                );
					        }, function (error) {
					            UIControlService.unloadLoadingModal();
					            UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TYPES')
					        }
                            );
					    }, function (err) {
					        UIControlService.msg_growl("error", "MESSAGE.API");
					        UIControlService.unloadLoading();
					    });
					}, function (error) {
					    UIControlService.unloadLoadingModal();
						UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_CURRENCY')
                    });
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
			});
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.openDobCalendar = openDobCalendar;
		function openDobCalendar() {
			vm.isDobCalendarOpened = true;
		}

		vm.onUnitChange = onUnitChange;
		function onUnitChange() {
			vm.showCurrencyField = false;
			for (var i = 0; i < vm.stockUnits.length; i++) {
				if (vm.stockUnits[i].RefID === vm.stock.UnitID) {
					vm.showCurrencyField = vm.stockUnits[i].Name === 'STOCK_UNIT_CURRENCY';
					break;
				}
			}
		}

		vm.labelChange = labelChange;
		function labelChange() {
		    for (var i = 0; i < vm.stockOption.length; i++) {
		        if (vm.stockOption[i].RefID === vm.stock.stockTypeID) {
		            vm.tes = vm.stockOption[i].Value;
		            break;
		        }
		    }
		    if (vm.tes == 'STOCK_PERSONAL')
		    {
		        //PERSONAL
		        vm.nama = "NM_PERSONAL";
		        vm.tgl = "TGL_PERSONAL";
		        vm.identitas = "NO_PERSONAL";
		    } else if (vm.tes == 'STOCK_COMPANY') {
		        //COMPANY
		        vm.nama = "NM_COMPANY";
		        vm.tgl = "TGL_COMPANY";
		        vm.identitas = "NO_COMPANY";
		    }
		}

		vm.validateAge = validateAge;
		function validateAge(inputDate) {
		    if (vm.stock.stockTypeID == 4309) {
		        var validatedAge = false;
		        var birthDate = moment(inputDate).format("DD-MM");;
		        var dateNow = moment().format("DD-MM");
		        var birthYear = moment(inputDate).format("YYYY");;
		        var yearNow = moment().format("YYYY");
		        var yearAge = yearNow - birthYear;
		        if (yearAge > 17) {
		            var validatedAge = true;
		        } else if (yearAge === 17) {
		            if (birthDate < dateNow || birthDate === dateNow) {
		                var validatedAge = true;
		            }
		        }

		        if (validatedAge === false) {
		            UIControlService.msg_growl('error', "ERRORS.AGE_UNDER17");
		            vm.stock.OwnerDOBDate = null;
		        }
		    }
		}


		vm.save = save;
		function save() {
		    var selisih = +vm.TotalPercent + +vm.stock.Quantity;
		    if (vm.stock.Quantity == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_STOCKCANNOTZERO");
		        return;
		    }
		    else if ((vm.stock.UnitID == 23 && vm.stock.Quantity > 100) || (vm.stock.UnitID == 23 && selisih > 100)) {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_STOCKMAX");
		        return;
		    }
		    else {
		        AssessmentPrequalService.isAnotherStockHolder({
                    PrequalStepID: vm.PrequalStepID,
		            VendorPrequalID: vm.VendorPrequalID,
		            OwnerID: vm.stock.OwnerID
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200 && reply.data === true) {
		                UIControlService.msg_growl('error', "ERRORS.IS_ANOTHER_STOCKHOLDER");
		            } else {
		                uploadAndSave();
		            }
		        }, function (error) {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl('error', "ERRORS.CANNOT_VERIFY_STOCKHOLDER");
		            uploadAndSave();
		        });
		    }
		};

		function uploadAndSave() {
			if (!vm.file && !vm.stock.OwnerURL) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return;
			}
			if (vm.file) {
				uploadFile(vm.file);
			} else {
				saveVendorStock(vm.stock.OwnerURL);
			}
		}

		function uploadFile(file) {
			if (validateFileType(file, vm.idUploadConfigs)) {
				upload(file, vm.idFileSize, vm.idFileTypes);
			}
		}

		function validateFileType(file, idUploadConfigs) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		function upload(file, config, types) {

			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}
			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UIControlService.loadLoadingModal(loadmsg);
			console.info(vm.stock);
			UploaderService.uploadSingleFileStock(vm.VendorPrequalID, vm.stock.OwnerID,file, size, types, function (reply) {
				if (reply.status == 200) {
					UIControlService.unloadLoadingModal();
					var url = reply.data.Url;
					saveVendorStock(url);
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
			});
		};

		function saveVendorStock(url) {
			vm.stock.OwnerURL = url;
			vm.stock.Position;
			vm.stock.OwnerDOB = UIControlService.getStrDate(vm.stock.OwnerDOBDate);
			vm.stock.PrequalStepID = vm.PrequalStepID;
			vm.stock.VendorPrequalID = vm.VendorPrequalID;
			UIControlService.loadLoadingModal(loadmsg);
			AssessmentPrequalService.UpdateStock(vm.stock, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE_VSTOCK");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_VSTOCK");
				}
			}, function (error) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_VSTOCK");
			});
		};

		vm.close = close;
		function close() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();