(function () {
    'use strict';

    angular.module("app").controller("KuisionerPrakualifikasiController", ctrl);

    ctrl.$inject = ['$filter', '$scope', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'PrequalQuestionnaireService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService','AssessmentPrequalService'];
    function ctrl($filter, $scope, $http, $translate, $translatePartialLoader, $location, SocketService,
        PrequalQuestionnaireService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, AssessmentPrequalService) {

        var vm = this;
        vm.PrequalStepID = Number($stateParams.PrequalStepID);
        vm.VendorID = Number($stateParams.VendorID);
        vm.PrequalSetupID = Number($stateParams.PrequalSetupID);

        vm.prequalQuest = {};
        vm.parentCriterias = [];
        vm.selectedParent = {};
        vm.allCriterias = [];
        vm.level2Criterias = [];

        vm.bolehakses = true;
        //vm.isTime = false;

        vm.thereIsChange = false;

        vm.init = init;
        function init() {

            UIControlService.loadLoading("MESSAGE.LOADING");            
            PrequalQuestionnaireService.getParents({
                ID: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                vm.parentCriterias = reply.data;
                vm.selectedCategory = vm.parentCriterias[0].Id;
                vm.selectedParent = vm.selectedCategory;
                loadKuisioner();
                loadStep();
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "MESSAGE.FAIL_GETDATA");
            });
        }

        vm.loadStep = loadStep;
        function loadStep() {
            $translatePartialLoader.addPart('assessment-prakualifikasi');
            AssessmentPrequalService.selectStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    console.info("step" + JSON.stringify(vm.step));
                    vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                    vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
                    vm.bolehakses = vm.step.IsNotStarted;
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        function recalculateScore() {
            vm.totalScore = 0;
            vm.level2Criterias.forEach(function (lv2) {
                lv2.TotalScore = 0;
                lv2.Children.forEach(function (lv3) {
                    if (lv3.SelectedStandard) {
                        lv2.TotalScore += lv3.SelectedStandard.MaxScore;
                    }
                });
                vm.totalScore += lv2.TotalScore;
            });
        }

        vm.onCategoryChange = onCategoryChange;
        function onCategoryChange()
        {
            if (vm.thereIsChange) {
                bootbox.confirm("MESSAGE.CONFIRM", function (yes) {
                    if (yes) {
                        vm.selectedParent = vm.selectedCategory;
                        loadKuisioner();
                    } else {
                        vm.selectedCategory = vm.selectedParent;
                    }
                });
            }
            else {
                vm.selectedParent = vm.selectedCategory;
                loadKuisioner();
            }
        }

        vm.onOptionSelect = onOptionSelect;
        function onOptionSelect() {
            vm.thereIsChange = true;
            recalculateScore();
        }

        vm.loadKuisioner = loadKuisioner;
        function loadKuisioner() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            PrequalQuestionnaireService.getVendorPrequalQuest({
                VendorID: vm.VendorID,
                PrequalSetupStepID: vm.PrequalStepID
            }, function (reply) {
                vm.prequalQuest = reply.data;
                var prequalQuestDetails = reply.data.VendorPrequalQuestionnaireDetails;
                PrequalQuestionnaireService.getSubCriterias({
                    VPEMDCriteriaID: vm.selectedParent,
                    VendorID: vm.VendorID,
                    PrequalSetupStepID: vm.PrequalStepID
                }, function (reply) {
                    UIControlService.unloadLoading();
                    vm.allCriterias = reply.data;
                    //console.info("all" + JSON.stringify(vm.allCriterias));
                    var level2Criterias = [];
                    var level3Criterias = [];
                    for (var i = 0; i < vm.allCriterias.length; i++) {
                        for (var j = 0; j < prequalQuestDetails.length; j++) {
                            if (vm.allCriterias[i].Id === prequalQuestDetails[j].VPEMDCriteriaID) {
                                vm.allCriterias[i].Standards.forEach(function (std) {
                                    if (std.Id === prequalQuestDetails[j].SelectedStandard) {
                                        vm.allCriterias[i].SelectedStandard = std;
                                    }
                                });
                                break;
                            }
                        }

                        if (vm.allCriterias[i].Level === 2) {
                            level2Criterias.push(vm.allCriterias[i]);
                        }
                        else if (vm.allCriterias[i].Level === 3) {
                            level3Criterias.push(vm.allCriterias[i]);
                        }
                    }

                    for (var i = 0; i < level2Criterias.length; i++) {
                        level2Criterias[i].Children = [];
                        for (var j = 0; j < level3Criterias.length; j++) {
                            if (level3Criterias[j].Parent === level2Criterias[i].CriteriaId) {
                                level2Criterias[i].Children.push(level3Criterias[j]);
                            }
                        }
                    }
                    vm.level2Criterias = level2Criterias;
                    vm.thereIsChange = false;
                    recalculateScore();
                }, function (err) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
                });
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", "MESSAGE.FAIL_GETDATA");
            });
        }

        vm.saveQuestionnaire = saveQuestionnaire;
        function saveQuestionnaire() {
            UIControlService.loadLoading("MESSAGE.LOADING");

            var details = [];
            vm.allCriterias.forEach(function (crit) {
                if (crit.SelectedStandard) {
                    details.push({
                        VPEMDCriteriaID: crit.Id,
                        SelectedStandard: crit.SelectedStandard.Id
                    });
                }
            });

            PrequalQuestionnaireService.saveQuestionnaire({
                PrequalSetupStepID: vm.PrequalStepID,
                VendorID: vm.VendorID,
                VendorPrequalQuestionnaireDetails: details
            }, function (reply) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('success', "MESSAGE.SUCC_SAVE");
                loadKuisioner();
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "MESSAGE.ERR_SAVE");
            });
        }
        
        vm.back = back;
        function back(data) {
            $state.transitionTo('assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepID, PrequalSetupID: vm.PrequalSetupID });
        }
    }
})();

