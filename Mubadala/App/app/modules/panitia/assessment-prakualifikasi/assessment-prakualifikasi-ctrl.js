﻿(function () {
    'use strict';

    angular.module("app")
    .controller("AssessmentPrequalCtrl", ctrl);

    ctrl.$inject = ['$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'AssessmentPrequalService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($timeout, $http, $translate, $translatePartialLoader, $location, SocketService, AssessmentPrequalService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams) {
        var vm = this;
        vm.PrequalStepID = Number($stateParams.PrequalStepID);
        vm.PrequalSetupID = Number($stateParams.PrequalSetupID);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.textSearch = '';
        vm.listPengumuman = [];
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.step = [];



        vm.init = init;
        function init() {
            loadStep();
            jLoad(1);
        }

        vm.loadStep = loadStep;
        function loadStep() {
            $translatePartialLoader.addPart('assessment-prakualifikasi');
            AssessmentPrequalService.selectStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    //console.info("step" + JSON.stringify(vm.step));
                    vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                    vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            vm.list = [];
            AssessmentPrequalService.select({
                Status: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data;
                    console.info("list:" + JSON.stringify(vm.list));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Penilai" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.do_summary = do_summary;
        function do_summary() {
            var data = {
                PrequalSetupID:vm.PrequalSetupID,
                PrequalStepID:vm.PrequalStepID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/panitia/assessment-prakualifikasi/summary.html',
                controller: 'SummaryPQVerificationCtrl',
                controllerAs: 'SummaryPQVerificationCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        }

        //vm.kembali = kembali;
        //function kembali() {
        //    $state.transitionTo('evaluation-technical', { TenderRefID: vm.TenderRefID, StepID: vm.step[0].ID, ProcPackType: vm.ProcPackType });
        //};

        vm.view = view;
        function view(data) {
            $state.transitionTo('assessment-prakualifikasi-detail', { PrequalStepID: vm.PrequalStepID, VendorPrequalID: data.VendorPrequalID });
        }

        vm.printAss = printAss;
        function printAss(dt, exportPO) {
            vm.exportAss = exportPO;
            vm.popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');

            AssessmentPrequalService.LoadAssessmentReport({
                VendorPrequalID: dt.VendorPrequalID,
                PrequalSetupStep: {
                    PrequalSetupID: vm.PrequalSetupID
                }

            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.dataReport = reply.data;
                    vm.dataReport.contact.forEach(function (data) {
                        if (data.VendorContactTypeID == 20) {
                            vm.Phone = data.Contact.Phone;
                            vm.Fax = data.Contact.Fax;
                            vm.Email = data.Contact.Email;
                        }
                        if (data.VendorContactTypeID == 1042) {
                            if (data.Contact.Address.StateID != null) {
                                if (data.Contact.Address.CityID != null) {
                                    if (data.Contact.Address.DistrictID != null) vm.addressPrequal = data.Contact.Address.AddressInfo + ", " + data.Contact.Address.Distric.Name + ", " + data.Contact.Address.City.Name + ", " + data.Contact.Address.State.Name + ", " + data.Contact.Address.State.Country.Name;
                                    else vm.addressPrequal = data.Contact.Address.AddressInfo + ", " + data.Contact.Address.City.Name + ", " + data.Contact.Address.State.Name + ", " + data.Contact.Address.State.Country.Name;
                                }
                                else vm.addressPrequal = data.Contact.Address.AddressInfo + ", " + data.Contact.Address.State.Name + ", " + data.Contact.Address.State.Country.Name;
                            }
                            else vm.addressPrequal = data.Contact.Address.AddressInfo;
                        }
                    });
                   
                    $timeout(function () {
                        vm.innerContents = document.getElementById(vm.exportAss).innerHTML;
                        vm.popupWindow.document.open();
                        vm.popupWindow.document.write('<html><head><title>Lembar Verifikasi Berkas</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + vm.innerContents + '</body></html>');
                        vm.popupWindow.document.close();
                    }, 3000);
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });


            //$uibModalInstance.close();
        }
    }
})();;