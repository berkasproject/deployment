﻿(function () {
    'use strict';

    angular.module("app")
    .controller("AssessmentPrequalDetailCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'AssessmentPrequalService', '$state', 'UIControlService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, AssessmentPrequalService,
        $state, UIControlService, $uibModal, GlobalConstantService, $stateParams) {
        var vm = this;
        vm.PrequalStepAssID = Number($stateParams.PrequalStepID);
        vm.VendorPrequalID = Number($stateParams.VendorPrequalID);
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.textSearch = '';
        vm.listPengumuman = [];
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.step = [];
        vm.init = init;
        vm.oriDate = new Date();
        function init() {
            $translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
            $translatePartialLoader.addPart("permintaan-ubah-data");
            $translatePartialLoader.addPart("assessment-prakualifikasi");
            loadStep();
            loadSetupStep();
        }

        vm.loadStep = loadStep;
        function loadStep() {
            AssessmentPrequalService.selectStep({
                Status: vm.PrequalStepAssID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.step = reply.data;
                    vm.step.StartDate = UIControlService.convertDateTime(vm.step.StartDate);
                    vm.step.EndDate = UIControlService.convertDateTime(vm.step.EndDate);
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadSetupStep = loadSetupStep;
        function loadSetupStep() {
            AssessmentPrequalService.SelectSetupStep({
                Status: vm.PrequalStepAssID,
                column: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.isEntry = reply.data.IsNotStarted;
                    console.info("isentry" + vm.isEntry);
                    vm.PrequalStepID = reply.data.PrequalStepID;
                    vm.ID = reply.data.ID;
                    vm.PrequalSetupID = reply.data.PrequalSetupID
                    vm.IsNeedRevision = reply.data.IsNeedRevision;
                    jLoad(1);
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }


        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.list = [];
            vm.flagAss = true;
            vm.flagPassed = true;
            AssessmentPrequalService.selectDetailEntry({
                PrequalSetupStepID: vm.ID,
                VendorPrequalID: vm.VendorPrequalID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.list = data;
                    vm.list.forEach(function (list) {
                        if (list.IsAgree == null) {
                            vm.flagPassed = false;
                        }
                        if (list.IsAgree == false) vm.flagAss = false;
                        if (list.VendorEntryPrequal.SubmitDate !== null) {
                            list.VendorEntryPrequal.SubmitDate = UIControlService.convertDateTime(list.VendorEntryPrequal.SubmitDate);
                        }
                    });
                } else {
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        //vm.kembali = kembali;
        //function kembali() {
        //    $state.transitionTo('evaluation-technical', { TenderRefID: vm.TenderRefID, StepID: vm.step[0].ID, ProcPackType: vm.ProcPackType });
        //};

        vm.view = view;
        function view(data) {
            $state.transitionTo('assessment-prakualifikasi-detail', { PrequalStepID: vm.PrequalStepID, VendorPrequalID: data.VendorPrequalID });
        }

        vm.DetailPrequal = DetailPrequal;
        function DetailPrequal(data, nameModule) {
            console.info("modul:" + nameModule);
            if (nameModule == "OC_ADM_LEGAL") $state.go('administrasi-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDORLICENSI") $state.go('izin-usaha-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDORSTOCK") $state.go('akta-pendirian-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_COMPANYPERSON") $state.go('pengurus-perusahaan-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDORBALANCE") $state.go('neraca-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDOREXPERTS") $state.go('data-tenaga-ahli-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDOREQUIPMENT") $state.go('data-perlengkapan-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDOREXPERIENCE") $state.go('data-pengalaman-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDORBANKDETAIL") $state.go('bank-detail-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_VENDORBUSINESSFIELD") $state.go('bidang-usaha-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "K3L_POINT") $state.go('k3l-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "VENDOR_OTHERDOCUMENT") $state.go('dokumen-lainlain-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
            if (nameModule == "OC_STATEMENTLETTER") $state.go('surat-pernyataan-assessment-prakualifikasi', { PrequalStepID: vm.PrequalStepAssID, VendorPrequalID: data.VendorEntryPrequal.VendorPrequalID, VEPID: data.VendorEntryPrequal.ID });
        }

        vm.SaveAss = SaveAss;
        function SaveAss() {
            AssessmentPrequalService.SaveAss({
                PrequalSetupStepID: vm.PrequalStepAssID,
                VendorPrequalID: vm.VendorPrequalID,
                IsPassed: vm.flagAss
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    $state.go('assessment-prakualifikasi',{ PrequalStepID: vm.PrequalStepAssID, PrequalSetupID: vm.PrequalSetupID})
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }
    }
})();;