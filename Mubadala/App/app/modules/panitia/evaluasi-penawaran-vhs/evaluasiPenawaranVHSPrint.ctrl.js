﻿(function () {
	'use strict';

	angular.module("app").controller("evaluasiPenawaranVHSPrintCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'DataPengadaanService', 'UIControlService', '$stateParams', 'EvaluasiPenawaranVHSService'];
	function ctrl($translatePartialLoader, DataPengadaanService, UIControlService, $stateParams, EvaluasiPenawaranVHSService) {
		var vm = this
		vm.stepID = Number($stateParams.StepID);
		vm.tenderRefID = Number($stateParams.TenderRefID);
		vm.procPackType = Number($stateParams.ProcPackType);
		var loadmsg = "MESSAGE.LOADING";
		vm.jumlahlembar = 1

		vm.init = init
		function init() {
			$translatePartialLoader.addPart('evaluasi-penawaran-vhs');

			DataPengadaanService.IsAllowedEdit({
				TenderRefID: vm.tenderRefID,
				ProcPackageType: vm.procPackType
			}, function (reply) {
				vm.isAllowedEdit = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHECK_IS_ALLOWED');
			});

			EvaluasiPenawaranVHSService.isItemize({
				ID: vm.stepID
			}, function (reply) {
				vm.isItemize = reply.data;
			}, function (error) {
				UIControlService.msg_growl("error", 'MESSAGE.ERR_CHK_TENDER_OPTION');
			});

			loadData();
		}

		vm.makePDF = makePDF;
		var indeks = "";
		function makePDF() {
			for (var i = 1; i <= vm.jumlahlembar; i++) {
				indeks = i.toString();
				html2canvas(document.getElementById('print' + indeks), {
					onrendered: function (canvas) {
						var data = canvas.toDataURL();
						var docDefinition = {
							content: [{
								image: data,
								width: 500
							}]
						};
						pdfMake.createPdf(docDefinition).download(vm.tenderStepData.tender.TenderName.replace('/', '-') /*+ " - page" + i + ' of ' + vm.jumlahlembar + ".pdf"*/);
					}
				});
				indeks = "";
			}
		}

		function loadData() {
			UIControlService.loadLoading(loadmsg);
			DataPengadaanService.GetStepByID({
				ID: vm.stepID
			}, function (reply) {
				vm.tenderStepData = reply.data;
				vm.tenderID = vm.tenderStepData.TenderID;
				vm.isProcess = vm.tenderStepData.StatusName === "PROCUREMENT_TYPE_PROCESS";
				EvaluasiPenawaranVHSService.getByTenderStepData({
					ID: vm.stepID,
					TenderID: vm.tenderStepData.TenderID
				}, function (reply) {
					UIControlService.unloadLoading();
					vm.evaluasi = reply.data;
					vm.noEvaluation = !vm.evaluasi.VHSOfferEvaluationVendors || vm.evaluasi.VHSOfferEvaluationVendors.length === 0;
					if (!vm.noEvaluation) {
						vm.evaluasi.VHSOfferEvaluationVendors.sort(function (a, b) {
							return b.Score - a.Score;
						});
					}
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
				});
			}, function (error) {
				UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD');
			});
		};
	}
})()