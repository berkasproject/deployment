(function () {
    'use strict';

    angular.module("app")
    .controller("setVHSItemPRAwardCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'EvaluasiPenawaranVHSService', 'UIControlService', 'CommonEngineService', 'GlobalConstantService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, SocketService, EvaluasiPenawaranVHSService, UIControlService, CommonEngineService, GlobalConstantService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        vm.itemPR = item.itemPR;
        vm.offerEntries = [];
        vm.awardedVendorName = "";
        vm.isProcess = item.isProcess;
        vm.isAllowedEdit = item.isAllowedEdit;

        vm.criterias = item.criterias;
        vm.criterias[0].CriteriaName = "Price";
        vm.criterias[1].CriteriaName = "Lead Time";
        vm.priceWeight = vm.criterias[0].Weight;
        vm.leadtimeWeight = vm.criterias[1].Weight;

        vm.init = init;
        function init() {

            var index = 0;
            item.offerEntries.forEach(function (oe) {
                var offerEntry = {
                    VendorID: oe.VendorID,
                    VendorName: oe.VendorName,
                    CurrencySymbol: oe.CurrencySymbol,
                    UnitPrice: oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].UnitPrice,
                    TotalPrice: oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].TotalPrice,
                    TotalPriceInUSD: oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].TotalPriceInUSD,
                    LeadTime: parseInt(oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].LeadTime),
                    ItemDescrip: oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].ItemDescrip,
                    Remark: oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].Remark,
                    //DocUrl: oe.VHSOfferEntryDetails[0].detail[item.itemPRIndex].DocUrl
                    ScoreDetails: []
                };
                offerEntry.LeadTime == NaN ? 0 : offerEntry.LeadTime;

                item.evaluationVendors[index++].VHSOEvaluationVendorScoreDetails.forEach(function (det) {
                    offerEntry.ScoreDetails.push({
                        Score: Number(det.Score),
                        CalculatedScore: Number(det.CalculatedScore)
                    })
                })

                if (offerEntry.VendorID === item.itemPR.AwardedVendor) {
                    vm.awardedVendorName = oe.VendorName;
                    offerEntry.isWinner = true;
                };

                vm.offerEntries.push(offerEntry);
            });

            calculateScore();
        };

        function calculateScore() {

            var minLeadTime;
            vm.offerEntries.forEach(function (entry) {
                var totalLeadTime = entry.LeadTime;
                if (totalLeadTime > 0 && (minLeadTime === undefined || totalLeadTime < minLeadTime)) {
                    minLeadTime = totalLeadTime;
                }
            });
            vm.offerEntries.forEach(function (entry) {
                var totalLeadTime = entry.LeadTime;
                entry.ScoreDetails[1].Score = totalLeadTime > 0 ? minLeadTime * 100 / totalLeadTime : 0;
                entry.ScoreDetails[1].CalculatedScore = entry.ScoreDetails[1].Score * vm.leadtimeWeight / 100;
            });

            var minPrice;
            vm.offerEntries.forEach(function (entry) {
                var totalPrice = Number(entry.TotalPriceInUSD);
                if (totalPrice > 0 && (minPrice === undefined || totalPrice < minPrice)) {
                    minPrice = totalPrice;
                }
            });
            vm.offerEntries.forEach(function (entry) {
                var totalPrice = entry.TotalPriceInUSD;
                entry.ScoreDetails[0].Score = totalPrice > 0 ? minPrice * 100 / totalPrice : 0;
                entry.ScoreDetails[0].CalculatedScore = entry.ScoreDetails[0].Score * vm.priceWeight / 100;
            });

            vm.offerEntries.forEach(function (entry) {
                entry.TotalScore = 0;
                entry.ScoreDetails.forEach(function (det) {
                    entry.TotalScore += det.CalculatedScore;
                })
            });

            vm.offerEntries.sort(function (a, b) {
                return b.TotalScore - a.TotalScore;
            });
        }

        vm.setItemAward = setItemAward;
        function setItemAward(offerEntry) {
            UIControlService.loadLoadingModal(loadmsg);
            EvaluasiPenawaranVHSService.setItemPRAward({
                ID: vm.itemPR.ID,
                AwardedVendor: offerEntry ? offerEntry.VendorID : null
            }, function (reply) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("notice", $filter('translate')('MESSAGE.SUCC_SET_ITEMPR_AWARD'));
                $uibModalInstance.close();
            }, function (error) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_SET_ITEMPR_AWARD'));
                console.info(error.Message.substr(0, 4));
                if (error.Message.substr(0, 4) === "ERR_") {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.' + error.Message));
                }
            });
        }        

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();