﻿(function () {
	'use strict';

	angular.module("app").controller("prequalCertificateVendorCtrl", ctrl);

	ctrl.$inject = ['PrequalCertificateService', 'UIControlService', '$stateParams', '$http', 'GlobalConstantService', '$translatePartialLoader'];

	function ctrl(PrequalCertificateService, UIControlService, $stateParams, $http, GlobalConstantService, $translatePartialLoader) {
		var vendorpoint = GlobalConstantService.getConstant("vendor_endpoint");
		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.stepId = Number($stateParams.SetupStepID);

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('prequal-certificate');
			getStepData();
		}

		function getStepData() {
			PrequalCertificateService.GetStepDataVendor({
				PrequalSetupStepID: vm.stepId
			}, function (reply) {
				var result = reply.data;
				vm.StartDate = result.StartDate;
				vm.EndDate = result.EndDate;
				vm.PrequalSetupName = result.PrequalSetupName;
				vm.IsPublished = result.IsPublished;
				vm.PrequalSetupID = result.PrequalSetupID;
				vm.CertificateUrl = encodeURI(result.CertificateUrl);
				UIControlService.unloadLoading();
			}, function (error) {
				UIControlService.unloadLoading();
			});
		}

		vm.downloadCert = downloadCert;
		function downloadCert(formCertificate) {
			var headers = {};
			headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

			$http({
				method: 'POST',
				url: vendorpoint + '/PrequalCertificate/GenerateCertificate',
				headers: headers,
				data: { PrequalSetupStepID: vm.stepId, PrequalSetupID: vm.PrequalSetupID },
				responseType: 'arraybuffer'
			}).success(function (data, status, headers) {
				var linkElement = document.createElement('a');
				var fileName = "PrequalificationCertificate.pdf";

				try {
					var blob = new Blob([data], { type: headers('content-type') });
					var url = window.URL.createObjectURL(blob);
					linkElement.setAttribute('href', url);
					linkElement.setAttribute('download', fileName);

					var clickEvent = new MouseEvent("click", {
						"view": window,
						"bubbles": true,
						"cancelable": false
					});

					linkElement.dispatchEvent(clickEvent);
				} catch (e) {
					console.log(e);
				}
			});
			//var a = document.createElement("a");
			//document.body.appendChild(a);

			//PrequalCertificateService.GenerateCertificate({
			//	PrequalSetupStepID: vm.stepId
			//}, function (reply) {
			//	if (reply.status === 200) {

			//		var octetStreamMime = 'application/octet-stream';
			//		var success = false;
			//		var filename = 'download.docx';
			//		var contentType = octetStreamMime;
			//		var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;

			//		if (urlCreator) {
			//			var link = document.createElement('a');
			//			if ('download' in link) {
			//				try {
			//					// Prepare a blob URL
			//					console.log("Trying download link method with simulated click ...");
			//					var blob = new Blob([reply.data], { type: contentType });
			//					var url = urlCreator.createObjectURL(blob);
			//					link.setAttribute('href', url);

			//					// Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
			//					link.setAttribute("download", filename);

			//					// Simulate clicking the download link
			//					var event = document.createEvent('MouseEvents');
			//					event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
			//					link.dispatchEvent(event);
			//					console.log("Download link method with simulated click succeeded");
			//					success = true;

			//				} catch (ex) {
			//					console.log("Download link method with simulated click failed with the following exception:");
			//					console.log(ex);
			//				}
			//			}
			//		}
			//		//var file = new Blob([reply.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
			//		//var file = new File([reply.data], fileName, { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
			//		//var fileURL = window.URL.createObjectURL(file);
			//		////window.open(fileURL);
			//		//a.href = fileURL;
			//		//a.download = fileName;
			//		//a.click();
			//	}
			//}, function (error) {
			//	UIControlService.unloadLoading();
			//});

			//var innerContents = document.getElementById(formCertificate).innerHTML;
			//var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
			//popupWindow.document.open();
			//popupWindow.document.write('<html><head><title>Prequal Cerificate</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
			//popupWindow.document.close();
		}
	}
})();