﻿(function () {
	'use strict';

	angular.module("app").controller('prequalRegistrationCtrl', ctrl);

	ctrl.$inject = ['GlobalConstantService', '$uibModalInstance', 'item', 'PengumumanPengadaanService', 'UIControlService', 'PrequalAnnounceVendorService'];
	function ctrl(GlobalConstantService, $uibModalInstance, item, PengumumanPengadaanService, UIControlService, PrequalAnnounceVendorService) {
		var vm = this;
		vm.data = item.item;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.statusMinat = false;
		vm.isOpened = false;

		vm.init = init;
		function init() {
			loadDataVendor(1);
		}

		function loadDataVendor(current) {
			var offset = (current * 10) - 10;
			PengumumanPengadaanService.selectDataVendor({
				Keyword: '', Offset: offset, Limit: 10
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.vendorID = reply.data[0].VendorID;
					//console.info("vendor ID" + JSON.stringify(vm.vendorID));
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.changeStatusMinat = changeStatusMinat;
		function changeStatusMinat() {
			PrequalAnnounceVendorService.isRegistrationOpened({
				Status: vm.data.PrequalSetupID
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					vm.isOpened = reply.data;
					//console.info("vendor ID" + JSON.stringify(vm.vendorID));
					if (vm.isOpened) vm.statusMinat = true;
					else vm.statusMinat = false;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.minat = minat;
		function minat() {
			UIControlService.loadLoadingModal("Silahkan Tunggu...");
			PrequalAnnounceVendorService.insertRegistration({
				IsSurvive: 1,
				PrequalSetupID: vm.data.PrequalSetupID,
				ID: vm.data.ID
			}, function (reply) {
				console.info(JSON.stringify(reply.status));
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "Berhasil Mendaftar Prakualifikasi");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "Gagal Mendaftar Prakualifikasi ");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "Gagal Mendaftar Prakualifikasi");
				UIControlService.unloadLoadingModal();
			});

		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
			//$uibModalInstance.close();
		};
	}
})();