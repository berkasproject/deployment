﻿(function () {
	'use strict';

	angular.module("app").controller("prequalResultAnnounceCtrl", ctrl);

	ctrl.$inject = ['$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
		'prequalResultAnnounceService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($timeout, $http, $translate, $translatePartialLoader, $location, SocketService, prequalResultAnnounceService,
		UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.StepID = Number($stateParams.SetupStepID);
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.NegoId = 0;
		vm.IsSubmit = null;
		vm.Submit = true;
		vm.PrequalSetupID
		function init() {
			$translatePartialLoader.addPart("pengumuman-hasil-prakualifikasi");
			UIControlService.loadLoading('MESSAGE.LOADING');
			loadPrequalAnnounce();
		}

		vm.loadPrequalRegis = loadPrequalRegis;
		function loadPrequalRegis() {
			PrequalResultAnnounceService.getAnnouncement({
				Status: vm.StepID
			}, function (reply) {
				if (reply.status == 200) {
					vm.dataPrequalRegist = reply.data;

					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
			});
		};

		vm.loadPrequalAnnounce = loadPrequalAnnounce;
		function loadPrequalAnnounce() {
			prequalResultAnnounceService.getAnnouncement({
				PrequalSetupStepID: vm.StepID
			}, function (reply) {
				if (reply.status == 200) {
					vm.Description = reply.data.Description;
					vm.PrequalSetupID = reply.data.ID;
					vm.VendorClassification = reply.data.VendorClassification;
					UIControlService.unloadLoading();
				}
			}, function (error) {
				UIControlService.unloadLoading();
			});
		};

		vm.DetailPrequal = DetailPrequal;
		function DetailPrequal(data, nameModule) {
			if (nameModule == "OC_ADM_LEGAL") $state.go('data-administrasi-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDORLICENSI") $state.go('izin-usaha-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDORSTOCK") $state.go('akta-pendirian-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_COMPANYPERSON") $state.go('pengurus-perusahaan-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDORBALANCE") $state.go('neraca-perusahaan-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDOREXPERTS") $state.go('tenaga-ahli-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDOREQUIPMENT") $state.go('data-perlengkapan-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDOREXPERIENCE") $state.go('data-pengalaman-prakualifikasi', { PrequalStepID: vm.StepID });
			if (nameModule == "OC_VENDORBANKDETAIL") $state.go('bank-detail-prakualifikasi', { PrequalStepID: vm.StepID });
		}

		vm.cekSubmit = cekSubmit;
		function cekSubmit() {
			if (vm.Submit == false) {
				UIControlService.msg_growl("error", "ALERTERRORSUBMIT");
				return;
			}
			else {
				VendorPrequalEntryService.UpdateSubmit({
					Status: vm.StepID
				}, function (reply) {
					if (reply.status == 200) {
						UIControlService.msg_growl("success", "Success Submit !!");
						return;
					}
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
				});
			}
		}

	}
})();
//TODO


