﻿(function () {
	'use strict';

	angular.module("app").controller('prequalAnnounceVendorDtlController', ctrl);

	ctrl.$inject = ['item', '$uibModalInstance', 'GlobalConstantService'];
	function ctrl(item, $uibModalInstance, GlobalConstantService) {
		var vm = this;
		var item = item;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.init = init;
		function init() {
			// vm.TenderCode = item.TenderStepData.tender.TenderCode;
			vm.TenderName = item.Name;
			vm.IsLocal = item.IsLokal;
			vm.IsNational = item.IsNational;
			vm.IsInternational = item.IsInternasional;
			vm.Description = item.Description;
			vm.StartDate = item.StartDate;
			vm.EndDate = item.EndDate;
			vm.RegistrationStartDate = item.RegistrationStartDate;
			vm.RegistrationEndDate = item.RegistrationEndDate;
			vm.DocUrl = item.DocUrl
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();