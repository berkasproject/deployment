﻿(function () {
	'use strict';

	angular.module("app").controller("PrequalClrChtVndrCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PrequalificationClarificationChatVendorService',
		'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, PrequalificationClarificationChatVendorService,
		UIControlService, $uibModal, $state, $stateParams, GlobalConstantService) {

		var vm = this;
		vm.PrequalSetupID = 0;
		vm.PrequalSetupStepID = Number($stateParams.SetupStepID);

		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.PrequalSetupName = "";

		vm.IsPeriodOpen = false;

		vm.stepInfo = {};
		vm.chats = [];
		vm.chatsHtml = [];
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("klarifikasi-prakualifikasi");
			UIControlService.loadLoading("");
			PrequalificationClarificationChatVendorService.GetStepInfo({
				PrequalStepID: vm.PrequalSetupStepID
			}, function (reply) {
				vm.PrequalSetupID = reply.data.PrequalSetupID;
				vm.PrequalSetupName = reply.data.PrequalSetupName;
				vm.StartDate = reply.data.StartDate;
				vm.EndDate = reply.data.EndDate;
				PrequalificationClarificationChatVendorService.GetIsPeriodOpen({
					ID: vm.PrequalSetupStepID,
				}, function (reply) {
					vm.IsPeriodOpen = reply.data;
					loadChats(1);
				}, function (error) {
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
				})
			});
		}

		//tampil isi chat
		vm.loadChats = loadChats;
		function loadChats(current) {
			vm.currentPage = current;
			UIControlService.loadLoading("");
			PrequalificationClarificationChatVendorService.GetChats({
				column: vm.PrequalSetupStepID, //tenderStepDataId
				Offset: vm.pageSize * (vm.currentPage - 1),
				Limit: vm.pageSize,
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.chats = reply.data.List;
				vm.totalItems = reply.data.Count;
			}, function (err) {
				UIControlService.msg_growl("error", "ERR_LOAD_CHATS");
				UIControlService.unloadLoading();
			});
		}

		vm.tulis = tulis;
		function tulis() {
			var data = {
				PrequalSetupStepID: vm.PrequalSetupStepID,
				PrequalSetupName: vm.PrequalSetupName,
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/prequalification/klarifikasi-prakualifikasi/write-chat.modal.html',
				controller: 'WritePrequalificationClarificationChatVCtrl',
				controllerAs: 'wccvCtrl',
				resolve: {
					item: data
				}
			});
			modalInstance.result.then(function (ini) {
				console.info(ini);

				if (ini) {
					if (vm.chats.length === 0) {
						PrequalificationClarificationChatVendorService.SendMailToCommitee({
							PrequalSetupStepID: vm.PrequalSetupStepID,
							//PrequalVendorID: vm.PrequalVendorID
						}, function (reply) {
							UIControlService.msg_growl("notice", 'SUCC_SEND_EMAIL');
						}, function (error) {
							UIControlService.msg_growl("error", 'ERR_SEND_EMAIL');
						});
					}
				}
				loadChats(vm.currentPage);
			});
		}

		vm.back = back;
		function back() {
			$state.transitionTo('detail-tahapan-prakual-vendor', { PrequalSetupID: vm.PrequalSetupID });
		}
	}
})();


