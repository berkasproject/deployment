﻿(function () {
  'use strict';

  angular.module("app").controller("WritePrequalificationClarificationChatVCtrl", ctrl);

  ctrl.$inject = ['item', '$http', '$translate', 'UploaderService', '$translatePartialLoader', '$location', 'SocketService', 'PrequalificationClarificationChatVendorService',
      'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', '$uibModalInstance', 'UploadFileConfigService'];
  function ctrl(item, $http, $translate, UploaderService, $translatePartialLoader, $location, SocketService, PrequalificationClarificationChatVendorService,
      UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, $uibModalInstance, UploadFileConfigService) {

    var vm = this;
    vm.message = "";
    vm.pathFile = null;
    

    vm.init = function () {
      console.info(item);
      vm.loadConfig();

    }

    vm.simpan = function () {
      UIControlService.loadLoadingModal("");
      if (vm.message == "" || vm.message == " ") {
        UIControlService.unloadLoadingModal();
        UIControlService.msg_growl("error", "ERR_BLANK_MESSAGE");
      }
      else {
        console.info(vm.pathFile);
        if (vm.pathFile == undefined) {
          vm.pathFile = vm.DocUrl;
        }
        PrequalificationClarificationChatVendorService.AddChat({
          IsSent: true,
          PrequalSetupStepID: item.PrequalSetupStepID,
          Message: vm.message,
          UrlDoc:vm.pathFile
        }, function (reply) {
          UIControlService.unloadLoadingModal();
          UIControlService.msg_growl("success", "SUCC_SEND_MESSAGE");
          $uibModalInstance.close();
        }, function (err) {
          UIControlService.unloadLoadingModal();
          UIControlService.msg_growl("error", "ERR_SEND_MESSAGE");
        });
      }

    }

    vm.uploadFile = function () {
      if (vm.fileUpload) {
        vm.upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
      }
      else if(vm.Id !== 0 && (vm.fileUpload == undefined || vm.fileUpload == null)) {
        vm.simpan();
      }
    }

    vm.upload = function (file, config, filters, callback) {
      vm.prefix = "Prakualifikasi_" + item.PrequalSetupName;
      var size = config.Size;
      var unit = config.SizeUnitName;

      if (unit == 'SIZE_UNIT_KB') {
        size *= 1024;
        vm.flag = 0;
      }

      if (unit == 'SIZE_UNIT_MB') {
        size *= (1024 * 1024);
        vm.flag = 1;
      }

      UploaderService.uploadSinglePrequalChat(item.PrequalSetupName, vm.prefix, file, size, filters,
          function (response) {
            UIControlService.unloadLoading();
            if (response.status == 200) {
              var url = response.data.Url;
              vm.pathFile = url;
              console.info(vm.pathFile);
              vm.name = response.data.FileName;
              var s = response.data.FileLength;
              if (vm.flag == 0) vm.size = Math.floor(s);
              else if (vm.flag == 1) vm.size = Math.floor(s / (1024));
              vm.simpan();
            } else {
              UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
              return;
            }
          },
          function (response) {
            UIControlService.msg_growl("error", "MESSAGE.API")
            UIControlService.unloadLoading();
          });
    }

    vm.loadConfig = function () {
      UploadFileConfigService.getByPageName("PAGE.VENDOR.PREQCHATUPLOAD", function (response) {
        if (response.status == 200) {
          vm.idUploadConfigs = response.data;
          vm.idFileTypes = generateFilterStrings(response.data);
          vm.idFileSize = vm.idUploadConfigs[0];
        } else {
          UIControlService.unloadLoadingModal();
          UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
        }
      }, function (err) {
        UIControlService.unloadLoadingModal();
        UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
      });
    }

    function generateFilterStrings(allowedTypes) {
      var filetypes = "";
      for (var i = 0; i < allowedTypes.length; i++) {
        filetypes += "." + allowedTypes[i].Name + ",";
      }
      return filetypes.substring(0, filetypes.length - 1);
    }

    vm.batal = batal;
    function batal() {
      $uibModalInstance.dismiss();
    }
  }
})();


