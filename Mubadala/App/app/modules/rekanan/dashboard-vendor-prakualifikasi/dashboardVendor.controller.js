(function () {
	'use strict';

	angular.module("app")
    .controller("DashboardVendorPrequalCtrl", ctrl);

	ctrl.$inject = ['$http', '$state', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'DashboardVendorService', 'UIControlService'];
	/* @ngInject */
	function ctrl($http, $state, $uibModal, $translate, $translatePartialLoader, $location, DashboardVendorService, UIControlService) {

		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		var loadingCount;

		vm.paket = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.prequal = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.prekualAnnounce = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.paketPO = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.pengumuman = {
			currentPage: 1,
			pageSize: 10,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.news = {
			currentPage: 1,
			pageSize: 10,
			totalItems: 0,
			keyword: "",
			list: []
		};
		vm.FilterColumn = 0;
		vm.textSearch = '';
		vm.maxSize = 10;
		vm.currentPage = 0;

		vm.kata = new Kata("");
		vm.orderBy = 1;
		vm.isAsc = true;

		vm.paketMRKO = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("dashboard-vendor");
			$translatePartialLoader.addPart("permintaan-ubah-data");
			UIControlService.loadLoading(loadmsg);
			loadingCount = 7;
			loadPrequal();
		}


		vm.remHtml = remHtml;
		function remHtml(text) {
			return text ? String(text).replace(/<[^>]+>/gm, '') : '';
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("Silahkan Tunggu...");
			vm.currentPage = current;
			var offset = (current * 5) - 5;
			DashboardVendorService.getDataCR({
				column: vm.FilterColumn,
				Keyword: vm.textSearch,
				Offset: (current - 1) * vm.maxSize,
				Limit: 10
			}, function (reply) {
				unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listVendors = data.List;
					if (vm.listVendors) {
						for (var i = 0; i < vm.listVendors.length; i++) {
							if (!(vm.listVendors.ChangeRequestDate === null)) {
								vm.listVendors[i].ChangeRequestDate = UIControlService.getStrDate(vm.listVendors[i].ChangeRequestDate);
							}
							if (!(vm.listVendors[i].EndChangeDate === null)) {
								vm.listVendors[i].EndChangeDate = UIControlService.getStrDate(vm.listVendors[i].EndChangeDate);
							}
						}
					}
					vm.totalItems = data.Count;
				} else {
					UIControlService.msg_growl("error", "Gagal mendapatkan data Master Badan Usaha");
					unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "Gagal akses API");
				unloadLoading();
			});
		}

		vm.reloadPaket = reloadPaket;
		function reloadPaket() {
			loadingCount = 1;
			UIControlService.loadLoading(loadmsg);
			loadPaket();
		}

		vm.searchPaket = searchPaket;
		function searchPaket() {
			vm.paket.currentPage = 1;
			UIControlService.loadLoading(loadmsg);
			loadPaket();
		}

		function loadPaket() {
			DashboardVendorService.SelectTender({
				Keyword: vm.paket.keyword,
				Offset: vm.paket.pageSize * (vm.paket.currentPage - 1),
				Limit: vm.paket.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paket.list = reply.data.List;
				vm.paket.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.reloadPrekual = reloadPrekual;
		function reloadPrekual() {
			loadingCount = 1;
			UIControlService.loadLoading(loadmsg);
			loadPrequal();
		}

		vm.searchPrekual = searchPrekual;
		function searchPrekual() {
			vm.prekual.currentPage = 1;
			UIControlService.loadLoading(loadmsg);
			loadPrekual();
		}

		function loadPrequalAnnounce() {
			DashboardVendorService.loadPrequalAnnounce({ Limit: 5 }, function (reply) {
				unloadLoading();
				vm.prekualAnnounce.list = reply.data.List;
				vm.prekualAnnounce.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_PREQUAL_ANNOUNCEMENT');
			});
		}

		function loadPrequal() {
			DashboardVendorService.SelectPrequal({
			    Keyword: vm.prequal.keyword,
			    Offset: vm.prequal.pageSize * (vm.prequal.currentPage - 1),
			    Limit: vm.prequal.pageSize
			}, function (reply) {
			    UIControlService.unloadLoading();
				vm.prequal.list = reply.data.List;
				vm.prequal.totalItems = reply.data.Count;
			}, function (error) {
			    UIControlService.unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		function loadPaket1() {
			DashboardVendorService.MonitoringPO({
				Keyword: vm.paketPO.keyword,
				Offset: vm.paketPO.pageSize * (vm.paketPO.currentPage - 1),
				Limit: vm.paketPO.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paketPO.list = reply.data.List;
				unloadLoading();
				vm.paketPO.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.detailtahapan = detailtahapan;
		function detailtahapan(tender) {
			$state.transitionTo('detail-tahapan-vendor', { TenderID: tender.TenderID });
		};

		vm.detailtahapanPrakual = detailtahapanPrakual;
		function detailtahapanPrakual(prakual) {
			$state.transitionTo('detail-tahapan-prakual-vendor', { PrequalSetupID: prakual.PrequalID });
		};

		vm.detailmonitoringPO = detailmonitoringPO;
		function detailmonitoringPO(tender) {
			$state.transitionTo('detail-monitoring-po', { ID: tender.ID });
		};

		function loadDataPengumuman(current) {
			var offset = (current * 10) - 10;
			PengumumanPengadaanService.getAllDataAnnouncementByVendor({
				Keyword: vm.pengumuman.keyword,
				Offset: vm.pengumuman.pageSize * (vm.paket.currentPage - 1),
				Limit: vm.pengumuman.pageSize
			}, function (reply) {
				//console.info("announc::"+JSON.stringify(reply));
				unloadLoading();
				if (reply.status === 200) {
					vm.pengumuman.list = reply.data.List;
					for (var i = 0; i < vm.pengumuman.list.length; i++) {
						vm.pengumuman.list[i].RegistrationStartDate = UIControlService.convertDateTime(vm.pengumuman.list[i].RegistrationStartDate);
						vm.pengumuman.list[i].RegistrationEndDate = UIControlService.convertDateTime(vm.pengumuman.list[i].RegistrationEndDate);
					}
					unloadLoading();
					vm.pengumuman.totalItems = reply.data.Count;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				unloadLoading();
			});
		}

		function loadPengumuman() {
			PengumumanPengadaanService.getDataAnnouncementByVendor({
				Keyword: vm.pengumuman.keyword,
				Offset: vm.pengumuman.pageSize * (vm.paket.currentPage - 1),
				Limit: vm.pengumuman.pageSize
			}, function (reply) {
				vm.pengumuman.list = reply.data.List;
				unloadLoading();
				vm.pengumuman.totalItems = reply.data.Count;
			}, function (err) {
				unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_ANNOUNCEMENT");
			});
		}

		function unloadLoading() {
			loadingCount--;
			if (loadingCount <= 0) {
				UIControlService.unloadLoading();
			}
		}

		vm.loadMonitoringVHS = loadMonitoringVHS;
		function loadMonitoringVHS(current) {
			DashboardVendorService.MonitoringMRKO({
				Keyword: vm.paketMRKO.keyword,
				Offset: vm.paketMRKO.pageSize * (vm.paketMRKO.currentPage - 1),
				Limit: vm.paketMRKO.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paketMRKO.list = reply.data.List;
				unloadLoading();
				vm.paketMRKO.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.detailmonitoringMRKO = detailmonitoringMRKO;
		function detailmonitoringMRKO(data) {
			var item = {
				item: data
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/detail-itemmrko-modal.html',
				controller: 'DetailItemMRKOCtrl',
				controllerAs: 'DetailItemMRKOCtrl',
				resolve: {
					item: function () {
						return item;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();

			});
		}
	}

})();

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}