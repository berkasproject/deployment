﻿(function () {
    'use strict';

    angular.module("app").controller("VerifiedSendCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', 'SocketService', 'VerifiedSendService', 'UIControlService'];
    function ctrl($translatePartialLoader, SocketService, VerifiedSendService, UIControlService) {
        var vm = this;
        vm.verified = [];
        vm.init = init;
        vm.lang = localStorage.getItem('currLang');
        console.log(vm.lang)


        function init() {
            $translatePartialLoader.addPart('kirim-verifikasi');
            jLoad(1);
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            VerifiedSendService.selectVerifikasi(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.verified = reply.data;
                    vm.vendorID = vm.verified.VendorID;
                    role(1);
                } else {
                    $.growl.error({ message: "FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
        vm.data = [];
        vm.role = role;
        function role(current) {
            UIControlService.loadLoading();

            VerifiedSendService.role(function (reply) {
                if (reply.status === 200) {
                    var data = reply.data[1].Children;
                    vm.data = data;
                    console.info(vm.data);
                    vm.notCompletedData = [];
                    for (var i = 0; i < vm.data.length; i++) {
                        if (vm.data[i].IsChecked == null && vm.data[i].StateName != "upload-dokumen-lainlain" && vm.data[i].StateName != "kirim-verifikasi") {
                            vm.notCompletedData.push(vm.data[i]);
                        }
                    }
                    console.info(vm.notCompletedData);
                    check();
                } else {
                    $.growl.error({ message: "FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadContact = loadContact;
        function loadContact() {
            VerifiedSendService.selectcontact({ VendorID: vm.vendorID }, function (reply) {
                //UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.contact = reply.data;
                    vm.listEmail = [];
                    for (var i = 0; i < vm.contact.length; i++) {
                        if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            vm.listEmail.push(vm.contact[i].Contact.Email);
                        }
                    }
                    //console.info("list email" + JSON.stringify(vm.listEmail));
                    sendMail(vm.listEmail);
                    //console.info(JSON.stringify(reply.data));
                } else {
                    $.growl.error({ message: "FAIL_GET_DATA_COMP" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.sendMail = sendMail;
        function sendMail(listEmail) {
            var email = {
                subject: 'Verifikasi Akun',
                mailContent: 'Permintaan verifikasi akun Anda telah terkirim. Mohon tunggu sampai akun Anda diverifikasi oleh pihak administrator.<br>Terima kasih.',
                isHtml: true,
                addresses: listEmail,
                VendorID: vm.vendorID,
                lang: vm.lang
            };

            // UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            VerifiedSendService.kirimVerifikasiSendEmail(email,
                function (response) {
                    // UIControlService.unloadLoading();
                    if (response.status == 200) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("notice", "SENT_EMAIL")
                        $state.go('dashboard-vendor');
                    }
                },
                function (response) {
                    UIControlService.unloadLoading();
                });
        }

        vm.isChecked;
        vm.check = check;
        vm.flag = 1;
        function check() {
            if (vm.verified.VendorTypeID !== null) {
                console.info(vm.verified);
                if (vm.verified.VendorType.Name === "VENDOR_TYPE_GOODS") {
                    for (var i = 0; i < vm.data.length - 1; i++) {
                        if (vm.data[i].Label !== "MENUS.COMPANYDATA.SENDVERIFICATION" && vm.data[i].Label !== "MENUS.COMPANYDATA.OTHERDOCUMENT"
                            && vm.data[i].Label !== "MENUS.COMPANYDATA.EXPERTS" && vm.data[i].Label !== "MENUS.COMPANYDATA.EXPERIENCE") {
                            if (vm.data[i].IsChecked === null) {
                                vm.flag = 0;
                                break;
                            }
                        }
                    }
                }
                else {
                    for (var i = 0; i < vm.data.length - 1; i++) {
                        if (vm.data[i].Label !== "MENUS.COMPANYDATA.SENDVERIFICATION" && vm.data[i].Label !== "MENUS.COMPANYDATA.OTHERDOCUMENT" && vm.data[i].Label !== "MENUS.COMPANYDATA.EXPERIENCE") {
                            if (vm.data[i].IsChecked === null) {
                                console.info(vm.data[i]);
                                vm.flag = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else vm.flag = 0;
            UIControlService.unloadLoading();

        }


        vm.add = add;
        function add() {
            if (vm.notCompletedData.length != 0) {
                UIControlService.msg_growl('warning', 'MESSAGE.DATA_BELUM_LENGKAP');
                return;
            }
            UIControlService.loadLoading("LOADING");
            //console.info("ada:"+JSON.stringify(data))
            VerifiedSendService.update(function (reply) {
                if (reply.status === 200) {
                    if (reply.data == true) {
                        UIControlService.msg_growl("success", "SUC_SEND");
                        loadContact();
                        jLoad(1);
                        SocketService.emit("daftarRekanan");
                    }
                    else {
                        UIControlService.msg_growl("error", 'INCOMPLETE');
                        return;
                    }
                } else {
                    UIControlService.msg_growl("error", "ERR_VER");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "API");
                UIControlService.unloadLoading();
            });
        }
    }
})();
