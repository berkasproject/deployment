﻿(function () {
	'use strict';

	angular.module("app").controller("UploadFileVHSCtrl", ctrl);

	ctrl.$inject = ['$filter', '$timeout', '$stateParams', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'PPVHSService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'GlobalConstantService', 'ExcelReaderService', '$state'];
	function ctrl($filter, $timeout, $stateParams, $http, $translate, $translatePartialLoader, $location, SocketService, PPVHSService, UploadFileConfigService,
        UIControlService, UploaderService, GlobalConstantService, ExcelReaderService, $state) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.pathFile;
		vm.DocTypeID = Number($stateParams.DocTypeID);
		vm.VendorID = Number($stateParams.VendorID);

		vm.deletedIds = [];
		vm.newExcel = [];
		vm.StepID = Number($stateParams.StepID);
		vm.TenderRefID = Number($stateParams.TenderRefID);
		vm.tglSekarang = UIControlService.getDateNow("");
		vm.message = "MESSAGE.LOADING";//Silahkan Tunggu
		vm.totalItems = 0;
		vm.currentPage = 1;
		vm.pageSize = 10;
		vm.init = init;
		vm.maxSize = 10;
		vm.vhs = [];
		function init() {
			$translatePartialLoader.addPart("pemasukkan-penawaran-vhs");
			loadAwal();
			GetRFQ();
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.VHS.OFFERENTRY", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = [];
					vm.list = response.data;
					for (var i = 0; i < vm.list.length; i++) {
						if (vm.list[i].Name == "xls" || vm.list[i].Name == "xlsx") vm.idUploadConfigs.push(vm.list[i]);

					}
					vm.idFileTypes = UIControlService.generateFilterStrings(vm.idUploadConfigs);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}

		vm.clear = clear;
		function clear() {
			vm.newExcel = [];
		}

		vm.loadAwal = loadAwal;
		function loadAwal(current) {
			vm.currentPage = current;
			var offset = (vm.currentPage * vm.maxSize) - vm.maxSize;
			PPVHSService.selectDetail({
				TenderDocTypeID: vm.DocTypeID,
				vhs: { TenderStepID: vm.StepID },
				Offset: offset,
				Limit: vm.pageSize
			}, function (reply) {
				UIControlService.unloadLoading();
				vm.newExcel = [];
				if (reply.status === 200) {
					vm.det = reply.data.List;
					vm.totalItems = Number(reply.data.Count);
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.GetRFQ = GetRFQ;
		function GetRFQ() {
			PPVHSService.selectRFQId({ Status: vm.TenderRefID }, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.RFQId = reply.data;
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			//console.info("curr "+current)
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			var tender = {
				Status: vm.TenderRefID
			}
			PPVHSService.select(tender, function (reply) {
				//console.info("data:"+JSON.stringify(reply));
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.vhs = reply.data;
				} else {
					UIControlService.unloadLoading();
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}


		vm.selectUpload = selectUpload;
		//vm.fileUpload;
		function selectUpload() {
		}
		/*start upload */

		vm.ReadFile = ReadFile;
		function ReadFile() {
			ExcelReaderService.readExcel(vm.fileUpload, function (reply) {
				console.info(reply);
				if (reply.status === 200) {
					var excelContents = reply.data;
					var sheet1 = excelContents[Object.keys(excelContents)[0]];
					vm.list = [];
					vm.list = sheet1;
					vm.dataplusplus = 1;
					vm.flag = 0;
					vm.listExcel = [];
					cekLimit();

				}
			});
		}

		vm.cekLimit = cekLimit;
		function cekLimit() {
			console.info(vm.RFQId);
			for (var i = 1; i < vm.list.length; i++) {
				vm.iplus = i;
				if (vm.RFQId.TenderType === 4171) {
					if (vm.list[i].Column14 !== null) {
						if (vm.list[i].Column14.toLowerCase().includes("yes")) {
							if (vm.list[i].Column9 == null) {
								var getError = $filter('translate')('UNITPRICE_ROW') + (" ") + (i + 1) + $filter('translate')('FORM.UNITPRICENULL');
								UIControlService.msg_growl('error', getError);
							} else {
								if (!/^[0-9]+([.][0-9]{1,9})?$/.test((vm.list[i].Column9))) {
									UIControlService.unloadLoading();
									var getError = $filter('translate')('UNITPRICE_ROW') + (" ") + (i + 1) + $filter('translate')('FORM.CONT_UNITPRICE');
									UIControlService.msg_growl('error', getError);
									return;
								} else if (!/^[0-9]+$/.test((vm.list[i].Column10))) {
									UIControlService.unloadLoading();
									var getError = $filter('translate')('FORM.LEADTIME') + (i + 1) + $filter('translate')('FORM.CONT_LEADTIME');
									UIControlService.msg_growl('error', getError);
									return;
								} else {
									var objExcel = {
										Code: vm.list[i].Column1,
										MaterialCode: vm.list[i].Column2,
										ItemDescrip: vm.list[i].Column3,
										Manufacture: vm.list[i].Column4,
										PartNo: vm.list[i].Column5,
										Estimate: vm.list[i].Column6,
										Unit: vm.list[i].Column7,
										Currency: vm.list[i].Column8,
										PriceIDR: vm.list[i].Column9,
										LeadTime: vm.list[i].Column10,
										CountryOfOrigin: vm.list[i].Column11,
										Remark: vm.list[i].Column12,
										Alternatif: vm.list[i].Column13
									};
									vm.newExcel.push(objExcel);
								}
							}
						} else if (vm.list[i].Column14.toLowerCase().includes("no")) {
							var objExcel = {
								MaterialCode: vm.list[i].Column2
							};

							vm.deletedIds.push(objExcel);
						}
					}
					if (i == (vm.list.length - 1)) {
						var data = {
							DocumentUrl: vm.pathFile,
							Filename: vm.name,
							FileSize: vm.size,
							IsPublish: false,
							TenderDocTypeID: vm.DocTypeID,
							TenderStepID: vm.StepID,
							detail: vm.newExcel
						}
						PPVHSService.Insert(data, function (reply) {
							if (reply.status === 200) {

								var dataDelete = {
									TenderStepID: vm.StepID,
									detail: vm.deletedIds
								}

								PPVHSService.deleteItem(dataDelete, function (reply2) {
									if (reply2.status === 200) {
										UIControlService.msg_growl("success", $filter('translate')('FORM.MSG_SUC_SAVE') + (" ") + (vm.newExcel.length) + " data");
										UIControlService.unloadLoading();
										vm.deletedIds = []
										vm.newExcel = [];
										vm.loadAwal(1);
									} else {
										UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
										return;
									}
								}, function (err) {
									UIControlService.msg_growl("error", err);
									UIControlService.unloadLoading();
								})
							} else {
								UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
								return;
							}
						}, function (err) {
							UIControlService.msg_growl("error", err);
							UIControlService.unloadLoading();
						});
					}
				} else {
					if (vm.list[i].Column13 !== null) {
						if (vm.list[i].Column13.includes("yes") || vm.list[i].Column13.includes("Yes") || vm.list[i].Column13.includes("YES")) {
							if (vm.list[i].Column9 == null) {
								var getError = $filter('translate')('UNITPRICE_ROW') + (" ") + (i + 1) + (" ") + $filter('translate')('FORM.UNITPRICENULL');
								UIControlService.msg_growl('error', getError);
								return;
							} else {
								if (!/^[0-9]+([.][0-9]{1,9})?$/.test((vm.list[i].Column9))) {
									UIControlService.unloadLoading();
									var getError = $filter('translate')('UNITPRICE_ROW') + (" ") + (i + 1) + (" ") + $filter('translate')('FORM.CONT_UNITPRICE');
									UIControlService.msg_growl('error', getError);
									return;
								} else if (!/^[0-9]+$/.test((vm.list[i].Column10))) {
									UIControlService.unloadLoading();
									var getError = $filter('translate')('FORM.LEADTIME') + (i + 1) + (" ") + $filter('translate')('FORM.CONT_LEADTIME');
									UIControlService.msg_growl('error', getError);
									return;
								} else {
									var objExcel = {
										Code: vm.list[i].Column1,
										MaterialCode: vm.list[i].Column2,
										ItemDescrip: vm.list[i].Column3,
										Manufacture: vm.list[i].Column4,
										PartNo: vm.list[i].Column5,
										Estimate: vm.list[i].Column6,
										Unit: vm.list[i].Column7,
										Currency: vm.list[i].Column8,
										PriceIDR: vm.list[i].Column9,
										LeadTime: vm.list[i].Column10,
										CountryOfOrigin: vm.list[i].Column11,
										Remark: vm.list[i].Column12
									};
									vm.newExcel.push(objExcel);


								}
							}

						} else if (vm.list[i].Column13.toLowerCase().includes("no")) {
							var objExcel = {
								MaterialCode: vm.list[i].Column2
							};

							vm.deletedIds.push(objExcel);
						}
					}
					if (i == (vm.list.length - 1)) {
						var data = {
							DocumentUrl: vm.pathFile,
							Filename: vm.name,
							FileSize: vm.size,
							IsPublish: false,
							TenderDocTypeID: vm.DocTypeID,
							TenderStepID: vm.StepID,
							detail: vm.newExcel
						}
						PPVHSService.Insert(data, function (reply) {
							if (reply.status === 200) {

								var dataDelete = {
									TenderStepID: vm.StepID,
									detail: vm.deletedIds
								}

								PPVHSService.deleteItem(dataDelete, function (reply2) {
									if (reply2.status === 200) {
										UIControlService.msg_growl("success", $filter('translate')('FORM.MSG_SUC_SAVE') + (" ") + (vm.newExcel.length) + " data");
										UIControlService.unloadLoading();
										vm.deletedIds = []
										vm.newExcel = [];

										PPVHSService.recountTotalOffer({
											TenderStepID: vm.StepID
										}, function (reply3) {
										}, function () {
										})

										vm.loadAwal(1);
									} else {
										UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
										return;
									}
								}, function (err) {
									UIControlService.msg_growl("error", err);
									UIControlService.unloadLoading();
								})

							} else {
								UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
								return;
							}
						}, function (err) {
							UIControlService.msg_growl("error", err);
							UIControlService.unloadLoading();
						});
					}
				}

				if (vm.list[i].Column1 == null) {
					i = vm.list.length - 1;
					var data = {
						DocumentUrl: vm.pathFile,
						Filename: vm.name,
						FileSize: vm.size,
						IsPublish: false,
						TenderDocTypeID: vm.DocTypeID,
						TenderStepID: vm.StepID,
						detail: vm.newExcel
					}
					PPVHSService.Insert(data, function (reply) {
						if (reply.status === 200) {

							UIControlService.msg_growl("success", $filter('translate')('FORM.MSG_SUC_SAVE') + (" ") + (vm.newExcel.length) + " data");
							UIControlService.unloadLoading();
							vm.newExcel = [];
							vm.loadAwal(1);
						} else {
							UIControlService.msg_growl("error", 'FORM.MSG_ERR_SAVE');
							return;
						}
					}, function (err) {
						UIControlService.msg_growl("error", err);
						UIControlService.unloadLoading();
					});
				}
			}
		}


		/*count of property object*/
		function numAttrs(obj) {
			var count = 0;
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					++count;
				}
			}
			return count;
		}

		//vm.uploadSave = uploadSave;
		//function uploadSave(dataList) {
		//    vm.flag = 0;
		//    vm.iplus = 1;
		//    vm.listExcel = [];
		//    vm.flagDataExt = false;
		//    var data = {
		//        DocumentUrl: vm.pathFile,
		//        Filename: vm.name,
		//        FileSize: vm.size,
		//        IsPublish: false,
		//        TenderDocTypeID: vm.DocTypeID,
		//        TenderStepID: vm.StepID,
		//        detail: dataList

		//    }
		//    PPVHSService.Insert(data, function (reply) {
		//        if (reply.status === 200) {
		//            vm.listExcel = [];
		//            UIControlService.unloadLoading();
		//            UIControlService.loadLoading("Berhasil simpan " + vm.dataiplus * 100 + " data");
		//        }
		//        else {
		//            UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
		//            return;
		//        }
		//    }, function (err) {
		//        UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		//        UIControlService.unloadLoadingModal();
		//    });

		//}
		//for (var exc = 0; exc < vm.newExcel.length; exc++) {
		//    vm.listExcel.push(vm.newExcel[exc]);
		//    if (vm.newExcel[exc].CountryOfOrigin === null && vm.RFQId.RFQType == 2) {
		//        vm.flag = 1;
		//        UIControlService.unloadLoading();
		//        UIControlService.msg_growl("error", "Maaf Country of origin tidak boleh kosong");
		//        return;
		//    }
		//    if (vm.newExcel[exc].Currency === null) {
		//        vm.flag = 1;
		//        UIControlService.unloadLoading();
		//        UIControlService.msg_growl("error", "Maaf Currency tidak boleh kosong");
		//        return;
		//    }
		//    else  {
		//        PPVHSService.cekCurr({
		//            Keyword: vm.newExcel[exc].Currency
		//        }, function (reply) {
		//            UIControlService.unloadLoading();
		//            if (reply.status === 200) {
		//                UIControlService.loadLoading("Save "+ vm.dataiplus + " data");
		//                vm.flagCurr = reply.data;
		//                $timeout(function () {
		//                }, 1000);
		//                if (vm.flagCurr == false) {
		//                    vm.flag = 1;
		//                    exc = (vm.newExcel.length - 1);
		//                    UIControlService.msg_growl("error", "Maaf Currency tidak sesuai");
		//                    return;
		//                }
		//            } else {
		//                $.growl.error({ message: "Gagal mendapatkan data Tender" });
		//                UIControlService.unloadLoading();
		//            }
		//        }, function (err) {
		//            console.info("error:" + JSON.stringify(err));
		//            //$.growl.error({ message: "Gagal Akses API >" + err });
		//            UIControlService.unloadLoading();
		//        });
		//    }
		//if (exc === (vm.iplus * 1000)) {
		//    if(exc === vm.newExcel.length - 1) vm.flagDataExt = true;
		//    vm.iplus++;
		//    vm.dataNotif = vm.iplus * 1000;
		//    vm.listDetail = [];
		//    vm.VHSOfferEntry = {};
		//    vm.id = 0;


		//}
		//else if (exc === vm.newExcel.length - 1 && vm.listExcel.length !== 1000) {
		//    vm.flagDataExt = true;
		//    vm.dataNotif = vm.listExcel.length;
		//    vm.listDetail = [];
		//    vm.VHSOfferEntry = {};
		//    vm.id = 0;
		//    var data = {
		//        DocumentUrl: vm.pathFile,
		//        Filename: vm.name,
		//        FileSize: vm.size,
		//        IsPublish: false,
		//        TenderDocTypeID: vm.DocTypeID,
		//        TenderStepID: vm.StepID,
		//        detail: vm.listExcel

		//    }
		//    PPVHSService.Insert(data, function (reply) {
		//        if (reply.status === 200) {
		//            vm.listExcel = [];
		//            UIControlService.msg_growl("success", "Berhasil Simpan data " + (vm.dataNotif));
		//            if (vm.flagDataExt == true) {
		//                UIControlService.unloadLoading();
		//                vm.kembali();
		//            }
		//        }
		//        else {
		//            UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
		//            return;
		//        }
		//    }, function (err) {
		//        UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		//        UIControlService.unloadLoadingModal();
		//    });
		//    return;
		//}
		//if (exc === vm.newExcel.length - 1 && vm.flag === 0) {
		//    saveAll();
		//    return;
		//}



		vm.uploadFile = uploadFile;
		function uploadFile() {
			if (vm.fileUpload === undefined) {
				UIControlService.msg_growl("error", "ERR.NO_FILE");
				return
			} else {
				UIControlService.loadLoading("MESSAGE.LOADING_SAVE");
				if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
				}
			}
		}


		vm.saveAll = saveAll;
		function saveAll() {
			PPVHSService.selectStep({
				ID: vm.StepID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.step = reply.data;
					PPVHSService.selectRFQ({
						Status: reply.data.tender.TenderRefID
					}, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							vm.getRFQ = reply.data;
							if (vm.getRFQ.Keyword === 'TENDER_OPTIONS_PACKAGE') {
								if (((vm.RFQId.Limit / 100) * vm.getRFQ.Status).toFixed() !== vm.newExcel.length) {
									UIControlService.msg_growl("error", "ERR.OFFER_NOTCOMPLETE");
									return;
								} else {
									if (vm.fileUpload === undefined && vm.newExcel.length !== 0) {
										UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
										init();
									} else if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
										upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
									}
								}
								if (vm.fileUpload === undefined && vm.newExcel.length !== 0) {
									UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
									init();
								} else if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
									upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
								}
							} else {
								if (vm.fileUpload === undefined && vm.newExcel.length !== 0) {
									UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
									init();
								} else if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
									upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
								}
							}
						} else {
							$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						console.info("error:" + JSON.stringify(err));
						//$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});

				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA_TENDER" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});

		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "ERR.NO_FILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.unit = "KB";
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UploaderService.uploadSingleFileVHSOfferEntry(vm.VendorID, file, size, filters,
                function (response) {
                	if (response.status == 200) {
                		var url = response.data.Url;
                		vm.pathFile = url;
                		vm.name = response.data.FileName;
                		var s = response.data.FileLength;
                		if (vm.flag == 0) {
                			vm.size = Math.floor(s);
                		} else if (vm.flag == 1) {
                			vm.size = Math.floor(s / (1024));
                		}
                		ReadFile();
                	} else {
                		UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	UIControlService.msg_growl("error", "MESSAGE.API")
                	UIControlService.unloadLoading();
                });

		}

		vm.kembali = kembali;
		function kembali() {
			PPVHSService.selectStep({
				ID: vm.StepID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.step = reply.data;
					$state.transitionTo("pemasukan-penawaran-vhs-vendor", { StepID: vm.StepID, TenderRefID: vm.step.tender.TenderRefID, ProcPackType: vm.step.tender.ProcPackageType });
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA_TENDER" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});

		}

	}
})();