(function () {
  'use strict';

  angular.module("app").controller("VendorDDQuestCtrl", ctrl);

  ctrl.$inject = ['$window','$filter','$stateParams','$timeout', '$uibModal', '$http', '$translate', '$translatePartialLoader', '$location', '$state', 'VerifiedSendService', 'SrtPernyataanService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'VendorRegistrationService'];
  /* @ngInject */
  function ctrl($window, $filter, $stateParams, $timeout, $uibModal, $http, $translate, $translatePartialLoader, $location, $state, VerifiedSendService, SrtPernyataanService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService) {
      var endpoint = GlobalConstantService.getConstant("api_endpoint");
      var vm = this;
      vm.IsSent = false;
      vm.VendorID = Number($stateParams.VendorID);
      vm.initialize = initialize;
      function initialize() {
          vm.currentLang = $translate.use();
          $translatePartialLoader.addPart('surat-pernyataan');
          loadKuesionerDD();
      }

      vm.loadradio = loadradio;
      function loadradio(questionID,questiondetailID) {
          for (var i = 0; i <= vm.ddQuest.MstDDQuestionnaire.DDQuestion.length - 1; i++) {
              if (vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].ID == questionID) {
                  if (vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail.length > 0) {
                      for (var j = 0; j <= vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail.length - 1; j++) {
                          if (vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail[j].ID != questiondetailID) {
                              vm.ddQuest.MstDDQuestionnaire.DDQuestion[i].DDQuestionDetail[j].VendorComplianceQuestionnaire[0].Answer = null;
                          }
                      }
                  }
              }
          }
      }


      vm.loadKuesionerDD = loadKuesionerDD;
      function loadKuesionerDD() {
          UIControlService.loadLoading("MESSAGE.LOADING");
          SrtPernyataanService.loadDDQuest({
              VendorID: vm.VendorID
          }, function (reply) {
              UIControlService.unloadLoading();
              if (reply.status === 200) {
                  vm.ddQuest = reply.data;
                  console.info("loadDDquest:" + JSON.stringify(vm.ddQuest));
                  if (vm.ddQuest.SendQuestionnaireDate != null) {
                      vm.IsSent = true;
                  }
                  console.info("issent?" + vm.IsSent);
              } else {
                  $.growl.error({ message: "Gagal mendapatkan dokumen" });
                  UIControlService.unloadLoading();
              }
          }, function (err) {
              //$.growl.error({ message: "Gagal Akses API >" + err });
              UIControlService.unloadLoading();
          });
      }

      vm.kirim = kirim;
      function kirim() {
          //console.info("simpan?" + JSON.stringify(vm.ddQuest));
          bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_SEND_DDQUEST') + '</h4>', function (res) {
              if (res) {
                  SrtPernyataanService.sendAnswerQuest(vm.ddQuest, function (reply) {
                      UIControlService.unloadLoading();
                      if (reply.status === 200) {
                          UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SAVE', '');
                          UIControlService.unloadLoading();
                          //initialize();
                          $state.transitionTo('suratPernyataan');
                      } else {
                          UIControlService.msg_growl('error', 'MESSAGE.ERR_SAVE', '');
                          UIControlService.unloadLoading();
                      }
                  }, function (err) {
                      UIControlService.unloadLoading();
                  });

                  //SocketService.emit("daftarRekanan");
              }
          });
      }


      vm.simpan = simpan;
      function simpan() {
          bootbox.confirm('<h4 class="afta-font center-block">' + $filter('translate')('MESSAGE.SURE_SAVE_DDQUEST') + '</h4>', function (res) {
              if (res) {
                  SrtPernyataanService.answerQuest(vm.ddQuest, function (reply) {
                      UIControlService.unloadLoading();
                      if (reply.status === 200) {
                          UIControlService.msg_growl('notice', 'MESSAGE.SUCC_SAVE', '');
                          UIControlService.unloadLoading();
                          //initialize();
                          $state.transitionTo('suratPernyataan');
                      } else {
                          UIControlService.msg_growl('error', 'MESSAGE.ERR_SAVE', '');
                          UIControlService.unloadLoading();
                      }
                  }, function (err) {
                      UIControlService.unloadLoading();
                  });

                  //SocketService.emit("daftarRekanan");
              }
          });
      }

      vm.back = back;
      function back() {
          localStorage.setItem('currLang', vm.currentLang);
          $state.go('suratPernyataan');
      }


  }
})();

