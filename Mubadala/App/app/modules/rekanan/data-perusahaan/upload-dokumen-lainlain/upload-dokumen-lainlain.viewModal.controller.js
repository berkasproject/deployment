﻿(function () {
	'use strict';

	angular.module("app").controller("viewUploadDocCtrl", ctrl);

	ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'UploadDokumenLainlainService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService','VendorRegistrationService', 'FileSaver'];
	/* @ngInject */
	function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, UploadDokumenLainlainService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService, FileSaver) {

		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.item = item.item;
		vm.VendorCategoryID = vm.item.VendorCategoryID;
		vm.ValidDateConverted = convertDate(item.item.ValidDate);

		function convertDate(date) {
			return UIControlService.convertDate(date);
		}

		vm.downloadFileCivd = downloadFileCivd;
		function downloadFileCivd(url, name) {
		    UIControlService.loadLoadingModal();

		    VendorRegistrationService.getCIVDData({
		        Keyword: url
		    }, function (response) {
		        var reply = JSON.parse(response.data);
		        if (reply.status === 200) {
		            var byteCharacters = atob(reply.result);

		            var byteNumbers = new Array(byteCharacters.length);
		            for (let i = 0; i < byteCharacters.length; i++) {
		                byteNumbers[i] = byteCharacters.charCodeAt(i);
		            }

		            var byteArray = new Uint8Array(byteNumbers);

		            var blob = new Blob([byteArray], {
		                type: "application/pdf"
		            });

		            FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
		            UIControlService.unloadLoadingModal();

		        } else {
		            UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		        }
		    }, function (error) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		    });
		}

		vm.close = close;
		function close() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();