﻿(function () {
	'use strict';

	angular.module("app").controller("UploadDokModalCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$stateParams', '$location', 'VerifiedSendService', 'UploadDokumenLainlainService',
        '$state', 'UIControlService', '$uibModal', '$uibModalInstance', 'GlobalConstantService', 'item', 'UploadFileConfigService', 'UploaderService'];
	function ctrl($http, $translate, $translatePartialLoader, $stateParams, $location, VerifiedSendService, UploadDokumenLainlainService,
        $state, UIControlService, $uibModal, $uibModalInstance, GlobalConstantService, item, UploadFileConfigService, UploaderService) {
		var vm = this;
		//console.info("console modal upload");
		vm.detail = item.item;
		vm.VendorID;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isAdd = item.act;
		vm.action = "";
		vm.pathFile;
		vm.Description;
		vm.fileUpload;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;
		vm.isCalendarOpened = [false, false, false];
		vm.Nama;
		vm.No;
		vm.ID;
		vm.idFileTypes;
		vm.idFileSize;
		vm.idUploadConfigs;
		//vm.StartDate= {};
		vm.DocUrl='';
		vm.tglSekarang = UIControlService.getDateNow("");
		vm.StartDate = vm.StartDate == undefined ? '' : item.item.ValidDate;
		//vm.docNameForSO = "";

		vm.init = init;
		function init() {
		    if (localStorage.getItem('currLang').toLowerCase() == 'id') {
		        vm.btnBack = vm.isAdd != 1 ? 'Batal' : 'Kembali';
		    }
		    else if (localStorage.getItem("currLang").toLowerCase() == 'en') {
		        vm.btnBack = vm.isAdd != 1 ? 'Cancel' : 'Back';
		    }
			$translatePartialLoader.addPart('other-docs');
			loadVerifiedVendor();
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.UPLOADDL", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
				} else {
					UIControlService.msg_growl("error", ".NOTIF.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "NOTIF.API");
				UIControlService.unloadLoading();
				return;
			});
			//console.info("item" + JSON.stringify(item));
			if (vm.isAdd === 1) {
			    vm.action = "Tambah";
			    //vm.StartDate = null;
			    //console.info("item" + JSON.stringify(item));
			    vm.isForStructure = item.isForStructure;
			    vm.isStructureUploaded = item.isStructureUploaded;
			    if (vm.isForStructure == 1) {
			        if (item.lang == "ID" || item.lang == "id") {
			            vm.Nama = "Struktur Organisasi";
			        }
			        else {
			            vm.Nama = "Structure of Organization";
			        }
			    }

			} else {
			    vm.action = "Ubah";
			    vm.isForStructure = item.item.isForStructure;
				vm.ID = item.item.ID;
				vm.Nama = item.item.DocumentName;
				vm.No = item.item.DocumentNo;
				vm.DocUrl = item.item.DocumentUrl;
				if(item.item.ValidDate == 'Invalid Date'){
					vm.StartDate = null;
				}
				else{
					vm.StartDate = item.item.ValidDate;
				}
				console.info("vm.docUrl" + vm.DocUrl +" "+ vm.StartDate);
				//console.info(vm.StartDate);
			}


		}

		vm.changeDate = changeDate;
		function changeDate(d) {
		    vm.changed = 1;
		}

		vm.cekDate = cekDate;
		function cekDate() {
		    if (vm.StartDate == undefined) {
		        var errorMessage = localStorage.getItem("currLang").toLowerCase() == 'id' ? "Tanggal tidak valid" : "Invalid date";
		        UIControlService.msg_growl("error", errorMessage);
		        vm.StartDate = "";
		        vm.invalidDate = 1;
		        vm.changed = 0;
		        //return;
		    } else {
		        vm.invalidDate = 0;
		        //return;
		    }
		}

		vm.simpan = simpan;
		function simpan() {
		    //vm.cekDate();
		    //vm.changed = vm.StartDate == null ? 0 : 1;
		    if (vm.invalidDate == 1 && vm.changed == 1) {
		        vm.invalidDate = 0;
		        vm.changed = 0;
		        return;
		    }
			if (vm.Nama === "" || vm.Nama == undefined) {
				UIControlService.msg_growl("error", "NOTIF.ERR_NAMEDOC");
				return;
			} else if (vm.No === "" || vm.No == undefined) {
				UIControlService.msg_growl("error", "NOTIF.ERR_NO");
				return;
			}
			//else if (vm.StartDate === undefined) {
			    
			//}
			//else if (vm.StartDate == undefined && vm.changed == 1) {
			//    var errorMessage = localStorage.getItem("currLang").toLowerCase() == 'id' ? "Tanggal tidak valid" : "Invalid date";
			//    UIControlService.msg_growl("error", errorMessage);
			//    vm.StartDate = "";
			//    vm.changed = 0;
			//    return;
			//}
			else {
			    if (vm.fileUpload != undefined) {
			        uploadFile();
			    }
			    else {
			        if (vm.isAdd == 1) {
			            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			            return;
			        } else {
			            savetoDB();
			        }
			    }
			}
		}

		


		//ambil VendorID
		vm.loadVerifiedVendor = loadVerifiedVendor;
		function loadVerifiedVendor() {
			VerifiedSendService.selectVerifikasi(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data;
					vm.cekTemporary = vm.verified.IsTemporary;
					vm.VendorID = vm.verified.VendorID;
					//console.info(JSON.stringify(vm.verified.VendorID));
				} else {
					$.growl.error({ message: "NOTIF.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}
		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		}

		vm.selectUpload = selectUpload;
		//vm.fileUpload;
		function selectUpload() {
			//console.info(vm.fileUpload);
		}
		/*start upload */
		vm.uploadFile = uploadFile;
		function uploadFile() {
		    if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		        upload(vm.VendorID, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
		    }
		}

		//function validateFileType(file, allowedFileTypes) {
		//    var valid_size = allowedFileTypes[0].Size;
		//    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
		//    if (size_file > valid_size) {
		//        UIControlService.unloadLoadingModal();
		//        UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE");
		//        vm.fileUpload = "";
		//        return false;
		//    }
		    //if (!file || file.length == 0) {
		    //    UIControlService.msg_growl("error", "ERROR.NO_FILE");
		    //    return false;
		    //}
		//	return true;
		//}

		vm.upload = upload;
		function upload(id, file, config, filters, callback) {
			convertToDate();

			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}
			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}

			UIControlService.loadLoading("NOTIF.LOADING_UPLOAD");
			UploaderService.uploadSingleFileUploadDocument(id, file, size, filters,
                function (response) {
                	UIControlService.unloadLoading();
                	//console.info("response:" + JSON.stringify(response));
                	if (response.status == 200) {
                		//console.info(response);
                		var url = response.data.Url;
                		vm.pathFile = url;
                		vm.name = response.data.FileName;
                		var s = response.data.FileLength;
                		vm.DocUrl = vm.pathFile;
                		if (vm.flag == 0) {
                			vm.size = Math.floor(s);

                		}
                		if (vm.flag == 1) {
                			vm.size = Math.floor(s / (1024));
                		}
                		savetoDB();

                	} else {
                		UIControlService.msg_growl("error", "ERROR.FAIL_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	//console.info(response);
                	UIControlService.msg_growl("error", "NOTIF.NOT_COMPLETE")
                	UIControlService.unloadLoading();
                });



		}

		function savetoDB() {
		    if (vm.isAdd === 1) {
		        if (vm.StartDate == "NaN-NaN-NaN") {
		            vm.StartDate = null;
		        }
		        //console.info(vm.StartDate);
		        UploadDokumenLainlainService.insert({
		            DocumentName: vm.Nama,
		            ValidDate: vm.StartDate,
		            DocumentNo: vm.No,
		            DocumentUrl: vm.DocUrl,
					VendorID: vm.VendorID,
					VendorCategoryID : 33
		        }, function (reply) {
		            //console.info("reply" + JSON.stringify(reply))
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                //console.info(vm.StartDate);
		                UIControlService.msg_growl("success", "NOTIF.SUCCESS_UPLOAD");
		                $uibModalInstance.close();

		            } else {
		                UIControlService.msg_growl("error", "ERROR.FAIL_ADD_DOC");
		                return;
		            }
		        }, function (err) {
		            console.info(err);
		            UIControlService.msg_growl("error", "NOTIF.NOT_COMPLETE");
		            UIControlService.unloadLoadingModal();
		        }
                    );
		    } else {
				//console.info("vm.docUrl " + vm.StartDate);
				if (vm.StartDate == "NaN-NaN-NaN" || vm.StartDate == null) {
		            vm.StartDate = null;
				}
				else{
					vm.StartDate = UIControlService.getStrDate(vm.StartDate);
				}
		        
		        //console.info("vm.docUrl" + vm.DocUrl);
                
		        UploadDokumenLainlainService.Update({
		            DocumentName: vm.Nama, DocumentUrl: vm.DocUrl, DocumentNo: vm.No, VendorID: vm.VendorID, ValidDate: vm.StartDate, ID: vm.ID
		        }, function (reply) {
		            UIControlService.unloadLoadingModal();
		            if (reply.status === 200) {
		                UIControlService.msg_growl("success", "NOTIF.SUCCESS_UPDATE");
		                $uibModalInstance.close();
		            } else {
		                UIControlService.msg_growl("error", "ERROR.FAIL_UPDATE");
		                return;
		            }
		        }, function (err) {
		            //console.info(vm.StartDate);
		            UIControlService.msg_growl("error", "NOTIF.NOT_COMPLETE");
		            UIControlService.unloadLoadingModal();
		        });
		    }
		}

		function convertAllDateToString() { // TIMEZONE (-)
			vm.StartDate= UIControlService.getStrDate(vm.tgl);
		};

		function convertToDate() {
			vm.StartDate = UIControlService.getStrDate(vm.StartDate);
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();
