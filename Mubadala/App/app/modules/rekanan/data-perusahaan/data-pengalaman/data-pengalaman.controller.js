(function () {
    'use strict';

    angular.module("app").controller("DataPengalamanController", ctrl);

    ctrl.$inject = ['$timeout', '$uibModal', '$translatePartialLoader', 'UIControlService', 'AuthService', 'VendorExperienceService', '$rootScope', '$q', 'VendorRegistrationService','VerifiedSendService', '$filter'];
    /* @ngInject */
    function ctrl($timeout, $uibModal, $translatePartialLoader, UIControlService, AuthService, VendorExperienceService, $rootScope, $q, VendorRegistrationService, VerifiedSendService, $filter) {
        var vm = this;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.keyword = '';
        vm.vendorID = [];
        vm.comodity = [];
        vm.isApprovedCR = false;
        vm.menuIndex = 6;
        vm.username = localStorage.getItem('username');
        vm.revisiVendor = false;//false
        vm.langID = true;

        vm.initialize = initialize;
        function initialize() {
            $translatePartialLoader.addPart('data-pengalaman');
            if (localStorage.getItem('currLang').toLowerCase() != 'id') {
                vm.langID = false;
            }
            getVendorNation();
            loadVendor();
            cekPrakualifikasiVendor();
            cekCR();
            getVendorCategoryID();
            loadVendorVerificationReview().then(function () {
                loadCheckCR();
            });
        };
        
        function loadVendorVerificationReview() {
            var defer = $q.defer();
            VerifiedSendService.getVendorVerificationReview({
                MenuID: 1042
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.vendorReviewData = data;
                    //console.log(data)
                    if (data != null) {
                        if (data.ReviewStatus == 4526) {
                            vm.revisiVendor = true;
                        } else {
                            vm.revisiVendor = false;
                        }
                        if (vm.langID) {
                            vm.revisiName = data.Locale_Id;
                        } else {
                            vm.revisiName = data.Locale_En;
                        }
                    }
                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                defer.reject;
                return;
            })
            return defer.promise;
        }

        function getVendorNation() {
            AuthService.getRoleUserLogin({ Keyword: vm.username }, function (reply) {
                if (reply.status === 200 && reply.data.List.length > 0) {
                    var data = reply.data.List;
                    if (data[0].RoleName == "APPLICATION.ROLE_VENDOR_INTERNATIONAL") {
                        vm.menuIndex = 4;
                    }
                    //console.log(reply.data.List)
                } else {

                }
            }, function (err1) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        function cekCR() {
            VendorExperienceService.cekCR(function (reply) {
                if (reply.status === 200) {
                    vm.isCR = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        };

        vm.loadVendor = loadVendor;
        function loadVendor() {
            VendorExperienceService.selectVendor(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.Vendor = reply.data;
                    loadVendorCommodity(vm.Vendor.VendorID);
                } else {
                    $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        function getVendorCategoryID() {
            var defer = $q.defer();
            AuthService.getVendorCategoryID({}, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.vendorCategoryID = data.VendorCategoryID;
                    vm.supplierID = data.SupplierID;
                    vm.civdID = data.CIVDID;
                    //console.log(vm.vendorCategoryID)
                    defer.resolve(true);
                } else {
                    defer.reject(false);
                }
            }, function (err) {
                defer.reject(false);
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            })
            return defer.promise;
        }

        vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
        function cekPrakualifikasiVendor() {
            VendorExperienceService.cekPrakualifikasiVendor(function (reply) {
                UIControlService.loadLoadingModal();
                if (reply.status === 200) {
                    vm.pqWarning = reply.data;
                    //console.info("isregistered" + vm.pqWarning);
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
            });
        }

        vm.loadVendorCommodity = loadVendorCommodity;
        function loadVendorCommodity(data) {
            VendorExperienceService.SelectVendorCommodity({
                VendorID: data
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //console.info(reply.status);
                    vm.comodity = reply.data;
                    loadawal(vm.comodity.BusinessFieldID);
                    //console.info("comodity:" + JSON.stringify(vm.comodity));
                } else {
                    $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        function loadCheckCR() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            //console.info("CR:");
            VendorExperienceService.getCRbyVendor({ CRName: 'OC_VENDOREXPERIENCE' }, function (reply) {
                UIControlService.unloadLoading();
                //console.info("CR:" + JSON.stringify(reply.status));
                if (reply.status === 200) {
                    //console.info("CR:" + JSON.stringify(reply.data));
                    vm.CR = reply.data;
                    if (reply.data[0] === true) {
                        vm.isApprovedCR = true;
                    } else {
                        vm.isApprovedCR = false;
                    }

                    if (vm.vendorReviewData != null && reply.data[1] != "Approved_CR") {
                        if (vm.revisiVendor) {
                            vm.IsApprovedCR = true;
                        } else {
                            vm.IsApprovedCR = false;
                        }
                    }
                    
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.addExp = addExp;
        function addExp(data, isAdd, isCR) {
            //console.info(isCR);
            var sendData = {
                item: data,
                isAdd: isAdd,
                isCR: isCR
            }
            //console.info(JSON.stringify(sendData));
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/data-pengalaman/form-pengalaman.html',
                backdrop: 'static',
                controller: 'formExpCtrl',
                controllerAs: 'formExpCtrl',
                resolve: {
                    item: function () {
                        return sendData;
                    }
                }
            });

            modalInstance.result.then(function () {
                // window.location.reload();
                initialize();
                $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
            });
        }

        vm.detailExp = detailExp;
        function detailExp(data) {
            var sendData = {
                item: data,
                isAdd: false
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/data-pengalaman/detail-pengalaman.html',
                backdrop: 'static',
                controller: 'formExpCtrl',
                controllerAs: 'formExpCtrl',
                resolve: {
                    item: function () {
                        return sendData;
                    }
                }
            });

            modalInstance.result.then(function () {
                loadawal();
            });
        }

        vm.loadAwal = loadawal;
        function loadawal(data) {
            //limit sementara dibuat 100
            vm.loadCurrentExp().then(function () {
                VendorExperienceService.select({
                    Offset: (vm.currentPage - 1) * vm.maxSize,
                    Limit: 100,
                    Keyword: vm.keyword,
                    FilterType: data,
                    column: 1
                }, function (reply) {
                    //console.info("lrjlbij" + vm.comodity.BusinessFieldID);
                    if (reply.status === 200) {
                        vm.listFinishExp = reply.data.List;
                        //console.info("listFinishExp:" + JSON.stringify(vm.listFinishExp));
                        for (var i = 0; i < vm.listFinishExp.length; i++) {
                            vm.listFinishExp[i].StartDate = UIControlService.getStrDate(vm.listFinishExp[i].StartDate);

                            if (vm.listFinishExp[i].CityLocation == null) {
                                vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address;
                            } else {
                                vm.listFinishExp[i].AddressInfo = vm.listFinishExp[i].Address + ", " + vm.listFinishExp[i].CityLocation.Name + ", " + vm.listFinishExp[i].CityLocation.State.Country.Name;
                            }
                        }

                        vm.totalItems = reply.data.Count;
                        //console.info("finish:" + JSON.stringify(vm.listFinishExp));
                        if (vm.listFinishExp.length == 0 && vm.listCurrentExp.length == 0) {
                            $rootScope.menus[vm.menuIndex].IsChecked = '';
                        }
                        UIControlService.unloadLoading();
                    } else {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
                });




            })

        }

        vm.loadCurrentExp = loadCurrentExp;
        function loadCurrentExp() {
            var def = $q.defer();
            UIControlService.loadLoading('MESSAGE.LOADING');
            VendorExperienceService.select({
                Offset: (vm.currentPage - 1) * vm.maxSize,
                Limit: 100,
                Keyword: vm.keyword,
                column: 2
            }, function (reply) {
                //console.info("current?:"+JSON.stringify(reply));
                if (reply.status === 200) {
                    vm.listCurrentExp = reply.data.List;
                    for (var i = 0; i < vm.listCurrentExp.length; i++) {
                        vm.listCurrentExp[i].StartDate = UIControlService.getStrDate(vm.listCurrentExp[i].StartDate);
                        if (vm.listCurrentExp[i].CityLocation == null) {
                            vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address;
                        } else {
                            vm.listCurrentExp[i].AddressInfo = vm.listCurrentExp[i].Address + ", " + vm.listCurrentExp[i].CityLocation.Name + ", " + vm.listCurrentExp[i].CityLocation.State.Country.Name;
                        }
                    }
                    //console.info("current exp:" + JSON.stringify(vm.listCurrentExp));
                    vm.totalItems = reply.data.Count;
                    def.resolve(true)
                } else {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
                    def.reject('NOTIFICATION.VENDOREXPERIENCE.ERROR')
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'NOTIFICATION.VENDOREXPERIENCE.ERROR', "NOTIFICATION.VENDOREXPERIENCE.TITLE");
                def.reject('NOTIFICATION.VENDOREXPERIENCE.ERROR')
            });

            return def.promise;

        }

        vm.editActive = editActive;
        function editActive(data, active) {
            bootbox.confirm({
                message: '<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL_EXPERIENCE') + '<h3>',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-close"></i> ' + $filter('translate')('BTN.CANCEL')
                    },
                    confirm: {
                        label: '<i class="fa fa-save"></i> OK'
                    }
                },
                callback: function (result) {
                    if (result) {
                        UIControlService.loadLoading("MESSAGE.LOADING");
                        VendorExperienceService.Delete({ ID: data.ID, IsActive: active }, function (reply) {
                            UIControlService.unloadLoading();
                            if (reply.status === 200) {
                                var msg = "";
                                if (active === false) msg = " NonAktifkan ";
                                if (active === true) msg = "Aktifkan ";
                                UIControlService.msg_growl("success", "Data Berhasil di " + msg);
                                // $timeout(function () {
                                // 	window.location.reload();
                                // }, 1000);
                                initialize();

                            } else {
                                UIControlService.msg_growl("error", "MESSAGE.FAIL_NON_AKTIF");
                                return;
                            }
                        }, function (err) {
                            UIControlService.msg_growl("error", "MESSAGE.FAIL_DELETE");
                            UIControlService.unloadLoading();
                        });
                    }
                }
            });
        }

        vm.insertVendorExperiences = insertVendorExperiences;
        function insertVendorExperiences() {
            
            UIControlService.loadLoading();

            getDataCivdExperiences().then(function (data) {
                var dataKirim = [];
                for (var i = 0; i < data.length; i++) {

                    var CurrencyId = 0;

                    if (data[i].jenisMataUang == 'USD') {
                        CurrencyId = 147;
                    } else if (data[i].jenisMataUang == 'EUR') {
                        CurrencyId = 44;
                    } else if (data[i].jenisMataUang == 'IDR') {
                        CurrencyId = 61;
                    }
                    else {
                        CurrencyId = 164;
                    }

                    dataKirim.push({
                        ContractName: data[i].namaPaketPekerjaan,
                        Address: data[i].alamat,
                        Agency: data[i].namaPenggunaJasa,
                        AgencyTelpNo: data[i].noTelepon,
                        ContractNo: data[i].noKontrakPO,
                        ContractValue: data[i].nilaiKontrakPO,
                        StartDate: data[i].tanggalKontrakPO,
                        EndDate: data[i].selesaiKontrakPO,
                        UploadDate: data[i].completedDate,
                        Remark: data[i].bidangSubBidang,
                        ExperienceType: new Date(data[i].selesaiKontrakPO) < UIControlService.getDateNow2("-") ? 3154 : 3153,
                        DocumentURL: data[i].fileBuktiPengalaman,
                        CIVDExperienceID: data[i].id,
                        ExpCurrID: CurrencyId
                    })
                }

                VendorExperienceService.insertVendorExperiencesCIVD({ dataVendorExperiences: dataKirim }, function (reply) {
                    UIControlService.unloadLoading();
                    if (reply.status === 200) {
                        initialize();
                    } else {
                        $.growl.error({ message: "MESSAGE.GAGAL_API" });
                        UIControlService.unloadLoading();
                    }
                }, function (err) {
                    //console.info("error:" + JSON.stringify(err));
                    //$.growl.error({ message: "Gagal Akses API >" + err });
                    UIControlService.unloadLoading();
                });


            })
        }

        function getDataCivdExperiences() {
            var defer = $q.defer();
            var url = "https://apiprovider.civd-migas.com/vendor/pengalaman?vendorId=" + vm.civdID;

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply.result)
                } else {
                    UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                    defer.reject(false);
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject(false);
            });
            return defer.promise;
        }

        vm.revisiVendorDetail = revisiVendorDetail;
        function revisiVendorDetail() {
            var data = {
                data: vm.vendorReviewData,
                langID: vm.langID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/revisi-modal/revisi-modal.html',
                controller: 'revisiModalController',
                controllerAs: 'revisiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

    }

})();// baru controller pertama
