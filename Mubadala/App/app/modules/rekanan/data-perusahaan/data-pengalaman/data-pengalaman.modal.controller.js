(function () {
	'use strict';

	angular.module("app").controller("formExpCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'VendorExperienceService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'UploaderService', 'item', 'ProvinsiService', 'AuthService', '$uibModalInstance', 'VendorRegistrationService', 'FileSaver', '$uibModal'];
	/* @ngInject */
	function ctrl($translatePartialLoader, VendorExperienceService, UIControlService, GlobalConstantService, UploadFileConfigService, UploaderService, item, ProvinsiService, AuthService, $uibModalInstance, VendorRegistrationService, FileSaver, $uibModal) {
		var vm = this;
		//cek hanya bisa input pengalaman 5 tahun terakhir
		//console.info(JSON.stringify(item));
		vm.isAdd = item.isAdd;
		vm.dataExp = {};
		var dataEdit = item.item;
		if (vm.isAdd != true) {
			vm.CurrencyRef = dataEdit.CurrencyRef;
		}
		console.log(item);
		console.log(item.item.FieldRef);
		vm.fileUpload;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isCalendarOpened = [false, false, false, false];
		vm.IDN_id = 360;
		vm.regionID;
		vm.countryID;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.datepelaksanaan;
		vm.dateselesai;
		vm.lokasidetail;
		vm.IsCR = item.isCR;
		vm.VendorCategoryID = item.item.VendorCategoryID;
		//console.info(JSON.stringify(item));
		vm.selectedCurrencies = [];
		vm.listCurrencies = [];
		vm.dataBidangUsaha = [];
		//if (vm.isAdd === false) {
		//	vm.cekTemporary = dataEdit.IsTemporary;
	    //}
		vm.myConfig = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "CountryID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig2 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "StateID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig3 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "CityID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig5 = {
		    maxItems: 1,
		    optgroupField: "Symbol",
		    labelField: "symbolLabel",
		    valueField: "CurrencyID",
		    searchField: ["Symbol", "Label"],
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.init = init;
		function init() {
            //console.info(item)
			//$translatePartialLoader.addPart("vendor-experience");
		    if (localStorage.getItem('currLang').toLowerCase() == 'id') {
		        vm.btnBack = !vm.isAdd ? 'Batal' : 'Kembali';
		    }
		    else if (localStorage.getItem("currLang").toLowerCase() == 'en') {
		        vm.btnBack = !vm.isAdd ? 'Cancel' : 'Back';
		    }
			loadTypeExp();
			if (vm.isAdd === true) {
				vm.dataExp = new dataField('', null, '', '', '', '', 0, '', '', new Date(), 0, 0, '', 0, '', null, 0,'',0);
				loadCountries();
			} else {console.log(dataEdit.ExperienceRef.Value)
				vm.dataExp = new dataField(dataEdit.ContractName, dataEdit.Location, dataEdit.Address, dataEdit.Agency, dataEdit.AgencyTelpNo,
                    dataEdit.ContractNo, dataEdit.ContractValue, new Date(Date.parse(dataEdit.StartDate)),
                    new Date(Date.parse(dataEdit.EndDate)), new Date(Date.parse(dataEdit.UploadDate)), dataEdit.Field, dataEdit.FieldType,
					dataEdit.Remark, dataEdit.ExperienceType, dataEdit.DocumentURL, dataEdit.StateLocation, dataEdit.ExpCurrID, 
					dataEdit.ExperienceRef, dataEdit.FieldTypeRef.RefID);
				vm.stateID = dataEdit.StateLocation;
				vm.cityID = dataEdit.CityLocation.CityID;
				vm.currencyID = dataEdit.ExpCurrID;
				//console.info("dataexp:" + JSON.stringify(vm.CurrencyRef));

				vm.regionID = dataEdit.StateLocationRef.Country.Continent.ContinentID;
				vm.countryID = dataEdit.StateLocationRef.CountryID;
				vm.datepelaksanaan = UIControlService.getStrDate(vm.dataExp.StartDate);
				vm.dateselesai = UIControlService.getStrDate(vm.dataExp.EndDate);
				if (vm.countryID === 360) {
					vm.lokasidetail = dataEdit.CityLocation.Name + ", " + dataEdit.StateLocationRef.Name + ", " + dataEdit.StateLocationRef.Country.Name + ", " + dataEdit.StateLocationRef.Country.Continent.Name;
				} else {
					vm.lokasidetail = dataEdit.StateLocationRef.Name + ", " + dataEdit.StateLocationRef.Country.Name + ", " + dataEdit.StateLocationRef.Country.Continent.Name;
				}
				loadCountries(dataEdit.StateLocationRef);
			}
			//loadCountries(dataEdit.StateLocationRef);
			loadTypeTender();
			getTypeSizeFile();
			loadCurrency(dataEdit);
		}

		vm.checkStartDate = checkStartDate;
		function checkStartDate(selectedStartDate) {
		    if (vm.dataExp.StartDate === undefined) {
		        var errorMessage = localStorage.getItem("currLang").toLowerCase() == 'id' ? "Tanggal tidak valid" : "Invalid date";
		        UIControlService.msg_growl("error", errorMessage);
		        vm.dataExp.StartDate = '';
		        return;
		    }
			var today = new Date();
			if (today < selectedStartDate) {
				UIControlService.msg_growl("warning", "ERROR.START_DATE");
				vm.dataExp.StartDate = " ";
			}
		}

		vm.checkEndDate = checkEndDate;
		function checkEndDate(selectedEndDate, selectedStartDate, selectedExpType) {
		    if (vm.dataExp.EndDate === undefined) {
		        var errorMessage = localStorage.getItem("currLang").toLowerCase() == 'id' ? "Tanggal tidak valid" : "Invalid date";
		        UIControlService.msg_growl("error", errorMessage);
		        vm.dataExp.EndDate = '';
		        return;
		    }
			//console.info(selectedExpType);
			if (selectedEndDate < selectedStartDate) {
				UIControlService.msg_growl("warning", "ERROR.END_DATE");
				vm.dataExp.EndDate = " ";
			}
			else if (selectedExpType == 3153) {
				var convertedEndDate = moment(selectedEndDate).format('YYYY-MM-DD');

				//console.info(convertedEndDate);
				var datenow = new Date();
				var convertedDateNow = moment(datenow).format('YYYY-MM-DD');
				//console.info(convertedDateNow);
				if (datenow < selectedEndDate) {
					UIControlService.msg_growl("warning", "ERROR.END_DATE_FINISH_CONTRACT");
				}
				else {
					//console.info("masuk");
				}

				vm.datesNow = new Date(Date.now()).toString();
				var yearNow = vm.datesNow.substring(11, 15);
				//var monthNow = monthToNumber(vm.dateNow.substring(4, 7));
				//var dateNow = vm.dateNow.substring(8, 10);
				var validYear = Number(yearNow) - 7;
				var dateInput = new Date(selectedEndDate).toString();
				var year = dateInput.substring(11, 15);
				//var month = monthToNumber(dateInput.substring(4, 7));
				//var date = dateInput.substring(8, 10);
				if (Number(year) < validYear) {
				    UIControlService.unloadLoadingModal();
				    vm.dataExp.EndDate = "";
				    vm.dataExp.StartDate = "";
				    UIControlService.msg_growl("error", "ERROR.ENDDATE");
				    return;
				}



			}
		}

		function monthToNumber(input) {
		    var result = 0;
		    if (input == "JAN") {
		        result = 1;
		    } else if (input == "FEB") {
		        result = 2;
		    } else if (input == "MAR") {
		        result = 3;
		    } else if (input == "APR") {
		        result = 4;
		    } else if (input == "MAY") {
		        result = 5;
		    } else if (input == "JUN") {
		        result = 6;
		    } else if (input == "JUL") {
		        result = 7;
		    } else if (input == "AUG") {
		        result = 8;
		    } else if (input == "SEP") {
		        result = 9;
		    } else if (input == "OCT") {
		        result = 10;
		    } else if (input == "NOV") {
		        result = 11;
		    } else if (input == "DEC") {
		        result = 12;
		    }
		    return result;
		}

		vm.listTypeExp = [];
		vm.cekTypeExp = 3154;
		function loadTypeExp() {
			UIControlService.loadLoadingModal("MESSAGE.LOADING_EXPERIENCE_TYPE");
			VendorExperienceService.typeExperience(
            function (reply) {
            	UIControlService.unloadLoadingModal();
            	vm.listTypeExp = reply.data.List;

            }, function (err) {
            	UIControlService.msg_growl("error", "MESSAGE.API");
            	UIControlService.unloadLoadingModal();
            });
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.VENDOR.EXPERIENCE", function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				return;
			});
		}

		vm.defineCountry = defineCountry;
		function defineCountry(id) {
		    vm.countryID = id;
		    loadStates();
		}

		vm.loadStates = loadStates;
		function loadStates(country) {
		    if (!country) {
		        for (var i = 0; i < vm.countryList.length; i++) {
		            if (vm.countryID == vm.countryList[i].CountryID) {
		                country = vm.countryList[i];
		                vm.selectedCountry = country;
		            }
		        }
		        vm.selectedState = "";
		        vm.selectedCities = "";
		        vm.stateID = "";
		        vm.cityID = "";
		    }
		    if (country) {
		        vm.dataExp.StateLocation = null;
		        loadRegions(country.CountryID);
		        UIControlService.loadLoadingModal("MESSAGE.LOADING_STATE");
		        VendorExperienceService.getStates(country.CountryID,
                    function (response) {
                        vm.stateList = response.data;
                        for (var i = 0; i < vm.stateList.length; i++) {
                            if (country.StateID === vm.stateList[i].StateID) {
                                vm.selectedState = vm.stateList[i];
                                //console.info("...." + JSON.stringify(vm.selectedState.Country.Code));
                                if (vm.selectedState.Country.Code === 'IDN') {
                                    changeState();
                                    break;
                                }
                            }
                        }
                        UIControlService.unloadLoadingModal();
                    });
		    }
		}

		vm.loadCountries = loadCountries;
		function loadCountries(data) {
			UIControlService.loadLoadingModal("LOADERS.LOADING_COUNTRY");
			//console.info("loadCountries");
			VendorExperienceService.getCountries(
                function (response) {
                	vm.countryList = response.data;
                	for (var i = 0; i < vm.countryList.length; i++) {
                		if (data !== undefined) {
                			if (data.CountryID === vm.countryList[i].CountryID) {
                				vm.selectedCountry = vm.countryList[i];
                				loadStates(data);
                				break;
                			}
                		}
                	}
                	UIControlService.unloadLoadingModal();
                });
		}

		vm.loadRegions = loadRegions;
		function loadRegions(data) {
			UIControlService.loadLoadingModal("LOADERS.LOADING_REGION");
			VendorExperienceService.getRegions({ CountryID: data },
                function (response) {
                	vm.selectedRegion = response.data;
                	UIControlService.unloadLoadingModal();
                }
            );
		}

		/*city dkk
        vm.listRegions = [];
        vm.selectedRegions;
        function getRegion() {
            ProvinsiService.getRegion(
                function (response) {
                    vm.listRegions = response.data;
                    if (vm.isAdd === false) {
                        for (var i = 0; i < vm.listRegions.length; i++) {
                            if (vm.regionID === vm.listRegions[i].ContinentID) {
                                vm.selectedRegions = vm.listRegions[i];
                                changeRegion();
                                break;
                            }
                        }
                    }
                },
            function (response) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.changeRegion = changeRegion;
        vm.listCountry = [];
        vm.selectedCountry;
        function changeRegion() {
            console.info(JSON.stringify( vm.selectedRegions));
            ProvinsiService.getCountries(vm.selectedRegions.ContinentID,
               function (response) {
                   //console.info("neg>" + JSON.stringify(response));
                   vm.listCountry = response.data;
                   if (vm.isAdd === false) {
                       for (var i = 0; i < vm.listCountry.length; i++) {
                           if (vm.countryID === vm.listCountry[i].CountryID) {
                               vm.selectedCountry = vm.listCountry[i];
                               changeCountry();
                               break;
                           }
                       }
                   }
               },
           function (response) {
               UIControlService.msg_growl("error", "Gagal Akses API");
               return;
           });
        }

        vm.changeCountry = changeCountry;
        vm.listState = [];
        vm.selectedState;
        function changeCountry() {
            console.info(JSON.stringify(vm.selectedCountry));
            ProvinsiService.getStates(vm.selectedCountry.CountryID,
               function (response) {
                   vm.listState = response.data;
                   //console.info(">> " + JSON.stringify(vm.listState));
                   if (vm.isAdd === false) {
                       for (var i = 0; i < vm.listState.length; i++) {
                           if (vm.dataExp.StateLocation === vm.listState[i].StateID) {
                               vm.selectedState = vm.listState[i];
                               changeState();
                               break;
                           }
                       }
                   }

               },
           function (response) {
               UIControlService.msg_growl("error", "Gagal Akses API");
               return;
           });
        }*/
		vm.defineState = defineState;
		function defineState(id) {
		    vm.stateID = id;
		    changeState();
		}

		vm.changeState = changeState;
		vm.listCities = [];
		vm.selectedCities;
		function changeState() {
		    if (vm.stateID == undefined) {
		        //vm.selectedState = "";
		        vm.cityID = "";
		    }
		    for (var i = 0; i < vm.stateList.length; i++) {
		        if (vm.stateID == vm.stateList[i].StateID) {
		            vm.selectedState = vm.stateList[i];
		            break;
		        }
		    }
		    vm.dataExp.StateLocation = vm.selectedState.StateID;

		    if(!(vm.selectedState.CountryID === vm.IDN_id)){
		        vm.dataExp.Location = null;
		    }

			ProvinsiService.getCities(vm.selectedState.StateID, function (response) {
				vm.listCities = response.data;
				//console.info(">> " + JSON.stringify(vm.listCities));
				if (vm.isAdd === false) {
				    for (var i = 0; i < vm.listCities.length; i++) {
				        if (vm.dataExp.Location) {
				            if (vm.dataExp.Location === vm.listCities[i].CityID) {
				                vm.selectedCities = vm.listCities[i];
				                changeCities();
				                break;
				            }
				        }
					}
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}
		vm.defineCity = defineCity;
		function defineCity(id) {
		    vm.cityID = id;
		    changeCities();
		}

		vm.changeCities = changeCities;
		function changeCities() {
			//console.info(JSON.stringify(vm.selectedCities));
			vm.dataExp.StateLocation = vm.selectedState.StateID;
			if (vm.selectedCountry.CountryID === vm.IDN_id) {
			    for (var i = 0; i < vm.listCities.length; i++) {
			        if (vm.cityID == vm.listCities[i].CityID) {
			            vm.selectedCities = vm.listCities[i];
			            break;
			        }
			    }
				vm.dataExp.Location = vm.selectedCities.CityID;
			}
		}
		//end city dkk

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		/*field*/
		vm.selectedTypeTender;
		vm.listTypeTender;
		function loadTypeTender() {
			VendorExperienceService.getTypeTender(function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					console.log("AAA")
					vm.listTypeTender = reply.data.List;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listTypeTender.length; i++) {
							if (vm.listTypeTender[i].RefID === vm.dataExp.FieldType) {
								vm.selectedTypeTender = vm.listTypeTender[i];
								loadBusinessField();
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.loadCurrency = loadCurrency;
		vm.selectedCurrencies = [];
		function loadCurrency(data) {
			VendorExperienceService.getCurrencies(function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    //console.info(data.ExpCurrID);
				    vm.listCurrencies = reply.data;
				    for (var i = 0; i < vm.listCurrencies.length; i++) {
				        vm.listCurrencies[i].symbolLabel = vm.listCurrencies[i].Symbol + ' | ' + vm.listCurrencies[i].Label;
				        if (vm.isAdd === false) {
				            if (data.ExpCurrID === vm.listCurrencies[i].CurrencyID) {
				                vm.selectedCurrencies = vm.listCurrencies[i];
				            }
				        }
				    }
					
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.changeCurr = changeCurr;
		function changeCurr(id) {
		    //console.info(data);
			 vm.currencyID = id;
			 vm.dataExp.ExpCurrID = vm.currencyID;
			 vm.listCurrencies.forEach(function (item) {
			     if (item.CurrencyID == vm.currencyID) {
			         vm.selectedCurrencies = item;
			     }
			 });
		}

		vm.listBussinesDetailField = []
		vm.changeTypeTender = changeTypeTender;
		function changeTypeTender() {
			//console.info(JSON.stringify(vm.selectedTypeVendor));
			if (vm.selectedTypeTender === undefined) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_TYPETENDER");
				return;
			}
			vm.dataExp.FieldType = vm.selectedTypeTender.RefID;
			loadBusinessField();
			vm.listBussinesDetailField = [];
		}

		vm.loadBusinessField = loadBusinessField;
		vm.selectedBusinessField;
		vm.listBusinessField = [];
		function loadBusinessField() {
			//secara default, GoodsOrService harus bernilai 3092
			VendorExperienceService.SelectBusinessField({
				GoodsOrService: 3092
			}, function (response) {
				if (response.status === 200) {
					vm.listBusinessField = response.data;
					if (vm.isAdd === false) {
					    //vm.dataBidangUsaha = vm.listBusinessField;
						for (var i = 0; i < vm.listBusinessField.length; i++) {
							if (vm.listBusinessField[i].ID === vm.dataExp.Field) {
								//vm.selectedBusinessField = vm.listBusinessField[i];
							    vm.dataBidangUsaha.push(vm.listBusinessField[i]);
								break;
							} 
						}

						getBusinessType(vm.dataBidangUsaha[0].BusinessFieldTypeID);
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_FIELD");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		function getBusinessType(id) {
		        //console.info(vm.dataExp.Field);
		        VendorExperienceService.getBusinessType({
		            GoodsOrService: id
		        }, function (response) {
		            if (response.status === 200) {
		                vm.dataBidangUsaha[0].BusinessFieldName = response.data.BusinessTypeName;
		                console.info(vm.dataBidangUsaha);
		            } else {
		                UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_FIELD");
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            return;
		        });
		    
		}

		vm.changeBusinessField = changeBusinessField;
		function changeBusinessField() {
			//console.info("field:" + JSON.stringify(vm.selectedBusinessField));
			vm.dataExp.Field = vm.selectedBusinessField.ID;
		}
		/*end field*/

		/*proses upload file*/
		function uploadFile() {
			AuthService.getUserLogin(function (reply) {
				//console.info(JSON.stringify(reply));
			    vm.dataExpLogin = reply.data.CurrentUsername + "_expe";
			    if (!vm.fileUpload || vm.fileUpload == undefined) {
			        if (vm.isAdd == true) {
			            UIControlService.unloadLoadingModal();
			            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			            return;
			        } else {
			            vm.dataExp.DocumentURL = item.DocumentURL;
			        }
			    }
				if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.dataExpLogin);
				} else {
				    UIControlService.unloadLoadingModal();
				    vm.fileUpload = "";
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});

		}

		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			//UIControlService.loadLoadingModal("MESSAGE.LOADING_UPLOAD");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, dates, function (response) {
				//console.info()
				//UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.dataExp.DocumentURL = url;
					vm.pathFile = vm.folderFile + url;
					UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
					saveProcess();

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.NOT_COMPLETE")
				UIControlService.unloadLoadingModal();
			});

		}

		function validateFileType(file, allowedFileTypes) {
		    var valid_size = allowedFileTypes[0].Size;
		    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
		    if (size_file > valid_size) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE");
		        return false;
		    }
			//console.info(JSON.stringify(allowedFileTypes));
			//if (!file || file.length == 0) {
			//	UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			//	return false;
			//}
			return true;
		}

		/* end proses upload*/

		function validateForm() {
		    if (vm.dataExp.ExperienceType == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.PROSESPEKERJAAN");
		        return false;
		    }

		    if (vm.dataExp.ContractName == "" || vm.dataExp.ContractName == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.CONTRACTNAME");
		        return false;
		    }

		    if (vm.dataExp.Agency == "" || vm.dataExp.Agency == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.AGENCY");
		        return false;
		    }

		    if (vm.dataExp.FieldType == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.FIELDTYPE");
		        return false;
		    }

		    if (vm.dataExp.Field == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.FIELD");
		        return false;
		    }

		    if (!vm.selectedCountry || vm.countryID == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.COUNTRY");
		        return false;
		    }

		    if (vm.dataExp.StateLocation == undefined || vm.dataExp.StateLocation == null || vm.stateID == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.STATE");
		        return false;
		    }

		    if (vm.selectedCountry.CountryID === 360 && vm.dataExp.Location == null || vm.cityID == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.CITY");
		        return false;
		    }

		    if (vm.dataExp.Address == "" || vm.dataExp.Address == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.ADDRESS");
		        return false;
		    }

		    if (vm.dataExp.AgencyTelpNo == "" || vm.dataExp.AgencyTelpNo == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.NOTELP");
		        return false;
		    }

		    if (vm.dataExp.ContractNo == "" || vm.dataExp.ContractNo == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.CONTRACTNO");
		        return false;
		    }

		    if (vm.dataExp.ExpCurrID == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.CURR");
		        return false;
		    }

		    if (vm.dataExp.ContractValue == "" || vm.dataExp.ContractValue == undefined) {
		        UIControlService.msg_growl("error", "MESSAGE.VALUE");
		        return false;
		    }

		    if (!vm.dataExp.StartDate || vm.dataExp.StartDate == "") {
		        UIControlService.msg_growl("error", "MESSAGE.STARTDATE");
		        return false;
		    }

		    if (!vm.dataExp.EndDate || vm.dataExp.EndDate == "") {
		        UIControlService.msg_growl("error", "MESSAGE.ENDDATE");
		        return false;
		    }

		    return true;

		}


		/*proses simpan*/
		vm.saveData = saveData;
		function saveData() {
			/*handle change request*/
			//if (vm.IsCR === false) vm.IsTemporary = false;
			//else if (vm.IsCR === true) vm.IsTemporary = true;

			//if (vm.isAdd === true) { vm.Action = 0; }
			//if (vm.isAdd === false) { vm.Action = 1; }
			//vm.dataExp['IsTemporary'] = vm.IsTemporary;
			//vm.dataExp['Action'] = vm.Action;
			//if (vm.isAdd === true && vm.IsCR === true) {
			//vm.dataExp['RefVendorId'] = null;
			//}
			/*end handle change request*/
		    if (validateForm()) {
		        if (vm.fileUpload === undefined && vm.isAdd === true) {
		            vm.dataExp.DocumentURL = '';
		            uploadFile();
		        } else if (!(vm.fileUpload === undefined)) {
		            uploadFile();
		        } else if (vm.fileUpload === undefined && vm.isAdd === false) {
		            saveProcess();
		        }
		        vm.dataExp.ExperienceType = Number(vm.dataExp.ExperienceType);
		    }
		}

		function saveProcess() {
			//console.info(vm.IsCR);
			//console.info(vm.isAdd);
			//console.info(vm.cekTemporary);
		    //if (vm.isAdd == true || ((vm.IsCR === true) && (vm.cekTemporary === false))) {
		    vm.dataExp['StartDate'] = UIControlService.getStrDate(vm.dataExp.StartDate);
		    vm.dataExp['EndDate'] = UIControlService.getStrDate(vm.dataExp.EndDate);

		    UIControlService.loadLoadingModal();
			if (vm.isAdd == true) {
				vm.dataExp['ExpCurrID'] = vm.dataExp.ExpCurrID;
				vm.dataExp['StateLocation'] = vm.selectedState.StateID;
				
				//console.info(vm.dataExp);
				VendorExperienceService.Insert(vm.dataExp, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.NOT_COMPLETE");
					UIControlService.unloadLoadingModal();
				});
			}
				//else if (vm.isAdd === false || ((vm.IsCR === true) && (vm.cekTemporary === true))) {
			else if (vm.isAdd === false) {
				vm.dataExp['ID'] = dataEdit.ID;
				//vm.dataExp['RefVendorId'] = dataEdit.ID;
				//console.info("edit:"+JSON.stringify(vm.dataExp));
				VendorExperienceService.Update(vm.dataExp, function (reply) {
					UIControlService.unloadLoadingModal();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
						$uibModalInstance.close();
					} else {
						UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
						return;
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.NOT_COMPLETE");
					UIControlService.unloadLoadingModal();
				});
			}

		}

	    /*end proses simpan*/

		vm.downloadFileCivd = downloadFileCivd;
		function downloadFileCivd(url, name) {
		    UIControlService.loadLoadingModal();

		    VendorRegistrationService.getCIVDData({
		        Keyword: url
		    }, function (response) {
		        var reply = JSON.parse(response.data);
		        if (reply.status === 200) {
		            var byteCharacters = atob(reply.result);

		            var byteNumbers = new Array(byteCharacters.length);
		            for (let i = 0; i < byteCharacters.length; i++) {
		                byteNumbers[i] = byteCharacters.charCodeAt(i);
		            }

		            var byteArray = new Uint8Array(byteNumbers);

		            var blob = new Blob([byteArray], {
		                type: "application/pdf"
		            });

		            FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
		            UIControlService.unloadLoadingModal();

		        } else {
		            UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		        }
		    }, function (error) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		    });
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}

		function dataField(ContractName, Location, Address, Agency, AgencyTelpNo, ContractNo, ContractValue, StartDate,
			EndDate, UploadDate, Field, FieldType, Remark, ExperienceType, DocumentURL, StateLocation, ExpCurrID, ExperienceRef,
			TenderType) {
			var self = this;
			self.ContractName = ContractName;
			self.Location = Location;
			self.Address = Address;
			self.Agency = Agency;
			self.AgencyTelpNo = AgencyTelpNo;
			self.ContractNo = ContractNo;
			self.ContractValue = ContractValue;
			self.StartDate = StartDate;
			self.EndDate = EndDate;
			self.UploadDate = UploadDate;
			self.Field = Field;
			self.FieldType = FieldType;
			self.Remark = Remark;
			self.ExperienceType = ExperienceType;
			self.DocumentURL = DocumentURL;
			self.StateLocation = StateLocation;
			self.ExpCurrID = ExpCurrID;
			self.ExperienceRef = ExperienceRef;
			self.TenderType = TenderType;
		}

		vm.addBidangUsaha = addBidangUsaha;
		function addBidangUsaha() {

		    //if (vm.jenisIzinUsaha == '') {
		    //    UIControlService.msg_growl('error', "ERRORS.PILIH_JENIS_IJIN");
		    //    return;
		    //}

		    var data = {
		        dataBidangUsaha: vm.dataBidangUsaha,
		        tipeTender: vm.selectedTypeTender
		    };

		    var modalInstance = $uibModal.open({
		        templateUrl: "app/modules/rekanan/data-perusahaan/data-pengalaman/pilihGolonganBidangUsaha.modal.html",
		        controller: 'PilihGolonganBidangUsahaPengalamanModalController',
		        controllerAs: 'PilihGolonganBidangUsahaPengalamanModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (reply) {
		        console.log(reply)
		        if (reply != "") {
		            vm.dataBidangUsaha = reply;
		            vm.dataExp.Field = vm.dataBidangUsaha[0].ID;

		        }
		    });
		}

		vm.removeBidangUsaha = removeBidangUsaha;
		function removeBidangUsaha(data) {
		    //if (data.DataNew) {

		        var id = $.grep(vm.dataBidangUsaha, function (n) { return n.ID == data.ID; });
		        vm.dataBidangUsaha.splice(id, 1);

		    //} else {
		    //    var id = vm.dataBidangUsaha.findIndex(x=> x.ID == data.ID);
		    //}

		}
	}
})();

