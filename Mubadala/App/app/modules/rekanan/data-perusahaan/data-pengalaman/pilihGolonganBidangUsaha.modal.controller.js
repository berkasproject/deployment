﻿(function () {
    'use strict';
    
    angular.module("app").controller("PilihGolonganBidangUsahaPengalamanModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'item', '$uibModal', 'IzinUsahaService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService', 'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService', 'FileSaver', 'VendorRegistrationService'];

    function ctrl($http, $translate, $translatePartialLoader, $location, UIControlService, item, $uibModal, IzinUsahaService, $uibModalInstance, UploadFileConfigService, UploaderService, AuthService, $filter, ProvinsiService, GlobalConstantService, FileSaver, VendorRegistrationService) {
        var vm = this;
        vm.data = [];
        vm.dataSelected = item.dataBidangUsaha;

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal();
            IzinUsahaService.getMstBussinessFieldType(
            {},
            function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.data = data;
                } else {
                    UIControlService.msg_growl('error', "ERRORS.ERROR_API");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "ERRORS.ERROR_API");

            })
        }
       
        vm.pilihBidangUsaha = pilihBidangUsaha;
        function pilihBidangUsaha(data) {

            var data = {
                item: data,
                dataSelected: vm.dataSelected,
                tipeTender: item.tipeTender
            };

            var modalInstance = $uibModal.open({
                templateUrl: "app/modules/rekanan/data-perusahaan/data-pengalaman/pilihBidangUsahaTree.modal.html",
                controller: 'PilihBidangUsahaTreePengalamanModalController',
                controllerAs: 'PilihBidangUsahaTreePengalamanModalCtrl',
                backdrop:'static',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                vm.dataSelected = reply;
                $uibModalInstance.close(vm.dataSelected);
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();