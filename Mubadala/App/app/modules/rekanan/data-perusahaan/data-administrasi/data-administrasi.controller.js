(function () {
	'use strict';

	angular.module("app").controller("DataAdministrasiCtrl", ctrl);

	ctrl.$inject = ['$uibModal', '$translatePartialLoader', 'VerifiedSendService', 'DataAdministrasiService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'VendorRegistrationService', '$rootScope','FileSaver','$q'];
	/* @ngInject */
	function ctrl($uibModal, $translatePartialLoader, VerifiedSendService, DataAdministrasiService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, VendorRegistrationService, $rootScope, FileSaver,$q) {
		var vm = this;
		var flag;
		vm.change = false;
		vm.aaass = "Negara";
		vm.vendorContactForm;
		vm.address1;
		vm.pageSize = 10;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.bisaMengubahData;
		vm.dataAdmin = {};
		vm.arrProvince = [];
		vm.arrTown = [];
		vm.administrasi = [];
		vm.contact = [];
		vm.id_page_config = 1;
		vm.businessID;
		vm.administrasiDate = {};
		vm.isCalendarOpened = [false, false, false, false];
		vm.pathFile;
		vm.fileUpload;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;
		vm.selectedTypeVendor = {};
		vm.listCurrFalse = [];
		vm.listPersFalse = [];
		vm.address = {
			AddressID: 0,
			AddressInfo: "",
			PostalCode: "",
			StateID: 0,
			CityID: 0,
			DistrictID: 0
		};
		vm.flag = false;
		vm.addressFlag = 0;
		vm.addressAlterFlag = 0;
		vm.AddressAlterId = 0;
		vm.address2 = {
			AddressID: 0,
			AddressInfo: "",
			PostalCode: "",
			StateID: 0,
			CityID: 0,
			DistrictID: 0
		};
		vm.addresses = [];
		vm.contact1 = {
			ContactID: 0,
			Name: "",
			Website: "",
			Phone: "",
			MobilePhone: "",
			Email: "",
			email3: "",
			Address: {}
		};
		vm.selectedBE = null;
		vm.Contact = [];
		vm.contactpersonal = {};
		vm.isApprovedCRAdm = false;
		vm.isApprovedCRBF = false;
		vm.isCalendarOpened = [false, false, false, false];
		vm.IsEdit = false;
		vm.IsEditAlter = false;
		vm.initialize = initialize;
		vm.menuIndex = 1;
		vm.addresses = [];
		vm.addressType = "";
		vm.AddressRemovedMain = [];
		vm.AddressRemovedBranch = [];
		vm.vendorCategoryID = 0;
		vm.revisiVendor = false;//false
		vm.langID = true;


		vm.myConfig = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "CountryID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig2 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "StateID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig3 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "CityID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig4 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "DistrictID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig5 = {
		    maxItems: 1,
		    optgroupField: "PhonePrefix",
		    labelField: "namePhonePrefix",
		    valueField: "PhonePrefix",
		    searchField: ["Name", "PhonePrefix"],
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig6 = {
		    maxItems: 1,
		    optgroupField: "AssosiationName",
		    labelField: "AssosiationName",
		    valueField: "AssosiationID",
		    searchField: "AssosiationName",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.isVerified = null;
		function initialize() {
		    vm.addresses = [];

		    if (localStorage.getItem('currLang').toLowerCase() != 'id') {
		        vm.langID = false;
		    }
			$translatePartialLoader.addPart('data-administrasi');
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.ADMINISTRATION.PKP", function (response) {
				//UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
				    UIControlService.unloadLoading();
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.NPWP", function (response) {
			    //UIControlService.unloadLoading();
			    if (response.status == 200) {
			        vm.name1 = response.data.name;
			        vm.idUploadConfigs1 = response.data;
			        vm.idFileTypes1 = generateFilterStrings(response.data);
			        vm.idFileSize1 = vm.idUploadConfigs1[0];

			    } else {
			        UIControlService.unloadLoading();
			        UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
			        return;
			    }
			}, function (err) {
			    UIControlService.msg_growl("error", "MESSAGE.API");
			    UIControlService.unloadLoading();
			    return;
			});
			jLoad(1);
			//loadCountryAlternatif();
			
			
			
		}

		vm.changeForm = changeForm;
		function changeForm() {
		    vm.change = true;
		}

		vm.changeAssosiation = changeAssosiation;
		function changeAssosiation(id) {
		    vm.asosiasiID = id;
		    for (var i = 0; i < vm.listAssociation.length; i++) {
		        if (vm.asosiasiID == vm.listAssociation[i].AssosiationID) {
		            vm.selectedAssociation = vm.listAssociation[i];
		        }
		    }
		}


		function loadVendorVerificationReview() {
		    var defer = $q.defer();
		    VerifiedSendService.getVendorVerificationReview({
		        MenuID: 1033
		    }, function (reply) {
		        if (reply.status == 200) {
		            var data = reply.data;
		            vm.vendorReviewData = data;
		            //console.log(data)
		            if (data != null) {
		                if (data.ReviewStatus == 4526) {
		                    vm.revisiVendor = true;
		                } else {
		                    vm.revisiVendor = false;
		                }
		                if (vm.langID) {
		                    vm.revisiName = data.Locale_Id;
		                } else {
		                    vm.revisiName = data.Locale_En;
		                }
		            }
		            defer.resolve(true)
		        }
		        else {
		            UIControlService.unloadLoading();
                }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		        defer.reject;
		        return;
		    })
		    return defer.promise;
		}
        
		function cekCR() {
		    DataAdministrasiService.cekCR(function (reply) {
		        if (reply.status === 200) {
		            vm.isCR = reply.data;
		            getAllEmail();
		        } else {
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		function getAllEmail() {
		    //UIControlService.loadLoading("Loading...");
		    DataAdministrasiService.getAllEmail(
                function (response) {
                    if (response.status == 200) {
                        vm.allEmail = response.data.List;
                        loadVendorVerificationReview().then(function () {
                            loadCheckCR();
                        });
                        //vm.allEmail ? UIControlService.unloadLoading() : UIControlService.unloadLoading();
                    } else {
                        UIControlService.unloadLoading();
                        handleRequestError(response);
                    }
                },
                handleRequestError);
		}

		vm.changeAddressType = changeAddressType;
		function changeAddressType() {
		    vm.selectedCountry = [];
		    vm.selectedState = [];
		    vm.listRegion.Name = "";
		    vm.address1 = "";
		    vm.selectedAssociation = [];
		    vm.postalcode = "";
		}
		function loadOfficeType() {
		    //UIControlService.loadLoading("LOADERS.LOADING_OFFICE_TYPE");
		    VendorRegistrationService.getOfficeType(
                function (response) {
                    if (response.status == 200) {
                        vm.officeTypes = response.data;
                        cekCR();
                    } else {
                        UIControlService.unloadLoading();
                        handleRequestError(response);
                    }
                },
                handleRequestError);
		}

		vm.addAddress = addAddress;
		function addAddress() {

			if (!vm.countryIDInformasi){
				UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_COUNTRY"); 
				return;
			}
		    if (vm.selectedCountry.Code == "IDN") {
				console.log("YAA ");
				console.log(vm.selectedCountry)
		        if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_MAIN") {
		            if (!vm.addressType) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_OFFICETYPE"); 
						return; 
					}
		            else if (!vm.address1) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_ADDRESS"); 
						return; 
					}
		            else if (!vm.selectedCountry || vm.selectedCountry.length == 0) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_COUNTRY"); 
						return; 
					}
		            else if (!vm.selectedState) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_PROVINCE"); 
						return; 
					}
		            else if (!vm.selectedCity) {
						IControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_CITY");
						return;
					} 
					else if (!vm.selectedDistrict) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_DISTRICT"); 
						return; 
					}
		            //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		            else if (!vm.selectedAssociation) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_ASSOCIATION"); 
						return; 
					}
		            else {
		                var centerOffice = 0;
		                var id = vm.addresses.length + 1;
		                var VendorContactType = {
		                    Name: "VENDOR_OFFICE_TYPE_MAIN",
		                    RefID: 1042,
		                    Type: "VENDOR_OFFICE_TYPE",
		                    Value: "VENDOR_OFFICE_TYPE_MAIN",
		                    VendorContactTypeID: 1042
		                };
		                for (var i = 0; i < vm.addresses.length; i++) {
		                    if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                        UIControlService.msg_growl("error", "OFFICE_MESSAGE.ALREADY_OFFICETYPE");
		                        vm.addressType = "";
		                        vm.countryIDInformasi = undefined;
		                        vm.stateIDInformasi = undefined;
		                        vm.cityIDInformasi = undefined;
		                        vm.districtIDInformasi = undefined;
		                        vm.postalcode = "";
		                        vm.AsosiasiID = undefined;
		                        vm.address1 = "";
		                        vm.listRegion.Name = "";
		                        return;
		                        centerOffice += 1;
		                    }
		                }
		                if (centerOffice <= 0) {
		                    vm.tmp = {
		                        id: vm.addresses.length + 1,
		                        AddressID: 0,
		                        type: vm.addressType.Value,
		                        address: vm.address1,
		                        countryID: vm.selectedCountry.CountryID,
		                        countryName: vm.selectedCountry.Name,
		                        stateID: vm.selectedState.StateID,
		                        stateName: vm.selectedState.Name,
		                        cityID: vm.selectedCity.CityID,
		                        cityName: vm.selectedCity.Name,
		                        districtID: vm.selectedDistrict.DistrictID,
		                        districtName: vm.selectedDistrict.Name,
		                        postalCode: vm.postalcode,
		                        VendorContactType: VendorContactType,
		                        ContactID: 0,
		                        AssociationID: vm.selectedAssociation.AssosiationID,
                                AssociationName: vm.selectedAssociation.AssosiationName
		                    }
		                    vm.addresses.push(vm.tmp);
		                    vm.change = true;
		                    vm.selectedCountry = [];
		                    vm.selectedState = [];
		                    vm.listRegion.Name = "";
		                    vm.address1 = "";
		                    //vm.selectedAssociation = [];
		                    vm.postalcode = "";
		                    vm.addressType = "";
		                    vm.asosiasiID = "";
		                }
		            }
		        } else if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_BRANCH") {
		            if (!vm.addressType) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_OFFICETYPE"); 
						return; 
					}
		            else if (!vm.address1) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_ADDRESS"); 
						return; 
					}
		            else if (!vm.selectedCountry || vm.selectedCountry.length == 0) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_COUNTRY"); 
						return; 
					}
		            else if (!vm.selectedState) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_PROVINCE"); 
						return; 
					}
		            else if (!vm.selectedCity) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_CITY"); 
						return; 
					}
		            else if (!vm.selectedDistrict) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_DISTRICT"); 
						return; 
					}
		            //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		            else {
		                var centerOffice = 0;
		                var id = vm.addresses.length + 1;
		                var VendorContactType = {
		                    Name: "VENDOR_OFFICE_TYPE_BRANCH",
		                    RefID: 1043,
		                    Type: "VENDOR_OFFICE_TYPE",
		                    Value: "VENDOR_OFFICE_TYPE_BRANCH",
		                    VendorContactTypeID: 1043
		                };
		                //for (var i = 0; i < vm.addresses.length; i++) {
		                //    if (vm.addresses[i].VendorContactType.Name == "VENDOR_OFFICE_TYPE_MAIN") {
		                //        UIControlService.msg_growl("error", "Kantor pusat sudah diisi"); return;
		                //        centerOffice += 1;
		                //    }
		                //}
		                //if (centerOffice <= 0) {
		                    vm.tmp = {
		                        id: vm.addresses.length + 1,
		                        AddressID: 0,
		                        type: vm.addressType.Value,
		                        address: vm.address1,
		                        countryID: vm.selectedCountry.CountryID,
		                        countryName: vm.selectedCountry.Name,
		                        stateID: vm.selectedState.StateID,
		                        stateName: vm.selectedState.Name,
		                        cityID: vm.selectedCity.CityID,
		                        cityName: vm.selectedCity.Name,
		                        districtID: vm.selectedDistrict.DistrictID,
		                        districtName: vm.selectedDistrict.Name,
		                        postalCode: vm.postalcode,
		                        VendorContactType: VendorContactType,
		                        ContactID: 0
		                    }
		                    vm.addresses.push(vm.tmp);
		                    vm.change = true;
		                    vm.selectedCountry = [];
		                    vm.selectedState = [];
		                    vm.listRegion.Name = "";
		                    vm.address1 = "";
		                    //vm.selectedAssociation = [];
		                    vm.postalcode = "";
		                    vm.addressType = "";
		                //}
		            }
		        }
			} 
			else {
		        if (vm.addressType.Value == "VENDOR_OFFICE_TYPE_MAIN") {
		            if (!vm.addressType) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_OFFICETYPE"); 
						return; 
					}
		            else if (!vm.address1) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_ADDRESS"); 
						return; 
					}
		            else if (!vm.selectedCountry || vm.selectedCountry.length == 0) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_COUNTRY"); 
						return; 
					}
		            else if (!vm.selectedState) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_PROVINCE"); 
						return; 
					}
		            //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		            // else if (!vm.selectedAssociation) { 
					// 	UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_ASSOCIATION"); 
					// 	return; 
					// }
		            else {
		                var centerOffice = 0;
		                var id = vm.addresses.length + 1;
		                var VendorContactType = {
		                    Name: "VENDOR_OFFICE_TYPE_MAIN",
		                    RefID: 1042,
		                    Type: "VENDOR_OFFICE_TYPE",
		                    Value: "VENDOR_OFFICE_TYPE_MAIN",
		                    VendorContactTypeID: 1042
		                };
		                for (var i = 0; i < vm.addresses.length; i++) {
		                    if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                        UIControlService.msg_growl("error", "OFFICE_MESSAGE.ALREADY_OFFICETYPE");
		                        vm.addressType = "";
		                        vm.countryIDInformasi = undefined;
		                        vm.stateIDInformasi = undefined;
		                        vm.cityIDInformasi = undefined;
		                        vm.districtIDInformasi = undefined;
		                        vm.postalcode = "";
		                        vm.AsosiasiID = undefined;
		                        vm.address1 = "";
		                        vm.listRegion.Name = "";
		                        return;
		                        centerOffice += 1;
		                    }
		                }
		                if (centerOffice <= 0) {
		                    vm.tmp = {
		                        id: vm.addresses.length + 1,
		                        AddressID: 0,
		                        type: vm.addressType.Value,
		                        address: vm.address1,
		                        countryID: vm.selectedCountry.CountryID,
		                        countryName: vm.selectedCountry.Name,
		                        stateID: vm.selectedState.StateID,
		                        stateName: vm.selectedState.Name,
		                        //cityID: vm.selectedCity.CityID,
		                        //cityName: vm.selectedCity.Name,
		                        //districtID: vm.selectedDistrict.DistrictID,
		                        //districtName: vm.selectedDistrict.Name,
		                        postalCode: vm.postalcode,
		                        VendorContactType: VendorContactType,
		                        ContactID: 0,
		                        AssociationID: vm.selectedAssociation.AssosiationID,
		                        AssociationName: vm.selectedAssociation.AssosiationName
		                    }
		                    vm.addresses.push(vm.tmp);
		                    vm.change = true;
		                    vm.selectedCountry = [];
		                    vm.selectedState = [];
		                    vm.listRegion.Name = "";
		                    vm.address1 = "";
		                    //vm.selectedAssociation = [];
		                    vm.postalcode = "";
		                    vm.addressType = "";
		                    vm.asosiasiID = "";
		                }
		            }
				} 
				else if(vm.addressType.Value == "VENDOR_OFFICE_TYPE_BRANCH"){
		            if (!vm.addressType) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_OFFICETYPE"); 
						return; 
					}
		            else if (!vm.address1) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_ADDRESS"); 
						return; 
					}
		            else if (!vm.selectedCountry || vm.selectedCountry.length == 0) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_COUNTRY"); 
						return; 
					}
		            else if (!vm.selectedState) { 
						UIControlService.msg_growl("error", "OFFICE_MESSAGE.NULL_PROVINCE"); 
						return; 
					}
		            //else if (!vm.postalcode) { UIControlService.msg_growl("error", "Kode pos harus diisi"); return; }
		            else {
		                var centerOffice = 0;
		                var id = vm.addresses.length + 1;
		                var VendorContactType = {
		                    Name: "VENDOR_OFFICE_TYPE_BRANCH",
		                    RefID: 1043,
		                    Type: "VENDOR_OFFICE_TYPE",
		                    Value: "VENDOR_OFFICE_TYPE_BRANCH",
		                    VendorContactTypeID: 1043
		                };
		                //for (var i = 0; i < vm.addresses.length; i++) {
		                //    if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                //        UIControlService.msg_growl("error", "Kantor pusat sudah diisi"); return;
		                //        centerOffice += 1;
		                //    }
		                //}
		                //if (centerOffice <= 0) {
		                    vm.tmp = {
		                        id: vm.addresses.length + 1,
		                        AddressID: 0,
		                        type: vm.addressType.Value,
		                        address: vm.address1,
		                        countryID: vm.selectedCountry.CountryID,
		                        countryName: vm.selectedCountry.Name,
		                        stateID: vm.selectedState.StateID,
		                        stateName: vm.selectedState.Name,
		                        //cityID: vm.selectedCity.CityID,
		                        //cityName: vm.selectedCity.Name,
		                        //districtID: vm.selectedDistrict.DistrictID,
		                        //districtName: vm.selectedDistrict.Name,
		                        postalCode: vm.postalcode,
		                        VendorContactType: VendorContactType,
		                        ContactID: 0
		                    }
		                    vm.addresses.push(vm.tmp);
		                    vm.change = true;
		                    vm.selectedCountry = [];
		                    vm.selectedState = [];
		                    vm.listRegion.Name = "";
		                    vm.address1 = "";
		                    //vm.selectedAssociation = [];
		                    vm.postalcode = "";
		                    vm.addressType = "";
		                //}
		            }
		        }
		    }
		    vm.countryIDInformasi = "";
		    vm.stateIDInformasi = "";
		    vm.cityIDInformasi = "";
		    vm.districtIDInformasi = "";
		}

		vm.removeAddress = removeAddress;
		function removeAddress(data) {
		    for (var i = 0; i < vm.addresses.length; i++) {
		        if (vm.addresses[i].id == data.id) {
		            if (vm.addresses[i].type == "VENDOR_OFFICE_TYPE_MAIN") {
		                vm.AddressRemovedMain.push(vm.addresses[i]);
		            } else {
		                vm.AddressRemovedBranch.push(vm.addresses[i]);
		            }
		            vm.change = true;
		            vm.addresses.splice(i, 1);
		        }
		    }
		}

		vm.loadFilePrefix = loadFilePrefix;
		function loadFilePrefix() {
		    //UIControlService.loadLoading("LOADERS.LOADING_PREFIX");
		    VendorRegistrationService.getUploadPrefix(
                function (response) {
                    if (response.status == 200) {
                        var prefixes = response.data;
                        cekPrakualifikasiVendor();
                        vm.prefixes = {};
                        for (var i = 0; i < prefixes.length; i++) {
                            vm.prefixes[prefixes[i].Name] = prefixes[i];
                        }
                    } else {
                        UIControlService.unloadLoading();
                    }
                    
                    //UIControlService.unloadLoading();
                }, handleRequestError);
		}

		vm.loadPhoneCodes = loadPhoneCodes;
		function loadPhoneCodes(data) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			VendorRegistrationService.getCountries(
              function (response) {
              	vm.phoneCodeList = response.data;
              	for (var i = 0; i < vm.phoneCodeList.length; i++) {
              	    vm.phoneCodeList[i].namePhonePrefix = vm.phoneCodeList[i].Name + " (" + vm.phoneCodeList[i].PhonePrefix + ")";
              		if (vm.phoneCodeList[i].PhonePrefix === data) {
              			vm.phoneCode = vm.phoneCodeList[i];
              		}
              	}
              	UIControlService.unloadLoading();
              }, function (err) {
              	//$.growl.error({ message: "Gagal Akses API >" + err });
              	UIControlService.unloadLoading();
              });
		}

		vm.changePhone = changePhone;
		function changePhone(phone) {
		    vm.change = true;
		    for (var i = 0; i < vm.phoneCodeList.length; i++) {
		        if (vm.phoneCodeList[i].PhonePrefix === phone) {
		            vm.phoneCode = vm.phoneCodeList[i];
		        }
		    }
		}

		vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
		function cekPrakualifikasiVendor() {
		    DataAdministrasiService.cekPrakualifikasiVendor(function (reply) {
		        //UIControlService.loadLoadingModal();
		        if (reply.status === 200) {
		            vm.pqWarning = reply.data;
		            loadOfficeType();
		            //console.info("isregistered" + vm.pqWarning);
		            //UIControlService.unloadLoadingModal();
		        } else {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		    });
		}

		function loadCheckCR() {
			//UIControlService.loadLoading("MESSAGE.LOADING");
			DataAdministrasiService.getCRbyVendor({ CRName: 'OC_ADM_LEGAL' }, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.CR = reply.data;
				    loadAssociation(vm.administrasi);
				    //console.info("cr" + vm.CR);
					if (reply.data[0] === true) {
						vm.isApprovedCRAdm = true;
					}
					else {
					    vm.isApprovedCRAdm = false;
					}

					if (vm.vendorReviewData != null && reply.data[1] != "Approved_CR") {
					    if (vm.revisiVendor) {
					        vm.isApprovedCRAdm = true;
					    } else {
					        vm.isApprovedCRAdm = false;
					    }
					}

					
				}
			}, function (err) {
				//UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
			DataAdministrasiService.getCRbyVendor({ CRName: 'OC_VENDORBUSINESSFIELD' }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.CRBF = reply.data;
					if (reply.data === true) {
						vm.isApprovedCRBF = true;
					}
					else {
						
						vm.isApprovedCRBF = false;
						
						if (vm.vendorReviewData != null) {
						    if (vm.revisiVendor) {
						        vm.isApprovedCRBF = true;
						    } else {
						        vm.isApprovedCRBF = false;
						    }
						}
					}

					
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadVerifiedVendor = loadVerifiedVendor;
		function loadVerifiedVendor() {
			VerifiedSendService.selectVerifikasi(function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.verified = reply.data;
				    loadFilePrefix();
					
					//vm.isVerified = vm.verified.Isverified;
					//loadCheckCR();
					vm.cekTemporary = vm.verified.IsTemporary;
					//console.info("verified" + JSON.stringify(vm.verified));
					if (vm.verified.Isverified == null) {
					    if (vm.verified.VerifiedSendDate != null) {
					        vm.isVerified = false;
						}
						else{
							vm.isVerified = false;
						}
					}
					else {
					    if (vm.verified.Isverified == true) {
					        vm.isVerified = true;
					    }
					}
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			DataAdministrasiService.select({
				Offset: offset,
				Limit: vm.pageSize,
				Keyword: vm.Username
			}, function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.list = {};
					vm.administrasi = reply.data[0];
					if (vm.administrasi) {
					    vm.administrasiDate.StartDate = vm.administrasi.FoundedDate;
					    vm.Username = vm.administrasi.user.Username;
					    vm.PKPNumber = vm.administrasi.PKPNumber;
					    convertToDate();
					    loadCurrency();
					    //loadContactVendor(vm.administrasi);
					}
					
					
					//if (vm.listAssociation) {
					    
					//}
					
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}
		vm.loadContactVendor = loadContactVendor;
		function loadContactVendor(dataAdmin) {
		    DataAdministrasiService.selectcontact({
		        VendorID: dataAdmin.VendorID
		    }, function (reply) {
		        //UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.listPersonal = [];
		            vm.contact = reply.data;
		            loadTypeVendor(vm.administrasi);
		            loadCountry();
		            vm.flagPrimary = 0;
		            //for (var i = 0; i < vm.contact.length; i++) {
		            //    if (vm.contact[i].IsPrimary === 2) { vm.flagPrimary = 1; }
		            //    if (i == (vm.contact.length - 1)) {
		            //        if (vm.flagPrimary == 0) {
		            //            loadCountryAlternatif();
		            //        }
		            //    }
		            //}
		            for (var i = 0; i < vm.contact.length; i++) {
		                console.info(vm.contact.length);
		                if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
		                    vm.VendorContactTypeCompany = vm.contact[i].VendorContactType;
		                    vm.vendorCategoryID = vm.contact[i].Contact.VendorCategoryID;
		                    if (vm.contact[i].Contact.Fax !== null) {
		                        vm.fax = vm.contact[i].Contact.Fax;
		                    }
		                    //console.info(vm.contact[i].Contact);
		                    vm.ContactID = vm.contact[i].Contact.ContactID;
		                    if (vm.contact[i].Contact.Phone != null) {
		                        //console.info("phone" + vm.contact[i].Contact.Phone);
		                        vm.phone = vm.contact[i].Contact.Phone.split(' ');
		                        vm.Phone = (vm.phone[1]);
		                        vm.AreaCode = vm.contact[i].Contact.AreaCode ? vm.contact[i].Contact.AreaCode : null;
		                        if (vm.Phone == undefined) {
		                            vm.Phone = vm.contact[i].Contact.Phone;
		                        }
		                        vm.phone = vm.phone[0].split(')');
		                        vm.phone = vm.phone[0].split('(');
		                        loadPhoneCodes(vm.phone[1]);
		                    }
		                    else loadPhoneCodes(undefined);
		                    vm.EmailOri = vm.contact[i].Contact.Email;
		                    vm.Email = vm.contact[i].Contact.Email;
		                    vm.Website = vm.contact[i].Contact.Website;
		                    vm.addressIdComp = vm.contact[i].Contact.AddressID;
		                    loadCountryAdmin(vm.contact[i].Contact.Address.State);
		                    vm.countryID = vm.contact[i].Contact.Address.State.Country.CountryID;
		                    vm.stateID = vm.contact[i].Contact.Address.State.StateID;
		                    vm.cityID = vm.contact[i].Contact.Address.City ? vm.contact[i].Contact.Address.City.CityID : "";
		                    vm.districtID = vm.contact[i].Contact.Address.Distric ? vm.contact[i].Contact.Address.Distric.DistrictID : "";
		                    vm.CityCompany = vm.contact[i].Contact.Address.City ? vm.contact[i].Contact.Address.City : [];
		                    vm.DistrictCompany = vm.contact[i].Contact.Address.Distric ? vm.contact[i].Contact.Address.Distric : [];
		                    vm.Region = vm.contact[i].Contact.Address.State.Country.Continent.Name;
		                    if (vm.contact[i].IsPrimary === 1 || vm.contact[i].IsPrimary === null) {
		                        vm.CountryCode = vm.contact[i].Contact.Address.State.Country.Code;
		                        loadCurrencies(dataAdmin);
		                        if (vm.CountryCode == 'IDN') {
		                            loadBusinessEntity(dataAdmin);
		                        }
		                    }
		                    //console.info("isprim" + JSON.stringify(vm.contact[i].IsPrimary));
		                    vm.contactpersonal = vm.contact[i];
		                } else if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {
		                    vm.listPersonal.push(vm.contact[i]);
		                    vm.VendorContactTypePers = vm.contact[i].VendorContactType;
		                }
		                    //else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === null) {
		                else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN') {
		                    //if (vm.contact[i].Contact.Address.AddressInfo != undefined) {
		                    vm.arr = [];
		                    vm.arr.push(vm.contact[i]);
		                    //if (vm.listAssociation) {
		                    //    if (vm.arr[0].Vendor.AssociationID) {
		                    //        for (var i = 0; i < vm.listAssociation.length; i++) {
		                    //            if (vm.arr[0].Vendor.AssociationID == vm.listAssociation[i].AssociationID) {
		                    //                var assID = vm.listAssociation[i].AssosiationID;
		                    //                var assName = vm.listAssociation[i].AssosiationName;
		                    //                vm.selectedAssociation = vm.listAssociation[i];
		                    //            }
		                    //        }
		                    //    }
		                    //}

		                    if (vm.arr[0].Contact.Address.State.Country.Code == 'IDN') {
		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            AddressID: vm.arr[0].Contact.Address.AddressID,
		                            type: vm.arr[0].VendorContactType.Name,
		                            address: vm.arr[0].Contact.Address.AddressInfo,
		                            countryID: vm.arr[0].Contact.Address.State.Country.CountryID,
		                            countryName: vm.arr[0].Contact.Address.State.Country.Name,
		                            stateID: vm.arr[0].Contact.Address.State.StateID,
		                            stateName: vm.arr[0].Contact.Address.State.Name,
		                            cityID: vm.arr[0].Contact.Address.City.CityID,
		                            cityName: vm.arr[0].Contact.Address.City.Name,
		                            districtID: vm.arr[0].Contact.Address.Distric.DistrictID,
		                            districtName: vm.arr[0].Contact.Address.Distric.Name,
		                            postalCode: vm.arr[0].Contact.Address.PostalCode,
		                            VendorContactType: vm.arr[0].VendorContactType,
		                            ContactID: vm.arr[0].Contact.ContactID,
		                            AssociationID: vm.arr[0].Vendor.AssociationDetail ? vm.arr[0].Vendor.AssociationDetail.AssosiationID : "",
		                            AssociationName: vm.arr[0].Vendor.AssociationDetail ? vm.arr[0].Vendor.AssociationDetail.AssosiationName : ""
		                        }
		                    }
		                    else if (vm.arr[0].Contact.Address.State.Country.Code != 'IDN') {
		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            AddressID: vm.arr[0].Contact.Address.AddressID,
		                            type: vm.arr[0].VendorContactType.Name,
		                            address: vm.arr[0].Contact.Address.AddressInfo,
		                            countryID: vm.arr[0].Contact.Address.State.Country.CountryID,
		                            countryName: vm.arr[0].Contact.Address.State.Country.Name,
		                            stateID: vm.arr[0].Contact.Address.State.StateID,
		                            stateName: vm.arr[0].Contact.Address.State.Name,
		                            cityID: vm.arr[0].Contact.Address.CityID,
		                            districtID: vm.arr[0].Contact.Address.DistrictID,
		                            postalCode: vm.arr[0].Contact.Address.PostalCode,
		                            VendorContactType: vm.arr[0].VendorContactType,
		                            ContactID: vm.arr[0].Contact.ContactID,
		                            AssociationID: vm.arr[0].Vendor.AssociationDetail ? vm.arr[0].Vendor.AssociationDetail.AssosiationID : "",
		                            AssociationName: vm.arr[0].Vendor.AssociationDetail ? vm.arr[0].Vendor.AssociationDetail.AssosiationName : ""
		                        }
		                    }

		                    vm.addresses.push(vm.tmp);
		                    //}
		                    //vm.addressFlag = 1;
		                    vm.ContactOfficeId = vm.arr[0].ContactID;
		                    //vm.ContactName = "Kantor Pusat";
		                    vm.Name = vm.arr[0].Contact.Name;
		                    //vm.AddressId = vm.contact[i].Contact.AddressID;
		                    //vm.VendorContactType = vm.contact[i].VendorContactType;

		                    //vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
		                    //vm.cekAddress = vm.contact[i].Contact.Address.AddressInfo;
		                    //vm.postalcode = vm.contact[i].Contact.Address.PostalCode;
		                    //vm.cekPostCode = vm.postalcode;
		                    loadCountry(vm.arr[0].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                    //vm.selectedState1 = vm.contact[i].Contact.Address.State;
		                    //if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                    //	vm.selectedCity1 = vm.contact[i].Contact.Address.City;
		                    //	vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

		                    //}
		                }
		                    //else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === null) {
		                else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH') {
		                    vm.arr = [];
		                    vm.arr.push(vm.contact[i]);
		                    //vm.arr.push(vm.contact[i]);
		                    //if (vm.listAssociation) {
		                    //    if (vm.arr[0].Vendor.AssociationID) {
		                    //        for (var i = 0; i < vm.listAssociation.length; i++) {
		                    //            if (vm.arr[0].Vendor.AssociationID == vm.listAssociation[i].AssociationID) {
		                    //                var assID = vm.listAssociation[i].AssosiationID;
		                    //                var assName = vm.listAssociation[i].AssosiationName;
		                    //                vm.selectedAssociation = vm.listAssociation[i];
		                    //            }
		                    //        }
		                    //    }
		                    //}

		                    if (vm.arr[0].Contact.Address.State.Country.Code == 'IDN') {
		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            type: vm.arr[0].VendorContactType.Name,
		                            AddressID: vm.arr[0].Contact.Address.AddressID,
		                            address: vm.arr[0].Contact.Address.AddressInfo,
		                            countryID: vm.arr[0].Contact.Address.State.Country.CountryID,
		                            countryName: vm.arr[0].Contact.Address.State.Country.Name,
		                            stateID: vm.arr[0].Contact.Address.State.StateID,
		                            stateName: vm.arr[0].Contact.Address.State.Name,
		                            cityID: vm.arr[0].Contact.Address.City.CityID,
		                            cityName: vm.arr[0].Contact.Address.City.Name,
		                            districtID: vm.arr[0].Contact.Address.Distric.DistrictID,
		                            districtName: vm.arr[0].Contact.Address.Distric.Name,
		                            ContactID: vm.arr[0].Contact.ContactID,
		                            //AssociationID: vm.arr[0].Vendor.AssociationDetail.AssosiationID,
		                            //AssociationName: vm.arr[0].Vendor.AssociationDetail.AssosiationName,
		                            postalCode: vm.arr[0].Contact.Address.PostalCode
		                        }
		                    }
		                    else if (vm.arr[0].Contact.Address.State.Country.Code != 'IDN') {
		                        vm.tmp = {
		                            id: vm.addresses.length + 1,
		                            type: vm.arr[0].VendorContactType.Name,
		                            AddressID: vm.arr[0].Contact.Address.AddressID,
		                            address: vm.arr[0].Contact.Address.AddressInfo,
		                            countryID: vm.arr[0].Contact.Address.State.Country.CountryID,
		                            countryName: vm.arr[0].Contact.Address.State.Country.Name,
		                            stateID: vm.arr[0].Contact.Address.State.StateID,
		                            stateName: vm.arr[0].Contact.Address.State.Name,
		                            cityID: vm.arr[0].Contact.Address.CityID,
		                            districtID: vm.arr[0].Contact.Address.DistrictID,
		                            ContactID: vm.arr[0].Contact.ContactID,
		                            postalCode: vm.arr[0].Contact.Address.PostalCode
		                            //AssociationID: vm.arr[0].Vendor.AssociationDetail.AssosiationID,
		                            //AssociationName: vm.arr[0].Vendor.AssociationDetail.AssosiationName
		                        }
		                    }

		                    vm.addresses.push(vm.tmp);
		                    //console.log("Adresses main")
		                    //console.log(vm.addresses)

		                    //if (vm.addressFlag == 0) {
		                    //	vm.AddressId = vm.contact[i].Contact.AddressID;
		                    vm.ContactOfficeAlterId = vm.arr[0].Contact.ContactID;
		                    vm.VendorContactTypeAlter = vm.arr[0].VendorContactType;
		                    //	vm.ContactName = "Kantor Cabang";
		                    vm.Name = vm.arr[0].Contact.Name;
		                    //	vm.address1 = vm.contact[i].Contact.Address.AddressInfo;
		                    //	vm.cekAddress = vm.contact[i].Contact.Address.AddressInfo;
		                    //	vm.postalcode = vm.contact[i].Contact.Address.PostalCode;
		                    //	vm.cekPostCode = vm.postalcode;
		                    loadCountry(vm.arr[0].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                    //vm.selectedState1 = vm.contact[i].Contact.Address.State;
		                    //	if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                    //		vm.selectedCity1 = vm.contact[i].Contact.Address.City;
		                    //		vm.selectedDistrict1 = vm.contact[i].Contact.Address.Distric;

		                    //	}
		                    //}


		                }
		                //} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && vm.contact[i].IsPrimary === 2) {
		                //	vm.addressAlterFlag = 1;
		                //	vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
		                //	vm.AddressAlterId = vm.contact[i].Contact.AddressID;
		                //	vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
		                //	vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo;
		                //	vm.PostalCodeAlternatif = vm.contact[i].Contact.Address.PostalCode;
		                //	vm.cekPostCode1 = vm.PostalCodeAlternatif;
		                //	//console.info("addresss" + JSON.stringify(vm.contact[i].Contact.Address));
		                //	if (vm.contact[i].Contact.Address.State != null) {
		                //	    loadCountryAlternatif(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                //	    vm.selectedStateAlternatif1 = vm.contact[i].Contact.Address.State;
		                //	    if (vm.contact[i].Contact.Address.State.Country.Code == "IDN") {
		                //	        vm.selectedCityAlternatif1 = vm.contact[i].Contact.Address.City;
		                //	        vm.selectedDistrictAlternatif1 = vm.contact[i].Contact.Address.Distric;

		                //	    }
		                //	}

		                //} else if (vm.contact[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && vm.contact[i].IsPrimary === 2) {
		                //	if (vm.addressAlterFlag == 0) {
		                //		vm.ContactOfficeAlterId = vm.contact[i].Contact.ContactID;
		                //		vm.AddressAlterId = vm.contact[i].Contact.AddressID;
		                //		vm.VendorContactTypeAlter = vm.contact[i].VendorContactType;
		                //		vm.addressInfo = vm.contact[i].Contact.Address.AddressInfo
		                //		vm.PostalCodeAlternatif = vm.contact[i].Contact.Address.PostalCode;
		                //		vm.cekPostCode1 = vm.PostalCodeAlternatif;
		                //		loadCountryAlternatif(vm.contact[i].Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
		                //		vm.selectedStateAlternatif1 = vm.contact[i].Contact.Address.State;
		                //		if (vm.contact[i].Contact.Address.State.Country.Code === "IDN") {
		                //			vm.selectedCityAlternatif1 = vm.contact[i].Contact.Address.City;
		                //			vm.selectedDistrictAlternatif1 = vm.contact[i].Contact.Address.Distric;

		                //		}
		                //	}
		                //}

		            }
		        } else {
		            $.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.CheckAddress = CheckAddress;
		function CheckAddress(flag) {
			if (flag == true) {
				if (vm.address1 !== vm.cekAddress) vm.IsEdit = true;
			}
			else {
				if (vm.addressInfo !== vm.cekAddress1) vm.IsEditAlter = true;
			}
		}
		vm.CheckPostcode = CheckPostcode;
		function CheckPostcode(flag) {
			if (flag == true) {
				if (vm.postalcode !== vm.cekPostCode) vm.IsEdit = true;
			}
			else {
				if (vm.PostalCodeAlternatif !== vm.cekPostCode1) vm.IsEditAlter = true;
			}
		}

		/*isi combo jenis pemasok*/
		vm.selectedSupplier;
		vm.listSupplier;
		function loadSupplier(data) {
			DataAdministrasiService.getSupplier(function (reply) {
				UIControlService.unloadLoading();
				//console.info("PMS:"+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.listSupplier = reply.data.List;
					if (data) {
						for (var i = 0; i < vm.listSupplier.length; i++) {
							if (data.SupplierID === vm.listSupplier[i].RefID) {
								vm.selectedSupplier = vm.listSupplier[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		/*isi combo jenis Vendor*/
		vm.selectedTypeVendor;
		vm.listTypeVendor;
		function loadTypeVendor(data) {
			DataAdministrasiService.getTypeVendor(function (reply) {
				//UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.listTypeVendor = reply.data.List;
					if (data !== undefined) {
						for (var i = 0; i < vm.listTypeVendor.length; i++) {
							if (data.VendorTypeID === vm.listTypeVendor[i].RefID) {
								vm.selectedTypeVendor = vm.listTypeVendor[i];
								changeTypeVendor(vm.administrasi);
								break;
							}
						}
					} else {
					    UIControlService.unloadLoading();
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.listBussinesDetailField = []
		vm.changeTypeVendor = changeTypeVendor;
		function changeTypeVendor(data) {
			if (vm.selectedTypeVendor !== undefined) {
				if (vm.selectedTypeVendor.Value === "VENDOR_TYPE_SERVICE") {
					vm.disablePemasok = true;
					vm.listSupplier = {};
					UIControlService.unloadLoading();
					UIControlService.msg_growl("warning", "MESSAGE.SUPPLIER");
					//console.info("dpm:" + JSON.stringify(vm.disablePemasok));
				}
				if (vm.selectedTypeVendor.Value !== "VENDOR_TYPE_SERVICE") {
					vm.disablePemasok = false;
					if (data) {

						loadSupplier(data);
					}
					else {
					    vm.change = true;
						loadSupplier();
					}
					//console.info("dpm:" + JSON.stringify(vm.disablePemasok));
				}

				vm.GoodsOrService = vm.selectedTypeVendor.RefID;
				loadBusinessField();
				vm.listBussinesDetailField = [];
				vm.listComodity = [];
			}

		}

		vm.loadVendorCommodity = loadVendorCommodity;
		function loadVendorCommodity(data) {
			DataAdministrasiService.SelectVendorCommodity({ VendorID: data }, function (reply) {
				//UIControlService.unloadLoading();
				//console.info("PMS:"+JSON.stringify(reply));
				if (reply.status === 200) {
				    vm.listBussinesDetailField = reply.data;
				    loadContactVendor(vm.administrasi);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadBusinessField = loadBusinessField;
		vm.selectedBusinessField;
		vm.listBusinessField = [];
		function loadBusinessField() {
			DataAdministrasiService.SelectBusinessField({
				GoodsOrService: vm.GoodsOrService
			},
			   function (response) {
			   	if (response.status === 200) {
			   	    vm.listBusinessField = response.data;
			   	    UIControlService.unloadLoading();
			   		//console.info("bfield" + JSON.stringify(vm.listBusinessField));
			   	}
			   	else {
			   		UIControlService.msg_growl("error", "MESSAGE.ERR_GET_BUSSFIELD");
			   		return;
			   	}
			   }, function (err) {
			   	UIControlService.msg_growl("error", "MESSAGE.API");
			   	return;
			   });
		}

		vm.changeBussinesField = changeBussinesField;
		function changeBussinesField() {
			//console.info("bfield" + JSON.stringify(vm.listBusinessField[0].GoodsOrService));
			if (vm.selectedBusinessField === undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD_TYPE");
				return;
			} else {
			    if (vm.selectedBusinessField.Name != "Lain - lain")
			        vm.loadComodity();
			}
		}

		vm.editcontact = editcontact;
		function editcontact(data) {
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/data-administrasi/DetailContact.html',
				controller: 'DetailContactAdministrasiCtrl',
				controllerAs: 'DetailContactAdministrasiCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function (data) {
				//console.info(data);
				vm.listPers = [];
				vm.listPers = vm.listPersonal;
				vm.listPersonal = [];
				for (var i = 0; i < vm.listPers.length; i++) {
					if (vm.listPers[i].ContactID === data.ContactID) {
						var aish = {
							VendorContactType: vm.listPers[i].VendorContactType,
							ContactID: vm.listPers[i].ContactID,
							VendorID: vm.listPers[i].VendorID,
							Contact: {
								ContactID: data.ContactID,
								Name: data.Name,
								Email: data.Email,
								Phone: data.Phone
							},
							IsActive: true,
							IsEdit: true
						}
						vm.listPersonal.push(aish);
					}
					else {
						vm.listPersonal.push(vm.listPers[i]);
					}
				}
			});
		}

		function handleRequestError(response) {
			UIControlService.log(response);
			//UIControlService.handleRequestError(response.data, response.status);
			UIControlService.unloadLoading();
		}
		vm.validateEmail = validateEmail;
		vm.emailFormat = false;
		function validateEmail(email) {
		    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
		        vm.emailFormat = true;
		    } else {
		        vm.emailFormat = false;
		    }
		}

		vm.CheckEmail = CheckEmail;
		function CheckEmail() {
		    UIControlService.loadLoading("Check Email . . .");
		    var count = 0;
		    if (vm.EmailPers !== '') {
		        for (var i = 0; i < vm.allEmail.length; i++) {
		            if (vm.EmailPers == vm.allEmail[i].Contact.Email) {
		                count++;
		                break;
		            }
		        }

		        if (count > 0) {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');

		            vm.NamePers = undefined;
		            vm.PhonePers = undefined;
		            vm.EmailPers = undefined;
		        } else {
		            vm.addContactPers();
		        }


				//var data = {
				//	Keyword: vm.EmailPers
				//};
				//DataAdministrasiService.checkEmail(data,
                //        function (response) {
                //        	vm.EmailAvailable = response.data;
                //        	if (vm.EmailAvailable) {
                //        	    UIControlService.unloadLoading();
                //        	    UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');

                //        	    vm.NamePers = undefined;
                //        	    vm.PhonePers = undefined;
                //        	    vm.EmailPers = undefined;
                //        	}
                //        	else {
                //        	    vm.addContactPers();
                //        	}
                //        }, handleRequestError);
			}
		}


		vm.CheckUsername = CheckUsername;
		function CheckUsername() {
			if (vm.Username !== vm.administrasi.user.Username) {
				DataAdministrasiService.checkUsername({ Keyword: vm.Username },
                        function (response) {
                        	vm.UsernameAvailable = (response.data == false || response.data == 'false');
                        	if (!vm.UsernameAvailable) {
                        		UIControlService.msg_growl('error', 'Username not valid');
                        		vm.Username = vm.administrasi.user.Username;
                        	}
                        }, handleRequestError);
			}
			if (vm.Username === '') {
				vm.Username = vm.administrasi.user.Username;
			}
		}
		vm.addCurrency = addCurrency;
		function addCurrency() {
			vm.flagCurr = false;
			//console.info(vm.Currency);
			for (var i = 0; i < vm.listCurrencies.length; i++) {
				if (vm.Currency.CurrencyID == vm.listCurrencies[i].MstCurrency.CurrencyID && vm.listCurrencies[i].IsActive == true) { vm.flagCurr = true; }
			}
			if (vm.flagCurr == false) {
				vm.listCurrencies.push({
					ID: 0,
					CurrencyID: vm.Currency.CurrencyID,
					VendorID: vm.administrasi.VendorID,
					MstCurrency: vm.Currency,
					IsActive: true
				});
			}
			else {
			    UIControlService.msg_growl("warning", "Mata Uang yang ditambahkan sudah ada");
			    return;

			}
		}


		vm.addContactPers = addContactPers;
		function addContactPers() {

		    vm.listPersonal.push({

		        Contact: {
		            ContactID: 0,
		            Name: vm.NamePers,
		            Phone: vm.PhonePers,
		            Email: vm.EmailPers
		        },
		        IsActive: true
		    });
		    vm.change = true;
		    vm.NamePers = undefined;
		    vm.PhonePers = undefined;
		    vm.EmailPers = undefined;
		    UIControlService.unloadLoading();
		}

		vm.loadCurrency = loadCurrency;
		function loadCurrency() {
			//console.info("ss");
			//UIControlService.loadLoading("LOADERS.LOADING_CURRENCY");
			VendorRegistrationService.getCurrencies(
                function (response) {
                    if (response.status == 200) {
                        vm.currencyList = response.data;
                        loadVerifiedVendor();
                    } else {
                        UIControlService.unloadLoading();
                    }
                	
                },
                handleRequestError);
		}

		vm.loadComodity = loadComodity;
		vm.selectedComodity;
		vm.listComodity = [];
		function loadComodity() {
			//console.info("bidang usaha goodsorservice" + JSON.stringify(vm.selectedBusinessField.GoodsOrService));
			if (vm.selectedBusinessField.GoodsOrService === 3091) {
				UIControlService.msg_growl("success", "MESSAGE.COMMODITY");
				vm.listComodity = [];
			} else {
				DataAdministrasiService.SelectComodity({ ID: vm.selectedBusinessField.ID },
				   function (response) {
				   	//console.info("xx"+JSON.stringify(response));
				   	if (response.status === 200 && response.data.length > 0) {
				   		vm.listComodity = response.data;
				   	} else if (response.status === 200 && response.data.length < 1) {
				   		UIControlService.msg_growl("success", "MESSAGE.COMMODITY");
				   		vm.listComodity = [];
				   	} else {
				   		UIControlService.msg_growl("error", "MESSAGE.COMMODITY_LIST");
				   		return;
				   	}
				   }, function (err) {
				   	UIControlService.msg_growl("error", "MESSAGE.API");
				   	return;
				   });
			}
		}


		vm.addDetailBussinesField = addDetailBussinesField;
		function addDetailBussinesField() {
			if (vm.selectedBusinessField === undefined) {
				UIControlService.msg_growl("warning", "MESSAGE.BUSINESSFIELD");
				return;
			}

			var comID;
			if (vm.selectedComodity === undefined) {
				//UIControlService.msg_growl("warning", "Komoditas Belum di Pilih");
				//return;
				comID = null;
			} else if (!(vm.selectedComodity === undefined)) {
				comID = vm.selectedComodity.ID;
			}
			countDetailBusinessField = [];
			for (var i = 0; i < vm.listBusinessField.length; i++) {
			    if (vm.listBusinessField[i].Name != "Lain - lain")
			        countDetailBusinessField.push(vm.listBusinessField[i]);
			}
			var countDetailBusinessField = vm.listBussinesDetailField.length;
			var addPermission = false; var sameItem = true;
			//console.info("vendortype:" + JSON.stringify(vm.selectedTypeVendor))
			//console.info("countDetail" + JSON.stringify(countDetailBusinessField));
			var dataDetail = {
				VendorID: vm.administrasi.VendorID,
				CommodityID: comID,
				BusinessFieldID: vm.selectedBusinessField.ID,
				Commodity: vm.selectedComodity,
				BusinessField: vm.selectedBusinessField,
				Remark: vm.RemarkBusinessField
			}

			DataAdministrasiService.businessfieldValidation({
			    BusinessFields: vm.listBussinesDetailField,
			    VendorID: vm.administrasi.VendorID,
			    GoodsOrService: vm.selectedTypeVendor.RefID,
                CommodityParameter: dataDetail
			}, function (reply) {
			    UIControlService.unloadLoading();
			    //console.info("PMS:"+JSON.stringify(reply));
			    if (reply.status === 200) {
			        vm.validasi = reply.data;
			        if (vm.validasi == 1) {
			            vm.listBussinesDetailField.push(dataDetail);
			        }
			        else if (vm.validasi == 0) {
			            UIControlService.msg_growl("warning", "Gagal menambahkan bidang usaha atau komoditas.");
			        } else if (vm.validasi == 2) {
			            UIControlService.msg_growl("warning", "Bidang usaha sudah dipilih");
			        }
			        //console.info("validasi" + vm.validasi);
			    }
			}, function (err) {
			    UIControlService.msg_growl("error", "MESSAGE.API");
			    UIControlService.unloadLoading();
			});
            
			//if (addPermission === true && sameItem === true) {
			    //console.info(dataDetail);
			    //vm.listBussinesDetailField.push(dataDetail);
			//}
		    //}
			vm.selectedComodity = undefined;

			vm.selectedComodity = undefined;
			//console.info("listbusinessfield:" + JSON.stringify(vm.listBussinesDetailField));

		}

		vm.deleteRow = deleteRow;
		function deleteRow(index) {
			var idx = index - 1;
			var _length = vm.listBussinesDetailField.length; // panjangSemula
			vm.listBussinesDetailField.splice(idx, 1);
		};
		vm.deleteRowCurr = deleteRowCurr;
		function deleteRowCurr(index, data) {
			if (data.ID != undefined) {
				data.IsActive = false;
				vm.listCurrFalse.push(data);
			}
			var idx = index;
			var _length = vm.listCurrencies.length; // panjangSemula
			vm.listCurrencies.splice(idx, 1);
		};
		vm.deleteRowPers = deleteRowPers;
		function deleteRowPers(index, data) {
			//console.info(data);
			if (data.ContactID != 0) {
				data.IsActive = false;
				vm.listPersFalse.push(data);
			}
			var idx = index;
			var _length = vm.listPersonal.length; // panjangSemula
			vm.listPersonal.splice(idx, 1);
			vm.change = true;
		};

		vm.loadSelectedBusinessEntity = loadSelectedBusinessEntity;
		function loadSelectedBusinessEntity(selectedBE) {
		    vm.change = true;
			vm.selectedBE = vm.selectedBusinessEntity.Description;
			if (vm.selectedBE === "CV") {
				for (var i = 0; i < vm.listTypeVendor.length; i++) {
					if (vm.listTypeVendor[i].Value === "VENDOR_TYPE_GOODS") {
						vm.listTV = vm.listTypeVendor[i];
						i = vm.listTypeVendor.length;
					}
				}
				vm.listTypeVendor = {};
				vm.listTypeVendor[0] = vm.listTV;
				//loadTypeVendor();
				changeTypeVendor();
				//console.info("lteeee" + JSON.stringify(vm.listTypeVendor));
			}
			else if (vm.selectedBE !== "CV") {
				loadTypeVendor();
			}
		}

		function showSelectedTypeVendor(selectedBE) {
			DataAdministrasiService.getTypeVendor(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTypeVendor = reply.data.List;
					for (var i = 0; i < vm.listTypeVendor.length; i++) {
						if (vm.administrasi.VendorTypeID === vm.listTypeVendor[i].VendorTypeID) {
							vm.selectedTypeVendor = vm.listTypeVendor[i];
							changeTypeVendor();
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadBusinessEntity = loadBusinessEntity;
		vm.selectedBusinessEntity;
		vm.listBusinessEntity = [];
		function loadBusinessEntity(data1) {
			DataAdministrasiService.SelectBusinessEntity(function (response) {
				if (response.status === 200) {
					vm.listBusinessEntity = response.data;
					for (var i = 0; i < vm.listBusinessEntity.length; i++) {
					    if (data1.business != null) {
					        if (data1.business.BusinessID === vm.listBusinessEntity[i].BusinessID) {
					            vm.selectedBusinessEntity = vm.listBusinessEntity[i];
					            break;
					        }
					    }
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCurrencies = loadCurrencies;
		vm.selectedCurrencies = [];
		vm.listCurrencies = [];
		var CurrencyID;
		function loadCurrencies(data) {
			DataAdministrasiService.getCurrencies(function (response) {
				if (response.status === 200) {
					vm.listCurrencies = response.data;

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadAssociation = loadAssociation;
		vm.selectedAssociation;
		vm.listAssociation = [];
		function loadAssociation(data) {
		    UIControlService.loadLoading("Loading...");
			DataAdministrasiService.getAssociation({
				Offset: 0,
				Limit: 0,
				Keyword: ""
			},
			function (response) {
				if (response.status === 200) {
				    vm.listAssociation = response.data.List;
				    loadVendorCommodity(vm.administrasi.VendorID);
					for (var i = 0; i < vm.listAssociation.length; i++) {
						if (data.AssociationID === vm.listAssociation[i].AssosiationID) {
							vm.selectedAssociation = vm.listAssociation[i];
							break;
						}
					}
				} else {
				    UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.ERR_GET_COMPANY_TYPE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		function convertAllDateToString() { // TIMEZONE (-)
			if (vm.administrasiDate.StartDate) {
				vm.administrasiDate.StartDate = UIControlService.getStrDate(vm.administrasiDate.StartDate);
			}
		};

		//Supaya muncul di date picker saat awal load
		function convertToDate() {
			if (vm.administrasiDate.StartDate) {
				vm.administrasiDate.StartDate = new Date(Date.parse(vm.administrasiDate.StartDate));
			}
		}

		

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.loadRegion = loadRegion;
		vm.selectedRegion;
		vm.listRegion = [];
		function loadRegion(countryID) {
			DataAdministrasiService.SelectRegion({
				CountryID: countryID
			}, function (response) {
				vm.listRegion = response.data;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCountry = loadCountry;
		vm.selectedCountry;
		vm.listCountry = [];
		function loadCountry(data) {
			DataAdministrasiService.SelectCountry(function (response) {
				vm.listCountry = response.data;
				for (var i = 0; i < vm.listCountry.length; i++) {
					if (data.CountryID === vm.listCountry[i].CountryID) {
						vm.selectedCountry = vm.listCountry[i];
						loadState(data);
						break;
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.defineCountryInformasi = defineCountryInformasi;
		function defineCountryInformasi(id) {
		    vm.change = true;
			vm.countryIDInformasi = id;
			console.log("IDnya "+id)
		    loadState();
		}

		vm.loadState = loadState;
		vm.selectedState;
		vm.listState = [];
		function loadState(data) {
		    
		    if (!data || data == undefined) {
		        for (var i = 0; i < vm.listCountry.length; i++) {
		            if (vm.countryIDInformasi == vm.listCountry[i].CountryID) {
		                data = vm.listCountry[i];
		                vm.selectedCountry = data;
		                break;
		            }
		        }
				vm.selectedState = "";
				vm.selectedCity = "";
				vm.selectedDistrict = "";
				vm.selectedState1 = "";
				vm.stateIDInformasi = "";
				vm.cityIDInformasi = "";
				vm.districtIDInformasi = "";
		    }

		    if (data) {

		        loadRegion(data.CountryID);

		        DataAdministrasiService.SelectState(data.CountryID, function (response) {
		            vm.listState = response.data;
		            for (var i = 0; i < vm.listState.length; i++) {
		                if (vm.stateIDInformasi === vm.listState[i].StateID) {
		                    vm.selectedState = vm.listState[i];
		                    if (vm.selectedState.Country.Code === 'IDN') {
		                        loadCity(vm.selectedState);
		                        break;
		                    }
		                }
		            }


		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            return;
		        });
		    }
		}

		vm.defineStateIDInformasi = defineStateIDInformasi;
		function defineStateIDInformasi(id) {
		    vm.change = true;
		    vm.stateIDInformasi = id;
		    loadCity();
		}

		vm.loadCity = loadCity;
		vm.selectedCity;
		vm.listCity = [];
		function loadCity(data) {
			if (!data) {
			    for (var i = 0; i < vm.listState.length; i++) {
			        if (vm.stateIDInformasi == vm.listState[i].StateID) {
			            data = vm.listState[i];
			            vm.selectedState = data;
			            break;
			        }
			    }
				vm.selectedCity = "";
				vm.selectedCity1 = "";
				vm.selectedDistrict = "";
			}
			DataAdministrasiService.SelectCity(data.StateID, function (response) {
				vm.listCity = response.data;
				for (var i = 0; i < vm.listCity.length; i++) {
					if (vm.cityIDInformasi === vm.listCity[i].CityID) {
						vm.selectedCity = vm.listCity[i];
						if (vm.selectedState.Country.Code === 'IDN') {
							loadDistrict(vm.selectedCity);
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.defineCityInformasi = defineCityInformasi;
		function defineCityInformasi(id) {
		    vm.change = true;
		    vm.cityIDInformasi = id;
		    loadDistrict();
		}

		vm.loadDistrict = loadDistrict;
		vm.selectedDistrict;
		vm.listDistrict = [];
		function loadDistrict(city) {
		    if (!city) {
		        for (var i = 0; i < vm.listCity.length; i++) {
		            if (vm.cityIDInformasi == vm.listCity[i].CityID) {
		                city = vm.listCity[i];
		                vm.selectedCity = city;
		                break;
		            }
		        }
				vm.selectedDistrict = "";
				vm.selectedDistrict1 = "";

			}
		    if (city) {
		        DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
		            vm.listDistrict = response.data;
		            for (var i = 0; i < vm.listDistrict.length; i++) {
		                if (vm.districtIDInformasi === vm.listDistrict[i].DistrictID) {
		                    vm.selectedDistrict = vm.listDistrict[i];
		                    break;
		                }
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            return;
		        });
		    }
		}

		vm.defineDistrictInformasi = defineDistrictInformasi;
		function defineDistrictInformasi(id) {
		    vm.districtIDInformasi = id;
		    for (var i = 0; vm.listDistrict.length; i++) {
		        if (vm.districtIDInformasi == vm.listDistrict[i].DistrictID) {
		            vm.selectedDistrict = vm.listDistrict[i];
		            break;
		        }
		    }
		}

		vm.loadRegionAlternatif = loadRegionAlternatif;
		vm.selectedRegionAlternatif;
		vm.listRegionAlternatif = [];
		function loadRegionAlternatif(countryID) {
			DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
				vm.listRegionAlternatif = response.data;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadRegionAdmin = loadRegionAdmin;
		vm.selectedRegionAdmin;
		vm.listRegionAdmin = [];
		function loadRegionAdmin(countryID) {
			//console.info(countryID);
			DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
				vm.listRegionAdmin = response.data;
				//console.info(vm.listRegionAdmin);
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCountryAlternatif = loadCountryAlternatif;
		vm.selectedCountryAlternatif;
		vm.listCountryAlternatif = [];
		function loadCountryAlternatif(data) {
			DataAdministrasiService.SelectCountry(function (response) {
				vm.listCountryAlternatif = response.data;
				for (var i = 0; i < vm.listCountryAlternatif.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.listCountryAlternatif[i].CountryID) {
							vm.selectedCountryAlternatif = vm.listCountryAlternatif[i];
							loadStateAlternatif(data);
							break;
						}

					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.loadCountryAdmin = loadCountryAdmin;
		vm.selectedCountryAdmin;
		vm.listCountry = [];
		function loadCountryAdmin(data) {
			DataAdministrasiService.SelectCountry(function (response) {
				vm.listCountryAdmin = response.data;
				for (var i = 0; i < vm.listCountryAdmin.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.listCountryAdmin[i].CountryID) {
							vm.selectedCountryAdmin = vm.listCountryAdmin[i];
							loadStateAdmin(data);
							break;
						}
					}
				}
				for (var i = 0; i < vm.listCountryAdmin.length; i++) {
					if (data !== undefined) {
						if(data.CountryID !== 360 && vm.listCountryAdmin[i].CountryID == 360){
							vm.listCountryAdmin.splice(i, 1);
						}
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}


		vm.loadStateAlternatif = loadStateAlternatif;
		vm.selectedStateAlternatif;
		vm.listStateAlternatif = [];
		function loadStateAlternatif(data) {
			if (!data) {
				data = vm.selectedCountryAlternatif;
				vm.selectedStateAlternatif = "";
				vm.selectedCityAlternatif = "";
				vm.selectedDistrictAlternatif = "";
				vm.selectedStateAlternatif1 = "";
			}
			loadRegionAlternatif(data.CountryID);

			DataAdministrasiService.SelectState(data.CountryID, function (response) {
				vm.listStateAlternatif = response.data;
				for (var i = 0; i < vm.listStateAlternatif.length; i++) {
					if (vm.selectedStateAlternatif1 !== "" && vm.selectedStateAlternatif1.StateID === vm.listStateAlternatif[i].StateID) {
						vm.selectedStateAlternatif = vm.listStateAlternatif[i];
						if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
							loadCityAlternatif(vm.selectedStateAlternatif);
							break;
						}
					}
				}


			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.defineCountry = defineCountry;
		function defineCountry(id) {
		    vm.countryID = id;
		    vm.loadStateAdmin();
		}

		vm.loadStateAdmin = loadStateAdmin;
		vm.selectedStateAdmin;
		vm.listStateAdmin = [];
		function loadStateAdmin(data) {
			if (!data || data == undefined) {
			    //data = vm.selectedCountryAdmin;
			    for (var i = 0; i < vm.listCountryAdmin.length; i++) {
			        if (vm.countryID == vm.listCountryAdmin[i].CountryID) {
			            data = vm.listCountryAdmin[i];
			            break;
			        }
			    }
				vm.selectedStateAdmin = "";
				vm.selectedCityAdmin = "";
				vm.selectedDistrictAdmin = "";
				vm.selectedStateAdmin1 = "";
				vm.stateID = "";
				vm.cityID = "";
				vm.districtID = "";
			}
			//else {
			//    for (var i = 0; i < vm.listCountryAdmin.length; i++) {
			//        if (data.CountryID == vm.listCountryAdmin[i].CountryID) {
			//            data = vm.listCountryAdmin[i];
			//        }
			//    }
		    //}
			
			if (data) {
			    loadRegionAdmin(data.CountryID);

			    DataAdministrasiService.SelectState(data.CountryID, function (response) {
			        vm.listStateAdmin = response.data;
			        for (var i = 0; i < vm.listStateAdmin.length; i++) {
			            if (data !== undefined) {
			                if (data.StateID === vm.listStateAdmin[i].StateID) {
			                    vm.selectedStateAdmin = vm.listStateAdmin[i];
			                    if (vm.selectedStateAdmin.Country.Code === 'IDN') {
			                        loadCityAdmin(vm.selectedStateAdmin);
			                        break;
			                    }
			                }
			            }
			        }


			    }, function (err) {
			        UIControlService.msg_growl("error", "MESSAGE.API");
			        return;
			    });
			}
		}

		vm.defineStateID = defineStateID;
		function defineStateID(id) {
		    vm.stateID = id;
		    loadCityAdmin();
		}
		vm.loadCityAdmin = loadCityAdmin;
		vm.selectedCityAdmin;
		vm.listCityAdmin = [];
		function loadCityAdmin(data) {
			if (!data) {
			    for (var i = 0; i < vm.listStateAdmin.length; i++) {
			        if (vm.stateID == vm.listStateAdmin[i].StateID) {
			            data = vm.listStateAdmin[i];
			            vm.selectedStateAdmin = vm.listStateAdmin[i];
			            break;
			        }
			    }
				vm.selectedCityAdmin = "";
				vm.selectedCityAdmin1 = "";
				vm.selectedDistrictAdmin = "";
				vm.cityID = undefined;
				vm.districtID = undefined;
			}
			if (data) {
			    DataAdministrasiService.SelectCity(data.StateID, function (response) {
			        vm.listCityAdmin = response.data;
			        for (var i = 0; i < vm.listCityAdmin.length; i++) {
			            if (data !== undefined) {
			                if (vm.CityCompany) {
			                    if (vm.CityCompany.CityID === vm.listCityAdmin[i].CityID) {
			                        vm.selectedCityAdmin = vm.listCityAdmin[i];
			                        if (vm.selectedStateAdmin.Country.Code === 'IDN') {
			                            loadDistrictAdmin(vm.selectedCityAdmin);
			                            break;
			                        }
			                    }
			                } else {
			                    if (vm.cityID === vm.listCityAdmin[i].CityID) {
			                        vm.selectedCityAdmin = vm.listCityAdmin[i];
			                        if (vm.selectedStateAdmin.Country.Code === 'IDN') {
			                            loadDistrictAdmin(vm.selectedCityAdmin);
			                            break;
			                        }
			                    }
			                }
			            }
			        }
			    }, function (err) {
			        UIControlService.msg_growl("error", "MESSAGE.API");
			        return;
			    });
			}
		}



		vm.loadCityAlternatif = loadCityAlternatif;
		vm.selectedCityAlternatif;
		vm.listCityAlternatif = [];
		function loadCityAlternatif(data) {
			if (!data) {

				data = vm.selectedStateAlternatif;
				vm.selectedCityAlternatif = "";
				vm.selectedCityAlternatif1 = "";
				vm.selectedDistrictAlternatif = "";
			}
			DataAdministrasiService.SelectCity(data.StateID, function (response) {
				vm.listCityAlternatif = response.data;
				for (var i = 0; i < vm.listCityAlternatif.length; i++) {
					if (vm.selectedCityAlternatif1 !== "" && vm.selectedCityAlternatif1.CityID === vm.listCityAlternatif[i].CityID) {
						vm.selectedCityAlternatif = vm.listCityAlternatif[i];
						if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
							loadDistrictAlternatif(vm.selectedCityAlternatif);
							break;
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}

		vm.defineCity = defineCity;
		function defineCity(id) {
		    vm.cityID = id;
		    loadDistrictAdmin();
		}

		vm.loadDistrictAdmin = loadDistrictAdmin;
		vm.selectedDistrictAdmin;
		function loadDistrictAdmin(city) {
		    if (!city) {
		        for (var i = 0; i < vm.listCityAdmin.length; i++) {
		            if (vm.cityID == vm.listCityAdmin[i].CityID) {
		                city = vm.listCityAdmin[i];
		                vm.selectedCityAdmin = city;
		                break;
		            }
		        }
				vm.selectedDistrictAdmin = "";
				vm.selectedDistrictAdmin1 = "";
				vm.districtID = undefined;

			}
			
			if (city) {
			    DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
			        vm.listDistrictAdmin = response.data;
			        for (var i = 0; i < vm.listDistrictAdmin.length; i++) {
			            if (city !== undefined) {
			                if (vm.DistrictCompany) {
			                    if (vm.DistrictCompany.DistrictID === vm.listDistrictAdmin[i].DistrictID) {
			                        vm.selectedDistrictAdmin = vm.listDistrictAdmin[i];
			                        break;
			                    }
			                } else {
			                    if (vm.districtID === vm.listDistrictAdmin[i].DistrictID) {
			                        vm.selectedDistrictAdmin = vm.listDistrictAdmin[i];
			                        break;
			                    }
			                }
			                
			            }
			        }
			    }, function (err) {
			        UIControlService.msg_growl("error", "MESSAGE.API");
			        return;
			    });
			}
		}

		vm.defineDistrict = defineDistrict;
		function defineDistrict(id) {
		    vm.change = true;
		    vm.districtID = id;
		    for (var i = 0; i < vm.listDistrictAdmin.length; i++) {
		        if (vm.districtID == vm.listDistrictAdmin[i].DistrictID) {
		            vm.selectedDistrictAdmin = vm.listDistrictAdmin[i];
		            break;
		        }
		    }
		}




		vm.loadDistrictAlternatif = loadDistrictAlternatif;
		vm.selectedDistrictAlternatif;
		vm.listDistrictAlternatif = [];
		function loadDistrictAlternatif(city) {
			if (!city) {
				city = vm.selectedCityAlternatif;
				vm.selectedDistrictAlternatif = "";
				vm.selectedDistrictAlternatif1 = "";

			}
			DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
				vm.listDistrictAlternatif = response.data;
				for (var i = 0; i < vm.listDistrictAlternatif.length; i++) {
					if (vm.selectedDistrictAlternatif1 !== "" && vm.selectedDistrictAlternatif1.DistrictID === vm.listDistrictAlternatif[i].DistrictID) {
						vm.selectedDistrictAlternatif = vm.listDistrictAlternatif[i];
						break;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				return;
			});
		}


		vm.uploadFile = uploadFile;
		function uploadFile() {
		    if (vm.fileUpload == undefined && vm.administrasi.PKPUrl == "" || vm.fileUploadNPWP == undefined && vm.administrasi.NpwpUrl == "") {
		        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		        return;
		    }

		    if (vm.fileUpload == undefined) {
		        vm.PKPUrl = vm.administrasi.PKPUrl;
		        if (vm.fileUploadNPWP == undefined) {
		            vm.NpwpUrl = vm.administrasi.NpwpUrl ? vm.administrasi.NpwpUrl : "";
		            savedata();
		        } else {
		            if (validateFileType(false, vm.fileUploadNPWP, vm.idUploadConfigs1)) {
                        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		            }
		        }
		    } else {
		        if (validateFileType(true, vm.fileUpload, vm.idUploadConfigs)) {
		            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
		        }
		    }

		    //if (vm.fileUpload === undefined) {
		    //    vm.PKPUrl = vm.administrasi.PKPUrl;
		    //    if (vm.fileUploadNPWP === undefined) {
		    //        vm.NpwpUrl = vm.administrasi.NpwpUrl;
		    //        savedata();
		    //    }
		    //    else {
		    //        if (vm.fileUploadNPWP !== null) {
		    //            upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		    //        }
		    //        else {
		    //            vm.NpwpUrl = vm.administrasi.NpwpUrl;
		    //            savedata();
		    //        }
		    //    }
		    //}
		    //else {
		    //    if (vm.fileUpload !== null) {
		    //        upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
		    //    }
		    //    else {
		    //        vm.PKPUrl = vm.administrasi.PKPUrl;
		    //        if (vm.fileUploadNPWP === undefined) {
		    //            vm.NpwpUrl = vm.administrasi.NpwpUrl;
		    //            savedata();
		    //        }
		    //        else {
		    //            if (vm.fileUploadNPWP !== null) {
		    //                upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
		    //            }
		    //            else {
		    //                vm.NpwpUrl = vm.administrasi.NpwpUrl;
		    //                savedata();
		    //            }
		    //        }
		    //    }
		    //}



			//if (vm.fileUpload === undefined) {
			//    vm.PKPUrl = vm.administrasi.PKPUrl;
			//    if (vm.fileUploadNPWP === undefined) {
			//        vm.NpwpUrl = vm.administrasi.NpwpUrl;
			//        savedata();
			//    }
			//    else {
			//        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
			//    }
			//} else {
			//	//console.info("masuk");
			//    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			//}
		}

		vm.validateFileType = validateFileType;
		function validateFileType(flag, file, allowedFileTypes) {
		    var valid_size = allowedFileTypes[0].Size;
		    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
		    var selectedFileType = file[0].name;
		    selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('.') + 1);


		    var allowedFileType = 0;
		    var allowedFileSize = 0;
		    for (var i = 0; i < allowedFileTypes.length; i++) {
		        if (allowedFileTypes[i].Name == selectedFileType) {
		            allowedFileType = 1;
		        }
		    }

		    if (allowedFileType == 0) {
		        flag ? (UIControlService.msg_growl("error", "MESSAGE.ERR_INVALID_FILETYPE_PKP"), vm.fileUpload = "") : (UIControlService.msg_growl("error", "MESSAGE.ERR_INVALID_FILETYPE_NPWP"), vm.fileUploadNPWP = "");
		        return false;
		    }

		    if (size_file > valid_size) {
		        UIControlService.unloadLoadingModal();
		        flag ? (UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE_PKP"), vm.fileUpload = "") : (UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE_NPWP"), vm.fileUploadNPWP = "");
		        return false;
		    }
		    else {
		        allowedFileSize = 1;
		    }
			
		    if (allowedFileType == 1 && allowedFileSize == 1) {
		        return true;
		    }
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
			//console.info("masuk"+JSON.stringify(vm.administrasi.PKPUrl));
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			//if (vm.administrasi.PKPUrl === null) {
			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileSPPKP(vm.administrasi.VendorID, file, size, filters, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.pathFile = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;
					if (vm.flag == 0) {
						vm.size = Math.floor(s)
					}
					if (vm.flag == 1) {
						vm.size = Math.floor(s / (1024));
					}
					vm.PKPUrl = vm.pathFile;
					if (vm.fileUploadNPWP === undefined) {
					    vm.NpwpUrl = vm.administrasi.NpwpUrl ? vm.administrasi.NpwpUrl : "";
					    savedata();
					}
					else {
					    if (validateFileType(false, vm.fileUploadNPWP, vm.idUploadConfigs1)) {
					        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
					    }
					}
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
			    if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
                    UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
			        UIControlService.unloadLoading();
			    }
			});
			//} end if
		}

		vm.upload1 = upload1;
		function upload1(file, config, filters, callback) {
		    var size = config.Size;
		    var unit = config.SizeUnitName;

		    if (unit == 'SIZE_UNIT_KB') {
		        size *= 1024;
		        vm.flag = 0;
		    }

		    if (unit == 'SIZE_UNIT_MB') {
		        size *= (1024 * 1024);
		        vm.flag = 1;
		    }
		    //console.info(config);
		    UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
		    UploaderService.uploadRegistration(file, vm.administrasi.Npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            var url = response.data.Url;
		            vm.pathFile1 = url;
		            vm.name1 = response.data.FileName;
		            var s = response.data.FileLength;
		            if (vm.flag == 0) {
		                vm.size = Math.floor(s)
		            }
		            if (vm.flag == 1) {
		                vm.size = Math.floor(s / (1024));
		            }
		            vm.NpwpUrl = vm.pathFile1;
		            savedata();
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
		            return;
		        }
		    }, function (response) {
		        if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
		            UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
		            UIControlService.unloadLoading();
		        }
		    });
		}

		function check() {
		    if (vm.Email == undefined || vm.Email == "") {
		        //vm.cek = 1;
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", "MESSAGE.ERR_EMAIL");
		        return 0;
		    } else {
		        UIControlService.loadLoading("Check Email . . .");
		        var count = 0;
		        for (var i = 0; i < vm.allEmail.length; i++) {
		            if (vm.Email == vm.allEmail[i].Contact.Email) {
		                count++;
		                break;
		            }
		        }
		        if (count > 0) {
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');
		            return 0;
		        } else {
		            return 1;
		        }
		    }
		}

		function savedata() {
		    UIControlService.loadLoading("Loading...");
		    vm.cek = 0;
		    //console.info(vm.phoneCode);
		    var cekEmail = check();
		    if (cekEmail == 1) {
		        if (vm.countryID == undefined) {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY");
		            return;
		        }
		        else if (vm.stateID == undefined || vm.stateID == "") {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_STATE");
		            return;
		        }
		        else if (vm.selectedStateAdmin.Country.Code === 'IDN') {
		            if (vm.cityID == undefined) {
		                vm.cek = 1;
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "MESSAGE.ERR_CITY");
		                return;
		            }
		            else if (vm.districtID == undefined) {
		                vm.cek = 1;
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "MESSAGE.ERR_DISTRICT");
		                return;
		            }
		        }
		        if (vm.administrasi.VendorName == undefined || vm.administrasi.VendorName == "") {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_VENDORNAME");
		            return;
		        }
		        else if (vm.Username == undefined || vm.Username == "") {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_USERNAME");
		            return;
		        }
		        else if (vm.administrasiDate.StartDate == undefined) {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_STARTDATE");
		            return;
		        }
		        else if (vm.phoneCode == undefined) {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_PHONECODE");
		            return;
		        }
		        else if (vm.phoneCode !== undefined) {
		            if (vm.Phone == undefined || vm.Phone == "") {
		                vm.cek = 1;
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "MESSAGE.ERR_PHONE");
		                return;
		            }


		        }
		        if (vm.selectedStateAdmin.Country.Code === 'IDN') {
		            if (vm.PKPNumber == undefined) {
		                vm.cek = 1;
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "MESSAGE.ERR_PKPNUMBER");
		                return;
		            }
		            else if (vm.administrasi.PKPUrl == undefined && vm.fileUpload == undefined) {
		                vm.cek = 1;
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "MESSAGE.ERR_PKPUPLOAD");
		                return;
		            }
		            if (vm.AreaCode == undefined || vm.AreaCode == "") {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("error", "MESSAGE.AREACODE");
		                return;
					}
					if(vm.selectedBusinessEntity == undefined){
						vm.cek = 1;
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.ERR_COMPANYTYPE");
						return;
					}
		        }

		        //if (vm.listCurrencies.length === 0) {
		        //    vm.cek = 1;
		        //    UIControlService.msg_growl("error", "MESSAGE.ERR_CURR");
		        //    return;
		        //}

		        //if (vm.listBussinesDetailField.length == 0) {
		        //    vm.cek = 1;
		        //    UIControlService.unloadLoading();
		        //    UIControlService.msg_growl("error", "MESSAGE.ERR_BUSINESSFIELD");
		        //    return;
		        //}
		        if (vm.listPersonal.length == 0) {
		            vm.cek = 1;
		            UIControlService.unloadLoading();
		            UIControlService.msg_growl("error", "MESSAGE.ERR_CP");
		            return;
		        }
		        //if (vm.listBussinesDetailField.length > 6) {
		        //    vm.cek = 1;
		        //    UIControlService.unloadLoading();
		        //    UIControlService.msg_growl("error", "MESSAGE.ERR_LIMITBUSINESSFIELD");
		        //    return;
		        //}
		        //if (vm.selectedCountry == undefined) {
		        //    vm.cek = 1;
		        //    UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY_ADDR");
		        //    return;
		        //}
		        //else if (vm.selectedState == undefined) {
		        //    vm.cek = 1;
		        //    UIControlService.msg_growl("error", "MESSAGE.ERR_STATE_ADDR");
		        //    return;
		        //}
		        //else if (vm.selectedState.Country.Code === 'IDN') {
		        //    if (vm.selectedCity == undefined) {
		        //        vm.cek = 1;
		        //        UIControlService.msg_growl("error", "MESSAGE.ERR_CITY_ADDR");
		        //        return;
		        //    }
		        //    else if (vm.selectedDistrict == undefined) {
		        //        vm.cek = 1;
		        //        UIControlService.msg_growl("error", "MESSAGE.ERR_DISTRICT_ADDR");
		        //        return;
		        //    }
				//}
				if(vm.selectedTypeVendor !== undefined){
					if (vm.selectedTypeVendor.RefID) {
						var RID = vm.selectedTypeVendor.RefID;
						vm.VendorTypeID = RID;
						if (vm.selectedTypeVendor.Name === "VENDOR_TYPE_SERVICE") {
							vm.SupplierID = null;
						}
						else {
							//if (vm.selectedSupplier === undefined) {
							//    vm.cek = 1;
							//    UIControlService.msg_growl("error", "MESSAGE.ERR_SUPPLIER"); 
							//    return;
							//}
							//else 
							//vm.SupplierID = vm.selectedSupplier.RefID;
						}
					}
					else {
						vm.cek = 1;
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.ERR_VENDORTYPE");
						return;
					}
				}
				else{
					vm.cek = 1;
					UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.ERR_VENDORTYPE");
					return;
				}
				
		        if (vm.cek === 0) {
		            addtolist(vm.VendorTypeID, vm.SupplierID);
		        }
		    }
		    
		}

		vm.addtolist = addtolist;
		function addtolist(data1, data2) {
		    vm.vendor = {};
		    vm.listcontact = [];
			if (!vm.selectedCityAdmin && !vm.selectedDistrictAdmin) {
				var addressComp = {
					AddressID: vm.addressIdComp,
					StateID: vm.selectedStateAdmin.StateID
				}
			}
			else {
				var addressComp = {
					AddressID: vm.addressIdComp,
					StateID: vm.selectedStateAdmin.StateID,
					CityID: vm.selectedCityAdmin.CityID,
					DistrictID: vm.selectedDistrictAdmin.DistrictID
				}
			}
			var contactdt = {
				VendorContactType: vm.VendorContactTypeCompany,
				Contact: {
					ContactID: vm.ContactID,
					Email: vm.Email,
					Phone: '(' + vm.phoneCode.PhonePrefix + ') ' + vm.Phone,
					Website: vm.Website,
					Fax: vm.fax,
					Address: addressComp,
                    AreaCode: vm.AreaCode
				}
			}
			
			vm.listcontact.push(contactdt);
		    //if (vm.selectedCity == undefined && vm.selectedDistrict == undefined) {
			vm.address2 = [];
			var count = 0;
			for (var j = 0; j < vm.addresses.length; j++) {
			    if (vm.addresses[j].countryName == "Indonesia") {
			        if (vm.addresses[j].type == "VENDOR_OFFICE_TYPE_MAIN") {
			            count++;
			            vm.ContactCenterOffice = vm.addresses[j].ContactID;
			            vm.address = {
			                AddressID: vm.addresses[j].AddressID,
			                AddressInfo: vm.addresses[j].address,
			                PostalCode: vm.addresses[j].postalCode,
			                StateID: vm.addresses[j].stateID,
			                CityID: vm.addresses[j].cityID,
			                DistrictID: vm.addresses[j].districtID,
                            VendorContactType: vm.addresses[j].VendorContactType
			            }
			        } else {
			            var par = {
			                AddressID: vm.addresses[j].AddressID,
			                AddressInfo: vm.addresses[j].address,
			                PostalCode: vm.addresses[j].postalCode,
			                StateID: vm.addresses[j].stateID,
			                CityID: vm.addresses[j].cityID,
			                DistrictID: vm.addresses[j].districtID,
			                VendorContactType: vm.addresses[j].VendorContactType,
			                ContactID: vm.addresses[j].ContactID
			            }
			            vm.address2.push(par);
			        }
			    } else {
			        if (vm.addresses[j].type == "VENDOR_OFFICE_TYPE_MAIN") {
			            count++;
			            vm.ContactCenterOffice = vm.addresses[j].ContactID;
			            vm.address = {
			                AddressID: vm.addresses[j].AddressID,
			                AddressInfo: vm.addresses[j].address,
			                PostalCode: vm.addresses[j].postalCode,
			                StateID: vm.addresses[j].stateID,
			                CityID: vm.addresses[j].cityID,
			                VendorContactType: vm.addresses[j].VendorContactType
			            }
			        } else {
			            var par = {
			                AddressID: vm.addresses[j].AddressID,
			                AddressInfo: vm.addresses[j].address,
			                PostalCode: vm.addresses[j].postalCode,
			                StateID: vm.addresses[j].stateID,
			                CityID: vm.addresses[j].cityID,
			                VendorContactType: vm.addresses[j].VendorContactType
			            }
			            vm.address2.push(par);
			        }
			    }
			    
			    
			    //} else {

			    
			    //}
			}
			if (count == 0) {
			    count = 0;
			    UIControlService.unloadLoading();
			    UIControlService.msg_growl("error", "Kantor pusat harus diisi");
			    return;
			}
			
			var VendorContactTypeBranch = {
			    Name: "VENDOR_OFFICE_TYPE_BRANCH",
			    RefID: 1043,
			    Type: "VENDOR_OFFICE_TYPE",
			    Value: "VENDOR_OFFICE_TYPE_BRANCH",
			    VendorContactTypeID: 1043
			};
			var VendorContactTypeMain = {
			    Name: "VENDOR_OFFICE_TYPE_MAIN",
			    RefID: 1042,
			    Type: "VENDOR_OFFICE_TYPE",
			    Value: "VENDOR_OFFICE_TYPE_MAIN",
			    VendorContactTypeID: 1042
			};
			if (vm.address.AddressInfo != "") {
			    var contact = {
			        ContactID: vm.ContactCenterOffice,
			        Address: vm.address
			    }
			    var contactdt = {
			        VendorContactType: VendorContactTypeMain,
			        Contact: contact,
			        IsEdit: vm.IsEdit
			    }
			    vm.listcontact.push(contactdt);
			}
            /*
			if (vm.addressInfo != '' && vm.selectedCountryAlternatif == undefined) {
			    UIControlService.msg_growl("error", "MESSAGE.ERR_LIMITBUSINESSFIELD");
			    return;
			}*/
			//if (vm.selectedCountryAlternatif != undefined && vm.addressInfo != ' ') {
				//if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {

					
				//} else {
					
			
		    //}
			if (vm.AddressRemovedMain.length > 0) {
			    if (vm.AddressRemovedMain[0].ContactID != 0) {
			        var contactRemoved = {
			            ContactID: vm.AddressRemovedMain[0].ContactID,
			            Address: vm.AddressRemovedMain[0]
			        }

			        var contactdtRemoved = {
			            VendorContactType: VendorContactTypeMain,
			            Contact: contactRemoved,
			            IsEdit: vm.IsEdit,
			            IsRemoved: true
			        }

			        vm.listcontact.push(contactdtRemoved);
			    }
			}
			vm.AddressRemovedBranch.forEach(function (item) {
			    if (item.ContactID != 0) {
			        var contactRemovedBranch = {
			            ContactID: item.ContactID,
			            Address: item
			        }

			        var contactdtRemovedBranch = {
			            VendorContactType: VendorContactTypeBranch,
			            Contact: contactRemovedBranch,
			            IsEdit: vm.IsEdit,
			            IsRemoved: true
			        }

			        vm.listcontact.push(contactdtRemovedBranch);
			    }
			});

			vm.address2.forEach(function (item) {
			    //if (vm.AddressAlterId == 0) {
			    //    var contact = {
			    //        Name: vm.Name,
			    //        ModifiedBy: vm.administrasi.user.Username,
			    //        Address: item
			    //    }
			    //} else {
			        var contact = {
			            ContactID: item.ContactID,
			            ModifiedBy: vm.administrasi.user.Username,
			            Address: item,
			            Name: vm.Name
			        }
			    //}
			    //if (vm.VendorContactTypeAlter == undefined) {
			    //    var contactdt = {
			    //        VendorContactType: VendorContactTypeBranch,
			    //        Contact: contact,
			    //        IsPrimary: 2,
			    //        IsEdit: vm.IsEditAlter
			    //    }
			    //} else {
			        var contactdt = {
			            VendorContactType: VendorContactTypeBranch,
			            Contact: contact,
			            IsPrimary: 2,
			            IsEdit: vm.IsEditAlter,
                        ContactID: item.ContactID
			        }
			    //}

			    vm.listcontact.push(contactdt);
			});

				vm.listcontact.push(vm.contactpersonal);
			//}
			if (!vm.VendorContactTypePers) {
			    vm.VendorContactTypePers = {
			        Name: 'VENDOR_CONTACT_TYPE_PERSONAL'
			    };
			}
			for (var i = 0; i < vm.listPersonal.length; i++) {
				var contactdt = {
					VendorContactType: vm.VendorContactTypePers,
					Contact: {
						ContactID: vm.listPersonal[i].Contact.ContactID,
						Email: vm.listPersonal[i].Contact.Email,
						Phone: vm.listPersonal[i].Contact.Phone,
						Name: vm.listPersonal[i].Contact.Name
					},
					IsActive: true,
					IsEdit: vm.listPersonal[i].IsEdit
				}
				vm.listcontact.push(contactdt);
			}
			for (var i = 0; i < vm.listPersFalse.length; i++) {
				var contactdt = {
					VendorContactType: vm.VendorContactTypePers,
					Contact: {
						ContactID: vm.listPersFalse[i].Contact.ContactID,
						Email: vm.listPersFalse[i].Contact.Email,
						Phone: vm.listPersFalse[i].Contact.Phone,
						Name: vm.listPersFalse[i].Contact.Name
					},
					IsActive: false
				}
				vm.listcontact.push(contactdt);
			}
			for (var i = 0; i < vm.listCurrFalse.length; i++) {
				vm.listCurrencies.push(vm.listCurrFalse[i]);
			}

			var asoc;
			if (vm.selectedAssociation === undefined) {
				asoc = null;
			} else {
				asoc = vm.selectedAssociation.AssosiationID
			}

			//console.info("coba" + JSON.stringify(vm.selectedSupplier));
			if (vm.selectedSupplier === null || vm.selectedSupplier === undefined) {
				vm.selectedSupplier = {
					RefID: null
				};
			}
			if (vm.CountryCode !== 'IDN') {
				vm.selectedBusinessEntity = {
					BusinessID: null
				};
			}

			for (var a = 0; a < vm.listcontact.length; a++) {
			    if (vm.listcontact[a].VendorContactTypeID == 20) {
			        vm.listcontact.splice(a, 1);
			    }
			}

			vm.vendor = {
			    SupplierID: vm.selectedSupplier.RefID,
                NpwpUrl: vm.NpwpUrl,
				VendorName: vm.administrasi.VendorName,
				VendorID: vm.administrasi.VendorID,
				user: {
					Username: vm.Username
				},
				FoundedDate: UIControlService.getStrDate(vm.administrasiDate.StartDate),
				//BusinessID: vm.selectedBusinessEntity.BusinessID,
				PKPNumber: vm.PKPNumber,
				BusinessID : vm.selectedBusinessEntity.BusinessID,
				PKPUrl: vm.PKPUrl,
				ModifiedBy: vm.administrasi.user.Username,
				AssociationID: asoc,
				Contacts: vm.listcontact,
				commodity: vm.listBussinesDetailField,
				VendorTypeID: data1,
				Currency: vm.listCurrencies,
				VendorCategoryID : vm.administrasi.VendorCategoryID

			}
            
			//console.info(JSON.stringify(vm.vendor));
			DataAdministrasiService.insert(vm.vendor, function (reply) {
				//console.info("reply" + JSON.stringify(reply))
				
				if (reply.status === 200) {
				    UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
				    vm.addresses = [];
				    vm.AddressRemovedBranch = [];
				    vm.AddressRemovedMain = [];
				    UIControlService.unloadLoading();
				    vm.initialize();
				    $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
					//window.location.reload();
				} else {
				    UIControlService.unloadLoading();
					UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
            


		}

		vm.revisiVendorDetail = revisiVendorDetail;
		function revisiVendorDetail() {
		    var data = {
		        data: vm.vendorReviewData,
		        langID: vm.langID
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/rekanan/data-perusahaan/revisi-modal/revisi-modal.html',
		        controller: 'revisiModalController',
		        controllerAs: 'revisiModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {

		    });
		}

		vm.downloadFileCivd = downloadFileCivd;
		function downloadFileCivd(url, name) {
		    UIControlService.loadLoading();

		    VendorRegistrationService.getCIVDData({
		        Keyword: url
		    }, function (response) {
		        var reply = JSON.parse(response.data);
		        if (reply.status === 200) {
		            var byteCharacters = atob(reply.result);

		            var byteNumbers = new Array(byteCharacters.length);
		            for (let i = 0; i < byteCharacters.length; i++) {
		                byteNumbers[i] = byteCharacters.charCodeAt(i);
		            }

		            var byteArray = new Uint8Array(byteNumbers);

		            var blob = new Blob([byteArray], {
		                type: "application/pdf"
		            });

		            FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
		            UIControlService.unloadLoading();

		        } else {
		            UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		    });
		}


	}
})();

