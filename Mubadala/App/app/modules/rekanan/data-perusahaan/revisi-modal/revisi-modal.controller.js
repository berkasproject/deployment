﻿(function () {
    'use strict';

    angular.module("app")
            .controller("revisiModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'UploadFileConfigService',
        'UIControlService', 'item', '$uibModalInstance', 'GlobalConstantService', '$q'];
    function ctrl($http, $translate, $translatePartialLoader, $location, UploadFileConfigService,
        UIControlService, item, $uibModalInstance, GlobalConstantService, $q) {

        var vm = this;
        vm.item = item;
        console.log(item)
        vm.data = item.data;
        vm.langID = item.langID;

        vm.init = init;
        function init() {

        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };

       
    }
})();