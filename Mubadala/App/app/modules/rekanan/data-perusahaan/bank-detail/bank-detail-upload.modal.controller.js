﻿(function () {
    'use strict';

    angular.module("app").controller("BankDetailUploadModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'VerifiedSendService', 'BankDetailService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService','$q'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, VerifiedSendService, BankDetailService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService,$q) {
        var vm = this;
        vm.init = init;
        vm.fileUpload = "";
        vm.IsApprovedCR = item.IsApprovedCR;
        vm.tglSekarang = UIControlService.getDateNow("");
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";

        function init() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            getDocumentForDownload().then(function (res) {
                //get tipe dan max.size file - 1
                vm.documentForDownload = res.data[0];
                vm.documentStatementLetter = res.data[1];
                console.log(res)
                console.log(vm.documentForDownload)
                console.log(vm.documentStatementLetter)
                UploadFileConfigService.getByPageName("PAGE.VENDOR.STATEMENT.LETTER", function (response) {
                    UIControlService.unloadLoadingModal();
                    if (response.status == 200) {
                        vm.list = response.data;
                        vm.idUploadConfigs = vm.list;
                        vm.idFileTypes = UIControlService.generateFilterStrings(vm.idUploadConfigs);
                        vm.idFileSize = vm.idUploadConfigs[0];

                    } else {
                        UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
                        return;
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEFILE");
                    UIControlService.unloadLoadingModal();
                    return;
                });
            })
		    
        }

        function getDocumentForDownload() {
            var defer = $q.defer();
            BankDetailService.getDocumentForDownload({
                DocumentCategory: 4507
            }, function (response) {
                if (response.status == 200) {
                    defer.resolve(response);
                }
            }, function (error) {
                defer.reject(false);
                UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                UIControlService.unloadLoadingModal();
            })
            return defer.promise;
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (!vm.fileUpload || vm.fileUpload == undefined) {
                vm.fileUpload = "";
                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                return;
            }
            if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.tglSekarang);
            } else {
                vm.fileUpload = "";
            }
        }

        //function validateFileType(file, allowedFileTypes) {
        //    var valid_size = allowedFileTypes[0].Size;
        //    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
        //    if (size_file > valid_size) {
        //        UIControlService.unloadLoadingModal();
        //        UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE");
        //        vm.fileUpload = "";
        //        return false;
        //    }

                //if (!file || file.length == 0) {
                //    UIControlService.unloadLoadingModal();
                //    UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
                //    return false;
                //}
        //    else return true;
        //}

        function upload(file, config, filters, dates) {
            //console.info("file" + JSON.stringify(file));
            UIControlService.loadLoadingModal("MESSAGE.LOADING_UPLOAD_FILE");

            var size = config.Size;

            var unit = config.SizeUnitName;
            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
            }

            UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_STATEMENT_LETTER", size, filters, dates,
                function (response) {
                    //console.info("upload:" + JSON.stringify(response.data));
                    if (response.status == 200) {
                        var url = response.data.Url;
                        var fileName = response.data.FileName;
                        saveStatementLetter(url);
                    } else {
                        UIControlService.msg_growl("error", "MSG_ERR_UPLOAD");
                        return;
                    }
                },
                function (response) {
                    UIControlService.msg_growl("error", "MSG_ERR_UPLOAD")
                    UIControlService.unloadLoadingModal();
                });

        }

        function saveStatementLetter(url) {
            BankDetailService.saveStatementLetter({
                StatementLetterURL: url,

            }, function (response) {
                UIControlService.msg_growl("notice", "MSG_SCC_SAVE");
                if (response.status == 200) {
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();

                }
            }, function (error) {
                UIControlService.msg_growl("error", "MSG_ERR_SAVE");
                UIControlService.unloadLoadingModal();
            })
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();