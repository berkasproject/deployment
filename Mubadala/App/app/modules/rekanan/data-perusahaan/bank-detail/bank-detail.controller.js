﻿(function () {
	'use strict';

	angular.module("app").controller("BankDetailCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'BankDetailService', 'UIControlService', 'GlobalConstantService', 'VerifiedSendService', '$filter','$rootScope','$q','AuthService','VendorRegistrationService','FileSaver'];
	/* @ngInject */
	function ctrl($translatePartialLoader, $uibModal, BankDetailService, UIControlService, GlobalConstantService, VerifiedSendService, $filter, $rootScope, $q, AuthService, VendorRegistrationService, FileSaver) {
		var vm = this;

		vm.totalItems = 0;
		vm.currentPage = 0;
		vm.maxSize = 10;
		vm.page_id = 35;
		vm.menuhome = 0;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.userId = 0;
		vm.jLoad = jLoad;
		vm.bank = [];
		vm.ambilUrl;
		vm.ambilUrl2;
		vm.IsApprovedCR = false;
		vm.menuIndex = 9;
		vm.civdID = 0;
		vm.username = localStorage.getItem('username');
		vm.revisiVendor = false;//false
		vm.langID = true;
		//vm.Kata = "";
		vm.VendorID;

		vm.init = init;
		function init() {
		    vm.show = 0;
		    $translatePartialLoader.addPart('bank-detail');
		    if (localStorage.getItem('currLang').toLowerCase() != 'id') {
		        vm.langID = false;
		    }
		    getVendorNation();
		    loadVerifiedVendor();
		    loadVendorVerificationReview();
		}

		function loadVendorVerificationReview() {
		    UIControlService.loadLoading()
		    VerifiedSendService.getVendorVerificationReview({
		        MenuID: 1051
		    }, function (reply) {
		        UIControlService.unloadLoading();

		        if (reply.status == 200) {
		            var data = reply.data;
		            vm.vendorReviewData = data;
		            console.log(data)
		            if (data != null) {
		                if (data.ReviewStatus == 4526) {
		                    vm.revisiVendor = true;
		                } else {
		                    vm.revisiVendor = false;
		                }
		                if (vm.langID) {
		                    vm.revisiName = data.Locale_Id;
		                } else {
		                    vm.revisiName = data.Locale_En;
		                }
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		        return;
		    })
		}

		function getVendorNation() {
		    AuthService.getRoleUserLogin({ Keyword: vm.username }, function (reply) {
		        if (reply.status === 200 && reply.data.List.length > 0) {
		            var data = reply.data.List;
		            if (data[0].RoleName == "APPLICATION.ROLE_VENDOR_INTERNATIONAL") {
		                vm.menuIndex = 7;
		            }
		            console.log(reply.data.List)
		        } else {

		        }
		    }, function (err1) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		    });
		}

		vm.loadVerifiedVendor = loadVerifiedVendor;
		function loadVerifiedVendor() {
			VerifiedSendService.selectVerifikasi(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.verified = reply.data;
					if (vm.verified.VerifiedSendDate === null && vm.verified.VerifiedDate === null) {
						//vm.IsApprovedCR = true;
					}
					//vm.cekTemporary = vm.verified.IsTemporary;
					vm.VendorID = vm.verified.VendorID;
					cekVendor();
				} else {
					$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				$.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		function cekVendor() {
		    BankDetailService.cekVendor(function (reply) {
		        if (reply.status === 200) {
		            var dataVendor = reply.data[0];
		            if (dataVendor) {
		                vm.vendorName = dataVendor.VendorName;
		                vm.vendorCategoryID = dataVendor.VendorCategoryID;
		                vm.civdID = dataVendor.CIVDID;
		                cek();
		                if (vm.vendorCategoryID == 32) {
		                    //cekVendorBankDetail();
		                    vm.isDataCIVD = true;
		                    UIControlService.unloadLoading();
		                }
		                vm.jLoad(1);
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		function cekVendorBankDetail() {
		    BankDetailService.cekVendorBankDetail(function (reply) {
		        if (reply.status === 200) {
		            var countVendorBankDetail = reply.data.Count;
		            if (countVendorBankDetail > 0) {
		                
		            }
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		vm.getDataCIVD = getDataCIVD;
		function getDataCIVD() {
		    UIControlService.loadLoading("Loading...");
		    var defer = $q.defer();
		    var url = "https://apiprovider.civd-migas.com/vendor/rekeningBank?vendorName=" + vm.vendorName;

		    BankDetailService.getDataCIVD({
		        Keyword: url
		    }, function (response) {
		        UIControlService.unloadLoading();
		        var reply = JSON.parse(response.data);
		        if (reply.status === 200) {
		            defer.resolve(reply);
		            vm.bank = reply.result;
		            simpan(vm.bank);
		        } else {
		            defer.reject(false)
		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
		        defer.reject();
		    });

		    return defer.promise;
		}

		vm.loadCurrency = loadCurrency;
		function loadCurrency(symbol) {
		    BankDetailService.getCurrencies(function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.currencyList = reply.data;
		            for (var i = 0; i < vm.currencyList.length; i++) {
		                if (vm.currencyList[i].Symbol == symbol) {
		                    return vm.currencyList[i].CurrencyID;
		                    break;
		                }
		            }
		        } else {
		            $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		function simpan(data) {
		    BankDetailService.saveCIVD({
		        CIVDID: vm.civdID,
                dataCIVD: data
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status === 200) {
		            vm.jLoad(1);
		        } else {
		            UIControlService.msg_growl("error", "ERROR.FAIL_ADD_DATA");
		            return;
		        }
		    }, function (err) {
		        console.info(err);
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoadingModal();
		    });
		}



		function cekCR() {
		    BankDetailService.cekCR(function (reply) {
		        if (reply.status === 200) {
		            vm.isCR = reply.data;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		function getDocumentForDownload() {
		    var defer = $q.defer();
		    BankDetailService.getDocumentForDownload({
		        DocumentCategory: 4507
		    }, function (response) {
		        if (response.status == 200) {
		            console.info(response.data[1]);
		            //if (!(response.data[1])) {
		            //    vm.show = 1;
		            //}
		            defer.resolve(response);
		        }
		    }, function (error) {
		        defer.reject(false);
		        UIControlService.msg_growl("error", "MSG_ERR_SAVE");
		        UIControlService.unloadLoadingModal();
		    })
		    return defer.promise;
		}

		function cek() {
		    BankDetailService.getCRbyVendor({ CRName: 'OC_VENDORBANKDETAIL' }, function (reply) {
		        UIControlService.unloadLoading();
		        console.info("CR:" + JSON.stringify(reply.status));
		        if (reply.status === 200) {
		            cekCR();
		            console.info("CR:" + JSON.stringify(reply.data));
		            if (reply.data[0] === true) {
		                vm.IsApprovedCR = true;
		            } else {
		                vm.IsApprovedCR = false;
		            }

		            if (vm.vendorReviewData != null && reply.data[1] != "Approved_CR") {
		                if (vm.revisiVendor) {
		                    vm.IsApprovedCR = true;
		                } else {
		                    vm.IsApprovedCR = false;
		                }
		            }
		            
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			getDocumentForDownload().then(function (res) {
			    vm.currentPage = current;
			    var offset = (current * 10) - 10;
			    BankDetailService.Select({ VendorID: vm.VendorID }, function (reply) {
			        UIControlService.unloadLoading();
			        if (reply.status === 200) {
			            var data = reply.data;
			            vm.bank = data.List;
			            if ($rootScope.menus != undefined) {
			                if (vm.bank.length == 0 || (res.data[1] == "" || res.data[1] == null)) {
			                    $rootScope.menus[vm.menuIndex].IsChecked = '';
			                    vm.show = 1;
			                } else {
			                    $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
			                }
			            }
			        } else {
			            $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
			            UIControlService.unloadLoading();
			        }
			    }, function (err) {
			        console.info("error:" + JSON.stringify(err));
			        //$.growl.error({ message: "Gagal Akses API >" + err });
			        UIControlService.unloadLoading();
			    });
			})
		}

		vm.upload = upload;
		function upload() {
		    var data = { IsApprovedCR: vm.IsApprovedCR };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/rekanan/data-perusahaan/bank-detail/bank-detail-upload.modal.html',
		        controller: "BankDetailUploadModalCtrl",
		        controllerAs: "bankDetailUploadModalCtrl",
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {
		        // window.location.reload();
		        init();
		    });
		}

		vm.tambah = tambah;
		function tambah(data) {
			var data = {
				act: 1,
				item: data,
                vendorCategoryID: vm.vendorCategoryID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/bank-detail/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				// window.location.reload();
				init();
			});
		}

		vm.edit = edit;
		function edit(data) {
			var data = {
				act: 0,
				item: data,
				vendorCategoryID: vm.vendorCategoryID
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/data-perusahaan/bank-detail/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				jLoad(1);
			});
		}

		vm.remove = remove;
		function remove(doc) {
		    bootbox.confirm({
		        message: '<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>',
		        buttons: {
		            cancel: {
		                label: '<i class="fa fa-close"></i> ' + $filter('translate')('Batal')
		            },
		            confirm: {
		                label: '<i class="fa fa-save"></i> OK'
		            }
		        },
		        callback: function (result) {
		            if (result) {
		                //UIControlService.loadLoading(loadmsg);
		                BankDetailService.remove({
		                    ID: doc.ID
		                }, function (reply2) {
		                    UIControlService.unloadLoading();
		                    if (reply2.status === 200) {
		                        UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_DELETE');
		                        // window.location.reload();
		                        vm.init();
		                    } else {
		                        UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
		                    }
		                }, function (error) {
		                    UIControlService.unloadLoading();
		                    UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
		                });
		            }
		        }
		    });
		};

		vm.revisiVendorDetail = revisiVendorDetail;
		function revisiVendorDetail() {
		    var data = {
		        data: vm.vendorReviewData,
		        langID: vm.langID
		    }
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/rekanan/data-perusahaan/revisi-modal/revisi-modal.html',
		        controller: 'revisiModalController',
		        controllerAs: 'revisiModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function () {

		    });
		}

		vm.downloadFileCivd = downloadFileCivd;
		function downloadFileCivd(url, name) {
		    UIControlService.loadLoading();

		    VendorRegistrationService.getCIVDData({
		        Keyword: url
		    }, function (response) {
		        var reply = JSON.parse(response.data);
		        if (reply.status === 200) {
		            var byteCharacters = atob(reply.result);

		            var byteNumbers = new Array(byteCharacters.length);
		            for (let i = 0; i < byteCharacters.length; i++) {
		                byteNumbers[i] = byteCharacters.charCodeAt(i);
		            }

		            var byteArray = new Uint8Array(byteNumbers);

		            var blob = new Blob([byteArray], {
		                type: "application/pdf"
		            });

		            FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
		            UIControlService.unloadLoading();

		        } else {
		            UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		        }
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		    });
		}
	}
})();