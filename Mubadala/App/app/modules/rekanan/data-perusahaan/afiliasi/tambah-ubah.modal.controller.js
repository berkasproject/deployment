﻿(function () {
    'use strict';

    angular.module("app").controller("AfiliasiModalCtrl", ctrl);

    ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'VerifiedSendService', 'AfiliasiService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'UploaderService'];
    /* @ngInject */
    function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, VerifiedSendService, AfiliasiService, UIControlService, GlobalConstantService, UploadFileConfigService, UploaderService) {
        var vm = this;
        vm.detail = item.item;
        vm.isAdd = item.act;
        vm.action = "";
        vm.tipeAfiliasi = "";
        vm.deskripsi = "";
        vm.jumlahSaham = 0;
        vm.klik = 0;
        vm.afiliasiID = 0;
        vm.selectedVendor;
        vm.vendorName = "";
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.hasAffiliation = "4521";
        
        

        vm.init = init;
        function init() {
            if (localStorage.getItem('currLang').toLowerCase() == 'id') {
                vm.btnBack = vm.isAdd != 1 ? 'Batal' : 'Kembali';
            }
            else if (localStorage.getItem("currLang").toLowerCase() == 'en') {
                vm.btnBack = vm.isAdd != 1 ? 'Cancel' : 'Back';
            }
            $translatePartialLoader.addPart('data-afiliasi');
            loadVendor();
            if (vm.isAdd === 1) {
                vm.action = "Tambah";
                vm.adaData = item.adaData;
                vm.sisaSaham = 100 - Number(item.totalSaham);
            } else {
                vm.action = "Ubah";
                vm.vendorAfiliasiID = vm.detail.VendorAfiliasiID;
                vm.tipeAfiliasi = vm.detail.AfiliasiID.toString();
                vm.deskripsi = vm.detail.AfiliasiDetail;
                vm.jumlahSaham = vm.detail.StockQuantity;
                vm.vendorID = vm.detail.VendorID;
                vm.vendorName = vm.detail.VendorName;
                vm.Dokumen_Url = vm.detail.Document_Url;
                vm.afiliasiID = vm.detail.AfiliasiID;
                vm.sisaSaham = 100 - (Number(item.totalSaham) - Number(vm.jumlahSaham));
            }

            UploadFileConfigService.getByPageName("PAGE.VENDOR.AFFILIATION", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    //console.info(response);
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }

        vm.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        //vm.selectUpload = selectUpload;

        vm.uploadFile = uploadFile;
        function uploadFile() {
            //var selectedFileType = vm.fileUpload[0].name;
            //selectedFileType = selectedFileType.substring(selectedFileType.lastIndexOf('.') + 1);
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            if (vm.klik > 0) {
                return;
            }

            if (vm.afiliasiID != 6) {
                if (vm.vendorName.VendorName) {
                    vm.vendorSave = vm.vendorName.VendorName;
                } else {
                    vm.vendorSave = vm.vendorName;
                }
                if (vm.vendorSave == "") {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.VENDOR");
                    return;
                }

                if (vm.tipeAfiliasi == "") {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.TIPEAFILIASI");
                    return;
                }

                //if (vm.deskripsi == "") {
                //    UIControlService.unloadLoadingModal();
                //    UIControlService.msg_growl("error", "MESSAGE.DESKRIPSI");
                //    return;
                //}

                if (vm.jumlahSaham == 0 || vm.jumlahSaham == "") {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", "MESSAGE.JUMLAH");
                    return;
                }
            }
            vm.klik += 1;

            if (vm.fileUpload) {
                if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
                        upload(1, vm.fileUpload, vm.idFileSize, vm.idFileTypes);
                } else {
                    UIControlService.unloadLoadingModal();
                    vm.fileUpload = "";
                    vm.klik = 0;
                    return false;
                }
            } else {
                vm.klik = 0;
                if (vm.isAdd == 1) {
                    UIControlService.unloadLoadingModal();
                    vm.fileUpload = "";
                    UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
                    return;
                } else {
                    vm.simpan('');
                }
            }
        }

        //function validateFileType(file, allowedFileTypes) {
        //    var valid_size = allowedFileTypes[0].Size;
        //    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
        //    if (size_file > valid_size) {
        //        UIControlService.unloadLoadingModal();
        //        UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE");
        //        vm.fileUpload = "";
        //        return false;
        //    }

            //if (!file || file.length == 0) {
            //    UIControlService.unloadLoadingModal();
            //    UIControlService.msg_growl("error", "MESSAGE.UPLOAD_DATA");
            //    return false;
            //}
        //    else return true;
        //}

        vm.upload = upload;
        function upload(id, file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }

            //UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleAffiliation(id, file, size, filters, function (response) {
                //console.info("response:" + JSON.stringify(response));
                if (response.status == 200) {
                    //console.info(response);
                    var url = response.data.Url;
                    vm.pathFile = url;
                    vm.name = response.data.FileName;
                    var s = response.data.FileLength;
                    vm.DocUrl = vm.pathFile;
                    //console.info(vm.DocUrl);
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s);
                        //  console.info(vm.size);
                    }

                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }

                    vm.simpan(vm.DocUrl);
                } else {
                    UIControlService.msg_growl("error", "Error!");
                    return;
                }
            }, function (response) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "MESSAGE.API");
                return;
            });


        }

        vm.ubahVendor = ubahVendor;
        function ubahVendor(data) {
            vm.vendorID = data.VendorID;
        }

        vm.ubahJumlahSaham = ubahJumlahSaham;
        function ubahJumlahSaham(jumlah) {
            console.info(jumlah);
            console.info(vm.sisaSaham);
            if (Number(jumlah) < 0 || Number(jumlah) > vm.sisaSaham) {
                UIControlService.msg_growl("warning", "MESSAGE.SAHAM");
                vm.jumlahSaham = "";
            }
        }

        vm.loadAfiliasi = loadAfiliasi;
        function loadAfiliasi() {
            AfiliasiService.getAfiliasi(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.afiliasi = reply.data;
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendor = loadVendor;
        function loadVendor() {
            UIControlService.loadLoadingModal("MESSAGE.LOADING");
            AfiliasiService.getVendor(function (reply) {
                if (reply.status === 200) {
                    vm.datavendor = reply.data;
                    vm.vendor = [];
                    for (var i = 0; i < vm.datavendor.length; i++) {
                        var param = {
                        VendorName: vm.datavendor[i].VendorName
                        }
                        vm.vendor.push(param);
                    }
                    loadAfiliasi();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }





        vm.changeMemiliki = changeMemiliki;
        function changeMemiliki(data) {
            vm.hasAffiliation = data;
            if (data == "4521") {
                vm.afiliasiID = 0;
            } else {
                vm.afiliasiID = 6;
            }
        }

        vm.ubahTipe = ubahTipe;
        function ubahTipe(data) {
            vm.tipeAfiliasi = data;
        }

        vm.simpan = simpan;
        function simpan(url) {
            var DocUrl;
            if (url != '') {
                DocUrl = url;
            } else {
                if (vm.isAdd == 0) {
                    DocUrl = vm.Dokumen_Url;
                }
            }
            var afID = 0;
            var desc = "";
            if (vm.isAdd == 1 && vm.adaData == 0) {
                if (vm.hasAffiliation == "4521") {
                    afID = Number(vm.tipeAfiliasi);
                    desc = vm.deskripsi;
                } else {
                    afID = vm.afiliasiID;
                    desc = "Tidak Memiliki afiliasi / no affiliation";
                }
            } else {
                afID = Number(vm.tipeAfiliasi);
                desc = vm.deskripsi;
            }
            
            AfiliasiService.save({
                create: vm.isAdd,
                VendorAfiliasiID: vm.vendorAfiliasiID,
                VendorID: Number(vm.vendorID),
                VendorName: vm.vendorSave,
                AfiliasiID: afID,
                StockQuantity: Number(vm.jumlahSaham),
                AfiliasiDetail: desc,
                Document_Url : DocUrl
            }, function (reply) {
                if (reply.status === 200) {
                    if (vm.isAdd == 1) {
                        if (reply.data == 0) {
                            UIControlService.unloadLoadingModal();
                            UIControlService.msg_growl("warning", "MESSAGE.SUDAHADA");
                            return;
                        }
                    }
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS");
                    $uibModalInstance.close();
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();