﻿(function () {
    'use strict';

    angular.module("app").controller("afiliasiCtrl", ctrl);

    ctrl.$inject = ['$translatePartialLoader', '$uibModal', 'UIControlService', 'GlobalConstantService', 'AfiliasiService', 'VerifiedSendService','$filter', '$rootScope', '$q','AuthService','FileSaver'];
    /* @ngInject */
    function ctrl($translatePartialLoader, $uibModal, UIControlService, GlobalConstantService, AfiliasiService, VerifiedSendService, $filter, $rootScope, $q, AuthService, FileSaver) {
        var vm = this;

        vm.totalItems = 0;
        vm.currentPage = 0;
        //vm.maxSize = 10;
        vm.pageSize = 10;
        vm.page_id = 35;
        vm.menuhome = 0;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.userId = 0;
        vm.jLoad = jLoad;
        vm.bank = [];
        vm.ambilUrl;
        vm.ambilUrl2;
        vm.IsApprovedCR = false;
        vm.menuIndex = 7;
        vm.isDataCIVD = false;
        vm.afiliasiID = 0;
        vm.adaData = 0;
        vm.civdID = 0;
        vm.username = localStorage.getItem('username');
        
        vm.langID = true;
        //vm.Kata = "";
        vm.VendorID;

        vm.init = init;
        function init() {
            UIControlService.loadLoading("Loading...");
            if (localStorage.getItem('currLang').toLowerCase() != 'id') {
                vm.langID = false;
            }
            $translatePartialLoader.addPart('data-afiliasi');
            vm.afiliasiID = 0;
            vm.totalSaham = 0;
            getVendorNation();
            
            loadVendorVerificationReview().then(function () {
                loadCheckCR();
            });
        }

        function loadVendorVerificationReview() {
            var defer = $q.defer();
            VerifiedSendService.getVendorVerificationReview({
                MenuID: 1047
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.vendorReviewData = data;
                    console.log(data)
                    if (data != null) {
                        if (data.ReviewStatus == 4526) {
                            vm.revisiVendor = true;
                        } else {
                            vm.revisiVendor = false;
                        }
                        if (vm.langID) {
                            vm.revisiName = data.Locale_Id;
                        } else {
                            vm.revisiName = data.Locale_En;
                        }
                    }
                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                defer.reject;
                return;
            })
            return defer.promise;
        }

        function getVendorNation() {
            AuthService.getRoleUserLogin({ Keyword: vm.username }, function (reply) {
                if (reply.status === 200 && reply.data.List.length > 0) {
                    var data = reply.data.List;
                    if (data[0].RoleName == "APPLICATION.ROLE_VENDOR_INTERNATIONAL") {
                        vm.menuIndex = 5;
                    }
                } else {

                }
            }, function (err1) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        function loadCheckCR() {
            AfiliasiService.getCRbyVendor({ CRName: 'OC_VENDORAFFILIATION' }, function (reply) {
                if (reply.status === 200) {
                    vm.CRStock = reply.data;
                    cekVendor();
                    cekCR();
                    if (reply.data[0] === true) {
                        vm.IsApprovedCR = true;
                    } else {
                        if (reply.data[0] === false) {
                            vm.IsApprovedCR = false;
                        }
                    }
                }

            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function cekCR() {
            AfiliasiService.cekCR(function (reply) {
                if (reply.status === 200) {
                    vm.isCR = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function cekVendor() {
            AfiliasiService.cekVendor( function (reply) {
                if (reply.status === 200) {
                    var dataVendor = reply.data[0];
                    if (dataVendor) {
                        vm.vendorName = dataVendor.VendorName;
                        vm.vendorCategoryID = dataVendor.VendorCategoryID;
                        vm.civdID = dataVendor.CIVDID;
                        if (vm.vendorCategoryID == 32) {
                            //cekVendorAfiliasi();
                            vm.isDataCIVD = true;
                        }
                        vm.jLoad(1);
                    } else {
                        UIControlService.unloadLoading();
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        //function cekVendorAfiliasi() {
        //    AfiliasiService.cekVendorAfiliasi(function (reply) {
        //        if (reply.status === 200) {
        //            var countVendorAfiliasi = reply.data.Count;
        //            if (countVendorAfiliasi > 0) {
        //            }
        //        }
        //    }, function (err) {
        //        UIControlService.msg_growl("error", "MESSAGE.API");
        //        UIControlService.unloadLoading();
        //    });
        //}

        vm.getDataCIVD = getDataCIVD;
        function getDataCIVD() {
            UIControlService.loadLoading("Loading...");
            var defer = $q.defer();
            var url = "https://apiprovider.civd-migas.com/vendor/afiliasi?vendorName=" + vm.vendorName;

            AfiliasiService.getDataCIVD({
                Keyword: url
            }, function (response) {
                UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply);
                    vm.dataAfiliasi = reply.result;
                    simpan(vm.dataAfiliasi);
                } else {
                    defer.reject(false)
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject();
            });

            return defer.promise;
        }

        function simpan(data) {
            AfiliasiService.saveCIVD({
                dataCIVD: data,
                CIVDID: vm.civdID
            }, function (reply) {
                if (reply.status === 200) {
                    vm.jLoad(1);
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        function getAfiliasiID(afiliasiName) {
            AfiliasiService.getAfiliasiID({
                Keyword: afiliasiName
            }, function (reply) {
                if (reply.status === 200) {
                    var afiliasiID = reply.data[0].AfiliasiID;
                    return afiliasiID;
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_ADD_DATA");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoadingModal();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            UIControlService.loadLoading("MESSAGE.LOADING");
            vm.currentPage = current;
            var offset = (current * 10) - 10;
            AfiliasiService.select({
                Keyword: vm.keyword,
                Offset: offset,
                Limit: vm.pageSize
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.afiliasi = data.List;
                    vm.adaData = vm.afiliasi.length == 0 ? 0 : 1;
                    vm.totalItems = data.Count;
                    if (vm.afiliasi.length > 0) {
                        for (var a = 0; a < vm.afiliasi.length; a++) {
                            vm.totalSaham += vm.afiliasi[a].StockQuantity;
                            console.info(vm.totalSaham);
                            console.info(vm.afiliasi[a].StockQuantity);
                            if (vm.afiliasi[a].AfiliasiID == 6) {
                                vm.afiliasiID = 6;
                            }
                        }
                        console.info(vm.totalSaham);
                    }
                    if ($rootScope.menus != undefined) {
                        if (vm.afiliasi.length == 0) {
                            $rootScope.menus[vm.menuIndex].IsChecked = '';
                        } else {
                            $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
                        }
                    }


                    AfiliasiService.getCRbyVendor({ CRName: 'OC_VENDORAFFILIATION' }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {
                            if (reply.data[0] === true) {
                                vm.IsApprovedCR = true;
                            } else {
                                vm.IsApprovedCR = false;
                            }

                            if (vm.vendorReviewData != null && reply.data[1] != "Approved_CR") {
                                if (vm.revisiVendor) {
                                    vm.IsApprovedCR = true;
                                } else {
                                    vm.IsApprovedCR = false;
                                }
                            }

                            
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", "MESSAGE.API");
                        UIControlService.unloadLoading();
                    });
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_DATA");
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.tambah = tambah;
        function tambah(data) {
            var data = {
                act: 1,
                item: data,
                adaData: vm.adaData,
                totalSaham: vm.totalSaham
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/afiliasi/tambah-ubah.modal.html',
                controller: "AfiliasiModalCtrl",
                controllerAs: "AfiliasiModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                // window.location.reload();
                vm.init();
            });
        }

        vm.edit = edit;
        function edit(data) {
            var data = {
                act: 0,
                item: data,
                totalSaham: vm.totalSaham
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/afiliasi/tambah-ubah.modal.html',
                controller: "AfiliasiModalCtrl",
                controllerAs: "AfiliasiModalCtrl",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.init();
            });
        }

        vm.remove = remove;
        function remove(data) {
            bootbox.confirm({
                message: '<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-close"></i> ' + $filter('translate')('Batal')
                    },
                    confirm: {
                        label: '<i class="fa fa-save"></i> OK'
                    }
                },
                callback: function (result) {
                    if (result) {
                        //UIControlService.loadLoading(loadmsg);
                        AfiliasiService.remove({
                            VendorAfiliasiID: data.VendorAfiliasiID
                        }, function (reply2) {
                            UIControlService.unloadLoading();
                            if (reply2.status === 200) {
                                UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_DELETE');
                                vm.init();
                            } else {
                                UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
                            }
                        }, function (error) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
                        });
                    }
                }
            });
        };

        vm.revisiVendorDetail = revisiVendorDetail;
        function revisiVendorDetail() {
            var data = {
                data: vm.vendorReviewData,
                langID: vm.langID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/revisi-modal/revisi-modal.html',
                controller: 'revisiModalController',
                controllerAs: 'revisiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        vm.downloadFileCivd = downloadFileCivd;
        function downloadFileCivd(url, name) {
            UIControlService.loadLoading();

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    var byteCharacters = atob(reply.result);

                    var byteNumbers = new Array(byteCharacters.length);
                    for (let i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    var blob = new Blob([byteArray], {
                        type: "application/pdf"
                    });

                    FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
                    UIControlService.unloadLoading();

                } else {
                    UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

            });
        }
    }
})();