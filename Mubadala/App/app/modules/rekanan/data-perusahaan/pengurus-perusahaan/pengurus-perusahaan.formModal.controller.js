﻿
(function () {
	'use strict';

	angular.module("app").controller("formPengurusPerusahaanCtrl", ctrl);

	ctrl.$inject = ['$uibModalInstance', 'item', '$location', 'PengurusPerusahaanService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'ProvinsiService'];
	/* @ngInject */
	function ctrl($uibModalInstance, item, $location, PengurusPerusahaanService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService, ProvinsiService) {
		var loadmsg = 'MESSAGE.LOADING';
		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.compPerson = item.compPerson;
		vm.file;
		vm.isCalendarOpened = [false, false, false];
		vm.positionTypes = [];
		//vm.IsCR = item.compPerson.IsCR;
		vm.vendorLocation = item.compPerson.Location;
		vm.action = item.action;
		//vm.compPerson.PositionRef = "AUTHORIZED_PERSON";

		//console.info("cr:" + JSON.stringify(item.compPerson.Address.StateID));
		vm.StateID = item.compPerson.Address.StateID;
		vm.compPerson.DateOfBirth = item.compPerson.DateOfBirth2;

		vm.myConfig = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "CountryID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig2 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "StateID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig3 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "CityID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.myConfig4 = {
		    maxItems: 1,
		    optgroupField: "Name",
		    labelField: "Name",
		    valueField: "DistrictID",
		    searchField: "Name",
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.init = init;
		function init() {
			//console.info(item);
			UIControlService.loadLoadingModal(loadmsg);
			//console.info("action:" + JSON.stringify(vm.action));
			//Konfigurasi upload disamakan dengan yang ada di halaman pendaftaran
			if (localStorage.getItem('currLang').toLowerCase() == 'id') {
			    vm.btnBack = vm.action === 'edit' ? 'Batal' : 'Kembali';
			}
			else if (localStorage.getItem("currLang").toLowerCase() == 'en') {
			    vm.btnBack = vm.action === 'edit' ? 'Cancel' : 'Back';
			}
			UploadFileConfigService.getByPageName("PAGE.VENDOR.COMPANYPERSON", function (response) {
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
					PengurusPerusahaanService.GetPositionTypes(function (response) {
						UIControlService.unloadLoadingModal();
						if (response.status == 200) {
							vm.positionTypes = response.data;
							if (vm.action === 'add') {
								loadCountries();
							} else if (vm.action === 'edit') {
							    loadCountries(vm.compPerson.Address.State);
							    vm.countryID = vm.compPerson.Address.State.CountryID;
							    vm.stateID = vm.compPerson.Address.StateID;
							    vm.cityID = vm.compPerson.Address.CityID;
							    vm.districtID = vm.compPerson.Address.DistrictID;
							    vm.url = vm.compPerson.IDUrl;
							}
							//loadCountries();getProvinsi();
						} else {
							UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_POSITION");
						}
					}, function (err) {
						UIControlService.unloadLoadingModal();
						UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_POSITION");
					});
				} else {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_FILETYPE");
			});
		}

		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		/* begin provinsi, kabupaten, kecamatan */

		function loadCountries(data) {
			//UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
			PengurusPerusahaanService.getCountries(
                function (response) {
                	vm.countryList = response.data;
                	//console.info("negara"+JSON.stringify(vm.countryList));
                	if (vm.action === 'edit') {
                		for (var i = 0; i < vm.countryList.length; i++) {
                			if (vm.countryList[i].CountryID === data.CountryID) {
                				vm.selectedCountry = vm.countryList[i];
                				vm.countryCode = data.Country.Code;
                				changeCountry(false, data);
                			}
                		}
                	}
                }, function (response) {
                	UIControlService.unloadLoadingModal();
                	UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_STATES");;
                });
		}

		vm.defineState = defineState;
		function defineState(id) {
		    vm.stateID = id;
		    if (vm.stateID == undefined) {
		        vm.compPerson.Address.StateID = "";
		        vm.compPerson.Address.CityID = "";
		        vm.compPerson.Address.DistrictID = "";
		    }
		    vm.compPerson.Address.StateID = vm.stateID;
		    changeProvince();
		}

		function getProvinsi() {
			UIControlService.loadLoadingModal(loadmsg);
			ProvinsiService.getStates(vm.CountryID, function (response) {
				UIControlService.unloadLoadingModal();
				vm.listProvinsi = response.data;
				getCities();
			}, function (response) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_STATES");;
			});
		}

		vm.changeProvince = changeProvince;
		function changeProvince() {
			vm.compPerson.Address.CityID = null;
		    //vm.listKabupaten = [];
			vm.cityID = undefined;
			vm.districtID = undefined;
			vm.compPerson.Address.DistrictID = null;
			//vm.listKecamatan = [];
			getCities();
		}

		vm.defineCountry = defineCountry;
		function defineCountry(id) {
		    vm.countryID = id;
		    if (vm.countryID == undefined) {
		        vm.countryCode = "";
		        vm.stateID = undefined;
		        vm.cityID = undefined;
		        vm.districtID = undefined;
		        vm.selectedCountry = "";
		        vm.compPerson.Address.StateID = "";
		        vm.compPerson.Address.CityID = "";
		        vm.compPerson.Address.DistrictID = "";
		        return;
		    }

		    vm.countryList.forEach(function (item) {
		        if (item.CountryID == vm.countryID) {
		            vm.selectedCountry = item;
		        }
		    });
		    changeCountry(true, vm.selectedCountry);
		}

		vm.changeCountry = changeCountry;
		function changeCountry(flag, data) {
			if (flag === true) {
				vm.countryCode = data.Code;
			}
			vm.CountryID = data.CountryID;
			getProvinsi();
		}

		vm.defineCity = defineCity;
		function defineCity(id) {
		    vm.cityID = id;
		    if (vm.cityID == undefined) {
		        vm.compPerson.Address.CityID = "";
		        vm.compPerson.Address.DistrictID = "";
		    }
		    vm.compPerson.Address.CityID = vm.cityID;
		    vm.changeCities();
		}

		function getCities() {
			if (vm.compPerson.Address.StateID) {
				UIControlService.loadLoadingModal(loadmsg);
				ProvinsiService.getCities(vm.compPerson.Address.StateID, function (response) {
					UIControlService.unloadLoadingModal();
					vm.listKabupaten = response.data;
					getDistrict();
				}, function (response) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_CITIES");
				});
			}
		}

		vm.validateAge = validateAge;
		function validateAge(inputDate) {
		    //vm.compPerson.DateOfBirth2
		    if (inputDate === undefined) {
		        var errorMessage = localStorage.getItem("currLang").toLowerCase() == 'id' ? "Tanggal tidak valid" : "Invalid date";
		        UIControlService.msg_growl("error", errorMessage);
		        vm.compPerson.DateOfBirth = '';
		        return;
		    }
			//var convertedDate = moment(inputDate).format("DD-MM-YYYY");
			var validatedAge = false;
			var birthDate = moment(inputDate).format("DD-MM");;
			var dateNow = moment().format("DD-MM");
			var birthYear = moment(inputDate).format("YYYY");;
			var yearNow = moment().format("YYYY");
			var yearAge = yearNow - birthYear;
			if (yearAge > 17) {
				var validatedAge = true;
			} else if (yearAge === 17) {
				if (birthDate < dateNow || birthDate === dateNow) {
					var validatedAge = true;
				}
			}

			if (validatedAge === false) {
				UIControlService.msg_growl('error', "ERRORS.AGE_UNDER17");
				vm.compPerson.DateOfBirth = "";
				return;
			}

			//console.info("umur" + JSON.stringify(yearAge));
			//console.info("validasi" + JSON.stringify(validatedAge));
		}

		vm.changeCities = changeCities;
		function changeCities() {
			vm.compPerson.Address.DistrictID = null;
			vm.listKecamatan = [];
			getDistrict();
		}

		vm.defineDistrict = defineDistrict;
		function defineDistrict(id) {
		    vm.districtID = id;
		    if (vm.districtID == undefined) {
		        vm.compPerson.Address.DistrictID = "";
		    }
		    vm.compPerson.Address.DistrictID = vm.districtID;
		}

		function getDistrict() {
			if (vm.compPerson.Address.CityID) {
				UIControlService.loadLoadingModal(loadmsg);
				ProvinsiService.getDistrict(vm.compPerson.Address.CityID, function (response) {
					UIControlService.unloadLoadingModal();
					vm.listKecamatan = response.data;
				}, function (response) {
					UIControlService.unloadLoadingModal();
					UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_DISTRICT");
				});
			}
		}
		/* end provinsi, kabupaten, kecamatan */

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		}

		vm.fileSelect = fileSelect;
		function fileSelect(file) {
			vm.file = file;
		}

		vm.save = save;
		function save() {
			//if (validateField() === false) {
			//	UIControlService.msg_growl("error", "MESSAGE.ERR_INCOMPLETE_FIELD");
			//	return;
			//}
			if (validateField()) {
			    if (!vm.file && !vm.compPerson.IDUrl) {
			        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			        return;
			    }
			    if (vm.file) {
			        uploadFile(vm.file);
			    } else {
			        if (vm.action == 'add') {
			            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			            return;
			        } else {
			            saveCompPerson(vm.compPerson.IDUrl);
			        }
			    }
			}		
		};

		function validateField() {
		    if (!vm.compPerson.PersonName) {
		        UIControlService.msg_growl("error", "MESSAGE.NAME");
		        return false;
		    }

		    if (!vm.compPerson.DateOfBirth) {
		        UIControlService.msg_growl("error", "MESSAGE.BIRTH");
		        return false;
		    }

		    if (vm.vendorLocation === 'IDN' && !vm.compPerson.NoID) {
		        UIControlService.msg_growl("error", "MESSAGE.NOID");
		        return false;
		    }

		    if (!vm.compPerson.Address.AddressInfo) {
		        UIControlService.msg_growl("error", "MESSAGE.ADDRESS");
		        return false;
		    }

		    if (!vm.selectedCountry) {
		        UIControlService.msg_growl("error", "MESSAGE.COUNTRY");
		        return false;
		    }

		    if (!vm.compPerson.Address.StateID) {
		        UIControlService.msg_growl("error", "MESSAGE.STATE");
		        return false;
		    }

		    if (vm.countryCode === 'IDN' && !vm.compPerson.Address.CityID) {
		        UIControlService.msg_growl("error", "MESSAGE.CITY");
		        return false;
		    }

		    if (vm.countryCode === 'IDN' && !vm.compPerson.Address.DistrictID) {
		        UIControlService.msg_growl("error", "MESSAGE.DISTRICT");
		        return false;
		    }


		    if (!vm.compPerson.ServiceStartDate) {
		        UIControlService.msg_growl("error", "MESSAGE.STARTDATE");
		        return false;
		    }

		    return true;


			//console.info("validate" + JSON.stringify(vm.vendorLocation));
			//if (vm.vendorLocation === 'IDN') {
				//if (!vm.compPerson.PersonName || !vm.compPerson.DateOfBirth || !vm.compPerson.NoID || !vm.compPerson.ServiceStartDate || !vm.compPerson.Address.AddressInfo) {
				//	//console.info("vendor lokal");
				//	return false;
				//}

				



			//} else if (vm.vendorLocation !== 'IDN') {
			//	if (!vm.compPerson.PersonName || !vm.compPerson.DateOfBirth || !vm.compPerson.ServiceStartDate || !vm.compPerson.Address.AddressInfo) {
			//		//console.info("vendor internasional");
			//		return false;
			//	}
			//}
			
		};

		function uploadFile(file) {
		    if (!file) {
		        if (vm.action == 'add') {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
		        } else {
		            saveCompPerson(vm.url);
		        }
		    }
		    if (UIControlService.validateFileType(file, vm.idUploadConfigs)) {
		        upload(file, vm.idFileSize, vm.idFileTypes);
		    }
		}

		function validateFileType(file, allowedFileTypes) {
		    var valid_size = allowedFileTypes[0].Size;
		    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
		    if (size_file > valid_size) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("ERRORS", "MESSAGE.FILE_SIZE_EXCEEDED");
		        return false;
		    }
			//if (!file || file.length == 0) {
			//	UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			//	return false;
			//}
			return true;
		}

		function upload(file, config, types) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}
			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UIControlService.loadLoadingModal(loadmsg);
			UploaderService.uploadCompanyPersonID(vm.compPerson.VendorID, file, size, types,
            function (reply) {
            	if (reply.status == 200) {
            		UIControlService.unloadLoadingModal();
            		var url = reply.data.Url;
            		saveCompPerson(url);
            	} else {
            		UIControlService.unloadLoadingModal();
            		UIControlService.msg_growl("error", 'MESSAGE.ERR_UPLOAD');
            	}
            }, function (err) {
            	UIControlService.unloadLoadingModal();
            	UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
            });
		};

		function saveCompPerson(url) {
			vm.compPerson.IDUrl = url;
			vm.compPerson.CompanyPosition;
			vm.compPerson.DateOfBirth = UIControlService.getStrDate(vm.compPerson.DateOfBirth);
			vm.compPerson.ServiceStartDate = UIControlService.getStrDate(vm.compPerson.ServiceStartDate);
			vm.compPerson.ServiceEndDate = UIControlService.getStrDate(vm.compPerson.ServiceEndDate);
			/*start handle change request*/
			//console.info(JSON.stringify(vm.compPerson.NoID));
			if (vm.compPerson.ID === undefined) {
				//console.info("Add");
				//vm.compPerson['IsTemporary'] = true;
				//vm.compPerson['RefVendorId'] = null;
				//vm.compPerson['Action'] = 0;
			} else if (!vm.compPerson.ID === undefined) {
				//vm.compPerson['IsTemporary'] = true;
				//vm.compPerson['RefVendorId'] = vm.compPerson.ID;
				//vm.compPerson['Action'] = 1;
				//console.info("edit");
			}
			/*end handle change request*/
			//console.info(vm.IsCR);
			var saveSingle = vm.compPerson.ID ? PengurusPerusahaanService.EditSingle : PengurusPerusahaanService.CreateSingle;
			if (vm.vendorLocation !== 'IDN' && vm.compPerson.NoID === undefined) {
				vm.compPerson.NoID = "";
			}
			UIControlService.loadLoadingModal(loadmsg);
			saveSingle(vm.compPerson, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("notice", "MESSAGE.SUCC_SAVE");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
				}
			}, function (error) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE");
			});
		};

		vm.close = close;
		function close() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.selectedStartDate = selectedStartDate;
		function selectedStartDate(sid) {
		    if (!(vm.compPerson.ServiceEndDate == undefined || vm.compPerson.ServiceEndDate == null)) {
		        if (sid > vm.compPerson.ServiceEndDate) {
		            UIControlService.msg_growl("warning", "MESSAGE.ERR_ISSUEDDATE");
		            vm.compPerson.ServiceStartDate = undefined;
		            return;
		        }
		    }
		}

		vm.selectedFinishDate = selectedFinishDate;
		function selectedFinishDate(sed) {
		    if (sed > vm.compPerson.ServiceStartDate) {
		    } else if (sed < vm.compPerson.ServiceStartDate) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_DATE");
		        vm.compPerson.ServiceEndDate = undefined;
		    }
		}
	}
})();