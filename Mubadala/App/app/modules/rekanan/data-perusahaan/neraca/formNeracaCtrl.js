﻿(function () {
	'use strict';

	angular.module("app")
            .controller("formNeracaCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'BalanceVendorService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService','$q'];
	function ctrl($http, $translate, $translatePartialLoader, $location, BalanceVendorService, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService,$q) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isAdd = item.act;
		vm.item = item.item;
		vm.VendorID = item.VendorID;
		vm.action = "";
		vm.fileUpload;
		vm.fileUrl;
		vm.Nominal = "";
		vm.Amount;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.DocUrl;
		vm.isApprovedCR = false;
		vm.langID = true;
		vm.disableCurr = 0;
		vm.vendorbalance = item.VendorBalance;

		vm.myConfig5 = {
		    maxItems: 1,
		    optgroupField: "Symbol",
		    labelField: "symbolLabel",
		    valueField: "CurrencyID",
		    searchField: ["Symbol", "Label"],
		    render: {
		        optgroup_header: function (item, escape) {
		            return '<div class="optgroup-header">' + ' <h5 class="scientific" style="margin-top:5px;margin-bottom:5px;"><strong>' + escape(item.label_scientific) + '</strong></h5></div>';
		        }
		    },
		    optgroups: [
              { value: '1', label: 'is_parent', label_scientific: '1' }
		    ]
		};

		vm.init = init;
		function init() {
		    if (localStorage.getItem('currLang').toLowerCase() != 'id') {
		        vm.langID = false;
		        vm.btnBack = !vm.isAdd ? 'Cancel' : 'Back';
		    }
		    else if (localStorage.getItem("currLang").toLowerCase() == 'id') {
		        vm.btnBack = !vm.isAdd ? 'Batal' : 'Kembali';
		    }

			$translatePartialLoader.addPart('vendor-balance');
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.BALANCE", function (response) {
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				return;
			});
			if (vm.isAdd === true) {
			    vm.action = "Tambah";
			} else {
				vm.action = "Ubah ";
				vm.Amount = vm.item.Amount;
				vm.Nominal = vm.item.Nominal;
				vm.fileUrl = vm.item.DocUrl;
				vm.currencyID = vm.item.CurrencyID;
			}
			loadCurrency();
			UIControlService.loadLoadingModal("Silahkan Tunggu");

			var prom1 = loadAsset().then(function (response) {
			    return response
			});
			//var prom2 = loadUnit().then(function (response) {
			//    return response
			//});
			var prom3 = loadCheckCR().then(function (response) {
			    return response
			});
			$q.all([prom1]).then(function (result) {
			   
			    //get Sisa Balance
			    getSisaBalance();
			})
			

		}

		vm.changeCurr = changeCurr;
		function changeCurr(id) {
		    vm.currencyID = id;
		    vm.loadCurrency();
		}

		vm.loadCurrency = loadCurrency;
		function loadCurrency() {
		    BalanceVendorService.getCurrencies(function (reply) {
		        UIControlService.unloadLoading();
		        if (reply.status === 200) {
		            vm.currencyList = reply.data;
		            for (var i = 0; i < vm.currencyList.length; i++) {
		                vm.currencyList[i].symbolLabel = vm.currencyList[i].Symbol + ' | ' + vm.currencyList[i].Label;
		                if (vm.currencyID == vm.currencyList[i].CurrencyID)
		                    vm.currency = vm.currencyList[i];
		            }
		            if (vm.isAdd === true) {
		                cekCurrentCurrency();
		            }
		        } else {
		            $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
		            UIControlService.unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        //$.growl.error({ message: "Gagal Akses API >" + err });
		        UIControlService.unloadLoading();
		    });
		}

		vm.changeSubCOA = changeSubCOA;
		function changeSubCOA(param) {
			BalanceVendorService.getUnit(function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    vm.listUnit = reply.data.List;
				    vm.selectedUnit = [];
					if (param.Name === 'SUB_CASH_TYPE2') {
						vm.selectedUnit = vm.listUnit[0];
					} else if (param.Name === 'SUB_CASH_TYPE3') {
						vm.selectedUnit = vm.listUnit[2];
					} else if (param === 0) {
						vm.selectedUnit = vm.listUnit[1];
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		function loadCheckCR() {
		    var defer = $q.defer();
		    BalanceVendorService.getCRbyVendor({ CRName: 'OC_VENDORBALANCE' }, function (reply) {
				if (reply.status === 200) {
					if (!(reply.data === null) && reply.data.ApproveBy === 1) {
						vm.isApprovedCR = true;
					} else {
						vm.isSentCR = false;
					}
					defer.resolve(true);
				}

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				defer.reject();
			});
			return defer.promise;
		}
		vm.selectedAsset = [];
		vm.listAsset;
		vm.asset = 0;
		vm.hutang = 0;
		function loadAsset() {
		    var defer = $q.defer();

		    BalanceVendorService.select({ VendorID : 0}, function (reply) {
		        if (reply.status === 200) {
				    vm.listAsset = reply.data;
				    for (var i = 0; i < vm.listAsset.length; i++) {
                        if (vm.listAsset[i].listDataBalanceModel != null) {
                            if (vm.listAsset[i].BalanceName == "WEALTH_TYPE_ASSET") {
                                for (var c = 0; c < vm.listAsset[i].listDataBalanceModel.length; c++) {
                                    for (var g = 0; g < vm.listAsset[i].listDataBalanceModel[c].subCategory.length; g++) {
                                        vm.asset += Number(vm.listAsset[i].listDataBalanceModel[c].subCategory[g].Nominal);
                                    }
                                }
                            }
                            if (vm.listAsset[i].BalanceName == "WEALTH_TYPE_DEBTH") {
                                for (var c = 0; c < vm.listAsset[i].listDataBalanceModel.length; c++) {
                                    for (var g = 0; g < vm.listAsset[i].listDataBalanceModel[c].subCategory.length; g++) {
                                        vm.hutang += Number(vm.listAsset[i].listDataBalanceModel[c].subCategory[g].Nominal);
                                    }
                                }
                            }
                        }
                        if (vm.listAsset[i].BalanceName == "WEALTH_TYPE_MODAL") {
                            vm.listAsset.splice(i, 1);
                        }
                    }
                   


				    //vm.selectedAsset = [];
					//if (vm.isAdd === false) {
					//	for (var i = 0; i < vm.listAsset.length; i++) {
					//	    if (item.item.Wealth.Name === vm.listAsset[i].BalanceName) {
					//			vm.selectedAsset = vm.listAsset[i];
					//			loadCOA(vm.selectedAsset);
					//			break;
					//		}
					//	}
					//}

					defer.resolve(true);


				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				defer.reject();
			});

		    return defer.promise;
		}

		function getSisaBalance() {
		    BalanceVendorService.getSisaMstBalance(
            { assetBalance: vm.asset, hutangBalance: vm.hutang },
            function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    var dataAsset = data[0];
                    var dataHutang = data[1];

                    for (var i = 0; i < dataAsset.length; i++) {
                        vm.listAsset.push(dataAsset[i]);
                    }
                    for (var i = 0; i < dataHutang.length; i++) {
                        vm.listAsset.push(dataHutang[i]);
                    }

                    UIControlService.unloadLoadingModal();

                    vm.selectedAsset = [];
                    if (vm.isAdd === false) {
                        for (var i = 0; i < vm.listAsset.length; i++) {
                            if (item.item.Wealth.Name === vm.listAsset[i].BalanceName) {
                                vm.selectedAsset = vm.listAsset[i];
                                loadCOA(vm.selectedAsset);
                                break;
                            }
                        }
                    }


                    //console.log(data)
                }
            }, function (err) {
                $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                UIControlService.unloadLoadingModal();
            })
		}

		vm.loadCOA = loadCOA;
		vm.selectedCOA = [];
		vm.disableLU;
		vm.listCOA = [];
		function loadCOA(data) {
		    vm.param = "";
			if (data.BalanceName === "WEALTH_TYPE_ASSET") {
				vm.param = "COA_TYPE_ASSET"
			} else if (data.BalanceName === "WEALTH_TYPE_DEBTH") {
				vm.param = "COA_TYPE_DEBTH"
			}
			UIControlService.loadLoadingModal();

			BalanceVendorService.getCOA({
				Keyword: vm.param
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    vm.listCOA = reply.data.List;
				    vm.selectedCOA = [];
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listCOA.length; i++) {
							if (item.item.COA.RefID === vm.listCOA[i].RefID) {
								vm.selectedCOA = vm.listCOA[i];
								loadSubCOA(vm.selectedCOA);
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});

		}


		vm.loadSubCOA = loadSubCOA;
		vm.selectedSubCOA = [];
		vm.listSubCOA = [];
		function loadSubCOA(data) {
			vm.param = "";
			if (data.RefID === 3100) {
				vm.param = "SUB_COA_CASH"
			} else if (data.RefID === 3101) {
				vm.param = "SUB_COA_DEBTHSTOCK"
			}
			BalanceVendorService.getSubCOA({
				Keyword: vm.param
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    vm.listSubCOA = reply.data.List;
				    vm.selectedSubCOA = [];
					var param = vm.listSubCOA.length;
					if (param === 0) {
						changeSubCOA(param);
					}
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listSubCOA.length; i++) {
							if (item.item.SubCOA.RefID === vm.listSubCOA[i].RefID) {
								vm.selectedSubCOA = vm.listSubCOA[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		vm.selectedUnit = [];
		vm.listUnit;
		function loadUnit() {
		    var defer = $q.defer();
			BalanceVendorService.getUnit(function (reply) {
				//UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
				    vm.listUnit = reply.data.List;
				    vm.selectedUnit = [];
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listUnit.length; i++) {
							if (item.item.Unit.RefID === vm.listUnit[i].RefID) {
								vm.selectedUnit = vm.listUnit[i];
								break;
							}
						}
					}
					defer.resolve(true);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
				defer.reject();
			});

			return defer.promise;
		}

		vm.selected = selected;
		function selected() {
			//console.info("respon1:" + JSON.stringify(vm.selectedDocumentType));
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		vm.uploadFile = uploadFile;
		function uploadFile() {
		    
		    if (vm.selectedAsset.length === 0) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_WEALTH_TYPE"); return;
		    }
		    else if (vm.selectedCOA.length === 0 && vm.listCOA.length !== 0) {
		       UIControlService.msg_growl("warning", "MESSAGE.ERR_ACC_TYPE"); return;
		    }
		    else if (vm.selectedSubCOA.length === 0 && vm.listSubCOA.length !== 0) {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_SUBACC_TYPE"); return;
		    }
		    //else if (vm.selectedUnit.length === 0) {
		    //    UIControlService.msg_growl("warning", "MESSAGE.ERR_UNIT"); return;
		    //}
		    //else if (vm.Amount === undefined || vm.Amount == 0) {
		    //    UIControlService.msg_growl("warning", "MESSAGE.ERR_AMOUNT"); return;
		        //}
		    else if(!vm.currency){
		        UIControlService.msg_growl("warning", "MESSAGE.CURR");
		        return;
		    }
		    else if (vm.Nominal === "") {
		        UIControlService.msg_growl("warning", "MESSAGE.ERR_NOMINAL"); return;
		    }
            addToSave();
		    //else if ((vm.fileUpload == undefined && vm.isAdd == true) || (vm.fileUpload == undefined && vm.isAdd == false && item.item.DocUrl == null)) {
		    //    UIControlService.msg_growl("warning", "MESSAGE.ERR_NOFILE"); return;
		    //}
		    //else {
		    //    //console.log(vm.selectedAsset)
		    //    //console.log(vm.idFileTypes)
		    //    //console.log(vm.idUploadConfigs);
		    //    //var tes = UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs);

            //    //console.log(tes)

		    //    //return;
		    //    if (vm.fileUpload === undefined && vm.isAdd === false) {
		    //        vm.DocUrl = item.item.DocUrl;
		    //        addToSave();
		    //    }
		    //    else if (vm.fileUpload !== undefined) {
		    //        if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
		    //            upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
		    //        }
		    //    }
		    //}




			
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
		    UIControlService.loadLoadingModal("LOADERS.LOADING_UPLOAD_FILE");

		    if (vm.listSubCOA.length !== 0)
		    {
		        vm.prefix = vm.selectedSubCOA.RefID + '_' + vm.selectedUnit.RefID;
		    } else if (vm.selectedCOA.length !== 0) {
		        vm.prefix = vm.selectedCOA.RefID + '_' + vm.selectedUnit.RefID;

		    } else {
		        vm.prefix = vm.selectedAsset.BalanceID;
		    }

			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}


			UploaderService.uploadSingleFileBalance(vm.VendorID,vm.prefix,file, size, filters, function (response) {
				UIControlService.unloadLoadingModal();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.DocUrl = url;
					vm.name = response.data.FileName;
					var s = response.data.FileLength;
					if (vm.flag == 0) {

						vm.size = Math.floor(s)
					}

					if (vm.flag == 1) {
						vm.size = Math.floor(s / (1024));
					}
					addToSave();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", response.data)
				UIControlService.unloadLoadingModal();
			});
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.addToSave = addToSave;
		vm.vendor = {};
		function addToSave() {
		    UIControlService.loadLoadingModal();
			if (vm.isAdd === true) {
				if (vm.listSubCOA.length === 0) {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						//DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						//Amount: vm.Amount,
						//UnitType: vm.selectedUnit.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID,
						WealthTypeName: vm.selectedAsset.BalanceName,
                        CurrencyID: vm.currency.CurrencyID
					};
				} else {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						//DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						//Amount: vm.Amount,
						//UnitType: vm.selectedUnit.RefID,
						SubCOAType: vm.selectedSubCOA.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID,
						WealthTypeName: vm.selectedAsset.BalanceName,
						CurrencyID: vm.currency.CurrencyID
					};
				}

			} else if (vm.isAdd === false) {
				if (vm.listSubCOA.length === 0) {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						BalanceID: item.item.BalanceID,
						//DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						//Amount: vm.Amount,
						//UnitType: vm.selectedUnit.RefID,
						COAType: vm.selectedCOA.RefID,
                        WealthType: vm.selectedAsset.RefID,
                        WealthTypeName: vm.selectedAsset.BalanceName,
                        CurrencyID: vm.currency.CurrencyID
					};
				} else {
					vm.vendor = {
						isApprovedCR: vm.isApprovedCR,
						BalanceID: item.item.BalanceID,
						//DocUrl: vm.DocUrl,
						Nominal: vm.Nominal,
						//Amount: vm.Amount,
						//UnitType: vm.selectedUnit.RefID,
						SubCOAType: vm.selectedSubCOA.RefID,
						COAType: vm.selectedCOA.RefID,
						WealthType: vm.selectedAsset.RefID,
						WealthTypeName: vm.selectedAsset.BalanceName,
						CurrencyID: vm.currency.CurrencyID
					};
				}
			}
			if (vm.isAdd === true) {
				BalanceVendorService.insert(vm.vendor,
                    function (reply) {
                    	UIControlService.unloadLoadingModal();
                    	if (reply.status === 200) {
                    		UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
                    		$uibModalInstance.close();

                    	} else {
                    		UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
                    		return;
                    	}
                    },
                    function (err) {
                    	UIControlService.msg_growl("error", "MESSAGE.API");
                    	UIControlService.unloadLoadingModal();
                    }
                );
			} else if (vm.isAdd === false) {
				BalanceVendorService.update(vm.vendor,
                    function (reply) {
                    	UIControlService.unloadLoadingModal();
                    	if (reply.status === 200) {
                    		UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
                    		$uibModalInstance.close();

                    	}
                    	else {
                    		UIControlService.msg_growl("error", "MESSAGE.FAILED_ADD");
                    		return;
                    	}
                    }, function (err) {
                    	UIControlService.msg_growl("error", "MESSAGE.API");
                    	UIControlService.unloadLoadingModal();
                    }
                );
			}
		}

		function cekCurrentCurrency() {
		    for (var i = 0; i < vm.vendorbalance.length; i++) {
		        if (vm.vendorbalance[i].listDataBalanceModel != null) {
		            //if (vm.vendorbalance[i].BalanceName == "WEALTH_TYPE_ASSET" && vm.selectedAsset.BalanceName == "WEALTH_TYPE_ASSET") {
		                if (vm.vendorbalance[i].listDataBalanceModel.length > 0) {
		                    if (vm.vendorbalance[i].listDataBalanceModel[0].subCategory.length > 0) {
		                        if (vm.isAdd) {
		                            vm.currencyID = vm.vendorbalance[i].listDataBalanceModel[0].subCategory[0].CurrencyID;
		                            vm.disableCurr = 1;
		                            break;
		                        }
		                    }
		                }
		            //}
		        }
		    }
		    for (var i = 0; i < vm.currencyList.length; i++) {
		        if (vm.currencyID == vm.currencyList[i].CurrencyID)
		            vm.currency = vm.currencyList[i];
		    }
		}

	}
})();