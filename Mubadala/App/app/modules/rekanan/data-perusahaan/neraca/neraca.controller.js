(function () {
    'use strict';

    angular.module("app").controller("BalanceVendorCtrl", ctrl);

    ctrl.$inject = ['AuthService', '$scope', '$state', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'BalanceVendorService', 'RoleService', 'UIControlService', 'GlobalConstantService', '$uibModal','$rootScope','VerifiedSendService','$q', '$filter'];
    function ctrl( AuthService, $scope, $state, $http, $translate, $translatePartialLoader, $location, SocketService, BalanceVendorService,
        RoleService, UIControlService, GlobalConstantService, $uibModal, $rootScope, VerifiedSendService,$q, $filter) {

        var vm = this;
        var page_id = 141;
        vm.departemen = [];
        var asset = [];
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        vm.userBisaMengatur = false;
        vm.allowAdd = true;
        vm.allowEdit = true;
        vm.allowDelete = true;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        //vm.isApprovedCR;
        vm.balanceDocUrl = "";
        vm.menuIndex = 4;
        vm.username = localStorage.getItem('username');
        vm.revisiVendor = false;//false
        vm.IsApprovedCR = false;
        vm.initialize = initialize;
        vm.changeDataPermission = false;
        vm.langID = true;
        //vm.totalData = 0;
        vm.show = 0;
        function initialize() {
            $translatePartialLoader.addPart('vendor-balance');
            //vm.totalData = 0;
            if (localStorage.getItem('currLang').toLowerCase() != 'id') {
                vm.langID = false;
            }
            UIControlService.loadLoading();
            loadCurrency();
            getVendorNation();

            loadVendor();
            jLoad(1);
            //loadCheckCR();
            //loadUnit();
            checkIsVerified();
            cekPrakualifikasiVendor();
            cekCR();
            loadVendorVerificationReview();
        }

        function loadVendorVerificationReview() {
            UIControlService.loadLoading()
            VerifiedSendService.getVendorVerificationReview({
                MenuID: 1038
            }, function (reply) {
                UIControlService.unloadLoading();

                if (reply.status == 200) {
                    var data = reply.data;
                    vm.vendorReviewData = data;
                    //console.log(data)
                    if (data != null) {
                        if (data.ReviewStatus == 4526) {
                            vm.revisiVendor = true;
                        } else {
                            vm.revisiVendor = false;
                        }
                        if (vm.langID) {
                            vm.revisiName = data.Locale_Id;
                        } else {
                            vm.revisiName = data.Locale_En;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            })
        }

        function getVendorNation() {
            AuthService.getRoleUserLogin({ Keyword: vm.username }, function (reply) {
                if (reply.status === 200 && reply.data.List.length > 0) {
                    var data = reply.data.List;
                    if (data[0].RoleName == "APPLICATION.ROLE_VENDOR_INTERNATIONAL") {
                        vm.menuIndex = 2;
                    }
                    //console.log(reply.data.List)
                } else {
                   
                }
            }, function (err1) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }


        function cekCR() {
            BalanceVendorService.cekCR(function (reply) {
                if (reply.status === 200) {
                    vm.isCR = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
        function cekPrakualifikasiVendor() {
            BalanceVendorService.cekPrakualifikasiVendor(function (reply) {
                UIControlService.loadLoadingModal();
                if (reply.status === 200) {
                    vm.pqWarning = reply.data;
                    //console.info("isregistered" + vm.pqWarning);
                    UIControlService.unloadLoadingModal();
                } else {
                    UIControlService.unloadLoadingModal();
                    UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
            });
        }

        function loadCheckCR() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            BalanceVendorService.getCRbyVendor({ CRName: 'OC_VENDORBALANCE'},function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //console.info("checkCR:" + JSON.stringify(reply));
                    vm.CR = reply.data;
                    if (reply.data[0]==true) {
                        vm.IsApprovedCR = true;
                    } else {
                        vm.IsApprovedCR = false;
                    }

                    if (vm.vendorReviewData != null && reply.data[1] != "Approved_CR") {
                        if (vm.revisiVendor) {
                            vm.IsApprovedCR = true;
                        } else {
                            vm.IsApprovedCR = false;
                        }
                    }
                    
                    //console.info("sentcr" + JSON.stringify(vm.isSentCR));
                    //console.info("isApprove?" + JSON.stringify(vm.isApprovedCR));
                   // changePermission();
                }

            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function loadBalanceDocUrl() {
            BalanceVendorService.balanceDocUrl(function (reply) {
                if (reply.status === 200) {
                    if (reply.data != null) {
                        vm.balanceDocUrl = reply.data.DocUrl;
                        if (vm.totalData > 0) {
                            $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
                            vm.show = 0;
                        }
                    }
                    else {
                        $rootScope.menus[vm.menuIndex].IsChecked = '';
                        vm.show = 1;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
            });
        }

        function checkIsVerified() {
            BalanceVendorService.isVerified(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //console.info("check"+JSON.stringify(reply));
                    var data = reply.data;
                    vm.vendorId = reply.data.VendorID;
                    vm.verified = data.Isverified;
                    //console.info("isver" + JSON.stringify(vm.verified));
                    //if (vm.verified === 1) {
                        //loadCheckCR();
                   // }
                   // changePermission();

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function changePermission() {
            if (vm.verified !== null || (vm.verified === 1 && vm.isApprovedCR === true)) {
                vm.changeDataPermission = true;
            }
            //if (vm.isSentCR===true && vm.isApprovedCR===fal)
        }
        vm.loadVendor = loadVendor;
        function loadVendor() {
            BalanceVendorService.selectVendor(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.Vendor = reply.data;
                    //console.info(vm.Vendor.VerifiedSendDate);
                    if (vm.Vendor.VerifiedSendDate === null && vm.Vendor.VerifiedDate===null) {
                        vm.IsApprovedCR = true;
                        if (vm.vendorReviewData != null) {
                            if (vm.revisiVendor) {
                                vm.IsApprovedCR = true;
                            } else {
                                vm.IsApprovedCR = false;
                            }
                        }
                    }
                    else {
                        loadCheckCR();
                    }
                    //console.info("vendor" + JSON.stringify(vm.isApprovedCR));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }


        vm.jLoad = jLoad;
        function jLoad(current) {
            vm.asset = 0;
            vm.hutang = 0;
            vm.modal = 0;
            vm.totalData = 0;
            //console.info("curr "+current)
            vm.vendorbalance = [];
            vm.currentPage = current;
            BalanceVendorService.select({VendorID: 0 }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //loadCurrency();
                    vm.vendorbalance = reply.data;
                    //console.info("curr " + JSON.stringify(vm.vendorbalance));

                    for (var i = 0; i < vm.vendorbalance.length; i++) {
                        if (vm.vendorbalance[i].listDataBalanceModel != null) {
                            vm.totalData++;
                            vm.currID = vm.vendorbalance[0].listDataBalanceModel[0].subCategory[0].CurrencyID;
                            loadCurrency(vm.currID);
                            if (vm.vendorbalance[i].BalanceName == "WEALTH_TYPE_ASSET") {
                                for (var c = 0; c < vm.vendorbalance[i].listDataBalanceModel.length; c++) {
                                    for (var g = 0; g < vm.vendorbalance[i].listDataBalanceModel[c].subCategory.length; g++) {
                                        vm.asset += Number(vm.vendorbalance[i].listDataBalanceModel[c].subCategory[g].Nominal);
                                    }
                                }
                            }
                            if (vm.vendorbalance[i].BalanceName == "WEALTH_TYPE_DEBTH") {
                                for (var c = 0; c < vm.vendorbalance[i].listDataBalanceModel.length; c++) {
                                    for (var g = 0; g < vm.vendorbalance[i].listDataBalanceModel[c].subCategory.length; g++) {
                                        vm.hutang += Number(vm.vendorbalance[i].listDataBalanceModel[c].subCategory[g].Nominal);
                                    }
                                }
                            }
                        }
                    }
                    if (vm.totalData == 0) {
                        vm.show = 1;
                        $rootScope.menus[vm.menuIndex].IsChecked = '';
                    }

                    loadBalanceDocUrl();
                    UIControlService.loadLoading();

                    BalanceVendorService.getSisaMstBalance(
                    { assetBalance: vm.asset, hutangBalance: vm.hutang },
                    function (reply) {
                        if (reply.status == 200) {
                            var data = reply.data;
                            var dataAsset = data[0];
                            var dataHutang = data[1];

                            for (var i = 0; i < dataAsset.length; i++) {
                                vm.vendorbalance.push(dataAsset[i]);
                            }
                            for (var i = 0; i < dataHutang.length; i++) {
                                vm.vendorbalance.push(dataHutang[i]);
                            }
                            UIControlService.unloadLoading();

                            //console.log(data)
                        }
                    }, function (err) {
                        $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                        UIControlService.unloadLoading();
                    })

                    vm.modal = +vm.asset - +vm.hutang;
                                
                    //console.info(JSON.stringify(vm.modal));
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data Neraca Perusahaan" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.ubah_aktif = ubah_aktif;
        function ubah_aktif(data, active) {
            bootbox.confirm({
                message: '<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-close"></i> ' + $filter('translate')('BTN.CANCEL')
                    },
                    confirm: {
                        label: '<i class="fa fa-save"></i> OK'
                    }
                },
                callback: function (result) {
                    if (result) {
                        UIControlService.loadLoading("MESSAGE.LOADING");
                        //console.info("ada:"+JSON.stringify(data))
                        BalanceVendorService.editActive({
                            BalanceID: data.BalanceID,
                            IsActive: active,
                            isApprovedCR: vm.isApprovedCR
                        }, function (reply) {
                            UIControlService.unloadLoading();
                            if (reply.status === 200) {
                                var msg = "";
                                //if (active === false) msg = "Non Aktifkan ";
                                //if (active === true) msg = "Aktifkan ";
                                UIControlService.msg_growl("success", "MESSAGE.NON_AKTIF");
                                initialize();
                                //if (vm.totalData == 0 || vm.balanceDocUrl == "") {
                                //    $rootScope.menus[vm.menuIndex].IsChecked = '';
                                //    vm.show = 1;
                                //}

                            }
                            else {
                                UIControlService.msg_growl("error", "Gagal menonaktifkan data ");
                                return;
                            }
                        }, function (err) {

                            UIControlService.msg_growl("error", "Gagal Akses API ");
                            UIControlService.unloadLoading();
                        });
                    }
                }
            });
            

            

        }

        vm.tambah = tambah;
        function tambah(vendorBalance) {
            //console.info("masuk form add/edit");
            var data = {
                act: true,
                VendorID: vm.vendorId,
                VendorBalance: vendorBalance
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/neraca/formNeraca.html',
                backdrop:"static",
                controller: 'formNeracaCtrl',
                controllerAs: 'formNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                // window.location.reload();
                initialize();
                //if (vm.balanceDocUrl != "") {
                //    $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
                //    vm.show = 0;
                //}
            });
        }

        vm.edit = edit;
        function edit(data, flag) {
            //console.info("masuk form add/edit");
            if (flag == 1) {
                var data = {
                    act: false,
                    item: data,
                    VendorID: vm.vendorId,
                    VendorBalance: vm.vendorbalance
                }
            }
            if (flag != 1) {

                var data = {
                    act: false,
                    item:
                    {
                        BalanceID: flag.BalanceID,
                        Wealth: data,
                        COA: flag.COAType,
                        Unit: flag.Unit,
                        Amount: flag.Amount,
                        DocUrl: flag.DocUrl,
                        Nominal: flag.nominal
                    },
                    VendorID: vm.vendorId
                }
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/neraca/formNeraca.html',
                controller: 'formNeracaCtrl',
                controllerAs: 'formNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.jLoad(1);
            });
        }

        vm.upload = upload;
        function upload() {
            //console.info("masuk form add/edit");
            var data = {
                act: true,
                VendorID: vm.vendorId,
                BalanceDocUrl: vm.balanceDocUrl != "" ? vm.balanceDocUrl : ""
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/neraca/formUploadNeraca.html',
                controller: 'frmUploadNeracaCtrl',
                controllerAs: 'frmUploadNeracaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                initialize();
                //if (vm.totalData > 0) {
                //    $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
                //    vm.show = 0;
                //}
            });
        }

        vm.revisiVendorDetail = revisiVendorDetail;
        function revisiVendorDetail() {
            var data = {
                data: vm.vendorReviewData,
                langID : vm.langID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/revisi-modal/revisi-modal.html',
                controller: 'revisiModalController',
                controllerAs: 'revisiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                
            });
        }
        function loadCurrency(id) {
            BalanceVendorService.getCurrencies(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.currencyList = reply.data;
                    if (id != undefined) {
                        for (var i = 0; i < vm.currencyList.length; i++) {
                            if (vm.currencyList[i].CurrencyID == id) {
                                vm.labelCurr = vm.currencyList[i].Symbol;
                                break;
                            }
                        }
                    }
                } else {
                    $.growl.error({ message: "MESSAGE.FAIL_GET_DATA" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                //console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.getCurrName = getCurrName;
        function getCurrName(currID) {
            for (var i = 0; i < vm.currencyList.length; i++) {
                if (vm.currencyList[i].CurrencyID == currID) {
                    return vm.currencyList[i].Symbol;
                    break;
                }
            }
        }
    }
})();
