﻿(function () {
    'use strict';

    angular.module("app").controller("PilihGolonganBidangUsahaModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'item', '$uibModal', 'IzinUsahaService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService', 'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService', 'FileSaver', 'VendorRegistrationService'];

    function ctrl($http, $translate, $translatePartialLoader, $location, UIControlService, item, $uibModal, IzinUsahaService, $uibModalInstance, UploadFileConfigService, UploaderService, AuthService, $filter, ProvinsiService, GlobalConstantService, FileSaver, VendorRegistrationService) {
        var vm = this;
        vm.data = [];
        vm.dataSelected = item.dataBidangUsaha;

        vm.init = init;
        function init() {
            UIControlService.loadLoadingModal();
            IzinUsahaService.getMstBussinessFieldType(
            {},
            function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.data = data;
                } else {
                    UIControlService.msg_growl('error', "ERRORS.ERROR_API");

                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl('error', "ERRORS.ERROR_API");

            })
        }
       
        vm.pilihBidangUsaha = pilihBidangUsaha;
        function pilihBidangUsaha(data) {

            var data = {
                item: data,
                dataSelected: vm.dataSelected,
                jenisIzinUsaha: item.jenisIzinUsaha
            };

            var modalInstance = $uibModal.open({
                templateUrl: "app/modules/rekanan/data-perusahaan/izin-usaha/pilihBidangUsahaTree.modal.html",
                controller: 'PilihBidangUsahaTreeModalController',
                controllerAs: 'PilihBidangUsahaTreeModalCtrl',
                backdrop:'static',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (reply) {
                console.log(reply)
                vm.dataSelected = reply;
                $uibModalInstance.close(vm.dataSelected);
            });
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();