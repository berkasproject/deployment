(function () {
    'use strict';

    angular.module("app").controller("IzinUsahaController", ctrl);

    ctrl.$inject = ['$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'IzinUsahaService', 'AuthService', 'UIControlService', '$filter', '$rootScope', '$q', 'VendorRegistrationService','VerifiedSendService'];
    /* @ngInject */
    function ctrl($http, $uibModal, $translate, $translatePartialLoader, $location, IzinUsahaService, AuthService, UIControlService, $filter, $rootScope, $q, VendorRegistrationService, VerifiedSendService) {
        var vm = this;

        vm.listLicensi = [];
        vm.isChangeData = false;
        vm.IsApprovedCR = false;
        vm.menuIndex = 2;
        vm.vendorCategoryID;
        vm.revisiVendor = false;//false
        vm.langID = true;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('data-izinusaha');
            if (localStorage.getItem('currLang').toLowerCase() != 'id') {
                vm.langID = false;
            }
            cekCR();
            getVendorCategoryID().then(function (reply) {
                jLoad();
            });
            loadVendorVerificationReview().then(function () {
                chekcIsVerified();
            });
        }

        function loadVendorVerificationReview() {
            var defer = $q.defer();
            VerifiedSendService.getVendorVerificationReview({
                MenuID: 1035
            }, function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.vendorReviewData = data;
                    //console.log(data)
                    if (data != null) {
                        if (data.ReviewStatus == 4526) {
                            vm.revisiVendor = true;
                        } else {
                            vm.revisiVendor = false;
                        }
                        if (vm.langID) {
                            vm.revisiName = data.Locale_Id;
                        } else {
                            vm.revisiName = data.Locale_En;
                        }
                    }
                    defer.resolve(true)
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                defer.reject;
                return;
            })
            return defer.promise;
        }
        
        function getVendorCategoryID() {
            var defer = $q.defer();
            AuthService.getVendorCategoryID({},function (reply) {
                if (reply.status == 200) {
                    var data = reply.data;
                    vm.vendorCategoryID = data.VendorCategoryID;
                    vm.supplierID = data.SupplierID;
                    vm.VendorTypeID = data.VendorTypeID;
                    vm.CIVDID = data.CIVDID;
                    vm.VendorID = data.VendorID;
                    //vm.CompanyScale = data.CompanyScale;

                    defer.resolve(true);
                } else {
                    defer.reject(false);
                }
            }, function (err) {
                defer.reject(false);
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            })
            return defer.promise;
        }

        function chekcIsVerified() {
            IzinUsahaService.getCRbyVendor({ CRName: 'OC_VENDORLICENSI' }, function (reply) {
                if (reply.status === 200) {
                    vm.CR = reply.data;
                    if (reply.data[0]) {
                        vm.IsApprovedCR = true;
                    } else {
                        vm.IsApprovedCR = false;   
                    }

                    if (vm.vendorReviewData != null && reply.data[1] != "Approved_CR") {
                        if (vm.revisiVendor) {
                            vm.IsApprovedCR = true;
                        } else {
                            vm.IsApprovedCR = false;
                        }
                    }

                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function cekCR() {
            IzinUsahaService.cekCR(function (reply) {
                if (reply.status === 200) {
                    vm.isCR = reply.data;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        function checkCR() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            IzinUsahaService.getCRbyVendor({ CRName: 'OC_VENDORLICENSI' }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    //console.info("CR:" + JSON.stringify(reply.data));
                    //vm.CR = reply.data.length;
                    if (reply.data.length > 0) {
                        //if (reply.data === true) {
                        vm.IsApprovedCR = true;
                        // }
                        /*
                    else {
                        vm.isSentCR = false;
                    }*/
                    }
                    //console.info(JSON.stringify(vm.IsApprovedCR));
                }

            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            IzinUsahaService.selectLicensi({ VendorCategoryID: vm.vendorCategoryID, SupplierID: vm.supplierID, VendorTypeID: vm.VendorTypeID }, function (response) {
                if (response.status == 200) {
                    var list = response.data;
                    var verify = false;
                    if (vm.vendorCategoryID == 33) {
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].IsVendorCommodityExist) {
                                verify = true;
                            }
                        }
                    } else {
                        //if (list.length == 0) {
                        //    verify = false;
                        //}
                    }
                    if ($rootScope.menus != undefined) {
                        if (verify) {
                            $rootScope.menus[vm.menuIndex].IsChecked = 'fa-check';
                        } else {
                            $rootScope.menus[vm.menuIndex].IsChecked = '';
                        }
                    }
                    vm.listLicensi = list;
                    checkExpiredDate(vm.listLicensi);
                    loadCityCompany();
                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    UIControlService.unloadLoading();
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }


        function loadCityCompany() {
            UIControlService.loadLoading();

            IzinUsahaService.selectcontact({ VendorID: vm.VendorID }, function (reply) {
                if (reply.status == 200) {
                    UIControlService.unloadLoading();
                    vm.contactCompany = reply.data;
                    for (var i = 0; i < vm.contactCompany.length; i++) {
                        if (vm.contactCompany[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            //console.info("kontak" + JSON.stringify(vm.contactCompany[i].Contact.Address.State.Country.CountryID));
                            vm.cityID = vm.contactCompany[i].Contact.Address.State.Country.CountryID;
                            break
                        }
                    }

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }


        function checkExpiredDate(listLicensi) {
            //console.info("masuk");
            var today = moment().format("YYYY-MM-DD");
            //console.info(JSON.stringify(today));
            //console.info(today);
            vm.dateA = []; vm.dateB = []; vm.dateC = [];
            for (var i = 0; i < listLicensi.length; i++) {
                vm.dateA[i] = moment(listLicensi[i].ExpiredDate).subtract(90, 'days').format("YYYY-MM-DD");
                vm.dateB[i] = moment(listLicensi[i].ExpiredDate).subtract(60, 'days').format("YYYY-MM-DD");
                vm.dateC[i] = moment(listLicensi[i].ExpiredDate).subtract(30, 'days').format("YYYY-MM-DD");
                //vm.coba = moment(listLicensi[0].ExpiredDate).subtract(30, 'days').format("YYYY-MM-DD");
                //console.info(JSON.stringify(vm.coba));
                if (vm.dateA[i] === today) {
                    //console.info("hariini");
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 90;
                } else if (vm.dateB[i] === today) {
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 60;
                } else if (vm.dateC[i] === today) {
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 30;
                } else if (listLicensi[i].ExpiredDate === today) {
                    loadEmailCompany();
                    vm.LicenseName = listLicensi[i].LicenseName;
                    vm.days = 0;
                }
            }
        }

        //load email vendor
        vm.loadEmailCompany = loadEmailCompany;
        function loadEmailCompany() {
            //console.info("kirimemail");
            IzinUsahaService.selectcontact({ VendorID: vm.VendorID }, function (reply) {
                if (reply.status == 200) {
                    vm.contact = reply.data;
                    vm.listEmail = [];
                    for (var i = 0; i < vm.contact.length; i++) {
                        if (vm.contact[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            vm.listEmail.push(vm.contact[i].Contact.Email);
                        }
                    }
                    //console.info("list email" + JSON.stringify(vm.listEmail));
                    sendMail(vm.listEmail);
                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }


        vm.sendMail = sendMail;
        function sendMail(listEmail) {
            //console.info("kirimemail");
            var email = {
                subject: 'Notifikasi Ijin Usaha',
                mailContent: 'Kurang ' + vm.days + ' hari lagi ijin usaha ' + vm.LicenseName + ' akan kadaluarsa. Terima kasih.',
                isHtml: false,
                addresses: listEmail
            };
            //console.info("kirimemail");
            // UIControlService.loadLoading("LOADERS.LOADING_SEND_EMAIL");
            IzinUsahaService.sendMail(email, function (response) {
                // UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.days = ""; vm.LicenseName = "";
                    UIControlService.msg_growl("notice", "MESSAGE.SENT_EMAIL")
                } else {
                    UIControlService.handleRequestError(response.data);
                }
            }, function (response) {
                UIControlService.handleRequestError(response.data);
                UIControlService.unloadLoading();
            });
        }

        vm.deleteLic = deleteLic;
        function deleteLic(lic) {
            vm.lic = lic;
            bootbox.confirm({
		        message: '<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>',
		        buttons: {
		            cancel: {
		                label: '<i class="fa fa-close"></i> ' + $filter('translate')('Batal')
		            },
		            confirm: {
		                label: '<i class="fa fa-save"></i> OK'
		            }
		        },
		        callback: function (reply) {
                if (reply) {
                    UIControlService.loadLoading("DELETING");
                    IzinUsahaService.deleteLic({ LicenseID: lic.LicenseID, VendorID: lic.VendorID, VendorLicenseID: lic.VendorLicenseID }, function (reply) {
                        if (reply.status == 200) {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("success", "MESSAGE.DELETE_SUCCESS");
                            //window.location.reload();
                            vm.init();
                        } else {
                            UIControlService.unloadLoading();
                            UIControlService.msg_growl("error", "MESSAGE.DELETE_FAILED");
                            return;
                        }
                    }, function (err) {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", "MESSAGE.DELETE_FAILED");
                        return;
                    });
                }
            }});
        }

        //open form
        vm.openForm = openForm;
        function openForm(data, isForm) {
            var data = {
                item: data,
                isForm: isForm,
                cityID: vm.cityID,
                VendorCategoryID: vm.vendorCategoryID,
                VendorID: vm.VendorID
            }
            var temp;
            if (isForm === true) {
                temp = "app/modules/rekanan/data-perusahaan/izin-usaha/form-izin-usaha.html";
            } else {
                temp = "app/modules/rekanan/data-perusahaan/izin-usaha/detail-izin-usaha.html";
            }
            var modalInstance = $uibModal.open({
                templateUrl: temp,
                controller: 'FormIzinCtrl',
                controllerAs: 'FormIzinCtrl',
                backdrop:"static",
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                //window.location.reload();
                init();
            });
        }

        

        vm.simpanIzinUsahaCivd = simpanIzinUsahaCivd;
        function simpanIzinUsahaCivd() {
            UIControlService.loadLoading();

            getIzinUsahaCivd().then(function (reply) {
                var data = reply;
                var kirimSimpan = [];

                for (var i = 0; i < data.length; i++) {
                    kirimSimpan.push({
                        LicenseName: data[i].jenisIzinUsaha,
                        LicenseNo: data[i].noIzinUsaha,
                        IssuedDate: data[i].mulaiBerlaku,
                        ExpiredDate: data[i].ExpiredDate,
                        IssuedBy: data[i].instansiPemberiIzin,
                        DocumentUrl: data[i].fileIzinUsaha,
                        Remark: data[i].jenisIzinUsaha == 'others' ? data[i].others : data[i].bidangUsaha,
                        CIVDLicenseID: data[i].id,
                        GolonganUsaha: data[i].golonganUsaha
                    });
                }

                IzinUsahaService.simpanIzinUsahaCivd({ dataVendorLicense: kirimSimpan }, function (reply) {
                    if (reply.status == 200) {
                        getListVendorCIVD(vm.CIVDID).then(function (reply) {
                            UIControlService.unloadLoading();

                            var data = reply[0];
                            if (data.situEndDate != null && data.situFile != null) {
                                var kirimSimpanFromList = [{
                                    LicenseID: 13,
                                    LicenseNo: data.situ,
                                    IssuedDate: data.situStartDate,
                                    ExpiredDate: data.situEndDate,
                                    DocumentUrl: data.situFile
                                }];

                                IzinUsahaService.simpanIzinUsahaCivdFromList({ dataVendorLicense: kirimSimpanFromList }, function (reply) {
                                    if (reply.status == 200) {
                                        vm.init()
                                    } else {
                                        UIControlService.unloadLoading();
                                        UIControlService.msg_growl("error", "MESSAGE.CIVD_INSERT_FAIL");
                                        return;
                                    }
                                }, function (err) {
                                    UIControlService.unloadLoading();
                                    UIControlService.msg_growl("error", "MESSAGE.CIVD_INSERT_FAIL");
                                    return;
                                })
                            }
                        })
                    } else {
                        UIControlService.unloadLoading();
                        UIControlService.msg_growl("error", "MESSAGE.CIVD_INSERT_FAIL");
                        return;
                    }
                }, function (err) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", "MESSAGE.CIVD_INSERT_FAIL");
                    return;
                });

            })
        }

        function getIzinUsahaCivd() {
            var defer = $q.defer();
            var url = "https://apiprovider.civd-migas.com/vendor/izinUsaha?vendorId=" + vm.CIVDID;

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply.result)
                } else {
                    UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                    defer.reject(false);
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject(false);
            });
            return defer.promise;
        }

        function getListVendorCIVD(CIVDID) {
            var defer = $q.defer();
            var url = "https://apiprovider.civd-migas.com/vendor/allVendor?vendorId=" + CIVDID;

            VendorRegistrationService.getCIVDData({
                Keyword: url
            }, function (response) {
                UIControlService.unloadLoading();
                var reply = JSON.parse(response.data);
                if (reply.status === 200) {
                    defer.resolve(reply.result);
                } else {
                    defer.reject(false)
                }
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");
                defer.reject();
            });

            return defer.promise;
        }

        vm.revisiVendorDetail = revisiVendorDetail;
        function revisiVendorDetail() {
            var data = {
                data: vm.vendorReviewData,
                langID: vm.langID
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/revisi-modal/revisi-modal.html',
                controller: 'revisiModalController',
                controllerAs: 'revisiModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

    }
})();