﻿(function () {
	'use strict';

	angular.module("app").controller("FormIzinCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'item', '$uibModal', 'IzinUsahaService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService', 'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService','FileSaver','VendorRegistrationService','$q'];

	function ctrl($http, $translate, $translatePartialLoader, $location, UIControlService, item, $uibModal, IzinUsahaService, $uibModalInstance, UploadFileConfigService, UploaderService, AuthService, $filter, ProvinsiService, GlobalConstantService, FileSaver, VendorRegistrationService,$q) {
		var vm = this;
		//console.info(JSON.stringify(item));
		console.log(item);
		
		vm.isCalendarOpened = [false, false, false, false];
		vm.pathFile;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.mindate = new Date();
		vm.cityID = item.cityID;
		vm.tglSekarang = new Date();
		vm.VendorID = item.VendorID;
		vm.dataBidangUsahaCount = 0;

		//console.log(item.data)

		var dataModal = item.item;

		vm.isEdit = true;
		if(dataModal.LicenseNo == undefined){
		    vm.isEdit = false;
		}
		
		vm.uiControl = UIControlService;

		vm.dataSelectIzinUsaha = [];
		vm.noJenisIzinUsaha = dataModal.LicenseNo == undefined ? '' : dataModal.LicenseNo;
		vm.isDobCalendarOpened = false;
		vm.dataSelectProvinsi = [];
		vm.currency = 'Rp';
		vm.dataBidangUsaha = [];
		vm.tipeLicense = dataModal.LicenseType == undefined ? '' : dataModal.LicenseType;
		vm.provinsi = dataModal.IssuedState == undefined ? '' : dataModal.IssuedState;
		vm.instansiPemberi = dataModal.IssuedBy == undefined ? '' : dataModal.IssuedBy;
		vm.kualifikasi = dataModal.LicenseQualification == undefined ? '' : dataModal.LicenseQualification;
		//vm.city = dataModal.IssuedLocation == undefined ? '' : dataModal.IssuedLocation;
		vm.CapitalAmount = dataModal.CapitalAmount == undefined ? '' : dataModal.CapitalAmount;
		vm.IssuedDate = dataModal.IssuedDate == undefined ? '' : new Date(UIControlService.convertDateForDatepicker(dataModal.IssuedDate));
		vm.DocUrl = dataModal.DocumentURL == undefined ? '' : dataModal.DocumentURL;
		//vm.IssuedDate = data.IssuedDate == undefined ? '' : UIControlService.convertDateForDatepicker(data.IssuedDate);
		vm.ExpiredDate = dataModal.ExpiredDate == undefined ? '' : new Date(UIControlService.convertDateForDatepicker(dataModal.ExpiredDate));
		vm.jenisIzinUsaha = '';
		vm.keterangan = dataModal.Remark == undefined ? '' : dataModal.Remark;
		vm.datasimpan = {};
		vm.VendorLicenseID = dataModal.VendorLicenseID == undefined ? null : dataModal.VendorLicenseID;


		vm.init = init;
		function init() {
		    if (localStorage.getItem("currLang").toLowerCase() == 'id') {
		        vm.btnBack = vm.isEdit ? 'Batal' : 'Kembali';
		    }
		    if (localStorage.getItem("currLang").toLowerCase() == 'en') {
		        vm.btnBack = vm.isEdit ? 'Cancel' : 'Back';
		    }
			$translatePartialLoader.addPart("data-izinusaha");
			loadKlasifikasi();
			getTypeSizeFile();
			changeCountry(vm.cityID);

			if (vm.isEdit) {
			    getVendorCommodity(1);
			}

			if (vm.tipeLicense != '') {
			    changeJenisIzin();
			}
            // Suatu saat di pakai
			//getCityByID(vm.dataLicensi.IssuedLocation);

		}

		function getVendorCommodity(current) {
		    //vm.currentPage = current;
		    //var offset = (current * 10) - 10;
            //,
		    //Offset: offset,
		    //Limit: vm.pageSize
		    IzinUsahaService.getVendorCommodityBy(
                {
                    IntParam1: vm.VendorLicenseID,
                    IntParam2: dataModal.LicenseID
                },
                function (reply) {
		        UIControlService.unloadLoading();
		        var data = reply.data.List;
		        vm.dataBidangUsaha = data;
		        var count = 0;
		        for (var i = 0; i < vm.dataBidangUsaha.length; i++) {
		            count +=1;
		            vm.dataBidangUsaha[i].No = count;
		        }
		        vm.dataBidangUsahaCount = data.length;
                console.log(data)
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		    });
		}

		/*get username*/
		function getUsLogin() {
			AuthService.getUserLogin(function (reply) {
				vm.VendorLogin = reply.data.CurrentUsername;
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});
		}

		vm.cekDate = cekDate;
		function cekDate(flag) {
		    vm.flag = flag;
		    if (flag == 1) {
		        if (vm.IssuedDate === undefined) {
		            UIControlService.msg_growl("error", 'MESSAGE.STARTDATE_INVALID');
		            vm.IssuedDate = '';
		            return;
		        }
		    } else {
		        if (vm.ExpiredDate === undefined) {
		            UIControlService.msg_growl("error", 'MESSAGE.EXPIREDDATE_INVALID');
		            vm.ExpiredDate = '';
		            return;
		        }
		    }
		    IzinUsahaService.cekDate(
                {
                    Status: flag,
                    Date1: UIControlService.getStrDate(vm.IssuedDate),
                    Date2: UIControlService.getStrDate(vm.ExpiredDate)
                },
               function (response) {
                   if (flag == 1 && response.data == 0) {
                       UIControlService.msg_growl("error", 'MESSAGE.STARTDATE_VALID');
                       vm.IssuedDate = null;
                       return;
                   }
                   else if (flag == 2 && response.data == 0) {
                       UIControlService.msg_growl("error", 'MESSAGE.DATE_VALID');
                       vm.ExpiredDate = null;
                       return;
                   }
                   else if (flag == 1 && response.data == 1) {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_ISSUEDDATE');
                        
                       return;
                   }
                   else if (flag == 2 && response.data == 1) {
                       UIControlService.msg_growl("error", 'MESSAGE.ERR_DATE');
                       vm.ExpiredDate = null;
                       return;
                   }
               },
           function (response) {
               UIControlService.msg_growl("error", "MESSAGE.API");
               return;
           });
		}




		/*get city by id*/
		function getCityByID(id) {
			ProvinsiService.getCityByID({ column: id }, function (reply) {
				UIControlService.unloadLoading();
				var data = reply.data.List[0];
				//console.info(data);
				vm.selectedCities = data;
				vm.selectedState = data.State;
				changeState(data.State.StateID);
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		/*open form date*/
		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		/*get combo klasifikasi*/
		vm.listClasification = [];
		vm.selectedClasification;
		function loadKlasifikasi() {
			//alert("load");
			IzinUsahaService.getClasification(function (reply) {
				UIControlService.unloadLoading();
				vm.dataClass = reply.data.List;
				for (var i = 0; i < vm.dataClass.length; i++) {
				    if (vm.dataClass[i].Name != "ALL_COMPANY") {
				        vm.listClasification.push(vm.dataClass[i]);

				        if (vm.kualifikasi != '') {
				            if(vm.dataClass[i].RefID == vm.kualifikasi){
				                vm.CompanyScaleName = vm.dataClass[i].Value;
				            }
				        }
				      
				    }
				}
				    
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}

		/* combo country, state, city */
		vm.changeCountry = changeCountry;
		vm.listState = [];
		vm.selectedState;
		function changeCountry(idstate) {
		    if (idstate != undefined) {
		        ProvinsiService.getStates(idstate,
               function (response) {
                   vm.listState = response.data;

                   if (dataModal.IssuedState != undefined) {
                       changeState();

                       for (var i = 0; i < vm.listState.length; i++) {
                           if (vm.listState[i].StateID == vm.provinsi) {
                               vm.provinsiName = vm.listState[i].Name;
                           }
                       }
                   }
                   },
               function (response) {
                   UIControlService.msg_growl("error", "MESSAGE.API");
                   return;
               });
		    }
			
		}

		vm.changeState = changeState;
		vm.listCities = [];
		vm.selectedCities;
		function changeState() {
            vm.city = '';
		    if (vm.provinsi != undefined) {
		        UIControlService.loadLoadingModal();
		        ProvinsiService.getCities(vm.provinsi, function (response) {
		            UIControlService.unloadLoadingModal();
		            vm.listCities = response.data;
		            if (vm.isEdit) {
		                vm.city = dataModal.IssuedLocation == undefined ? '' : dataModal.IssuedLocation;
		            }

		            if (vm.city != '') {
		                for (var i = 0; i < vm.listCities.length; i++) {
		                    if (vm.listCities[i].CityID == vm.city) {
		                        vm.cityName = vm.listCities[i].Name;
		                    }
		                }
		            }
		        }, function (response) {
		            UIControlService.unloadLoadingModal();
		            UIControlService.msg_growl("error", "MESSAGE.API");
		            return;
		        });
		    }
		    
		}


		vm.changeCities = changeCities;
		function changeCities() {
			vm.IssuedLocation = vm.selectedCities.CityID;
		}
		/* end combo country, state, city*/

		/*get type n size file upload*/
		vm.selectUpload = selectUpload;
		vm.fileUpload;
		function selectUpload() {
			//console.info(">" + vm.fileUpload);
			//vm.fileUpload = vm.fileUpload;
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.VENDOR.LICENSI", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];
					//console.info("file:" + JSON.stringify(response));

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}

		//simpan
		vm.savedata = savedata;
		function savedata() {
		    //console.info(vm.dataLicensi.CapitalAmount);
            cekLicenseAda().then(function (response) {
                if (!response) {
                    if (validateField()) {
                        UIControlService.loadLoadingModal();
                        if (!(vm.fileUpload === undefined || vm.fileUpload === '')) {
                            uploadFile();
                        } else {
                            if (!vm.DocUrl) {
                                UIControlService.unloadLoadingModal();
                                UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                                return;
                            }
                            vm.datasimpan['DocumentURL'] = vm.DocUrl;
                            //console.info("1-"+JSON.stringify(vm.datasimpan));
                            saveprocess();
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.LICENSE_EXIST");
                }
            })
		    
		    
		        

		    
		    
		}

		function cekLicenseAda() {
		    var defer = $q.defer();
		    if (vm.jenisIzinUsaha.ID == dataModal.LicenseID) {
		        defer.resolve(false);
		        return defer.promise;
		    } 
		    UIControlService.loadLoadingModal();
		    IzinUsahaService.CekLicenseInVendorLicense(
                {
                    IntParam1: vm.jenisIzinUsaha.ID,
                    IntParam2: vm.VendorID
                },
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    var data = reply.data;
               
                    if (data == 0) {
                        defer.resolve(false);

                    } else {
                        defer.resolve(true);
                    }
                }, function (err) {
                    UIControlService.msg_growl("error", "MESSAGE.API");
                    UIControlService.unloadLoadingModal();
                    defer.reject();
                });
		    return defer.promise;
		}

		function validateField() {
		    //if (vm.LicenseNo == "") {
		    //    var lcnsname = vm.licensiname.substring(0, 3) == "NIB" ? vm.licensiname.substring(0, 9) : vm.licensiname.substring(0, 4);
		    //    localStorage.getItem("currLang").toLowerCase() == "id" ? UIControlService.msg_growl("error", "Nomor " + lcnsname + " belum diisi") :
		    //    UIControlService.msg_growl("error", lcnsname + " number was not filled");
		    //    return false;
		    //}
		    if (vm.tipeLicense == '') {
		        UIControlService.msg_growl("error", "MESSAGE.TIPE_LICENSE_EMPTY");
		        return false;
		    }

		    if (vm.jenisIzinUsaha == '') {
		        UIControlService.msg_growl("error", "MESSAGE.JENIS_IZIN_USAHA");
		        return false;
		    }

		    if (vm.noJenisIzinUsaha == '') {
		        UIControlService.msg_growl("error", "MESSAGE.JENIS_IZIN_USAHA");
		        return false;
		    }

		    if (vm.IssuedDate === "") {
		        UIControlService.msg_growl("error", "MESSAGE.ISSUED_DATE");
		        return false;
		    }

		    if (vm.jenisIzinUsaha.IsEndDate && vm.ExpiredDate === "") {
		        UIControlService.msg_growl("error", "MESSAGE.EXPIRED_DATE");
		        return false;
		    }

		    if (vm.instansiPemberi == "") {
		        UIControlService.msg_growl("error", "MESSAGE.INSTANSI_PEMBERI");
		        return false;
		    }

		    if (vm.provinsi == "") {
		        UIControlService.msg_growl("error", "MESSAGE.STATE");
		        return false;
		    }

		    if (vm.city == "") {
		        UIControlService.msg_growl("error", "MESSAGE.NO_CITY");
		        return false;
		    }


		    if (vm.jenisIzinUsaha.Name === 'SIUP' && vm.kualifikasi === "") {
		        UIControlService.msg_growl("error", "MESSAGE.KUALIFIKASI");
		        return false;
		    }

		    if (vm.jenisIzinUsaha.Name === 'SIUP' && vm.CapitalAmount === "") {
		        UIControlService.msg_growl("error", "MESSAGE.NOMINAL");
		        return false;
		    }

		    if (vm.dataBidangUsahaCount == 0) {
		        UIControlService.msg_growl("error", "MESSAGE.DATA_BIDANG_USAHA_EMPTY");
		        return false;
		    }

		    return true;
		}


		/*proses upload file*/
		function uploadFile() {
			AuthService.getUserLogin(function (reply) {
			    //console.info(JSON.stringify(reply));
			    if (vm.fileUpload === undefined) {
			        vm.fileUpload = "";
			        UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			        return;
			    }
				vm.VendorLogin = reply.data.CurrentUsername;
				if (UIControlService.validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.VendorLogin);
				} else {
				    UIControlService.unloadLoadingModal();
				    vm.fileUpload = "";
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});

		}

		function upload(file, config, filters, dates, callback) {
			//console.info(dates);
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			//UIControlService.loadLoadingModal("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, dates, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.datasimpan['DocumentURL'] = url;
					vm.pathFile = vm.folderFile + url;
					UIControlService.msg_growl("success", "FORM.MSG_SUC_UPLOAD");
					saveprocess();

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});

		}

		function validateFileType(file, allowedFileTypes) {
		    var valid_size = allowedFileTypes[0].Size;
		    var size_file = allowedFileTypes[0].SizeUnitName == "SIZE_UNIT_KB" ? Math.ceil(file[0].size / 1024) : Math.ceil(file[0].size / 1024) / 1024;
		    if (size_file > valid_size) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", "MESSAGE.INVALID_SIZE");
		        vm.fileUpload = "";
		        return false;
		    }
			//if (!file || file.length == 0) {
			//	UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
			//	return false;
			//}
			return true;
		}

		/* end proses upload*/

		function saveprocess() {
		    
		    vm.datasimpan['VendorCategoryID'] = item.VendorCategoryID;
		    //console.info("data simpan" + JSON.stringify(vm.datasimpan));
		    vm.datasimpan['VendorID'] = item.VendorID;
		    vm.datasimpan['LicenseID'] = vm.jenisIzinUsaha.ID;
		    vm.datasimpan['LicenseNo'] = vm.noJenisIzinUsaha;


		    if (!vm.jenisIzinUsaha.IsEndDate) {
		        vm.datasimpan['ExpiredDate'] = null;
		    } else {
		        vm.datasimpan['ExpiredDate'] = UIControlService.getStrDate(vm.ExpiredDate);

		    }

		    vm.datasimpan['IssuedBy'] = vm.instansiPemberi;
		    vm.datasimpan['IssuedDate'] = UIControlService.getStrDate(vm.IssuedDate);
		    vm.datasimpan['IssuedLocation'] = vm.city;
		    vm.datasimpan['Remark'] = vm.keterangan;
		    vm.datasimpan['BusinessFieldData'] = vm.dataBidangUsaha;

		    if (vm.jenisIzinUsaha.Name == "SIUP") {
		        vm.datasimpan['CompanyScale'] = vm.kualifikasi;
		        vm.datasimpan['CapitalAmount'] = vm.CapitalAmount;

		    } else {
		        vm.datasimpan['CompanyScale'] = null;
		        vm.datasimpan['CapitalAmount'] = null;

		    }
		    


			IzinUsahaService.updateLicensi(vm.datasimpan, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", "FORM.MSG_SUC_SAVE");
					$uibModalInstance.close();
				} else {
					UIControlService.msg_growl("error", "FORM.MSG_ERR_SAVE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				UIControlService.unloadLoadingModal();
			});
            
		}

		vm.downloadFileCivd = downloadFileCivd;
		function downloadFileCivd(url, name) {
		    UIControlService.loadLoadingModal();

		    VendorRegistrationService.getCIVDData({
		        Keyword: url
		    }, function (response) {
		        var reply = JSON.parse(response.data);
		        if (reply.status === 200) {
		            var byteCharacters = atob(reply.result);

		            var byteNumbers = new Array(byteCharacters.length);
		            for (let i = 0; i < byteCharacters.length; i++) {
		                byteNumbers[i] = byteCharacters.charCodeAt(i);
		            }

		            var byteArray = new Uint8Array(byteNumbers);

		            var blob = new Blob([byteArray], {
		                type: "application/pdf"
		            });

		            FileSaver.saveAs(blob, name + ' ' + UIControlService.getDateNow('-') + '.pdf');
		            UIControlService.unloadLoadingModal();

		        } else {
		            UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		        }
		    }, function (error) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl('error', "ERRORS.CANNOT_GET_CIVD");

		    });
		}

		vm.addBidangUsaha = addBidangUsaha;
		function addBidangUsaha() {

		    if (vm.jenisIzinUsaha == '') {
		        UIControlService.msg_growl('error', "ERRORS.PILIH_JENIS_IJIN");
		        return;
		    }

		    var data = {
		        dataBidangUsaha: vm.dataBidangUsaha,
                jenisIzinUsaha: vm.jenisIzinUsaha
		    };

		    var modalInstance = $uibModal.open({
		        templateUrl: "app/modules/rekanan/data-perusahaan/izin-usaha/pilihGolonganBidangUsaha.modal.html",
		        controller: 'PilihGolonganBidangUsahaModalController',
		        controllerAs: 'PilihGolonganBidangUsahaModalCtrl',
		        resolve: {
		            item: function () {
		                return data;
		            }
		        }
		    });
		    modalInstance.result.then(function (reply) {
		        console.log(reply)
		        if (reply != "") {
		            vm.dataBidangUsaha = reply;

		            var count = 0;
		            for (var i = 0; i < vm.dataBidangUsaha.length; i++) {
		                if (vm.dataBidangUsaha[i].IsActive) {
		                    count += 1;
		                    vm.dataBidangUsaha[i].No = count;
		                }
		            }
		            vm.dataBidangUsahaCount = count;
		        }
		    });
		}

		vm.changeJenisIzin = changeJenisIzin;
		function changeJenisIzin() {
		    UIControlService.loadLoadingModal();

		    IzinUsahaService.getJenisIzinUsaha({
		        LicenseType: vm.tipeLicense
		    }, function (reply) {
		        UIControlService.unloadLoadingModal();
		        if (reply.status == 200) {
		            var data = reply.data;
		            vm.dataSelectIzinUsaha = data;
		            if (dataModal.LicenseID != undefined) {
		                for (var i = 0; i < data.length; i++) {
		                    if (data[i].ID == dataModal.LicenseID) {
		                        vm.jenisIzinUsaha = data[i];
		                    }
		                }

		            }
                    console.log(data)
		        } else {
		            UIControlService.msg_growl('error', "ERRORS.ERROR_API");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl('error', "ERRORS.ERROR_API");
		    })
		}

		vm.removeBidangUsaha = removeBidangUsaha;
		function removeBidangUsaha(data) {
		    if (data.DataNew) {

		        var id = $.grep(vm.dataBidangUsaha, function (n) { return n.ID == data.ID; });
		        vm.dataBidangUsaha.splice(id, 1);

		    } else {
		        var id = vm.dataBidangUsaha.findIndex(x=> x.ID == data.ID);
                console.log(id)
                vm.dataBidangUsaha[id].IsActive = false;
                var count = 0;
                for (var i = 0; i < vm.dataBidangUsaha.length; i++) {
                    if (vm.dataBidangUsaha[i].IsActive) {
                        count += 1;
                        vm.dataBidangUsaha[i].No = count;
                    }
                }

                vm.dataBidangUsahaCount = count;

		    }

		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();