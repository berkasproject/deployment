﻿(function () {
    'use strict';

    angular.module("app").controller("PilihBidangUsahaTreeModalController", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'item', '$uibModal', 'IzinUsahaService', '$uibModalInstance', 'UploadFileConfigService', 'UploaderService', 'AuthService', '$filter', 'ProvinsiService', 'GlobalConstantService', 'FileSaver', 'VendorRegistrationService','$scope'];

    function ctrl($http, $translate, $translatePartialLoader, $location, UIControlService, item, $uibModal, IzinUsahaService, $uibModalInstance, UploadFileConfigService, UploaderService, AuthService, $filter, ProvinsiService, GlobalConstantService, FileSaver, VendorRegistrationService,$scope) {
        var vm = this;
        console.log(item);
        vm.TypeID = item.item.TypeID;
        var lang = $translate.use();

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.maxSize = 10;
        vm.userBisaMengatur = true; //TODO
        $scope.my_tree = {};
        vm.menuhome = 0;
        vm.page_id = 135;
        vm.level = 1;
        vm.data = [];
        vm.srcText = '';
        vm.dataSelected = item.dataSelected;
        vm.jenisIzinUsaha = item.jenisIzinUsaha;

        vm.expanding_property = {
            field: "BusinessCodePlusName",
            displayName: lang.toLowerCase() === 'id' ? 'Nama Bidang Usaha' : 'Business Field Name',
            sortable: false,
            sortingType: "string",
            filterable: true
        };

        vm.col_defs = [
            {
                field: "obj",
                displayName: "  "
               
            },
            {
                field: "obj",
                displayName: " ",
                cellTemplate: '<a ng-show="row.branch.userBisaMenghapus == false" class="btn btn-flat btn-xs btn-primary" ng-click="cellTemplateScope.click(row.branch)" title="' + (lang.toLowerCase() === 'id' ? 'Pilih' : 'Choose') + '"><i class="fa fa-plus-circle"></i> ' + $filter('translate')('CMB_CLASIFICATION.LABEL') + ' </a>',
                cellTemplateScope: {
                    click: function (data) {         // this works too: $scope.someMethod;
                        //vm.addMasterKriteria(data.Level, data.CriteriaId);
                        data.userBisaMenghapus = true;
                        data.DataNew = true;
                        Selected(data,true)
                    }
                }
            },
            {
                field: "obj",
                displayName: " ",
                cellTemplate: '<a ng-show="row.branch.userBisaMenghapus == true" ng-click="cellTemplateScope.click(row.branch)" title="' + (lang.toLowerCase() === 'id' ? 'Hapus' : 'Delete') + '" class="btn btn-flat btn-xs btn-danger center-block text-center"><i class="fa fa-trash-o"></i>&nbsp; ' + $filter('translate')('BTN.HAPUS') + '</a>',
                cellTemplateScope: {
                    click: function (data) {
                        data.userBisaMenghapus = false;
                        Selected(data,false)

                        //$scope.removeBU(data);
                    }
                }
            }
        ];

        function Selected(data, isAdd) {
            if (isAdd) {
                vm.dataSelected.push(data);
            } else {

                var dataID = $.grep(vm.dataSelected, function (n) { return n.ID == data.ID; });

                if (dataID[0].DataNew) {
                    vm.dataSelected.splice(dataID, 1);
                } else {
                    var id = vm.dataSelected.findIndex(x=> x.ID == data.ID);
                    console.log(id)
                    vm.dataSelected[id].IsActive = false;
                }
            }
        }

        vm.init = init;
        function init() {
            jLoad(1);
        }

        vm.ShowAll = ShowAll;
        function ShowAll() {
            angular.element("#expand").triggerHandler('click');
        }

        vm.jLoad = jLoad;

        function jLoad(current) {
            UIControlService.loadLoadingModal();

            vm.currentPage = current;
            var offset = (current * 10) - 10;

            IzinUsahaService.getMstBusinessFieldTree(
                {
                    IntParam1: vm.TypeID,
                    IntParam2:vm.jenisIzinUsaha.ID,
                    keyword: vm.srcText,
                    offset: offset,
                    limit: vm.maxSize
                },
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status == 200) {
                        var data = reply.data;
                        vm.data = data.List;
                        vm.totalItems = data.Count;

                        for (var i = 0; i < vm.data.length; i++) {
                            if (vm.data[i].HasSub == 1) {
                                for (var c = 0; c < vm.data[i].children.length; c++) {
                                    if (vm.data[i].children[c].HasSub == 1) {
                                        for (var j = 0; j < vm.data[i].children[c].children.length; j++) {
                                            if (vm.data[i].children[c].children[j].HasSub == 1) {
                                                for (var g = 0; g < vm.data[i].children[c].children[j].children.length; g++) {
                                                    if (vm.data[i].children[c].children[j].children[g].HasSub == 1) {
                                                        for (var h = 0; h < vm.data[i].children[c].children[j].children[g].children.length; h++) {
                                                            
                                                            var dataPilih = $.grep(vm.dataSelected, function (n) { return n.ID == vm.data[i].children[c].children[j].children[g].children[h].ID && n.IsActive; });
                                                            if(dataPilih.length != 0){
                                                                vm.data[i].children[c].children[j].children[g].children[h].userBisaMenghapus = true;
                                                            }

                                                        }
                                                    } else {
                                                        var dataPilih = $.grep(vm.dataSelected, function (n) { return n.ID == vm.data[i].children[c].children[j].children[g].ID && n.IsActive; });
                                                        if(dataPilih.length != 0){
                                                            vm.data[i].children[c].children[j].children[g].userBisaMenghapus = true;
                                                        }

                                                    }
                                                }
                                            } else {
                                                var dataPilih = $.grep(vm.dataSelected, function (n) { return n.ID == vm.data[i].children[c].children[j].ID && n.IsActive; });
                                                if(dataPilih.length != 0){
                                                    vm.data[i].children[c].children[j].userBisaMenghapus = true;
                                                }

                                            }
                                        }
                                    } else {
                                        var dataPilih = $.grep(vm.dataSelected, function (n) { return n.ID == vm.data[i].children[c].ID && n.IsActive; });
                                        if(dataPilih.length != 0){
                                            vm.data[i].children[c].userBisaMenghapus = true;
                                        }

                                    }
                                }
                            } else {
                                var dataPilih = $.grep(vm.dataSelected, function (n) { return n.ID == vm.data[i].ID && n.IsActive; });
                                if(dataPilih.length != 0){
                                    vm.data[i].userBisaMenghapus = true;
                                }

                            }
                        }

                        console.log(vm.dataSelected);


                    }
                },
                function (err) {
                    UIControlService.msg_growl('error', "ERRORS.ERROR_API");
                    UIControlService.unloadLoadingModal();

                }
            )
        }

        vm.simpanTree = simpanTree;
        function simpanTree(param) {
            //if (vm.dataSelected.length == 0) {
            //    UIControlService.msg_growl('error', "MESSAGE.HARAP_PILIH");
            //    return;
            //}

            //console.log(vm.dataSelected)
            if (param) {
                $uibModalInstance.close(vm.dataSelected);

            } else {
                $uibModalInstance.close("");
            }
        }

        vm.expandAll = expandAll;
        function expandAll() {
            UIControlService.loadLoadingModal();

            setTimeout(function () { $scope.my_tree.expand_all(); }, 100);

            
            setTimeout(function () { UIControlService.unloadLoadingModal(); }, 10000);
        }

        vm.collapseAll = collapseAll
        
        function collapseAll(){
            UIControlService.loadLoadingModal();
            setTimeout(function () { $scope.my_tree.collapse_all(); }, 100);

            setTimeout(function () { UIControlService.unloadLoadingModal(); }, 10000);
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();