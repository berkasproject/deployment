﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailTahapanPrakualifikasiVendorController", ctrl);

    ctrl.$inject = ['$state', '$http', '$filter', '$stateParams', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'DetailTahapanPrakualifikasiVendorService', 'UIControlService'];
    /* @ngInject */
    function ctrl($state, $http, $filter, $stateParams, $uibModal, $translate, $translatePartialLoader, $location, SocketService, DetailTahapanPrakualifikasiVendorService, UIControlService) {

        var vm = this;
        var loadmsg = "MESSAGE.LOADING";

        var PrequalSetupID = Number($stateParams.PrequalSetupID);

        vm.prequalReg;
        vm.step = [];
        //vm.isCancelled;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('dashboard-vendor');
            loadSteps();
        };

        function loadSteps() {
            UIControlService.loadLoading(loadmsg);
            DetailTahapanPrakualifikasiVendorService.GetPrequalReg({
                ID: PrequalSetupID
            }, function (reply) {
                vm.prequalReg = reply.data;
                DetailTahapanPrakualifikasiVendorService.GetPrequalSteps({
                    ID: PrequalSetupID
                }, function (reply) {
                    vm.step = reply.data;
                    UIControlService.unloadLoading();
                }, function (error) {
                    UIControlService.unloadLoading();
                    UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
                });
            }, function (error) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_PREQUAL_REG');
            });            
        };       

        vm.kembali = kembali;
        function kembali() {
            $state.transitionTo('dashboard-vendor');
        };

        vm.menujuTahapan = menujuTahapan;
        function menujuTahapan(data) {
            $state.transitionTo(data.PrequalStepURL + '-vendor', {
                PrequalSetupID: data.PrequalSetupID,
                SetupStepID: data.ID,
                PrequalStepID: data.ID /* alternative for SetupStepID */
            });
        };
    }
})();

