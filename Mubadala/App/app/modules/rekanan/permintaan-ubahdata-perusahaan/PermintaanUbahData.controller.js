﻿(function () {
	'use strict';

	angular.module("app").controller("PermintaanPerubahanDataCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'PermintaanUbahDataService', 'DataAdministrasiService', 'UIControlService', 'SocketService', '$filter','$q','$rootScope'];
	function ctrl($translatePartialLoader, PUbahDataService, DataAdministrasiService, UIControlService, SocketService, $filter,$q,$rootScope) {
		var vm = this;
		vm.init = init;
		vm.listCR = [];
		vm.Remark = '';
		vm.isVerified;
		vm.IsApprovedBy;
		vm.IsLocal = false;
		vm.IsOverseas = false; // 外国会社

		function init() {
		    $translatePartialLoader.addPart('permintaan-ubah-data');
		    cekPrakualifikasiVendor();
		    UIControlService.loadLoading();
		    loadDataVendor(1);
			//loadListOpsiChangeRequest();
			//loadCheckCR();
			chekcIsVerified();
		}

		vm.cekPrakualifikasiVendor = cekPrakualifikasiVendor;
		function cekPrakualifikasiVendor() {
		   PUbahDataService.cekPrakualifikasiVendor(function (reply) {
		        if (reply.status === 200) {
		            vm.pqWarning = reply.data;
		            console.info("isregistered" + vm.pqWarning);
		        } else {
		            UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		        }
		    }, function (err) {
		        UIControlService.unloadLoadingModal();
		        UIControlService.msg_growl("error", 'NOTIFICATION.GET.PREQUALANNOUNCE.ERROR', "NOTIFICATION.GET.PREQUALANNOUNCE.TITLE");
		    });
		}

		function chekcIsVerified() {
			PUbahDataService.isVerified(function (reply) {
				if (reply.status === 200) {
					console.info("ver>>" + JSON.stringify(reply));
					var data = reply.data;
					if (!(data.Isverified === null)) {
						vm.isVerified = true;
					} else {
						vm.isVerified = false;
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.isCivd = false;

		vm.loadDataVendor = loadDataVendor;
		function loadDataVendor(current) {
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			DataAdministrasiService.select({
				Offset: offset,
				Limit: 10,
				Keyword: vm.Username
			}, function (reply) {
				if (reply.status === 200) {
					vm.list = {};
					vm.administrasi = reply.data[0];
					if (vm.administrasi.VendorCategoryID == 32) {
					    vm.isCivd = true;
					}

					DataAdministrasiService.selectcontact({
						VendorID: vm.administrasi.VendorID
					}, function (reply) {
						if (reply.status === 200) {
							vm.contact = reply.data;
							loadListOpsiChangeRequest();
							//loadCheckCR()
						} else {
							$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
							UIControlService.unloadLoading();
						}
					}, function (err) {
						console.info("error:" + JSON.stringify(err));
						//$.growl.error({ message: "Gagal Akses API >" + err });
						UIControlService.unloadLoading();
					});
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}


		//function loadCheckCR() {
		//	UIControlService.loadLoading("Silahkan Tunggu");

		//}

		function loadListOpsiChangeRequest() {
			//alert("load");
			vm.listCRAll = [];
			PUbahDataService.getOpsiChangeReq({ IntParam1: vm.contact[0].Contact.Address.State.Country.CountryID }, function (reply) {
				//UIControlService.unloadLoading();
				var data = reply.data;
				vm.listCR = [];
				for (var i = 0; i < data.length; i++) {
					var newmap = {
					    ChangeRequestRefID: data[i].SysReference.RefID,
					    CRName: data[i].SysReference.Value,
                        StateName: data[i].StateName,
						Description: "",
						IsApproved: false,
						IsRequested: false,
						IsTouched: false
					}
					vm.listCR.push(newmap);
					
				}
				PUbahDataService.getCRbyVendor(vm.listCR, function (reply) {
				    UIControlService.unloadLoading();

					if (reply.status === 200) {
					    vm.listCR = reply.data;
					    console.log($rootScope.menus);
					    for (var i = 0; i < $rootScope.menus.length; i++) {
					        for (var c = 0; c < vm.listCR.length; c++) {
					            if (vm.listCR[c].StateName == $rootScope.menus[i].state && $rootScope.menus[i].IsChecked == null) {
					                vm.listCR[c].IsRequested = true;
					            }
					        }

					    }

						vm.isSentCR = false;
						
					}

				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.API");
					UIControlService.unloadLoading();
				});
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.commit = commit;
		function commit(refId) {
			bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('CONFIRM.CHANGES') + '<h3>', function (reply) {
				if (reply) {
					UIControlService.loadLoading("MESSAGE.LOADING");
					PUbahDataService.commit({ OpsiCode: refId }, function (reply) {
						UIControlService.unloadLoading();
						if (reply.status === 200) {
							UIControlService.msg_growl("success", "MESSAGE.SUC_SEND");
							SocketService.emit("SubmitUbahData");
							init();
						} else {
							UIControlService.msg_growl("error", "MESSAGE.ERR_SEND");
							return;
						}
					}, function (err) {
						UIControlService.msg_growl("error", "MESSAGE.API");
						UIControlService.unloadLoading();
					});
				}
			});
		}

		vm.sendData = sendData;
		function sendData() {
			//mapping data
			var newlist = [];
			var validCheck = true;
			for (var i = 0; i < vm.listCR.length; i++) {
				if (vm.listCR[i].IsApproved === true) {
					var newmap = {
						ChangeRequestRefID: vm.listCR[i].ChangeRequestRefID,
						Description: vm.listCR[i].Description,
						IsApproved: 0
					}
					newlist.push(newmap);
					if (vm.listCR[i].Description == '') {
					    
					    validCheck = false;
					}
				}
			}

			if (newlist.length == 0) {
			    UIControlService.msg_growl("warning", "MESSAGE.CHECKED_NULL");
			    return;
			}

			if (!validCheck) {
			    UIControlService.msg_growl("warning", "MESSAGE.DESCRIPTION_NULL");
			    return;
			}
			UIControlService.loadLoading("MESSAGE.LOADING");
			//console.info(JSON.stringify(newlist));
			var datasimpan = {
				Remark: vm.Remark,
				ChangeRequestDataDetails: newlist
			}
			PUbahDataService.insertChangeReq(datasimpan, function (reply) {
				if (reply.status === 200) {
					init();
					UIControlService.msg_growl("success", "MESSAGE.SUC_SEND");
					//SocketService.emit("PermintaanUbahData");
					//window.location.reload();
					//UIControlService.unloadLoading();
				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_SEND");
					UIControlService.unloadLoading();
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}
	}
})();