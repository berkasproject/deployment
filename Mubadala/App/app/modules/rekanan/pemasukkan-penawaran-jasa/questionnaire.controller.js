﻿(function () {
    'use strict';

    angular.module("app").controller("questionnaireServiceTenderCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'OfferEntryService',
        '$state', 'UIControlService', 'UploaderService', '$uibModal', 'GlobalConstantService', '$stateParams'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, OEService, $state,
        UIControlService, UploaderService, $uibModal, GlobalConstantService, $stateParams) {
        var vm = this;
        vm.listQuest = [];
        vm.IDTender = Number($stateParams.TenderRefID);
        vm.IDStepTender = Number($stateParams.StepID);
        vm.ProcPackType = Number($stateParams.ProcPackType);
        vm.IDDoc = Number($stateParams.DocID);

        vm.back = back;
        function back() {
            $state.transitionTo('kelengkapan-datakomersial-jasa-vendor', {
                TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: vm.IDDoc
            });
        }
        vm.backpengadaan = backpengadaan;
        function backpengadaan() {
            $state.transitionTo('kelengkapan-datakomersial-jasa-vendor', {
                TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: vm.IDDoc
            });
        }
        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('pemasukkan-penawaran-jasa');
            UIControlService.loadLoading("MESSAGE.LOADING");
            OEService.getQuestionaireByVendor({
                TenderStepID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.listQuest = data.VendorQuestionaires;
                    console.info(JSON.stringify(vm.listQuest));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });

            loadDataTender();
        }

        function loadDataTender() {
            OEService.getDataStepTender({
                ID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.TenderName = data.tender.TenderName;
                    vm.StartDate = UIControlService.getStrDate(data.StartDate);
                    vm.EndDate = UIControlService.getStrDate(data.EndDate);
                    vm.StartDateFull = new Date(data.StartDate);
                    vm.EndDateFull = new Date(data.EndDate);
                    vm.today = new Date();
                    vm.accessPermission = false;
                    console.info("sd" + vm.StartDateFull);
                    console.info("ndate" + vm.today);
                    if (vm.today >= vm.StartDateFull && vm.today <= vm.EndDateFull) {
                        console.info("true");
                        vm.accessPermission = true;
                    }
                    //console.info("tender::" + JSON.stringify(vm.dataTenderReal));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }


        function loadDataTender() {
            OEService.getDataStepTender({
                ID: vm.IDStepTender
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.TenderName = data.tender.TenderName;
                    vm.StartDate = UIControlService.getStrDate(data.StartDate);
                    vm.EndDate = UIControlService.getStrDate(data.EndDate);
                    vm.StartDateFull = new Date(data.StartDate);
                    vm.EndDateFull = new Date(data.EndDate);
                    vm.today = new Date();
                    vm.accessPermission = false;
                    console.info("sd" + vm.StartDateFull);
                    console.info("ndate" + vm.today);
                    if (vm.today >= vm.StartDateFull && vm.today <= vm.EndDateFull) {
                        console.info("true");
                        vm.accessPermission = true;
                    }
                    //console.info("tender::" + JSON.stringify(vm.dataTenderReal));
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.saveQuest = saveQuest;
        function saveQuest() {
            UIControlService.loadLoading("MESSAGE.LOADING");
            OEService.approveQuestionaire(vm.listQuest, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE_QUEST");
                    vm.init();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
        }

        vm.back = back;
        function back() {
            $state.transitionTo('pemasukkan-penawaran-jasa-vendor', {
                TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: vm.IDDoc
            });
        }
    }
})();
