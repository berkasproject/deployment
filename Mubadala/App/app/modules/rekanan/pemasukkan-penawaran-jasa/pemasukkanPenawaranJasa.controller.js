﻿(function () {
	'use strict';

	angular.module("app").controller("ServiceOfferEntryVendorCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'OfferEntryService', 'PengumumanPengadaanService', '$state', 'UIControlService', '$uibModal', '$stateParams', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, OEService, PengumumanPengadaanService,
        $state, UIControlService, $uibModal, $stateParams, GlobalConstantService) {
		var vm = this;
		vm.IDTender = Number($stateParams.TenderRefID);
		vm.IDStepTender = Number($stateParams.StepID);
		vm.ProcPackType = Number($stateParams.ProcPackType);
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.maxSize = 10;
		vm.currentPage = 1;
		vm.TenderName = '';
		vm.TenderCode = '';
		vm.StartDate = null;
		vm.EndDate = null;
		vm.listKelengkapan = [];
		vm.IsFlagToSubmit = true;
		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart("pemasukkan-penawaran-jasa");
		    loadSteps();
			if (vm.ProcPackType === 4189) {
				loadDataPenawaran();
				loadDataTender();
			} else {
				UIControlService.msg_growl("warning", "MESSAGE.PAGE");
				return;
			}
		}

		function loadDataTender() {
			OEService.getDataStepTender({
				ID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    var data = reply.data;
				    //console.info("dataStep" + JSON.stringify(data));
				    vm.TenderName = data.tender.TenderName;
				    vm.TenderID = data.TenderID;
					vm.StartDate = UIControlService.convertDateTime(data.StartDate);
					vm.EndDate = UIControlService.convertDateTime(data.EndDate);
					
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		function loadSteps() {
		    OEService.GetSteps({
		        ID: vm.IDStepTender
		    }, function (reply) {
		        vm.stepOvertime = reply.data;
		        for (var i = 0; i < vm.stepOvertime.length; i++) {
		            if (vm.stepOvertime[i].step.FormTypeURL == "pemasukkan-penawaran-jasa") {
		                vm.accessPermission = vm.stepOvertime[i].IsOvertime;

		            }
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		        UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_STEP');
		    });
		}

		function loadDataPenawaran() {
			UIControlService.loadLoading("MESSAGE.LOADING");
			OEService.getKelengkapanDocVendor({
				TenderStepID: vm.IDStepTender
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    var data = reply.data;
				    vm.IsSubmit = data.IsSubmit;
				    vm.SubmitDate = UIControlService.convertDateTime(data.SubmitDate);
					console.info("penawaran:" + JSON.stringify(data));
					vm.listKelengkapan = data.VendorDocuments;
					for (var i = 0; i < vm.listKelengkapan.length; i++) {
						if (!(vm.listKelengkapan[i].ApproveDate === null)) {
						    vm.listKelengkapan[i].ApproveDate = UIControlService.getStrDate(vm.listKelengkapan[i].ApproveDate);
						    
						}
						if (vm.listKelengkapan[i].IsApproved !== true) vm.IsFlagToSubmit = false;
					}

				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.detailDokumen = detailDokumen;
		function detailDokumen(data) {
			console.info(JSON.stringify(data));
			if (!(data.DocumentType === 'FORM_DOCUMENT')) {
				var datax = {
					DocumentName: data.DocumentName,
					FileType: data.FileType,
					ApproveDate: data.ApproveDate,
					DocumentURL: data.DocumentURL,
					OfferEntryDocumentID: data.OfferEntryDocumentID,
					OfferEntryVendorID: data.OfferEntryVendorID,
					IsCheck: false,
                    accessPermission:vm.accessPermission
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'app/modules/rekanan/pemasukkan-penawaran-jasa/detailDokumen.html',
					controller: 'detailDokumenJasaCtrl',
					controllerAs: 'detDokCtrl',
					resolve: {
						item: function () {
							return datax;
						}
					}
				});
				modalInstance.result.then(function () {
					vm.init();
				});
			} else {
				$state.transitionTo('kelengkapan-datakomersial-jasa-vendor', {
					TenderRefID: vm.IDTender, StepID: vm.IDStepTender, ProcPackType: vm.ProcPackType, DocID: data.OfferEntryDocumentID
				});
			}
		}

		vm.backDetailTahapan = backDetailTahapan;
		function backDetailTahapan() {
			$state.transitionTo('data-pengadaan-tahapan', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, TenderID: vm.TenderID});
		}

		vm.backTahapan = backTahapan;
		function backTahapan() {
			$state.transitionTo('pemasukkan-penawaran-jasa', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, StepID: vm.IDStepTender });
		}

		vm.batal = batal;
		function batal() {
			$state.transitionTo('dashboard-vendor');
		}

		vm.complete = complete;
		function complete() {
		    vm.IsFlagToChecklist = true;
		    vm.listChecklist = [];
		    vm.i = 1;
		    if (vm.IsFlagToSubmit === true) {
		        OEService.getDataChecklistVendor({
		            TenderStepID: vm.IDStepTender
		        }, function (reply) {
		            UIControlService.unloadLoading();
		            if (reply.status === 200) {
		                vm.listChecklist = reply.data.VendorChecklists;
		                vm.listChecklist.forEach( function (data){
		                    if (data.IsApproved !== true) {
		                        vm.IsFlagToChecklist = false;
		                        UIControlService.msg_growl("warning", "MESSAGE.NOT_COMPLETE");
		                        UIControlService.unloadLoading();
		                    }
                            
		                    if (vm.i == vm.listChecklist.length) {
		                        if (vm.IsFlagToChecklist === true) {
		                            OEService.complete({
		                                TenderStepID: vm.IDStepTender
		                            }, function (reply) {
		                                UIControlService.unloadLoading();
		                                if (reply.status === 200) {
		                                    UIControlService.msg_growl('success', 'MESSAGE.SUC_SEND');
		                                    vm.init();
		                                }
		                            }, function (err) {
		                                //UIControlService.msg_growl("error", "MESSAGE.API");
		                                UIControlService.unloadLoading();
		                            });
                                }
		                    }
		                   vm.i =  +vm.i + 1;
		                });
		            }
		        }, function (err) {
		            UIControlService.unloadLoading();
		        });
		    }
		    else {
		        UIControlService.msg_growl("warning", "MESSAGE.TENDER_NOT_COMPLETE");
		        UIControlService.unloadLoading();
		    }
		}

		vm.kelengkapanTender = kelengkapanTender;
		function kelengkapanTender() {
			$state.transitionTo('kelengkapan-tender-jasa', { TenderRefID: vm.IDTender, ProcPackType: vm.ProcPackType, StepID: vm.IDStepTender });
		}

		vm.updateChecklist = updateChecklist;
		function updateChecklist(data) {
			//console.info("data:" + JSON.stringify(data));
			UIControlService.loadLoadingModal("MESSAGE.LOADING");
			OEService.approveDocByVendor({
				OfferEntryDocumentID: data.OfferEntryDocumentID,
				OfferEntryVendorID: data.OfferEntryVendorID,
				IsApproved: data.IsApproved
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					var data = reply.data;
					UIControlService.msg_growl("succcess", "MESSAGE.SUC_SAVE");
					vm.init();
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoadingModal();
			});
		}
	}
})();