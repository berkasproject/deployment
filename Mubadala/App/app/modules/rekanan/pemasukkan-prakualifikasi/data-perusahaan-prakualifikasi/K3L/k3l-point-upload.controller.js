﻿(function () {
	'use strict';

	angular.module("app")
            .controller("K3LPointUploadCtrl", ctrl);

	ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'VendorPrequalEntryService', 'UploadFileConfigService',
        'UIControlService', 'UploaderService', 'item', '$uibModalInstance', 'GlobalConstantService'];
	function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, VendorPrequalEntryService, UploadFileConfigService,
        UIControlService, UploaderService, item, $uibModalInstance, GlobalConstantService) {

		var vm = this;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.action = "";
		vm.pathFile;
		vm.Description;
		vm.fileUpload;
		vm.size;
		vm.name;
		vm.type;
		vm.flag;
		vm.selectedForm;


		vm.init = init;
		function init() {
		    vm.DocumentUrl = item.DocumentUrl;
		    vm.pathFile = item.DocumentUrl;
			$translatePartialLoader.addPart("master-library");
			UIControlService.loadLoading("MESSAGE.LOADING");
			//get tipe dan max.size file - 1
			UploadFileConfigService.getByPageName("PAGE.VENDOR.CSMSPRAKUAL", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.name = response.data.name;
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
			loadPrequalStep();
		}

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    VendorPrequalEntryService.loadPrequalStep({
		        Status: item.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		//get tipe dan max.size file - 2
		function generateFilterStrings(allowedTypes) {
			console.info(allowedTypes);
			var filetypes = "";
			for (var i = 0; i < allowedTypes.length; i++) {
				filetypes += "." + allowedTypes[i].Name + ",";
			}
			return filetypes.substring(0, filetypes.length - 1);
		}

		/*start upload */
		vm.uploadFile = uploadFile;
		function uploadFile() {

			if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
				upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
			}
		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		vm.upload = upload;
		function upload(file, config, filters, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;

			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
				vm.flag = 0;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
				vm.flag = 1;
			}
			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFileLibrary(file, size, filters,
                function (response) {
                	UIControlService.unloadLoading();
                	console.info("response:" + JSON.stringify(response));
                	if (response.status == 200) {
                		var url = response.data.Url;
                		vm.pathFile = url;
                		vm.name = response.data.FileName;
                		var s = response.data.FileLength;
                		if (vm.flag == 0) {

                			vm.size = Math.floor(s)
                		}

                		if (vm.flag == 1) {
                			vm.size = Math.floor(s / (1024));
                		}
                		save();

                	} else {
                		UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                		return;
                	}
                },
                function (response) {
                	UIControlService.msg_growl("error", "MESSAGE.API")
                	UIControlService.unloadLoading();
                });
		}

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		};

		vm.save = save;
		function save() {
		    VendorPrequalEntryService.insertDocCSMS({
                Status: item.PrequalStepID,
		        Keyword: vm.pathFile
		    },
                function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "SUCCESS_UPLOAD");
                        $uibModalInstance.close();
                    }
                },
                function (err) {
                    UIControlService.unloadLoadingModal();
                });
		}
	}
})();