﻿(function () {
    'use strict';

    angular.module("app").controller("SuratPernyataanPrequalController", ctrl);

    ctrl.$inject = ['$http', '$window', '$translate', '$stateParams', '$state', '$uibModal', '$translatePartialLoader', 'VerifiedSendService', 'SrtPernyataanPrequalService', 'UIControlService', 'GlobalConstantService', 'VendorRegistrationService'];
    /* @ngInject */
    function ctrl($http, $window, $translate, $stateParams, $state, $uibModal, $translatePartialLoader, VerifiedSendService, SrtPernyataanPrequalService, UIControlService, GlobalConstantService, VendorRegistrationService) {
        var vm = this;
        vm.currentLang = $translate.use();
        var endpoint = GlobalConstantService.getConstant("api_endpoint");
        $translatePartialLoader.addPart('daftar');
        vm.PrequalStepID = Number($stateParams.PrequalStepID);
        vm.tglSekarang = new Date();
        vm.data = [];
        vm.pos;
        vm.cek;
        vm.DocUrlAgreement;
        vm.DocUrlBConduct;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.DocType = null;
        vm.isApprovedCR = false;

        vm.init = init;
        function init() {
            $translatePartialLoader.addPart('surat-pernyataan');
            $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
            
            
            //loadVerifiedVendor();
            if (localStorage.getItem("currLang") === 'id') {
                vm.DocType = 4225;
            } else {
                vm.DocType = 4232;
            }
            loadUrlLibrary();
            loadUrlLibraryAgree(1);
            loadUrlKuesioner();
            loadPrequalStep();
            cekSubmit();
            SrtPernyataanPrequalService.load({Status:vm.PrequalStepID},function (reply) {
                UIControlService.unloadLoading();
                //console.info("CR:" + JSON.stringify(reply));
                if (reply.status == 200) {
                    console.info("datav:" + JSON.stringify(reply.data));
                    vm.data = reply.data;
                    loadUrlAgreement(1);
                    loadVendorContact();
                    loadDataVendorAgreement();
                    //forKuesioner
                    loadVendor();
                    loadCek();
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
            });
            
        }

        vm.cekSubmit = cekSubmit;
        function cekSubmit() {
            SrtPernyataanPrequalService.cekSubmit({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.flagSubmit = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadPrequalStep = loadPrequalStep;
        function loadPrequalStep() {
            SrtPernyataanPrequalService.loadPrequalStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.isEntry = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }


        vm.loadVendor = loadVendor();
        function loadVendor() {
            SrtPernyataanPrequalService.loadContact({
                Status: vm.PrequalStepID
            },
            function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendor = reply.data;
                    vm.VendorID = vm.vendor.VendorID;
                    for (var i = 0; i < vm.vendor.length; i++) {
                        if (vm.vendor[i].VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN') {
                            if (vm.vendor[i].Contact.Address.StateID != null) {
                                if (vm.vendor[i].Contact.Address.CityID != null) {
                                    if (vm.vendor[i].Contact.Address.DistrictID != null) vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.Distric.Name + ", " + vm.vendor[i].Contact.Address.City.Name + ", " + vm.vendor[i].Contact.Address.State.Name;
                                    else vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.City.Name + ", " + vm.vendor[i].Contact.Address.State.Name;
                                }
                                else vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.State.Name;
                            }
                            else vm.address = vm.vendor[i].Contact.Address.AddressInfo + ", " + vm.vendor[i].Contact.Address.State.Name;
                            vm.VendorName = vm.vendor[i].Vendor.VendorName;
                            vm.npwp = vm.vendor[i].Vendor.Npwp;
                        }
                        else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
                            vm.email = vm.vendor[i].Contact.Email;
                            vm.username = vm.vendor[i].Vendor.user.Username;
                            vm.VendorName = vm.vendor[i].Vendor.VendorName;
                        }
                        else if (vm.vendor[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_PERSONAL') {

                            if (vm.telp == undefined) {
                                vm.name = vm.vendor[i].Contact.Name;
                                vm.telp = vm.vendor[i].Contact.Phone;
                            }
                        }
                    }
                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DATA_DEPT" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadCek = loadCek;
        function loadCek() {
            VendorRegistrationService.CekVendorPrequal({ Status: vm.PrequalStepID }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.vendorQues = reply.data;
                    jloadKuesioner();
                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DATA_DEPT" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.jloadKuesioner = jloadKuesioner;
        function jloadKuesioner() {
            UIControlService.loadLoading("LOADERS.LOADING");
            VendorRegistrationService.selectQuestionnaire({ Keyword: vm.currentLang },
               function (reply) {
                   vm.data = [];
                   vm.type = [];
                   UIControlService.unloadLoading();
                   if (reply.status === 200) {
                       vm.list = reply.data;
                       if (vm.vendorQues.length !== 0) {
                           vm.flag = 1;
                           for (var i = 0; i < vm.vendorQues.length; i++) {
                               for (var j = 0; j < vm.list[i].type.length; j++) {
                                   var calldateType = {
                                       ID: vm.list[i].type[j].ID,
                                       question: vm.list[i].type[j].question,
                                       DetailAnswer: vm.list[i].type[j].DetailAnswer
                                   }
                                   vm.type.push(calldateType);
                               }
                               vm.calldata = {
                                   ID: vm.list[i].ID,
                                   question: vm.list[i].question,
                                   DetailId: vm.list[i].DetailId,
                                   AnswerName: vm.list[i].AnswerName,
                                   Value: vm.vendorQues[i].VendQuesDetailId,
                                   Description: vm.vendorQues[i].Description,
                                   type: vm.type
                               }
                               vm.data.push(vm.calldata);
                               vm.type = [];
                           }
                       }
                       else {
                           vm.data = vm.list;
                       }
                   }
               }, function (err) {
               });
        }

        vm.downloadQuestionnaire = downloadQuestionnaire;
        function downloadQuestionnaire() {
            var questionaire = [];
            for (var i = 0; i < vm.data.length; i++) {
                var data = {
                    Value: vm.data[i].Value,
                    VendorID: vm.VendorID,
                    Description: vm.data[i].Description,
                    PrequalStepID: vm.PrequalStepID
                }
                questionaire.push(data);
            }
            var headers = {};
            headers.Authorization = 'bearer ' + GlobalConstantService.readToken();

            $http({
                method: 'POST',
                url: endpoint + '/vendorAgreementpq/generateQuestionnaire',
                headers: headers,
                data: questionaire,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                var linkElement = document.createElement('a');
                var fileName = "Initial Risk Assessment Questionnaire " + vm.VendorName + ".pdf";

                try {
                    var blob = new Blob([data], { type: headers('content-type') });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute('download', fileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);
                } catch (e) {
                    console.log(e);
                }
            });
            //var a = document.createElement("a");
            //document.body.appendChild(a);

            //PrequalCertificateService.GenerateCertificate({
            //	PrequalSetupStepID: vm.stepId
            //}, function (reply) {
            //	if (reply.status === 200) {

            //		var octetStreamMime = 'application/octet-stream';
            //		var success = false;
            //		var filename = 'download.docx';
            //		var contentType = octetStreamMime;
            //		var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;

            //		if (urlCreator) {
            //			var link = document.createElement('a');
            //			if ('download' in link) {
            //				try {
            //					// Prepare a blob URL
            //					console.log("Trying download link method with simulated click ...");
            //					var blob = new Blob([reply.data], { type: contentType });
            //					var url = urlCreator.createObjectURL(blob);
            //					link.setAttribute('href', url);

            //					// Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
            //					link.setAttribute("download", filename);

            //					// Simulate clicking the download link
            //					var event = document.createEvent('MouseEvents');
            //					event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
            //					link.dispatchEvent(event);
            //					console.log("Download link method with simulated click succeeded");
            //					success = true;

            //				} catch (ex) {
            //					console.log("Download link method with simulated click failed with the following exception:");
            //					console.log(ex);
            //				}
            //			}
            //		}
            //		//var file = new Blob([reply.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
            //		//var file = new File([reply.data], fileName, { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
            //		//var fileURL = window.URL.createObjectURL(file);
            //		////window.open(fileURL);
            //		//a.href = fileURL;
            //		//a.download = fileName;
            //		//a.click();
            //	}
            //}, function (error) {
            //	UIControlService.unloadLoading();
            //});

            //var innerContents = document.getElementById(formCertificate).innerHTML;
            //var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            //popupWindow.document.open();
            //popupWindow.document.write('<html><head><title>Prequal Cerificate</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            //popupWindow.document.close();
        }

        //ambil VendorID
        vm.loadVerifiedVendor = loadVerifiedVendor;
        function loadVerifiedVendor() {
            VerifiedSendService.selectVerifikasi(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.verified = reply.data;
                    vm.cekTemporary = vm.verified.IsTemporary;
                    vm.VendorID = vm.verified.VendorID;
                    vm.VendorName = vm.verified.VendorName;
                    jLoad(1);
                    loadUrlAgreement(1);
                    loadVendorContact();
                    loadDataVendorAgreement();
                    //forKuesioner
                    loadVendor();
                    //loadCek();
                    //console.info(JSON.stringify(vm.verified.VendorID));
                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DATA_COMP" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.printForm = printForm;
        function printForm(form) {
            var innerContents = document.getElementById(form).innerHTML;
            //console.info(innerContents);
            var popupWindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWindow.document.open();
            popupWindow.document.write('<html><head><title>Kuesioner-' + vm.VendorName + '</title><link rel="stylesheet" type="text/css" media="print" href="assets/css/print.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            popupWindow.document.close();

            //$uibModalInstance.close();
        }

        vm.loadDataVendorAgreement = loadDataVendorAgreement;
        function loadDataVendorAgreement() {
            //console.info("curr " + vm.VendorID);
            SrtPernyataanPrequalService.selectData({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listData = reply.data;
                    //console.info("data VendorAgreement:" + JSON.stringify(vm.listData));
                    vm.IsBConductUploaded = false; vm.IsAgreementLetterUploaded = false;
                    for (var i = 0; i < vm.listData.length; i++) {
                        if (vm.listData[i].DocType === 4232 || vm.listData[i].DocType === 4225) {
                            if (vm.listData[i].DocumentUrl != null) {
                                if (vm.listData[i].DocumentUrl != "") {
                                    //console.info("masuk");
                                    vm.IsBConductUploaded = true;
                                    vm.urlBConduct = vm.listData[i].DocumentUrl;
                                    //console.info("urlnya:" + vm.urlBConduct);
                                }
                            }
                        }
                        if (vm.listData[i].DocType === 4226) {
                            if (vm.listData[i].DocumentUrl != null) {
                                if (vm.listData[i].DocumentUrl != "") {
                                    vm.IsAgreementLetterUploaded = true;
                                    vm.urlAgreementLetter = vm.listData[i].DocumentUrl;
                                }
                            }
                        }
                    }
                    //console.info("isBconduct" + vm.IsBConductUploaded);
                    //console.info("isAgreementLetter" + vm.IsAgreementLetterUploaded);
                    //console.info("url:" + vm.urlBConduct);
                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DATA_DEPT" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorContact = loadVendorContact;
        function loadVendorContact(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectContactID({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.ContactID = data[0].ContactID;
                    getAddressID(1);
                    //console.info(vm.ContactID);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.getAddressID = getAddressID;
        function getAddressID(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectAddressID({
                ContactID: vm.ContactID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.AddressID = data[0].AddressID;
                    loadAddress(1);
                    //console.info(vm.AddressID);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadAddress = loadAddress;
        function loadAddress(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectAddress({
                AddressID: vm.AddressID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Address = data[0].AddressDetail;
                    vm.StateID = data[0].StateID;
                    loadCountryID(1);
                    //console.info(vm.Address);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountryID = loadCountryID;
        function loadCountryID(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectCountryID({
                StateID: vm.StateID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.CountryID = data[0].CountryID;
                    vm.StateName = data[0].Name;
                    loadCountry(1);
                    //console.info(vm.StateName);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadCountry = loadCountry;
        function loadCountry(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectCountry({
                CountryID: vm.CountryID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.Country = data[0].Name;
                    console.info(vm.Country);
                    if (vm.Country === 'Indonesia') {
                        vm.cek = true;
                    } else {
                        vm.cek = false;
                    }

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.jLoad = jLoad;
        function jLoad(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.All({
                VendorId: vm.VendorID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.data = data;
                    //console.info(vm.data);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlLibrary = loadUrlLibrary;
        function loadUrlLibrary() {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            SrtPernyataanPrequalService.selectDocLibrary({
                DocType: vm.DocType
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    console.info("bconduct:" + JSON.stringify(data));
                    vm.DocUrlBConduct = data[0].DocUrl;
                    //console.info("url:" + vm.DocUrlBConduct);
                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlLibraryAgree = loadUrlLibraryAgree;
        function loadUrlLibraryAgree(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectDocLibrary({
                DocType: 4226
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.DocUrlAgreement = data[0].DocUrl;
                    console.info("kue:" + JSON.stringify(data));

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlAgreement = loadUrlAgreement;
        function loadUrlAgreement(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectUrlAgree({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    //getAddressID(1);
                    //console.info(reply.data);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlBConduct = loadUrlBConduct;
        function loadUrlBConduct(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectUrlBConduct({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.DOcUrlBConduct = data[0].AddressID;
                    //console.info(reply.data);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.loadUrlBConductEN = loadUrlBConductEN;
        function loadUrlBConductEN(current) {
            //console.info("curr "+current)
            UIControlService.loadLoading("LOADERS.LOADING");
            //vm.currentPage = current;
            //var offset = (current * 10) - 10;
            SrtPernyataanPrequalService.selectBConductEN({
                PrequalStepID: vm.PrequalStepID
            }, function (reply) {
                //console.info("data:" + JSON.stringify(reply));
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    //vm.Address = data[0].AddressDetail;
                    //console.info(reply.data);

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }

        vm.baca = baca;
        function baca() {
            var data = {
                DocType: vm.DocType,
                PrequalStepID: vm.PrequalStepID,
                isEntry: vm.isEntry
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/suratPernyataan/suratPernyataan.modal.html',
                controller: 'SuratPernyataanModalCtrl',
                controllerAs: 'SrtPernyataanModalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
        };

        vm.baca2 = baca2;
        function baca2() {
            var data = {
                item: data
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modalBaca.html',
                controller: 'SuratPernyataanModalBacaCtrl',
                controllerAs: 'SrtPernyataanModalBacaCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
        };

        vm.upload = upload;
        function upload(DocType) {
            var data = {
                DocType: DocType,
                act: 1,
                PrequalStepID: vm.PrequalStepID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modalUpload.html',
                controller: 'SuratPernyataanModalUploadCtrl',
                controllerAs: 'SrtPernyataanModalUpCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        };

        vm.upload2 = upload2;
        function upload2(data) {
            var data = {
                DocType: data,
                act: 0,
                PrequalStepID: vm.PrequalStepID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/suratPernyataan.modalUpload.html',
                controller: 'SuratPernyataanModalUploadCtrl',
                controllerAs: 'SrtPernyataanModalUpCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                window.location.reload();
            });
        };

        vm.editKuesioner = editKuesioner;
        function editKuesioner() {
            $state.go('kuesioner-vendor-prequal', { PrequalStepID: vm.PrequalStepID });
        }

        vm.back = back;
        function back() {
            $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
        }

        vm.Submit = Submit;
        function Submit() {
            if (vm.flagSubmit == false) {
                UIControlService.msg_growl("error", 'Data Belum lengkap');
                return;
            }
            else {
                SrtPernyataanPrequalService.Submit({ Status: vm.PrequalStepID }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "Success Submit");
                        $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
            
        }

        vm.uploadKuesioner = uploadKuesioner;
        function uploadKuesioner() {
            var data = {
                VendorID: vm.VendorID
            };
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/data-perusahaan/suratPernyataan/formUploadQuestionnaire.html',
                controller: 'FormQuestionnaireCtrl',
                controllerAs: 'FormQuestionnaireCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function () {
                init();
            });
        };

        vm.loadUrlKuesioner = loadUrlKuesioner;
        function loadUrlKuesioner() {
            SrtPernyataanPrequalService.selectUrlKuesioner({}, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    var data = reply.data;
                    vm.UrlKuesioner = data.Vendor.QuestionnaireUrl;
                    console.info("urlkue:" + JSON.stringify(vm.UrlKuesioner));

                } else {
                    $.growl.error({ message: "ERRORS.FAIL_GET_DOC" });
                    UIControlService.unloadLoading();
                }
            }, function (err) {
                console.info("error:" + JSON.stringify(err));
                //$.growl.error({ message: "Gagal Akses API >" + err });
                UIControlService.unloadLoading();
            });
        }
    }
})();