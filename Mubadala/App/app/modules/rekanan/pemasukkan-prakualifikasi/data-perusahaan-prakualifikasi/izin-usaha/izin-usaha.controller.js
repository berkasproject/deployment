(function () {
	'use strict';

	angular.module("app").controller("IzinUsahaPrequalController", ctrl);

	ctrl.$inject = ['$filter', '$state','$stateParams','$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'IzinUsahaPrequalService', 'AuthService', 'UIControlService', 'VendorPrequalEntryService'];
	/* @ngInject */
	function ctrl($filter, $state, $stateParams, $http, $uibModal, $translate, $translatePartialLoader, $location, IzinUsahaPrequalService, AuthService, UIControlService, VendorPrequalEntryService) {
	    var vm = this;
	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.listLicensi = [];
		vm.isChangeData = false;
		vm.IsApprovedCR = false;
		vm.dataUpdate = {
		    item: {},
		    isForm: false,
		    cityID: 0,
		    VendorEntryPrequal: {
		        PrequalSetupStepID: 0
		    }
		}

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
		    $translatePartialLoader.addPart('data-izinusaha');
		    loadLicense();
		    //loadData();
		    //loadPrequalStep();
		    //cekSubmit();
		    //loadCityCompany();
		}

		vm.loadLicense = loadLicense;
		function loadLicense() {
		    IzinUsahaPrequalService.loadLicense({
		        Status: vm.PrequalStepID
		    }, function (response) {
		        var list = response.data;
		        for (var i = 0; i < list.length; i++) {
		            if (!(list[i].IssuedDate === null)) {
		                list[i].IssuedDate = UIControlService.getStrDate(list[i].IssuedDate);
		            }
		            if (!(list[i].ExpiredDate === null)) {
		                list[i].ExpiredDate = UIControlService.getStrDate(list[i].ExpiredDate);
		            }
		        }
		        vm.listLicensi = list;
		        loadPrequalStep();
		        cekSubmit();
		        loadCityCompany();
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.loadData = loadData;
		function loadData() {
		    UIControlService.loadLoading("MSG.LOADING");
		    IzinUsahaPrequalService.selectLicensi({ Status: vm.PrequalStepID }, function (response) {
		        UIControlService.unloadLoading();
		        if (response.status == 200) {
		            var list = response.data;
		            for (var i = 0; i < list.length; i++) {
		                if (!(list[i].IssuedDate === null)) {
		                    list[i].IssuedDate = UIControlService.getStrDate(list[i].IssuedDate);
		                }
		                if (!(list[i].ExpiredDate === null)) {
		                    list[i].ExpiredDate = UIControlService.getStrDate(list[i].ExpiredDate);
		                }
		            }
		            vm.listLicensi = list;
		        } else {
		            UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
		            return;
		        }
		    }, function (err) {
		        UIControlService.msg_growl("error", "MESSAGE.API");
		        UIControlService.unloadLoading();
		        return;
		    });
		}

		vm.cekSubmit = cekSubmit;
		function cekSubmit() {
		    IzinUsahaPrequalService.CekSubmit({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isSubmit = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}


		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    VendorPrequalEntryService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.jLoad = jLoad;
		function jLoad() {
			UIControlService.loadLoading("MSG.LOADING");
			IzinUsahaPrequalService.selectLicensi({Status: vm.PrequalStepID},function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
				    var list = response.data;
				    loadCityCompany();
					for (var i = 0; i < list.length; i++) {
						if (!(list[i].IssuedDate === null)) {
							list[i].IssuedDate = UIControlService.getStrDate(list[i].IssuedDate);
						}
						if (!(list[i].ExpiredDate === null)) {
							list[i].ExpiredDate = UIControlService.getStrDate(list[i].ExpiredDate);
						}
					}
					vm.listLicensi = list;
				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}

		vm.loadCityCompany = loadCityCompany;
		function loadCityCompany() {
		    IzinUsahaPrequalService.loadContact({
		        Status: vm.PrequalStepID
		    }, function (reply) {
				if (reply.status == 200) {
				    vm.contactCompany = reply.data;
					for (var i = 0; i < vm.contactCompany.length; i++) {
						if (vm.contactCompany[i].VendorContactType.Name === 'VENDOR_CONTACT_TYPE_COMPANY') {
							vm.cityID = vm.contactCompany[i].Contact.Address.State.Country.CountryID;
							break
						}
					}
                    
				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}

		vm.deleteLic = deleteLic;
		function deleteLic(lic) {
			vm.lic = lic;
			bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
				if (reply) {
					UIControlService.loadLoading("DELETING");
					IzinUsahaPrequalService.deleteLic({ ID: lic.ID }, function (reply) {
						if (reply.status == 200) {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("success", "MESSAGE.DELETE_SUCCESS");
							init();
						} else {
							UIControlService.unloadLoading();
							UIControlService.msg_growl("error", "MESSAGE.DELETE_FAILED");
							return;
						}
					}, function (err) {
						UIControlService.unloadLoading();
						UIControlService.msg_growl("error", "MESSAGE.DELETE.FAILED");
						return;
					});
				}
			});
		}

		//open form
		vm.openForm = openForm;
		function openForm(data, isForm) {
			vm.dataUpdate = {
				item: data,
				isForm: isForm,
				cityID: vm.cityID,
				VendorEntryPrequal: {
				    PrequalSetupStepID: vm.PrequalStepID
				}
			}
			var temp;
			if (isForm === true) {
				temp = "app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/izin-usaha/form-izin-usaha.html";
			} else {
			    temp = "app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/izin-usaha/detail-izin-usaha.html";
			}
			var modalInstance = $uibModal.open({
				templateUrl: temp,
				controller: 'FormIzinPrequalCtrl',
				controllerAs: 'FormIzinPrequalCtrl',
				resolve: {
					item: function () {
						return vm.dataUpdate;
					}
				}
			});
			modalInstance.result.then(function () {
			    init();
			});
		}

		vm.back = back;
		function back() {
		    $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		}

		vm.Submit = Submit;
		function Submit() {
		    if (vm.isSubmit == false) {
		        UIControlService.msg_growl("error", "Data Belum lengkap");
		        return;

		    }
		    else {
		        IzinUsahaPrequalService.Submit({ Status: vm.PrequalStepID }, function (reply) {
		            if (reply.status == 200) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", "Success Submit");
		                $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		            } else {
		                UIControlService.unloadLoading();
		                return;
		            }
		        }, function (err) {
		            UIControlService.unloadLoading();
		            return;
		        });

		    }
		}
	}
})();