(function () {
	'use strict';

	angular.module("app").controller("formExpCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', 'VendorExperienceService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService', 'UploaderService', 'item', 'ProvinsiService', 'AuthService', '$uibModalInstance'];
	/* @ngInject */
	function ctrl($translatePartialLoader, VendorExperienceService, UIControlService, GlobalConstantService, UploadFileConfigService, UploaderService, item, ProvinsiService, AuthService, $uibModalInstance) {
		var vm = this;
		vm.isAdd = item.isAdd;
		vm.dataExp = {};
		vm.PrequalStepID = item.PrequalStepID;
		var dataEdit = item.item;
		if (vm.isAdd != true) {
			vm.CurrencyRef = dataEdit.CurrencyRef;
		}
		vm.fileUpload;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.isCalendarOpened = [false, false, false, false];
		vm.IDN_id = 360;
		vm.regionID;
		vm.countryID;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.datepelaksanaan;
		vm.dateselesai;
		vm.lokasidetail;
		vm.IsCR = item.isCR;
		vm.selectedCurrencies = {};
		vm.listCurrencies = [];

		//if (vm.isAdd === false) {
		//	vm.cekTemporary = dataEdit.IsTemporary;
		//}

		vm.init = init;
		function init() {
			loadTypeExp();
			if (vm.isAdd === true) {
				vm.dataExp = new dataField('', null, '', '', '', '', 0, null, null, new Date(), 0, 0, '', 0, '', null, 0, vm.PrequalStepID);
				loadCountries();
			} else {
				vm.dataExp = new dataField(dataEdit.ContractName, dataEdit.Location, dataEdit.Address, dataEdit.Agency, dataEdit.AgencyTelpNo,
                    dataEdit.ContractNo, dataEdit.ContractValue, new Date(Date.parse(dataEdit.StartDate)),
                    new Date(Date.parse(dataEdit.EndDate)), new Date(Date.parse(dataEdit.UploadDate)), dataEdit.Field, dataEdit.FieldType,
                    dataEdit.Remark, dataEdit.ExperienceType, dataEdit.DocumentURL, dataEdit.StateLocation, dataEdit.ExpCurrID, vm.PrequalStepID);

				vm.regionID = dataEdit.StateLocationRef.Country.Continent.ContinentID;
				vm.countryID = dataEdit.StateLocationRef.CountryID;
				vm.datepelaksanaan = UIControlService.getStrDate(vm.dataExp.StartDate);
				vm.dateselesai = UIControlService.getStrDate(vm.dataExp.EndDate);
				if (vm.countryID === 360) {
					vm.lokasidetail = dataEdit.CityLocation.Name + ", " + dataEdit.StateLocationRef.Name + ", " + dataEdit.StateLocationRef.Country.Name + ", " + dataEdit.StateLocationRef.Country.Continent.Name;
				} else {
					vm.lokasidetail = dataEdit.StateLocationRef.Name + ", " + dataEdit.StateLocationRef.Country.Name + ", " + dataEdit.StateLocationRef.Country.Continent.Name;
				}
				loadCountries(dataEdit.StateLocationRef);
			}
			//loadCountries(dataEdit.StateLocationRef);
			loadTypeTender();
			getTypeSizeFile();
			loadCurrency(dataEdit);
		}

		vm.checkStartDate = checkStartDate;
		function checkStartDate(selectedStartDate) {
			var today = new Date();
			if (today < selectedStartDate) {
				UIControlService.msg_growl("warning", "ERROR.START_DATE");
				vm.dataExp.StartDate = null;
				return;
			}
		}

		vm.checkEndDate = checkEndDate;
		function checkEndDate(selectedEndDate, selectedStartDate, selectedExpType) {
			if (selectedEndDate < selectedStartDate) {
				UIControlService.msg_growl("error", "ERROR.END_DATE");
				vm.dataExp.EndDate = null;
				return;
			} else if (selectedExpType == 3153) {
				var convertedEndDate = moment(selectedEndDate).format('YYYY-MM-DD');
				var datenow = new Date();
				var convertedDateNow = moment(datenow).format('YYYY-MM-DD');
				if (datenow < selectedEndDate) {
					UIControlService.msg_growl("error", "ERROR.END_DATE_FINISH_CONTRACT");
					vm.dataExp.EndDate = null;
					return;
				}
			} else if (selectedExpType == 3154) { //pekerjaan yg sedang berlangsung
				var convertedEndDate = moment(selectedEndDate).format('YYYY-MM-DD');
				var datenow = new Date();
				var convertedDateNow = moment(datenow).format('YYYY-MM-DD');
				if (datenow > selectedEndDate) {
					UIControlService.msg_growl("error", "ERROR.END_DATE_CURRENT_CONTRACT");
					vm.dataExp.EndDate = null;
					return;
				}
			}
		}

		vm.listTypeExp = [];
		vm.cekTypeExp = 3154;
		function loadTypeExp() {
			UIControlService.loadLoading("Loading . . .");
			VendorExperienceService.typeExperience(function (reply) {
				UIControlService.unloadLoading();
				vm.listTypeExp = reply.data.List;

			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API.TYPE_EXP");
				UIControlService.unloadLoading();
			});
		}

		function getTypeSizeFile() {
			UploadFileConfigService.getByPageName("PAGE.VENDOR.EXPERIENCE", function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					vm.idUploadConfigs = response.data;
					vm.idFileTypes = UIControlService.generateFilterStrings(response.data);
					vm.idFileSize = vm.idUploadConfigs[0];

				} else {
					UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
				return;
			});
		}

		vm.loadStates = loadStates;
		function loadStates(country) {
			if (!country) {
				country = vm.selectedCountry;
				vm.selectedState = "";
			}
			loadRegions(country.CountryID);
			UIControlService.loadLoading("LOADERS.LOADING_STATE");
			VendorExperienceService.getStates(country.CountryID, function (response) {
				vm.stateList = response.data;
				for (var i = 0; i < vm.stateList.length; i++) {
					if (country.StateID === vm.stateList[i].StateID) {
						vm.selectedState = vm.stateList[i];
						if (vm.selectedState.Country.Code === 'IDN') {
							changeState();
							break;
						}
					}
				}
				UIControlService.unloadLoading();
			});
		}

		vm.loadCountries = loadCountries;
		function loadCountries(data) {
			UIControlService.loadLoading("LOADERS.LOADING_COUNTRY");
			VendorExperienceService.getCountries(function (response) {
				vm.countryList = response.data;
				for (var i = 0; i < vm.countryList.length; i++) {
					if (data !== undefined) {
						if (data.CountryID === vm.countryList[i].CountryID) {
							vm.selectedCountry = vm.countryList[i];
							loadStates(data);
							break;
						}
					}
				}
				UIControlService.unloadLoading();
			});
		}

		vm.loadRegions = loadRegions;
		function loadRegions(data) {
			UIControlService.loadLoading("LOADERS.LOADING_REGION");
			VendorExperienceService.getRegions({ CountryID: data }, function (response) {
				vm.selectedRegion = response.data;
				UIControlService.unloadLoading();
			});
		}

		vm.changeState = changeState;
		vm.listCities = [];
		vm.selectedCities;
		function changeState() {
			if (!(vm.selectedState.CountryID === vm.IDN_id)) {
				vm.dataExp.Location = null;
				vm.dataExp.StateLocation = vm.selectedState.StateID;
			}
			ProvinsiService.getCities(vm.selectedState.StateID, function (response) {
				vm.listCities = response.data;
				if (vm.isAdd === false) {
					for (var i = 0; i < vm.listCities.length; i++) {
						if (vm.dataExp.Location === vm.listCities[i].CityID) {
							vm.selectedCities = vm.listCities[i];
							changeCities();
							break;
						}
					}
				}
			}, function (response) {
				UIControlService.msg_growl("error", "Gagal Akses API");
				return;
			});
		}

		vm.changeCities = changeCities;
		function changeCities() {
			vm.dataExp.StateLocation = vm.selectedState.StateID;
			if (vm.selectedCountry.CountryID === vm.IDN_id) {
				vm.dataExp.Location = vm.selectedCities.CityID;
			}

		}
		//end city dkk

		vm.openCalendar = openCalendar;
		function openCalendar(index) {
			vm.isCalendarOpened[index] = true;
		};

		/*field*/
		vm.selectedTypeTender;
		vm.listTypeTender;
		function loadTypeTender() {
			VendorExperienceService.getTypeTender(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listTypeTender = reply.data.List;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listTypeTender.length; i++) {
							if (vm.listTypeTender[i].RefID === vm.dataExp.FieldType) {
								vm.selectedTypeTender = vm.listTypeTender[i];
								loadBusinessField();
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.loadCurrency = loadCurrency;
		vm.selectedCurrencies = [];
		function loadCurrency(data) {
			VendorExperienceService.getCurrencies(function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					vm.listCurrencies = reply.data;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listCurrencies.length; i++) {
							if (data.ExpCurrID === vm.listCurrencies[i].CurrencyID) {
								vm.selectedCurrencies = vm.listCurrencies[i];
								break;
							}
						}
					}
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
				UIControlService.unloadLoading();
			});
		}

		vm.changeCurr = changeCurr;
		function changeCurr(data) {
			vm.dataExp.ExpCurrID = data.CurrencyID;
		}

		vm.listBussinesDetailField = []
		vm.changeTypeTender = changeTypeTender;
		function changeTypeTender() {
			if (vm.selectedTypeTender === undefined) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_TYPEVENDOR");
				return;
			}
			vm.dataExp.FieldType = vm.selectedTypeTender.RefID;
			loadBusinessField();
			vm.listBussinesDetailField = [];
		}

		vm.loadBusinessField = loadBusinessField;
		vm.selectedBusinessField;
		vm.listBusinessField = [];
		function loadBusinessField() {
			VendorExperienceService.SelectBusinessField({
				GoodsOrService: vm.dataExp.FieldType
			}, function (response) {
				if (response.status === 200) {
					vm.listBusinessField = response.data;
					if (vm.isAdd === false) {
						for (var i = 0; i < vm.listBusinessField.length; i++) {
							if (vm.listBusinessField[i].ID === vm.dataExp.Field) {
								vm.selectedBusinessField = vm.listBusinessField[i];
								break;
							}
						}
					}
				} else {
					UIControlService.msg_growl("error", "Gagal mendapatkan list bidang usaha");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "Gagal Akses API");
				return;
			});
		}

		vm.changeBusinessField = changeBusinessField;
		function changeBusinessField() {
			vm.dataExp.Field = vm.selectedBusinessField.ID;
		}
		/*end field*/

		/*proses upload file*/
		function uploadFile() {
			AuthService.getUserLogin(function (reply) {
				vm.dataExpLogin = reply.data.CurrentUsername + "_expe";
				if (validateFileType(vm.fileUpload, vm.idUploadConfigs)) {
					upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, vm.dataExpLogin);
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.API");
			});

		}

		function upload(file, config, filters, dates, callback) {
			var size = config.Size;
			var unit = config.SizeUnitName;
			if (unit == 'SIZE_UNIT_KB') {
				size *= 1024;
			}

			if (unit == 'SIZE_UNIT_MB') {
				size *= (1024 * 1024);
			}

			UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
			UploaderService.uploadSingleFile(file, "UPLOAD_DIRECTORIES_VENDORDATA", size, filters, dates, function (response) {
				UIControlService.unloadLoading();
				if (response.status == 200) {
					var url = response.data.Url;
					vm.dataExp.DocumentURL = url;
					vm.pathFile = vm.folderFile + url;
					UIControlService.msg_growl("success", "MESSAGE.SUC_UPLOAD");
					saveProcess();

				} else {
					UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
					return;
				}
			}, function (response) {
				UIControlService.msg_growl("error", "MESSAGE.API")
				UIControlService.unloadLoading();
			});

		}

		function validateFileType(file, allowedFileTypes) {
			if (!file || file.length == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
				return false;
			}
			return true;
		}

		/*proses simpan*/
		vm.saveData = saveData;
		function saveData() {
			if (vm.dataExp.ContractName == "") {
				UIControlService.msg_growl("error", "MESSAGE.ERR_CONTRACTNAME");
				return;
			} else if (vm.dataExp.Agency == "") {
				UIControlService.msg_growl("error", "MESSAGE.ERR_AGENCY");
				return;
			} else if (vm.selectedTypeTender == undefined) {
				UIControlService.msg_growl("error", "MESSAGE.TYPETENDER");
				return;
			} else if (vm.selectedBusinessField == undefined) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_BUSINESSFIELD");
				return;
			} else if (vm.selectedCountry == undefined) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY");
				return;
			} else if (vm.selectedState == "") {
				UIControlService.msg_growl("error", "MESSAGE.ERR_STATE");
				return;
			}
			if (vm.selectedCountry.Code == "IDN") {
				if (vm.dataExp.Location == null) {
					UIControlService.msg_growl("error", "MESSAGE.ERR_CITY");
					return;

				}
			}
			if (vm.dataExp.Address == "") {
				UIControlService.msg_growl("error", "MESSAGE.ERR_ADDRESS");
				return;
			} else if (vm.dataExp.AgencyTelpNo == "") {
				UIControlService.msg_growl("error", "MESSAGE.ERR_AGENCYTELPNO");
				return;
			} else if (vm.dataExp.ContractNo == "") {
				UIControlService.msg_growl("error", "MESSAGE.ERR_CONTRACTNO");
				return;
			} else if (vm.dataExp.ExpCurrID == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_CURR");
				return;
			} else if (vm.dataExp.ContractValue == 0) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_CONTRACTVALUE");
				return;
			} else if (vm.dataExp.StartDate == null) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_STARTDATE");
				return;
			} else if (vm.dataExp.EndDate == null) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_ENDDATE");
				return;
			} else if (vm.fileUpload === undefined && vm.isAdd === true) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_EXPERIENCEFILE");
				return;
			} else if (!(vm.fileUpload === undefined)) {
				uploadFile();
			} else if (vm.fileUpload === undefined && vm.isAdd === false) {
				saveProcess();
			}
		}

		function saveProcess() {
			vm.dataExp.ExperienceType = Number(vm.dataExp.ExperienceType);
			vm.dataExp.PrequalStepID = vm.PrequalStepID;
			if (vm.isAdd == true) {
				vm.dataExp['ID'] = 0;
				vm.dataExp['ExpCurrID'] = vm.dataExp.ExpCurrID;
				vm.dataExp['StateLocation'] = vm.selectedState.StateID;
				VendorExperienceService.Insert(vm.dataExp, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
						$uibModalInstance.close();
					}
				}, function (err) {
					UIControlService.unloadLoadingModal();
				});
			}
			else if (vm.isAdd === false) {
				vm.dataExp['ID'] = dataEdit.ID;
				VendorExperienceService.Insert(vm.dataExp, function (reply) {
					UIControlService.unloadLoading();
					if (reply.status === 200) {
						UIControlService.msg_growl("success", "MESSAGE.SUC_SAVE");
						$uibModalInstance.close();
					}
				}, function (err) {
					UIControlService.msg_growl("error", "MESSAGE.ERR_API");
					UIControlService.unloadLoadingModal();
				});
			}

		}

		/*end proses simpan*/

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}

		function dataField(ContractName, Location, Address, Agency, AgencyTelpNo, ContractNo, ContractValue, StartDate,
            EndDate, UploadDate, Field, FieldType, Remark, ExperienceType, DocumentURL, StateLocation, ExpCurrID, PrequalStepID) {
			var self = this;
			self.ContractName = ContractName;
			self.Location = Location;
			self.Address = Address;
			self.Agency = Agency;
			self.AgencyTelpNo = AgencyTelpNo;
			self.ContractNo = ContractNo;
			self.ContractValue = ContractValue;
			self.StartDate = StartDate;
			self.EndDate = EndDate;
			self.UploadDate = UploadDate;
			self.Field = Field;
			self.FieldType = FieldType;
			self.Remark = Remark;
			self.ExperienceType = ExperienceType;
			self.DocumentURL = DocumentURL;
			self.StateLocation = StateLocation;
			self.ExpCurrID = ExpCurrID;
			PrequalStepID = PrequalStepID;
		}
	}
})();

