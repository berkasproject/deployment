(function () {
	'use strict';

	angular.module("app").controller("FormBuildingPrequalController", ctrl);

	ctrl.$inject = ['$uibModalInstance', 'DataPerlengkapanPrequalService', 'item', 'ProvinsiService', 'UIControlService'];
	/* @ngInject */
	function ctrl($uibModalInstance, DataPerlengkapanPrequalService, item, ProvinsiService, UIControlService) {
		var vm = this;
		vm.dataBuilding = {};
		vm.isAdd = item.isForm;
		vm.tipeform = item.type;
		vm.title = "Data Bangunan dan Infrastruktur";
		vm.FullAddress;

		vm.init = init;
		function init() {
			if (vm.isAdd === true) {
				vm.dataBuilding = new BuildingField(0, 0, 0, "", 0);
			}
			else {
				vm.ID = item.data.ID;
				vm.dataBuilding = new BuildingField(item.data.VendorID, item.data.category, item.data.BuildingArea,
                    item.data.address.AddressInfo, item.data.ownershipStatus);
				vm.FullAddress = item.data.address.AddressInfo + " , " + item.data.address.AddressDetail
			}
			getProvinsi();
			loadCategory();
			loadKepemilikan();
		}

		vm.listCategory = [];
		vm.selectedCategory;
		function loadCategory() {
			DataPerlengkapanPrequalService.getCategoryBuilding(
            function (reply) {
            	vm.listCategory = reply.data.List;
            	if (vm.isAdd === false) {
            		for (var i = 0; i < vm.listCategory.length; i++) {
            			if (item.data.Category === vm.listCategory[i].RefID) {
            				vm.selectedCategory = vm.listCategory[i];
            				break;
            			}
            		}
            	}
            }, function (err) {
            	UIControlService.msg_growl("error", "Gagal mendapat daftar opsi kategori");
            });
		}

		vm.listKepemilikan = [];
		vm.selectedKepemilikan;
		function loadKepemilikan() {
			DataPerlengkapanPrequalService.getOwnership(
            function (reply) {
            	vm.listKepemilikan = reply.data.List;
            	if (vm.isAdd === false) {
            		for (var i = 0; i < vm.listKepemilikan.length; i++) {
            			if (item.data.OwnershipStatus === vm.listKepemilikan[i].RefID) {
            				vm.selectedKepemilikan = vm.listKepemilikan[i];
            				break;
            			}
            		}
            	}
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal mendapat daftar opsi kepemilikan");
            });
		}

		/* begin provinsi, kabupaten, kecamatan */
		vm.getProvinsi = getProvinsi;
		vm.selectedProvince;
		function getProvinsi() {
		    UIControlService.loadLoadingModal("");
		    ProvinsiService.getStates(360, function (response) {
		        UIControlService.unloadLoadingModal();
                vm.listProvinsi = response.data;
                if (vm.isAdd === false) {
               	    for (var i = 0; i < vm.listProvinsi.length; i++) {
               		    if (item.data.address.StateID === vm.listProvinsi[i].StateID) {
               		        vm.selectedProvince = vm.listProvinsi[i];
               			    vm.changeProvince();
               			    break;
               		    }
               	    }
                }
            }, function (response) {
           	    UIControlService.unloadLoadingModal();
           	    UIControlService.msg_growl("error", "Gagal mendapat daftar propinsi");
           });
		}

		vm.changeProvince = changeProvince;
		vm.selectedKabupaten;
		function changeProvince() {
		    UIControlService.loadLoadingModal("");
		    ProvinsiService.getCities(vm.selectedProvince.StateID, function (response) {
		        UIControlService.unloadLoadingModal();
                vm.listKabupaten = response.data;
                if (vm.isAdd === false) {
               	    for (var i = 0; i < vm.listKabupaten.length; i++) {
               		    if (item.data.address.CityID === vm.listKabupaten[i].CityID) {
               			    vm.selectedKabupaten = vm.listKabupaten[i];
               			    vm.changeCities();
               			    break;
               		    }
               	    }
                }
            }, function (response) {
           	    UIControlService.unloadLoadingModal();
           	    UIControlService.msg_growl("error", "Gagal mendapat daftar kota/kabupaten");
           });
		}

		vm.changeCities = changeCities;
		vm.selectedKecamatan;
		function changeCities() {
		    if (!vm.selectedKabupaten) {
		        vm.listKecamatan = [];
		        return;
		    }
		    UIControlService.loadLoadingModal("");
		    ProvinsiService.getDistrict(vm.selectedKabupaten.CityID, function (response) {
		        UIControlService.unloadLoadingModal();
                vm.listKecamatan = response.data;
                if (vm.isAdd === false) {
               	    for (var i = 0; i < vm.listKecamatan.length; i++) {
               		    if (item.data.address.DistrictID === vm.listKecamatan[i].DistrictID) {
               			    vm.selectedKecamatan = vm.listKecamatan[i];
               			    break;
               		    }
               	    }
                }
			}, function (response) {
                UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("error", "Gagal mendapat daftar kecamatan");
           });

		}
		/* end provinsi, kabupaten, kecamatan */

		vm.batal = batal;
		function batal() {
			$uibModalInstance.dismiss('cancel');
		}

		vm.saveData = saveData;
		function saveData() {
			if (!vm.selectedCategory) {
				UIControlService.msg_growl("warning", "MESSAGE.NO_CATEGORY");
				return;
			}
			if (!vm.selectedKepemilikan) {
				UIControlService.msg_growl("error", "MESSAGE.NO_OWNERSHIP");
				return;
			}
			if (!vm.selectedKabupaten) {
				UIControlService.msg_growl("error", "MESSAGE.NO_CITY");
				return;
			}
			if (!vm.selectedProvince) {
				UIControlService.msg_growl("error", "MESSAGE.NO_PROVINCE");
				return;
			}
			if (!vm.selectedKecamatan) {
				UIControlService.msg_growl("error", "MESSAGE.NO_DISTRICT");
				return;
			}

			var addr = {
			    AddressID: item.data.BuildingAddress,
				AddressDetail: vm.selectedKecamatan.Name + " , " + vm.selectedKabupaten.Name + " , " + vm.selectedProvince.Name,
				AddressInfo: vm.dataBuilding.Address,
				StateID: vm.selectedProvince.StateID,
				CityID: vm.selectedKabupaten.CityID,
				DistrictID: vm.selectedKecamatan.DistrictID
			}

			UIControlService.loadLoadingModal("");
			DataPerlengkapanPrequalService.saveBuilding({
			    ID: item.data.ID,
				address: addr,
				BuildingArea: vm.dataBuilding.BuildingArea,
				Category: vm.selectedCategory.RefID,
				OwnershipStatus: vm.selectedKepemilikan.RefID,
				PrequalStepID: item.PrequalStepID
			}, function (reply) {
			    UIControlService.unloadLoadingModal();
                UIControlService.msg_growl("success", "MESSAGE.SUCCES_SAVE_BUILDING");
                $uibModalInstance.close();
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", "MESSAGE.ERR_SAVE_BUILDING");
			});
		}

		function BuildingField(VendorID, Category, BuildingArea, Address, OwnershipStatus) {
			var self = this;
			self.VendorID = VendorID;
			self.Category = Category;
			self.BuildingArea = BuildingArea;
			self.Address = Address;
			self.OwnershipStatus = OwnershipStatus;
		}
	};
})();