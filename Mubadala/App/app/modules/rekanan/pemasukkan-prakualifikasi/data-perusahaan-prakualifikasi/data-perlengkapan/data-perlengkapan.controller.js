(function () {
	'use strict';
	angular.module("app")
    .controller("DataPerlengkapanPrequalController", ctrl);

	ctrl.$inject = ['$filter', '$state', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$stateParams', 'DataPerlengkapanPrequalService', 'UIControlService'];
	/* @ngInject */
	function ctrl($filter,$state, $http, $uibModal, $translate, $translatePartialLoader, $stateParams, DataPerlengkapanPrequalService, UIControlService) {

	    var vm = this;

	    vm.PrequalStepID = Number($stateParams.PrequalStepID);

	    var limit = 10;
	    
		vm.msgLoading = "MESSAGE.LOADING";
		vm.currentPageBuild = 1;
		vm.currentPageVehic = 1;
		vm.currentPageEquip = 1;
		vm.totalBuild = 0;
		vm.totalVehic = 0;
		vm.totalEquip = 0;
		vm.limit = limit;
		vm.flagEquipment = false;
		vm.init = init;
		function init() {

		    DataPerlengkapanPrequalService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
                vm.isEntry = reply.data;
		    }, function (error) {
		        UIControlService.msg_growl("error", "Gagal mendapat tanggal pemasukan dokumen prakualifikasi");
		    });

		    $translatePartialLoader.addPart("pemasukkan-prakualifikasi");
		    $translatePartialLoader.addPart("data-perlengkapan");
			loadBuilding(true);
			//loadEquipmentVehicle(1);
			//loadEquipmentTools(1);
		}

		vm.listBuilding = [];
		vm.loadBuilding = loadBuilding;
		function loadBuilding(initCall) {
		    var offset = (vm.currentPageBuild * limit) - limit;
			UIControlService.loadLoading(vm.msgLoading);
			DataPerlengkapanPrequalService.selectBuilding({
			    Status : vm.PrequalStepID,
			    Offset: offset,
			    Limit: limit
			},
            function (reply) {
            	UIControlService.unloadLoading();
            	vm.listBuilding = reply.data.List;
            	if (reply.data.Count != 0) vm.flagEquipment = true;
            	vm.totalBuild = reply.data.Count;
            	if (initCall) {
            	    loadEquipmentVehicle(true);
            	}
            }, function (err) {
            	UIControlService.unloadLoading();
            });
		}

		vm.listVehicle = [];
		vm.loadEquipmentVehicle = loadEquipmentVehicle;
		function loadEquipmentVehicle(initCall) {
		    var offset = (vm.currentPageVehic * limit) - limit;
			UIControlService.loadLoading(vm.msgLoading);
			DataPerlengkapanPrequalService.selectEquipment({
			    Status: vm.PrequalStepID,
			    Offset: offset,
			    Limit: limit,
                column: 1 //kendaraan
			},
            function (reply) {
            	UIControlService.unloadLoading();
            	vm.listVehicle = reply.data.List;
            	vm.totalVehic = reply.data.Count;
            	if (reply.data.Count != 0) vm.flagEquipment = true;
            	if (initCall) {
            	    loadEquipmentTools();
            	}
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal mendapat data kendaraan");
            	UIControlService.unloadLoading();
            });
		}

		vm.listEquipmentTools = [];
		vm.loadEquipmentTools = loadEquipmentTools;
		function loadEquipmentTools() {
		    var offset = (vm.currentPageEquip * limit) - limit;
			UIControlService.loadLoading(vm.msgLoading);
			DataPerlengkapanPrequalService.selectEquipment({
			    Status: vm.PrequalStepID,
			    Offset: offset,
			    Limit: limit,
			    column: 2 //peralatan
			},
            function (reply) {
            	UIControlService.unloadLoading();
            	vm.listEquipmentTools = reply.data.List;
            	vm.totalEquip = reply.data.Count;
            	if (reply.data.Count != 0) vm.flagEquipment = true;
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal mendapat data perlengkapan");
            	UIControlService.unloadLoading();
            });
		}

		vm.openForm = openForm;
		function openForm(type, data, isAdd) {
			var data = {
				type: type,
				data: data ? data : {},
				isForm: isAdd,
				PrequalStepID: vm.PrequalStepID
			}
			var temp;
			var ctrl;
			var ctrlAs;
			if (type === "building") {
				temp = "app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-perlengkapan/formBuilding.html";
				ctrl = "FormBuildingPrequalController";
				ctrlAs = "FormBuildingCtrl";
			} else {
			    temp = "app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-perlengkapan/formNonBuilding.html";
				ctrl = "FormNonBuildingPrequalController";
				ctrlAs = "FormNonBuildingCtrl";
			}
			var modalInstance = $uibModal.open({
				templateUrl: temp,
				controller: ctrl,
				controllerAs: ctrlAs,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
			    switch (type) {
			        case "building": loadBuilding(false); break;
			        case "vehicle": loadEquipmentVehicle(false); break;
			        case "equipment": loadEquipmentTools(false); break;
			    }
			});
		}

		vm.detailForm = detailForm;
		function detailForm(type, data, isAdd) {
			var data = {
				type: type,
				data: data,
				isForm: isAdd
			}
			var ctrl;
			var ctrlAs;
			if (type === "building") {
				ctrl = "FormBuildingPrequalController";
				ctrlAs = "FormBuildingCtrl";
			} else {
				ctrl = "FormNonBuildingPrequalController";
				ctrlAs = "FormNonBuildingCtrl";
			}

			var modalInstance = $uibModal.open({
			    templateUrl: "app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-perlengkapan/detailData.html",
				controller: ctrl,
				controllerAs: ctrlAs,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.deleteBuilding = deleteBuilding;
		function deleteBuilding(data) {
		    bootbox.confirm($filter('translate')('Anda yakin untuk menghapus data ini?'), function (yes) {
		        if (yes) {
		            UIControlService.loadLoading("Silahkan Tunggu");
		            DataPerlengkapanPrequalService.deleteBuilding({
		                ID: data.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", "MESSAGE.SUCCESS_DELETE");
		                loadBuilding(false);
		            }, function (err) {
		                UIControlService.unloadLoading();
		            });
		        }
		    });
		}

		vm.deleteVehicle = deleteVehicle;
		function deleteVehicle(data) {
		    bootbox.confirm($filter('translate')('Anda yakin untuk menghapus data ini?'), function (yes) {
		        if (yes) {
		            UIControlService.loadLoading("Silahkan Tunggu");
		            DataPerlengkapanPrequalService.deleteEquipment({
		                ID: data.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", "MESSAGE.SUCCESS_DELETE");
		                loadEquipmentVehicle(false);
		            }, function (err) {
		                UIControlService.unloadLoading();
		            });
		        }
		    });
		}

		vm.deleteEquipment = deleteEquipment;
		function deleteEquipment(data) {
		    bootbox.confirm($filter('translate')('Anda yakin untuk menghapus data ini?'), function (yes) {
		        if (yes) {
		            UIControlService.loadLoading("Silahkan Tunggu");
		            DataPerlengkapanPrequalService.deleteEquipment({
		                ID: data.ID
		            }, function (reply) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", "Berhasil menghapus data peralatan");
		                loadEquipmentTools(false);
		            }, function (err) {
		                UIControlService.msg_growl("error", "Gagal menghapus data peralatan");
		                UIControlService.unloadLoading();
		            });
		        }
		    });
		}

		vm.Submit = Submit;
		function Submit() {

		    if (vm.flagEquipment == false) {
		        UIControlService.msg_growl("error", 'Data Belum lengkap');
		        return;
		    }
		    else {
		        UIControlService.loadLoading("Silahkan Tunggu");
		        DataPerlengkapanPrequalService.Submit({
		            Status: vm.PrequalStepID
		        }, function (reply2) {
		            UIControlService.unloadLoading();
		            if (reply2.status === 200) {
		                UIControlService.msg_growl('notice', 'Success Submit');
		                $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		            }
		        }, function (error) {
		            UIControlService.unloadLoading();
		        });
		    }

		}
	}
})();