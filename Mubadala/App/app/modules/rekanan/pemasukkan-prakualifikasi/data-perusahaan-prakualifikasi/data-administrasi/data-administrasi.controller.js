﻿(function () {
    'use strict';

    angular.module("app").controller("DataAdministrasiPrequalCtrl", ctrl);

    ctrl.$inject = ['$timeout', '$http', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', '$uibModal', '$state', '$stateParams', 'GlobalConstantService', 'AdministrasiPrequalService', 'DataAdministrasiService', 'VendorRegistrationService', 'UploadFileConfigService', 'UploaderService'];
    function ctrl($timeout, $http, $translate, $translatePartialLoader, $location, SocketService, UIControlService, $uibModal, $state, $stateParams, GlobalConstantService, AdministrasiPrequalService, DataAdministrasiService, VendorRegistrationService, UploadFileConfigService, UploaderService) {

        var vm = this;
        vm.folderFile = GlobalConstantService.getConstant('api') + "/";
        vm.PrequalStepID = Number($stateParams.PrequalStepID);

        vm.init = init;
        function init() {
            vm.flagSubmit = true;
            vm.totalItems = 0;
            vm.currentPage = 1;
            vm.pageSize = 10;
            vm.init = init;
            vm.NegoId = 0;
            vm.IsSubmit = null;
            vm.listPersonal = [];
            vm.isCalendarOpened = [false, false, false, false];
            vm.administrasiDate = {};
            vm.listCurrFalse = [];
            vm.listPersFalse = [];
            vm.IsEdit = false;
            vm.IsEditAlter = false;
            vm.addressFlag = 0;
            vm.ContactName = "";
            vm.addressFlagAlternatif = 0;
            vm.listOfficeAddress = {};
            vm.listOfficeAddressAlternatif = {};
            vm.AddressAlterId = 0;
            vm.listCompanyContact = {};

            $translatePartialLoader.addPart("pemasukkan-prakualifikasi");
            $translatePartialLoader.addPart("data-administrasi");
            UIControlService.loadLoading("Silahkan Tunggu...");
            CekVendorEntry();
            vm.fileUploadNPWP = undefined;
            vm.fileUpload = undefined;
            loadPrequalStep();
            cekSubmit();

        }

        vm.cekSubmit = cekSubmit;
        function cekSubmit() {
            AdministrasiPrequalService.cekSubmit({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.flagSubmit = reply.data;
                }
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.CekVendorEntry = CekVendorEntry;
        function CekVendorEntry() {
            AdministrasiPrequalService.CekVendorEntryPrequal({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    loadVendorContactPrequal();
                    loadVendorCurrencyPrequal();
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadPrequalStep = loadPrequalStep;
        function loadPrequalStep() {
            AdministrasiPrequalService.loadPrequalStep({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.isEntry = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorContactPrequal = loadVendorContactPrequal;
        function loadVendorContactPrequal() {
            AdministrasiPrequalService.loadContact({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listContact = reply.data;
                    vm.listContact.forEach(function (contact) {
                        if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_COMPANY") {
                            vm.listCompanyContact = contact.Contact;
                            loadCountryAdmin(contact.Contact.Address.State);
                            vm.VendorContactTypeCompany = contact.VendorContactType;
                            vm.CityCompany = contact.Contact.Address.City;
                            vm.DistrictCompany = contact.Contact.Address.Distric;
                            vm.Region = contact.Contact.Address.State.Country.Continent.Name;
                            vm.administrasi = contact.Vendor;
                            vm.administrasi.VendorName = contact.Vendor.VendorName;
                            vm.PKPNumber = contact.Vendor.PKPNumber;
                            vm.administrasi.FoundedDate = new Date(Date.parse(vm.administrasi.FoundedDate));
                            if (contact.Contact.Phone != null) {
                                vm.phone = contact.Contact.Phone.split(' ');
                                vm.Phone = (vm.phone[1]);
                                vm.phone = vm.phone[0].split(')');
                                vm.phone = vm.phone[0].split('(');
                                loadPhoneCodes(vm.phone[1]);
                            }
                            else loadPhoneCodes(undefined);
                            vm.CountryCode = contact.Contact.Address.State.Country.Code;
                            loadTypeVendor(vm.administrasi);
                            loadSizeTypeFilePKP();
                            loadSizeTypeFileNPWP();
                            loadCurrency();
                            loadAssociation(vm.administrasi);
                            loadBusinessEntity();
                            loadCountryAlternatif();
                            loadFilePrefix();
                        }
                        else if (contact.VendorContactType.Name == "VENDOR_CONTACT_TYPE_PERSONAL") {
                            vm.VendorContactTypePers = contact.VendorContactType;
                            vm.listPersonal.push(contact);
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && contact.IsPrimary === null) {

                            vm.addressFlag = 1;
                            vm.ContactName = "Kantor Pusat";
                            vm.listOfficeAddress = contact.Contact;
                            vm.VendorContactType = contact.VendorContactType;
                            vm.addressInfo = contact.Contact.Address.AddressInfo;
                            vm.address1 = contact.Contact.Address.AddressInfo;
                            vm.cekAddress = vm.address1;
                            vm.postcalcode = contact.Contact.Address.PostalCode;
                            vm.cekPostCode = vm.postcalcode;
                            loadCountry(contact.Contact.Address.State);
                            vm.selectedState1 = contact.Contact.Address.State;
                            if (contact.Contact.Address.State.Country.Code === "IDN") {
                                vm.selectedCity1 = contact.Contact.Address.City;
                                vm.selectedDistrict1 = contact.Contact.Address.Distric;

                            }
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && contact.IsPrimary === null) {
                            if (vm.addressFlag == 0) {
                                vm.ContactName = "Kantor Cabang";
                                vm.listOfficeAddress = contact.Contact;
                                vm.VendorContactType = contact.VendorContactType;
                                vm.addressInfo = contact.Contact.Address.AddressInfo;
                                vm.address1 = contact.Contact.Address.AddressInfo;
                                vm.cekAddress = vm.address1;
                                vm.postcalcode = contact.Contact.Address.PostalCode;
                                vm.cekPostCode = vm.postcalcode;
                                loadCountry(contact.Contact.Address.State);
                                vm.selectedState1 = contact.Contact.Address.State;
                                if (contact.Contact.Address.State.Country.Code === "IDN") {
                                    vm.selectedCity1 = contact.Contact.Address.City;
                                    vm.selectedDistrict1 = contact.Contact.Address.Distric;

                                }
                            }
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_MAIN' && contact.IsPrimary === 2) {
                            vm.addressFlagAlternatif = 1;
                            vm.listOfficeAddressAlternatif = contact.Contact;
                            vm.VendorContactTypeAlternatif = contact.VendorContactType;
                            vm.addressInfo = contact.Contact.Address.AddressInfo;
                            vm.addressinfo = contact.Contact.Address.AddressInfo;
                            vm.ContactOfficeAlterId = contact.Contact.ContactID;
                            vm.AddressAlterId = contact.Contact.AddressID;
                            vm.cekAddress1 = vm.addressinfo;
                            vm.PostalCodeAlternatif = contact.Contact.Address.PostalCode;
                            vm.cekPostCode1 = vm.PostalCodeAlternatif;
                            loadCountryAlternatif(contact.Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
                            vm.selectedStateAlternatif1 = contact.Contact.Address.State;
                            if (contact.Contact.Address.State.Country.Code === "IDN") {
                                vm.selectedCityAlternatif1 = contact.Contact.Address.City;
                                vm.selectedDistrictAlternatif1 = contact.Contact.Address.Distric;

                            }
                        }
                        else if (contact.VendorContactType.Name === 'VENDOR_OFFICE_TYPE_BRANCH' && contact.IsPrimary === 2) {
                            if (vm.addressFlagAlternatif == 0) {
                                vm.listOfficeAddressAlternatif = contact.Contact;
                                vm.ContactOfficeAlterId = contact.Contact.ContactID;
                                vm.AddressAlterId = contact.Contact.AddressID;
                                vm.VendorContactTypeAlternatif = contact.VendorContactType;
                                vm.addressInfo = contact.Contact.Address.AddressInfo;
                                vm.addressinfo = contact.Contact.Address.AddressInfo;
                                vm.cekAddress1 = vm.addressinfo;
                                vm.PostalCodeAlternatif = contact.Contact.Address.PostalCode;
                                vm.cekPostCode1 = vm.PostalCodeAlternatif;
                                loadCountryAlternatif(contact.Contact.Address.State);//loadRegion(vm.contact[i].Contact.Address.State.Country.CountryID);
                                vm.selectedStateAlternatif1 = contact.Contact.Address.State;
                                if (contact.Contact.Address.State.Country.Code === "IDN") {
                                    vm.selectedCityAlternatif1 = contact.Contact.Address.City;
                                    vm.selectedDistrictAlternatif1 = contact.Contact.Address.Distric;

                                }
                            }
                        }
                    });
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorCurrencyPrequal = loadVendorCurrencyPrequal;
        function loadVendorCurrencyPrequal() {
            AdministrasiPrequalService.loadCurrency({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listCurrencies = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadVendorCommodityPrequal = loadVendorCommodityPrequal;
        function loadVendorCommodityPrequal() {
            AdministrasiPrequalService.loadCommodity({
                Status: vm.PrequalStepID
            }, function (reply) {
                if (reply.status == 200) {
                    vm.listBussinesDetailField = reply.data;
                }
                UIControlService.unloadLoading();
            }, function (error) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadFilePrefix = loadFilePrefix;
        function loadFilePrefix() {
            //UIControlService.loadLoading("LOADERS.LOADING_PREFIX");
            VendorRegistrationService.getUploadPrefix(
                function (response) {
                    var prefixes = response.data;
                    vm.prefixes = {};
                    for (var i = 0; i < prefixes.length; i++) {
                        vm.prefixes[prefixes[i].Name] = prefixes[i];
                    }
                    UIControlService.unloadLoading();
                }, handleRequestError);
        }

        vm.loadCountryAdmin = loadCountryAdmin;
        vm.selectedCountryAdmin;
        vm.listCountry = [];
        function loadCountryAdmin(data) {
            DataAdministrasiService.SelectCountry(function (response) {
                vm.listCountryAdmin = response.data;
                for (var i = 0; i < vm.listCountryAdmin.length; i++) {
                    if (data !== undefined) {
                        if (data.CountryID === vm.listCountryAdmin[i].CountryID) {
                            vm.selectedCountryAdmin = vm.listCountryAdmin[i];
                            loadStateAdmin(data);
                            break;
                        }

                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadStateAdmin = loadStateAdmin;
        vm.selectedStateAdmin;
        vm.listStateAdmin = [];
        function loadStateAdmin(data) {
            if (!data) {
                data = vm.selectedCountryAdmin;
                vm.selectedStateAdmin = undefined;
                vm.selectedCityAdmin = undefined;
                vm.selectedDistrictAdmin = undefined;
                vm.selectedStateAdmin1 = undefined;
            }
            loadRegionAdmin(data.CountryID);

            DataAdministrasiService.SelectState(data.CountryID, function (response) {
                vm.listStateAdmin = response.data;
                for (var i = 0; i < vm.listStateAdmin.length; i++) {
                    if (data !== undefined) {
                        if (data.StateID === vm.listStateAdmin[i].StateID) {
                            vm.selectedStateAdmin = vm.listStateAdmin[i];
                            if (vm.selectedStateAdmin.Country.Code === 'IDN') {
                                loadCityAdmin(vm.selectedStateAdmin);
                                break;
                            }
                        }
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadRegionAdmin = loadRegionAdmin;
        vm.selectedRegionAdmin;
        vm.listRegionAdmin = [];
        function loadRegionAdmin(countryID) {
            DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
                vm.listRegionAdmin = response.data;
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadCityAdmin = loadCityAdmin;
        vm.selectedCityAdmin;
        vm.listCityAdmin = [];
        function loadCityAdmin(data) {
            if (!data) {

                data = vm.selectedStateAdmin;
                vm.selectedCityAdmin = undefined;
                vm.selectedCityAdmin1 = undefined;
                vm.selectedDistrictAdmin = undefined;
            }
            DataAdministrasiService.SelectCity(data.StateID, function (response) {
                vm.listCityAdmin = response.data;
                for (var i = 0; i < vm.listCityAdmin.length; i++) {
                    if (data !== undefined) {
                        if (vm.CityCompany.CityID === vm.listCityAdmin[i].CityID) {
                            vm.selectedCityAdmin = vm.listCityAdmin[i];
                            if (vm.selectedStateAdmin.Country.Code === 'IDN') {
                                loadDistrictAdmin(vm.selectedCityAdmin);
                                break;
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadDistrictAdmin = loadDistrictAdmin;
        vm.selectedDistrictAdmin;
        vm.listDistrictAdmin = [];
        function loadDistrictAdmin(city) {
            if (!city) {
                city = vm.selectedCityAdmin;
                vm.selectedDistrictAdmin = undefined;
                vm.selectedDistrictAdmin1 = undefined;

            }
            DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
                vm.listDistrictAdmin = response.data;
                for (var i = 0; i < vm.listDistrictAdmin.length; i++) {
                    if (city !== undefined) {
                        if (vm.DistrictCompany.DistrictID === vm.listDistrictAdmin[i].DistrictID) {
                            vm.selectedDistrictAdmin = vm.listDistrictAdmin[i];
                            break;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.openCalendar = openCalendar;
        function openCalendar(index) {
            vm.isCalendarOpened[index] = true;
        };

        function convertAllDateToString() { // TIMEZONE (-)
            if (vm.administrasiDate.StartDate) {
                vm.administrasiDate.StartDate = UIControlService.getStrDate(vm.administrasiDate.StartDate);
            }
        };

        function convertToDate() {
            if (vm.administrasi.StartDate) {
                vm.administrasiDate.StartDate = new Date(Date.parse(vm.administrasiDate.StartDate));
            }
        }

        vm.loadPhoneCodes = loadPhoneCodes;
        function loadPhoneCodes(data) {
            UIControlService.loadLoading("Loading");
            VendorRegistrationService.getCountries(
              function (response) {
                  vm.phoneCodeList = response.data;
                  for (var i = 0; i < vm.phoneCodeList.length; i++) {
                      if (vm.phoneCodeList[i].PhonePrefix === data) {
                          vm.phoneCode = vm.phoneCodeList[i];
                      }
                  }
                  UIControlService.unloadLoading();
              }, function (err) {
                  //$.growl.error({ message: "Gagal Akses API >" + err });
                  UIControlService.unloadLoading();
              });
        }

        vm.loadSizeTypeFilePKP = loadSizeTypeFilePKP;
        function loadSizeTypeFilePKP() {
            UploadFileConfigService.getByPageName("PAGE.VENDOR.ADMINISTRATION.PKP", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.name = response.data.name;
                    vm.idUploadConfigs = response.data;
                    vm.idFileTypes = generateFilterStrings(response.data);
                    vm.idFileSize = vm.idUploadConfigs[0];

                }
            }, function (err) {
                UIControlService.unloadLoading();
                return;
            });
        }

        vm.loadSizeTypeFileNPWP = loadSizeTypeFileNPWP;
        function loadSizeTypeFileNPWP() {
            UploadFileConfigService.getByPageName("PAGE.VENDOR.REGISTRATION.NPWP", function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    vm.name1 = response.data.name;
                    vm.idUploadConfigs1 = response.data;
                    vm.idFileTypes1 = generateFilterStrings(response.data);
                    vm.idFileSize1 = vm.idUploadConfigs1[0];

                } else {
                    UIControlService.msg_growl("error", ".MESSAGE.ERR_TYPEFILE");
                    return;
                }
            }, function (err) {
                UIControlService.msg_growl("error", "MESSAGE.API");
                UIControlService.unloadLoading();
                return;
            });
        }

        function generateFilterStrings(allowedTypes) {
            var filetypes = "";
            for (var i = 0; i < allowedTypes.length; i++) {
                filetypes += "." + allowedTypes[i].Name + ",";
            }
            return filetypes.substring(0, filetypes.length - 1);
        }

        vm.selectedTypeVendor;
        vm.listTypeVendor;
        function loadTypeVendor(data) {
            DataAdministrasiService.getTypeVendor(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listTypeVendor = reply.data.List;
                    if (data !== undefined) {
                        for (var i = 0; i < vm.listTypeVendor.length; i++) {
                            if (data.VendorTypeID === vm.listTypeVendor[i].RefID) {
                                vm.selectedTypeVendor = vm.listTypeVendor[i];
                                changeTypeVendor(vm.administrasi);
                                break;
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.changeTypeVendor = changeTypeVendor;
        function changeTypeVendor(data) {
            if (vm.selectedTypeVendor !== undefined) {
                if (vm.selectedTypeVendor.Value === "VENDOR_TYPE_SERVICE") {
                    vm.disablePemasok = true;
                    vm.listSupplier = {};
                }
                if (vm.selectedTypeVendor.Value !== "VENDOR_TYPE_SERVICE") {
                    vm.disablePemasok = false;
                    if (data) {
                        loadSupplier(data);
                    }
                    else {
                        loadSupplier();
                    }
                }

                vm.GoodsOrService = vm.selectedTypeVendor.RefID;
                loadBusinessField();
                vm.listComodity = [];
            }

        }

        vm.loadBusinessField = loadBusinessField;
        vm.selectedBusinessField;
        vm.listBusinessField = [];
        function loadBusinessField() {
            DataAdministrasiService.SelectBusinessField({
                GoodsOrService: vm.GoodsOrService
            },
			   function (response) {
			       if (response.status === 200) {
			           vm.listBusinessField = response.data;
			       }
			       else {
			           return;
			       }
			   }, function (err) {
			       return;
			   });
        }

        vm.changeBussinesField = changeBussinesField;
        function changeBussinesField() {
            if (vm.selectedBusinessField === undefined) {
                UIControlService.msg_growl("warning", "Jenis Bidang Usaha Belum di Pilih!");
                return;
            } else {
                if (vm.selectedBusinessField.Name != "Lain - lain")
                    vm.loadComodity();
            }
        }

        vm.loadComodity = loadComodity;
        vm.selectedComodity;
        vm.listComodity = [];
        function loadComodity() {
            if (vm.selectedBusinessField.GoodsOrService === 3091) {
                UIControlService.msg_growl("success", "Tidak ada komoditas dalam bidang ini");
                vm.listComodity = [];
            } else {
                DataAdministrasiService.SelectComodity({ ID: vm.selectedBusinessField.ID },
				   function (response) {
				       if (response.status === 200 && response.data.length > 0) {
				           vm.listComodity = response.data;
				       } else if (response.status === 200 && response.data.length < 1) {
				           UIControlService.msg_growl("success", "Tidak ada komoditas dalam bidang ini");
				           vm.listComodity = [];
				       } else {
				           return;
				       }
				   }, function (err) {
				       return;
				   });
            }
        }

        vm.selectedSupplier;
        vm.listSupplier;
        function loadSupplier(data) {
            DataAdministrasiService.getSupplier(function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    vm.listSupplier = reply.data.List;
                    if (data) {
                        for (var i = 0; i < vm.listSupplier.length; i++) {
                            if (data.SupplierID === vm.listSupplier[i].RefID) {
                                vm.selectedSupplier = vm.listSupplier[i];
                                break;
                            }
                        }
                    }
                }
            }, function (err) {
                UIControlService.unloadLoading();
            });
        }

        vm.loadCurrency = loadCurrency;
        function loadCurrency() {
            UIControlService.loadLoading("LOADERS.LOADING_CURRENCY");
            VendorRegistrationService.getCurrencies(
                function (response) {
                    vm.currencyList = response.data;
                    UIControlService.unloadLoading();
                },
                handleRequestError);
        }

        function handleRequestError(response) {
            UIControlService.log(response);
            //UIControlService.handleRequestError(response.data, response.status);
            UIControlService.unloadLoading();
        }

        vm.addCurrency = addCurrency;
        function addCurrency() {
            vm.flagCurr = false;
            for (var i = 0; i < vm.listCurrencies.length; i++) {
                if (vm.Currency.CurrencyID == vm.listCurrencies[i].MstCurrency.CurrencyID && vm.listCurrencies[i].IsActive == true) { vm.flagCurr = true; }
            }
            if (vm.flagCurr == false) {
                vm.listCurrencies.push({
                    ID: 0,
                    CurrencyID: vm.Currency.CurrencyID,
                    VendorID: vm.administrasi.VendorID,
                    MstCurrency: vm.Currency,
                    IsActive: true
                });
            }
            else {
                UIControlService.msg_growl("warning", "Mata Uang yang ditambahkan sudah ada");
                return;

            }
        }

        vm.addDetailBussinesField = addDetailBussinesField;
        function addDetailBussinesField() {
            if (vm.selectedBusinessField === undefined) {
                UIControlService.msg_growl("warning", "Bidang Usaha Belum di Pilih");
                return;
            }

            var comID;
            if (vm.selectedComodity === undefined) {
                //UIControlService.msg_growl("warning", "Komoditas Belum di Pilih");
                //return;
                comID = null;
            } else if (!(vm.selectedComodity === undefined)) {
                comID = vm.selectedComodity.ID;
            }
            countDetailBusinessField = [];
            for (var i = 0; i < vm.listBusinessField.length; i++) {
                if (vm.listBusinessField[i].Name != "Lain - lain")
                    countDetailBusinessField.push(vm.listBusinessField[i]);
            }
            var countDetailBusinessField = vm.listBussinesDetailField.length;
            var addPermission = false; var sameItem = true;
            var dataDetail = {
                VendorID: vm.administrasi.VendorID,
                CommodityID: comID,
                BusinessFieldID: vm.selectedBusinessField.ID,
                Commodity: vm.selectedComodity,
                BusinessField: vm.selectedBusinessField,
                Remark: vm.RemarkBusinessField
            }
            if (vm.selectedBusinessField.Name == "Lain - lain") {
                vm.listBussinesDetailField.push(dataDetail);
            }
            else {

                if (countDetailBusinessField <= 6) {
                    var countGoodsDetail = 0; var countServiceDetail = 0;
                    for (var a = 0; a < vm.listBussinesDetailField.length; a++) {

                        if (vm.listBusinessField[a].Name != "Lain - lain") {
                            if (countDetailBusinessField > 0) {
                                if (dataDetail.BusinessField.GoodsOrService === 3091) {
                                    if (vm.listBussinesDetailField[a].BusinessField.GoodsOrService === 3091) {
                                        if (vm.listBussinesDetailField[a].BusinessFieldID !== dataDetail.BusinessFieldID) {
                                            countServiceDetail = +countServiceDetail + 1;
                                        }
                                        else {
                                            UIControlService.msg_growl("warning", "Bidang usaha jasa telah dipilih");
                                            sameItem = false;
                                        }
                                    }
                                }
                                else if (dataDetail.BusinessField.GoodsOrService === 3090) {
                                    if (vm.listBussinesDetailField[a].BusinessField.GoodsOrService === 3090) {
                                        if (vm.listBussinesDetailField[a].CommodityID !== dataDetail.CommodityID) {
                                            countGoodsDetail = +countGoodsDetail + 1;
                                        }
                                        else {
                                            UIControlService.msg_growl("warning", "Komoditas telah dipilih");
                                            sameItem = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //barang & jasa
                    if (vm.selectedBusinessField.Name != "Lain - lain") {
                        if (vm.GoodsOrService === 3092) {
                            if (dataDetail.BusinessField.GoodsOrService === 3090) {
                                if (countGoodsDetail < 3) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "Untuk tipe vendor barang dan jasa, maksimal bidang usaha barang adalah 3.");
                                }
                            }
                            else if (dataDetail.BusinessField.GoodsOrService === 3091) {
                                if (countServiceDetail < 5) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "Untuk tipe vendor barang dan jasa, maksimal bidang usaha jasa adalah 3");
                                }
                            }

                            if (countDetailBusinessField === 6) {
                                UIControlService.msg_growl("warning", "Untuk tipe vendor barang dan jasa, maksimal total bidang usaha adalah 6");
                                addPermission = true;
                            }
                        }
                            //barang
                        else if (vm.GoodsOrService === 3090) {
                            if (dataDetail.BusinessField.GoodsOrService === 3090) {
                                if (countGoodsDetail < 3) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "Untuk tipe vendor barang, maksimal bidang usaha barang adalah 3");
                                }
                            }
                        }

                            //jasa
                        else if (vm.GoodsOrService === 3091) {
                            if (dataDetail.BusinessField.GoodsOrService === 3091) {
                                if (countServiceDetail < 5) {
                                    addPermission = true;
                                }
                                else {
                                    UIControlService.msg_growl("warning", "Untuk tipe vendor jasa, maksimal bidang usaha jasa adalah 5");
                                }
                            }
                        }
                    }
                    else {
                        vm.listBussinesDetailField.push(dataDetail);
                    }


                }
                if (addPermission === true && sameItem === true) {
                    vm.listBussinesDetailField.push(dataDetail);
                }
            }

            vm.selectedComodity = undefined;

        }

        vm.deleteRow = deleteRow;
        function deleteRow(index) {
            var idx = index - 1;
            var _length = vm.listBussinesDetailField.length; // panjangSemula
            vm.listBussinesDetailField.splice(idx, 1);
        };

        vm.deleteRowCurr = deleteRowCurr;
        function deleteRowCurr(index, data) {
            if (data.ID != undefined) {
                data.IsActive = false;
                vm.listCurrFalse.push(data);
            }
            var idx = index;
            var _length = vm.listCurrencies.length; // panjangSemula
            vm.listCurrencies.splice(idx, 1);
        };

        vm.deleteRowPers = deleteRowPers;
        function deleteRowPers(index, data) {
            if (data.ContactID != 0) {
                data.IsActive = false;
                vm.listPersFalse.push(data);
            }
            var idx = index;
            var _length = vm.listPersonal.length; // panjangSemula
            vm.listPersonal.splice(idx, 1);
        };

        vm.CheckEmail = CheckEmail;
        function CheckEmail() {
            UIControlService.loadLoading("Check Email . . .");
            if (vm.EmailPers !== '') {
                var data = {
                    Keyword: vm.EmailPers
                };
                DataAdministrasiService.checkEmail(data,
                        function (response) {
                            vm.EmailAvailable = response.data;
                            if (vm.EmailAvailable) {
                                UIControlService.unloadLoading();
                                UIControlService.msg_growl('error', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.MESSAGE', 'FORM.VALIDATION_ERRORS.EMAIL_AVAILABLE.TITLE');

                                vm.NamePers = undefined;
                                vm.PhonePers = undefined;
                                vm.EmailPers = undefined;
                            }
                            else {
                                vm.addContactPers();
                            }
                        }, handleRequestError);
            }
        }

        vm.addContactPers = addContactPers;
        function addContactPers() {
            vm.listPersonal.push({

                Contact: {
                    ContactID: 0,
                    Name: vm.NamePers,
                    Phone: vm.PhonePers,
                    Email: vm.EmailPers
                },
                IsActive: true
            });
            vm.NamePers = undefined;
            vm.PhonePers = undefined;
            vm.EmailPers = undefined;

            UIControlService.unloadLoading();
        }

        vm.editcontact = editcontact;
        function editcontact(data) {
            var data = {
                item: data
            }
            var modalInstance = $uibModal.open({
                templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/data-administrasi/DetailContact.html',
                controller: 'DetailContactAdministrasiPrequalCtrl',
                controllerAs: 'DetailContactAdministrasiPrequalCtrl',
                resolve: {
                    item: function () {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                vm.listPers = [];
                vm.listPers = vm.listPersonal;
                vm.listPersonal = [];
                for (var i = 0; i < vm.listPers.length; i++) {
                    if (vm.listPers[i].ContactID === data.ContactID) {
                        var aish = {
                            VendorContactType: vm.listPers[i].VendorContactType,
                            ContactID: vm.listPers[i].ContactID,
                            VendorID: vm.listPers[i].VendorID,
                            Contact: {
                                ContactID: data.ContactID,
                                Name: data.Name,
                                Email: data.Email,
                                Phone: data.Phone
                            },
                            IsActive: true,
                            IsEdit: true
                        }
                        vm.listPersonal.push(aish);
                    }
                    else {
                        vm.listPersonal.push(vm.listPers[i]);
                    }
                }
            });
        }

        vm.CheckAddress = CheckAddress;
        function CheckAddress(flag) {
            if (flag == true) {
                if (vm.address1 !== vm.cekAddress) vm.IsEdit = true;
            }
            else {
                if (vm.addressinfo !== vm.cekAddress1) vm.IsEditAlter = true;
            }
        }

        vm.loadRegion = loadRegion;
        vm.selectedRegion;
        vm.listRegion = [];
        function loadRegion(countryID) {
            DataAdministrasiService.SelectRegion({
                CountryID: countryID
            }, function (response) {
                vm.listRegion = response.data;
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadCountry = loadCountry;
        vm.selectedCountry;
        vm.listCountry = [];
        function loadCountry(data) {
            DataAdministrasiService.SelectCountry(function (response) {
                vm.listCountry = response.data;
                for (var i = 0; i < vm.listCountry.length; i++) {
                    if (data.CountryID === vm.listCountry[i].CountryID) {
                        vm.selectedCountry = vm.listCountry[i];
                        loadState(data);
                        break;
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadState = loadState;
        vm.selectedState;
        vm.listState = [];
        function loadState(data) {
            if (!data) {
                data = vm.selectedCountry;
                vm.selectedState = "";
                vm.selectedCity = "";
                vm.selectedDistrict = "";
                vm.selectedState1 = "";
            }
            loadRegion(data.CountryID);

            DataAdministrasiService.SelectState(data.CountryID, function (response) {
                vm.listState = response.data;
                for (var i = 0; i < vm.listState.length; i++) {
                    if (vm.selectedState1 !== "" && vm.selectedState1.StateID === vm.listState[i].StateID) {
                        vm.selectedState = vm.listState[i];
                        if (vm.selectedState.Country.Code === 'IDN') {
                            loadCity(vm.selectedState);
                            break;
                        }
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadCity = loadCity;
        vm.selectedCity;
        vm.listCity = [];
        function loadCity(data) {
            if (!data) {

                data = vm.selectedState;
                vm.selectedCity = "";
                vm.selectedCity1 = "";
                vm.selectedDistrict = "";
            }
            DataAdministrasiService.SelectCity(data.StateID, function (response) {
                vm.listCity = response.data;
                for (var i = 0; i < vm.listCity.length; i++) {
                    if (vm.selectedCity1 !== "" && vm.selectedCity1.CityID === vm.listCity[i].CityID) {
                        vm.selectedCity = vm.listCity[i];
                        if (vm.selectedState.Country.Code === 'IDN') {
                            loadDistrict(vm.selectedCity);
                            break;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadDistrict = loadDistrict;
        vm.selectedDistrict;
        vm.listDistrict = [];
        function loadDistrict(city) {
            if (!city) {
                city = vm.selectedCity;
                vm.selectedDistrict = "";
                vm.selectedDistrict1 = "";

            }
            DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
                vm.listDistrict = response.data;
                for (var i = 0; i < vm.listDistrict.length; i++) {
                    if (vm.selectedDistrict1 !== "" && vm.selectedDistrict1.DistrictID === vm.listDistrict[i].DistrictID) {
                        vm.selectedDistrict = vm.listDistrict[i];
                        break;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadAssociation = loadAssociation;
        vm.selectedAssociation;
        vm.listAssociation = [];
        function loadAssociation(data) {
            DataAdministrasiService.getAssociation({
                Offset: 0,
                Limit: 0,
                Keyword: ""
            },
			function (response) {
			    if (response.status === 200) {
			        vm.listAssociation = response.data.List;
			        for (var i = 0; i < vm.listAssociation.length; i++) {
			            if (data.AssociationID === vm.listAssociation[i].AssosiationID) {
			                vm.selectedAssociation = vm.listAssociation[i];
			                break;
			            }
			        }
			    } else {
			        return;
			    }
			}, function (err) {
			    return;
			});
        }

        vm.loadRegionAlternatif = loadRegionAlternatif;
        vm.selectedRegionAlternatif;
        vm.listRegionAlternatif = [];
        function loadRegionAlternatif(countryID) {
            DataAdministrasiService.SelectRegion({ CountryID: countryID }, function (response) {
                vm.listRegionAlternatif = response.data;
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadCountryAlternatif = loadCountryAlternatif;
        vm.selectedCountryAlternatif;
        vm.listCountryAlternatif = [];
        function loadCountryAlternatif(data) {
            DataAdministrasiService.SelectCountry(function (response) {
                vm.listCountryAlternatif = response.data;
                for (var i = 0; i < vm.listCountryAlternatif.length; i++) {
                    if (data !== undefined) {
                        if (data.CountryID === vm.listCountryAlternatif[i].CountryID) {
                            vm.selectedCountryAlternatif = vm.listCountryAlternatif[i];
                            loadStateAlternatif(data);
                            break;
                        }

                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadStateAlternatif = loadStateAlternatif;
        vm.selectedStateAlternatif;
        vm.listStateAlternatif = [];
        function loadStateAlternatif(data) {
            if (!data) {
                data = vm.selectedCountryAlternatif;
                vm.selectedStateAlternatif = "";
                vm.selectedCityAlternatif = "";
                vm.selectedDistrictAlternatif = "";
                vm.selectedStateAlternatif1 = "";
            }
            loadRegionAlternatif(data.CountryID);

            DataAdministrasiService.SelectState(data.CountryID, function (response) {
                vm.listStateAlternatif = response.data;
                for (var i = 0; i < vm.listStateAlternatif.length; i++) {
                    if (vm.selectedStateAlternatif1 !== "" && vm.selectedStateAlternatif1.StateID === vm.listStateAlternatif[i].StateID) {
                        vm.selectedStateAlternatif = vm.listStateAlternatif[i];
                        if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
                            loadCityAlternatif(vm.selectedStateAlternatif);
                            break;
                        }
                    }
                }


            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadCityAlternatif = loadCityAlternatif;
        vm.selectedCityAlternatif;
        vm.listCityAlternatif = [];
        function loadCityAlternatif(data) {
            if (!data) {

                data = vm.selectedStateAlternatif;
                vm.selectedCityAlternatif = "";
                vm.selectedCityAlternatif1 = "";
                vm.selectedDistrictAlternatif = "";
            }
            DataAdministrasiService.SelectCity(data.StateID, function (response) {
                vm.listCityAlternatif = response.data;
                for (var i = 0; i < vm.listCityAlternatif.length; i++) {
                    if (vm.selectedCityAlternatif1 !== "" && vm.selectedCityAlternatif1.CityID === vm.listCityAlternatif[i].CityID) {
                        vm.selectedCityAlternatif = vm.listCityAlternatif[i];
                        if (vm.selectedStateAlternatif.Country.Code === 'IDN') {
                            loadDistrictAlternatif(vm.selectedCityAlternatif);
                            break;
                        }
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadDistrictAlternatif = loadDistrictAlternatif;
        vm.selectedDistrictAlternatif;
        vm.listDistrictAlternatif = [];
        function loadDistrictAlternatif(city) {
            if (!city) {
                city = vm.selectedCityAlternatif;
                vm.selectedDistrictAlternatif = "";
                vm.selectedDistrictAlternatif1 = "";

            }
            DataAdministrasiService.SelectDistrict(city.CityID, function (response) {
                vm.listDistrictAlternatif = response.data;
                for (var i = 0; i < vm.listDistrictAlternatif.length; i++) {
                    if (vm.selectedDistrictAlternatif1 !== "" && vm.selectedDistrictAlternatif1.DistrictID === vm.listDistrictAlternatif[i].DistrictID) {
                        vm.selectedDistrictAlternatif = vm.listDistrictAlternatif[i];
                        break;
                    }
                }
            }, function (err) {
                UIControlService.msg_growl("error", "Gagal Akses API");
                return;
            });
        }

        vm.loadBusinessEntity = loadBusinessEntity;
        vm.selectedBusinessEntity;
        vm.listBusinessEntity = [];
        function loadBusinessEntity() {
            DataAdministrasiService.SelectBusinessEntity(function (response) {
                if (response.status === 200) {
                    vm.listBusinessEntity = response.data;
                    for (var i = 0; i < vm.listBusinessEntity.length; i++) {
                        if (vm.administrasi.BusinessID === vm.listBusinessEntity[i].BusinessID) {
                            vm.selectedBusinessEntity = vm.listBusinessEntity[i];
                            break;
                        }
                    }
                } else {
                    return;
                }
            }, function (err) {
                return;
            });
        }

        vm.uploadFile = uploadFile;
        function uploadFile() {
            if (vm.fileUpload === undefined) {
                vm.PKPUrl = vm.administrasi.PKPUrl;
                if (vm.fileUploadNPWP === undefined) {
                    vm.NpwpUrl = vm.administrasi.NpwpUrl;
                    savedata();
                }
                else {
                    if (vm.fileUploadNPWP !== null) {
                        upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
                    }
                    else {
                        vm.NpwpUrl = vm.administrasi.NpwpUrl;
                        savedata();
                    }
                }
            }
            else {
                if (vm.fileUpload !== null) {
                    upload(vm.fileUpload, vm.idFileSize, vm.idFileTypes, "");
                }
                else {
                    vm.PKPUrl = vm.administrasi.PKPUrl;
                    if (vm.fileUploadNPWP === undefined) {
                        vm.NpwpUrl = vm.administrasi.NpwpUrl;
                        savedata();
                    }
                    else {
                        if (vm.fileUploadNPWP !== null) {
                            upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
                        }
                        else {
                            vm.NpwpUrl = vm.administrasi.NpwpUrl;
                            savedata();
                        }
                    }
                }
            }

        }

        vm.validateFileType = validateFileType;
        function validateFileType(administrasi, flag, file, allowedFileTypes) {
            if (flag == true) {
                if (!file && administrasi.PKPUrl === "") {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                    return false;
                }
            }
            else {
                if (!file && administrasi.NpwpUrl === "") {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_NOFILE");
                    return false;
                }
            }
            return true;
        }

        vm.upload = upload;
        function upload(file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            //if (vm.administrasi.PKPUrl === null) {
            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadSingleFileSPPKP(vm.administrasi.VendorID, file, size, filters, function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    vm.pathFile = url;
                    vm.name = response.data.FileName;
                    var s = response.data.FileLength;
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s)
                    }
                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }
                    vm.PKPUrl = vm.pathFile;
                    if (vm.fileUploadNPWP === undefined) {
                        vm.NpwpUrl = vm.administrasi.NpwpUrl;
                        savedata();
                    }
                    else {
                        if (vm.fileUploadNPWP !== null) {
                            upload1(vm.fileUploadNPWP, vm.idFileSize1, vm.idFileTypes1, "");
                        }
                        else {
                            vm.NpwpUrl = vm.administrasi.NpwpUrl;
                            savedata();
                        }
                    }
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (response) {
                if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
                    UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
                    UIControlService.unloadLoading();
                }
            });
            //} end if
        }

        vm.upload1 = upload1;
        function upload1(file, config, filters, callback) {
            var size = config.Size;
            var unit = config.SizeUnitName;

            if (unit == 'SIZE_UNIT_KB') {
                size *= 1024;
                vm.flag = 0;
            }

            if (unit == 'SIZE_UNIT_MB') {
                size *= (1024 * 1024);
                vm.flag = 1;
            }
            UIControlService.loadLoading("LOADERS.LOADING_UPLOAD_FILE");
            UploaderService.uploadRegistration(file, vm.administrasi.Npwp, vm.prefixes.UPLOAD_PREFIX_NPWP.Value, size, filters, function (response) {
                UIControlService.unloadLoading();
                if (response.status == 200) {
                    var url = response.data.Url;
                    vm.pathFile1 = url;
                    vm.name1 = response.data.FileName;
                    var s = response.data.FileLength;
                    if (vm.flag == 0) {
                        vm.size = Math.floor(s)
                    }
                    if (vm.flag == 1) {
                        vm.size = Math.floor(s / (1024));
                    }
                    vm.NpwpUrl = vm.pathFile1;
                    savedata();
                } else {
                    UIControlService.msg_growl("error", "MESSAGE.ERR_UPLOAD");
                    return;
                }
            }, function (response) {
                if (response.data.InnerException.ExceptionMessage == "Maximum request length exceeded.") {
                    UIControlService.msg_growl("error", "MESSAGE.EER_SPPKPFILEMAKS")
                    UIControlService.unloadLoading();
                }
            });
        }

        function savedata() {

            vm.cek = 0;
            if (vm.selectedCountryAdmin == undefined) { 
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY");
                return;
            }
            else if (vm.selectedStateAdmin == undefined) { 
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_STATE");
                return;
            }
            else if (vm.selectedStateAdmin.Country.Code === 'IDN') { 
                    if (vm.selectedCityAdmin == undefined) {
                        vm.cek = 1;
                        UIControlService.msg_growl("error", "MESSAGE.ERR_CITY");
                        return;
                    }
                    else if (vm.selectedDistrictAdmin == undefined) {
                        vm.cek = 1;
                        UIControlService.msg_growl("error", "MESSAGE.ERR_DISTRICT");
                        return;
                    }
                }
            if (vm.administrasi.FoundedDate == undefined) { 
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_STARTDATE");
                return;
            }
            else if (vm.phoneCode == undefined) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_PHONECODE");
                return;
            }
            else if (vm.phoneCode !== undefined) {
                if (vm.Phone == undefined || vm.Phone == "") {
                    vm.cek = 1;
                    UIControlService.msg_growl("error", "MESSAGE.ERR_PHONE");
                    return;
                }
                else if (vm.listCompanyContact.Email == undefined || vm.listCompanyContact.Email == "") {
                    vm.cek = 1;
                    UIControlService.msg_growl("error", "MESSAGE.ERR_EMAIL");
                    return;
                }
            }
            if (vm.administrasi.NpwpUrl == undefined && vm.fileUploadNPWP == undefined) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_NPWPURL");
                return;
            }
            else if (vm.selectedStateAdmin.Country.Code === 'IDN') {
                if (vm.PKPNumber == undefined || vm.PKPNumber == "") {
                    vm.cek = 1;
                    UIControlService.msg_growl("error", "MESSAGE.ERR_PKPNUMBER");
                    return;
                }
                else if (vm.administrasi.PKPUrl == undefined && vm.fileUpload == undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl("error", "MESSAGE.ERR_PKPUPLOAD");
                    return;
                }
            }
            if (vm.selectedTypeVendor === undefined) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_VENDORTYPE");
                return;
            }
            else if (vm.selectedTypeVendor !== undefined) {
                vm.VendorTypeID = vm.selectedTypeVendor.RefID;
                if (vm.selectedTypeVendor.Name === "VENDOR_TYPE_SERVICE") {
                    vm.SupplierID = null;
                }
                else {
                    if (vm.selectedSupplier === undefined) {
                        vm.cek = 1;
                        UIControlService.msg_growl("error", "MESSAGE.ERR_SUPPLIER");
                        return;
                    }
                    else vm.SupplierID = vm.selectedSupplier.RefID;
                }
            }
            if (vm.listCurrencies.length === 0) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_CURR");
                return;
            }
            else if (vm.listPersonal.length === 0) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_CP");
                return;
            }
            if (vm.selectedCountry == undefined) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_COUNTRY_ADDR");
                return;
            }
            else if (vm.selectedState == undefined) {
                vm.cek = 1;
                UIControlService.msg_growl("error", "MESSAGE.ERR_STATE_ADDR");
                return;
            }
            else if (vm.selectedState.Country.Code === 'IDN') {
                if (vm.selectedCity == undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl("error", "MESSAGE.ERR_CITY_ADDR");
                    return;
                }
                else if (vm.selectedDistrict == undefined) {
                    vm.cek = 1;
                    UIControlService.msg_growl("error", "MESSAGE.ERR_DISTRICT_ADDR");
                    return;
                }
            }
            if (vm.cek === 0) {
                addtolist(vm.VendorTypeID, vm.SupplierID);
            }
        }

        vm.addtolist = addtolist;
        vm.vendor = {};
        vm.listcontact = [];
        function addtolist(data1, data2) {
            /* Untuk Contact Company */
            if (!vm.selectedCityAdmin && !vm.selectedDistrictAdmin) {
                var addressComp = {
                    AddressID: vm.listCompanyContact.AddressID,
                    StateID: vm.selectedStateAdmin.StateID
                }
            }
            else {
                var addressComp = {
                    AddressID: vm.listCompanyContact.AddressID,
                    StateID: vm.selectedStateAdmin.StateID,
                    CityID: vm.selectedCityAdmin.CityID,
                    DistrictID: vm.selectedDistrictAdmin.DistrictID
                }
            }
            var contactdt = {
                IsActive: true,
                VendorContactType: vm.VendorContactTypeCompany,
                Contact: {
                    ContactID: vm.listCompanyContact.ContactID,
                    Email: vm.listCompanyContact.Email,
                    Phone: '(' + vm.phoneCode.PhonePrefix + ') ' + vm.Phone,
                    Website: vm.listCompanyContact.Website,
                    Fax: vm.listCompanyContact.Fax,
                    Address: addressComp,
                    Name: vm.listCompanyContact.Name
                }
            }
            vm.listcontact.push(contactdt);

            /* Untuk Contact Utama */
            if (vm.selectedCity == undefined && vm.selectedDistrict == undefined) {
                vm.address = {
                    AddressID: vm.listOfficeAddress.AddressID,
                    AddressInfo: vm.address1,
                    PostalCode: vm.postcalcode,
                    StateID: vm.selectedState.StateID
                }
            } else {

                vm.address = {
                    AddressID: vm.listOfficeAddress.AddressID,
                    AddressInfo: vm.address1,
                    PostalCode: vm.postcalcode,
                    StateID: vm.selectedState.StateID,
                    CityID: vm.selectedCity.CityID,
                    DistrictID: vm.selectedDistrict.DistrictID
                }
            }
            var contact = {
                ContactID: vm.listOfficeAddress.ContactID,
                Address: vm.address,
                Name: vm.administrasi.VendorName
            }
            var contactdt = {
                VendorContactType: vm.VendorContactType,
                Contact: contact,
                IsActive: true,
                IsEdit: vm.IsEdit
            }
            vm.listcontact.push(contactdt);

            /* Untuk Contact Alternatif */
            if (vm.selectedCountryAlternatif !== undefined) {
                if (!vm.selectedCityAlternatif && !vm.selectedDistrictAlternatif) {


                    vm.address2 = {
                        AddressID: vm.AddressAlterId,
                        AddressInfo: vm.addressinfo,
                        PostalCode: vm.PostalCodeAlternatif,
                        StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null

                    }
                }
                else {

                    vm.address2 = {
                        AddressID: vm.AddressAlterId,
                        AddressInfo: vm.addressinfo,
                        PostalCode: vm.PostalCodeAlternatif,
                        StateID: vm.selectedStateAlternatif ? vm.selectedStateAlternatif.StateID : null,
                        CityID: vm.selectedCityAlternatif.CityID,
                        DistrictID: vm.selectedDistrictAlternatif.DistrictID
                    }
                }
                if (vm.AddressAlterId == 0) {
                    var contact = {
                        Name: vm.administrasi.VendorName,
                        ModifiedBy: vm.administrasi.VendorName,
                        Address: vm.address2
                    }
                }
                else {
                    var contact = {
                        ContactID: vm.ContactOfficeAlterId,
                        Name: vm.administrasi.VendorName,
                        ModifiedBy: vm.administrasi.user.Username,
                        Address: vm.address2
                    }
                }
                var contactdt = {
                    VendorContactType: vm.VendorContactType,
                    Contact: contact,
                    IsPrimary: 2,
                    IsActive: true,
                    IsEdit: vm.IsEditAlter
                }

                if (contactdt != null) vm.listcontact.push(contactdt);

                /* Untuk Contact Personal*/

            }
            if (vm.contactpersonal != null) vm.listcontact.push(vm.contactpersonal);
            for (var i = 0; i < vm.listPersonal.length; i++) {
                var contactdt = {
                    VendorContactType: vm.VendorContactTypePers,
                    Contact: {
                        ContactID: vm.listPersonal[i].Contact.ContactID,
                        Email: vm.listPersonal[i].Contact.Email,
                        Phone: vm.listPersonal[i].Contact.Phone,
                        Name: vm.listPersonal[i].Contact.Name
                    },
                    IsActive: true,
                    IsEdit: vm.listPersonal[i].IsEdit
                }
                vm.listcontact.push(contactdt);
            }
            for (var i = 0; i < vm.listPersFalse.length; i++) {
                var contactdt = {
                    VendorContactType: vm.VendorContactTypePers,
                    Contact: {
                        ContactID: vm.listPersFalse[i].Contact.ContactID,
                        Email: vm.listPersFalse[i].Contact.Email,
                        Phone: vm.listPersFalse[i].Contact.Phone,
                        Name: vm.listPersFalse[i].Contact.Name
                    },
                    IsActive: false
                }
                vm.listcontact.push(contactdt);
            }
            for (var i = 0; i < vm.listCurrFalse.length; i++) {
                vm.listCurrencies.push(vm.listCurrFalse[i]);
            }

            var asoc;
            if (vm.selectedAssociation === undefined) {
                asoc = null;
            } else {
                asoc = vm.selectedAssociation.AssosiationID
            }

            if (vm.selectedSupplier === null) {
                vm.selectedSupplier = {
                    RefID: null
                };
            }
            if (vm.CountryCode !== 'IDN') {
                vm.selectedBusinessEntity = {
                    BusinessID: null
                };
            }
            vm.insertdata = {
                VendorContact: vm.listcontact,
                VendorCurrency: vm.listCurrencies,

                VendorPrequal: {
                    SupplierID: data2,
                    VendorID: vm.administrasi.VendorID,
                    FoundedDate: UIControlService.getStrDate(vm.administrasi.FoundedDate),
                    PKPNumber: vm.PKPNumber,
                    PKPUrl: vm.PKPUrl,
                    AssociationID: asoc,
                    VendorTypeID: data1,
                    NpwpUrl: vm.NpwpUrl
                },
                PrequalStepID: vm.PrequalStepID
            };
            AdministrasiPrequalService.insert(vm.insertdata, function (reply) {
                UIControlService.unloadLoadingModal();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", "MESSAGE.SUCCESS_ADD");
                    init();
                }
            }, function (err) {
                UIControlService.unloadLoadingModal();
            });



        }

        vm.Submit = Submit;
        function Submit() {
            if (vm.flagSubmit === false) {
                UIControlService.msg_growl('error', 'Data Belum lengkap');
                return;
            }
            else if (vm.flagSubmit === true) {
                AdministrasiPrequalService.Submit({ Status: vm.PrequalStepID }, function (reply) {
                    UIControlService.unloadLoadingModal();
                    if (reply.status === 200) {
                        UIControlService.msg_growl("success", "Success Submit");
                        $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
                    }
                }, function (err) {
                    UIControlService.unloadLoadingModal();
                });
            }
        }
    }
})();
//TODO
