﻿(function () {
	'use strict';

	angular.module("app").controller("BankDetailCtrl", ctrl);

	ctrl.$inject = ['$filter', '$state', '$stateParams', '$translatePartialLoader', '$uibModal', 'BankDetailPrequalService', 'BankDetailService', 'UIControlService', 'GlobalConstantService', 'VerifiedSendService'];
	/* @ngInject */
	function ctrl($filter, $state, $stateParams, $translatePartialLoader, $uibModal, BankDetailPrequalService, BankDetailService, UIControlService, GlobalConstantService, VerifiedSendService) {
	    var vm = this;
	    vm.PrequalStepID = Number($stateParams.PrequalStepID);

		vm.totalItems = 0;
		vm.currentPage = 0;
		vm.maxSize = 10;
		vm.page_id = 35;
		vm.menuhome = 0;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.userId = 0;
		vm.jLoad = jLoad;
		vm.bank = [];
		vm.ambilUrl;
		vm.ambilUrl2;

		//vm.Kata = "";
		vm.VendorID;

		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
		    $translatePartialLoader.addPart('bank-detail');
		    loadPrequalStep();
		    jLoad();
		}

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    BankDetailPrequalService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("Silahkan Tunggu...");
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			BankDetailPrequalService.Select({ Status: vm.PrequalStepID }, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.bank = data;
				} else {
					$.growl.error({ message: "Gagal mendapatkan data" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.tambah = tambah;
		function tambah(data) {
		    var data = {
				act: 1,
				item: {
                    PrequalStepID: vm.PrequalStepID
				}
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/bank-detail/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				window.location.reload();
			});
		}

		vm.edit = edit;
		function edit(data) {
		    data.PrequalStepID = vm.PrequalStepID;
			var data = {
				act: 0,
				item: data
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/bank-detail/bank-detail.modal.html',
				controller: "BankDetailModalCtrl",
				controllerAs: "BankDetModalCtrl",
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				jLoad(1);
			});
		}

		vm.remove = remove;
		function remove(doc) {
		    bootbox.confirm('<h3 class="afta-font center-block">' + $filter('translate')('MESSAGE.CONFIRM_DEL') + '<h3>', function (reply) {
				if (reply) {
					//UIControlService.loadLoading(loadmsg);
					BankDetailPrequalService.remove({
						ID: doc.ID
					}, function (reply2) {
						UIControlService.unloadLoading();
						if (reply2.status === 200) {
						    UIControlService.msg_growl('notice', 'MESSAGE.SUCCESS_DELETE');
						    window.location.reload();
						} else {
						    UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
						}
					}, function (error) {
					    UIControlService.unloadLoading();
					    UIControlService.msg_growl('error', 'MESSAGE.FAIL_DELETE');
					});
				}
			});
		};

		vm.Submit = Submit;
		function Submit() {
		    if (vm.bank.length == 0) {
		        UIControlService.msg_growl("error", "Data Belum lengkap");
		        return;

		    }
		    else {
		        BankDetailPrequalService.Submit({ Status: vm.PrequalStepID }, function (reply) {
		            if (reply.status == 200) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", "Success Submit");
		                $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		            } else {
		                UIControlService.unloadLoading();
		                return;
		            }
		        }, function (err) {
		            UIControlService.unloadLoading();
		            return;
		        });

		    }
		}
	}
})();