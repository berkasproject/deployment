(function () {
	'use strict';

	angular.module("app").controller("TenagaAhliPrequalController", ctrl);

	ctrl.$inject = ['$state', '$stateParams', '$timeout', '$http', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'TenagaAhliPrequalService', 'UIControlService', 'GlobalConstantService'];
	/* @ngInject */
	function ctrl($state, $stateParams, $timeout, $http, $uibModal, $translate, $translatePartialLoader, $location, TenagaAhliService, UIControlService, GlobalConstantService) {
	    var vm = this;
	    vm.PrequalStepID = Number($stateParams.PrequalStepID);
		vm.fullSize = 10;
		var offset = 0;
		vm.flag = 0;
		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.vendorexpertsCertificate = [];
		vm.isApprovedCR = false;
		vm.flagCV = true;

		vm.initialize = initialize;
		function initialize() {
		    vm.flagCV = true;
		    $translatePartialLoader.addPart('pemasukkan-prakualifikasi');
		    $translatePartialLoader.addPart('tenaga-ahli');
		    jLoad(1);
		    loadPrequalStep();
		}

		vm.loadPrequalStep = loadPrequalStep;
		function loadPrequalStep() {
		    TenagaAhliService.loadPrequalStep({
		        Status: vm.PrequalStepID
		    }, function (reply) {
		        if (reply.status == 200) {
		            vm.isEntry = reply.data;
		        }
		        UIControlService.unloadLoading();
		    }, function (error) {
		        UIControlService.unloadLoading();
		    });
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			////console.info("curr "+current)
			vm.vendorexperts = [];
			vm.currentPage = current;
			var offset = (current * 10) - 10;
			TenagaAhliService.all({
				Status: vm.PrequalStepID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.vendorexperts = [];
					for (var i = 0; i < data.length; i++) {
					    if (data[i].IsActive === true) {
					        if (data[i].YearOfExperience == null) vm.flagCV = false;
							vm.vendorexperts.push(data[i]);
						}
					}
					//loadCertificate(vm.vendorexperts);
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.loadCertificate = loadCertificate;
		function loadCertificate(data) {
			vm.listJob = [];
			vm.listEducation = [];
			vm.listCertificate = [];
			////console.info("curr "+current)
			TenagaAhliService.selectCertificate({
				Offset: offset,
				Limit: vm.fullSize,
				Status: data.ID
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    vm.vendorexpertsCertificate = reply.data;
				    vm.vendorexpertsCertificate.forEach(function (data) {
				        data.StartDate = new Date(Date.parse(data.StartDate));
				        data.EndDate = new Date(Date.parse(data.EndDate));
				        if (data.SysReference.Name == "JOB_EXPERIENCE" && data.IsActive == true)
				            vm.listJob.push(data);
				        else if (data.SysReference.Name == "DIPLOMA" && data.IsActive == true)
				            vm.listEducation.push(data);
				        else if (data.SysReference.Name == "CERTIFICATE" && data.IsActive == true)
				            vm.listCertificate.push(data);
				    });
				} else {
					$.growl.error({ message: "Gagal mendapatkan data Tenaga Ahli Perusahaan" });
					UIControlService.unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				//$.growl.error({ message: "Gagal Akses API >" + err });
				UIControlService.unloadLoading();
			});
		}

		vm.UbahTenagaAhli = UbahTenagaAhli;
		function UbahTenagaAhli(data) {
			vm.flag = 0;
			//console.info("masuk form add/edit");
			var data = {
				act: false,
				item: data,
				PrequalStepID: vm.PrequalStepID
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/FormTenagaAhli.html',
				controller: 'formTenagaAhliPrequalCtrl',
				controllerAs: 'formTenagaAhliCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				initialize();
				viewdetail(vm.dataExpert);
			});
		}

		vm.lihatDetail = lihatDetail;
		function lihatDetail(data) {
			vm.flag = 0;
			//console.info("masuk form add/edit");
			var data = {
			    item: data,
			    PrequalStepID: vm.PrequalStepID
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/DetailTenagaAhli.html',
				controller: 'DetailTenagaAhliPrequalCtrl',
				controllerAs: 'DetailTenagaAhliCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});

			modalInstance.result.then(function () {
				initialize();
				viewdetail(vm.dataExpert);
			});
		}

		vm.HapusData = HapusData;
		function HapusData(obj, act) {
			vm.flag = 0;
			TenagaAhliService.editActive({
				ID: obj.ID,
				IsActive: act
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    if (act === false)
				        UIControlService.msg_growl("success", "MESSAGE.SUCCESS_NON_AKTIF");
                    if (act === true)
                        UIControlService.msg_growl("success", "MESSAGE.SUCCESS_AKTIF");
					$timeout(function () {
						window.location.reload();
					}, 1000);
				} else {
				    UIControlService.msg_growl("error", "MESSAGE.FAILED_NON_AKTIF");
				    return;
				}
			}, function (err) {
				UIControlService.unloadLoading();
			});
		}

		vm.TambahTenagaAhli = TambahTenagaAhli;
		function TambahTenagaAhli(data) {
			//console.info("masuk form add/edit");
			var data = {
				act: true,
				item: data,
                PrequalStepID: vm.PrequalStepID
			}
			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/FormTenagaAhli.html',
				controller: 'formTenagaAhliPrequalCtrl',
				controllerAs: 'formTenagaAhliCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {

				initialize();
				//viewdetail(vm.dataExpert);
				window.location.reload();
			});
		}

		vm.hapusdetail = hapusdetail;
		function hapusdetail(data, act) {
			TenagaAhliService.editCertificateActive({
				ID: data.ID,
				IsActive: act
			}, function (reply) {
				UIControlService.unloadLoading();
				if (reply.status === 200) {
				    if (act === false)
				        UIControlService.msg_growl("success", "MESSAGE.SUCCESS_DELETE");

				    if (act === true)
				        UIControlService.msg_growl("success", "MESSAGE.SUCCESS_AKTIF");
					loadCertificate(vm.dataExpert);
				} else {
					UIControlService.msg_growl("error", "Gagal menonaktifkan data ");
					return;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "Gagal Akses API ");
				UIControlService.unloadLoading();
			});
		}

		vm.DetailCertificate = DetailCertificate;
		function DetailCertificate(data1, act, type) {
		    console.info(data1);
			if (data1 == undefined) {
				vm.cc = {
					VendorExpertsPrequalID: vm.dataExpert.ID
				}
			} else {
				vm.cc = data1
			}

			var data = {
				type: type,
				act: act,
				item: vm.cc
			}

			var modalInstance = $uibModal.open({
			    templateUrl: 'app/modules/rekanan/pemasukkan-prakualifikasi/data-perusahaan-prakualifikasi/tenaga-ahli/FormDetailTenagaAhli.html',
				controller: 'formDetailTenagaAhliPrequalCtrl',
				controllerAs: 'formDetailTenagaAhliCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				initialize();
				viewdetail(vm.dataExpert);
			});
		}

		vm.viewdetail = viewdetail;
		function viewdetail(data) {
			vm.namavendor = data.Name;
			vm.flag = 1;
			loadCertificate(data);
			vm.dataExpert = data;
		}

		vm.Submit = Submit;
		function Submit() {
		    if (vm.flagCV == false) {
		        UIControlService.msg_growl("error", "MESSAGE.CV_NOT_COMPLETE");
		        return;
		    }
		    else {
		        TenagaAhliService.Submit({ Status: vm.PrequalStepID }, function (reply) {
		            UIControlService.unloadLoading();
		            if (reply.status == 200) {
		                UIControlService.unloadLoading();
		                UIControlService.msg_growl("success", "Success Submit");
		                $state.go('pemasukkan-prakualifikasi-vendor', { SetupStepID: vm.PrequalStepID });
		            } else {
		                UIControlService.unloadLoading();
		                return;
		            }
		        }, function (err) {
		            UIControlService.msg_growl("error", "Gagal Akses API ");
		            UIControlService.unloadLoading();
		        });
		    }
		}
	}
})();
