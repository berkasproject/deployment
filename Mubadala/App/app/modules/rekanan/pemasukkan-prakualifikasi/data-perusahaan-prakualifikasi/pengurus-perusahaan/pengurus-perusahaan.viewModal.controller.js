﻿(function () {
	'use strict';

	angular.module("app").controller("viewPengurusPerusahaanCtrl", ctrl);

	ctrl.$inject = ['$http', '$uibModalInstance', 'item', '$filter', '$translate', '$translatePartialLoader', '$location', 'PengurusPerusahaanService', 'UploaderService', 'UIControlService', 'GlobalConstantService', 'UploadFileConfigService'];
	/* @ngInject */
	function ctrl($http, $uibModalInstance, item, $filter, $translate, $translatePartialLoader, $location, PengurusPerusahaanService, UploaderService, UIControlService, GlobalConstantService, UploadFileConfigService) {

		var vm = this;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.compPerson = item.compPerson;
		vm.init = init;
		function init() {
		    if (vm.compPerson.Address.StateID != null) {
		        if (vm.compPerson.Address.CityID != null) {
		            if (vm.compPerson.Address.DistrictID != null) vm.address = vm.compPerson.Address.AddressInfo + ", " + vm.compPerson.Address.Distric.Name + ", " + vm.compPerson.Address.City.Name + ", " + vm.compPerson.Address.State.Name + ", " + vm.compPerson.Address.State.Country.Name;
		            else vm.address = vm.compPerson.Address.AddressInfo + ", " + vm.compPerson.Address.City.Name + ", " + vm.compPerson.Address.State.Name + ", " + vm.compPerson.Address.State.Country.Name;
		        }
		        else vm.address = vm.compPerson.Address.AddressInfo + ", " + vm.compPerson.Address.State.Name + ", " + vm.compPerson.Address.State.Country.Name;
		    }
		    else vm.address = vm.compPerson.Address.AddressInfo + ", " + vm.compPerson.Address.State.Name + ", " + vm.compPerson.Address.State.Country.Name;
		}

		vm.close = close;
		function close() {
			$uibModalInstance.dismiss('cancel');
		};
	}
})();