﻿(function () {
    'use strict';

    angular.module("app")
    .controller("detailAppWLVendorCtrl", ctrl);

    ctrl.$inject = ['$state', '$scope', '$http', '$filter', '$stateParams', '$uibModalInstance', 'item', '$translate', '$translatePartialLoader', '$location', 'UIControlService', 'GlobalConstantService', 'DashboardVendorService'];
    /* @ngInject */
    function ctrl($state, $scope, $http, $filter, $stateParams, $uibModalInstance, item, $translate, $translatePartialLoader, $location, UIControlService, GlobalConstantService, DashboardVendorService) {

        var vm = this;

        vm.WLID = item.LetterID;
        vm.remark = "";
      
        vm.init = init;

        function init() {
            $translatePartialLoader.addPart('dashboard-vendor');
        };
      
        // Revise Process
        /*
        vm.stateRevise = stateRevise;
        function stateRevise() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REVISE'), function (yes) {
                if (yes) {
                    DashboardVendorService.stateRejected({
                        LetterID: vm.WLID,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoading();
                        if (reply.status === 200) {                            
                            $uibModalInstance.close();
                            UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_REVISE'));
                        } else {
                            UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_STATE_REVISE'));
                            UIControlService.unloadLoading();
                        }
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoading();
                    });
                }
            });
        }
        */

        vm.stateComplete = stateComplete;
        function stateComplete() {
            bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_APPROVED_VENDOR'), function (yes) {
                if (yes) {
                    UIControlService.loadLoadingModal("");
                    DashboardVendorService.stateComplete({
                        LetterID: vm.WLID,
                        Remark: vm.remark
                    }, function (reply) {
                        UIControlService.unloadLoadingModal();
                        UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCC_STATE_APPROVE'));
                        $uibModalInstance.close();
                    }, function (err) {
                        UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_API'));
                        UIControlService.unloadLoadingModal();
                    });
                }
            });
        }

        vm.cancel = cancel;
        function cancel() {
            $uibModalInstance.dismiss();
        };
    }
})();