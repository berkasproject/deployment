﻿(function () {
	'use strict';

	angular.module("app").controller("DetailItemMRKOCtrl", ctrl);

	ctrl.$inject = ['$state', '$stateParams', 'item', '$http', '$filter', '$uibModalInstance', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'SocketService', 'UIControlService', 'DataVHSService', 'DashboardVendorService', 'GlobalConstantService', 'Excel'];
	/* @ngInject */
	function ctrl($state, $stateParams, item, $http, $filter, $uibModalInstance, $uibModal, $translate, $translatePartialLoader, $location, SocketService, UIControlService, DataVHSService, DashboardVendorService,GlobalConstantService, Excel) {

		var vm = this;
		vm.ID = item.ID;
		var loadmsg = "MESSAGE.LOADING";
		vm.currentPage = 1;
		vm.DocName = "";
		vm.DocumentUrl = "",
        vm.pageSize = 10;
		vm.totalItems = 0;
		vm.keyword = "";
		vm.column = 1;
		vm.maxSize = 10;
		console.info(item);
		vm.init = init;
		function init() {
		    $translatePartialLoader.addPart("dashboard-vendor");
			vm.item = item.item;
			vm.Periode1 = UIControlService.convertDate(item.item.Periode1);
			vm.Periode2 = UIControlService.convertDate(item.item.Periode2);
			loadPaket(1);
			//vm.DataDocList = item.item.UploadDataDetailMRKO;
			vm.totalItems = item.item.Count;
			vm.jumlahUSD = item.item.TotalInUSD;
			$translatePartialLoader.addPart('add-adendum');
			$translatePartialLoader.addPart('contract-signoff');
		};

		vm.loadPaket = loadPaket;
		function loadPaket(current) {
			vm.currentPage = current;
			var offset = (vm.currentPage * vm.pageSize) - vm.pageSize;
			UIControlService.loadLoading(loadmsg);
			DashboardVendorService.selectUploadDataMRKO({
				Status: vm.item.ID
			},
                   function (response) {
                   	UIControlService.unloadLoading();
                   	if (response.status == 200) {
                   		vm.DataDocList = response.data;
                   		console.info("list" + JSON.stringify(vm.DataDocList));
                   	}
                   },
                   function (response) {
                   	UIControlService.unloadLoading();
                   });
		}

		vm.month = new Array();
		vm.month[0] = "January";
		vm.month[1] = "February";
		vm.month[2] = "March";
		vm.month[3] = "April";
		vm.month[4] = "May";
		vm.month[5] = "June";
		vm.month[6] = "July";
		vm.month[7] = "August";
		vm.month[8] = "September";
		vm.month[9] = "October";
		vm.month[10] = "November";
		vm.month[11] = "December";

		vm.generate = [];
		vm.generate2 = [];
		vm.generateApproval = [];
		vm.generateApproval2 = [];
		vm.makePDF = makePDF;
		function makePDF() {

			var datePeriode1 = new Date(vm.DataDocList.Periode1);
			var datePeriode2 = new Date(vm.DataDocList.Periode2);

			//header
			var newmap1 = {
				text: 'No', style: "tableHeader"
			};
			var newmap2 = {
				text: 'Storage Location', style: "tableHeader"
			};
			var newmap3 = {
				text: 'Posting Date', style: "tableHeader"
			};
			var newmap4 = {
				text: 'Reservation', style: "tableHeader"
			};
			var newmap5 = {
				text: 'Vendor', style: "tableHeader"
			};
			var newmap6 = {
				text: 'Material Document', style: "tableHeader"
			};
			var newmap7 = {
				text: 'Material Doc Item', style: "tableHeader"
			};
			var newmap8 = {
				text: 'Material', style: "tableHeader"
			};
			var newmap9 = {
				text: 'Material Description', style: "tableHeader"
			};
			var newmap10 = {
				text: 'Qty in Un. of Entry', style: "tableHeader"
			};
			var newmap11 = {
				text: 'Unit of Entry', style: "tableHeader"
			};
			var newmap12 = {
				text: 'Currency MRKO', style: "tableHeader"
			};
			var newmap13 = {
				text: 'Unit Price', style: "tableHeader"
			};
			var newmap14 = {
				text: 'Payment Value', style: "tableHeader"
			};
			var newmap15 = {
				text: 'Tax Code', style: "tableHeader"
			};
			vm.generate.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8, newmap9, newmap10, newmap11, newmap12, newmap13, newmap14, newmap15);
			vm.generate2[0] = vm.generate;
			vm.generate = [];

			for (var i = 0; i <= vm.DataDocList.UploadDataDetailMRKO.length; i++) {

				if (i <= vm.DataDocList.UploadDataDetailMRKO.length - 1) {
					var newmap1 = { text: (i + 1).toString(), style: "textRight" };
					var newmap2 = vm.DataDocList.UploadDataDetailMRKO[i].StorageLocation;
					var newmap3 = convertDate(vm.DataDocList.UploadDataDetailMRKO[i].PostingDate);
					var newmap4 = vm.DataDocList.UploadDataDetailMRKO[i].Reservation;
					var newmap5 = vm.DataDocList.UploadDataDetailMRKO[i].VendorCode;
					var newmap6 = vm.DataDocList.UploadDataDetailMRKO[i].MaterialDocument;
					var newmap7 = vm.DataDocList.UploadDataDetailMRKO[i].MaterialDocItem;
					var newmap8 = vm.DataDocList.UploadDataDetailMRKO[i].Material;
					var newmap9 = vm.DataDocList.UploadDataDetailMRKO[i].MaterialDescription;
					var newmap10 = { text: vm.DataDocList.UploadDataDetailMRKO[i].Qty, style: "textRight" };
					var newmap11 = vm.DataDocList.UploadDataDetailMRKO[i].Unit;
					var newmap12 = vm.DataDocList.UploadDataDetailMRKO[i].Currency;
					var newmap13 = { text: vm.DataDocList.UploadDataDetailMRKO[i].UnitPrice.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'), style: "textRight" };
					var newmap14 = { text: vm.DataDocList.UploadDataDetailMRKO[i].TotalValue.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'), style: "textRight" };
					var newmap15 = vm.DataDocList.UploadDataDetailMRKO[i].TaxCodeInPur;
					vm.generate.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8, newmap9, newmap10, newmap11, newmap12, newmap13, newmap14, newmap15);
				}
				else if (i == vm.DataDocList.UploadDataDetailMRKO.length) {

					var newmap1 = {
						text: 'Total of Vendor Held Stock Issues for period ' + vm.month[datePeriode1.getMonth()] + ' ' + datePeriode2.getFullYear() + ' in ' + vm.DataDocList.UploadDataDetailMRKO[0].Currency,
						colSpan: 13,
						style: "tFoot"
					}
					var newmap2 = {};
					var newmap3 = {};
					var newmap4 = {};
					var newmap5 = {};
					var newmap6 = {};
					var newmap7 = {};
					var newmap8 = {};
					var newmap9 = {};
					var newmap10 = {};
					var newmap11 = {};
					var newmap12 = {};
					var newmap13 = {};

					var newmap14 = {
						text: vm.DataDocList.TotalRealCurrency.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'),
						style: "tFoot"
					}
					var newmap15 = {
						text: '',
						style: "tFoot"
					}
					vm.generate.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6, newmap7, newmap8, newmap9, newmap10, newmap11, newmap12, newmap13, newmap14, newmap15);

				}
				vm.generate2[i + 1] = vm.generate;
				vm.generate = [];
			}
			//console.info("generate2: " + JSON.stringify(vm.generate2));


			//header Approval
			var newmap1 = {
				text: $filter('translate')('TABLE.NO'), style: "tableHeader2"
			};
			var newmap2 = {
				text: $filter('translate')('TABLE.LEVEL_INFO'), style: "tableHeader2"
			};
			var newmap3 = {
				text: $filter('translate')('TABLE.APPROVAL'), style: "tableHeader2"
			};
			var newmap4 = {
				text: $filter('translate')('TABLE.TANGGAL_APPROVE'), style: "tableHeader2"
			};
			var newmap5 = {
				text: $filter('translate')('TABLE.STATUS'), style: "tableHeader2"
			};
			var newmap6 = {
				text: $filter('translate')('TABLE.REMARK'), style: "tableHeader2"
			};
			vm.generateApproval.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6);
			vm.generateApproval2[0] = vm.generateApproval;
			vm.generateApproval = [];
			for (var i = 0; i <= vm.DataDocList.MRKOApproval.length - 1; i++) {
				var delegasi = '';
				var status = '';

				if (vm.DataDocList.MRKOApproval[i].DOAEmployeeID != null) {
					delegasi = ' ' + $filter('translate')('Delegasi Ke') + ' : ' + vm.DataDocList.MRKOApproval[i].DelegateEmployeeName;
				}

				if (vm.DataDocList.MRKOApproval[i].ApprovalStatus != null && vm.DataDocList.MRKOApproval[i].ApprovalStatus == true) {
					status = 'Approved';
				}
				else if (vm.DataDocList.MRKOApproval[i].ApprovalStatus != null && vm.DataDocList.MRKOApproval[i].ApprovalStatus == false) {
					status = 'Rejected';
				}

				var newmap1 = { text: (i + 1).toString(), style: "textRight" };
				var newmap2 = vm.DataDocList.MRKOApproval[i].LevelInfoEmp;
				var newmap3 = vm.DataDocList.MRKOApproval[i].MstEmployee.FullName + ' ' + vm.DataDocList.MRKOApproval[i].MstEmployee.SurName + delegasi;
				var newmap4 = convertDate(vm.DataDocList.MRKOApproval[i].ApprovalDate);
				var newmap5 = status;
				var newmap6 = vm.DataDocList.MRKOApproval[i].Remark;
				vm.generateApproval.push(newmap1, newmap2, newmap3, newmap4, newmap5, newmap6);
				vm.generateApproval2[i + 1] = vm.generateApproval;
				vm.generateApproval = [];
			}


			var documentDefinition = {
				content:
                [
                    {
                    	image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAyCAYAAACQyQOIAAANoklEQVR4nO1de5AcRRn/dmfvwsMcyWWXA8UUAgEVLVFBSIDyeJSISKkl6xPKKNYKyuZub3u2HzO70xcKpaBACUgBicgjDzhCILvJlbzEKrUUTSWlsXgEBBF5JbcbAwnJZe+u/WO6Z3pf98ru3uHdV9WV25nunn785vf9uuebCYBu0agB0ajh/U4kDoc0/XyI29xw7M0hbu8wHHuP4djDIW4Lw7GLBrd2hhzrTyFu3xzi9oUQj8+RpQPAO0PAIQhTZJxD8GkOoac5hAAgoJ0K1Dg+w41DsAQAafJpg7MVBrdeDnFbTDDtCGaYAzz1wap1N8H6+sDo64OKa265E1q23Akt5cdnAQHgsoC6a7m1xODWk/rEGtwaMbg1JNOIwa2RivOOPVx+3uDW3hC3bwBC5ldcp0HGOQR1ABRy8xcWcuGr8rnwukI2vLWQjbxSyIX/VchFthVy4dUD2QVXvLrmqPmqLMx4MPBk2ODWam9yHXvYcOyicgETSeVlDW69ZWSs77rXaRg7BCQAAgAA+Vz74nw28nA+F9lfyEVE9RQeKeQiopCNDOSz4d5dG8NzVV0NaN/0NyNjXWo49qty0oYMbg1Nwh1UAsJliqKmJx4BSiMAAMA7Q/Vqf18fGPJuhoENCz5WyIazpZMdLhZy4WIhGx4uSe6xIQWIfC78n0Ju/kIAjx1mlkmhJwzHHqwHAGoxhATHG6EMOw8AFBgmfffpbuCt++DIgWzk+kI2POje5XKi1V0/agoflEB4QnMrM5AVbPtEg1tvqLu4EWDQVhgjBrdGghmWBIDJ6oaALu4Kj0a+VMhGXpYAGHLv8rEmX6ZseFi6h1fe2vCBo1X99Rvc95kZGevryjU0CghV2OFujxXGpxsCT3MIKdrOP9x+XD4X6StzAeNggBK3MVLIhod2PRLuBHDdTONGebqb9Nchx/q5unMbCgaXFQ7K378DShfo7ahontwLAHmnvtYHhxeyC3A+F96jscDw+AHgAaFYyEVEPhtGAN4SckZbAAACEIu1hBzrj81ghhLAOdYLwOnJAODpBjX5umh7cQXMyW+M/EBzA8MTcgPVQJCLPAQw05lANzXgduoUw7F3N1ovlIPB4NYApOm5JW1RS8FH2z9cyC7AHgAknU/MDVTqgnwuvOO1vrZ2/VqzBuBRs5Gxvt0sVtCvYzj2oMHT3xICQv9cf+zpO7OReD4beSyfjbynTeIk3UCFLhjM59oXA8yyQXVTYODstmboBY0Zhlu5JSCdOeDccfHbQ/1tYncuInZlO3wqPyQAuCkvXcJAdkEcYFYXjGYuRcbjc0Lc/kuTmWGkxbGHIcPFlbdeNrJnU3tx36b5QzuzHZNzASUAiIh87ujink0LxK6NR68GmGWCsU356DQ+1eDWO83SCyq1cDYC9rViyY0/Es9u+IgY7p8rdmU7xECuY1Ig2JXtEIVsePjA5nniz32nPHv9nRceJXs6qwvGNF8vfK+ZrKDSYb1MQLpXzF9OxH33LhbF/jaxd3O72Jk9ZlyAyEsA7MweI97bPG/kYH+buGXVeQeO4YnPAQBEo7NsMH6TmzyGY93ZTL2g0pxeSwSdjID0chH9xeVi+8MnipH+uWLv5nYxkHMneVe2oySpY7tzEXFg8zwx1N8mtq0/qXjJzUsFsOt/AgDQWcfnHDPFXOrksSNC3N46FczQyi0xp9cSkF4ujuxlInH7V8XW9YvEe5vni+H+uaLY3yYG++eJwf55otjf5h3ble0Qj607TXx/RbTYyi0B7Lo1AADNjov4/zE1cGn8KRlf0FS9oLODwdMC7GtFK7fEhTddKfhdF4u1958pNq39jMiu+ay4/97F4mcrvyAuX/EdccrPlgnIOMOQXi5C3H72BByb1QWHbP6S8odTwQo+O7jaIcRtAeleAfa17r8Z7qb0cnlsuYB0ZqSVW6KVW4MtaXomAMyyQV3M1wu/ngq9UA6IOb2WOKyXiTm9lpATXnKsxbGKIW6LILeuAYC6xj/MdHMpNZWaG3Ks7RIME45eakbytq0de1YXNMTUgHLrdMOx90s30XS9MAYIXHA61nPAu+fJls/qgrqbpNhgxr56ql1EBQhU0KxjDwK3zgKACbNBNBo1Ojs7Q52dnaHoOMtOpMxk6geAgCrT2TkxF6dfb6w0kXpV7a5ekMGu0wUMqh3BjB0HgHroggCMzSbjyVMr70TKTktzG4/xUSHHeq6EkqcYBAa31gLApHUBShGKML3DTLGVyRS5qqS/lRYEAEim6BUI01UI07tQilyjnyvPaxKy1EyxlQjTVT0m+WaNvMoCAADxOI0kMV2BML0dYXpTPB5vG6Nd3jkTszjCdFUS0xVJTG9DhN6qkvqdxHQFIvTWUeoaxXy9cJYKep0qvaCB8PnJ6gJFjYiw603ChJtoobu7+9ga9QUAAJYuXXoYwuwlVSZJWEKvT897NSHzTUxf9+rH7PloNNo6SnuDAADLTHMRwnTIJEwgwt41TfOYcfQxCACAMH3KJEyo8lXSsNsW+vg4h6qKeXqBdU2Vi1DBsAa3DkKaLgaAybKBe8ea5okI070mYYMmYQKl6DKAiokF5d8Rxpf4A0rfisdluL42SV7eFL1S5t2PMD3gThC+VM9TrU2JBDnJz892d3WxjvJr1CqLCH1MAuGABMOwSvL3foTpmqvVS0iTNm9/wX5wKsDg6QJuLQOAQ9IF/uSy9dpd+9fR8pqErlZ5EaZ36ec0C8h6fy/zFmUSCLP1NcoA1IERTEyf9PtCe7swXmia5mkIsU8kMP7kNSn5WmIdtIp8HpFoNxzrxWbqBW2/YB24o3lI+wXld7k/+KRTP6/63NXFOkxCB9RAJ0w31K5sUoMAAD0YL/EAQ9i7inFMwvYlCDlBz1tetl5ASBISA6hkN5m3DqLVex5Bz631fmTDdIFjvQA+rR1qZwIA7kAhQrf7E0fvcbvp9tPTEz7VCxOzZ6oPjWSOFFupAWG5xg4CpQjV69WsvkBIkR8DAMRisSOi0WhrNBptjcViLVBbrE7ClF7gFmq0i6ijLqgwXzQS7FM+261RaBCqDLJaLVQTid3d3ccizPKKZbq6WIeJaUoD2natnD65zWKEOpuMbDK4taGRYPD3C1iXe926diwIANCF8UIT03cQpiMmYSKJ3WsplZ9M0o/79E4Hqk2OxhzdGqh+AwCQwPhUhOlBVb9pki/K+ivcSv00AnsGYXq3Sehak9DViND7TEI3Ioy/cqiDVm5SL6CjDcd6pRF6QdsveADckav7c4SqQpDQLQD65JK0JhLv1suVjAVAEGG2zadneoXKgAh9Qluqrq1SR92AUGP5WJRsZk16sGqa7EiI2+fXWys0SBdU6YLbh54Uu6CaaIzFYi2I0L/5IKkQk97fSYwv0tjg5VgsdgT4q4hvaIr+nS6MF8riQf3fOgFhxMT0vyamryNM30SYvmli+ppJ2KBiu/qbrxdYvVxEiS7g1hIAaMZTxQAidIs2kfcCACBCzh/v8hJh+qA22X9PEpZEKeKYmBKUYjeYhBW9uxVTBFDiw+urEeSEJxKJ9hjGR8Xj8bbu7u55cf9TRw0wXy9skhN5SMEs2n5Bt1t/YwWP5t+XaWp/z9KlSw9DhN5SPrhlAszbnDIJ26dYxdMDZTt73iRjtg18NgjA+1Ys6qZC4hk71uDWvw9FL2j7BQ8CQLPiCyoUvzvxlPlLS1pIJBIf0vMD+AOdxDSjfHENECiNMSJ3+0RPil3gdjFqQA0gqBWMlkdPnjYBqFg+XgUAEI/H50SjUUNP0AAX65vSCxl20WSBoJXZATzR1PcUy/cAEKZDiLB3te3ee/V8etsSicThJmY7tOVlOpkkZ/Sk2AWIkM6eFDuvJ8XOS2J8jonpP8rdz2hAmMgW89QzgjL/eYQzUb3g6QLHLkKang0ATY028oBg0rMVjWtJIELO1/PpfydT9DLNpbxxdRVxW2vPQrGMqqsECJjuRYgu7ibk+J4eevIy01y0zDQXJRLkpK5U6hTlNry2l2gEasfjNJJM0o/q5Xp66Mmau2mgKb3gWI9NRC9o+wUJt54piTt0FT6hf5AT8V4Nf65MPvVjmzQg/BKgkpLlrh4kCDkBYboXYXpQ35xSexaJBDnJJGxQ6oyDcv+iPO2Ty9z79LImpo/Ldh+Q+xYH9HKqPyahqxs/lN6n+1LHeZ/oGcNNaPsFfeD2bKriDvVnBfsUGySJ++mfaiIxmSRnaL5fICR3P6ts5Wqri4f11QVo/n6ZaS7yWKi21pAPseg6WW+rrHfUx9D+cfpAY4avsscyqol+eSwg+F9hs170vqYytZE87uNgjE81CX0AYbpVW/NXPm7G+FKE6YOI0HsQYT8dIyTNrduk5yJM15kpthIRek88lTpOZejqYh0y8OVuE7NfVU1usMu6ch2ACDFNQtciTFeNUm6NFoTTBPM/0XNdLb1Qogu4dQ4ATJco5PIndBMB5njyvq9D1iZjAQAIGI71VDW9oO0X9ADAdHsfIagttUYNE5tEgGpwlEDSkuDVMQJQS9zPeINXy8s13jy9QI43HPttxQJl+wUPyV5MByaYtYaZ/8r915Qm0D7X+9I00QWz1hTz9cKNig0Mbg15H9SaZYMZY66fjUYNg1u/lfsFjYgvmLVpb/5/DXC69z7CTPwY9jSx/wHaStM+GgZv+AAAAABJRU5ErkJggg==',
                    	width: 70,
                    	height: 28,
                    	alignment: 'right'
                    },
                    {
                    	text: 'Mubadala Petroleum',
                    	alignment: 'right',
                    	bold: true,
                    	fontSize: 7
                    },
                    {
                    	text: 'SCM DEPT - PROCUREMENT ( DP.14 )',
                    	alignment: 'right',
                    	bold: true,
                    	fontSize: 7
                    },
                    {
                    	text: vm.item.Vendor.SAPCode + '-' + vm.item.Vendor.business.Name + '. ' + vm.item.Vendor.VendorName + ' ' + vm.item.ContractNumber + '-' + vm.item.StorageLocation,
                    	decoration: 'underline',
                    	alignment: 'center',
                    	bold: true,
                    	fontSize: 10,
                    	margin: [0, 10, 0, 0]
                    },
                    {
                    	text: 'PERIOD : ' + vm.month[datePeriode2.getMonth()] + ' ' + datePeriode1.getFullYear() + ' ( RUN DATE ' + datePeriode1.getDate() + '/' + (datePeriode1.getMonth()) + 1 + '/' + datePeriode1.getFullYear() + ' )',
                    	alignment: 'center',
                    	bold: true,
                    	fontSize: 9
                    },
                    {
                    	style: 'tableStyle',
                    	table:
                        {
                        	widths: ['auto', 'auto', '*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', '*', 'auto', 'auto', '*', '*', 'auto'],
                        	body: vm.generate2
                        }
                    },
                    {
                    	style: 'marginAtas',
                    	columns: [
				            {
				            	width: 50,
				            	text: 'Reviewed by :',
				            	bold: true,
				            	alignment: "left",
				            	fontSize: 7
				            },
				            {
				            	width: 100,
				            	text: vm.DataDocList.RFQVHS.Employee.FullName + ' ' + vm.DataDocList.RFQVHS.Employee.SurName,
				            	alignment: "left",
				            	fontSize: 7
				            }
                    	]
                    },
                    {
                    	style: 'tableStyle2',
                    	table:
                        {
                        	widths: ['auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
                        	body: vm.generateApproval2
                        }
                    }
                ],


				styles:
                {
                	tableHeader: {
                		bold: true,
                		alignment: 'center',
                		fillColor: '#EAC2C2'
                	},
                	tFoot: {
                		bold: true,
                		alignment: 'right',
                		fillColor: '#EAC2C2'
                	},
                	textRight: {
                		alignment: 'right'
                	},
                	marginAtas: {
                		margin: [0, 10, 0, 0],
                	},
                	tableStyle: {
                		fontSize: 4,
                		margin: [0, 10, 0, 0],
                		alignment: 'left'
                	},
                	tableHeader2: {
                		bold: true,
                		alignment: 'center'
                	},
                	tableStyle2: {
                		fontSize: 7,
                		margin: [0, 10, 0, 0],
                		alignment: 'left'
                	}
                }
			};

			pdfMake.createPdf(documentDefinition).download(vm.item.Vendor.SAPCode + '-' + vm.item.Vendor.business.Name + '. ' + vm.item.Vendor.VendorName + ' ' + vm.item.ContractNumber + '-' + vm.item.StorageLocation + ' PERIOD ' + vm.month[datePeriode2.getMonth()] + ' ' + datePeriode1.getFullYear() + ' (RUN DATE ' + datePeriode1.getDate() + '-' + (datePeriode1.getMonth()) + 1 + '-' + datePeriode1.getFullYear() + ').pdf');
		}

		vm.convertDate = convertDate;
		function convertDate(date) {
			return UIControlService.convertDate(date);
		}


		vm.valid = true;
		vm.Remark = "";
		vm.notvalid = function notvalid() {
		    vm.valid = false;
		}

		vm.submitByVendor = function submitByVendor(param) {
		    vm.valid = param;
		    //console.info("param:" + param);
		    if (param == true) {
		        //vm.status = 4330;
		        vm.Status = 4288;
		    }
		    else if (param == false) {
		        //vm.status = 4421;
		        vm.Status = 4306;
		    }
		    var toSave = {
		        Status: vm.Status,
		        column: vm.item.ID,
                Keyword:vm.Remark
		    }
		    //console.info("toSave" + JSON.stringify(toSave));
		    bootbox.confirm($filter('translate')('MESSAGE.CONFIRM_REVISE'), function (res) {
		        if (res) {
		            DashboardVendorService.acknowledgeMRKO(toSave, function (reply) {
		                UIControlService.unloadLoading();
		                if (reply.status === 200) {
		                    UIControlService.msg_growl("success", "MESSAGE.SUCC_ACKNOWLEDGE");
		                    UIControlService.unloadLoading();
		                    $uibModalInstance.close();
		                    vm.init;
		                }
		            }, function (err) {
		                UIControlService.msg_growl("error", "MESSAGE.sUCC_STATE_REVISE");
		                UIControlService.unloadLoading();
		            });
		        }
		    });
		    /*
            CprvVendorService.submitbyvendor({
                VPVHSDataId: vm.vpdata.VPVHSDataId,
                //StatusDescription: vm.status,
                SysReference: { Name: vm.status },
                NoteFromVendor: vm.NoteFromVendor
            }, function (reply) {
                UIControlService.unloadLoading();
                if (reply.status === 200) {
                    UIControlService.msg_growl("success", $filter('translate')('MESSAGE.SUCCESS_SUBMITDATA'));
                    $uibModalInstance.close();
                } else {
                    UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
                    //UIControlService.unloadLoadingModal();
                }
            }, function (err) {
                UIControlService.unloadLoading();
                UIControlService.msg_growl("error", $filter('translate')('MESSAGE.ERR_LOAD_VPDATA'));
            });*/
		}

        
		vm.filterdata = [];
		vm.exportItem = exportItem;
		function exportItem() {

		    for (var i = 0; i <= vm.DataDocList.UploadDataDetailMRKO.length - 1; i++) {
		        //no
		        var no_convert = '';
		        no_convert = i + 1;
		        var forExcel = {
		            No: no_convert,
		            StorageLocation: vm.DataDocList.UploadDataDetailMRKO[i].StorageLocation,
		            PostingDate: vm.convertDate(vm.DataDocList.UploadDataDetailMRKO[i].PostingDate),
		            Reservation: vm.DataDocList.UploadDataDetailMRKO[i].Reservation,
		            VendorCode: vm.DataDocList.UploadDataDetailMRKO[i].VendorCode,
		            MaterialDocument: vm.DataDocList.UploadDataDetailMRKO[i].MaterialDocument,
		            MaterialDocItem: vm.DataDocList.UploadDataDetailMRKO[i].MaterialDocItem,
		            Material: vm.DataDocList.UploadDataDetailMRKO[i].Material,
		            MaterialDescription: vm.DataDocList.UploadDataDetailMRKO[i].MaterialDescription,
		            QtyinUnitofEntry: vm.DataDocList.UploadDataDetailMRKO[i].Qty,
		            UnitofEntry: vm.DataDocList.UploadDataDetailMRKO[i].Unit,
		            Currency: vm.DataDocList.UploadDataDetailMRKO[i].Currency,
		            UnitPrice: vm.DataDocList.UploadDataDetailMRKO[i].UnitPrice,
		            TotalValue: vm.DataDocList.UploadDataDetailMRKO[i].TotalValue,
		            TaxCodeInPur:vm.DataDocList.UploadDataDetailMRKO[i].TaxCodeInPur

		        }
		        vm.filterdata.push(forExcel);
		    }
		    JSONToCSVConvertor(vm.filterdata, true);
		}


		vm.JSONToCSVConvertor = JSONToCSVConvertor;
		function JSONToCSVConvertor(JSONData, ShowLabel) {
		    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		    var arrData = JSONData;
		    //console.info(arrData[0]);
		    var CSV = '';

		    //This condition will generate the Label/Header
		    if (ShowLabel) {
		        var row = "sep=," + '\n';

		        //This loop will extract the label from 1st index of on array
		        for (var index in arrData[0]) {

		            //Now convert each value to string and comma-seprated
		            row += index + ',';
		        }

		        row = row.slice(0, -1);
		        //console.info(row);
		        //append Label row with line break
		        CSV += row + '\r\n';
		        //console.info(CSV);
		    }

		    //1st loop is to extract each row
		    for (var i = 0; i < arrData.length; i++) {
		        var row = "";

		        //2nd loop will extract each column and convert it in string comma-seprated
		        for (var index in arrData[i]) {
		            row += '"' + arrData[i][index] + '",';
		        }

		        row.slice(0, row.length - 1);

		        //add a line break after each row
		        CSV += row + '\r\n';
		    }

		    if (CSV == '') {
		        alert("Invalid data");
		        return;
		    }

		    //Generate a file name
		    var fileName = "Detail Item VHS";

		    //Initialize file format you want csv or xls
		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

		    // Now the little tricky part.
		    // you can use either>> window.open(uri);
		    // but this will not work in some browsers
		    // or you will not get the correct file extension    

		    //this trick will generate a temp <a /> tag
		    var link = document.createElement("a");
		    link.href = uri;

		    //set the visibility hidden so it will not effect on your web-layout
		    link.style = "visibility:hidden";
		    link.download = fileName + ".csv";

		    //this part will append the anchor tag and remove it after automatic click
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
		}



		vm.back = back;
		function back() {
			$uibModalInstance.close();
		}
	}
})();
