(function () {
	'use strict';

	angular.module("app")
    .controller("DashboardVendorCtrl", ctrl);

	ctrl.$inject = ['$http', '$filter', '$state', '$uibModal', '$translate', '$translatePartialLoader', '$location', 'DashboardVendorService', 'PengumumanPengadaanService', 'NewsService', 'CprvVendorService', 'CprVendorService', 'UIControlService', '$window', '$stateParams', 'GlobalConstantService'];
	/* @ngInject */
	function ctrl($http, $filter, $state, $uibModal, $translate, $translatePartialLoader, $location, DashboardVendorService, PengumumanPengadaanService, NewsService, CprvVendorService, CprVendorService, UIControlService, $window, $stateParams, GlobalConstantService) {
		var vm = this;
		var loadmsg = "MESSAGE.LOADING";
		var loadingCount;

		vm.folderFile = GlobalConstantService.getConstant('api') + "/";
		vm.show_regServices = false
		vm.show_regAward = false
		vm.totalTenderAward = 0;
		vm.listVendorsReject = [];
		vm.paket = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.prekual = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};


		vm.prekualAnnounce = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.paketPO = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.pengumuman = {
			currentPage: 1,
			pageSize: 10,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.cpr = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.cprvhs = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.news = {
			currentPage: 1,
			pageSize: 10,
			totalItems: 0,
			keyword: "",
			list: []
		};
		vm.FilterColumn = 0;
		vm.textSearch = '';
		vm.maxSize = 10;
		vm.currentPage = 0;

		vm.kata = new Kata("");
		vm.orderBy = 1;
		vm.isAsc = true;

		vm.paketMRKO = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.paketFPA = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.warning = {
			currentPage: 1,
			pageSize: 10,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.paketCSO = {
			currentPage: 1,
			pageSize: 5,
			totalItems: 0,
			keyword: "",
			list: []
		};

		vm.langID = true;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart("dashboard-vendor");
			$translatePartialLoader.addPart("permintaan-ubah-data");

			if (localStorage.getItem('currLang').toLowerCase() != 'id') {
			    vm.langID = false;
			}

			UIControlService.loadLoading(loadmsg);
			loadingCount = 10;
			loadPaket();
			loadPrekual();
			loadPaket1(1);
			loadDataPengumuman(1);
			jLoad(1);
			//loadDataCpr();
			//loadDataCprVhs();
			loadNews(1);
			loadMonitoringVHS(1);
			loadPrequalAnnounce();
			cekAcknowledgement();
			CPR(1);
			CPRVHS(1);
			loadWarning();
			loadCSO(1);
			loadMonitoringFPA(1);
			loadVendorReject();
		}

		vm.loadNews = loadNews;
		function loadNews() {
			DashboardVendorService.newsbyarea({
			}, function (reply) {
				//console.info("reply: "+JSON.stringify(reply));
				if (reply.status === 200) {
					vm.berita = reply.data;
					//console.info("berita" + JSON.stringify(vm.berita));
					vm.totalItems = reply.data.length;
					//	} else {
					//		$.growl.error({ message: "Gagal mendapatkan data berita!!" });
					//		$rootScope.unloadLoading();
					//	}
					//	$rootScope.unloadLoading();
					//}, function (err) {
					//	$.growl.error({ message: "Gagal Akses API >" + err });
					//	$rootScope.unloadLoading();
					//});
				} else {
					//$.growl.error({ message: "Gagal mendapatkan data berita!!" });
					//$rootScope.unloadLoading();
					//return;
				}
				//$rootScope.unloadLoading();
			}, function (err) {
				$.growl.error({ message: "Gagal Akses API >" + err });
				$rootScope.unloadLoading();
			});
		}

		vm.remHtml = remHtml;
		function remHtml(text) {
			return text ? String(text).replace(/<[^>]+>/gm, '') : '';
		}

		vm.jLoad = jLoad;
		function jLoad(current) {
			UIControlService.loadLoading("MESSAGE.LOADING");
			vm.currentPage = current;
			var offset = (current * 5) - 5;
			DashboardVendorService.getDataCR({
				column: vm.FilterColumn,
				Keyword: vm.textSearch,
				Offset: (current - 1) * vm.maxSize,
				Limit: 10
			}, function (reply) {
				unloadLoading();
				if (reply.status === 200) {
					var data = reply.data;
					vm.listVendors = data.List;
					if (vm.listVendors) {
						for (var i = 0; i < vm.listVendors.length; i++) {
							if (!(vm.listVendors.ChangeRequestDate === null)) {
								vm.listVendors[i].ChangeRequestDate = UIControlService.getStrDate(vm.listVendors[i].ChangeRequestDate);
							}
							if (!(vm.listVendors[i].EndChangeDate === null)) {
								vm.listVendors[i].EndChangeDate = UIControlService.getStrDate(vm.listVendors[i].EndChangeDate);
							}
						}
					}
					vm.totalItems = data.Count;
				} else {
					UIControlService.msg_growl("error", "MESSAGE.FAIL_GET_MASTER_BUS");
					unloadLoading();
				}
			}, function (err) {
				//console.info("error:" + JSON.stringify(err));
				UIControlService.msg_growl("error", "MESSAGE.ERR_API");
				unloadLoading();
			});
		}

		vm.loadVendorReject = loadVendorReject;

		function loadVendorReject() {

		    DashboardVendorService.getDataVendorReject({
		    }, function (reply) {
		        if (reply.status === 200) {
		            var data = reply.data;
		            vm.listVendorsReject = data.List;
		            vm.totalItems = data.Count;
		        } else {
		            UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		            unloadLoading();
		        }
		    }, function (err) {
		        //console.info("error:" + JSON.stringify(err));
		        UIControlService.msg_growl("error", "MESSAGE.ERR_API");
		        unloadLoading();
		    });
		}

		vm.reloadPaket = reloadPaket;
		function reloadPaket() {
			loadingCount = 1;
			UIControlService.loadLoading(loadmsg);
			loadPaket();
		}

		vm.reloadCPR = reloadCPR;
		function reloadCPR(current) {
			loadingCount = 1;
			UIControlService.loadLoading(loadmsg);
			CPR(current);
		}

		vm.reloadCPRVHS = reloadCPRVHS;
		function reloadCPRVHS(current) {
		    loadingCount = 1;
		    UIControlService.loadLoading(loadmsg);
		    CPRVHS(current);
		}

		vm.searchPaket = searchPaket;
		function searchPaket() {
			vm.paket.currentPage = 1;
			UIControlService.loadLoading(loadmsg);
			loadPaket();
		}

		function loadPaket() {
			DashboardVendorService.SelectTender({
				Keyword: vm.paket.keyword,
				Offset: vm.paket.pageSize * (vm.paket.currentPage - 1),
				Limit: vm.paket.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paket.list = reply.data.List;
				vm.paket.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.reloadPrekual = reloadPrekual;
		function reloadPrekual() {
			loadingCount = 1;
			UIControlService.loadLoading(loadmsg);
			loadPrekual();
		}

		vm.searchPrekual = searchPrekual;
		function searchPrekual() {
			vm.prekual.currentPage = 1;
			UIControlService.loadLoading(loadmsg);
			loadPrekual();
		}

		function loadPrequalAnnounce() {
			DashboardVendorService.loadPrequalAnnounce({ Limit: 5 }, function (reply) {
				unloadLoading();
				vm.prekualAnnounce.list = reply.data.List;
				vm.prekualAnnounce.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_ANNOUNCEMENT');
			});
		}

		function loadPrekual() {
			DashboardVendorService.SelectPrequal({
				Keyword: vm.prekual.keyword,
				Offset: vm.prekual.pageSize * (vm.prekual.currentPage - 1),
				Limit: vm.prekual.pageSize
			}, function (reply) {
				unloadLoading();
				vm.prekual.list = reply.data.List;
				vm.prekual.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.loadPaket1 = loadPaket1;
		function loadPaket1(current) {
			vm.paketPO.currentPage = current;
			DashboardVendorService.MonitoringPO({
				Keyword: vm.paketPO.keyword,
				Offset: vm.paketPO.pageSize * (vm.paketPO.currentPage - 1),
				Limit: vm.paketPO.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paketPO.list = reply.data.List;
				vm.paketPO.totalItems = reply.data.Count;
				calculateTotalTenderAward();
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.detailtahapan = detailtahapan;
		function detailtahapan(tender) {
			$state.transitionTo('detail-tahapan-vendor', { TenderID: tender.TenderID });
		};

		vm.detailCPR = detailCPR;
		function detailCPR(data) {
			$state.transitionTo('cpr', { CPRID: data.VPCPRDataId });
		};
		vm.detailCPRVHS = detailCPRVHS;
		function detailCPRVHS(data) {
			$state.transitionTo('cprv', { VPVHSID: data.VPVHSDataId });
		};

		vm.detailtahapanPrakual = detailtahapanPrakual;
		function detailtahapanPrakual(prakual) {
			$state.transitionTo('detail-tahapan-prakual-vendor', { PrequalSetupID: prakual.PrequalID });
		};

		vm.detailmonitoringPO = detailmonitoringPO;
		function detailmonitoringPO(tender) {
			$state.transitionTo('detail-monitoring-po', { ID: tender.ID });
		};

		function loadDataPengumuman(current) {
			var offset = (current * 10) - 10;
			PengumumanPengadaanService.getAllDataAnnouncementByVendor({
				Keyword: vm.pengumuman.keyword,
				Offset: vm.pengumuman.pageSize * (vm.paket.currentPage - 1),
				Limit: vm.pengumuman.pageSize
			}, function (reply) {
				//console.info("announc::"+JSON.stringify(reply));
				unloadLoading();
				if (reply.status === 200) {
					vm.pengumuman.list = reply.data.List;
					for (var i = 0; i < vm.pengumuman.list.length; i++) {
						vm.pengumuman.list[i].RegistrationStartDate = UIControlService.convertDateTime(vm.pengumuman.list[i].RegistrationStartDate);
						vm.pengumuman.list[i].RegistrationEndDate = UIControlService.convertDateTime(vm.pengumuman.list[i].RegistrationEndDate);
					}
					vm.pengumuman.totalItems = reply.data.Count;
				}
			}, function (err) {
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_PO");
				unloadLoading();
			});
		}

		function loadPengumuman() {
			PengumumanPengadaanService.getDataAnnouncementByVendor({
				Keyword: vm.pengumuman.keyword,
				Offset: vm.pengumuman.pageSize * (vm.paket.currentPage - 1),
				Limit: vm.pengumuman.pageSize
			}, function (reply) {
				vm.pengumuman.list = reply.data.List;
				unloadLoading();
				vm.pengumuman.totalItems = reply.data.Count;
			}, function (err) {
				unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_ANNOUNCEMENT");
			});
		}

        /*
		function loadDataCpr() {
			CprVendorService.select({
				Keyword: vm.cpr.keyword,
				Offset: vm.cpr.pageSize * (vm.cpr.currentPage - 1),
				Limit: vm.cpr.pageSize
			}, function (reply) {
				vm.cpr.list = reply.data.List;
				unloadLoading();
				vm.cpr.totalItems = reply.data.Count;
			}, function (err) {
				unloadLoading();
				UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_CPR");
			});
		}
        */
		/*
		function loadDataCprVhs() {
		    CprvVendorService.select({
		        Keyword: vm.cprvhs.keyword,
		        Offset: vm.cprvhs.pageSize * (vm.cprvhs.currentPage - 1),
		        Limit: vm.cprvhs.pageSize
		    }, function (reply) {
		        vm.cprvhs.list = reply.data.List;
		        unloadLoading();
		        vm.cprvhs.totalItems = reply.data.Count;
		    }, function (err) {
		        unloadLoading();
		        UIControlService.msg_growl("error", "MESSAGE.ERR_LOAD_CPR_VHS");
		    });
		}*/

		function unloadLoading() {
			loadingCount--
			if (loadingCount <= 0) {
				//UIControlService.unloadLoading();
				$.unblockUI({
					onUnblock: function () {
						setTimeout($.unblockUI, 3000);

						if (vm.monitoringPOID == 0) {
							cekAcknowledgementContract();
						}
					}
				})
			}
		}

		vm.loadMonitoringVHS = loadMonitoringVHS;
		function loadMonitoringVHS(current) {
			DashboardVendorService.MonitoringMRKO({
				Keyword: vm.paketMRKO.keyword,
				Offset: vm.paketMRKO.pageSize * (vm.paketMRKO.currentPage - 1),
				Limit: vm.paketMRKO.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paketMRKO.list = reply.data.List;
				vm.paketMRKO.totalItems = reply.data.Count;
				calculateTotalTenderAward();
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.loadMonitoringFPA = loadMonitoringFPA;
		function loadMonitoringFPA(current) {
			DashboardVendorService.MonitoringFPA({
				Keyword: vm.paketFPA.keyword,
				Offset: vm.paketFPA.pageSize * (vm.paketFPA.currentPage - 1),
				Limit: vm.paketFPA.pageSize
			}, function (reply) {
				unloadLoading();
				vm.paketFPA.list = reply.data.List;
				vm.paketFPA.totalItems = reply.data.Count;
				calculateTotalTenderAward();
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		vm.detailmonitoringMRKO = detailmonitoringMRKO;
		function detailmonitoringMRKO(data) {
			var item = {
				item: data
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/detail-itemmrko-modal.html',
				controller: 'DetailItemMRKOCtrl',
				controllerAs: 'DetailItemMRKOCtrl',
				resolve: {
					item: function () {
						return item;
					}
				}
			});
			modalInstance.result.then(function () {
				vm.init();

			});
		}

		function cekAcknowledgement() {
			DashboardVendorService.cekAcknowledgement({
			}, function (reply) {
				unloadLoading();
				vm.monitoringPOID = reply.data;
				//console.info("monPOID" + vm.monitoringPOID);
				if (vm.monitoringPOID != 0) {
					popUP(vm.monitoringPOID);
				} else {
				}
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		function CPR(current) {
			vm.cpr.currentPage = current;
			DashboardVendorService.CPR({
				Offset: vm.cpr.pageSize * (vm.cpr.currentPage - 1),
				Limit: vm.cpr.pageSize
			}, function (reply) {
				unloadLoading();
				vm.listCPRVendor = reply.data.List;
				vm.cpr.totalItems = reply.data.Count;
				//console.info("CPRvendor:" + JSON.stringify(reply.data));
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		function CPRVHS(current) {
			vm.cprvhs.currentPage = current;
			DashboardVendorService.CPRVHS({
				Offset: vm.cprvhs.pageSize * (vm.cprvhs.currentPage - 1),
				Limit: vm.cprvhs.pageSize
			}, function (reply) {
				unloadLoading();
				vm.listCPRVHSVendor = reply.data.List;
				vm.cprvhs.totalItems = reply.data.Count;
				//console.info("CPRvendor:" + JSON.stringify(reply.data));
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			});
		}

		function cekAcknowledgementContract() {
			DashboardVendorService.cekAcknowledgementContract({
			}, function (result) {
				vm.monitoringContract = result.data;
				if (vm.monitoringContract != 0) {
					popUpContract();
				} else {
					// cekAcknowledgementFPA
					cekAcknowledgementFPA()
				}
			}, function (fail) {
				//unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			})
		}

		function cekAcknowledgementFPA() {
			DashboardVendorService.cekAcknowledgementFPA({
			}, function (result) {
				vm.monitoringFPA = result.data;
				if (vm.monitoringFPA != 0) {
					popUpFPA()
				} else {
					// cekAcknowledgementVHS
					cekAcknowledgementVHS()
				}
			}, function (fail) {
				//unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			})
		}

		function cekAcknowledgementVHS() {
			DashboardVendorService.cekAcknowledgementFPA({
			}, function (result) {
				vm.monitoringVHS = result.data;
				if (vm.monitoringFPA != 0) {
					popUpVHS()
				} else {
					// cekAcknowledgementVHS
					//cekAcknowledgementVHS()
				}
			}, function (fail) {
				//unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_TENDER');
			})
		}

		function popUpContract() {
			var data = {
				list: vm.paketCSO.list
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/pop-up-contract.html',
				controller: 'PopUpAcknowledgementCtrl',
				controllerAs: 'PopUpAcknowledgementCtrl',
				resolve: {
					item: function () {
						return data;
					}
				},
				backdrop: 'static',
				keyboard: false
			});
			modalInstance.result.then(function () {
				var data = {
					list: vm.paketCSO.list
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'pop-up-contract-detail.html',
					controller: 'PopUpAcknowledgementCtrl',
					controllerAs: 'DVCtrl',
					resolve: {
						item: function () {
							return data;
						}
					},
					backdrop: 'static',
					keyboard: false
				})
				modalInstance.result.then(function () {
					updateCSO(1)
					cekAcknowledgementContract()
				})
			})
		}

		function getVhsOnly(arrayInput) {
			return arrayInput.Type === 'VHS'
		}

		function popUpFPA() {
			var data = {
				list: vm.paketFPA.list
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/pop-up-fpa.html',
				controller: 'PopUpAcknowledgementCtrl',
				controllerAs: 'PopUpAcknowledgementCtrl',
				resolve: {
					item: function () {
						return data;
					}
				},
				backdrop: 'static',
				keyboard: false
			});
			modalInstance.result.then(function () {
				var vhsOnly = vm.paketMRKO.list.filter(getVhsOnly)

				var data = {
					list: vm.paketFPA.list.concat(vhsOnly)
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'pop-up-contract-detail.html',
					controller: 'PopUpAcknowledgementCtrl',
					controllerAs: 'DVCtrl',
					resolve: {
						item: function () {
							return data;
						}
					},
					backdrop: 'static',
					keyboard: false
				})
				modalInstance.result.then(function () {
					updateCSO(1)
					cekAcknowledgementFPA()
				})
			})
		}

		function popUpVHS() {
			var data = {
				list: vm.paketVHS.list
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/pop-up-fpa.html',
				controller: 'PopUpAcknowledgementCtrl',
				controllerAs: 'PopUpAcknowledgementCtrl',
				resolve: {
					item: function () {
						return data;
					}
				},
				backdrop: 'static',
				keyboard: false
			});
			modalInstance.result.then(function () {
				var data = {
					list: vm.paketFPA.list
				}
				var modalInstance = $uibModal.open({
					templateUrl: 'pop-up-contract-detail.html',
					controller: 'PopUpAcknowledgementCtrl',
					controllerAs: 'DVCtrl',
					resolve: {
						item: function () {
							return data;
						}
					},
					backdrop: 'static',
					keyboard: false
				})
				modalInstance.result.then(function () {
					updateCSO(1)
					cekAcknowledgementFPA()
				})
			})
		}

		function popUP(id) {
			var data = {
				monitoringPOID: id,
				list: vm.paketCSO.list
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/pop-up.html',
				controller: 'PopUpAcknowledgementCtrl',
				controllerAs: 'PopUpAcknowledgementCtrl',
				resolve: {
					item: function () {
						return data;
					}
				},
				backdrop: 'static',
				keyboard: false
			});
			modalInstance.result.then(function () {
				//init();
			});
		}

		vm.loadWarning = loadWarning;
		function loadWarning() {
			DashboardVendorService.getDataWarning({
				Offset: vm.warning.pageSize * (vm.warning.currentPage - 1),
				Limit: vm.warning.pageSize
			}, function (reply) {
				unloadLoading();
				vm.warning.list = reply.data.List;
				vm.warning.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_WARNING');
			});
		}

		//Detail Warning Letter
		vm.detailWarning = detailWarning;
		function detailWarning(data) {
			var data = {
				item: data
			}
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/detailWarning.html',
				controller: 'detailWarningCtrl',
				controllerAs: 'detailWarningCtrl',
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			modalInstance.result.then(function () {
				//vm.jLoad(1);
			})
		}

		vm.stateComplete = stateComplete;
		function stateComplete(data) {
			var item = {
				LetterID: data.LetterID,
			};
			var modalInstance = $uibModal.open({
				templateUrl: 'app/modules/rekanan/dashboard/detailApprovalWarning.html',
				controller: 'detailAppWLVendorCtrl',
				controllerAs: 'detailAppCtrl',
				resolve: { item: function () { return item; } }
			});
			modalInstance.result.then(function () {
				loadWarning();
			});
		}

		/*
		vm.stateReviseByVendor = stateReviseByVendor;
		function stateReviseByVendor(data) {
		    var item = {
		        LetterID: data.LetterID,
		    };
		    var modalInstance = $uibModal.open({
		        templateUrl: 'app/modules/rekanan/dashboard/detailApprovalWarning.html',
		        controller: 'detailAppCtrl',
		        controllerAs: 'detailAppCtrl',
		        resolve: { item: function () { return item; } }
		    });
		    modalInstance.result.then(function () {
		        vm.jLoad(1);
		    });
		}
        */

		vm.loadCSO = loadCSO;
		function loadCSO(current) {
			vm.currentPageCSO = current;
			DashboardVendorService.getDataCSO({
				Offset: vm.paketCSO.pageSize * (vm.currentPageCSO - 1),
				Limit: vm.paketCSO.pageSize,
				Keyword: vm.paketCSO.keyword
			}, function (reply) {
				unloadLoading();
				vm.paketCSO.list = reply.data.List;
				vm.paketCSO.totalItems = reply.data.Count;
				calculateTotalTenderAward();
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_WARNING');
			});
		}

		vm.updateCSO = updateCSO;
		function updateCSO(current) {
			vm.currentPageCSO = current;
			DashboardVendorService.getDataCSO({
				Offset: vm.paketCSO.pageSize * (vm.currentPageCSO - 1),
				Limit: vm.paketCSO.pageSize,
				Keyword: vm.paketCSO.keyword
			}, function (reply) {
				vm.paketCSO.list = reply.data.List;
				vm.paketCSO.totalItems = reply.data.Count;
			}, function (error) {
				unloadLoading();
				UIControlService.msg_growl("error", 'MESSAGE.ERR_LOAD_WARNING');
			});
		}

		function calculateTotalTenderAward() {
			vm.totalTenderAward = 0;

			vm.totalTenderAward += +vm.paketPO.totalItems;
			vm.totalTenderAward += +vm.paketMRKO.totalItems;
			vm.totalTenderAward += +vm.paketCSO.totalItems;
			vm.totalTenderAward += +vm.paketFPA.totalItems;
		}
	}

})();

function Kata(srcText) {
	var self = this;
	self.srcText = srcText;
}