﻿(function () {
    'use strict';

    angular.module("app").controller("detailWarningCtrl", ctrl);

    ctrl.$inject = ['$http', '$translate', '$translatePartialLoader', '$location', 'SocketService',
        'WarningLetterService', 'UIControlService', 'item', '$uibModal', '$uibModalInstance'];
    function ctrl($http, $translate, $translatePartialLoader, $location, SocketService, WarningLetterService,
        UIControlService, item, $uibModal, $uibModalInstance) {
        var vm = this;
        var page_id = 141;

        vm.isAdd = item.act;
        vm.Area;
        vm.LetterID;
        vm.Description = "";
        vm.action = "";
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.pageSize = 10;
        
        vm.init = init;
        vm.warningLetter = {};
        vm.isCalendarOpened = [false, false, false, false];

        function init() {
         
            if (item.item.Type == 1)
                vm.GoodOrService = 'Pengaduan';
            else if (item.item.Type == 2)
                vm.GoodOrService = 'CPR';
            else if (item.item.Type == 3)
                vm.GoodOrService = 'VHS';
            vm.CreatedDate = item.item.CreatedDate;
            vm.VendorName = item.item.Vendor.VendorName;
            vm.businessType = item.item.Vendor.businessName;
            vm.WarningType = item.item.ComplainType;
            vm.StartDate = item.item.StartDate;
            vm.EndDate = item.item.EndDate;
            vm.Area = item.item.Area;
            vm.Description = item.item.Description;

            if (item.item.department == null) {
                vm.Reporter = item.item.DepartmentName;
            }
            else {
                vm.Reporter = item.item.department.department.DepartmentName;
            }
            
            vm.Status = item.item.StatusAppName;

            if (item.item.department == null) {
                vm.DeptCode = "-";
            }
            else {
                vm.DeptCode = item.item.department.department.DepartmentCode;
            }

            vm.TemplateDoc = item.item.TemplateDoc;          
            vm.complaintName = item.item.ComplainType.TypeName;
        }

        vm.batal = batal;
        function batal() {
            $uibModalInstance.dismiss('cancel');
        };


    }
})();