﻿(function () {
	angular.module("app").controller("MyAccountCtrl", ctrl);

	ctrl.$inject = ['$translatePartialLoader', '$state', 'UIControlService', 'AuthService'];

	function ctrl($translatePartialLoader, $state, UIControlService, AuthService) {
		var vm = this;

		vm.init = init;
		function init() {
			$translatePartialLoader.addPart('my-account');
		}

		vm.chgPasswdInit = chgPasswdInit;
		function chgPasswdInit() {
			$translatePartialLoader.addPart('change-password');
		}

		vm.toChangePassword = toChangePassword;
		function toChangePassword() {
			$state.transitionTo('change-password', {});
		}

		vm.toMyAccount = toMyAccount;
		function toMyAccount() {
			$state.transitionTo('view-akun-user', {});
		}

		vm.changePassword = changePassword;
		function changePassword() {
			var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
			if (!vm.passwd.match(passw)) {
				UIControlService.msg_growl('error', 'ERRORS.MATCHPassword');
				return;
			}

			if (vm.passwd === null || vm.passwd === '') {
				return false;
			}
			UIControlService.loadLoadingModal('LOADING.CHANGE.PASSWORD');
			AuthService.changePassword({
				Password: vm.passwd,
				OldPassword: vm.oldPasswd
			}, function (reply) {
				UIControlService.unloadLoadingModal();
				if (reply.status === 200) {
					UIControlService.msg_growl("success", 'MESSAGE.SUC_CHANGE', 'MESSAGE.SUC_CHANGE_BIG');
					toMyAccount();
				} else {
					UIControlService.msg_growl("error", 'MESSAGE.ERR_CHANGE', 'MESSAGE.ERR_CHANGE_BIG');
				}
			}, function (err) {
				UIControlService.unloadLoadingModal();
				UIControlService.msg_growl("error", err.Message);
			});
		}
	}
})();